ALTER TABLE LAB DISABLE CONSTRAINT FK_LAB_01; 
ALTER TABLE SAMPLE_TEST_SCHEDULE DISABLE CONSTRAINT FK_SAMPLE_TEST_SCHEDULE_09; 

set scan off

declare

	procedure f_ins_lab_contact(
		ls_code in varchar2,
		ls_name in varchar2,
		ls_first_name in varchar2,
		ls_last_name in varchar2,
		ls_title in varchar2,
		ls_position in varchar2,
		ls_office_phone in varchar2,
		ls_fax in varchar2,
		ls_email in varchar2,
		ls_address1 in varchar2,
		ls_address2 in varchar2)
  	is
  		ll_contact_id	number;
  		
	begin
		select contact_id
		into   ll_contact_id
		from   lab
		where  lab_code = ls_code;
		
		update lab
		set lab_name = ls_name,
			modify_date = sysdate,
			version = version + 1
		where lab_code = ls_code;
		
		update contact
		set first_name = nvl(ls_first_name, '-'),
			last_name = ls_last_name,
			position = ls_position,
			title = ls_title,
			office_phone = ls_office_phone,
			fax = ls_fax,
			email = ls_email,
			address1 = ls_address1,
			address2 = ls_address2,
			modify_date = sysdate,
			version = version + 1
		where contact_id = ll_contact_id;		
				
	exception when NO_DATA_FOUND then
	
		insert into contact (
			contact_id, first_name, last_name, position, title, office_phone, fax,
			email, address1, address2,
			create_user, create_date, modify_user, modify_date, version
		) values (
			seq_contact.nextval, nvl(ls_first_name, '-'), ls_last_name, ls_position, ls_title, ls_office_phone, ls_fax, 
			ls_email, ls_address1, ls_address2,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);
		
		insert into lab (
			lab_code, lab_name, contact_id,
			create_user, create_date, modify_user, modify_date, version
		) values (
			ls_code, ls_name, seq_contact.currval,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
					
	end;
	
begin

delete contact where contact_id in ( select contact_id from lab );
delete lab;

f_ins_lab_contact('CU', 'HKCU-Bio Unit', 'Sherry', 'Lam', 'MS', 'Technical Manager', '26096866', '26035295', 'sherryl@cuhk.edu.hk', 'Drug Development Centre, School of Pharmacy', 'The Chinese University of Hong Kong, Shatin, NT');
f_ins_lab_contact('SGS', 'SGS HK Ltd.', 'Alan', 'Ng', 'MR', 'Senior Customer Service Executive', '26099611', '26037577', 'alan.ng@sgs.com', '12/F, Metropole Square, 2 On Yiu Street', 'Siu Lek Yuen, Shatin, N.T., Hong Kong');
f_ins_lab_contact('INT', 'Intertek Test HK Ltd', 'Karmen', 'Lai', 'MI', 'Senior Technical Officer', '21738748', '34032507', 'karmen.lai@intertek.com', '2/F, Garment Centre, 576 Castle Peak Road', 'Kowloon, Hong Kong');



commit;
 
end;
/

set scan on

ALTER TABLE LAB ENABLE CONSTRAINT FK_LAB_01;
ALTER TABLE SAMPLE_TEST_SCHEDULE ENABLE CONSTRAINT FK_SAMPLE_TEST_SCHEDULE_09; 
