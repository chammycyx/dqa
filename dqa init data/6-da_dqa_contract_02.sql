set scan off
set serveroutput on

declare

	procedure f_add_change_contract(
		ls_contract_num in varchar2,
		ls_contract_suffix in varchar2,
		ls_item_code in varchar2,
		ls_contract_type in varchar2,
		ls_supplier_code in varchar2,
		ls_pharm_company_code in varchar2,
		ls_manuf_code in varchar2,
		ls_contract_start in varchar2,
		ls_contract_end in varchar2,
		ls_currency_code in varchar2,
		ll_pack_size in number,
		ll_pack_price in number,
		ls_suspend in varchar2)
	is
  		ll_contract_id	number;
  		
	begin
			
		select contract_id
		into   ll_contract_id
		from   contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
	
		update contract
		set    supplier_code = ls_supplier_code,
			   pharm_company_code = ls_pharm_company_code,
			   manuf_code = ls_manuf_code,
			   start_date = to_date(ls_contract_start, 'yyyy.mm.dd'),
			   end_date = to_date(ls_contract_end, 'yyyy.mm.dd'),
			   currency_code = ls_currency_code,
			   pack_size = ll_pack_size,
			   pack_price = ll_pack_price,
			   suspend_flag = ls_suspend,
			   modify_date = sysdate,
			   modify_user = 'dqaadmin'
		where  contract_id = ll_contract_id;
		
	exception when NO_DATA_FOUND then
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
	
	when TOO_MANY_ROWS then
	
		delete from contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
		
		dbms_output.put_line('Duplicate row found! ' || ls_contract_num || ', ' || ls_contract_suffix || ', ' || ls_contract_type || ', ' || ls_item_code);
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
		
	end;
	
begin

f_add_change_contract( 'SQ11-247-01', '', 'CARD03', 'Q', 'ZUEL', 'HOSP', 'HAME', '2012.03.29', '2013.03.28', 'HKD', 5, 975.0, 'N');
f_add_change_contract( 'SQ10-350-01', '', 'CEFA08', 'Q', 'TULL', 'ZHUH', 'ZHUH', '2011.05.14', '2012.05.13', 'HKD', 10, 30.0, 'N');
f_add_change_contract( 'HOC283-10', 'A', 'CEFE04', 'C', 'DKSH', 'BMS', 'BMS', '2011.04.06', '2013.04.05', 'HKD', 1, 102.0, 'N');
f_add_change_contract( 'HOC038-11', 'A', 'CEFO04', 'C', 'MPCL', 'YANG', 'YANG', '2011.07.11', '2013.07.10', 'HKD', 1, 11.5, 'N');
f_add_change_contract( 'HOC139-10', 'A', 'CEFT01', 'C', 'ZUEL', 'GSK', 'GSK', '2010.10.24', '2012.10.23', 'HKD', 1, 12.4, 'N');
f_add_change_contract( 'HOC139-10', 'B', 'CEFT02', 'C', 'ZUEL', 'GSK', 'GSK', '2010.10.24', '2012.10.23', 'HKD', 1, 25.7, 'N');
f_add_change_contract( 'HOC139-10', 'C', 'CEFT03', 'C', 'ZUEL', 'GSK', 'GSK', '2010.10.24', '2012.10.23', 'HKD', 1, 51.7, 'N');
f_add_change_contract( 'HOC228-11', 'A', 'CEFT14', 'C', 'MEYE', 'LIVZ', 'LIVZ', '2012.04.19', '2014.04.18', 'HKD', 10, 68.0, 'N');
f_add_change_contract( 'HOC043-10', 'A', 'CEFU02', 'C', 'JPL', 'SZP', 'SZP', '2010.08.08', '2012.08.07', 'HKD', 10, 46.9, 'N');
f_add_change_contract( 'SQ11-359-01', '', 'CHOR02', 'Q', 'DKSH', 'ORGA', 'ORGA', '2012.03.06', '2013.03.05', 'HKD', 1, 50.0, 'N');
f_add_change_contract( 'HOC070-11', 'A1', 'CISA04', 'C', 'ZUEL', 'GSK', 'GSKM', '2011.09.21', '2012.08.23', 'HKD', 5, 175.0, 'N');
f_add_change_contract( 'SQ11-292-01', '', 'CISP02', 'Q', 'TIMC', 'PBV', 'PBV', '2011.10.25', '2012.10.24', 'HKD', 1, 63.7, 'N');
f_add_change_contract( 'SQ11-261-01', '', 'CLAR05', 'Q', 'ZUEL', 'ABBO', 'FAMI', '2012.03.29', '2013.03.28', 'HKD', 1, 92.0, 'N');
f_add_change_contract( 'SQ10-490-01', '', 'CLIN03', 'Q', 'JPL', 'SX', 'SX', '2011.08.20', '2012.08.19', 'HKD', 5, 49.99, 'N');
f_add_change_contract( 'HOC089-10', 'A2', 'CLOX05', 'C', 'LFAU', 'PANP', 'PANP', '2011.10.06', '2012.11.24', 'HKD', 50, 220.0, 'N');
f_add_change_contract( 'SQ10-413-01', '', 'CYAN02', 'Q', 'STAM', 'ROTE', 'ROTE', '2011.07.29', '2012.07.28', 'HKD', 10, 21.0, 'N');
f_add_change_contract( 'SQ10-352-01', '', 'CYCL10', 'Q', 'BAXT', 'BAXT', 'BO', '2011.05.21', '2012.05.20', 'HKD', 1, 53.1, 'N');
f_add_change_contract( 'HOC260-10', 'A', 'CYCL13', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.02.14', '2013.02.13', 'HKD', 10, 3308.0, 'N');
f_add_change_contract( 'SQ11-373-01', '', 'CYTA02', 'Q', 'ZUEL', 'PFIZ', 'ACTA', '2012.02.24', '2013.02.23', 'HKD', 1, 135.0, 'N');
f_add_change_contract( 'SQ11-235-01', '', 'CYTA05', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2011.12.04', '2012.12.03', 'HKD', 1, 250.0, 'N');
f_add_change_contract( 'HOC062-10', 'A', 'DACA02', 'C', 'ZUEL', 'HOSP', 'HOSP', '2010.09.13', '2012.12.12', 'HKD', 1, 730.0, 'N');
f_add_change_contract( 'SQ10-293-01', '', 'DAUN01', 'Q', 'ZUEL', 'PFIZ', 'ACTA', '2011.04.08', '2012.04.07', 'HKD', 1, 208.0, 'N');
f_add_change_contract( 'SQ11-304-01', '', 'DAUN01', 'Q', 'ZUEL', 'PFIZ', 'ACTA', '2012.04.08', '2013.04.07', 'HKD', 1, 208.0, 'N');
f_add_change_contract( 'HOC304-10', 'A', 'DESF01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.04.21', '2013.04.20', 'HKD', 130, 3950.0, 'N');
f_add_change_contract( 'SQ10-507-01', '', 'DESM02', 'Q', 'FERR', 'FERR', 'FERR', '2011.08.31', '2012.08.30', 'HKD', 10, 800.0, 'N');
f_add_change_contract( 'SQ11-360-01', '', 'DESM06', 'Q', 'FERR', 'FERR', 'FERI', '2012.03.06', '2013.03.05', 'HKD', 10, 1200.0, 'N');
f_add_change_contract( 'SQ11-087-01', '', 'DEXA10', 'Q', 'HKMS', 'WEIM', 'WEIM', '2011.10.06', '2012.10.05', 'EUR', 50, 13.33, 'N');
f_add_change_contract( 'HOC106-10', 'A', 'DIAZ05', 'C', 'LCH', 'CPPH', 'CPPH', '2010.12.13', '2012.12.12', 'HKD', 10, 136.0, 'N');
f_add_change_contract( 'SQ11-144-01', '', 'DIAZ06', 'Q', 'DKSH', 'LIPU', 'BB', '2011.11.03', '2012.11.02', 'HKD', 10, 100.0, 'N');
f_add_change_contract( 'SQ10-432-01', '', 'DIGO06', 'Q', 'LCH', 'SIGP', 'KERN', '2011.07.09', '2012.07.08', 'HKD', 5, 143.0, 'N');
f_add_change_contract( 'SQ10-333-01', 'A1', 'DILT04', 'Q', 'PRIM', 'MTB', 'TAKS', '2012.03.24', '2012.05.08', 'HKD', 10, 480.0, 'N');
f_add_change_contract( 'SQ11-361-01', '', 'DINO04', 'Q', 'ZUEL', 'PFIZ', 'PMBN', '2012.03.06', '2013.03.05', 'HKD', 1, 855.0, 'N');
f_add_change_contract( 'SQ11-067-01', '', 'DOBU01', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2011.10.07', '2012.10.06', 'HKD', 1, 27.6, 'N');
f_add_change_contract( 'HOC014-10', 'A1', 'DOCE01', 'C', 'LFHC', 'SA', 'APL', '2011.07.01', '2013.06.19', 'HKD', 1, 1223.0, 'N');
f_add_change_contract( 'HOC014-10', 'B1', 'DOCE02', 'C', 'LFHC', 'SA', 'APL', '2011.07.01', '2013.06.19', 'HKD', 1, 4375.0, 'N');
f_add_change_contract( 'SQ11-126-01', '', 'DOXO02', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.11.03', '2012.11.02', 'HKD', 1, 67.3, 'N');
f_add_change_contract( 'SQ10-494-01', '', 'EPHE04', 'Q', 'ZUEL', 'HOSP', 'HAME', '2011.08.31', '2012.08.30', 'HKD', 5, 143.5, 'N');
f_add_change_contract( 'HOC212-11', 'A', 'EPIR01', 'C', 'ZUEL', 'PFIZ', 'PFIP', '2012.04.06', '2014.04.05', 'HKD', 1, 85.0, 'N');
f_add_change_contract( 'SQ11-197-01', '', 'EPIR01', 'Q', 'ZUEL', 'XXX', 'PFIZ', '2011.10.06', '2012.04.05', 'HKD', 1, 100.0, 'N');
f_add_change_contract( 'HOC212-11', 'B', 'EPIR02', 'C', 'ZUEL', 'PFIZ', 'PFIP', '2012.04.06', '2014.04.05', 'HKD', 1, 269.0, 'N');
f_add_change_contract( 'HOC046-10', 'A', 'ERTA01', 'C', 'DKSH', 'MER', 'MSD', '2010.07.21', '2012.07.20', 'HKD', 1, 228.0, 'N');
f_add_change_contract( 'SQ11-279-01', '', 'ETOM02', 'Q', 'DKSH', 'BB', 'BB', '2012.03.15', '2013.03.14', 'HKD', 10, 380.0, 'N');
f_add_change_contract( 'SQ11-305-01', '', 'ETOP04', 'Q', 'TIMC', 'PBV', 'PBV', '2012.01.06', '2013.01.05', 'HKD', 1, 83.0, 'N');
f_add_change_contract( 'SQ11-053-01', '', 'ETOP05', 'Q', 'TIMC', 'PBV', 'PBV', '2011.10.21', '2012.10.20', 'HKD', 1, 44.0, 'N');
f_add_change_contract( 'CRC5/11', '', 'FACT09', 'C', 'RC', 'CSL', 'CSL', '2011.08.01', '2012.07.31', 'HKD', 1, 815.0, 'N');
f_add_change_contract( 'HOC027-11', 'A', 'FACT13', 'C', 'UI', 'BAXT', 'BAXA', '2011.05.30', '2013.05.29', 'HKD', 1, 7526.0, 'N');
f_add_change_contract( 'CRC6/11', '', 'FACT14', 'C', 'RC', 'CSL', 'CSL', '2011.08.01', '2012.07.31', 'HKD', 1, 289.0, 'N');
f_add_change_contract( 'HOC270-10', 'A', 'FACT17', 'C', 'PRIM', 'CSL', 'CSL', '2011.03.11', '2013.03.10', 'HKD', 1, 468.0, 'N');
f_add_change_contract( 'HOC028-11', 'A', 'FACT20', 'C', 'DKSH', 'NOVO', 'NOVO', '2011.05.16', '2012.05.15', 'HKD', 6, 39100.0, 'N');
f_add_change_contract( 'CRC4/11', 'A1', 'FACT22', 'C', 'RC', 'CSBA', 'CSL', '2012.03.14', '2012.07.31', 'HKD', 1, 349.0, 'N');
f_add_change_contract( 'HOC021-10', 'A', 'FENT01', 'C', 'MEKI', 'HAME', 'HAME', '2010.10.21', '2012.10.20', 'HKD', 10, 30.7, 'N');
f_add_change_contract( 'HOC021-10', 'B', 'FENT02', 'C', 'MEKI', 'HAME', 'HAME', '2010.10.21', '2012.10.20', 'HKD', 10, 104.0, 'N');
f_add_change_contract( 'HOC166-10', 'A', 'FILG01', 'C', 'DKSH', 'ROCE', 'FHR', '2010.12.08', '2012.12.07', 'HKD', 5, 4500.0, 'N');
f_add_change_contract( 'SQ10-353-01', 'A1', 'FLUD03', 'Q', 'LFHC', 'BO', 'BO', '2012.03.01', '2012.05.28', 'HKD', 5, 7680.0, 'N');
f_add_change_contract( 'HOC071-11', 'A', 'FLUO44', 'C', 'FOCL', 'FOCL', 'FOCL', '2011.08.01', '2013.07.31', 'HKD', 1, 850.0, 'N');
f_add_change_contract( 'HOC071-11', 'B', 'FLUO45', 'C', 'FOCL', 'FOCL', 'FOCL', '2011.08.01', '2013.07.31', 'HKD', 1, 1570.0, 'N');
f_add_change_contract( 'HOC071-11', 'C', 'FLUO46', 'C', 'FOCL', 'FOCL', 'FOCL', '2011.08.01', '2013.07.31', 'HKD', 1, 2050.0, 'N');
f_add_change_contract( 'HOC136-11', 'A', 'FRUS03', 'C', 'MEKI', 'PH', 'PH', '2012.03.17', '2014.03.16', 'HKD', 10, 25.3, 'N');
f_add_change_contract( 'HOC287-10', 'A', 'GANC01', 'C', 'DKSH', 'ROCE', 'FHR', '2011.02.21', '2013.02.20', 'HKD', 5, 2995.0, 'N');
f_add_change_contract( 'HOC167-11', 'A', 'GEMC01', 'C', 'HKMS', 'SUNL', 'SUNL', '2012.03.23', '2014.03.22', 'HKD', 1, 88.7, 'N');
f_add_change_contract( 'HOC167-11', 'B', 'GEMC02', 'C', 'HKMS', 'SUNL', 'SUNL', '2012.03.23', '2014.03.22', 'HKD', 1, 494.0, 'N');
f_add_change_contract( 'SQ11-056-01', '', 'GENT02', 'Q', 'STAM', 'ROTE', 'ROTE', '2011.10.07', '2012.10.06', 'HKD', 10, 24.3, 'N');
f_add_change_contract( 'HOC069-11', 'A', 'GLUC37', 'C', 'DKSH', 'NOVO', 'NOVO', '2011.09.29', '2013.09.28', 'HKD', 1, 500.0, 'N');
f_add_change_contract( 'HOC221-11', 'A', 'GRAN01', 'C', 'DKSH', 'ROCE', 'CENE', '2012.02.26', '2014.02.25', 'HKD', 5, 868.0, 'N');
f_add_change_contract( 'HOC221-11', 'B', 'GRAN04', 'C', 'DKSH', 'ROCE', 'CENE', '2012.02.26', '2014.02.25', 'HKD', 5, 310.0, 'N');
f_add_change_contract( 'HOC327-10', 'A1', 'HALO08', 'C', 'LFAU', 'BB', 'BB', '2011.10.06', '2013.07.05', 'HKD', 10, 250.0, 'N');
f_add_change_contract( 'HOC022-11', 'A', 'HALO09', 'C', 'DKSH', 'JP', 'JP', '2011.06.05', '2013.06.04', 'HKD', 5, 400.0, 'N');
f_add_change_contract( 'HOC022-11', 'B', 'HALO11', 'C', 'DKSH', 'JP', 'JP', '2011.06.05', '2013.06.04', 'HKD', 5, 788.0, 'N');
f_add_change_contract( 'HOC200-10', 'A', 'HEPA03', 'C', 'LCH', 'WOCK', 'CPP', '2011.03.14', '2013.03.13', 'HKD', 10, 100.0, 'N');
f_add_change_contract( 'HOC201-10', 'A', 'HEPA04', 'C', 'DKSH', 'BB', 'BB', '2011.05.02', '2013.05.01', 'HKD', 10, 250.0, 'N');
f_add_change_contract( 'SQ10-325-01', '', 'HEPA11', 'Q', 'DKSH', 'BB', 'BB', '2011.04.18', '2012.04.17', 'HKD', 20, 80.0, 'N');
f_add_change_contract( 'SQ11-317-01', '', 'HEPA11', 'Q', 'DKSH', 'BB', 'BB', '2012.04.19', '2013.04.18', 'HKD', 20, 90.0, 'N');
f_add_change_contract( 'SQ11-173-01', '', 'HEPA15', 'Q', 'ZUEL', 'HOSP', 'HAME', '2011.12.06', '2012.12.05', 'HKD', 50, 750.0, 'N');
f_add_change_contract( 'HOC234-10', 'A', 'HYAL01', 'C', 'LCH', 'WOCK', 'CPP', '2011.02.21', '2013.02.20', 'HKD', 10, 1750.0, 'N');
f_add_change_contract( 'HOC172-10', 'A1', 'HYDR07', 'C', 'ZUEL', 'PMBN', 'PMBN', '2011.11.01', '2012.10.31', 'HKD', 1, 10.5, 'N');
f_add_change_contract( 'SQ11-364-01', '', 'HYOS03', 'Q', 'ZUEL', 'HOSP', 'HAME', '2012.03.06', '2013.03.05', 'HKD', 5, 350.0, 'N');
f_add_change_contract( 'SQ11-243-01', '', 'HYOS04', 'Q', 'ZUEL', 'BOEH', 'BOEH', '2012.03.31', '2013.03.30', 'HKD', 100, 235.0, 'N');
f_add_change_contract( 'HOC142-10', 'A', 'IFOS01', 'C', 'BAXT', 'BAXT', 'BO', '2010.11.19', '2012.11.18', 'HKD', 1, 238.1, 'N');
f_add_change_contract( 'SQ11-039-01', 'A1', 'IMIG02', 'Q', 'LFHC', 'GENZ', 'GENI', '2012.03.01', '2012.10.31', 'HKD', 1, 14750.5, 'N');
f_add_change_contract( 'SQ10-520-01', '', 'INDO01', 'Q', 'HKMS', 'DAIS', 'DAIS', '2011.05.17', '2012.05.16', 'HKD', 10, 2650.0, 'N');
f_add_change_contract( 'HOC250-10', 'A', 'INFL03', 'C', 'DKSH', 'CENT', 'CILA', '2011.02.09', '2013.02.08', 'HKD', 1, 4800.0, 'N');
f_add_change_contract( 'SQ10-488-01', '', 'INTE10', 'Q', 'ZUEL', 'BOEH', 'BOEH', '2011.08.26', '2012.08.25', 'HKD', 6, 7920.0, 'N');
f_add_change_contract( 'HOC078-11', 'A1', 'INTE20', 'C', 'ZUEL', 'BH', 'BH', '2011.11.01', '2013.08.30', 'HKD', 15, 7200.0, 'N');
f_add_change_contract( 'HOC003-11', 'C', 'IOHE02', 'C', 'GEM', 'GE', 'GESH', '2011.06.02', '2013.06.01', 'HKD', 1, 44.0, 'N');
f_add_change_contract( 'HOC003-11', 'A', 'IOHE03', 'C', 'GEM', 'GE', 'GESH', '2011.06.02', '2013.06.01', 'HKD', 1, 40.0, 'N');
f_add_change_contract( 'HOC003-11', 'B', 'IOHE07', 'C', 'GEM', 'GE', 'GESH', '2011.06.02', '2013.06.01', 'HKD', 1, 65.0, 'N');
f_add_change_contract( 'HOC003-11', 'D', 'IOHE09', 'C', 'GEM', 'GE', 'GESH', '2011.06.02', '2013.06.01', 'HKD', 1, 76.0, 'N');
f_add_change_contract( 'SQ11-474-01', '', 'IOME04', 'Q', 'BFEL', 'IDS', 'PATI', '2012.03.22', '2013.03.21', 'HKD', 1, 38.0, 'N');
f_add_change_contract( 'SQ11-475-01', '', 'IOME06', 'Q', 'BFEL', 'IDS', 'PATI', '2012.03.22', '2013.03.21', 'HKD', 1, 140.0, 'N');
f_add_change_contract( 'SQ10-384-01', '', 'ISOP12', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2011.06.10', '2012.06.09', 'HKD', 10, 1500.0, 'N');
f_add_change_contract( 'SQ11-418-01', '', 'ISOP12', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2012.06.10', '2013.06.09', 'HKD', 10, 1600.0, 'N');
f_add_change_contract( 'HOC049-10', 'A', 'ISOS05', 'C', 'ZUEL', 'SCHW', 'SCHW', '2010.08.12', '2012.08.11', 'HKD', 10, 218.0, 'N');
f_add_change_contract( 'HOC049-11', 'A', 'KETA02', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.09.03', '2013.09.02', 'HKD', 1, 250.0, 'N');
f_add_change_contract( 'HOC049-11', 'B', 'KETA04', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.09.03', '2013.09.02', 'HKD', 1, 182.0, 'N');
f_add_change_contract( 'HOC016-11', 'A', 'LABE05', 'C', 'LCH', 'UB', 'UB', '2011.07.05', '2013.07.04', 'HKD', 5, 145.0, 'N');
f_add_change_contract( 'HOC065-10', 'A1', 'LEUP03', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.08.25', '2012.08.24', 'HKD', 5, 14400.0, 'N');
f_add_change_contract( 'HOC065-10', 'A', 'LEUP03', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.08.25', '2012.08.24', 'HKD', 48, 134400.0, 'N');
f_add_change_contract( 'SQ11-090-01', '', 'LIGN02', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2011.10.05', '2012.10.04', 'HKD', 1, 45.0, 'N');
f_add_change_contract( 'SQ11-091-01', '', 'LIGN18', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2011.10.05', '2012.10.04', 'HKD', 50, 135.5, 'N');
f_add_change_contract( 'SQ10-456-01', '', 'LIGN53', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.24', '2012.07.23', 'HKD', 50, 279.0, 'N');
f_add_change_contract( 'HOC187-10', 'B', 'LINE13', 'C', 'ZUEL', 'PFIZ', 'FK', '2010.12.07', '2012.06.06', 'HKD', 1, 430.0, 'N');
f_add_change_contract( 'HOC053-10', 'A', 'LIPI01', 'C', 'HW', 'GUER', 'GUER', '2010.09.14', '2012.09.13', 'HKD', 1, 147.5, 'N');
f_add_change_contract( 'HOC090-11', 'A', 'MAGN13', 'C', 'ZUEL', 'HOSP', 'HAME', '2011.07.31', '2012.07.30', 'HKD', 10, 280.0, 'N');
f_add_change_contract( 'SQ10-473-01', '', 'MELP03', 'Q', 'PRIM', 'GSK', 'GSK', '2011.07.22', '2012.07.21', 'HKD', 1, 2880.0, 'N');
f_add_change_contract( 'HOC240-10', 'A', 'MENO01', 'C', 'FERR', 'IM', 'FERR', '2010.12.24', '2012.12.23', 'HKD', 10, 445.0, 'N');
f_add_change_contract( 'HOC181-11', 'A', 'MESN04', 'C', 'BAXT', 'BAXT', 'BO', '2012.03.02', '2014.03.01', 'HKD', 15, 709.8, 'N');
f_add_change_contract( 'HOC088-11', 'A', 'METH30', 'C', 'ZUEL', 'PFIZ', 'PMBN', '2011.10.08', '2013.10.07', 'HKD', 1, 89.0, 'N');
f_add_change_contract( 'SQ10-294-01', '', 'METH42', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.04.15', '2012.04.14', 'HKD', 1, 800.0, 'N');
f_add_change_contract( 'SQ11-307-01', '', 'METH42', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2012.04.15', '2013.04.14', 'HKD', 1, 880.0, 'N');
f_add_change_contract( 'SQ11-092-01', '', 'METO03', 'Q', 'LFHC', 'SA', 'SANO', '2011.10.05', '2012.10.04', 'HKD', 6, 22.4, 'N');
f_add_change_contract( 'SQ11-071-01', '', 'METO09', 'Q', 'ZUEL', 'ASTZ', 'CENF', '2011.10.20', '2012.10.19', 'HKD', 5, 1115.0, 'N');
f_add_change_contract( 'HOC112-10', 'A', 'MIDA02', 'C', 'MEKI', 'HOSP', 'HAME', '2011.02.16', '2013.02.15', 'HKD', 5, 19.4, 'N');
f_add_change_contract( 'HOC112-10', 'B', 'MIDA03', 'C', 'MEKI', 'HAME', 'HAME', '2011.02.16', '2013.02.15', 'HKD', 5, 29.4, 'N');
f_add_change_contract( 'SQ11-352-01', '', 'MIDA06', 'Q', 'LFAU', 'ROTE', 'ROTE', '2012.01.12', '2013.01.11', 'HKD', 30, 190.0, 'N');
f_add_change_contract( 'SQ11-074-01', '', 'MILR01', 'Q', 'LFHC', 'SA', 'SWI', '2011.10.20', '2012.10.19', 'HKD', 10, 2606.0, 'N');
f_add_change_contract( 'HOC093-11', 'A', 'MORP35', 'C', 'MEKI', 'HOSP', 'HAME', '2012.02.20', '2014.02.19', 'HKD', 10, 34.2, 'N');
f_add_change_contract( 'DQ11-100-01', '', 'NAJA02', 'C', 'HTPC', 'CDCD', 'CDCD', '2012.03.29', '2012.04.12', 'HKD', 1, 3300.0, 'N');
f_add_change_contract( 'HOC081-10', 'A', 'NALO02', 'C', 'MEKI', 'HAME', 'HAME', '2011.07.26', '2013.07.25', 'HKD', 10, 585.0, 'N');
f_add_change_contract( 'SQ10-440-01', '', 'NEOS06', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.18', '2012.07.17', 'HKD', 50, 309.0, 'N');
f_add_change_contract( 'HOC035-11', 'A', 'NIMO02', 'C', 'ZUEL', 'BH', 'BAYR', '2011.08.27', '2013.08.26', 'HKD', 5, 2900.0, 'N');
f_add_change_contract( 'HOC070-10', 'A', 'NORE02', 'C', 'ZUEL', 'HOSP', 'HOSP', '2010.09.10', '2012.09.09', 'HKD', 10, 805.0, 'N');
f_add_change_contract( 'HOC006-11', 'B', 'OCTR01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.12', '2013.07.11', 'HKD', 5, 880.0, 'N');
f_add_change_contract( 'HOC006-11', 'A', 'OCTR02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.12', '2013.07.11', 'HKD', 5, 465.0, 'N');
f_add_change_contract( 'HOC006-11', 'C', 'OCTR03', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.12', '2013.07.11', 'HKD', 5, 4600.0, 'N');
f_add_change_contract( 'HOC171-11', 'A', 'OCTR04', 'C', 'ZUEL', 'NOVA', 'SAND', '2012.02.06', '2014.02.05', 'HKD', 1, 7500.0, 'N');
f_add_change_contract( 'HOC171-11', 'B', 'OCTR05', 'C', 'ZUEL', 'NOVA', 'SAND', '2012.02.06', '2014.02.05', 'HKD', 1, 9970.0, 'N');
f_add_change_contract( 'HOC171-11', 'C', 'OCTR06', 'C', 'ZUEL', 'NOVA', 'SAND', '2012.02.06', '2014.02.05', 'HKD', 1, 13000.0, 'N');
f_add_change_contract( 'HOC002-11', 'C', 'ONDA03', 'C', 'ZUEL', 'GSK', 'GSKA', '2011.07.07', '2013.07.06', 'HKD', 10, 248.8, 'N');
f_add_change_contract( 'HOC002-11', 'D', 'ONDA04', 'C', 'ZUEL', 'GSK', 'GSKA', '2011.07.07', '2013.07.06', 'HKD', 10, 268.8, 'N');
f_add_change_contract( 'SQ10-458-01', '', 'OXYT03', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.20', '2012.07.19', 'HKD', 100, 530.0, 'N');
f_add_change_contract( 'HOC081-11', 'A', 'PACL08', 'C', 'JPL', 'YANG', 'YANG', '2011.08.06', '2013.08.05', 'HKD', 1, 70.0, 'N');
f_add_change_contract( 'HOC081-11', 'B', 'PACL09', 'C', 'JPL', 'YANG', 'YANG', '2011.08.06', '2013.08.05', 'HKD', 1, 220.0, 'N');
f_add_change_contract( 'HOC020-11', 'A', 'PANT02', 'C', 'ZUEL', 'NYCO', 'NYCO', '2011.04.22', '2012.04.21', 'HKD', 1, 28.0, 'N');
f_add_change_contract( 'SQ10-502-01', '', 'PAPA03', 'Q', 'ZUEL', 'HOSP', 'HAME', '2011.08.28', '2012.08.27', 'HKD', 5, 1200.0, 'N');
f_add_change_contract( 'HOC126-11', 'A', 'PEDI03', 'C', 'ZUEL', 'FK', 'FK', '2011.12.16', '2013.12.15', 'HKD', 10, 1200.0, 'N');
f_add_change_contract( 'HOC096-10', 'A1', 'PENT08', 'C', 'LFHC', 'SA', 'GL', '2011.07.01', '2012.12.15', 'HKD', 5, 2800.0, 'N');
f_add_change_contract( 'HOC108-10', 'A', 'PETH02', 'C', 'LCH', 'MARI', 'MARI', '2011.03.13', '2013.03.12', 'HKD', 10, 29.34, 'N');
f_add_change_contract( 'HOC108-10', 'B', 'PETH03', 'C', 'LCH', 'MARI', 'MARI', '2011.03.13', '2013.03.12', 'HKD', 10, 30.36, 'N');
f_add_change_contract( 'SQ11-101-01', '', 'PHEN10', 'Q', 'LCH', 'MARI', 'MARI', '2011.07.25', '2012.07.24', 'HKD', 10, 420.0, 'N');
f_add_change_contract( 'HOC055-10', 'A', 'PHEN34', 'C', 'ZUEL', 'HOSP', 'HAME', '2010.08.12', '2012.08.11', 'HKD', 5, 82.5, 'N');
f_add_change_contract( 'SQ11-182-01', '', 'PHYT04', 'Q', 'DKSH', 'ROCE', 'CENE', '2011.12.04', '2012.12.03', 'HKD', 5, 52.7, 'N');
f_add_change_contract( 'SQ11-244-01', '', 'PHYT06', 'Q', 'DKSH', 'ROCE', 'CENE', '2012.03.15', '2013.03.14', 'HKD', 5, 42.0, 'N');
f_add_change_contract( 'HOC024-11', 'A', 'PIPE03', 'C', 'JPL', 'NCP', 'NCP', '2011.08.14', '2013.08.13', 'HKD', 10, 269.0, 'N');
f_add_change_contract( 'DQ11-118-01', '', 'PLAS08', 'C', 'DKSH', 'GRIF', 'GRIF', '2012.03.29', '2012.04.12', 'HKD', 10, 3656.3, 'N');
f_add_change_contract( 'CRC2/11', '', 'PLAS09', 'C', 'RC', 'CSL', 'CSL', '2011.08.01', '2012.07.31', 'HKD', 1, 131.0, 'N');
f_add_change_contract( 'SQ11-002-01', '', 'POTA18', 'Q', 'ZUEL', 'HOSP', 'HAME', '2011.09.29', '2012.09.28', 'HKD', 10, 260.0, 'N');
f_add_change_contract( 'SQ11-358-01', '', 'PROC10', 'Q', 'STAM', 'MEDO', 'MEDO', '2012.03.27', '2013.03.26', 'HKD', 10, 52.0, 'N');
f_add_change_contract( 'HOC075-10', 'A', 'PROP03', 'C', 'DKSH', 'BB', 'BB', '2010.08.25', '2012.08.24', 'HKD', 5, 40.0, 'N');
f_add_change_contract( 'SQ11-335-01', '', 'PROT12', 'Q', 'VMSC', 'WOCK', 'STRI', '2012.01.23', '2013.01.22', 'HKD', 10, 700.0, 'N');
f_add_change_contract( 'HOC300-10', 'A', 'RANI03', 'C', 'ZUEL', 'GSK', 'GSK', '2011.05.16', '2013.05.15', 'HKD', 5, 70.2, 'N');
f_add_change_contract( 'HOC204-10', 'A', 'REMI01', 'C', 'ZUEL', 'GSK', 'GSKM', '2010.12.14', '2012.12.13', 'HKD', 5, 359.0, 'N');
f_add_change_contract( 'HOC204-10', 'B', 'REMI02', 'C', 'ZUEL', 'GSK', 'GSKM', '2010.12.14', '2012.12.13', 'HKD', 5, 626.0, 'N');
f_add_change_contract( 'HOC018-10', 'A', 'RITU01', 'C', 'DKSH', 'ROCE', 'FHR', '2010.05.20', '2012.05.19', 'HKD', 2, 5780.0, 'N');
f_add_change_contract( 'HOC018-10', 'B1', 'RITU02', 'C', 'DKSH', 'ROCE', 'ROCH', '2011.11.30', '2012.05.19', 'HKD', 1, 14500.0, 'N');
f_add_change_contract( 'SQ11-191-01', '', 'ROPI01', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.12.15', '2012.12.14', 'HKD', 5, 429.0, 'N');
f_add_change_contract( 'SQ11-288-01', '', 'ROPI02', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2012.03.15', '2013.03.14', 'HKD', 5, 463.0, 'N');
f_add_change_contract( 'SQ11-103-01', '', 'SALC01', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.20', '2012.07.19', 'HKD', 5, 311.0, 'N');
f_add_change_contract( 'HOC124-11', 'A', 'SODI1J', 'C', 'DKSH', 'BB', 'BB', '2012.01.24', '2014.01.23', 'HKD', 20, 14.0, 'N');
f_add_change_contract( 'HOC092-11', 'A', 'SOMA02', 'C', 'DKSH', 'MERC', 'MERS', '2011.10.17', '2013.10.16', 'HKD', 1, 75.2, 'N');
f_add_change_contract( 'SQ10-477-01', '', 'SOMA03', 'Q', 'DKSH', 'MERC', 'MESS', '2011.07.15', '2012.07.14', 'HKD', 1, 320.0, 'N');
f_add_change_contract( 'HOC092-11', 'B', 'SOMA11', 'C', 'DKSH', 'MERC', 'MERS', '2011.10.17', '2013.10.16', 'HKD', 1, 188.0, 'N');
f_add_change_contract( 'HOC040-10', 'A', 'STRE04', 'C', 'PRIM', 'CSL', 'CSLB', '2010.10.08', '2012.10.07', 'HKD', 1, 916.3, 'N');
f_add_change_contract( 'HOC254-10', 'A1', 'SULP31', 'C', 'ZUEL', 'PFIZ', 'NIPR', '2012.03.22', '2013.02.10', 'HKD', 10, 330.0, 'N');
f_add_change_contract( 'SQ11-098-01', '', 'SUXA02', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.05', '2012.10.04', 'HKD', 50, 475.0, 'N');
f_add_change_contract( 'SQ11-289-01', '', 'SYNT01', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2012.02.13', '2013.02.12', 'HKD', 100, 1040.0, 'N');
f_add_change_contract( 'SQ11-155-01', '', 'TEIC01', 'Q', 'LFHC', 'SA', 'GL', '2011.12.04', '2012.12.03', 'HKD', 1, 420.0, 'N');
f_add_change_contract( 'HOC012-11', 'A', 'TENE01', 'C', 'ZUEL', 'BOEH', 'BOEH', '2011.05.01', '2014.04.30', 'HKD', 1, 9000.0, 'N');
f_add_change_contract( 'SQ11-171-01', '', 'TERB05', 'Q', 'ZUEL', 'ASTZ', 'CENE', '2011.12.04', '2012.12.03', 'HKD', 5, 92.0, 'N');
f_add_change_contract( 'HOC243-10', 'A', 'TERL01', 'C', 'FERR', 'FERR', 'FERR', '2010.12.16', '2012.12.15', 'HKD', 1, 332.0, 'N');
f_add_change_contract( 'SQ10-460-01', '', 'TEST07', 'Q', 'ZUEL', 'BAYR', 'BSPG', '2011.07.20', '2012.07.19', 'HKD', 3, 290.0, 'N');
f_add_change_contract( 'DQ11-115-01', '', 'TIGE01', 'C', 'LCH', 'CSL', 'CSL', '2012.03.30', '2012.04.12', 'HKD', 1, 9200.0, 'N');
f_add_change_contract( 'HOC225-10', 'A1', 'TIME01', 'C', 'ZUEL', 'GSK', 'SB', '2011.01.03', '2013.01.02', 'HKD', 4, 218.0, 'N');
f_add_change_contract( 'HOC168-10', 'A1', 'TINZ01', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 10, 3003.0, 'N');
f_add_change_contract( 'HOC168-10', 'A', 'TINZ01', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 210, 60060.0, 'N');
f_add_change_contract( 'HOC168-10', 'B1', 'TINZ02', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 10, 1650.0, 'N');
f_add_change_contract( 'HOC168-10', 'B', 'TINZ02', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 210, 33000.0, 'N');
f_add_change_contract( 'HOC228-10', 'A', 'TOPO01', 'C', 'ZUEL', 'GSK', 'GSK', '2011.01.07', '2013.01.06', 'HKD', 1, 1750.0, 'N');
f_add_change_contract( 'SQ11-566-01', '', 'TRAM02', 'Q', 'HKMS', 'STAD', 'BBM', '2012.03.21', '2012.09.20', 'HKD', 5, 14.42, 'N');
f_add_change_contract( 'HOC029-10', 'A', 'TRAN02', 'C', 'HKMS', 'DAIS', 'DAIS', '2010.06.09', '2012.09.22', 'HKD', 50, 344.0, 'N');
f_add_change_contract( 'HOC073-10', 'A', 'TRAN03', 'C', 'HKMS', 'DAIS', 'DAIS', '2010.09.23', '2012.09.22', 'HKD', 50, 733.0, 'N');
f_add_change_contract( 'HOC174-11', 'A', 'TRAS01', 'C', 'DKSH', 'ROCE', 'GENE', '2011.11.28', '2013.11.27', 'HKD', 1, 15900.0, 'N');
f_add_change_contract( 'HOC174-11', 'B', 'TRAS02', 'C', 'DKSH', 'ROCE', 'FHR', '2011.11.28', '2013.11.27', 'HKD', 1, 5500.0, 'N');
f_add_change_contract( 'HOC113-11', 'A', 'TRIP02', 'C', 'FERR', 'FERR', 'FERR', '2011.10.10', '2013.10.09', 'HKD', 11, 10200.0, 'N');
f_add_change_contract( 'HOC032-11', 'A', 'TROP09', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.09.01', '2013.08.31', 'HKD', 34, 2000.0, 'N');
f_add_change_contract( 'SQ11-226-01', '', 'TUBE94', 'Q', 'MEKI', 'XXX', 'STAT', '2012.03.06', '2013.03.05', 'HKD', 1, 79.0, 'N');
f_add_change_contract( 'SQ11-227-01', '', 'TUBE95', 'Q', 'MEKI', 'XXX', 'STAT', '2012.05.07', '2013.05.06', 'HKD', 10, 2190.0, 'N');
f_add_change_contract( 'HOC253-10', 'A', 'UNAS01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.03.11', '2013.03.10', 'HKD', 1, 11.8, 'N');
f_add_change_contract( 'HOC247-10', 'A1', 'VALP04', 'C', 'LFHC', 'SA', 'GL', '2011.07.01', '2013.01.19', 'HKD', 1, 191.4, 'N');
f_add_change_contract( 'HOC285-10', 'A', 'VANC03', 'C', 'ZUEL', 'HOSP', 'HOSP', '2011.03.16', '2013.03.15', 'HKD', 10, 135.0, 'N');
f_add_change_contract( 'SQ10-461-01', '', 'VASO01', 'Q', 'ZUEL', 'PFIZ', 'UB', '2011.07.20', '2012.07.19', 'HKD', 10, 2300.0, 'N');
f_add_change_contract( 'SQ11-326-01', '', 'VERA03', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2012.01.16', '2013.01.15', 'HKD', 25, 1050.0, 'N');
f_add_change_contract( 'SQ11-140-01', '', 'VINB01', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2011.11.04', '2012.11.03', 'HKD', 5, 2600.0, 'N');
f_add_change_contract( 'HOC227-10', 'A1', 'VINO01', 'C', 'ZUEL', 'PFMP', 'PFOL', '2012.04.02', '2013.01.24', 'HKD', 10, 1800.0, 'N');
f_add_change_contract( 'HOC227-10', 'B1', 'VINO02', 'C', 'ZUEL', 'PFMP', 'PFOL', '2012.04.02', '2013.01.24', 'HKD', 10, 8960.0, 'N');
f_add_change_contract( 'SQ11-049-01', '', 'VITA15', 'Q', 'ZUEL', 'FK', 'FK', '2011.10.14', '2012.10.13', 'HKD', 10, 400.0, 'N');
f_add_change_contract( 'SQ10-487-01', '', 'VITA20', 'Q', 'ZUEL', 'FK', 'FK', '2011.08.23', '2012.08.22', 'HKD', 10, 360.0, 'N');
f_add_change_contract( 'HOC109-10', 'A', 'ZUCL07', 'C', 'DKSH', 'LUND', 'LUND', '2010.10.01', '2012.09.30', 'HKD', 10, 555.0, 'N');
f_add_change_contract( 'SQ10-394-01', '', 'ISOF01', 'Q', 'BAXT', 'BAXT', 'BAXT', '2011.06.26', '2012.06.25', 'HKD', 6, 1029.6, 'N');
f_add_change_contract( 'HOC003-10', 'A2', 'SEVO01', 'C', 'ZUEL', 'ABBO', 'AESI', '2011.12.01', '2012.06.20', 'HKD', 1, 1352.0, 'N');
f_add_change_contract( 'HOC187-11', 'A', 'TIOT01', 'C', 'ZUEL', 'BOEH', 'BOEH', '2012.02.05', '2014.02.04', 'HKD', 30, 315.0, 'N');
f_add_change_contract( 'HOC225-11', 'AB', 'PDF 03', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 40.8, 'N');
f_add_change_contract( 'HOC225-11', 'AC', 'PDF 04', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 96.07, 'N');
f_add_change_contract( 'HOC225-11', 'AA', 'PDF 05', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 30.3, 'N');
f_add_change_contract( 'HOC225-11', 'BA', 'PDF 09', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 41.3, 'N');
f_add_change_contract( 'HOC225-11', 'C', 'PDF 11', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 46.74, 'N');
f_add_change_contract( 'HOC225-11', 'DA', 'PDF 16', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 40.8, 'N');
f_add_change_contract( 'HOC225-11', 'BB', 'PDF 18', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 97.28, 'N');
f_add_change_contract( 'HOC225-11', 'EA', 'PDF 19', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 41.3, 'N');
f_add_change_contract( 'HOC008-10', 'A2', 'PDF 1A', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.07.20', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'A3', 'PDF 1A', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'B2', 'PDF 1B', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.07.20', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'B3', 'PDF 1B', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'G', 'PDF 1J', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'H', 'PDF 1K', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'I', 'PDF 1L', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'J', 'PDF 1O', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'K', 'PDF 1P', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'L', 'PDF 1Q', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.05.11', '2012.05.10', 'HKD', 1, 44.1, 'N');
f_add_change_contract( 'HOC008-10', 'A2', 'PDF 1V', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC008-10', 'B2', 'PDF 1W', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC008-10', 'C1', 'PDF 1X', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC008-10', 'E3', 'PDF 1Y', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'F1', 'PDF 1Z', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC225-11', 'F', 'PDF 20', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 46.74, 'N');
f_add_change_contract( 'HOC008-10', 'E2', 'PDF 2A', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC008-10', 'D3', 'PDF 2B', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 50.5, 'N');
f_add_change_contract( 'HOC008-10', 'D2', 'PDF 2C', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.03', '2012.05.10', 'HKD', 1, 39.1, 'N');
f_add_change_contract( 'HOC225-11', 'GA', 'PDF 34', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 41.07, 'N');
f_add_change_contract( 'HOC225-11', 'HA', 'PDF 42', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 42.24, 'N');
f_add_change_contract( 'HOC225-11', 'I', 'PDF 47', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 46.39, 'N');
f_add_change_contract( 'HOC225-11', 'JA', 'PDF 48', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 41.07, 'N');
f_add_change_contract( 'HOC225-11', 'KA', 'PDF 51', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 42.24, 'N');
f_add_change_contract( 'HOC225-11', 'LA', 'PDF 52', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 46.39, 'N');
f_add_change_contract( 'HOC225-11', 'DB', 'PDF 57', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 96.07, 'N');
f_add_change_contract( 'HOC225-11', 'EB', 'PDF 58', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 97.28, 'N');
f_add_change_contract( 'HOC225-11', 'GB', 'PDF 82', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 48.31, 'N');
f_add_change_contract( 'HOC225-11', 'HB', 'PDF 83', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 49.69, 'N');
f_add_change_contract( 'HOC225-11', 'JB', 'PDF 85', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 48.31, 'N');
f_add_change_contract( 'HOC225-11', 'KB', 'PDF 86', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 49.69, 'N');
f_add_change_contract( 'HOC225-11', 'LB', 'PDF 87', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 54.57, 'N');
f_add_change_contract( 'HOC225-11', 'AD', 'PDF 88', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 105.18, 'N');
f_add_change_contract( 'HOC225-11', 'BC', 'PDF 89', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 106.52, 'N');
f_add_change_contract( 'HOC225-11', 'DC', 'PDF 90', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 105.18, 'N');
f_add_change_contract( 'HOC225-11', 'EC', 'PDF 91', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2013.02.10', 'HKD', 1, 106.52, 'N');
f_add_change_contract( 'HOC225-11', 'M', 'PDF 92', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2015.02.10', 'HKD', 1, 72.0, 'N');
f_add_change_contract( 'HOC225-11', 'N', 'PDF 94', 'C', 'KFS', 'BAXT', 'BAXT', '2012.02.11', '2015.02.10', 'HKD', 1, 85.0, 'N');
f_add_change_contract( 'HOC024-10', 'D', 'CHLO1B', 'C', 'BAXT', 'BAXT', 'BAXT', '2010.07.29', '2012.07.28', 'HKD', 15, 180.0, 'N');
f_add_change_contract( 'HOC024-10', 'A', 'CHLO1J', 'C', 'BAXT', 'BAXT', 'BAXT', '2010.07.29', '2012.07.28', 'HKD', 15, 180.0, 'N');
f_add_change_contract( 'HOC024-10', 'B', 'CHLO2U', 'C', 'BAXT', 'BAXT', 'BAXT', '2010.07.29', '2012.07.28', 'HKD', 10, 126.0, 'N');
f_add_change_contract( 'HOC024-10', 'C', 'CHLO68', 'C', 'BAXT', 'BAXT', 'BAXT', '2010.07.29', '2012.07.28', 'HKD', 10, 126.0, 'N');
f_add_change_contract( 'HOC024-10', 'E', 'CHLO69', 'C', 'BAXT', 'BAXT', 'BAXT', '2010.07.29', '2012.07.28', 'HKD', 10, 126.0, 'N');
f_add_change_contract( 'SQ11-186-01', '', 'GLYC13', 'Q', 'BAXT', 'BAXT', 'BAXT', '2011.12.19', '2012.12.18', 'HKD', 6, 120.3, 'N');
f_add_change_contract( 'HOC217-10', 'B', 'SODI2D', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.03.01', '2013.02.28', 'HKD', 4, 101.6, 'N');
f_add_change_contract( 'HOC217-10', 'A', 'SODI78', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.02.20', '2013.02.19', 'HKD', 10, 77.5, 'N');
f_add_change_contract( 'HOC077-11', 'A1', 'AZAC02', 'C', 'JML', 'PHA', 'BO', '2011.10.01', '2013.08.29', 'HKD', 1, 3328.0, 'N');
f_add_change_contract( 'HOC107-11', 'A', 'ETAN01', 'C', 'ZUEL', 'WYET', 'BOEH', '2011.10.13', '2013.10.12', 'HKD', 24, 24250.0, 'N');
f_add_change_contract( 'HOC208-11', 'A', 'GOLI02', 'C', 'DKSH', 'JC', 'BAXP', '2012.01.26', '2014.01.25', 'HKD', 1, 7000.0, 'N');
f_add_change_contract( 'HOC067-11', 'A', 'INSU02', 'C', 'DKSH', 'NOVO', 'NOVO', '2011.09.28', '2013.09.27', 'HKD', 1, 88.0, 'N');
f_add_change_contract( 'HOC276-10', 'C', 'INSU03', 'C', 'YCW', 'EL', 'LPFD', '2011.01.20', '2013.01.19', 'HKD', 1, 85.5, 'N');
f_add_change_contract( 'HOC067-11', 'B', 'INSU06', 'C', 'DKSH', 'NOVO', 'NOVO', '2011.09.28', '2013.09.27', 'HKD', 1, 88.0, 'N');
f_add_change_contract( 'HOC067-11', 'C', 'INSU15', 'C', 'DKSH', 'NOVO', 'NOVO', '2011.09.28', '2013.09.27', 'HKD', 1, 88.0, 'N');
f_add_change_contract( 'HOC276-10', 'B', 'INSU16', 'C', 'YCW', 'EL', 'LPFD', '2011.01.20', '2013.01.19', 'HKD', 1, 85.5, 'N');
f_add_change_contract( 'HOC276-10', 'D', 'INSU38', 'C', 'YCW', 'EL', 'LPFD', '2011.01.20', '2013.01.19', 'HKD', 1, 162.0, 'N');
f_add_change_contract( 'HOC276-10', 'A', 'INSU40', 'C', 'YCW', 'EL', 'LPFD', '2011.01.20', '2013.01.19', 'HKD', 1, 85.5, 'N');
f_add_change_contract( 'HOC195-11', 'A', 'INSU43', 'C', 'LFHC', 'SA', 'SAAD', '2011.12.15', '2013.12.14', 'HKD', 1, 300.0, 'N');
f_add_change_contract( 'HOC098-11', 'A', 'RANI10', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.08.17', '2012.08.16', 'HKD', 3, 25800.0, 'N');
f_add_change_contract( 'HOC227-11', 'A1', 'RANI11', 'C', 'ZUEL', 'NOVA', 'NOVI', '2012.01.11', '2015.01.10', 'HKD', 1, 12900.0, 'N');
f_add_change_contract( 'HOC227-11', 'A', 'RANI11', 'C', 'ZUEL', 'NOVA', 'NOVI', '2012.01.11', '2015.01.10', 'HKD', 3, 25800.0, 'N');
f_add_change_contract( 'HOC079-11', 'A', 'BCG 05', 'C', 'ZUEL', 'SANF', 'SANF', '2011.11.16', '2013.11.15', 'HKD', 11, 11500.0, 'N');
f_add_change_contract( 'SQ11-201-01', '', 'K Y 01', 'Q', 'LCH', 'ECOL', 'ECOL', '2012.01.12', '2013.01.11', 'HKD', 150, 95.25, 'N');
f_add_change_contract( 'SQ11-009-01', '', 'K Y 02', 'Q', 'LCH', 'ECOL', 'ECOL', '2011.09.27', '2012.09.26', 'HKD', 12, 69.6, 'N');
f_add_change_contract( 'SQ10-379-01', 'S', 'AMOR01', 'Q', 'GALD', 'GALD', 'GALD', '2011.06.16', '2012.06.15', 'HKD', 1, 220.0, 'N');
f_add_change_contract( 'SQ11-409-01', 'S', 'AMOR01', 'Q', 'GALD', 'GALD', 'GALD', '2012.06.16', '2013.06.15', 'HKD', 1, 220.0, 'N');
f_add_change_contract( 'SQ10-379-01', '', 'AMOR01', 'Q', 'GALD', 'GALD', 'GALD', '2011.06.16', '2012.06.15', 'HKD', 11, 2200.0, 'N');
f_add_change_contract( 'SQ11-409-01', '', 'AMOR01', 'Q', 'GALD', 'GALD', 'GALD', '2012.06.16', '2013.06.15', 'HKD', 11, 2200.0, 'N');
f_add_change_contract( 'SQ10-414-01', '', 'DIME07', 'Q', 'HAPL', 'KISS', 'KISS', '2011.07.18', '2012.07.17', 'HKD', 300, 159.0, 'N');
f_add_change_contract( 'HOC117-10', 'A1', 'LACT05', 'C', 'USSC', 'DRUH', 'DRUH', '2012.03.01', '2013.01.05', 'HKD', 1000, 32.8, 'N');
f_add_change_contract( 'SQ10-442-01', '', 'POLY11', 'Q', 'ZUEL', 'SLL', 'SLL', '2011.07.27', '2012.07.26', 'HKD', 1, 52.0, 'N');
f_add_change_contract( 'SQ11-169-01', '', 'MOME03', 'Q', 'DKSH', 'SP', 'SP', '2011.12.19', '2012.12.18', 'HKD', 1, 68.0, 'N');
f_add_change_contract( 'SQ11-050-01', '', 'BENZ39', 'Q', 'ZUEL', 'INOV', 'NA', '2011.10.14', '2012.10.13', 'HKD', 16, 24.5, 'N');
f_add_change_contract( 'HOC060-11', 'A', 'DEQU01', 'C', 'SYNC', 'SYNC', 'SYNC', '2011.08.16', '2013.08.15', 'HKD', 500, 99.8, 'N');
f_add_change_contract( 'HOC266-10', 'A', 'DIPH14', 'C', 'MEDI', 'MEDI', 'MEDI', '2011.02.23', '2013.02.22', 'HKD', 30, 0.5, 'N');
f_add_change_contract( 'HOC218-10', 'A', 'PROM09', 'C', 'JEAN', 'JEAN', 'JEAN', '2011.03.03', '2013.03.02', 'HKD', 60, 4.34, 'N');
f_add_change_contract( 'SQ11-180-01', '', 'MAGN05', 'Q', 'MEDI', 'MEDI', 'MEDI', '2011.12.15', '2012.12.14', 'HKD', 30, 0.92, 'N');
f_add_change_contract( 'HOC154-11', 'A', 'GLIC02', 'C', 'LFHC', 'SERV', 'LLSI', '2012.04.26', '2014.04.25', 'HKD', 3600, 1080.0, 'N');
f_add_change_contract( 'HOC106-11', 'A', 'TRIM12', 'C', 'LFHC', 'SERV', 'LLSI', '2011.11.07', '2013.11.06', 'HKD', 1560, 3300.0, 'N');
f_add_change_contract( 'SQ11-125-01', '', 'CHLO2O', 'Q', 'ELC', 'ELC', 'ELC', '2011.11.10', '2012.11.09', 'HKD', 1, 10.5, 'N');
f_add_change_contract( 'SQ11-139-01', '', 'MUPI02', 'Q', 'ZUEL', 'GSK', 'GO', '2011.11.03', '2012.11.02', 'HKD', 1, 105.0, 'N');
f_add_change_contract( 'SQ11-366-01', '', 'XYLO01', 'Q', 'DKSH', 'NOVA', 'NOVA', '2012.02.24', '2013.02.23', 'HKD', 1, 18.0, 'N');
f_add_change_contract( 'SQ11-325-01', 'S', 'SALB10', 'Q', 'ZUEL', 'GSK', 'GSK', '2012.04.10', '2013.04.09', 'HKD', 20, 51.0, 'N');
f_add_change_contract( 'SQ11-325-01', '', 'SALB10', 'Q', 'ZUEL', 'GSK', 'GSK', '2012.04.10', '2013.04.09', 'HKD', 1160, 1275.0, 'N');
f_add_change_contract( 'SQ11-120-01', 'S', 'GLYC05', 'Q', 'HKMS', 'HV', 'GPB', '2011.11.17', '2012.11.16', 'HKD', 1, 71.48, 'N');
f_add_change_contract( 'SQ11-120-01', '', 'GLYC05', 'Q', 'HKMS', 'HV', 'GPB', '2011.11.17', '2012.11.16', 'HKD', 365, 21444.0, 'N');
f_add_change_contract( 'HOC170-11', 'A', 'BECL09', 'C', 'ZUEL', 'GWCL', 'GWCL', '2012.02.21', '2014.02.20', 'HKD', 1, 17.3, 'N');
f_add_change_contract( 'HOC100-11', 'A', 'BUDE04', 'C', 'HKMS', 'PH&T', 'MIPH', '2011.11.13', '2013.11.12', 'HKD', 1, 24.3, 'N');
f_add_change_contract( 'HOC312-10', 'A', 'BUDE16', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.04.19', '2012.04.18', 'HKD', 1, 30.24, 'N');
f_add_change_contract( 'HOC240-11', 'A', 'BUDE16', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2012.04.19', '2014.04.18', 'HKD', 1, 30.24, 'N');
f_add_change_contract( 'HOC312-10', 'B', 'BUDE17', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.04.19', '2012.04.18', 'HKD', 1, 33.6, 'N');
f_add_change_contract( 'HOC240-11', 'B', 'BUDE17', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2012.04.19', '2014.04.18', 'HKD', 1, 33.6, 'N');
f_add_change_contract( 'SQ11-154-01', '', 'BUSE03', 'Q', 'LFHC', 'SA', 'APL', '2011.12.04', '2012.12.03', 'HKD', 1, 530.0, 'N');
f_add_change_contract( 'HOC141-11', 'A', 'FLUT02', 'C', 'ZUEL', 'GSK', 'GWCL', '2012.01.13', '2014.01.12', 'HKD', 1, 32.0, 'N');
f_add_change_contract( 'HOC223-10', 'A1', 'MOME06', 'C', 'DKSH', 'SP', 'SP', '2012.02.01', '2013.01.11', 'HKD', 1, 65.0, 'N');
f_add_change_contract( 'SQ11-135-01', '', 'BONJ01', 'Q', 'ZUEL', 'RECK', 'RECK', '2011.11.06', '2012.11.05', 'HKD', 1, 10.6, 'N');
f_add_change_contract( 'HOC221-10', 'A1', 'ANUS03', 'C', 'LFHC', 'JJ', 'MCNE', '2011.07.01', '2013.03.22', 'HKD', 1, 25.0, 'N');
f_add_change_contract( 'SQ11-314-01', '', 'BETA07', 'Q', 'DKSH', 'SP', 'SP', '2012.01.16', '2013.01.15', 'HKD', 1, 13.0, 'N');
f_add_change_contract( 'SQ11-380-01', '', 'BETA09', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.02.25', '2013.02.24', 'HKD', 1, 12.5, 'N');
f_add_change_contract( 'SQ11-064-01', '', 'CALC37', 'Q', 'DKSH', 'LEO', 'LEO', '2011.10.20', '2012.10.19', 'HKD', 1, 111.0, 'N');
f_add_change_contract( 'SQ10-429-01', '', 'CALC61', 'Q', 'GALD', 'GALD', 'GALD', '2011.07.18', '2012.07.17', 'HKD', 1, 90.0, 'N');
f_add_change_contract( 'SQ10-324-01', '', 'CLOS01', 'Q', 'ZUEL', 'SN', 'NORD', '2011.04.16', '2012.04.15', 'HKD', 1, 103.0, 'N');
f_add_change_contract( 'SQ11-316-01', '', 'CLOS01', 'Q', 'ZUEL', 'SN', 'NORD', '2012.04.16', '2013.04.15', 'HKD', 1, 108.0, 'N');
f_add_change_contract( 'HOC105-11', 'A', 'DAIV01', 'C', 'DKSH', 'LEO', 'LEO', '2011.10.03', '2013.10.02', 'HKD', 1, 90.0, 'N');
f_add_change_contract( 'SQ11-026-01', '', 'DIFL08', 'Q', 'ZUEL', 'INTM', 'INTM', '2011.07.27', '2012.07.26', 'HKD', 1, 21.0, 'N');
f_add_change_contract( 'SQ11-066-01', '', 'DIPR06', 'Q', 'ELC', 'ELC', 'ELC', '2011.10.07', '2012.10.06', 'HKD', 1, 11.6, 'N');
f_add_change_contract( 'SQ10-415-01', '', 'FAKT01', 'Q', 'ZUEL', 'NYCO', 'NYCO', '2011.07.03', '2012.07.02', 'HKD', 1, 22.3, 'N');
f_add_change_contract( 'SQ10-438-01', '', 'MOME02', 'Q', 'DKSH', 'SP', 'SP', '2011.07.25', '2012.07.24', 'HKD', 1, 31.0, 'N');
f_add_change_contract( 'SQ11-256-01', '', 'MUPI01', 'Q', 'ZUEL', 'GSK', 'GO', '2012.03.01', '2013.02.28', 'HKD', 1, 48.5, 'N');
f_add_change_contract( 'SQ11-170-01', '', 'POVI01', 'Q', 'JML', 'MUND', 'MUND', '2011.12.08', '2012.12.07', 'HKD', 1, 74.5, 'N');
f_add_change_contract( 'SQ11-156-01', '', 'GAST01', 'Q', 'ZUEL', 'BH', 'BERL', '2011.12.19', '2012.12.18', 'HKD', 100, 138.0, 'N');
f_add_change_contract( 'HOC011-10', 'A1', 'LANS03', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.07.01', '2012.06.30', 'HKD', 70, 420.0, 'N');
f_add_change_contract( 'HOC011-10', 'A', 'LANS03', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.07.01', '2012.06.30', 'HKD', 3010, 10500.0, 'N');
f_add_change_contract( 'HOC011-10', 'B1', 'LANS04', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.07.01', '2012.06.30', 'HKD', 70, 441.0, 'N');
f_add_change_contract( 'HOC011-10', 'B', 'LANS04', 'C', 'ZUEL', 'TAKD', 'TAKD', '2010.07.01', '2012.06.30', 'HKD', 6790, 26460.0, 'N');
f_add_change_contract( 'HOC224-11', 'A', 'OLAN03', 'C', 'YCW', 'EL', 'LILS', '2012.01.13', '2012.07.12', 'HKD', 112, 1767.0, 'N');
f_add_change_contract( 'HOC224-11', 'B', 'OLAN04', 'C', 'YCW', 'EL', 'LILS', '2012.01.13', '2012.07.12', 'HKD', 112, 3534.0, 'N');
f_add_change_contract( 'HOC178-10', 'A', 'RISP09', 'C', 'DKSH', 'JC', 'JOL', '2010.10.22', '2012.10.21', 'HKD', 364, 2800.0, 'N');
f_add_change_contract( 'HOC178-10', 'B', 'RISP10', 'C', 'DKSH', 'JC', 'JOL', '2010.10.22', '2012.10.21', 'HKD', 364, 5200.0, 'N');
f_add_change_contract( 'HOC295-10', 'F', 'NICO12', 'C', 'LFHC', 'MCNE', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 7, 180.8, 'N');
f_add_change_contract( 'HOC295-10', 'E', 'NICO13', 'C', 'LFHC', 'MCNE', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 7, 163.6, 'N');
f_add_change_contract( 'HOC295-10', 'D', 'NICO14', 'C', 'LFHC', 'MCNE', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 7, 146.6, 'N');
f_add_change_contract( 'HOC161-11', 'A1', 'RIVA05', 'C', 'ZUEL', 'NOVA', 'LTS', '2011.12.10', '2013.12.09', 'HKD', 30, 495.0, 'N');
f_add_change_contract( 'HOC161-11', 'A', 'RIVA05', 'C', 'ZUEL', 'NOVA', 'LTS', '2011.12.10', '2013.12.09', 'HKD', 330, 4950.0, 'N');
f_add_change_contract( 'HOC161-11', 'B1', 'RIVA06', 'C', 'ZUEL', 'NOVA', 'LTS', '2011.12.10', '2013.12.09', 'HKD', 30, 495.0, 'N');
f_add_change_contract( 'HOC161-11', 'B', 'RIVA06', 'C', 'ZUEL', 'NOVA', 'LTS', '2011.12.10', '2013.12.09', 'HKD', 330, 4950.0, 'N');
f_add_change_contract( 'SQ11-382-01', '', 'DUOF01', 'Q', 'ZUEL', 'SLL', 'SLL', '2012.02.25', '2013.02.24', 'HKD', 1, 51.0, 'N');
f_add_change_contract( 'HOC197-11', 'A', 'INSU46', 'C', 'LFHC', 'SA', 'SAAD', '2012.02.18', '2014.02.17', 'HKD', 5, 600.0, 'N');
f_add_change_contract( 'SQ11-020-01', '', 'PEGI10', 'Q', 'DKSH', 'SP', 'SP', '2011.08.20', '2012.08.19', 'HKD', 2, 1240.0, 'N');
f_add_change_contract( 'HOC156-11', 'A', 'INSU25', 'C', 'DKSH', 'NOVO', 'NOVO', '2012.01.01', '2013.12.31', 'HKD', 5, 139.0, 'N');
f_add_change_contract( 'HOC156-11', 'B', 'INSU26', 'C', 'DKSH', 'NOVO', 'NOVO', '2012.01.01', '2013.12.31', 'HKD', 5, 139.0, 'N');
f_add_change_contract( 'HOC156-11', 'C', 'INSU27', 'C', 'DKSH', 'NOVO', 'NOVO', '2012.01.01', '2013.12.31', 'HKD', 5, 139.0, 'N');
f_add_change_contract( 'HOC276-10', 'E', 'INSU35', 'C', 'YCW', 'EL', 'LILL', '2011.01.20', '2013.01.19', 'HKD', 5, 134.7, 'N');
f_add_change_contract( 'HOC276-10', 'F', 'INSU36', 'C', 'YCW', 'EL', 'LILL', '2011.01.20', '2013.01.19', 'HKD', 5, 134.7, 'N');
f_add_change_contract( 'HOC276-10', 'G', 'INSU37', 'C', 'YCW', 'EL', 'LILL', '2011.01.20', '2013.01.19', 'HKD', 5, 134.7, 'N');
f_add_change_contract( 'HOC276-10', 'H', 'INSU39', 'C', 'YCW', 'EL', 'LILL', '2011.01.20', '2013.01.19', 'HKD', 5, 243.75, 'N');
f_add_change_contract( 'HOC156-11', 'D', 'INSU41', 'C', 'DKSH', 'NOVO', 'NOVO', '2012.01.01', '2013.12.31', 'HKD', 5, 264.0, 'N');
f_add_change_contract( 'SQ11-086-01', '', 'CLOT06', 'Q', 'GEHL', 'BIOL', 'BICH', '2011.10.06', '2012.10.05', 'HKD', 120, 200.0, 'N');
f_add_change_contract( 'HOC157-11', 'A', 'DINO05', 'C', 'ZUEL', 'PFIZ', 'SANI', '2012.02.21', '2014.02.20', 'HKD', 4, 1090.0, 'N');
f_add_change_contract( 'SQ11-306-01', 'S', 'GOSE01', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2012.01.06', '2013.01.05', 'HKD', 1, 1390.0, 'N');
f_add_change_contract( 'SQ11-306-01', '', 'GOSE01', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2012.01.06', '2013.01.05', 'HKD', 5, 5560.0, 'N');
f_add_change_contract( 'SQ11-057-01', 'S', 'GOSE02', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.14', '2012.10.13', 'HKD', 1, 3300.0, 'N');
f_add_change_contract( 'SQ11-057-01', '', 'GOSE02', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.14', '2012.10.13', 'HKD', 6, 16500.0, 'N');
f_add_change_contract( 'SQ11-238-01', '', 'ANTI28', 'Q', 'LCH', 'TALE', 'TALE', '2012.02.11', '2013.02.10', 'HKD', 1, 560.0, 'N');
f_add_change_contract( 'HOC220-10', 'A', 'HEPA39', 'C', 'LCH', 'TALE', 'TALE', '2011.02.25', '2013.02.24', 'HKD', 1, 350.0, 'N');
f_add_change_contract( 'HOC042-11', 'A', 'ADAL01', 'C', 'ZUEL', 'ABBO', 'VPF', '2011.09.26', '2013.09.25', 'HKD', 12, 48300.0, 'N');
f_add_change_contract( 'HOC029-11', 'A', 'DARB01', 'C', 'LFHC', 'KIRI', 'AMGE', '2011.06.24', '2012.12.23', 'HKD', 4, 840.0, 'N');
f_add_change_contract( 'HOC029-11', 'B', 'DARB02', 'C', 'LFHC', 'KIRI', 'AMGE', '2011.06.24', '2012.12.23', 'HKD', 4, 1660.0, 'N');
f_add_change_contract( 'HOC029-11', 'C', 'DARB03', 'C', 'LFHC', 'KIRI', 'AMGE', '2011.06.24', '2012.12.23', 'HKD', 4, 2490.0, 'N');
f_add_change_contract( 'HOC164-10', 'A1', 'ENOX01', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.10.21', 'HKD', 2, 53.7, 'N');
f_add_change_contract( 'HOC164-10', 'B1', 'ENOX02', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.10.21', 'HKD', 2, 78.8, 'N');
f_add_change_contract( 'HOC164-10', 'C1', 'ENOX03', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.10.21', 'HKD', 2, 98.9, 'N');
f_add_change_contract( 'HOC164-10', 'D1', 'ENOX04', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.10.21', 'HKD', 2, 125.8, 'N');
f_add_change_contract( 'HOC165-10', 'C2', 'ERYT13', 'C', 'DKSH', 'CILA', 'CILA', '2012.01.31', '2012.10.19', 'HKD', 30, 5850.0, 'N');
f_add_change_contract( 'HOC165-10', 'B2', 'ERYT14', 'C', 'DKSH', 'CILA', 'CILA', '2011.12.09', '2012.10.19', 'HKD', 30, 3000.0, 'N');
f_add_change_contract( 'HOC165-10', 'A2', 'ERYT18', 'C', 'DKSH', 'CILA', 'CILA', '2012.01.31', '2012.10.19', 'HKD', 6, 510.0, 'N');
f_add_change_contract( 'HOC273-10', 'A', 'ERYT23', 'C', 'DKSH', 'ROCE', 'ROCH', '2011.03.31', '2013.03.30', 'HKD', 6, 876.0, 'N');
f_add_change_contract( 'HOC273-10', 'C', 'ERYT24', 'C', 'DKSH', 'ROCE', 'ROCH', '2011.03.31', '2013.03.30', 'HKD', 6, 1878.0, 'N');
f_add_change_contract( 'HOC273-10', 'B', 'ERYT25', 'C', 'DKSH', 'ROCE', 'ROCH', '2011.03.31', '2013.03.30', 'HKD', 6, 1608.0, 'N');
f_add_change_contract( 'HOC165-10', 'D2', 'ERYT26', 'C', 'DKSH', 'CILA', 'CILA', '2011.12.09', '2012.10.19', 'HKD', 30, 6300.0, 'N');
f_add_change_contract( 'HOC107-11', 'B', 'ETAN04', 'C', 'ZUEL', 'WYET', 'WM', '2011.10.13', '2013.10.12', 'HKD', 24, 24250.0, 'N');
f_add_change_contract( 'HOC107-11', 'C', 'ETAN05', 'C', 'ZUEL', 'WYET', 'WM', '2011.10.13', '2013.10.12', 'HKD', 24, 48500.0, 'N');
f_add_change_contract( 'HOC166-10', 'B', 'FILG02', 'C', 'DKSH', 'ROCE', 'FHR', '2010.12.08', '2012.12.07', 'HKD', 1, 900.0, 'N');
f_add_change_contract( 'HOC093-10', 'A', 'INTE13', 'C', 'DKSH', 'MERC', 'SERO', '2010.10.10', '2012.10.09', 'HKD', 3, 1665.0, 'N');
f_add_change_contract( 'HOC093-10', 'B', 'INTE15', 'C', 'DKSH', 'MERC', 'SERO', '2010.10.10', '2012.10.09', 'HKD', 12, 7416.0, 'N');
f_add_change_contract( 'HOC010-11', 'A', 'PEGI07', 'C', 'DKSH', 'ROCE', 'FHR', '2011.06.30', '2013.06.29', 'HKD', 6, 2960.0, 'N');
f_add_change_contract( 'HOC010-11', 'B', 'PEGI08', 'C', 'DKSH', 'ROCE', 'FHR', '2011.06.30', '2013.06.29', 'HKD', 6, 3700.0, 'N');
f_add_change_contract( 'HOC200-11', 'A', 'HAEM28', 'C', 'GAMB', 'GAMB', 'GAMB', '2012.03.01', '2015.02.28', 'HKD', 10, 650.0, 'N');
f_add_change_contract( 'HOC212-10', 'A', 'HAEM55', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.11.28', '2012.11.27', 'HKD', 16, 1064.0, 'N');
f_add_change_contract( '8011016619', 'E', 'HAEM60', 'C', 'GAMB', 'GAMB', 'GAMB', '2010.08.13', '2022.08.12', 'HKD', 1, 68.0, 'N');
f_add_change_contract( '8011016619', 'F', 'HAEM61', 'C', 'GAMB', 'GAMB', 'GAMB', '2010.08.13', '2022.08.12', 'HKD', 1, 68.0, 'N');
f_add_change_contract( '8011016618', 'D', 'HAEM69', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.13', '2022.08.12', 'HKD', 1, 90.0, 'N');
f_add_change_contract( 'HOC302-10', 'A', 'AMIN21', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.04.05', '2013.04.04', 'HKD', 21, 1078.0, 'N');
f_add_change_contract( 'HOC115-10', 'A1', 'POLY07', 'C', 'LFHC', 'SA', 'SW', '2011.07.01', '2012.11.13', 'HKD', 300, 280.0, 'N');
f_add_change_contract( 'HOC115-10', 'B1', 'POLY08', 'C', 'LFHC', 'SA', 'SW', '2011.07.01', '2012.11.13', 'HKD', 454, 500.0, 'N');
f_add_change_contract( 'SQ11-046-01', '', 'MEBE05', 'Q', 'ZUEL', 'ABBO', 'ABBH', '2011.10.14', '2012.10.13', 'HKD', 30, 120.0, 'N');
f_add_change_contract( 'HOC099-11', 'A', 'TACR12', 'C', 'DKSH', 'ASTE', 'ASTE', '2011.08.18', '2012.08.17', 'HKD', 50, 796.0, 'N');
f_add_change_contract( 'HOC099-11', 'B', 'TACR13', 'C', 'DKSH', 'ASTE', 'ASTE', '2011.08.18', '2012.08.17', 'HKD', 50, 1314.0, 'N');
f_add_change_contract( 'HOC099-11', 'C', 'TACR14', 'C', 'DKSH', 'ASTE', 'ASTE', '2011.08.18', '2012.08.17', 'HKD', 50, 6566.0, 'N');
f_add_change_contract( 'SQ11-172-01', '', 'ZYRT01', 'Q', 'ZUEL', 'UCB', 'UCB', '2011.12.06', '2012.12.05', 'HKD', 10, 19.8, 'N');
f_add_change_contract( 'SQ10-508-01', '', 'DIAZ10', 'Q', 'PRIM', 'PTAI', 'PTAI', '2011.07.27', '2012.07.26', 'HKD', 5, 88.0, 'N');
f_add_change_contract( 'HOC267-10', 'A', 'METH20', 'C', 'VLL', 'VLL', 'VLL', '2011.03.11', '2013.03.10', 'HKD', 1, 1.5, 'N');
f_add_change_contract( 'SQ11-162-01', '', 'BETA06', 'Q', 'ZUEL', 'GSK', 'ABO', '2011.12.08', '2012.12.07', 'HKD', 1, 99.0, 'N');
f_add_change_contract( 'SQ10-383-01', '', 'FONG01', 'Q', 'ZUEL', 'SLL', 'SLL', '2011.06.10', '2012.06.09', 'HKD', 150, 74.5, 'N');
f_add_change_contract( 'SQ11-417-01', '', 'FONG01', 'Q', 'ZUEL', 'SLL', 'SLL', '2012.06.10', '2013.06.09', 'HKD', 150, 74.5, 'N');
f_add_change_contract( 'SQ11-069-01', '', 'KETO17', 'Q', 'UI', 'DOUG', 'DOUG', '2011.10.07', '2012.10.06', 'HKD', 1, 39.9, 'N');
f_add_change_contract( 'HOC072-11', 'A', 'DICL02', 'C', 'ZUEL', 'APT', 'APT', '2011.10.13', '2012.10.12', 'HKD', 500, 99.0, 'N');
f_add_change_contract( 'HOC097-11', 'A', 'MESA07', 'C', 'FERR', 'FERR', 'FERI', '2012.03.01', '2014.02.28', 'HKD', 100, 415.0, 'N');
f_add_change_contract( 'SQ10-393-01', '', 'DESM08', 'Q', 'FERR', 'FERR', 'CUSZ', '2011.06.02', '2012.06.01', 'HKD', 30, 340.0, 'N');
f_add_change_contract( 'HOC098-10', 'A', 'CYCL19', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.12.19', '2012.12.18', 'HKD', 2, 1360.0, 'N');
f_add_change_contract( 'HOC099-10', 'A', 'CYCL30', 'C', 'TIMC', 'TEVA', 'TEVA', '2010.12.19', '2012.12.18', 'HKD', 1, 395.0, 'N');
f_add_change_contract( 'HOC182-11', 'A', 'ITRA02', 'C', 'DKSH', 'JP', 'JP', '2012.01.28', '2014.01.27', 'HKD', 150, 990.0, 'N');
f_add_change_contract( 'SQ11-137-01', '', 'LAMI08', 'Q', 'ZUEL', 'GSK', 'GSK', '2011.11.17', '2012.11.16', 'HKD', 240, 650.0, 'N');
f_add_change_contract( 'SQ11-179-01', '', 'LEVO06', 'Q', 'SINO', 'SIGA', 'SIGA', '2011.12.19', '2012.12.18', 'HKD', 1, 260.8, 'N');
f_add_change_contract( 'HOC246-10', 'B2', 'VALP03', 'C', 'LFHC', 'SA', 'ULM', '2012.03.22', '2013.01.19', 'HKD', 300, 38.89, 'N');
f_add_change_contract( 'SQ11-337-01', '', 'BALA01', 'Q', 'DKSH', 'AHKL', 'AHKL', '2012.01.06', '2013.01.05', 'HKD', 36, 1296.0, 'N');
f_add_change_contract( 'HOC134-10', 'A', 'BALA03', 'C', 'DKSH', 'AHKL', 'AHKL', '2010.12.04', '2012.12.03', 'HKD', 1, 40.0, 'N');
f_add_change_contract( 'HOC134-10', 'B', 'BALA05', 'C', 'DKSH', 'AHKL', 'AHKL', '2010.12.04', '2012.12.03', 'HKD', 1, 125.0, 'N');
f_add_change_contract( 'HOC134-10', 'C', 'BALA06', 'C', 'DKSH', 'AHKL', 'AHKL', '2010.12.04', '2012.12.03', 'HKD', 1, 155.0, 'N');
f_add_change_contract( 'SQ11-338-01', '', 'BERI02', 'Q', 'PRIM', 'CSL', 'CSLB', '2012.01.06', '2013.01.05', 'HKD', 1, 2040.0, 'N');
f_add_change_contract( 'SQ10-512-01', '', 'BERI03', 'Q', 'PRIM', 'CSL', 'CSLB', '2011.07.29', '2012.07.28', 'HKD', 1, 900.0, 'N');
f_add_change_contract( 'SQ11-241-01', '', 'HAEM14', 'Q', 'ASS', 'GBIC', 'GBIC', '2012.02.19', '2013.02.18', 'HKD', 2, 330.0, 'N');
f_add_change_contract( 'SQ11-242-01', '', 'HAEM24', 'Q', 'UBL', 'BB', 'BB', '2012.02.10', '2013.02.09', 'HKD', 2, 146.0, 'N');
f_add_change_contract( 'SQ11-343-01', '', 'HAEM29', 'Q', 'UBL', 'BB', 'BB', '2012.01.06', '2013.01.05', 'HKD', 2, 156.0, 'N');
f_add_change_contract( 'HOC231-10', 'A', 'HAEM32', 'C', 'GAMB', 'GD', 'GDS', '2011.02.26', '2013.02.25', 'HKD', 1, 163.5, 'N');
f_add_change_contract( 'HOC011-11', 'A', 'HAEM44', 'C', 'FMCL', 'FMCL', 'FMCL', '2011.05.01', '2013.04.30', 'HKD', 2, 76.0, 'N');
f_add_change_contract( 'HOC011-11', 'B', 'HAEM45', 'C', 'FMCL', 'FMCL', 'FMCL', '2011.05.01', '2013.04.30', 'HKD', 2, 76.0, 'N');
f_add_change_contract( '8011016619', 'A', 'HAEM62', 'C', 'GAMB', 'GAMB', 'GDS', '2010.08.13', '2022.08.12', 'HKD', 1, 95.0, 'N');
f_add_change_contract( '8011016619', 'B', 'HAEM63', 'C', 'GAMB', 'GAMB', 'GDS', '2010.08.13', '2022.08.12', 'HKD', 1, 95.0, 'N');
f_add_change_contract( '8011016619', 'C', 'HAEM64', 'C', 'GAMB', 'GAMB', 'GDS', '2010.08.13', '2022.08.12', 'HKD', 1, 95.0, 'N');
f_add_change_contract( '8011016619', 'D', 'HAEM65', 'C', 'GAMB', 'GAMB', 'GDS', '2010.08.13', '2022.08.12', 'HKD', 1, 95.0, 'N');
f_add_change_contract( '8011016618', 'A', 'HAEM66', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.13', '2022.08.12', 'HKD', 1, 43.0, 'N');
f_add_change_contract( '8011016618', 'B', 'HAEM67', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.13', '2022.08.12', 'HKD', 1, 43.0, 'N');
f_add_change_contract( '8011016618', 'C', 'HAEM68', 'C', 'FMCL', 'FMCL', 'FMCL', '2010.08.13', '2022.08.12', 'HKD', 1, 43.0, 'N');
f_add_change_contract( 'SQ11-068-01', '', 'HYDR27', 'Q', 'HPCL', 'TC', 'TC', '2011.10.07', '2012.10.06', 'HKD', 450, 7.5, 'N');
f_add_change_contract( 'HOC056-11', 'A', 'ORTH02', 'C', 'JJ', 'JJ', 'SYST', '2011.07.23', '2013.07.22', 'HKD', 20, 3936.4, 'N');
f_add_change_contract( 'SQ11-434-01', '', 'POVI05', 'Q', 'JML', 'MUND', 'MUND', '2012.06.12', '2012.12.11', 'HKD', 500, 24.0, 'N');
f_add_change_contract( 'SQ11-200-01', '', 'GLUC36', 'Q', 'YKCL', 'PASC', 'PASC', '2012.01.17', '2013.01.16', 'HKD', 100, 91.0, 'N');
f_add_change_contract( 'HOC052-11', 'A', 'POLY19', 'C', 'TMD', 'HBPL', 'HBPL', '2011.09.29', '2013.09.28', 'HKD', 4, 42.0, 'N');
f_add_change_contract( 'SQ11-166-01', '', 'LIGN10', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.12.19', '2012.12.18', 'HKD', 1, 148.0, 'N');
f_add_change_contract( 'HOC128-10', 'A1', 'DILT07', 'C', 'PRIM', 'MTB', 'MTPF', '2012.01.31', '2012.10.03', 'HKD', 100, 117.0, 'N');
f_add_change_contract( 'HOC128-10', 'B1', 'DILT08', 'C', 'PRIM', 'MTB', 'MTPF', '2012.01.31', '2012.10.03', 'HKD', 100, 214.0, 'N');
f_add_change_contract( 'HOC147-11', 'A', 'NADR01', 'C', 'ZUEL', 'GSK', 'GWCP', '2011.12.30', '2013.12.29', 'HKD', 10, 476.0, 'N');
f_add_change_contract( 'HOC147-11', 'B', 'NADR02', 'C', 'ZUEL', 'GSK', 'GWCP', '2011.12.30', '2013.12.29', 'HKD', 10, 368.0, 'N');
f_add_change_contract( 'HOC147-11', 'C', 'NADR03', 'C', 'ZUEL', 'GSK', 'GWCP', '2011.12.30', '2013.12.29', 'HKD', 10, 328.0, 'N');
f_add_change_contract( 'HOC168-10', 'C1', 'TINZ03', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 2, 173.5, 'N');
f_add_change_contract( 'HOC168-10', 'C', 'TINZ03', 'C', 'DKSH', 'LEO', 'LEO', '2010.11.17', '2012.11.16', 'HKD', 62, 5205.0, 'N');
f_add_change_contract( 'HOC168-10', 'D1', 'TINZ04', 'C', 'DKSH', 'LEO', 'LEO', '2011.01.30', '2013.01.29', 'HKD', 2, 221.0, 'N');
f_add_change_contract( 'HOC168-10', 'D', 'TINZ04', 'C', 'DKSH', 'LEO', 'LEO', '2011.01.30', '2013.01.29', 'HKD', 62, 6630.0, 'N');
f_add_change_contract( 'SQ11-138-01', 'S', 'LEUP02', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.11.03', '2012.11.02', 'HKD', 1, 1600.0, 'N');
f_add_change_contract( 'SQ11-138-01', '', 'LEUP02', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.11.03', '2012.11.02', 'HKD', 4, 4800.0, 'N');
f_add_change_contract( 'SQ11-151-01', '', 'ALFU01', 'Q', 'LFHC', 'SA', 'SW', '2011.08.25', '2012.08.24', 'HKD', 56, 288.0, 'N');
f_add_change_contract( 'HOC165-11', 'A', 'INDA02', 'C', 'LFHC', 'SERV', 'LLSI', '2012.01.01', '2013.12.31', 'HKD', 1000, 350.0, 'N');
f_add_change_contract( 'HOC094-11', 'A', 'ISOS03', 'C', 'ZUEL', 'SCHW', 'SCHW', '2012.01.07', '2014.01.06', 'HKD', 50, 13.9, 'N');
f_add_change_contract( 'HOC133-11', 'A', 'MORP04', 'C', 'JML', 'MUND', 'BARD', '2012.01.08', '2014.01.07', 'HKD', 120, 314.0, 'N');
f_add_change_contract( 'HOC133-11', 'B', 'MORP05', 'C', 'JML', 'MUND', 'BARD', '2012.01.08', '2014.01.07', 'HKD', 120, 852.0, 'N');
f_add_change_contract( 'HOC133-11', 'C', 'MORP08', 'C', 'JML', 'MUND', 'BARD', '2012.01.08', '2014.01.07', 'HKD', 120, 1668.0, 'N');
f_add_change_contract( 'HOC186-11', 'A', 'NIFE03', 'C', 'ZUEL', 'BH', 'BH', '2012.02.17', '2014.02.16', 'HKD', 140, 17.1, 'N');
f_add_change_contract( 'HOC274-10', 'A', 'POTA02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.04.29', '2012.04.28', 'HKD', 2000, 563.2, 'N');
f_add_change_contract( 'HOC194-11', 'A', 'POTA02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2012.04.29', '2014.04.28', 'HKD', 2000, 563.2, 'N');
f_add_change_contract( 'HOC163-11', 'A', 'TERB02', 'C', 'ZUEL', 'APT', 'APT', '2012.03.07', '2014.03.06', 'HKD', 500, 114.0, 'N');
f_add_change_contract( 'HOC164-11', 'A', 'TERB03', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2012.03.07', '2014.03.06', 'HKD', 100, 37.7, 'N');
f_add_change_contract( 'HOC084-11', 'A', 'THEO17', 'C', 'ZUEL', 'INOV', 'INOV', '2011.09.01', '2013.08.31', 'HKD', 500, 125.49, 'N');
f_add_change_contract( 'HOC084-11', 'B', 'THEO18', 'C', 'ZUEL', 'INOV', 'INOV', '2011.09.01', '2013.08.31', 'HKD', 500, 178.58, 'N');
f_add_change_contract( 'HOC086-11', 'A', 'GLYC04', 'C', 'ZUEL', 'GSK', 'GSA', '2011.11.17', '2013.11.16', 'HKD', 100, 17.8, 'N');
f_add_change_contract( 'HOC026-11', 'A', 'ANUS04', 'C', 'LCH', 'MARI', 'MARI', '2011.08.15', '2013.08.14', 'HKD', 12, 9.2, 'N');
f_add_change_contract( 'HOC026-11', 'A', 'BISM09', 'C', 'LCH', 'MARI', 'MARI', '2011.08.15', '2013.08.14', 'HKD', 12, 9.2, 'N');
f_add_change_contract( 'HOC272-10', 'A', 'FAKT02', 'C', 'ZUEL', 'NYCO', 'NYCO', '2011.05.13', '2013.05.12', 'HKD', 120, 245.0, 'N');
f_add_change_contract( 'SQ11-114-01', '', 'MESA05', 'Q', 'JML', 'FALK', 'FALK', '2011.11.03', '2012.11.02', 'HKD', 30, 310.0, 'N');
f_add_change_contract( 'SQ11-181-01', '', 'MESA08', 'Q', 'FERR', 'FERR', 'PHAB', '2011.12.19', '2012.12.18', 'HKD', 28, 420.0, 'N');
f_add_change_contract( 'SQ11-356-01', '', 'PARA04', 'Q', 'STAM', 'MEDO', 'MEDO', '2012.03.27', '2013.03.26', 'HKD', 100, 89.0, 'N');
f_add_change_contract( 'SQ10-356-01', '', 'ULTR08', 'Q', 'ZUEL', 'INTM', 'IDAS', '2011.05.29', '2012.05.28', 'HKD', 6, 12.4, 'N');
f_add_change_contract( 'SQ11-400-01', '', 'ULTR08', 'Q', 'ZUEL', 'INTM', 'IDAS', '2012.05.29', '2013.05.28', 'HKD', 6, 13.5, 'N');
f_add_change_contract( 'SQ10-372-01', '', 'ACYC06', 'Q', 'ZUEL', 'GSK', 'ABO', '2011.06.10', '2012.06.09', 'HKD', 125, 666.0, 'N');
f_add_change_contract( 'HOC139-11', 'A', 'AZIT02', 'C', 'ZUEL', 'PFIZ', 'HPL', '2012.01.21', '2014.01.20', 'HKD', 15, 25.0, 'N');
f_add_change_contract( 'SQ10-310-01', '', 'CARB49', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.04.24', '2012.04.23', 'HKD', 250, 90.0, 'N');
f_add_change_contract( 'SQ11-331-01', '', 'CARB49', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2012.04.24', '2013.04.23', 'HKD', 250, 95.0, 'N');
f_add_change_contract( 'SQ11-052-01', '', 'CEFU07', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.10.14', '2012.10.13', 'HKD', 50, 55.6, 'N');
f_add_change_contract( 'HOC085-10', 'A', 'CLAR02', 'C', 'ZUEL', 'ABBO', 'ABBO', '2010.12.12', '2012.12.11', 'HKD', 60, 25.0, 'N');
f_add_change_contract( 'HOC241-11', 'A', 'FLUC06', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2012.04.01', '2014.03.31', 'HKD', 35, 258.0, 'N');
f_add_change_contract( 'HOC187-10', 'C', 'LINE14', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.12.07', '2012.06.06', 'HKD', 150, 2045.0, 'N');
f_add_change_contract( 'HOC130-11', 'C', 'OXCA03', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.11.18', '2013.11.17', 'HKD', 250, 233.0, 'N');
f_add_change_contract( 'HOC132-11', 'A', 'PARA18', 'C', 'MEDI', 'MEDI', 'MEDI', '2012.01.23', '2014.01.22', 'HKD', 30, 0.68, 'N');
f_add_change_contract( 'HOC176-10', 'B', 'PHEN33', 'C', 'ZUEL', 'PFIZ', 'PUJL', '2010.10.22', '2012.10.21', 'HKD', 237, 158.0, 'N');
f_add_change_contract( 'SQ10-374-01', '', 'TRIM10', 'Q', 'SINO', 'ORBI', 'ORBI', '2011.06.10', '2012.06.09', 'HKD', 100, 50.5, 'N');
f_add_change_contract( 'SQ11-229-01', '', 'AMPI03', 'Q', 'UI', 'CLON', 'CLON', '2011.09.16', '2013.01.15', 'HKD', 100, 44.0, 'N');
f_add_change_contract( 'HOC151-11', 'A', 'AUGM02', 'C', 'ZUEL', 'LEK', 'LEK', '2012.01.09', '2014.01.08', 'HKD', 100, 12.94, 'N');
f_add_change_contract( 'DQ11-074-01', '', 'AUGM06', 'C', 'ZUEL', 'SAND', 'SAND', '2012.03.14', '2012.06.13', 'HKD', 35, 16.5, 'N');
f_add_change_contract( 'SQ11-296-01', '', 'FERR29', 'Q', 'HKMS', 'VIFO', 'VIFO', '2012.01.06', '2013.01.05', 'CHF', 150, 5.7, 'N');
f_add_change_contract( 'SQ11-128-01', '', 'FUSI02', 'Q', 'DKSH', 'LEO', 'LEO', '2011.11.08', '2012.11.07', 'HKD', 90, 594.0, 'N');
f_add_change_contract( 'SQ11-131-01', '', 'METR02', 'Q', 'LFHC', 'SWI', 'SWI', '2011.11.03', '2012.11.02', 'HKD', 100, 115.0, 'N');
f_add_change_contract( 'SQ11-093-01', 'A1', 'MORP26', 'Q', 'LFAU', 'UPL', 'UPL', '2011.10.20', '2012.10.19', 'HKD', 140, 20.4, 'N');
f_add_change_contract( 'SQ11-093-01', 'A2', 'MORP26', 'Q', 'LFAU', 'UPL', 'UPL', '2011.10.20', '2012.10.19', 'HKD', 450, 56.0, 'N');
f_add_change_contract( 'SQ11-094-01', '', 'MORP29', 'Q', 'LFAU', 'UPL', 'UPL', '2011.10.05', '2012.10.04', 'HKD', 450, 282.0, 'N');
f_add_change_contract( 'SQ11-095-01', '', 'MORP37', 'Q', 'LFAU', 'UPL', 'UPL', '2011.10.13', '2012.10.12', 'HKD', 450, 25.8, 'N');
f_add_change_contract( 'SQ11-207-01', '', 'PHEN18', 'Q', 'LFAU', 'ATHL', 'ATHL', '2011.11.04', '2012.05.03', 'HKD', 600, 90.0, 'N');
f_add_change_contract( 'SQ11-245-01', '', 'RANI05', 'Q', 'ZUEL', 'GSK', 'ABO', '2012.03.16', '2013.03.15', 'HKD', 200, 241.1, 'N');
f_add_change_contract( 'SQ11-132-01', '', 'RIFA03', 'Q', 'LFHC', 'SA', 'GLS', '2011.11.03', '2012.11.02', 'HKD', 60, 44.0, 'N');
f_add_change_contract( 'HOC058-11', 'A', 'ABAC01', 'C', 'ZUEL', 'GSK', 'GO', '2011.07.09', '2012.07.08', 'HKD', 60, 2440.0, 'N');
f_add_change_contract( 'SQ11-109-01', '', 'ACYC01', 'Q', 'HKMS', 'STAD', 'STAD', '2011.07.15', '2012.07.14', 'EUR', 25, 1.84, 'N');
f_add_change_contract( 'SQ11-110-01', '', 'ACYC07', 'Q', 'HKMS', 'STAD', 'STAD', '2011.07.15', '2012.07.14', 'EUR', 35, 3.3, 'N');
f_add_change_contract( 'SQ11-111-01', '', 'ACYC08', 'Q', 'HKMS', 'STAD', 'STAD', '2011.07.15', '2012.07.14', 'EUR', 35, 4.09, 'N');
f_add_change_contract( 'HOC045-10', 'A2', 'ADEF01', 'C', 'ZUEL', 'GSK', 'GSKT', '2011.09.01', '2012.07.21', 'HKD', 30, 1360.0, 'N');
f_add_change_contract( 'HOC057-09', 'A', 'ALEN02', 'C', 'DKSH', 'MSD', 'MSD', '2009.07.22', '2012.08.21', 'HKD', 4, 65.0, 'N');
f_add_change_contract( 'HOC039-11', 'A', 'ALLO01', 'C', 'TIMC', 'TEVA', 'TPW', '2011.08.10', '2013.08.09', 'HKD', 100, 11.66, 'N');
f_add_change_contract( 'HOC128-11', 'A', 'ALPR01', 'C', 'VICK', 'VICK', 'VICK', '2012.01.01', '2013.12.31', 'HKD', 500, 61.5, 'N');
f_add_change_contract( 'HOC128-11', 'B', 'ALPR02', 'C', 'VICK', 'VICK', 'VICK', '2012.01.01', '2013.12.31', 'HKD', 500, 86.5, 'N');
f_add_change_contract( 'HOC201-11', 'A', 'ALUM02', 'C', 'ZUEL', 'INOV', 'INOV', '2012.03.14', '2014.03.13', 'HKD', 100, 130.0, 'N');
f_add_change_contract( 'HOC059-10', 'A', 'ALUM19', 'C', 'JEAN', 'JEAN', 'JEAN', '2011.01.01', '2012.12.31', 'HKD', 500, 42.8, 'N');
f_add_change_contract( 'SQ11-153-01', '', 'AMAN01', 'Q', 'JML', 'MERZ', 'MERZ', '2011.11.03', '2012.11.02', 'HKD', 100, 448.0, 'N');
f_add_change_contract( 'SQ11-023-01', '', 'AMIL01', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.07.18', '2012.07.17', 'HKD', 500, 149.5, 'N');
f_add_change_contract( 'HOC328-10', 'B1', 'AMIS02', 'C', 'LFHC', 'SA', 'SW', '2011.07.01', '2013.04.10', 'HKD', 30, 177.0, 'N');
f_add_change_contract( 'HOC328-10', 'A1', 'AMIS03', 'C', 'LFHC', 'SA', 'SW', '2011.07.01', '2013.04.10', 'HKD', 30, 124.0, 'N');


commit;

end;
/



set scan on
set serveroutput on

