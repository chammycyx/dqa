set scan off
set serveroutput on

declare

	procedure f_add_change_contract(
		ls_contract_num in varchar2,
		ls_contract_suffix in varchar2,
		ls_item_code in varchar2,
		ls_contract_type in varchar2,
		ls_supplier_code in varchar2,
		ls_pharm_company_code in varchar2,
		ls_manuf_code in varchar2,
		ls_contract_start in varchar2,
		ls_contract_end in varchar2,
		ls_currency_code in varchar2,
		ll_pack_size in number,
		ll_pack_price in number,
		ls_suspend in varchar2)
	is
  		ll_contract_id	number;
  		
	begin
			
		select contract_id
		into   ll_contract_id
		from   contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
	
		update contract
		set    supplier_code = ls_supplier_code,
			   pharm_company_code = ls_pharm_company_code,
			   manuf_code = ls_manuf_code,
			   start_date = to_date(ls_contract_start, 'yyyy.mm.dd'),
			   end_date = to_date(ls_contract_end, 'yyyy.mm.dd'),
			   currency_code = ls_currency_code,
			   pack_size = ll_pack_size,
			   pack_price = ll_pack_price,
			   suspend_flag = ls_suspend,
			   modify_date = sysdate,
			   modify_user = 'dqaadmin'
		where  contract_id = ll_contract_id;
		
	exception when NO_DATA_FOUND then
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
	
	when TOO_MANY_ROWS then
	
		delete from contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
		
		dbms_output.put_line('Duplicate row found! ' || ls_contract_num || ', ' || ls_contract_suffix || ', ' || ls_contract_type || ', ' || ls_item_code);
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
		
	end;
	
begin

f_add_change_contract( 'SQ10-449-01', 'S', 'AMIT01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.07.05', '2012.07.04', 'HKD', 500, 52.0, 'N');
f_add_change_contract( 'SQ10-449-01', '', 'AMIT01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.07.05', '2012.07.04', 'HKD', 3000, 260.0, 'N');
f_add_change_contract( 'SQ11-275-01', '', 'AMIT02', 'Q', 'HW', 'APOT', 'APOT', '2012.03.16', '2013.03.15', 'HKD', 1000, 78.0, 'N');
f_add_change_contract( 'HOC144-11', 'A', 'AMLO01', 'C', 'TBOM', 'PHAA', 'PHAA', '2012.01.01', '2012.12.31', 'HKD', 100, 17.9, 'N');
f_add_change_contract( 'HOC145-11', 'A', 'AMLO02', 'C', 'ZUEL', 'APT', 'APT', '2012.01.01', '2012.12.31', 'HKD', 500, 130.0, 'N');
f_add_change_contract( 'HOC035-10', 'A3', 'ANAS01', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.14', '2012.07.13', 'HKD', 28, 1311.0, 'N');
f_add_change_contract( 'HOC035-10', 'A2', 'ANAS01', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.14', '2012.07.13', 'HKD', 1148, 39330.0, 'N');
f_add_change_contract( 'HOC278-10', 'B1', 'ARIP01', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 30, 714.0, 'N');
f_add_change_contract( 'HOC278-10', 'B', 'ARIP01', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 720, 14280.0, 'N');
f_add_change_contract( 'HOC278-10', 'C1', 'ARIP02', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 30, 714.0, 'N');
f_add_change_contract( 'HOC278-10', 'C', 'ARIP02', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 660, 14280.0, 'N');
f_add_change_contract( 'HOC278-10', 'A1', 'ARIP03', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 30, 357.0, 'N');
f_add_change_contract( 'HOC278-10', 'A', 'ARIP03', 'C', 'ZUEL', 'OTSU', 'OTSU', '2011.03.18', '2013.03.17', 'HKD', 690, 7140.0, 'N');
f_add_change_contract( 'HOC137-11', 'A', 'ASPI01', 'C', 'SYNC', 'SYNC', 'SYNC', '2011.12.15', '2013.12.14', 'HKD', 1000, 79.5, 'N');
f_add_change_contract( 'SQ11-161-01', '', 'ASPI13', 'Q', 'ZUEL', 'RECK', 'RECK', '2011.12.19', '2012.12.18', 'HKD', 30, 7.2, 'N');
f_add_change_contract( 'SQ11-392-01', '', 'ATEN01', 'Q', 'LCH', 'WOCK', 'CPPH', '2012.03.23', '2012.06.22', 'HKD', 28, 2.84, 'N');
f_add_change_contract( 'SQ11-393-01', '', 'ATEN02', 'Q', 'LCH', 'WOCK', 'CPPH', '2012.03.23', '2012.06.22', 'HKD', 28, 2.99, 'N');
f_add_change_contract( 'HOC038-10', 'A', 'ATOR01', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2010.09.01', '2012.08.31', 'HKD', 30, 166.0, 'N');
f_add_change_contract( 'HOC038-10', 'B1', 'ATOR02', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.11.15', '2012.08.31', 'HKD', 630, 5640.0, 'N');
f_add_change_contract( 'HOC038-10', 'C1', 'ATOR03', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.11.15', '2012.08.31', 'HKD', 30, 282.0, 'N');
f_add_change_contract( 'HOC038-10', 'D', 'ATOR04', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2010.09.01', '2012.08.31', 'HKD', 420, 6450.0, 'N');
f_add_change_contract( 'HOC235-10', 'A', 'AUGM01', 'C', 'ZUEL', 'GSK', 'SB', '2010.12.21', '2012.12.20', 'HKD', 100, 71.9, 'N');
f_add_change_contract( 'HOC146-10', 'B', 'AUGM05', 'C', 'ZUEL', 'GSK', 'SB', '2010.12.03', '2012.12.02', 'HKD', 14, 14.0, 'N');
f_add_change_contract( 'HOC166-11', 'A', 'AZAT03', 'C', 'HW', 'APOT', 'RPG', '2012.02.28', '2014.02.27', 'HKD', 100, 98.0, 'N');
f_add_change_contract( 'HOC192-10', 'A1', 'AZAT05', 'C', 'PRIM', 'ASPE', 'EXC', '2012.01.01', '2013.02.18', 'HKD', 100, 333.0, 'N');
f_add_change_contract( 'HOC139-11', 'C', 'AZIT04', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2012.01.21', '2014.01.20', 'HKD', 6, 12.0, 'N');
f_add_change_contract( 'SQ10-380-01', 'S', 'BACL01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.06.16', '2012.06.15', 'HKD', 100, 33.6, 'N');
f_add_change_contract( 'SQ11-410-01', '', 'BACL01', 'Q', 'ZUEL', 'APT', 'APT', '2012.06.16', '2013.06.15', 'HKD', 500, 84.0, 'N');
f_add_change_contract( 'SQ10-380-01', '', 'BACL01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.06.16', '2012.06.15', 'HKD', 2000, 336.0, 'N');
f_add_change_contract( 'SQ10-505-01', '', 'BETA01', 'Q', 'DKSH', 'EISA', 'EISA', '2011.08.31', '2012.08.30', 'HKD', 1000, 148.0, 'N');
f_add_change_contract( 'SQ11-193-01', '', 'BICA02', 'Q', 'ZUEL', 'ASTZ', 'CORD', '2011.11.17', '2012.05.16', 'HKD', 28, 1428.0, 'N');
f_add_change_contract( 'SQ10-117-01', 'A1', 'BICA03', 'Q', 'ZUEL', 'ASTZ', 'CORD', '2011.10.19', '2012.05.18', 'HKD', 28, 3234.0, 'N');
f_add_change_contract( 'HOC211-11', 'A', 'BISO01', 'C', 'ZUEL', 'MERK', 'MERK', '2012.03.10', '2014.03.09', 'HKD', 100, 86.0, 'N');
f_add_change_contract( 'HOC211-11', 'B', 'BISO02', 'C', 'ZUEL', 'MERK', 'MERK', '2012.03.10', '2014.03.09', 'HKD', 30, 25.2, 'N');
f_add_change_contract( 'SQ11-268-01', '', 'BOSE02', 'Q', 'ZUEL', 'ACTE', 'PATH', '2011.11.15', '2012.11.14', 'HKD', 60, 19012.5, 'N');
f_add_change_contract( 'HOC107-10', 'A', 'BROM01', 'C', 'DKSH', 'ROCE', 'ROCE', '2010.09.14', '2012.09.13', 'HKD', 100, 32.7, 'N');
f_add_change_contract( 'HOC107-10', 'B1', 'BROM02', 'C', 'DKSH', 'ROCE', 'ROCM', '2012.02.23', '2012.09.13', 'HKD', 100, 46.8, 'N');
f_add_change_contract( 'HOC046-11', 'A', 'BROM03', 'C', 'SYNC', 'SYNC', 'SYNC', '2011.09.01', '2013.08.30', 'HKD', 1000, 88.0, 'N');
f_add_change_contract( 'HOC298-10', 'A1', 'BROM07', 'C', 'HW', 'APOT', 'APOT', '2011.06.13', '2013.06.12', 'HKD', 100, 47.0, 'N');
f_add_change_contract( 'HOC298-10', 'A', 'BROM07', 'C', 'HW', 'APOT', 'APOT', '2011.06.13', '2013.06.12', 'HKD', 14400, 5687.0, 'N');
f_add_change_contract( 'SQ11-025-01', '', 'BUME01', 'Q', 'DKSH', 'LEO', 'LEO', '2011.07.25', '2012.07.24', 'HKD', 100, 182.0, 'N');
f_add_change_contract( 'SQ11-370-01', '', 'BUSU01', 'Q', 'PRIM', 'GSK', 'EXC', '2012.02.24', '2013.02.23', 'HKD', 100, 978.0, 'N');
f_add_change_contract( 'HOC230-11', 'A', 'CABE01', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2012.02.23', '2014.02.22', 'HKD', 8, 550.0, 'N');
f_add_change_contract( 'SQ10-506-01', '', 'CAFE01', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.08.31', '2012.08.30', 'HKD', 20, 76.0, 'N');
f_add_change_contract( 'HOC305-10', 'A', 'CALC06', 'C', 'HW', 'APOT', 'APOT', '2011.07.08', '2013.07.07', 'HKD', 500, 146.0, 'N');
f_add_change_contract( 'SQ11-042-01', '', 'CALC14', 'Q', 'LCH', 'SHMH', 'SSPC', '2011.09.25', '2012.09.24', 'HKD', 1000, 33.0, 'N');
f_add_change_contract( 'HOC082-10', 'A', 'CALC29', 'C', 'ZUEL', 'WCH', 'WYEP', '2010.12.23', '2012.12.22', 'HKD', 60, 45.0, 'N');
f_add_change_contract( 'HOC306-10', 'A', 'CALC30', 'C', 'ZUEL', 'WCH', 'WYET', '2011.07.08', '2013.07.07', 'HKD', 1020, 212.4, 'N');
f_add_change_contract( 'SQ10-367-01', 'S', 'CAND01', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.05.28', '2012.05.27', 'HKD', 28, 109.2, 'N');
f_add_change_contract( 'SQ11-411-01', 'S', 'CAND01', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2012.05.28', '2013.05.27', 'HKD', 28, 109.2, 'N');
f_add_change_contract( 'SQ10-367-01', '', 'CAND01', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.05.28', '2012.05.27', 'HKD', 448, 1092.0, 'N');
f_add_change_contract( 'SQ11-411-01', '', 'CAND01', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2012.05.28', '2013.05.27', 'HKD', 448, 1092.0, 'N');
f_add_change_contract( 'HOC004-10', 'B1', 'CAPE01', 'C', 'DKSH', 'ROCE', 'PRSC', '2011.04.11', '2012.04.25', 'HKD', 2820, 20800.0, 'N');
f_add_change_contract( 'HOC004-10', 'A1', 'CAPE02', 'C', 'DKSH', 'ROCE', 'PRSC', '2011.04.11', '2012.04.25', 'HKD', 5640, 137760.0, 'N');
f_add_change_contract( 'SQ10-430-01', '', 'CAPT01', 'Q', 'ZUEL', 'APT', 'APT', '2011.09.14', '2012.09.13', 'HKD', 1000, 126.0, 'N');
f_add_change_contract( 'HOC333-10', 'A', 'CARB02', 'C', 'ZUEL', 'NOVA', 'NOVF', '2011.03.24', '2013.03.23', 'HKD', 500, 275.0, 'N');
f_add_change_contract( 'SQ11-272-01', '', 'CARV01', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.11.30', '2012.05.29', 'HKD', 500, 169.5, 'N');
f_add_change_contract( 'SQ11-270-01', '', 'CARV02', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.11.30', '2012.05.29', 'HKD', 500, 68.5, 'N');
f_add_change_contract( 'SQ11-271-01', '', 'CARV03', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.11.30', '2012.05.29', 'HKD', 500, 105.0, 'N');
f_add_change_contract( 'SQ11-269-01', '', 'CARV04', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.11.30', '2012.05.29', 'HKD', 500, 64.0, 'N');
f_add_change_contract( 'HOC147-10', 'A', 'CEFU04', 'C', 'ZUEL', 'GSK', 'GWCL', '2010.11.24', '2012.11.23', 'HKD', 50, 50.0, 'N');
f_add_change_contract( 'SQ11-395-01', '', 'CETI01', 'Q', 'VICK', 'VICK', 'VICK', '2012.04.04', '2013.04.03', 'HKD', 500, 139.0, 'N');
f_add_change_contract( 'SQ11-008-01', '', 'CHAR01', 'Q', 'LCH', 'JLB', 'JLB', '2011.09.23', '2012.09.22', 'HKD', 100, 56.0, 'N');
f_add_change_contract( 'SQ10-351-01', '', 'CHLO04', 'Q', 'PRIM', 'GSK', 'EXC', '2011.05.21', '2012.05.20', 'HKD', 25, 245.0, 'N');
f_add_change_contract( 'SQ11-117-01', '', 'CHLO39', 'Q', 'MEDI', 'MEDI', 'MEDI', '2011.11.17', '2012.11.16', 'HKD', 5000, 58.0, 'N');
f_add_change_contract( 'HOC237-10', 'A', 'CHLO44', 'C', 'ZUEL', 'APT', 'APT', '2011.03.17', '2013.03.16', 'HKD', 500, 119.0, 'N');
f_add_change_contract( 'HOC237-10', 'B', 'CHLO46', 'C', 'ZUEL', 'APT', 'APT', '2011.03.17', '2013.03.16', 'HKD', 500, 180.0, 'N');
f_add_change_contract( 'HOC237-10', 'C', 'CHLO47', 'C', 'ZUEL', 'APT', 'APT', '2011.03.17', '2013.03.16', 'HKD', 500, 275.0, 'N');
f_add_change_contract( 'HOC163-10', 'A', 'CILO01', 'C', 'ZUEL', 'OTSU', 'OTSU', '2010.11.08', '2012.11.07', 'HKD', 100, 258.0, 'N');
f_add_change_contract( 'HOC152-11', 'A', 'CIPR01', 'C', 'TIMC', 'TEVA', 'TPW', '2012.01.27', '2014.01.26', 'HKD', 100, 60.1, 'N');
f_add_change_contract( 'HOC103-10', 'A', 'CITA02', 'C', 'ELC', 'ELC', 'ELC', '2010.11.01', '2012.10.31', 'HKD', 100, 89.0, 'N');
f_add_change_contract( 'HOC184-10', 'A', 'CLAR01', 'C', 'ZUEL', 'APT', 'APT', '2010.12.23', '2012.12.22', 'HKD', 500, 580.0, 'N');
f_add_change_contract( 'SQ10-381-01', '', 'CLAR03', 'Q', 'DKSH', 'SP', 'SP', '2011.06.10', '2012.06.09', 'HKD', 14, 27.3, 'N');
f_add_change_contract( 'SQ11-412-01', '', 'CLAR03', 'Q', 'DKSH', 'SP', 'SP', '2012.06.10', '2013.06.09', 'HKD', 14, 27.3, 'N');
f_add_change_contract( 'HOC184-10', 'B', 'CLAR04', 'C', 'ZUEL', 'APT', 'APT', '2010.12.23', '2012.12.22', 'HKD', 500, 1159.0, 'N');
f_add_change_contract( 'HOC004-11', 'A1', 'CLOB01', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2013.04.30', 'HKD', 100, 160.0, 'N');
f_add_change_contract( 'SQ11-184-01', '', 'CLOM02', 'Q', 'HW', 'XXX', 'APOT', '2011.12.22', '2012.12.21', 'HKD', 100, 40.0, 'N');
f_add_change_contract( 'HOC334-10', 'A', 'CLON01', 'C', 'DKSH', 'ROCE', 'RFSA', '2011.03.17', '2013.03.16', 'HKD', 50, 54.3, 'N');
f_add_change_contract( 'HOC334-10', 'B', 'CLON02', 'C', 'DKSH', 'ROCE', 'RFSA', '2011.03.17', '2013.03.16', 'HKD', 100, 220.2, 'N');
f_add_change_contract( 'SQ11-231-01', '', 'CLOZ01', 'Q', 'TMD', 'GENN', 'SYNT', '2011.12.01', '2012.06.30', 'HKD', 30, 9.45, 'N');
f_add_change_contract( 'SQ11-231-02', '', 'CLOZ02', 'Q', 'TMD', 'SYNT', 'SYNT', '2011.12.01', '2012.06.30', 'HKD', 30, 21.1, 'N');
f_add_change_contract( 'SQ11-249-01', 'S', 'CO-D01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2012.02.10', '2013.02.09', 'HKD', 28, 139.0, 'N');
f_add_change_contract( 'SQ11-249-01', '', 'CO-D01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2012.02.10', '2013.02.09', 'HKD', 2044, 4865.0, 'N');
f_add_change_contract( 'SQ11-339-01', '', 'COMB01', 'Q', 'DKSH', 'MEDN', 'NORD', '2012.01.06', '2013.01.05', 'HKD', 1000, 1398.0, 'N');
f_add_change_contract( 'SQ11-375-01', '', 'COTR01', 'Q', 'HW', 'APOT', 'APOT', '2012.02.24', '2013.02.23', 'HKD', 1000, 288.0, 'N');
f_add_change_contract( 'SQ11-018-01', '', 'CYCL08', 'Q', 'BAXT', 'BO', 'PRAS', '2011.07.13', '2012.07.12', 'HKD', 50, 65.21, 'N');
f_add_change_contract( 'SQ10-363-01', '', 'CYPR03', 'Q', 'ZUEL', 'BAYR', 'DEL', '2011.05.16', '2012.05.15', 'HKD', 50, 605.0, 'N');
f_add_change_contract( 'HOC040-11', 'A1', 'DAFL02', 'C', 'LFHC', 'SERV', 'SERV', '2011.09.01', '2013.08.31', 'HKD', 30, 103.0, 'N');
f_add_change_contract( 'HOC040-11', 'A', 'DAFL02', 'C', 'LFHC', 'SERV', 'SERV', '2011.09.01', '2013.08.31', 'HKD', 12000, 30900.0, 'N');
f_add_change_contract( 'HOC185-11', 'A', 'DASA03', 'C', 'DKSH', 'BMS', 'BMS', '2012.02.01', '2014.01.31', 'HKD', 60, 9030.0, 'N');
f_add_change_contract( 'HOC185-11', 'B', 'DASA04', 'C', 'DKSH', 'BMS', 'BMS', '2012.02.01', '2014.01.31', 'HKD', 60, 18065.0, 'N');
f_add_change_contract( 'HOC185-11', 'C', 'DASA05', 'C', 'DKSH', 'BMS', 'BMS', '2012.02.01', '2014.01.31', 'HKD', 60, 27015.0, 'N');
f_add_change_contract( 'HOC023-11', 'A', 'DEAN01', 'C', 'VICK', 'VICK', 'VICK', '2011.09.21', '2013.09.20', 'HKD', 500, 199.5, 'N');
f_add_change_contract( 'HOC012-10', 'A', 'DEFE01', 'C', 'HW', 'APOT', 'APOT', '2010.07.22', '2012.07.21', 'HKD', 100, 1400.0, 'N');
f_add_change_contract( 'HOC229-11', 'A', 'DESM04', 'C', 'FERR', 'FERR', 'FERI', '2012.02.28', '2014.02.27', 'HKD', 30, 143.8, 'N');
f_add_change_contract( 'HOC229-11', 'B', 'DESM05', 'C', 'FERR', 'FERR', 'FERI', '2012.02.28', '2014.02.27', 'HKD', 30, 258.8, 'N');
f_add_change_contract( 'SQ11-185-01', '', 'DEXA19', 'Q', 'TBOM', 'PHAA', 'PHAA', '2011.12.22', '2012.12.21', 'HKD', 100, 54.8, 'N');
f_add_change_contract( 'HOC101-10', 'A', 'DIAZ01', 'C', 'VICK', 'VICK', 'VICK', '2011.03.08', '2013.03.07', 'HKD', 1000, 195.5, 'N');
f_add_change_contract( 'HOC101-10', 'B', 'DIAZ02', 'C', 'VICK', 'VICK', 'VICK', '2011.03.08', '2013.03.07', 'HKD', 1000, 214.0, 'N');
f_add_change_contract( 'SQ11-118-01', 'S', 'DICL01', 'Q', 'YSC', 'YSP', 'YSC', '2011.11.24', '2012.11.23', 'HKD', 1000, 110.0, 'N');
f_add_change_contract( 'SQ11-118-01', '', 'DICL01', 'Q', 'YSC', 'YSP', 'YSC', '2011.11.24', '2012.11.23', 'HKD', 16000, 1210.0, 'N');
f_add_change_contract( 'SQ11-381-01', '', 'DICL13', 'Q', 'JPL', 'YANG', 'YANG', '2012.02.25', '2013.02.24', 'HKD', 24, 6.0, 'N');
f_add_change_contract( 'HOC073-11', 'A', 'DIGO03', 'C', 'LCH', 'SIGP', 'SIGM', '2011.10.20', '2013.10.19', 'HKD', 500, 150.0, 'N');
f_add_change_contract( 'HOC073-11', 'B', 'DIGO04', 'C', 'LCH', 'SIGP', 'SIGM', '2011.10.20', '2013.10.19', 'HKD', 100, 32.0, 'N');
f_add_change_contract( 'SQ10-364-01', '', 'DIHY02', 'Q', 'ZUEL', 'GSK', 'GSK', '2011.05.26', '2012.05.25', 'HKD', 100, 209.0, 'N');
f_add_change_contract( 'SQ11-414-01', '', 'DIPY01', 'Q', 'HW', 'APOT', 'APOT', '2012.05.19', '2013.05.18', 'HKD', 100, 20.0, 'N');
f_add_change_contract( 'SQ10-425-01', 'S', 'DIPY01', 'Q', 'YSC', 'YSP', 'YSC', '2011.05.19', '2012.05.18', 'HKD', 1000, 80.0, 'N');
f_add_change_contract( 'SQ10-425-01', '', 'DIPY01', 'Q', 'YSC', 'YSP', 'YSC', '2011.05.19', '2012.05.18', 'HKD', 6000, 400.0, 'N');
f_add_change_contract( 'SQ11-250-01', '', 'DIPY02', 'Q', 'ZUEL', 'APT', 'APT', '2012.02.18', '2013.02.17', 'HKD', 1000, 182.0, 'N');
f_add_change_contract( 'HOC050-11', 'A', 'DIST01', 'C', 'ZUEL', 'NYCO', 'NYCO', '2011.09.15', '2013.09.14', 'HKD', 50, 195.0, 'N');
f_add_change_contract( 'HOC238-10', 'A', 'DOMP01', 'C', 'SYNC', 'SYNC', 'SYNC', '2011.02.14', '2013.02.13', 'HKD', 500, 298.0, 'N');
f_add_change_contract( 'HOC275-10', 'B', 'DOTH02', 'C', 'VICK', 'VICK', 'VICK', '2011.05.23', '2013.05.22', 'HKD', 500, 625.0, 'N');
f_add_change_contract( 'SQ11-277-01', '', 'DOXA03', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2012.03.01', '2013.02.28', 'HKD', 100, 180.0, 'N');
f_add_change_contract( 'HOC220-11', 'A', 'DOXA05', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2012.03.01', '2014.02.28', 'HKD', 100, 180.0, 'N');
f_add_change_contract( 'HOC220-11', 'B', 'DOXA06', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2012.03.01', '2014.02.28', 'HKD', 100, 350.0, 'N');
f_add_change_contract( 'SQ11-309-01', '', 'DOXY01', 'Q', 'HW', 'APOT', 'APOT', '2012.01.06', '2013.01.05', 'HKD', 100, 79.0, 'N');
f_add_change_contract( 'SQ11-217-01', '', 'DYAZ01', 'Q', 'HW', 'APOT', 'APOT', '2012.01.25', '2013.01.24', 'HKD', 1000, 172.0, 'N');
f_add_change_contract( 'HOC061-11', 'A', 'EFAV04', 'C', 'DKSH', 'MSD', 'MSD', '2011.07.20', '2012.07.19', 'HKD', 30, 2340.0, 'N');
f_add_change_contract( 'HOC120-10', 'C', 'ENAL01', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.02', '2012.12.01', 'HKD', 500, 149.5, 'N');
f_add_change_contract( 'HOC120-10', 'A', 'ENAL02', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.02', '2012.12.01', 'HKD', 500, 80.0, 'N');
f_add_change_contract( 'HOC120-10', 'B', 'ENAL03', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.02', '2012.12.01', 'HKD', 500, 89.0, 'N');
f_add_change_contract( 'SQ11-341-01', '', 'ENER01', 'Q', 'UNAM', 'UNIT', 'UNIE', '2012.01.06', '2013.01.05', 'HKD', 100, 38.0, 'N');
f_add_change_contract( 'HOC198-10', 'A', 'ENTA01', 'C', 'ZUEL', 'NOVA', 'ORIO', '2010.12.27', '2012.12.26', 'HKD', 100, 760.0, 'N');
f_add_change_contract( 'HOC185-10', 'A', 'ENTE01', 'C', 'DKSH', 'BMS', 'BMS', '2010.11.22', '2013.11.21', 'HKD', 30, 1272.6, 'N');
f_add_change_contract( 'HOC185-10', 'B', 'ENTE02', 'C', 'DKSH', 'BMS', 'BMS', '2010.11.22', '2013.11.21', 'HKD', 30, 2545.0, 'N');
f_add_change_contract( 'HOC120-11', 'A1', 'ERLO04', 'C', 'DKSH', 'ROCE', 'KREM', '2011.12.15', '2013.09.30', 'HKD', 30, 17500.0, 'N');
f_add_change_contract( 'HOC120-11', 'C', 'ERLO08', 'C', 'DKSH', 'ROCE', 'SCHP', '2011.10.01', '2013.09.30', 'HKD', 30, 3750.0, 'N');
f_add_change_contract( 'HOC120-11', 'B1', 'ERLO09', 'C', 'DKSH', 'ROCE', 'KREM', '2011.12.15', '2013.09.30', 'HKD', 30, 13300.0, 'N');
f_add_change_contract( 'HOC222-11', 'A', 'ESCI01', 'C', 'DKSH', 'LUND', 'LUND', '2012.01.18', '2013.01.17', 'HKD', 28, 223.0, 'N');
f_add_change_contract( 'HOC222-11', 'B', 'ESCI02', 'C', 'DKSH', 'LUND', 'LUND', '2012.01.18', '2013.01.17', 'HKD', 28, 365.0, 'N');
f_add_change_contract( 'HOC122-11', 'A1', 'ESOM01', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2011.12.22', '2013.12.21', 'HKD', 14, 94.55, 'N');
f_add_change_contract( 'HOC122-11', 'A', 'ESOM01', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2011.12.22', '2013.12.21', 'HKD', 12152, 47275.0, 'N');
f_add_change_contract( 'HOC122-11', 'B1', 'ESOM02', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2011.12.22', '2013.12.21', 'HKD', 14, 138.0, 'N');
f_add_change_contract( 'HOC122-11', 'B', 'ESOM02', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2011.12.22', '2013.12.21', 'HKD', 3976, 27600.0, 'N');
f_add_change_contract( 'HOC087-10', 'A', 'ETHA02', 'C', 'MEKI', 'FATO', 'FATO', '2010.11.24', '2012.11.23', 'HKD', 500, 155.0, 'N');
f_add_change_contract( 'HOC087-10', 'B', 'ETHA03', 'C', 'MEKI', 'FATO', 'FATO', '2010.11.24', '2012.11.23', 'HKD', 500, 385.0, 'N');
f_add_change_contract( 'HOC269-11', 'A', 'EVER01', 'C', 'ZUEL', 'NOVA', 'NOVI', '2012.04.01', '2014.03.31', 'HKD', 60, 1015.0, 'N');
f_add_change_contract( 'HOC269-11', 'B', 'EVER02', 'C', 'ZUEL', 'NOVA', 'NOVI', '2012.04.01', '2014.03.31', 'HKD', 60, 2030.0, 'N');
f_add_change_contract( 'HOC269-11', 'C', 'EVER03', 'C', 'ZUEL', 'NOVA', 'NOVI', '2012.04.01', '2014.03.31', 'HKD', 60, 3045.0, 'N');
f_add_change_contract( 'HOC121-11', 'A', 'EVER07', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.10.07', '2013.10.06', 'HKD', 30, 32526.0, 'N');
f_add_change_contract( 'HOC121-11', 'B', 'EVER08', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.10.07', '2013.10.06', 'HKD', 30, 22768.0, 'N');
f_add_change_contract( 'HOC314-10', 'A', 'EXEM01', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2011.03.23', '2013.03.22', 'HKD', 30, 1167.0, 'N');
f_add_change_contract( 'HOC045-11', 'A', 'EZET01', 'C', 'DKSH', 'MSD', 'SP', '2011.08.14', '2013.08.13', 'HKD', 30, 223.5, 'N');
f_add_change_contract( 'SQ11-055-01', '', 'FAMC01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2011.10.14', '2012.10.13', 'HKD', 21, 990.0, 'N');
f_add_change_contract( 'HOC208-10', 'B', 'FAMO01', 'C', 'JEAN', 'JEAN', 'JEAN', '2011.01.05', '2013.01.04', 'HKD', 500, 57.5, 'N');
f_add_change_contract( 'HOC208-10', 'A', 'FAMO02', 'C', 'JEAN', 'JEAN', 'JEAN', '2011.01.05', '2013.01.04', 'HKD', 500, 27.4, 'N');
f_add_change_contract( 'HOC143-11', 'A', 'FERR31', 'C', 'ZUEL', 'APT', 'APT', '2012.01.19', '2014.01.18', 'HKD', 500, 79.0, 'N');
f_add_change_contract( 'SQ10-368-01', 'A1', 'FEXO01', 'Q', 'LFHC', 'SA', 'SA', '2011.07.01', '2012.05.14', 'HKD', 50, 96.52, 'N');
f_add_change_contract( 'SQ11-415-01', '', 'FEXO01', 'Q', 'LFHC', 'SA', 'SA', '2012.05.15', '2013.05.14', 'HKD', 50, 96.52, 'N');
f_add_change_contract( 'SQ11-027-01', '', 'FEXO03', 'Q', 'LFHC', 'SA', 'SA', '2011.07.18', '2012.07.17', 'HKD', 50, 105.0, 'N');
f_add_change_contract( 'SQ10-455-01', '', 'FINA01', 'Q', 'COML', 'CIPL', 'CIPL', '2011.07.20', '2012.07.19', 'HKD', 100, 95.8, 'N');
f_add_change_contract( 'HOC134-11', 'A', 'FLEC01', 'C', 'VICK', 'VICK', 'VICK', '2012.01.20', '2014.01.19', 'HKD', 500, 1650.0, 'N');
f_add_change_contract( 'HOC103-11', 'A', 'FLUN04', 'C', 'DKSH', 'ROCE', 'FHR', '2011.09.13', '2013.09.12', 'HKD', 30, 67.0, 'N');
f_add_change_contract( 'HOC170-10', 'A', 'FLUP01', 'C', 'VICK', 'VICK', 'VICK', '2011.03.08', '2013.03.07', 'HKD', 500, 220.0, 'N');
f_add_change_contract( 'HOC170-10', 'B', 'FLUP02', 'C', 'VICK', 'VICK', 'VICK', '2011.03.08', '2013.03.07', 'HKD', 500, 319.0, 'N');
f_add_change_contract( 'HOC170-10', 'C', 'FLUP03', 'C', 'VICK', 'VICK', 'VICK', '2011.03.08', '2013.03.07', 'HKD', 500, 640.0, 'N');
f_add_change_contract( 'SQ11-262-01', '', 'FLUT01', 'Q', 'HKMS', 'STAD', 'ROTP', '2012.03.16', '2013.03.15', 'EUR', 84, 12.07, 'N');
f_add_change_contract( 'SQ11-362-01', '', 'FLUV01', 'Q', 'ZUEL', 'SOLV', 'SOLV', '2012.03.06', '2013.03.05', 'HKD', 60, 270.0, 'N');
f_add_change_contract( 'SQ11-145-01', '', 'FLUV04', 'Q', 'ZUEL', 'SOLV', 'SOLV', '2011.12.01', '2012.11.30', 'HKD', 30, 156.0, 'N');
f_add_change_contract( 'SQ11-342-01', 'S', 'FOLI01', 'Q', 'ZUEL', 'AUL', 'AUL', '2012.02.23', '2013.02.22', 'HKD', 1000, 67.4, 'N');
f_add_change_contract( 'SQ11-342-01', '', 'FOLI01', 'Q', 'ZUEL', 'AUL', 'AUL', '2012.02.23', '2013.02.22', 'HKD', 5000, 269.6, 'N');
f_add_change_contract( 'SQ11-225-01', '', 'FORT03', 'Q', 'UNAM', 'UNIT', 'UNIE', '2011.12.19', '2012.12.18', 'HKD', 100, 36.8, 'N');
f_add_change_contract( 'HOC050-10', 'A', 'FRUS01', 'C', 'VICK', 'VICK', 'VICK', '2010.09.20', '2012.09.19', 'HKD', 500, 73.0, 'N');
f_add_change_contract( 'SQ11-119-01', '', 'FRUS02', 'Q', 'LCH', 'SIGP', 'SIGP', '2011.11.17', '2012.11.16', 'HKD', 50, 110.0, 'N');
f_add_change_contract( 'HOC094-10', 'A', 'FUSI01', 'C', 'VICK', 'VICK', 'VICK', '2010.11.06', '2012.11.05', 'HKD', 100, 795.0, 'N');
f_add_change_contract( 'HOC296-10', 'D', 'GABA04', 'C', 'ZUEL', 'PFIZ', 'GODE', '2011.02.18', '2013.02.17', 'HKD', 100, 1030.0, 'N');
f_add_change_contract( 'HOC172-11', 'A', 'GEFI01', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2012.01.24', '2014.01.23', 'HKD', 30, 14682.0, 'N');
f_add_change_contract( 'HOC124-10', 'B', 'GEMF02', 'C', 'ZUEL', 'MERC', 'APT', '2010.12.19', '2012.12.18', 'HKD', 500, 220.5, 'N');
f_add_change_contract( 'HOC279-10', 'A', 'GLIB02', 'C', 'ZUEL', 'APT', 'APT', '2011.04.06', '2013.04.05', 'HKD', 500, 66.5, 'N');
f_add_change_contract( 'HOC102-11', 'A', 'GLIC01', 'C', 'ZUEL', 'APT', 'APT', '2011.11.29', '2013.11.28', 'HKD', 500, 98.5, 'N');
f_add_change_contract( 'HOC056-10', 'A1', 'GLIM02', 'C', 'LFHC', 'SA', 'SA', '2011.07.01', '2012.07.15', 'HKD', 30, 13.8, 'N');
f_add_change_contract( 'HOC056-10', 'B1', 'GLIM03', 'C', 'LFHC', 'SA', 'SA', '2011.07.01', '2012.07.15', 'HKD', 30, 27.6, 'N');
f_add_change_contract( 'HOC102-10', 'A', 'GLIP01', 'C', 'ZUEL', 'MERC', 'APT', '2010.12.25', '2012.12.24', 'HKD', 500, 141.0, 'N');
f_add_change_contract( 'SQ11-280-01', '', 'GRAN02', 'Q', 'DKSH', 'ROCE', 'FHR', '2012.02.26', '2013.02.25', 'HKD', 10, 715.0, 'N');
f_add_change_contract( 'SQ11-263-01', '', 'GRIS01', 'Q', 'VICK', 'VICK', 'VICK', '2012.03.01', '2013.02.28', 'HKD', 1000, 533.0, 'N');
f_add_change_contract( 'SQ11-363-01', '', 'HALO03', 'Q', 'ZUEL', 'APT', 'APT', '2012.03.06', '2013.03.05', 'HKD', 1000, 144.5, 'N');
f_add_change_contract( 'SQ11-089-01', '', 'HALO14', 'Q', 'ZUEL', 'APT', 'APT', '2011.10.06', '2012.10.05', 'HKD', 1000, 89.0, 'N');
f_add_change_contract( 'SQ11-213-01', '', 'HYDE01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2012.01.06', '2013.01.05', 'HKD', 100, 223.0, 'N');
f_add_change_contract( 'SQ11-318-01', '', 'HYDR01', 'Q', 'HW', 'APOT', 'APOT', '2012.01.12', '2013.01.11', 'HKD', 100, 18.0, 'N');
f_add_change_contract( 'HOC119-10', 'A', 'HYDR02', 'C', 'ZUEL', 'APT', 'APT', '2010.12.21', '2012.12.20', 'HKD', 500, 166.0, 'N');
f_add_change_contract( 'HOC074-10', 'A1', 'HYDR06', 'C', 'VICK', 'VICK', 'VICK', '2010.09.01', '2012.08.30', 'HKD', 500, 164.0, 'N');
f_add_change_contract( 'HOC282-10', 'A1', 'HYDR21', 'C', 'LFHC', 'SANO', 'SANO', '2011.07.01', '2013.03.31', 'HKD', 60, 40.0, 'N');
f_add_change_contract( 'SQ11-319-01', '', 'HYDR25', 'Q', 'ZUEL', 'UCB', 'UCB', '2012.01.16', '2013.01.15', 'HKD', 500, 172.8, 'N');
f_add_change_contract( 'SQ11-254-01', '', 'HYDR26', 'Q', 'VICK', 'VICK', 'VICK', '2012.02.21', '2013.02.20', 'HKD', 500, 94.0, 'N');
f_add_change_contract( 'SQ10-419-01', '', 'HYOS01', 'Q', 'SYNC', 'SYNC', 'SYNC', '2011.07.19', '2012.07.18', 'HKD', 1000, 120.0, 'N');
f_add_change_contract( 'SQ11-255-01', '', 'IBUP01', 'Q', 'VICK', 'VICK', 'VICK', '2012.03.01', '2013.02.28', 'HKD', 1000, 123.0, 'N');
f_add_change_contract( 'HOC069-10', 'A', 'INDA01', 'C', 'VICK', 'VICK', 'VICK', '2010.11.01', '2012.10.31', 'HKD', 500, 87.5, 'N');
f_add_change_contract( 'SQ11-121-01', '', 'IRBE01', 'Q', 'LFHC', 'SA', 'SW', '2011.11.10', '2012.11.09', 'HKD', 28, 55.0, 'N');
f_add_change_contract( 'SQ11-122-01', '', 'IRBE02', 'Q', 'LFHC', 'SA', 'SW', '2011.11.10', '2012.11.09', 'HKD', 28, 82.5, 'N');
f_add_change_contract( 'HOC047-10', 'A', 'ISOS01', 'C', 'ZUEL', 'APT', 'APT', '2010.10.12', '2012.10.11', 'HKD', 500, 68.9, 'N');
f_add_change_contract( 'HOC039-10', 'A', 'ISOS02', 'C', 'ZUEL', 'GSK', 'SCHW', '2011.01.01', '2012.12.31', 'HKD', 50, 10.5, 'N');
f_add_change_contract( 'HOC025-11', 'A', 'KALE03', 'C', 'ZUEL', 'ABBO', 'ABBO', '2011.05.20', '2012.05.19', 'HKD', 120, 3403.2, 'N');
f_add_change_contract( 'SQ11-386-01', '', 'KETO04', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2012.02.25', '2013.02.24', 'HKD', 100, 540.0, 'N');
f_add_change_contract( 'HOC244-11', 'B', 'KIVE01', 'C', 'ZUEL', 'GSK', 'GWCO', '2012.03.16', '2013.03.15', 'HKD', 30, 4687.0, 'N');
f_add_change_contract( 'HOC122-10', 'A', 'LABE01', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.31', '2012.12.30', 'HKD', 500, 363.0, 'N');
f_add_change_contract( 'HOC122-10', 'B', 'LABE02', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.31', '2012.12.30', 'HKD', 500, 583.0, 'N');
f_add_change_contract( 'HOC173-11', 'A', 'LAMI07', 'C', 'ZUEL', 'GSK', 'GO', '2012.03.16', '2013.03.15', 'HKD', 60, 863.0, 'N');
f_add_change_contract( 'HOC242-11', 'A', 'LAMI09', 'C', 'ZUEL', 'GSK', 'GO', '2012.03.23', '2013.03.22', 'HKD', 28, 728.0, 'N');
f_add_change_contract( 'HOC244-11', 'A', 'LAMI10', 'C', 'ZUEL', 'GSK', 'GO', '2012.03.16', '2013.03.15', 'HKD', 60, 4482.0, 'N');
f_add_change_contract( 'HOC181-10', 'A', 'LAMO01', 'C', 'ZUEL', 'GSK', 'GSK', '2010.11.01', '2012.10.31', 'HKD', 30, 226.1, 'N');
f_add_change_contract( 'HOC181-10', 'B', 'LAMO02', 'C', 'ZUEL', 'GSK', 'GSK', '2010.11.01', '2012.10.31', 'HKD', 30, 387.6, 'N');
f_add_change_contract( 'HOC313-10', 'A', 'LAPA02', 'C', 'ZUEL', 'GSK', 'GO', '2011.03.23', '2013.03.22', 'HKD', 70, 8800.0, 'N');
f_add_change_contract( 'HOC035-09', 'A1', 'LEFL01', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.06.18', 'HKD', 30, 380.0, 'N');
f_add_change_contract( 'HOC035-09', 'B1', 'LEFL02', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.06.18', 'HKD', 30, 480.0, 'N');
f_add_change_contract( 'HOC035-09', 'C1', 'LEFL03', 'C', 'LFHC', 'SA', 'SWI', '2011.07.01', '2012.06.18', 'HKD', 3, 135.0, 'N');
f_add_change_contract( 'HOC179-10', 'B1', 'LEVE01', 'C', 'ZUEL', 'GSK', 'UCBP', '2011.10.20', '2012.10.21', 'HKD', 60, 618.0, 'N');
f_add_change_contract( 'HOC179-10', 'C1', 'LEVE02', 'C', 'ZUEL', 'GSK', 'UCBP', '2011.12.01', '2012.10.21', 'HKD', 60, 1236.0, 'N');
f_add_change_contract( 'HOC179-10', 'A1', 'LEVE03', 'C', 'ZUEL', 'GSK', 'UCBP', '2011.09.20', '2012.10.21', 'HKD', 60, 348.0, 'N');
f_add_change_contract( 'HOC224-10', 'B', 'LEVO09', 'C', 'VICK', 'VICK', 'VICK', '2011.01.21', '2013.01.20', 'HKD', 500, 440.0, 'N');
f_add_change_contract( 'HOC224-10', 'A', 'LEVO18', 'C', 'VICK', 'VICK', 'VICK', '2011.01.21', '2013.01.20', 'HKD', 500, 825.0, 'N');
f_add_change_contract( 'HOC187-10', 'A', 'LINE12', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.12.07', '2012.06.06', 'HKD', 20, 8200.0, 'N');
f_add_change_contract( 'HOC146-11', 'A', 'LISI01', 'C', 'ZUEL', 'APT', 'APT', '2012.01.11', '2014.01.10', 'HKD', 500, 84.5, 'N');
f_add_change_contract( 'HOC146-11', 'B', 'LISI02', 'C', 'ZUEL', 'APT', 'APT', '2012.01.11', '2014.01.10', 'HKD', 500, 107.5, 'N');
f_add_change_contract( 'HOC146-11', 'C', 'LISI03', 'C', 'ZUEL', 'APT', 'APT', '2012.01.11', '2014.01.10', 'HKD', 500, 134.0, 'N');
f_add_change_contract( 'SQ10-328-01', '', 'LORA04', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.04.25', '2012.04.24', 'HKD', 500, 104.0, 'N');
f_add_change_contract( 'SQ11-390-01', '', 'LORA04', 'Q', 'JEAN', 'JEAN', 'JEAN', '2012.04.25', '2013.04.24', 'HKD', 500, 114.0, 'N');
f_add_change_contract( 'HOC048-11', 'A', 'LOSA01', 'C', 'DKSH', 'MSD', 'MSD', '2011.07.04', '2012.07.03', 'HKD', 30, 24.0, 'N');
f_add_change_contract( 'SQ10-329-01', '', 'LOSA02', 'Q', 'DKSH', 'MSD', 'MSD', '2011.04.18', '2012.04.17', 'HKD', 30, 49.5, 'N');
f_add_change_contract( 'SQ11-322-01', '', 'LOSA02', 'Q', 'DKSH', 'MSD', 'MSD', '2012.04.18', '2013.04.17', 'HKD', 30, 49.5, 'N');
f_add_change_contract( 'HOC048-11', 'B', 'LOSA03', 'C', 'DKSH', 'MSD', 'MSD', '2011.07.04', '2012.07.03', 'HKD', 30, 31.0, 'N');
f_add_change_contract( 'SQ11-030-01', '', 'LOSA04', 'Q', 'DKSH', 'MSD', 'MSD', '2011.07.13', '2012.07.12', 'HKD', 30, 88.5, 'N');
f_add_change_contract( 'HOC171-10', 'A2', 'MADO02', 'C', 'DKSH', 'ROCE', 'ROCS', '2011.01.01', '2012.10.29', 'HKD', 100, 130.0, 'N');
f_add_change_contract( 'SQ10-500-01', '', 'MEBE04', 'Q', 'ZUEL', 'SOLV', 'SOLV', '2011.07.11', '2012.07.10', 'HKD', 50, 139.0, 'N');
f_add_change_contract( 'SQ11-397-01', '', 'MECO01', 'Q', 'DKSH', 'XXX', 'EISA', '2012.07.09', '2013.07.08', 'HKD', 100, 35.0, 'N');
f_add_change_contract( 'HOC033-10', 'A', 'MECO01', 'C', 'JPL', 'NCPK', 'NCPK', '2010.07.09', '2012.07.08', 'HKD', 200, 77.8, 'N');
f_add_change_contract( 'SQ11-282-01', '', 'MEDR02', 'Q', 'HW', 'APOT', 'APOT', '2012.02.04', '2013.02.03', 'HKD', 100, 69.0, 'N');
f_add_change_contract( 'SQ11-205-01', '', 'MEGE01', 'Q', 'HW', 'APOT', 'APOT', '2012.01.16', '2013.01.15', 'HKD', 100, 248.0, 'N');
f_add_change_contract( 'SQ11-133-01', '', 'MELP01', 'Q', 'PRIM', 'GSK', 'EXC', '2011.11.12', '2012.11.11', 'HKD', 25, 338.0, 'N');
f_add_change_contract( 'HOC324-10', 'A', 'MEMA01', 'C', 'DKSH', 'LUND', 'ROTP', '2011.03.17', '2013.03.16', 'HKD', 56, 456.0, 'N');
f_add_change_contract( 'SQ10-474-01', '', 'MERC07', 'Q', 'PRIM', 'GSK', 'EXC', '2011.07.31', '2012.07.30', 'HKD', 25, 520.0, 'N');
f_add_change_contract( 'HOC265-10', 'A', 'METF02', 'C', 'ELC', 'ELC', 'ELC', '2011.04.14', '2013.04.13', 'HKD', 500, 59.0, 'N');
f_add_change_contract( 'HOC249-10', 'A', 'METF02', 'C', 'ZUEL', 'MERK', 'MSS', '2011.04.14', '2013.04.13', 'HKD', 500, 65.0, 'N');
f_add_change_contract( 'SQ10-408-01', 'A1', 'METH01', 'Q', 'LFAU', 'PLIV', 'PLIV', '2011.07.01', '2012.08.04', 'HKD', 100, 94.0, 'N');
f_add_change_contract( 'SQ11-070-01', '', 'METH10', 'Q', 'ZUEL', 'EPL', 'EBEW', '2011.10.04', '2012.10.03', 'HKD', 50, 24.8, 'N');
f_add_change_contract( 'HOC126-10', 'B', 'METH22', 'C', 'ZUEL', 'APT', 'APT', '2010.12.16', '2012.12.15', 'HKD', 500, 141.5, 'N');
f_add_change_contract( 'HOC126-10', 'A', 'METH23', 'C', 'ZUEL', 'MERC', 'APT', '2010.12.16', '2012.12.15', 'HKD', 500, 167.0, 'N');
f_add_change_contract( 'HOC020-10', 'A', 'METH27', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.06.18', '2012.06.17', 'HKD', 200, 200.0, 'N');
f_add_change_contract( 'SQ11-332-01', 'S', 'METO01', 'Q', 'ZUEL', 'AUL', 'AUL', '2012.04.13', '2013.04.12', 'HKD', 1000, 102.0, 'N');
f_add_change_contract( 'SQ10-315-01', 'S', 'METO01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.04.13', '2012.04.12', 'HKD', 1000, 107.0, 'N');
f_add_change_contract( 'SQ11-332-01', '', 'METO01', 'Q', 'ZUEL', 'AUL', 'AUL', '2012.04.13', '2013.04.12', 'HKD', 5000, 408.0, 'N');
f_add_change_contract( 'SQ10-315-01', '', 'METO01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.04.13', '2012.04.12', 'HKD', 5000, 428.0, 'N');
f_add_change_contract( 'SQ10-437-01', 'A1', 'METO05', 'Q', 'ZUEL', 'UCBM', 'UCBM', '2012.03.01', '2012.07.25', 'HKD', 100, 380.0, 'N');
f_add_change_contract( 'HOC207-11', 'A', 'METO06', 'C', 'ZUEL', 'APT', 'APT', '2012.05.01', '2014.04.30', 'HKD', 500, 48.8, 'N');
f_add_change_contract( 'HOC252-10', 'A', 'METO06', 'C', 'ZUEL', 'APT', 'APT', '2011.05.01', '2012.04.30', 'HKD', 500, 61.0, 'N');
f_add_change_contract( 'HOC207-11', 'B', 'METO07', 'C', 'ZUEL', 'APT', 'APT', '2012.05.01', '2014.04.30', 'HKD', 500, 68.8, 'N');
f_add_change_contract( 'HOC252-10', 'B', 'METO07', 'C', 'ZUEL', 'APT', 'APT', '2011.05.01', '2012.04.30', 'HKD', 500, 84.0, 'N');
f_add_change_contract( 'SQ11-127-01', 'S', 'METR01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.11.13', '2012.11.12', 'HKD', 250, 39.0, 'N');
f_add_change_contract( 'SQ11-127-01', '', 'METR01', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.11.13', '2012.11.12', 'HKD', 3000, 390.0, 'N');
f_add_change_contract( 'HOC319-10', 'A', 'MIAN01', 'C', 'LFHC', 'REME', 'REME', '2011.09.08', '2013.09.07', 'HKD', 100, 41.9, 'N');
f_add_change_contract( 'HOC319-10', 'B', 'MIAN02', 'C', 'LFHC', 'REME', 'REME', '2011.09.08', '2013.09.07', 'HKD', 20, 19.9, 'N');
f_add_change_contract( 'HOC109-11', 'A', 'MIDA01', 'C', 'DKSH', 'ROCE', 'FHR', '2012.02.01', '2014.01.31', 'HKD', 100, 285.0, 'N');
f_add_change_contract( 'SQ11-073-01', '', 'MIDO01', 'Q', 'ZUEL', 'NYCO', 'NYCO', '2011.10.07', '2012.10.06', 'HKD', 50, 105.0, 'N');
f_add_change_contract( 'SQ11-354-01', '', 'MOCL01', 'Q', 'ZUEL', 'FHR', 'FHR', '2012.03.27', '2013.03.26', 'HKD', 100, 405.0, 'N');
f_add_change_contract( 'HOC048-10', 'A', 'MODU01', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.08.10', '2012.08.09', 'HKD', 500, 93.7, 'N');
f_add_change_contract( 'HOC169-10', 'A', 'MONT02', 'C', 'DKSH', 'MSD', 'MSD', '2010.11.08', '2012.11.07', 'HKD', 28, 278.0, 'N');
f_add_change_contract( 'SQ11-264-01', '', 'MOXI01', 'Q', 'ZUEL', 'BH', 'BH', '2012.03.19', '2013.03.18', 'HKD', 5, 133.0, 'N');
f_add_change_contract( 'HOC062-11', 'B', 'MYCO06', 'C', 'DKSH', 'ROCE', 'ROCE', '2011.07.21', '2012.07.20', 'HKD', 50, 830.0, 'N');
f_add_change_contract( 'SQ11-257-01', '', 'NAFT03', 'Q', 'ZUEL', 'MERC', 'FAMO', '2012.03.19', '2013.03.18', 'HKD', 20, 66.0, 'N');
f_add_change_contract( 'HOC083-11', 'A', 'NAPR01', 'C', 'ZUEL', 'MERC', 'APT', '2011.10.20', '2013.10.19', 'HKD', 100, 27.4, 'N');
f_add_change_contract( 'SQ11-293-01', '', 'NEUR01', 'Q', 'LFAU', 'UPL', 'UPL', '2011.12.15', '2012.12.14', 'HKD', 1000, 230.0, 'N');
f_add_change_contract( 'HOC007-11', 'A', 'NEVI01', 'C', 'ZUEL', 'BOEH', 'BIE', '2011.06.30', '2012.06.29', 'HKD', 60, 2230.0, 'N');
f_add_change_contract( 'HOC074-11', 'A', 'NICE01', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2011.10.26', '2013.10.25', 'HKD', 30, 17.0, 'N');
f_add_change_contract( 'HOC074-11', 'B', 'NICE02', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2011.10.26', '2013.10.25', 'HKD', 50, 36.0, 'N');
f_add_change_contract( 'HOC074-11', 'C', 'NICE04', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2011.10.26', '2013.10.25', 'HKD', 30, 46.8, 'N');
f_add_change_contract( 'SQ11-096-01', '', 'NITR01', 'Q', 'LCH', 'ALPA', 'ALPA', '2011.10.26', '2012.10.25', 'HKD', 25, 17.0, 'N');
f_add_change_contract( 'SQ11-224-01', '', 'NORE01', 'Q', 'MEKI', 'GEDE', 'GEDE', '2012.01.08', '2013.01.07', 'HKD', 20, 10.4, 'N');
f_add_change_contract( 'SQ10-501-01', '', 'NORT01', 'Q', 'DKSH', 'LUND', 'LUND', '2011.08.28', '2012.08.27', 'HKD', 100, 65.0, 'N');
f_add_change_contract( 'SQ11-146-01', '', 'NORT02', 'Q', 'DKSH', 'LUND', 'LUND', '2011.12.01', '2012.11.30', 'HKD', 100, 94.0, 'N');
f_add_change_contract( 'SQ11-355-01', '', 'OEST04', 'Q', 'ZUEL', 'WYET', 'WYET', '2012.03.27', '2013.03.26', 'HKD', 28, 68.0, 'N');
f_add_change_contract( 'SQ11-283-01', '', 'OEST05', 'Q', 'ZUEL', 'WYET', 'WYET', '2012.03.21', '2013.03.20', 'HKD', 28, 70.0, 'N');
f_add_change_contract( 'SQ11-129-01', '', 'OFLO01', 'Q', 'HKMS', 'DAIS', 'CMIC', '2011.11.03', '2012.11.02', 'HKD', 500, 1851.0, 'N');
f_add_change_contract( 'HOC002-11', 'A', 'ONDA01', 'C', 'ZUEL', 'GSK', 'ABO', '2011.07.07', '2013.07.06', 'HKD', 10, 88.8, 'N');
f_add_change_contract( 'HOC002-11', 'B', 'ONDA02', 'C', 'ZUEL', 'GSK', 'ABO', '2011.07.07', '2013.07.06', 'HKD', 10, 98.8, 'N');
f_add_change_contract( 'HOC130-11', 'A', 'OXCA01', 'C', 'ZUEL', 'NOVA', 'NOVF', '2011.11.18', '2013.11.17', 'HKD', 50, 372.0, 'N');
f_add_change_contract( 'HOC130-11', 'B', 'OXCA02', 'C', 'ZUEL', 'NOVA', 'NOVF', '2011.11.18', '2013.11.17', 'HKD', 50, 203.0, 'N');
f_add_change_contract( 'HOC322-10', 'A', 'OXYB03', 'C', 'HW', 'APOT', 'APOT', '2011.04.01', '2013.03.31', 'HKD', 500, 420.0, 'N');
f_add_change_contract( 'HOC021-11', 'B', 'PANT01', 'C', 'ZUEL', 'NYCO', 'NOG', '2011.09.01', '2012.08.31', 'HKD', 2142, 4060.0, 'N');
f_add_change_contract( 'HOC021-11', 'A', 'PANT03', 'C', 'ZUEL', 'NYCO', 'NOG', '2011.09.01', '2012.08.31', 'HKD', 1750, 2660.0, 'N');
f_add_change_contract( 'HOC064-11', 'A', 'PARA01', 'C', 'MEDI', 'MEDI', 'MEDI', '2011.08.11', '2013.08.10', 'HKD', 1000, 37.3, 'N');
f_add_change_contract( 'HOC239-10', 'A', 'PARO02', 'C', 'ZUEL', 'GSK', 'SCE', '2011.04.13', '2013.04.12', 'HKD', 20, 17.6, 'N');
f_add_change_contract( 'HOC182-10', 'A2', 'PERI01', 'C', 'LFHC', 'SA', 'SA', '2012.01.31', '2013.02.17', 'HKD', 84, 30.8, 'N');
f_add_change_contract( 'HOC182-10', 'B1', 'PERI02', 'C', 'LFHC', 'SA', 'SA', '2011.07.01', '2013.02.17', 'HKD', 84, 70.0, 'N');
f_add_change_contract( 'SQ11-206-01', '', 'PHEN17', 'Q', 'UI', 'CLON', 'CLON', '2012.01.05', '2013.01.04', 'HKD', 1000, 860.0, 'N');
f_add_change_contract( 'SQ11-189-01', '', 'PHEN37', 'Q', 'CRCI', 'SSP', 'SSP', '2011.12.19', '2012.12.18', 'HKD', 1000, 75.5, 'N');
f_add_change_contract( 'SQ10-389-01', '', 'PHYT05', 'Q', 'SINO', 'XING', 'XING', '2011.06.10', '2012.06.09', 'HKD', 1000, 836.0, 'N');
f_add_change_contract( 'SQ11-398-01', '', 'PHYT05', 'Q', 'SINO', 'XING', 'XING', '2012.06.10', '2013.06.09', 'HKD', 1000, 879.8, 'N');
f_add_change_contract( 'HOC001-10', 'A1', 'PIMO01', 'C', 'VICK', 'VICK', 'VICK', '2011.06.17', '2012.06.16', 'HKD', 500, 223.0, 'N');
f_add_change_contract( 'HOC001-10', 'B1', 'PIMO02', 'C', 'VICK', 'VICK', 'VICK', '2011.06.17', '2012.06.16', 'HKD', 500, 814.0, 'N');
f_add_change_contract( 'SQ10-386-01', '', 'PIND01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2011.06.10', '2012.06.09', 'HKD', 30, 75.0, 'N');
f_add_change_contract( 'SQ11-420-01', '', 'PIND01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2012.06.10', '2013.06.09', 'HKD', 30, 80.0, 'N');
f_add_change_contract( 'SQ11-097-01', 'S', 'PIOG02', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.10.05', '2012.10.04', 'HKD', 30, 300.0, 'N');
f_add_change_contract( 'SQ11-097-01', '', 'PIOG02', 'Q', 'ZUEL', 'TAKD', 'TAKD', '2011.10.05', '2012.10.04', 'HKD', 90, 600.0, 'N');
f_add_change_contract( 'SQ11-284-01', '', 'PIRA02', 'Q', 'ZUEL', 'GSK', 'UCBP', '2012.03.15', '2013.03.14', 'HKD', 90, 28.9, 'N');
f_add_change_contract( 'SQ11-285-01', '', 'PIZO01', 'Q', 'ZUEL', 'NOVA', 'NOVF', '2012.03.29', '2013.03.28', 'HKD', 30, 68.0, 'N');
f_add_change_contract( 'SQ11-265-01', '', 'PRAZ02', 'Q', 'ZUEL', 'BH', 'BH', '2012.03.26', '2013.03.25', 'HKD', 4, 420.0, 'N');
f_add_change_contract( 'HOC125-10', 'A', 'PRAZ03', 'C', 'ZUEL', 'APT', 'APT', '2010.12.11', '2012.12.10', 'HKD', 500, 88.9, 'N');
f_add_change_contract( 'HOC125-10', 'B', 'PRAZ04', 'C', 'ZUEL', 'APT', 'APT', '2010.12.11', '2012.12.10', 'HKD', 500, 102.6, 'N');
f_add_change_contract( 'HOC293-10', 'A', 'PRED02', 'C', 'ZUEL', 'APT', 'APT', '2011.05.20', '2013.05.19', 'HKD', 500, 63.8, 'N');
f_add_change_contract( 'SQ11-031-01', '', 'PROB01', 'Q', 'SYNC', 'SYNC', 'SYNC', '2011.07.18', '2012.07.17', 'HKD', 500, 623.0, 'N');
f_add_change_contract( 'SQ10-395-01', '', 'PROC08', 'Q', 'ZUEL', 'AUL', 'AUL', '2011.06.02', '2012.06.01', 'HKD', 1000, 108.0, 'N');
f_add_change_contract( 'HOC135-11', 'A', 'PROP01', 'C', 'JEAN', 'JEAN', 'JEAN', '2012.01.07', '2014.01.06', 'HKD', 500, 1150.0, 'N');
f_add_change_contract( 'HOC110-11', 'A', 'PROP04', 'C', 'ZUEL', 'AUL', 'AUL', '2012.01.01', '2013.12.31', 'HKD', 28, 2.91, 'N');
f_add_change_contract( 'HOC063-10', 'A', 'PYRA04', 'C', 'MEKI', 'FATO', 'FATO', '2010.12.31', '2012.12.30', 'HKD', 1000, 700.0, 'N');
f_add_change_contract( 'HOC014-11', 'A', 'PYRI01', 'C', 'VICK', 'VICK', 'VICK', '2011.08.07', '2013.08.06', 'HKD', 100, 23.2, 'N');
f_add_change_contract( 'HOC037-10', 'A', 'PYRI02', 'C', 'ZUEL', 'INO', 'LABI', '2010.11.07', '2012.11.06', 'HKD', 150, 192.0, 'N');
f_add_change_contract( 'SQ10-418-01', '', 'PYRI04', 'Q', 'MEDI', 'MEDI', 'MEDI', '2011.07.23', '2012.07.22', 'HKD', 1000, 60.0, 'N');
f_add_change_contract( 'SQ11-287-01', '', 'PYRI06', 'Q', 'ZUEL', 'MERC', 'MERK', '2012.03.15', '2013.03.14', 'HKD', 100, 190.0, 'N');
f_add_change_contract( 'SQ11-336-01', 'S', 'QUET01', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.12.24', '2012.05.23', 'HKD', 60, 180.0, 'N');
f_add_change_contract( 'SQ11-336-01', '', 'QUET01', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.12.24', '2012.05.23', 'HKD', 720, 1800.0, 'N');
f_add_change_contract( 'HOC190-11', 'A', 'RAMI01', 'C', 'LFHC', 'SA', 'SA', '2012.02.23', '2014.02.22', 'HKD', 28, 14.2, 'N');
f_add_change_contract( 'HOC190-11', 'B', 'RAMI02', 'C', 'LFHC', 'SA', 'SA', '2012.02.23', '2014.02.22', 'HKD', 28, 13.7, 'N');
f_add_change_contract( 'SQ11-102-01', '', 'RISE02', 'Q', 'LFHC', 'SA', 'NORW', '2011.07.25', '2012.07.24', 'HKD', 4, 240.0, 'N');
f_add_change_contract( 'HOC111-10', 'A', 'RISP01', 'C', 'DKSH', 'JC', 'JC', '2010.09.21', '2012.09.20', 'HKD', 60, 28.75, 'N');
f_add_change_contract( 'HOC001-11', 'A', 'RISP02', 'C', 'DKSH', 'JC', 'JC', '2011.05.05', '2013.05.04', 'HKD', 60, 36.17, 'N');
f_add_change_contract( 'HOC001-11', 'B', 'RISP03', 'C', 'DKSH', 'JC', 'JC', '2011.05.05', '2013.05.04', 'HKD', 60, 63.62, 'N');
f_add_change_contract( 'HOC001-11', 'C', 'RISP04', 'C', 'DKSH', 'JP', 'JC', '2011.05.05', '2013.05.04', 'HKD', 60, 70.9, 'N');
f_add_change_contract( 'HOC237-11', 'A', 'ROSU01', 'C', 'ZUEL', 'ASTZ', 'IPR', '2012.04.19', '2015.04.18', 'HKD', 28, 155.0, 'N');
f_add_change_contract( 'HOC237-11', 'B', 'ROSU02', 'C', 'ZUEL', 'ASTZ', 'IPR', '2012.04.19', '2015.04.18', 'HKD', 28, 267.0, 'N');
f_add_change_contract( 'SQ10-469-01', '', 'SAQU03', 'Q', 'DKSH', 'ROCE', 'ROCE', '2011.07.31', '2012.07.30', 'HKD', 120, 2288.0, 'N');
f_add_change_contract( 'HOC031-10', 'A', 'SENN01', 'C', 'ZUEL', 'RECK', 'RECK', '2010.07.29', '2012.07.28', 'HKD', 500, 61.6, 'N');
f_add_change_contract( 'SQ11-394-01', '', 'SERT01', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2012.02.24', '2012.07.23', 'HKD', 30, 19.5, 'N');
f_add_change_contract( 'SQ10-390-01', 'A1', 'SEVE01', 'Q', 'LFHC', 'KIRI', 'GENI', '2011.07.01', '2012.06.22', 'HKD', 360, 1420.0, 'N');
f_add_change_contract( 'SQ11-399-01', '', 'SEVE01', 'Q', 'LFHC', 'KIRI', 'GENI', '2012.06.23', '2013.06.22', 'HKD', 360, 1420.0, 'N');
f_add_change_contract( 'HOC085-11', 'A', 'SILD04', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.08.01', '2013.07.31', 'HKD', 630, 26925.0, 'N');
f_add_change_contract( 'HOC100-10', 'A', 'SINE01', 'C', 'TIMC', 'TEVA', 'TEVA', '2011.01.10', '2013.01.09', 'HKD', 100, 39.0, 'N');
f_add_change_contract( 'HOC100-10', 'B', 'SINE02', 'C', 'TIMC', 'TEVA', 'TEVA', '2011.01.10', '2013.01.09', 'HKD', 100, 57.0, 'N');
f_add_change_contract( 'HOC217-11', 'A1', 'SIRO02', 'C', 'ZUEL', 'PFIZ', 'WYEP', '2012.02.24', '2014.02.18', 'HKD', 100, 4520.0, 'N');
f_add_change_contract( 'HOC063-11', 'A1', 'SITA02', 'C', 'DKSH', 'MSD', 'MSD', '2011.07.22', '2013.06.24', 'HKD', 28, 165.2, 'N');
f_add_change_contract( 'HOC063-11', 'B1', 'SITA03', 'C', 'DKSH', 'MSD', 'MSD', '2011.07.22', '2013.06.24', 'HKD', 28, 165.2, 'N');
f_add_change_contract( 'SQ11-116-01', '', 'SODI1Y', 'Q', 'MEDI', 'MEDI', 'MEDI', '2011.11.03', '2012.11.02', 'HKD', 1000, 74.0, 'N');
f_add_change_contract( 'HOC311-10', 'A', 'SORA01', 'C', 'ZUEL', 'BH', 'BH', '2011.03.22', '2013.03.21', 'HKD', 60, 18540.0, 'N');
f_add_change_contract( 'HOC077-10', 'A', 'SOTA01', 'C', 'ZUEL', 'APT', 'APT', '2010.11.01', '2012.10.31', 'HKD', 700, 593.6, 'N');
f_add_change_contract( 'HOC123-10', 'A', 'SPIR01', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.16', '2012.12.15', 'HKD', 500, 196.0, 'N');
f_add_change_contract( 'HOC206-10', 'A', 'STAL01', 'C', 'ZUEL', 'NOVA', 'ORIO', '2010.12.01', '2012.11.30', 'HKD', 100, 760.0, 'N');
f_add_change_contract( 'SQ10-496-01', '', 'SULI01', 'Q', 'LCH', 'ALPA', 'ALPA', '2011.08.29', '2012.08.28', 'HKD', 50, 115.7, 'N');
f_add_change_contract( 'HOC241-10', 'B', 'SULP20', 'C', 'VICK', 'VICK', 'VICK', '2011.03.26', '2013.03.25', 'HKD', 500, 244.0, 'N');
f_add_change_contract( 'SQ11-311-01', '', 'TAMO01', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.10.28', '2012.04.27', 'HKD', 30, 12.3, 'N');
f_add_change_contract( 'SQ11-312-01', '', 'TAMO02', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.10.28', '2012.04.27', 'HKD', 30, 25.4, 'N');
f_add_change_contract( 'HOC144-10', 'A1', 'TELB01', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.07.09', '2012.10.30', 'HKD', 560, 14000.0, 'N');
f_add_change_contract( 'SQ10-369-01', 'A1', 'TELF01', 'Q', 'LFHC', 'SA', 'SA', '2011.07.01', '2012.05.14', 'HKD', 50, 103.0, 'N');
f_add_change_contract( 'SQ11-422-01', '', 'TELF01', 'Q', 'LFHC', 'SA', 'SA', '2012.05.15', '2013.05.14', 'HKD', 50, 103.0, 'N');
f_add_change_contract( 'HOC112-11', 'A', 'TELM01', 'C', 'ZUEL', 'BOEH', 'BOEH', '2011.11.05', '2012.11.04', 'HKD', 30, 69.0, 'N');
f_add_change_contract( 'HOC121-11', 'A', 'TELM01', 'C', 'ZUEL', 'BOEH', 'BOEH', '2011.11.05', '2012.11.04', 'HKD', 30, 69.0, 'N');
f_add_change_contract( 'HOC112-11', 'B', 'TELM02', 'C', 'ZUEL', 'BOEH', 'BOEH', '2011.11.05', '2012.11.04', 'HKD', 30, 96.0, 'N');
f_add_change_contract( 'SQ11-291-01', '', 'TENO03', 'C', 'LFHC', 'GILE', 'PATH', '2011.10.20', '2012.10.19', 'HKD', 30, 1560.0, 'N');
f_add_change_contract( 'HOC118-10', 'A', 'TERA01', 'C', 'ZUEL', 'ABBO', 'AESI', '2011.02.01', '2013.01.31', 'HKD', 140, 38.5, 'N');
f_add_change_contract( 'HOC248-10', 'A', 'TERA02', 'C', 'ELC', 'ELC', 'ELC', '2011.06.01', '2013.05.31', 'HKD', 100, 38.0, 'N');
f_add_change_contract( 'HOC118-10', 'B', 'TERA03', 'C', 'ZUEL', 'ABBO', 'AESI', '2011.02.01', '2013.01.31', 'HKD', 28, 22.4, 'N');
f_add_change_contract( 'SQ11-376-01', '', 'TERB12', 'Q', 'HW', 'APOT', 'APOT', '2012.02.24', '2013.02.23', 'HKD', 100, 180.0, 'N');
f_add_change_contract( 'SQ11-349-01', '', 'THIA03', 'Q', 'VICK', 'VICK', 'VICK', '2012.02.23', '2013.02.22', 'HKD', 1000, 218.0, 'N');
f_add_change_contract( 'SQ11-134-01', '', 'THIO02', 'Q', 'PRIM', 'GSK', 'DSM', '2011.12.01', '2012.11.30', 'HKD', 25, 1177.0, 'N');
f_add_change_contract( 'HOC198-11', 'A', 'TOLT01', 'C', 'ZUEL', 'PFIZ', 'PFIS', '2012.01.21', '2014.01.20', 'HKD', 56, 160.0, 'N');
f_add_change_contract( 'HOC245-10', 'A', 'TOPI01', 'C', 'DKSH', 'JC', 'CILA', '2011.01.20', '2013.01.19', 'HKD', 60, 452.0, 'N');
f_add_change_contract( 'HOC245-10', 'B', 'TOPI02', 'C', 'DKSH', 'JC', 'CILA', '2011.01.20', '2013.01.19', 'HKD', 60, 676.0, 'N');
f_add_change_contract( 'HOC245-10', 'C', 'TOPI03', 'C', 'DKSH', 'JC', 'CILA', '2011.01.20', '2013.01.19', 'HKD', 60, 1185.0, 'N');
f_add_change_contract( 'HOC177-10', 'A1', 'TRAZ01', 'C', 'LFHC', 'AF', 'AF', '2011.07.01', '2012.10.21', 'HKD', 30, 37.0, 'N');
f_add_change_contract( 'SQ10-396-01', '', 'TRIA07', 'Q', 'ZUEL', 'PFIZ', 'SANI', '2011.06.10', '2012.06.09', 'HKD', 100, 390.0, 'N');
f_add_change_contract( 'SQ11-011-01', '', 'TRIF01', 'C', 'HW', 'APOT', 'APOT', '2011.08.19', '2012.08.18', 'HKD', 100, 20.8, 'N');
f_add_change_contract( 'SQ11-011-02', '', 'TRIF02', 'Q', 'HW', 'APOT', 'APOT', '2011.09.21', '2012.09.20', 'HKD', 1000, 208.0, 'N');
f_add_change_contract( 'HOC183-10', 'A1', 'TRIM06', 'C', 'LFHC', 'SA', 'SA', '2011.07.01', '2012.11.12', 'HKD', 50, 37.45, 'N');
f_add_change_contract( 'HOC184-11', 'A', 'TRIZ01', 'C', 'ZUEL', 'GSK', 'GWCO', '2012.01.08', '2013.01.07', 'HKD', 60, 6696.0, 'N');
f_add_change_contract( 'SQ11-078-01', '', 'UBID01', 'Q', 'DKSH', 'EISA', 'EISA', '2011.10.07', '2012.10.06', 'HKD', 100, 189.0, 'N');
f_add_change_contract( 'SQ11-303-01', '', 'URSO02', 'Q', 'HAPL', 'MTB', 'MPTE', '2012.01.06', '2013.01.05', 'HKD', 1000, 710.0, 'N');
f_add_change_contract( 'SQ11-059-01', 'S', 'VALA01', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.10.17', '2012.10.16', 'HKD', 42, 1130.0, 'N');
f_add_change_contract( 'SQ11-059-01', '', 'VALA01', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.10.17', '2012.10.16', 'HKD', 462, 11300.0, 'N');
f_add_change_contract( 'HOC218-11', 'A', 'VALG01', 'C', 'DKSH', 'ROCE', 'PATH', '2012.03.01', '2014.02.28', 'HKD', 60, 17900.0, 'N');
f_add_change_contract( 'HOC191-11', 'A1', 'VALS02', 'C', 'ZUEL', 'NOVA', 'NFSA', '2012.02.16', '2014.02.15', 'HKD', 28, 139.0, 'N');
f_add_change_contract( 'HOC191-11', 'A', 'VALS02', 'C', 'ZUEL', 'NOVA', 'NFSA', '2012.02.16', '2014.02.15', 'HKD', 2968, 4726.0, 'N');
f_add_change_contract( 'HOC191-11', 'B1', 'VALS03', 'C', 'ZUEL', 'NOVA', 'NFSA', '2012.02.16', '2014.02.15', 'HKD', 28, 204.0, 'N');
f_add_change_contract( 'HOC191-11', 'B', 'VALS03', 'C', 'ZUEL', 'NOVA', 'NFSA', '2012.02.16', '2014.02.15', 'HKD', 2268, 4488.0, 'N');
f_add_change_contract( 'SQ11-079-01', '', 'VERA01', 'Q', 'ZUEL', 'ABBO', 'ABBO', '2011.10.20', '2012.10.19', 'HKD', 100, 33.0, 'N');
f_add_change_contract( 'SQ10-366-01', 'A1', 'VIGA01', 'Q', 'LFHC', 'SA', 'PATH', '2011.07.01', '2012.05.25', 'HKD', 100, 979.0, 'N');
f_add_change_contract( 'SQ10-486-01', '', 'VITA10', 'Q', 'CRCI', 'SSP', 'SSP', '2011.08.28', '2012.08.27', 'HKD', 5000, 110.5, 'N');
f_add_change_contract( 'HOC044-10', 'A', 'VORI01', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2010.07.25', '2012.07.24', 'HKD', 14, 5300.0, 'N');
f_add_change_contract( 'HOC017-11', 'A', 'WARF01', 'C', 'TIMC', 'TEVA', 'TPW', '2011.07.17', '2013.07.16', 'HKD', 28, 8.12, 'N');
f_add_change_contract( 'HOC017-11', 'B', 'WARF02', 'C', 'TIMC', 'TEVA', 'TPW', '2011.07.17', '2013.07.16', 'HKD', 28, 8.96, 'N');
f_add_change_contract( 'HOC017-11', 'C', 'WARF03', 'C', 'TIMC', 'TEVA', 'TPW', '2011.07.17', '2013.07.16', 'HKD', 28, 9.52, 'N');
f_add_change_contract( 'SQ11-100-01', 'S', 'ZOLM01', 'Q', 'ZUEL', 'ASTZ', 'IPR', '2011.10.28', '2012.10.27', 'HKD', 6, 238.0, 'N');
f_add_change_contract( 'SQ11-100-01', '', 'ZOLM01', 'Q', 'ZUEL', 'ASTZ', 'IPR', '2011.10.28', '2012.10.27', 'HKD', 42, 1428.0, 'N');
f_add_change_contract( 'HOC160-11', 'A', 'ZOLP01', 'C', 'LFHC', 'SA', 'SW', '2012.01.01', '2013.12.31', 'HKD', 20, 16.6, 'N');
f_add_change_contract( 'HOC320-10', 'A', 'ZOPI01', 'C', 'TBOM', 'PHAA', 'PHAA', '2011.05.20', '2013.05.19', 'HKD', 30, 5.97, 'N');
f_add_change_contract( 'SQ11-088-01', '', 'FEMO01', 'Q', 'ZUEL', 'SOLV', 'SOBI', '2011.10.16', '2012.10.15', 'HKD', 1, 99.8, 'N');
f_add_change_contract( 'SQ11-353-01', '', 'MARV01', 'Q', 'DKSH', 'ORGA', 'ORGA', '2012.03.27', '2013.03.26', 'HKD', 1, 8.5, 'N');
f_add_change_contract( 'SQ10-503-01', '', 'PREM04', 'Q', 'ZUEL', 'WYET', 'WM', '2011.08.28', '2012.08.27', 'HKD', 1, 87.0, 'N');
f_add_change_contract( 'HOC333-10', 'B', 'CARB51', 'C', 'ZUEL', 'NOVA', 'NOVF', '2011.03.24', '2013.03.23', 'HKD', 50, 59.5, 'N');
f_add_change_contract( 'HOC189-11', 'A', 'ISOS10', 'C', 'ZUEL', 'ASTZ', 'ASTP', '2012.03.14', '2014.03.13', 'HKD', 30, 15.88, 'N');
f_add_change_contract( 'HOC210-11', 'B', 'METO10', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2012.03.19', '2014.03.18', 'HKD', 300, 488.4, 'N');
f_add_change_contract( 'HOC210-11', 'A', 'METO13', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2012.03.19', '2014.03.18', 'HKD', 300, 243.0, 'N');
f_add_change_contract( 'SQ11-072-01', '', 'METO16', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.07', '2012.10.06', 'HKD', 14, 10.8, 'N');
f_add_change_contract( 'SQ11-258-01', '', 'OXPE02', 'Q', 'LFHC', 'SA', 'SA', '2012.02.23', '2013.02.22', 'HKD', 100, 102.25, 'N');
f_add_change_contract( 'HOC091-11', 'A', 'PARO03', 'C', 'ZUEL', 'GSK', 'GSK', '2011.10.05', '2013.10.04', 'HKD', 30, 90.0, 'N');
f_add_change_contract( 'HOC091-11', 'B', 'PARO04', 'C', 'ZUEL', 'GSK', 'GSK', '2011.10.05', '2013.10.04', 'HKD', 30, 180.0, 'N');
f_add_change_contract( 'HOC246-10', 'E1', 'VALP05', 'C', 'LFHC', 'SA', 'SANO', '2011.07.01', '2013.01.19', 'HKD', 100, 171.0, 'N');
f_add_change_contract( 'HOC246-10', 'C1', 'VALP06', 'C', 'LFHC', 'SA', 'SANO', '2011.07.01', '2013.01.19', 'HKD', 100, 68.0, 'N');
f_add_change_contract( 'HOC246-10', 'D1', 'VALP07', 'C', 'LFHC', 'SA', 'SANO', '2011.07.01', '2013.01.19', 'HKD', 100, 101.0, 'N');
f_add_change_contract( '8011015821A', '', 'NITR05', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 7.8, 'N');
f_add_change_contract( 'HOC036-11', 'B1', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 6.55, 'N');
f_add_change_contract( 'HOC047-11', 'B1', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 6.55, 'N');
f_add_change_contract( 'HOC036-11', 'C', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 6.75, 'N');
f_add_change_contract( 'HOC047-11', 'C', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 6.75, 'N');
f_add_change_contract( 'HOC036-11', 'D', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.05.30', 'HKD', 1, 7.0, 'N');
f_add_change_contract( 'HOC047-11', 'D', 'OXYG01', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.06.30', 'HKD', 1, 7.0, 'N');
f_add_change_contract( 'HOC292-10', 'A', 'SURV01', 'C', 'ZUEL', 'ABBO', 'ABBO', '2011.04.12', '2013.04.11', 'HKD', 1, 5103.0, 'N');
f_add_change_contract( 'SQ10-431-01', '', 'CLOT05', 'Q', 'DKSH', 'SP', 'SP', '2011.07.14', '2012.07.13', 'HKD', 1, 18.2, 'N');
f_add_change_contract( 'HOC238-11', 'A', 'BUDE05', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2012.06.12', '2014.06.11', 'HKD', 1, 295.0, 'N');
f_add_change_contract( 'HOC127-10', 'A1', 'BUDE15', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2010.09.19', '2012.09.18', 'HKD', 1, 172.0, 'N');
f_add_change_contract( 'HOC127-10', 'A2', 'BUDE15', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.01', '2012.09.18', 'HKD', 142, 20640.0, 'N');
f_add_change_contract( 'SQ11-024-01', 'S', 'BUDE20', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.18', '2012.07.17', 'HKD', 1, 96.0, 'N');
f_add_change_contract( 'SQ11-024-01', '', 'BUDE20', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.18', '2012.07.17', 'HKD', 150, 11520.0, 'N');
f_add_change_contract( 'HOC127-10', 'B1', 'BUDE21', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2010.09.19', '2012.09.18', 'HKD', 1, 310.0, 'N');
f_add_change_contract( 'HOC127-10', 'B2', 'BUDE21', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.01', '2012.09.18', 'HKD', 155, 37200.0, 'N');
f_add_change_contract( 'SQ11-334-01', '', 'TERB11', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2012.04.06', '2013.04.05', 'HKD', 1, 120.0, 'N');
f_add_change_contract( 'HOC233-11', 'A', 'DIPH31', 'C', 'ZUEL', 'SANF', 'SANF', '2012.05.01', '2014.04.30', 'HKD', 1, 56.0, 'N');
f_add_change_contract( 'HOC199-11', 'A', 'MEAS03', 'C', 'DKSH', 'MSD', 'MER', '2012.04.10', '2014.04.09', 'HKD', 1, 35.8, 'N');
f_add_change_contract( 'HOC230-10', 'B', 'HEPA12', 'C', 'ZUEL', 'GSK', 'GB', '2011.03.01', '2013.02.28', 'HKD', 1, 31.7, 'N');
f_add_change_contract( 'HOC230-10', 'A', 'HEPA32', 'C', 'ZUEL', 'GSK', 'GB', '2011.03.01', '2013.02.28', 'HKD', 25, 470.0, 'N');
f_add_change_contract( 'HOC066-10', 'A', 'ALCO04', 'C', 'KHH', 'XXX', 'CSR', '2010.09.10', '2012.09.09', 'HKD', 20000, 350.0, 'N');
f_add_change_contract( 'HOC066-10', 'A', 'ALCO04', 'C', 'KHH', 'GGST', 'GGST', '2010.09.10', '2012.09.09', 'HKD', 20000, 350.0, 'N');
f_add_change_contract( 'HOC013-11', 'A', 'ALCO15', 'C', 'LCH', 'PROF', 'PROF', '2011.09.01', '2013.08.31', 'HKD', 4000, 231.6, 'N');
f_add_change_contract( 'HOC066-10', 'B', 'ALCO24', 'C', 'KHH', 'GGST', 'GGST', '2010.09.10', '2012.09.09', 'HKD', 10000, 196.0, 'N');
f_add_change_contract( 'HOC066-10', 'C', 'ALCO25', 'C', 'KHH', 'GGST', 'GGST', '2010.09.10', '2012.09.09', 'HKD', 10000, 209.0, 'N');
f_add_change_contract( 'HOC066-10', 'D', 'ALCO26', 'C', 'KHH', 'GGST', 'GGST', '2010.09.10', '2012.09.09', 'HKD', 20, 280.0, 'N');
f_add_change_contract( 'HOC169-11', 'A', 'ALCO27', 'C', 'NP', 'GNP', 'GNP', '2012.01.11', '2014.01.10', 'HKD', 20, 86.0, 'N');
f_add_change_contract( 'HOC169-11', 'B', 'ALCO28', 'C', 'NP', 'GNP', 'GNP', '2012.01.11', '2014.01.10', 'HKD', 10, 145.0, 'N');
f_add_change_contract( 'HOC169-11', 'D', 'ALCO29', 'C', 'NP', 'GNP', 'GNP', '2012.01.11', '2014.01.10', 'HKD', 20, 86.0, 'N');
f_add_change_contract( 'HOC169-11', 'E', 'ALCO30', 'C', 'NP', 'GNP', 'GNP', '2012.01.11', '2014.01.10', 'HKD', 10, 145.0, 'N');
f_add_change_contract( 'HOC169-11', 'C', 'ALCO31', 'C', 'NP', 'GNP', 'GNP', '2012.01.11', '2014.01.10', 'HKD', 10, 145.0, 'N');
f_add_change_contract( '8011016618', 'H', 'ALCO34', 'C', 'FMCL', 'VICK', 'VICK', '2010.08.13', '2022.08.12', 'HKD', 1, 51.5, 'N');
f_add_change_contract( '8011016619', 'I', 'ALCO34', 'C', 'GAMB', 'VICK', 'VICK', '2010.08.13', '2022.08.12', 'HKD', 1, 60.0, 'N');
f_add_change_contract( '8011016619', 'G', 'ALCO35', 'C', 'GAMB', 'PROF', 'PROF', '2010.08.13', '2022.08.12', 'HKD', 200, 32.0, 'N');
f_add_change_contract( '8011016618', 'E', 'ALCO35', 'C', 'FMCL', 'PROF', 'PROF', '2010.08.13', '2022.08.12', 'HKD', 200, 35.0, 'N');
f_add_change_contract( '8011015821A', '', 'DELI02', 'C', 'SGL', 'SGL', 'SGL', '2010.07.02', '2013.07.01', 'HKD', 1, 10.0, 'N');
f_add_change_contract( '8011015822A', '', 'DELIA1', 'C', 'HKOA', 'HKOA', 'HKOA', '2010.07.02', '2013.07.01', 'HKD', 1, 45.0, 'N');
f_add_change_contract( '8011026778', 'A', 'DELIA2', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2013.07.01', 'HKD', 1, 22.0, 'N');
f_add_change_contract( '8011015821A', '', 'DELIA4', 'C', 'SGL', 'XXX', 'SGSI', '2010.07.02', '2013.07.01', 'HKD', 1, 28.0, 'N');
f_add_change_contract( '8011015821A', '', 'DELIA7', 'C', 'SGL', 'SGSI', 'SGSI', '2010.07.02', '2013.07.01', 'HKD', 1, 25.0, 'N');
f_add_change_contract( '8011015824A', '', 'DELIC1', 'C', 'HSGC', 'HSGC', 'HSGC', '2010.07.02', '2013.07.01', 'HKD', 1, 10.0, 'N');
f_add_change_contract( '8011015824A', '', 'DELIC2', 'C', 'HSGC', 'HKSG', 'HKSG', '2010.07.02', '2013.07.01', 'HKD', 1, 10.0, 'N');
f_add_change_contract( '8011015823A', '', 'DELID3', 'C', 'AIR', 'AIR', 'AIR', '2010.07.02', '2013.07.01', 'HKD', 1, 25.0, 'N');
f_add_change_contract( '8011015823A', '', 'DELID4', 'C', 'AIR', 'AIR', 'AIR', '2010.07.02', '2013.07.01', 'HKD', 1, 45.0, 'N');
f_add_change_contract( '8011015821A', '', 'DELIE3', 'C', 'SGL', 'XXX', 'SGL', '2010.07.02', '2013.07.01', 'HKD', 1, 30.0, 'N');
f_add_change_contract( 'SQ11-019-01', '', 'GLYC52', 'Q', 'JEAN', 'JEAN', 'JEAN', '2011.07.16', '2012.07.15', 'HKD', 1, 37.2, 'N');
f_add_change_contract( 'SQ11-104-01', '', 'LEVO13', 'Q', 'ZUEL', 'BH', 'BSPO', '2011.08.28', '2012.08.27', 'HKD', 1, 1140.0, 'N');
f_add_change_contract( 'SQ10-515-01', '', 'POVI04', 'Q', 'LCH', 'ECOL', 'ECOL', '2011.04.21', '2012.04.20', 'HKD', 500, 17.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'RENT03', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 900.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'RENT03', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 950.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'RENT03', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 1000.0, 'N');
f_add_change_contract( '8011015821A', '', 'RENT09', 'C', 'SGL', 'XXX', 'SGL', '2010.07.02', '2013.07.01', 'HKD', 1, 28.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'RENT13', 'C', 'HKOA', 'HKOA', 'HKOA', '2012.07.01', '2013.06.30', 'HKD', 1, 78.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'RENT13', 'C', 'HKOA', 'HKOA', 'HKOA', '2013.07.01', '2014.06.30', 'HKD', 1, 82.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'RENT16', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 36.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'RENT16', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 38.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'RENT16', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 40.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 46.0, 'N');
f_add_change_contract( 'HOC036-11', 'B1', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 47.0, 'N');
f_add_change_contract( 'HOC047-11', 'B1', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 47.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 49.0, 'N');
f_add_change_contract( 'HOC036-11', 'C', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 49.0, 'N');
f_add_change_contract( 'HOC047-11', 'C', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 49.0, 'N');
f_add_change_contract( 'HOC036-11', 'D', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.05.30', 'HKD', 1, 51.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 51.0, 'N');
f_add_change_contract( 'HOC047-11', 'D', 'RENT17', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.06.30', 'HKD', 1, 51.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'RENT18', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 74.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'RENT18', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 78.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'RENT18', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 82.0, 'N');
f_add_change_contract( '8011026778', 'A', 'RENTA1', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2013.07.01', 'HKD', 1, 50.0, 'N');
f_add_change_contract( '8011015824A', '', 'RENTC1', 'C', 'HSGC', 'HSGC', 'HSGC', '2010.07.02', '2013.07.01', 'HKD', 1, 30.0, 'N');
f_add_change_contract( '8011015823A', '', 'RENTD1', 'C', 'AIR', 'AIR', 'AIR', '2010.07.02', '2013.07.01', 'HKD', 1, 25.0, 'N');
f_add_change_contract( 'SQ11-061-01', '', 'SILV02', 'Q', 'WH', 'HALE', 'HALE', '2011.07.18', '2012.07.17', 'HKD', 12, 441.0, 'N');
f_add_change_contract( 'SQ11-230-01', '', 'TRIS02', 'Q', 'RHL', 'TRIS', 'TRIS', '2011.11.04', '2012.11.03', 'HKD', 50, 1725.0, 'N');
f_add_change_contract( 'HOC135-10', 'A', 'WATE03', 'C', 'DKSH', 'BB', 'BB', '2010.11.01', '2012.10.31', 'HKD', 20, 12.5, 'N');
f_add_change_contract( 'SQ11-202-01', '', 'WATE10', 'Q', 'LCH', 'OTSU', 'OTSU', '2012.01.10', '2013.01.09', 'HKD', 40, 320.0, 'N');
f_add_change_contract( 'HOC217-10', 'C', 'WATE11', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.02.20', '2013.02.19', 'HKD', 10, 77.5, 'N');
f_add_change_contract( 'HOC214-10', 'D', 'WATE14', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 50, 220.0, 'N');

commit;

end;
/



set scan on
set serveroutput on

