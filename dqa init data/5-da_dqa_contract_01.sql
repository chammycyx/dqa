set scan off
set serveroutput on

declare

	procedure f_add_change_contract(
		ls_contract_num in varchar2,
		ls_contract_suffix in varchar2,
		ls_item_code in varchar2,
		ls_contract_type in varchar2,
		ls_supplier_code in varchar2,
		ls_pharm_company_code in varchar2,
		ls_manuf_code in varchar2,
		ls_contract_start in varchar2,
		ls_contract_end in varchar2,
		ls_currency_code in varchar2,
		ll_pack_size in number,
		ll_pack_price in number,
		ls_suspend in varchar2)
	is
  		ll_contract_id	number;
  		
	begin
			
		select contract_id
		into   ll_contract_id
		from   contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
	
		update contract
		set    supplier_code = ls_supplier_code,
			   pharm_company_code = ls_pharm_company_code,
			   manuf_code = ls_manuf_code,
			   start_date = to_date(ls_contract_start, 'yyyy.mm.dd'),
			   end_date = to_date(ls_contract_end, 'yyyy.mm.dd'),
			   currency_code = ls_currency_code,
			   pack_size = ll_pack_size,
			   pack_price = ll_pack_price,
			   suspend_flag = ls_suspend,
			   modify_date = sysdate,
			   modify_user = 'dqaadmin'
		where  contract_id = ll_contract_id;
		
	exception when NO_DATA_FOUND then
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
	
	when TOO_MANY_ROWS then
	
		delete from contract
		where  contract_num = ls_contract_num
		and    nvl(contract_suffix, '-') = decode(ls_contract_suffix, '', '-', ls_contract_suffix)
		and    contract_type = ls_contract_type
		and    item_code = ls_item_code
		and    module_type = 'A';
		
		dbms_output.put_line('Duplicate row found! ' || ls_contract_num || ', ' || ls_contract_suffix || ', ' || ls_contract_type || ', ' || ls_item_code);
		
		insert into contract (
			contract_id, contract_num, contract_suffix, item_code, contract_type, start_date, end_date, pack_size,
			pack_price, currency_code, supplier_code, manuf_code, pharm_company_code, suspend_flag, module_type, upload_date,
			create_user, create_date, modify_user, modify_date, version
		) values ( 
			seq_contract.nextval, ls_contract_num, ls_contract_suffix, ls_item_code, ls_contract_type, TO_DATE(ls_contract_start, 'yyyy.mm.dd'), TO_DATE(ls_contract_end, 'yyyy.mm.dd'), ll_pack_size,
			ll_pack_price, ls_currency_code, ls_supplier_code, ls_manuf_code, ls_pharm_company_code, ls_suspend, 'A', sysdate,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
		
	end;
	
begin

f_add_change_contract( 'SQ11-060-01', '', 'GENT06', 'Q', 'DKSH', 'BMET', 'EMCM', '2011.07.14', '2012.07.13', 'HKD', 1, 1398.0, 'N');
f_add_change_contract( 'DQ11-112-01', '', '5FLU05', 'C', 'HW', 'VPNA', 'LEGP', '2012.04.03', '2012.04.13', 'HKD', 100, 73000.0, 'N');
f_add_change_contract( 'HOC044-11', 'A', 'ACIT01', 'C', 'ZUEL', 'ROCE', 'PATH', '2011.10.01', '2013.09.30', 'HKD', 30, 330.0, 'N');
f_add_change_contract( 'HOC044-11', 'B', 'ACIT02', 'C', 'ZUEL', 'ROCE', 'PATH', '2011.10.01', '2013.09.30', 'HKD', 30, 690.0, 'N');
f_add_change_contract( 'SQ11-174-01', '', 'ALFA01', 'Q', 'TIMC', 'TEVP', 'TEVP', '2011.12.04', '2012.12.03', 'HKD', 30, 14.7, 'N');
f_add_change_contract( 'SQ11-212-01', '', 'ALFA02', 'Q', 'TIMC', 'TEVA', 'TEVP', '2011.09.10', '2012.12.03', 'HKD', 30, 48.0, 'N');
f_add_change_contract( 'SQ11-034-01', '', 'ALFA04', 'Q', 'DKSH', 'LEO', 'LEO', '2011.07.15', '2012.11.03', 'HKD', 100, 258.0, 'N');
f_add_change_contract( 'SQ11-175-01', '', 'ALPH09', 'Q', 'LFAU', 'JAMI', 'JAMI', '2011.12.08', '2012.12.07', 'HKD', 720, 318.0, 'N');
f_add_change_contract( 'HOC084-10', 'A', 'AMOX01', 'C', 'ZUEL', 'BFP', 'BFP', '2010.11.30', '2012.11.29', 'HKD', 500, 50.0, 'N');
f_add_change_contract( 'HOC148-10', 'A', 'AMPI01', 'C', 'ZUEL', 'APT', 'VICK', '2011.01.26', '2013.01.25', 'HKD', 500, 119.0, 'N');
f_add_change_contract( 'HOC148-10', 'B', 'AMPI02', 'C', 'ZUEL', 'APT', 'VICK', '2011.01.26', '2013.01.25', 'HKD', 500, 199.0, 'N');
f_add_change_contract( 'HOC019-11', 'A1', 'ANAG02', 'C', 'LFHC', 'SHIR', 'MALL', '2011.07.01', '2013.05.07', 'HKD', 100, 1396.0, 'N');
f_add_change_contract( 'HOC068-11', 'A1', 'APRE01', 'C', 'DKSH', 'MSD', 'MSD', '2011.10.06', '2013.07.25', 'HKD', 1, 592.0, 'N');
f_add_change_contract( 'HOC119-11', 'A', 'ATAZ02', 'C', 'DKSH', 'BMS', 'BMS', '2011.10.15', '2013.10.14', 'HKD', 60, 3550.0, 'N');
f_add_change_contract( 'HOC119-11', 'B', 'ATAZ03', 'C', 'DKSH', 'BMS', 'BMS', '2011.10.15', '2013.10.14', 'HKD', 30, 3069.0, 'N');
f_add_change_contract( 'HOC104-10', 'A', 'ATOM02', 'C', 'YCW', 'EL', 'LILY', '2010.10.07', '2012.10.06', 'HKD', 308, 6500.0, 'N');
f_add_change_contract( 'HOC104-10', 'B', 'ATOM03', 'C', 'YCW', 'EL', 'LILY', '2010.10.07', '2012.10.06', 'HKD', 308, 6500.0, 'N');
f_add_change_contract( 'HOC104-10', 'C', 'ATOM04', 'C', 'YCW', 'EL', 'LILY', '2010.10.07', '2012.10.06', 'HKD', 308, 6500.0, 'N');
f_add_change_contract( 'HOC104-10', 'D', 'ATOM05', 'C', 'YCW', 'EL', 'LILY', '2010.10.07', '2012.10.06', 'HKD', 308, 6500.0, 'N');
f_add_change_contract( 'SQ10-355-01', '', 'BUDE14', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.05.28', '2012.05.27', 'HKD', 50, 529.0, 'N');
f_add_change_contract( 'SQ11-396-01', '', 'BUDE14', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2012.05.28', '2013.05.27', 'HKD', 50, 566.0, 'N');
f_add_change_contract( 'HOC209-10', 'A', 'CALC03', 'C', 'TIMC', 'TEVA', 'RPST', '2011.02.16', '2013.02.15', 'HKD', 100, 55.4, 'N');
f_add_change_contract( 'HOC209-10', 'B', 'CALC04', 'C', 'TIMC', 'TEVA', 'RPS', '2011.02.16', '2013.02.15', 'HKD', 100, 110.8, 'N');
f_add_change_contract( 'SQ10-467-01', '', 'CEFT13', 'Q', 'DKSH', 'SP', 'SIFI', '2011.07.19', '2012.07.18', 'HKD', 6, 153.0, 'N');
f_add_change_contract( 'SQ11-136-01', '', 'CLIN01', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2011.11.24', '2012.11.23', 'HKD', 100, 260.0, 'N');
f_add_change_contract( 'HOC088-10', 'A', 'CLOX01', 'C', 'VICK', 'VICK', 'VICK', '2010.12.08', '2012.12.07', 'HKD', 500, 149.5, 'N');
f_add_change_contract( 'HOC088-10', 'B', 'CLOX02', 'C', 'VICK', 'VICK', 'VICK', '2010.12.08', '2012.12.07', 'HKD', 500, 189.0, 'N');
f_add_change_contract( 'HOC098-10', 'B', 'CYCL20', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.12.19', '2012.12.18', 'HKD', 750, 2550.0, 'N');
f_add_change_contract( 'HOC098-10', 'C', 'CYCL21', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.12.19', '2012.12.18', 'HKD', 750, 9960.0, 'N');
f_add_change_contract( 'HOC086-10', 'A', 'CYCL27', 'C', 'ZUEL', 'NOVA', 'RPS', '2010.10.01', '2012.09.30', 'HKD', 900, 1530.0, 'N');
f_add_change_contract( 'HOC099-10', 'B', 'CYCL28', 'C', 'TIMC', 'TEVA', 'TEVA', '2010.12.19', '2012.12.18', 'HKD', 50, 94.5, 'N');
f_add_change_contract( 'HOC099-10', 'C', 'CYCL29', 'C', 'TIMC', 'TEVA', 'TEVA', '2010.12.19', '2012.12.18', 'HKD', 50, 347.5, 'N');
f_add_change_contract( 'SQ11-276-01', '', 'DANA02', 'Q', 'LFHC', 'SA', 'SANO', '2012.03.15', '2013.03.14', 'HKD', 100, 375.0, 'N');
f_add_change_contract( 'HOC150-11', 'A', 'DIDA05', 'C', 'DKSH', 'BMS', 'BMS', '2011.12.20', '2012.12.19', 'HKD', 30, 1620.0, 'N');
f_add_change_contract( 'HOC150-11', 'B', 'DIDA06', 'C', 'DKSH', 'BMS', 'BMS', '2011.12.20', '2012.12.19', 'HKD', 30, 2195.0, 'N');
f_add_change_contract( 'HOC275-10', 'A', 'DOTH01', 'C', 'VICK', 'VICK', 'VICK', '2011.05.23', '2013.05.22', 'HKD', 500, 274.0, 'N');
f_add_change_contract( 'SQ11-278-01', '', 'DOXE02', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2012.04.01', '2013.03.31', 'HKD', 100, 142.0, 'N');
f_add_change_contract( 'HOC332-10', 'A1', 'DULO01', 'C', 'YCW', 'EL', 'LILS', '2011.10.14', '2013.03.16', 'HKD', 168, 1015.0, 'N');
f_add_change_contract( 'HOC332-10', 'B1', 'DULO02', 'C', 'YCW', 'EL', 'LILS', '2011.10.14', '2013.03.16', 'HKD', 168, 1512.0, 'N');
f_add_change_contract( 'SQ11-045-01', '', 'ESSE04', 'Q', 'LFHC', 'SA', 'NATT', '2011.10.14', '2012.10.13', 'HKD', 85, 145.0, 'N');
f_add_change_contract( 'SQ11-251-01', '', 'FENO05', 'Q', 'ZUEL', 'SOLV', 'FOUR', '2012.03.25', '2013.03.24', 'HKD', 30, 112.0, 'N');
f_add_change_contract( 'SQ11-143-01', '', 'FLUN01', 'Q', 'DKSH', 'JC', 'JC', '2011.12.01', '2012.11.30', 'HKD', 50, 235.0, 'N');
f_add_change_contract( 'HOC175-10', 'A', 'FLUO18', 'C', 'ZUEL', 'APT', 'APT', '2011.01.13', '2013.01.12', 'HKD', 500, 109.0, 'N');
f_add_change_contract( 'HOC153-11', 'A', 'FLUO34', 'C', 'HW', 'APOT', 'APOT', '2012.04.02', '2013.10.01', 'HKD', 100, 20.6, 'N');
f_add_change_contract( 'SQ11-028-01', 'S', 'FLUV02', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.14', '2012.07.13', 'HKD', 28, 85.0, 'N');
f_add_change_contract( 'SQ11-028-01', '', 'FLUV02', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.14', '2012.07.13', 'HKD', 3360, 5695.0, 'N');
f_add_change_contract( 'SQ11-029-01', 'S', 'FLUV03', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.14', '2012.07.13', 'HKD', 28, 100.0, 'N');
f_add_change_contract( 'SQ11-029-01', '', 'FLUV03', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2011.07.14', '2012.07.13', 'HKD', 3360, 6700.0, 'N');
f_add_change_contract( 'HOC296-10', 'A', 'GABA01', 'C', 'ZUEL', 'PFIZ', 'GODE', '2011.02.18', '2013.02.17', 'HKD', 100, 325.0, 'N');
f_add_change_contract( 'HOC296-10', 'B', 'GABA02', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.02.18', '2013.02.17', 'HKD', 100, 680.0, 'N');
f_add_change_contract( 'HOC296-10', 'C', 'GABA03', 'C', 'ZUEL', 'PFIZ', 'GODE', '2011.02.18', '2013.02.17', 'HKD', 100, 780.0, 'N');
f_add_change_contract( 'HOC129-11', 'A', 'GABA07', 'C', 'TIMC', 'TEVA', 'TEVA', '2011.10.01', '2012.09.30', 'HKD', 50, 54.8, 'N');
f_add_change_contract( 'HOC129-11', 'B', 'GABA08', 'C', 'TIMC', 'TEVA', 'TEVA', '2011.10.01', '2012.09.30', 'HKD', 50, 76.6, 'N');
f_add_change_contract( 'HOC129-11', 'C', 'GABA09', 'C', 'TIMC', 'TEVA', 'TEVA', '2011.10.01', '2012.09.30', 'HKD', 50, 101.9, 'N');
f_add_change_contract( 'HOC124-10', 'A', 'GEMF01', 'C', 'ZUEL', 'MERC', 'APT', '2010.12.19', '2012.12.18', 'HKD', 500, 147.0, 'N');
f_add_change_contract( 'HOC149-10', 'A1', 'HYDR24', 'C', 'HW', 'APOT', 'APOT', '2011.02.21', '2013.02.20', 'HKD', 100, 250.0, 'N');
f_add_change_contract( 'HOC149-10', 'A', 'HYDR24', 'C', 'HW', 'APOT', 'APOT', '2011.02.21', '2013.02.20', 'HKD', 3700, 9000.0, 'N');
f_add_change_contract( 'HOC118-11', 'A', 'IMAT02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.10.16', '2013.10.15', 'HKD', 120, 18408.0, 'N');
f_add_change_contract( 'SQ11-204-01', '', 'INDI02', 'Q', 'DKSH', 'MER', 'MSD', '2012.01.21', '2013.01.20', 'HKD', 180, 2940.0, 'N');
f_add_change_contract( 'SQ11-218-01', '', 'ISOT03', 'Q', 'UI', 'DOUG', 'SCA', '2012.01.06', '2013.01.05', 'HKD', 60, 730.0, 'N');
f_add_change_contract( 'HOC104-11', 'A', 'ITRA01', 'C', 'VICK', 'VICK', 'VICK', '2011.11.11', '2013.11.10', 'HKD', 350, 1330.0, 'N');
f_add_change_contract( 'SQ11-209-01', '', 'LENA01', 'Q', 'CELL', 'CELG', 'PENN', '2011.09.15', '2012.09.14', 'HKD', 63, 51206.0, 'N');
f_add_change_contract( 'SQ11-210-01', '', 'LENA02', 'Q', 'CELL', 'CELG', 'PENN', '2011.09.15', '2012.09.14', 'HKD', 63, 53230.0, 'N');
f_add_change_contract( 'SQ11-208-01', '', 'LENA04', 'Q', 'CELL', 'CELG', 'PENN', '2011.09.15', '2012.09.14', 'HKD', 63, 45779.0, 'N');
f_add_change_contract( 'HOC239-11', 'A', 'MADO04', 'C', 'DKSH', 'ROCE', 'ROCM', '2012.03.01', '2014.02.28', 'HKD', 100, 130.0, 'N');
f_add_change_contract( 'SQ11-167-01', '', 'MEFE01', 'Q', 'ZUEL', 'XXX', 'PFIZ', '2011.12.19', '2012.12.18', 'HKD', 500, 176.0, 'N');
f_add_change_contract( 'SQ10-316-01', '', 'METY01', 'Q', 'ZUEL', 'NOVA', 'RPS', '2011.04.23', '2012.04.22', 'HKD', 50, 605.0, 'N');
f_add_change_contract( 'SQ11-333-01', '', 'METY01', 'Q', 'ZUEL', 'NOVA', 'RPS', '2012.04.23', '2013.04.22', 'HKD', 50, 630.0, 'N');
f_add_change_contract( 'SQ11-368-01', '', 'MINO02', 'Q', 'HW', 'APOT', 'APOT', '2012.02.24', '2013.02.23', 'HKD', 100, 380.0, 'N');
f_add_change_contract( 'HOC062-11', 'A', 'MYCO05', 'C', 'DKSH', 'ROCE', 'ROCE', '2011.07.21', '2012.07.20', 'HKD', 100, 830.0, 'N');
f_add_change_contract( 'SQ10-330-01', '', 'NAFT02', 'Q', 'ZUEL', 'MERC', 'FAMI', '2011.04.30', '2012.04.29', 'HKD', 100, 190.0, 'N');
f_add_change_contract( 'SQ11-324-01', '', 'NAFT02', 'Q', 'ZUEL', 'MERC', 'FAMI', '2012.04.30', '2013.04.29', 'HKD', 100, 200.0, 'N');
f_add_change_contract( 'HOC138-11', 'A', 'NILO02', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.10.26', '2013.10.25', 'HKD', 224, 44226.0, 'N');
f_add_change_contract( 'SQ11-047-01', '', 'PANC06', 'Q', 'ZUEL', 'ABBO', 'ABBP', '2011.10.14', '2012.10.13', 'HKD', 100, 364.0, 'N');
f_add_change_contract( 'HOC176-10', 'A', 'PHEN31', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.10.22', '2012.10.21', 'HKD', 200, 275.0, 'N');
f_add_change_contract( 'SQ11-391-01', '', 'PIRO01', 'Q', 'ZUEL', 'AUL', 'AUL', '2012.04.11', '2013.04.10', 'HKD', 1000, 243.0, 'N');
f_add_change_contract( 'HOC087-11', 'A', 'PREG15', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.08.01', '2013.07.31', 'HKD', 56, 395.0, 'N');
f_add_change_contract( 'HOC087-11', 'B', 'PREG16', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.08.01', '2013.07.31', 'HKD', 56, 490.0, 'N');
f_add_change_contract( 'HOC087-11', 'C', 'PREG17', 'C', 'ZUEL', 'PFIZ', 'PMDG', '2011.08.01', '2013.07.31', 'HKD', 56, 780.0, 'N');
f_add_change_contract( 'HOC150-10', 'A', 'RIFA01', 'C', 'VICK', 'VICK', 'VICK', '2010.12.04', '2012.12.03', 'HKD', 500, 175.0, 'N');
f_add_change_contract( 'HOC150-10', 'B', 'RIFA02', 'C', 'VICK', 'VICK', 'VICK', '2010.12.04', '2012.12.03', 'HKD', 500, 340.0, 'N');
f_add_change_contract( 'SQ10-354-01', '', 'RIFA10', 'Q', 'ZUEL', 'PFIZ', 'PFIS', '2011.05.21', '2012.05.20', 'HKD', 30, 2300.0, 'N');
f_add_change_contract( 'HOC219-11', 'A', 'RITO06', 'C', 'ZUEL', 'ABBO', 'CATA', '2012.03.19', '2013.03.18', 'HKD', 120, 1285.0, 'N');
f_add_change_contract( 'HOC323-10', 'A1', 'RIVA01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.08.16', '2013.06.30', 'HKD', 28, 231.0, 'N');
f_add_change_contract( 'HOC323-10', 'A2', 'RIVA01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.08.16', '2013.06.30', 'HKD', 1848, 13860.0, 'N');
f_add_change_contract( 'HOC323-10', 'B1', 'RIVA02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.08.16', '2013.06.30', 'HKD', 28, 231.0, 'N');
f_add_change_contract( 'HOC323-10', 'B2', 'RIVA02', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.08.16', '2013.06.30', 'HKD', 1848, 13860.0, 'N');
f_add_change_contract( 'HOC323-10', 'C1', 'RIVA03', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.01', '2013.06.30', 'HKD', 28, 231.0, 'N');
f_add_change_contract( 'HOC323-10', 'C', 'RIVA03', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.01', '2013.06.30', 'HKD', 1848, 13860.0, 'N');
f_add_change_contract( 'HOC323-10', 'D1', 'RIVA04', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.01', '2013.06.30', 'HKD', 28, 231.0, 'N');
f_add_change_contract( 'HOC323-10', 'D', 'RIVA04', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.07.01', '2013.06.30', 'HKD', 1848, 13860.0, 'N');
f_add_change_contract( 'HOC114-11', 'A', 'STAV01', 'C', 'DKSH', 'BMS', 'EMCU', '2011.10.12', '2012.10.11', 'HKD', 60, 2125.0, 'N');
f_add_change_contract( 'HOC114-11', 'B', 'STAV02', 'C', 'DKSH', 'BMS', 'EMCU', '2011.10.12', '2012.10.11', 'HKD', 60, 2228.0, 'N');
f_add_change_contract( 'HOC241-10', 'A', 'SULP19', 'C', 'VICK', 'VICK', 'VICK', '2011.03.26', '2013.03.25', 'HKD', 500, 84.0, 'N');
f_add_change_contract( 'HOC023-10', 'A1', 'TEMO01', 'C', 'DKSH', 'SP', 'OP', '2011.01.01', '2012.07.31', 'HKD', 5, 4240.0, 'N');
f_add_change_contract( 'HOC023-10', 'B1', 'TEMO02', 'C', 'DKSH', 'SP', 'OP', '2011.01.01', '2012.07.31', 'HKD', 5, 928.0, 'N');
f_add_change_contract( 'HOC207-10', 'A1', 'TEST03', 'C', 'DKSH', 'ORGA', 'ORGN', '2011.01.01', '2012.11.30', 'HKD', 60, 235.0, 'N');
f_add_change_contract( 'HOC108-11', 'A', 'TRAM01', 'C', 'HKMS', 'STAD', 'STAD', '2012.01.29', '2014.01.28', 'HKD', 50, 12.71, 'N');
f_add_change_contract( 'HOC161-10', 'A', 'TRAN01', 'C', 'ZUEL', 'APT', 'APT', '2011.01.01', '2012.12.31', 'HKD', 500, 149.0, 'N');
f_add_change_contract( 'HOC243-11', 'A', 'TRET09', 'C', 'DKSH', 'ROCE', 'RPS', '2012.02.22', '2014.02.21', 'HKD', 100, 3600.0, 'N');
f_add_change_contract( 'HOC294-10', 'A1', 'TROP08', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.03.08', '2013.03.07', 'HKD', 5, 950.0, 'N');
f_add_change_contract( 'HOC294-10', 'A', 'TROP08', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.03.08', '2013.03.07', 'HKD', 150, 9500.0, 'N');
f_add_change_contract( 'HOC054-11', 'A', 'URSO01', 'C', 'JML', 'FALK', 'FALK', '2011.09.08', '2012.09.07', 'HKD', 100, 620.0, 'N');
f_add_change_contract( 'HOC037-11', 'A', 'ZIDO01', 'C', 'VICK', 'VICK', 'VICK', '2011.07.15', '2012.07.14', 'HKD', 100, 480.0, 'N');
f_add_change_contract( 'HOC216-11', 'A', 'ZIDO04', 'C', 'VICK', 'VICK', 'VICK', '2012.04.15', '2013.04.14', 'HKD', 100, 930.0, 'N');
f_add_change_contract( 'HOC205-10', 'A', 'ZIPR01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.11.27', '2012.11.26', 'HKD', 120, 808.0, 'N');
f_add_change_contract( 'HOC205-10', 'B', 'ZIPR02', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.11.27', '2012.11.26', 'HKD', 60, 755.0, 'N');
f_add_change_contract( 'HOC205-10', 'C', 'ZIPR03', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.11.27', '2012.11.26', 'HKD', 60, 832.0, 'N');
f_add_change_contract( 'HOC205-10', 'D', 'ZIPR04', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.11.27', '2012.11.26', 'HKD', 60, 1068.0, 'N');
f_add_change_contract( 'HOC131-11', 'A', 'LAMO03', 'C', 'ZUEL', 'GSK', 'GSK', '2012.01.17', '2014.01.16', 'HKD', 30, 81.0, 'N');
f_add_change_contract( 'HOC131-11', 'B', 'LAMO04', 'C', 'ZUEL', 'GSK', 'GSK', '2012.01.17', '2014.01.16', 'HKD', 30, 215.0, 'N');
f_add_change_contract( 'HOC055-11', 'A', 'CALC43', 'C', 'JEAN', 'JEAN', 'JEAN', '2011.08.25', '2013.08.24', 'HKD', 500, 58.5, 'N');
f_add_change_contract( 'SQ11-297-01', '', 'FERR30', 'Q', 'HKMS', 'VIFO', 'VIFO', '2012.01.06', '2013.01.05', 'CHF', 30, 4.5, 'N');
f_add_change_contract( 'SQ10-439-01', '', 'MONT01', 'Q', 'DKSH', 'MSD', 'MSD', '2011.07.25', '2012.07.24', 'HKD', 28, 278.0, 'N');
f_add_change_contract( 'SQ10-385-01', '', 'MONT03', 'Q', 'DKSH', 'MSD', 'MSD', '2011.06.17', '2012.06.16', 'HKD', 28, 223.0, 'N');
f_add_change_contract( 'SQ11-419-01', '', 'MONT03', 'Q', 'DKSH', 'MSD', 'MSD', '2012.06.17', '2013.06.16', 'HKD', 28, 223.0, 'N');
f_add_change_contract( 'HOC295-10', 'A', 'NICO10', 'C', 'LFHC', 'JJ', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 30, 57.0, 'N');
f_add_change_contract( 'HOC295-10', 'B', 'NICO11', 'C', 'LFHC', 'JJ', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 105, 325.1, 'N');
f_add_change_contract( 'HOC295-10', 'C', 'NICO24', 'C', 'LFHC', 'JJ', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 30, 57.0, 'N');
f_add_change_contract( 'SQ10-426-01', '', 'ACYC03', 'Q', 'HKMS', 'STAD', 'STAD', '2011.07.15', '2012.07.14', 'HKD', 1, 9.0, 'N');
f_add_change_contract( 'SQ11-246-01', '', 'BENZ40', 'Q', 'ZUEL', 'SLL', 'SLL', '2012.03.29', '2013.03.28', 'HKD', 1, 35.0, 'N');
f_add_change_contract( 'SQ11-379-01', '', 'BETA04', 'Q', 'DKSH', 'SP', 'SP', '2012.02.25', '2013.02.24', 'HKD', 1, 13.0, 'N');
f_add_change_contract( 'SQ11-163-01', '', 'BETA13', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.12.16', '2012.12.15', 'HKD', 1, 12.5, 'N');
f_add_change_contract( 'SQ11-248-01', '', 'CLOB02', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.03.10', '2013.03.09', 'HKD', 1, 90.0, 'N');
f_add_change_contract( 'HOC192-11', 'A', 'CLOT04', 'C', 'ELC', 'ELC', 'ELC', '2012.03.01', '2014.02.28', 'HKD', 1, 6.7, 'N');
f_add_change_contract( 'HOC041-11', 'A', 'CROT01', 'C', 'DKSH', 'NOVT', 'NOVT', '2011.08.29', '2013.08.28', 'HKD', 1, 8.9, 'N');
f_add_change_contract( 'HOC041-11', 'B', 'CROT02', 'C', 'DKSH', 'NOVA', 'NOVT', '2011.08.29', '2013.08.28', 'HKD', 1, 13.5, 'N');
f_add_change_contract( 'SQ10-493-01', '', 'DAKT01', 'Q', 'DKSH', 'JC', 'JP', '2011.08.18', '2012.08.17', 'HKD', 1, 49.0, 'N');
f_add_change_contract( 'SQ11-164-01', '', 'DIPR01', 'Q', 'DKSH', 'SP', 'SP', '2011.12.04', '2012.12.03', 'HKD', 1, 15.0, 'N');
f_add_change_contract( 'HOC330-10', 'A', 'EMLA01', 'C', 'ZUEL', 'ASTZ', 'RECH', '2011.05.01', '2013.04.30', 'HKD', 5, 344.0, 'N');
f_add_change_contract( 'SQ11-384-01', '', 'FLUO17', 'Q', 'MEDI', 'MEDI', 'MEDI', '2012.02.25', '2013.02.24', 'HKD', 100, 148.0, 'N');
f_add_change_contract( 'SQ10-435-01', '', 'FUSI04', 'Q', 'DKSH', 'LEO', 'LEO', '2011.07.15', '2012.07.14', 'HKD', 1, 55.0, 'N');
f_add_change_contract( 'HOC130-10', 'A1', 'HEPA06', 'C', 'DKSH', 'MEDN', 'MOBI', '2012.02.09', '2012.10.03', 'HKD', 1, 5.52, 'N');
f_add_change_contract( 'SQ10-326-01', '', 'ISOC02', 'Q', 'ZUEL', 'INTM', 'INTM', '2011.04.30', '2012.04.29', 'HKD', 1, 25.5, 'N');
f_add_change_contract( 'SQ11-320-01', '', 'ISOC02', 'Q', 'ZUEL', 'INTM', 'INTM', '2012.04.30', '2013.04.29', 'HKD', 1, 28.5, 'N');
f_add_change_contract( 'SQ11-168-01', 'S', 'METH87', 'Q', 'ZUEL', 'BAYR', 'INTM', '2011.09.21', '2012.09.20', 'HKD', 1, 31.0, 'N');
f_add_change_contract( 'SQ11-168-01', '', 'METH87', 'Q', 'ZUEL', 'BAYR', 'INTM', '2011.09.21', '2012.09.20', 'HKD', 60, 1550.0, 'N');
f_add_change_contract( 'HOC111-11', 'A', 'MOME01', 'C', 'DKSH', 'SP', 'SP', '2011.09.26', '2013.09.25', 'HKD', 1, 31.0, 'N');
f_add_change_contract( 'SQ10-441-01', '', 'NERI04', 'Q', 'ZUEL', 'INTM', 'INTM', '2011.07.25', '2012.07.24', 'HKD', 1, 26.5, 'N');
f_add_change_contract( 'SQ10-497-01', '', 'TIOC01', 'Q', 'LFHC', 'JJ', 'PFIZ', '2011.08.18', '2012.08.17', 'HKD', 1, 44.0, 'N');
f_add_change_contract( 'SQ11-077-01', '', 'TRAV01', 'Q', 'ZUEL', 'INTM', 'INTM', '2011.10.20', '2012.10.19', 'HKD', 1, 22.8, 'N');
f_add_change_contract( 'SQ11-259-01', '', 'TRID01', 'Q', 'DKSH', 'SP', 'SP', '2012.02.11', '2013.02.10', 'HKD', 2, 55.0, 'N');
f_add_change_contract( 'SQ11-351-01', '', 'ULTR09', 'Q', 'ZUEL', 'INTM', 'INTM', '2012.01.06', '2013.01.05', 'HKD', 1, 57.0, 'N');
f_add_change_contract( 'SQ10-498-01', '', 'UREA06', 'Q', 'ELC', 'ELC', 'ELC', '2011.08.23', '2012.08.22', 'HKD', 1, 9.5, 'N');
f_add_change_contract( 'SQ11-327-01', '', 'ZINC18', 'Q', 'MEDI', 'MEDI', 'MEDI', '2012.01.12', '2013.01.11', 'HKD', 100, 240.0, 'N');
f_add_change_contract( '8011015823A', '', 'ACET31', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 175.0, 'N');
f_add_change_contract( '8011015823A', '', 'ACET32', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 220.0, 'N');
f_add_change_contract( '8011015823A', '', 'ACET33', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 450.0, 'N');
f_add_change_contract( '8011026778', 'A', 'AIR 01', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2013.07.01', 'HKD', 1, 80.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'AIR 04', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 196.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'AIR 04', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 207.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'AIR 04', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 218.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'AIR 05', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 75.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'AIR 05', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 79.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'AIR 05', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 83.0, 'N');
f_add_change_contract( '8011015821A', '', 'AIR 07', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 79.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'CARB09', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 135.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'CARB09', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 142.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'CARB09', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 149.0, 'N');
f_add_change_contract( '8011015821A', '', 'CARB11', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 300.0, 'N');
f_add_change_contract( '8011015824A', '', 'CARB54', 'C', 'HSGC', 'HSGC', 'HSGC', '2010.07.02', '2013.07.01', 'HKD', 1, 300.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'CARB58', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 214.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'CARB58', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 226.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'CARB58', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 238.0, 'N');
f_add_change_contract( '8011026778', 'A', 'CARB67', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2013.07.01', 'HKD', 1, 135.0, 'N');
f_add_change_contract( '8011015821A', '', 'CARB73', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 120.0, 'N');
f_add_change_contract( '8011015823A', '', 'CARB94', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 450.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'ENTO01', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 205.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'ENTO01', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 216.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'ENTO01', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 227.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'ENTO02', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 87.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'ENTO02', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 91.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'ENTO02', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 95.0, 'N');
f_add_change_contract( '8011015823A', '', 'HELI01', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 650.0, 'N');
f_add_change_contract( '8011015824A', '', 'HELI06', 'C', 'HSGC', 'HSGC', 'HSGC', '2010.07.02', '2013.07.01', 'HKD', 1, 400.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'NITR07', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 306.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'NITR07', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 323.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'NITR07', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 340.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'NITR08', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 2280.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'NITR08', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 2410.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'NITR08', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 2540.0, 'N');
f_add_change_contract( '8011015823A', '', 'NITR09', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 75.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'NITR10', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 208.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'NITR10', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 219.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'NITR10', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 230.0, 'N');
f_add_change_contract( '8011015823A', '', 'NITR11', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 75.0, 'N');
f_add_change_contract( '8011015823A', '', 'NITR17', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 75.0, 'N');
f_add_change_contract( '8011015821A', '', 'NITR23', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 180.0, 'N');
f_add_change_contract( '8011015821A', '', 'NITR24', 'C', 'SGL', 'SGSG', 'SGSG', '2010.07.02', '2013.07.01', 'HKD', 1, 350.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'NITR36', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 208.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'NITR36', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 219.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'NITR36', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 230.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG02', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 76.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG02', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 80.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG02', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 84.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG03', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 80.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG03', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 84.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG03', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 88.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG04', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 124.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG04', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 131.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG04', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 137.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG05', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 154.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG05', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 162.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG05', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 171.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 214.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 226.0, 'N');
f_add_change_contract( 'HOC036-11', 'B1', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 227.0, 'N');
f_add_change_contract( 'HOC047-11', 'B1', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.11.30', 'HKD', 1, 227.0, 'N');
f_add_change_contract( 'HOC036-11', 'C', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 233.0, 'N');
f_add_change_contract( 'HOC047-11', 'C', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2012.12.01', '2013.11.30', 'HKD', 1, 233.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 237.0, 'N');
f_add_change_contract( 'HOC036-11', 'D', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.05.30', 'HKD', 1, 240.0, 'N');
f_add_change_contract( 'HOC047-11', 'D', 'OXYG07', 'C', 'LHL', 'LHL', 'LHL', '2013.12.01', '2014.06.30', 'HKD', 1, 240.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG10', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 76.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG10', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 80.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG10', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 84.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG11', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 2020.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG11', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 2135.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG11', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 2250.0, 'N');
f_add_change_contract( '8011015823A', '', 'OXYG14', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 85.0, 'N');
f_add_change_contract( '8011015823A', '', 'OXYG15', 'C', 'AIR', 'CWIS', 'CWIS', '2010.07.02', '2013.07.01', 'HKD', 1, 75.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG24', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 76.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG24', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 80.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG24', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 84.0, 'N');
f_add_change_contract( 'HOC043-11', 'A1', 'OXYG27', 'C', 'LHL', 'LHL', 'LHL', '2011.12.19', '2012.06.30', 'HKD', 1, 102.0, 'N');
f_add_change_contract( 'HOC043-11', 'B', 'OXYG27', 'C', 'LHL', 'LHL', 'LHL', '2012.07.01', '2013.06.30', 'HKD', 1, 107.0, 'N');
f_add_change_contract( 'HOC043-11', 'C', 'OXYG27', 'C', 'LHL', 'LHL', 'LHL', '2013.07.01', '2014.06.30', 'HKD', 1, 112.0, 'N');
f_add_change_contract( 'HOC239-11', 'B', 'MADO05', 'C', 'DKSH', 'ROCE', 'ROCS', '2012.03.01', '2014.02.28', 'HKD', 100, 130.0, 'N');
f_add_change_contract( 'HOC297-10', 'A', 'HALO07', 'C', 'DKSH', 'JC', 'JP', '2011.03.01', '2013.02.28', 'HKD', 1, 125.0, 'N');
f_add_change_contract( 'HOC168-11', 'A', 'CHLO26', 'C', 'LFHC', 'REGE', 'BCM', '2012.01.02', '2013.01.01', 'HKD', 5000, 223.0, 'N');
f_add_change_contract( 'SQ11-017-01', '', 'CHLO2H', 'Q', 'LFHC', 'SSL', 'SSL', '2011.07.27', '2012.07.26', 'HKD', 250, 445.0, 'N');
f_add_change_contract( '8011016618', 'G', 'CHLO3F', 'C', 'FMCL', 'REGE', 'BCM', '2010.08.13', '2022.08.12', 'HKD', 1, 67.0, 'N');
f_add_change_contract( '8011016619', 'H', 'CHLO3F', 'C', 'GAMB', 'REGE', 'BCM', '2010.08.13', '2022.08.12', 'HKD', 1, 100.0, 'N');
f_add_change_contract( 'HOC008-11', 'A1', 'CHLO98', 'C', 'LFHC', 'SSL', 'SSL', '2011.07.01', '2013.05.31', 'HKD', 250, 399.0, 'N');
f_add_change_contract( 'HOC222-10', 'A', 'DIAL02', 'C', 'JJ', 'JJ', 'SYST', '2011.01.19', '2013.01.18', 'HKD', 1, 102.0, 'N');
f_add_change_contract( 'SQ10-476-01', 'A1', 'FORM02', 'Q', 'LFAU', 'UPLA', 'UPLA', '2011.07.20', '2012.07.19', 'HKD', 1000, 19.0, 'N');
f_add_change_contract( 'SQ10-476-01', 'A2', 'FORM02', 'Q', 'LFAU', 'UPLA', 'UPLA', '2011.07.20', '2012.07.19', 'HKD', 3600, 56.0, 'N');
f_add_change_contract( 'SQ10-475-01', '', 'FORM17', 'Q', 'LFAU', 'UPLA', 'UPLA', '2011.07.20', '2012.07.19', 'HKD', 80, 344.0, 'N');
f_add_change_contract( 'SQ10-373-01', '', 'APLO01', 'Q', 'YKCL', 'PASC', 'PASC', '2011.06.10', '2012.06.09', 'HKD', 1, 9.9, 'N');
f_add_change_contract( 'HOC194-10', 'A', 'OFLO05', 'C', 'HKMS', 'DAIS', 'SANT', '2010.11.26', '2012.11.25', 'HKD', 10, 212.7, 'N');
f_add_change_contract( 'HOC251-10', 'A', 'ASPI11', 'C', 'ZUEL', 'GSK', 'SIGM', '2011.05.08', '2012.05.07', 'HKD', 168, 38.5, 'N');
f_add_change_contract( 'HOC193-11', 'A', 'ASPI11', 'C', 'ZUEL', 'GSK', 'SPAP', '2012.05.08', '2014.05.07', 'HKD', 168, 40.3, 'N');
f_add_change_contract( 'HOC051-11', 'A', 'MESA01', 'C', 'ASS', 'TILL', 'TILL', '2011.09.17', '2013.09.16', 'HKD', 100, 375.0, 'N');
f_add_change_contract( 'HOC095-11', 'A', 'MESA04', 'C', 'JML', 'FALK', 'FALK', '2011.12.29', '2013.12.28', 'HKD', 100, 340.0, 'N');
f_add_change_contract( 'HOC095-11', 'B', 'MESA10', 'C', 'JML', 'FALK', 'FALK', '2011.12.29', '2013.12.28', 'HKD', 100, 415.0, 'N');
f_add_change_contract( 'HOC091-10', 'B2', 'MYCO08', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.12.20', '2012.10.03', 'HKD', 120, 1461.0, 'N');
f_add_change_contract( 'HOC091-10', 'B1', 'MYCO08', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.12.20', '2012.10.03', 'HKD', 480, 4383.0, 'N');
f_add_change_contract( 'HOC091-10', 'A2', 'MYCO09', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.12.20', '2012.10.03', 'HKD', 120, 2922.0, 'N');
f_add_change_contract( 'HOC091-10', 'A1', 'MYCO09', 'C', 'ZUEL', 'NOVA', 'NOVA', '2011.12.20', '2012.10.03', 'HKD', 480, 8766.0, 'N');
f_add_change_contract( 'HOC089-11', 'A', 'RABE01', 'C', 'DKSH', 'EISA', 'EISA', '2011.11.19', '2013.11.18', 'HKD', 280, 500.0, 'N');
f_add_change_contract( 'HOC089-11', 'B', 'RABE02', 'C', 'DKSH', 'EISA', 'EISA', '2011.11.19', '2013.11.18', 'HKD', 5600, 11500.0, 'N');
f_add_change_contract( 'HOC101-11', 'A', 'SULP23', 'C', 'ELC', 'ELC', 'ELC', '2011.12.14', '2013.12.13', 'HKD', 100, 74.9, 'N');
f_add_change_contract( 'HOC246-10', 'A1', 'VALP02', 'C', 'LFHC', 'SA', 'SANO', '2011.07.01', '2013.01.19', 'HKD', 100, 39.5, 'N');
f_add_change_contract( 'HOC117-11', 'A', 'SOFR01', 'C', 'LFHC', 'SA', 'PATH', '2011.10.26', '2013.10.25', 'HKD', 1, 21.0, 'N');
f_add_change_contract( 'SQ10-411-01', '', 'CALC18', 'Q', 'DKSH', 'NOVT', 'FAMA', '2011.07.11', '2012.07.10', 'HKD', 10, 40.0, 'N');
f_add_change_contract( 'HOC053-11', 'A', 'GENT32', 'C', 'DKSH', 'NOVA', 'EXCE', '2011.10.31', '2013.10.30', 'HKD', 160, 2200.0, 'N');
f_add_change_contract( 'HOC149-11', 'A', 'CHLO41', 'C', 'MEDI', 'MEDI', 'MEDI', '2011.11.08', '2012.11.07', 'HKD', 30, 0.84, 'N');
f_add_change_contract( 'SQ11-220-01', '', 'POLY09', 'Q', 'ZUEL', 'SLL', 'SLL', '2012.01.10', '2013.01.09', 'HKD', 350, 126.0, 'N');
f_add_change_contract( 'HOC116-10', 'A', 'COCA35', 'C', 'JEAN', 'JEAN', 'JEAN', '2010.12.17', '2012.12.16', 'HKD', 1, 205.0, 'N');
f_add_change_contract( 'SQ10-402-01', '', 'BARI02', 'Q', 'ARB', 'EZEM', 'EZEM', '2011.05.15', '2012.05.14', 'HKD', 24, 3156.0, 'N');
f_add_change_contract( 'HOC137-10', 'A3', 'FLEE01', 'C', 'LFHC', 'CBFD', 'CBFD', '2011.07.01', '2012.10.25', 'HKD', 1, 7.6, 'N');
f_add_change_contract( 'HOC137-10', 'A2', 'FLEE01', 'C', 'LFHC', 'CBFD', 'CBFD', '2011.07.01', '2012.10.25', 'HKD', 155, 760.0, 'N');
f_add_change_contract( 'HOC030-11', 'A', 'MESA06', 'C', 'JML', 'FALK', 'FALK', '2011.06.14', '2013.06.13', 'HKD', 7, 320.0, 'N');
f_add_change_contract( 'HOC158-11', 'A', 'VENL04', 'C', 'ZUEL', 'WYET', 'WM', '2011.12.16', '2013.12.15', 'HKD', 28, 203.0, 'N');
f_add_change_contract( 'HOC158-11', 'B', 'VENL05', 'C', 'ZUEL', 'WYET', 'WM', '2011.12.16', '2013.12.15', 'HKD', 28, 375.0, 'N');
f_add_change_contract( 'HOC158-11', 'C', 'VENL06', 'C', 'ZUEL', 'WYET', 'WM', '2011.12.16', '2013.12.15', 'HKD', 7, 25.0, 'N');
f_add_change_contract( 'SQ10-323-01', '', 'CLAR06', 'Q', 'DKSH', 'SP', 'SP', '2011.04.30', '2012.04.29', 'HKD', 7, 22.4, 'N');
f_add_change_contract( 'SQ11-315-01', '', 'CLAR06', 'Q', 'DKSH', 'SP', 'SP', '2012.04.30', '2013.04.29', 'HKD', 7, 22.4, 'N');
f_add_change_contract( 'HOC162-11', 'B', 'FELO01', 'C', 'VICK', 'VICK', 'VICK', '2012.02.26', '2014.02.25', 'HKD', 500, 390.0, 'N');
f_add_change_contract( 'HOC162-11', 'C', 'FELO02', 'C', 'VICK', 'VICK', 'VICK', '2012.02.26', '2014.02.25', 'HKD', 500, 740.0, 'N');
f_add_change_contract( 'HOC162-11', 'A', 'FELO03', 'C', 'VICK', 'VICK', 'VICK', '2012.02.26', '2014.02.25', 'HKD', 500, 290.0, 'N');
f_add_change_contract( 'HOC203-10', 'A', 'METH72', 'C', 'DKSH', 'JP', 'JC', '2010.12.01', '2012.11.30', 'HKD', 30, 565.0, 'N');
f_add_change_contract( 'HOC203-10', 'C', 'METH74', 'C', 'DKSH', 'JP', 'JC', '2010.12.01', '2012.11.30', 'HKD', 30, 688.0, 'N');
f_add_change_contract( 'HOC203-10', 'B', 'METH77', 'C', 'DKSH', 'JP', 'JC', '2010.12.01', '2012.11.30', 'HKD', 30, 628.0, 'N');
f_add_change_contract( 'SQ10-491-01', '', 'NIFE04', 'Q', 'ZUEL', 'BH', 'BAYR', '2011.06.23', '2012.06.22', 'HKD', 30, 68.0, 'N');
f_add_change_contract( 'SQ10-492-01', '', 'NIFE05', 'Q', 'ZUEL', 'BH', 'BAYR', '2011.06.23', '2012.06.22', 'HKD', 30, 136.0, 'N');
f_add_change_contract( 'HOC326-10', 'A', 'QUET04', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.03.29', '2013.03.28', 'HKD', 60, 175.6, 'N');
f_add_change_contract( 'HOC326-10', 'B', 'QUET05', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.03.29', '2013.03.28', 'HKD', 60, 422.4, 'N');
f_add_change_contract( 'HOC326-10', 'C', 'QUET06', 'C', 'ZUEL', 'ASTZ', 'ASTZ', '2011.03.29', '2013.03.28', 'HKD', 60, 633.6, 'N');
f_add_change_contract( 'DQ10-129', '', 'ATRO15', 'C', 'DKSH', 'AHKL', 'ALCO', '2011.08.29', '2012.08.28', 'HKD', 1, 100.0, 'N');
f_add_change_contract( 'SQ11-007-01', '', 'BETA21', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.09.14', '2012.09.13', 'HKD', 1, 34.0, 'N');
f_add_change_contract( 'HOC125-11', 'A', 'BIMA01', 'C', 'DKSH', 'AAL', 'ASL', '2011.11.19', '2013.11.18', 'HKD', 12, 1200.0, 'N');
f_add_change_contract( 'HOC235-11', 'A1', 'BRIM01', 'C', 'DKSH', 'AAL', 'ASL', '2012.02.23', '2014.02.22', 'HKD', 1, 42.0, 'N');
f_add_change_contract( 'HOC235-11', 'A', 'BRIM01', 'C', 'DKSH', 'AAL', 'ASL', '2012.02.23', '2014.02.22', 'HKD', 515, 21000.0, 'N');
f_add_change_contract( 'HOC007-10', 'A', 'BRIN02', 'C', 'DKSH', 'AHKL', 'AHKL', '2010.05.09', '2012.05.08', 'HKD', 1, 77.5, 'N');
f_add_change_contract( 'HOC234-11', 'A', 'BRIN02', 'C', 'DKSH', 'AHKL', 'ALCC', '2012.05.09', '2014.05.08', 'HKD', 1, 79.0, 'N');
f_add_change_contract( 'SQ11-044-01', 'S', 'CARB85', 'Q', 'DKSH', 'AAL', 'AAL', '2011.10.14', '2012.10.13', 'HKD', 1, 14.0, 'N');
f_add_change_contract( 'SQ11-044-01', '', 'CARB85', 'Q', 'DKSH', 'AAL', 'AAL', '2011.10.14', '2012.10.13', 'HKD', 11, 140.0, 'N');
f_add_change_contract( 'SQ11-199-01', 'S', 'CART01', 'Q', 'ZUEL', 'OTSU', 'OTSU', '2012.01.26', '2013.01.25', 'HKD', 1, 42.6, 'N');
f_add_change_contract( 'SQ11-199-01', '', 'CART01', 'Q', 'ZUEL', 'OTSU', 'OTSU', '2012.01.26', '2013.01.25', 'HKD', 600, 12780.0, 'N');
f_add_change_contract( 'HOC096-11', 'A1', 'COSO01', 'C', 'DKSH', 'MSD', 'MSD', '2011.11.10', '2013.11.09', 'HKD', 1, 68.0, 'N');
f_add_change_contract( 'HOC096-11', 'A', 'COSO01', 'C', 'DKSH', 'MSD', 'MSD', '2011.11.10', '2013.11.09', 'HKD', 1134, 68000.0, 'N');
f_add_change_contract( 'SQ11-340-01', '', 'CYCL05', 'Q', 'DKSH', 'AHKL', 'AHKL', '2012.01.06', '2013.01.05', 'HKD', 1, 30.0, 'N');
f_add_change_contract( 'SQ11-295-01', '', 'DEXA16', 'Q', 'JML', 'ASHF', 'ASHF', '2012.01.06', '2013.01.05', 'HKD', 1, 6.0, 'N');
f_add_change_contract( 'SQ11-113-01', 'S', 'DORZ01', 'Q', 'DKSH', 'MSD', 'MSD', '2011.11.03', '2012.11.02', 'HKD', 1, 68.0, 'N');
f_add_change_contract( 'SQ11-113-01', '', 'DORZ01', 'Q', 'DKSH', 'MSD', 'MSD', '2011.11.03', '2012.11.02', 'HKD', 170, 10200.0, 'N');
f_add_change_contract( 'HOC127-11', 'A1', 'DUOT01', 'C', 'DKSH', 'AHKL', 'ALCC', '2011.11.08', '2013.11.07', 'HKD', 1, 160.0, 'N');
f_add_change_contract( 'HOC127-11', 'A', 'DUOT01', 'C', 'DKSH', 'AHKL', 'ALCC', '2011.11.08', '2013.11.07', 'HKD', 1170, 112000.0, 'N');
f_add_change_contract( 'SQ10-483-01', '', 'FUSI06', 'Q', 'DKSH', 'LEO', 'LEO', '2011.08.23', '2012.08.22', 'HKD', 1, 41.5, 'N');
f_add_change_contract( 'HOC301-10', 'A', 'HYPR01', 'C', 'ELC', 'ELC', 'ELC', '2011.03.26', '2013.03.25', 'HKD', 25, 97.0, 'N');
f_add_change_contract( 'HOC303-10', 'A1', 'LATA01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.03.17', '2013.03.16', 'HKD', 1, 180.0, 'N');
f_add_change_contract( 'HOC303-10', 'A', 'LATA01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.03.17', '2013.03.16', 'HKD', 730, 90000.0, 'N');
f_add_change_contract( 'SQ11-267-01', '', 'LEVO16', 'Q', 'HKMS', 'SANT', 'SANT', '2012.03.19', '2013.03.18', 'JPY', 10, 3000.0, 'N');
f_add_change_contract( 'SQ11-345-01', '', 'MAXI01', 'Q', 'DKSH', 'AHKL', 'AHKL', '2012.01.06', '2013.01.05', 'HKD', 1, 12.0, 'N');
f_add_change_contract( 'SQ11-041-01', '', 'MYDR03', 'Q', 'HKMS', 'SANT', 'SANT', '2011.08.23', '2012.08.22', 'JPY', 10, 4850.0, 'N');
f_add_change_contract( 'SQ10-296-01', '', 'NATU02', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.05.01', '2012.04.30', 'HKD', 1, 14.0, 'N');
f_add_change_contract( 'SQ11-299-01', '', 'NATU02', 'Q', 'DKSH', 'AHKL', 'AHKL', '2012.05.01', '2013.04.30', 'HKD', 1, 14.0, 'N');
f_add_change_contract( 'SQ11-001-01', '', 'NATU04', 'Q', 'DKSH', 'LALC', 'LALC', '2011.09.17', '2012.09.16', 'HKD', 32, 34.0, 'N');
f_add_change_contract( 'SQ10-485-01', 'S', 'NATU05', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.08.29', '2012.08.28', 'HKD', 1, 14.0, 'N');
f_add_change_contract( 'SQ10-485-01', '', 'NATU05', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.08.29', '2012.08.28', 'HKD', 112, 1344.0, 'N');
f_add_change_contract( 'SQ11-032-01', '', 'OCUL01', 'Q', 'HKMS', 'NOVA', 'EXCE', '2011.07.28', '2012.07.27', 'EUR', 1, 1.49, 'N');
f_add_change_contract( 'SQ10-444-01', '', 'PILO08', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.08.01', '2012.07.31', 'HKD', 1, 24.0, 'N');
f_add_change_contract( 'SQ10-445-01', '', 'PILO09', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.08.01', '2012.07.31', 'HKD', 1, 30.0, 'N');
f_add_change_contract( 'SQ10-446-01', '', 'PILO10', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.08.01', '2012.07.31', 'HKD', 1, 20.0, 'N');
f_add_change_contract( 'SQ11-033-01', '', 'PRED06', 'Q', 'DKSH', 'AAL', 'AAL', '2011.08.27', '2012.08.26', 'HKD', 1, 13.9, 'N');
f_add_change_contract( 'SQ11-048-01', '', 'PRED09', 'Q', 'DKSH', 'AAL', 'AAL', '2011.10.14', '2012.10.13', 'HKD', 1, 14.0, 'N');
f_add_change_contract( 'SQ11-183-01', '', 'SPER04', 'Q', 'ZUEL', 'NOVA', 'EXCE', '2011.12.18', '2012.12.17', 'HKD', 1, 17.0, 'N');
f_add_change_contract( 'HOC232-10', 'A', 'TIMO02', 'C', 'ELC', 'ELC', 'ELC', '2011.02.19', '2013.02.18', 'HKD', 1, 4.45, 'N');
f_add_change_contract( 'HOC123-11', 'A1', 'TRAV03', 'C', 'DKSH', 'AHKL', 'ALCC', '2011.10.20', '2013.10.19', 'HKD', 1, 145.0, 'N');
f_add_change_contract( 'HOC123-11', 'A', 'TRAV03', 'C', 'DKSH', 'AHKL', 'ALCC', '2011.10.20', '2013.10.19', 'HKD', 850, 101500.0, 'N');
f_add_change_contract( 'SQ11-350-01', '', 'TROP04', 'Q', 'DKSH', 'AHKL', 'AHKL', '2012.01.06', '2013.01.05', 'HKD', 1, 33.0, 'N');
f_add_change_contract( 'HOC303-10', 'B1', 'XALA01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.03.17', '2013.03.16', 'HKD', 1, 175.0, 'N');
f_add_change_contract( 'HOC303-10', 'B', 'XALA01', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2011.03.17', '2013.03.16', 'HKD', 790, 87500.0, 'N');
f_add_change_contract( 'SQ10-481-01', '', 'ACYC05', 'Q', 'ZUEL', 'XXX', 'DRAI', '2011.08.19', '2012.08.18', 'HKD', 1, 59.0, 'N');
f_add_change_contract( 'HOC018-11', 'A', 'DURA01', 'C', 'DKSH', 'AHKL', 'AHKL', '2011.05.01', '2013.04.30', 'HKD', 1, 20.0, 'N');
f_add_change_contract( 'SQ10-416-01', '', 'MAXI02', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.07.25', '2012.07.24', 'HKD', 1, 20.0, 'N');
f_add_change_contract( 'SQ11-346-01', '', 'OFLO02', 'Q', 'HKMS', 'SANT', 'SANT', '2012.01.06', '2013.01.05', 'JPY', 10, 3493.0, 'N');
f_add_change_contract( 'SQ11-300-01', '', 'PRED14', 'Q', 'HW', 'FORL', 'PNW', '2012.02.23', '2013.02.22', 'HKD', 1, 460.0, 'N');
f_add_change_contract( 'SQ11-253-01', '', 'FUSI09', 'Q', 'DKSH', 'LEO', 'LEO', '2012.03.29', '2013.03.28', 'HKD', 10, 83.0, 'N');
f_add_change_contract( 'SQ11-159-01', 'S', 'ADAP01', 'Q', 'GALD', 'GALD', 'GALD', '2011.12.04', '2012.12.03', 'HKD', 1, 57.6, 'N');
f_add_change_contract( 'SQ11-159-01', '', 'ADAP01', 'Q', 'GALD', 'GALD', 'GALD', '2011.12.04', '2012.12.03', 'HKD', 11, 576.0, 'N');
f_add_change_contract( 'SQ11-383-01', '', 'FLUO09', 'Q', 'DKSH', 'CSD', 'BIOG', '2012.02.25', '2013.02.24', 'HKD', 1, 52.5, 'N');
f_add_change_contract( 'SQ11-188-01', '', 'INST04', 'Q', 'SINC', 'FARC', 'ALME', '2011.12.10', '2012.12.09', 'HKD', 1, 19.5, 'N');
f_add_change_contract( 'SQ10-327-01', '', 'LIGN07', 'Q', 'ZUEL', 'ASTZ', 'RECH', '2011.04.30', '2012.07.29', 'HKD', 10, 149.0, 'N');
f_add_change_contract( 'SQ11-323-01', '', 'METR05', 'Q', 'GALD', 'GALD', 'GALD', '2012.01.16', '2013.01.15', 'HKD', 1, 40.0, 'N');
f_add_change_contract( 'SQ11-214-01', '', 'ACET17', 'Q', 'ZENF', 'ZAM', 'ZAM', '2012.01.08', '2013.01.07', 'HKD', 60, 21.0, 'N');
f_add_change_contract( 'SQ11-313-01', '', 'ACET40', 'Q', 'ZENF', 'ZAM', 'ZAM', '2012.01.16', '2013.01.15', 'HKD', 30, 6.93, 'N');
f_add_change_contract( 'SQ11-215-01', '', 'ACET41', 'Q', 'ZENF', 'ZAM', 'ZAM', '2012.01.08', '2013.01.07', 'HKD', 30, 9.0, 'N');
f_add_change_contract( 'SQ11-237-01', '', 'AGIO02', 'Q', 'LFHC', 'MADA', 'MADA', '2012.04.01', '2013.03.31', 'HKD', 250, 42.2, 'N');
f_add_change_contract( 'HOC060-10', 'A1', 'META14', 'C', 'ZUEL', 'PFIZ', 'PGM', '2012.01.12', '2012.10.31', 'HKD', 528, 46.0, 'N');
f_add_change_contract( 'HOC105-10', 'A', 'FLUP04', 'C', 'DKSH', 'LUND', 'LUND', '2010.10.13', '2012.10.12', 'HKD', 10, 435.0, 'N');
f_add_change_contract( 'SQ10-457-01', '', 'NAND03', 'Q', 'DKSH', 'ORGA', 'ORGA', '2011.07.20', '2012.07.19', 'HKD', 1, 132.0, 'N');
f_add_change_contract( 'HOC321-10', 'A', 'SUST01', 'C', 'DKSH', 'ORGA', 'ORGA', '2011.04.01', '2013.03.31', 'HKD', 1, 89.6, 'N');
f_add_change_contract( 'SQ11-043-01', '', 'CARB46', 'Q', 'DKSH', 'AHKL', 'AHKL', '2011.10.14', '2012.10.13', 'HKD', 12, 540.0, 'N');
f_add_change_contract( 'SQ11-378-01', '', 'BACL04', 'Q', 'ZUEL', 'NOVA', 'NOVA', '2012.02.25', '2013.02.24', 'HKD', 1, 1680.0, 'N');
f_add_change_contract( 'HOC195-10', 'A1', 'BORT01', 'C', 'DKSH', 'JP', 'PFMP', '2011.12.20', '2012.11.23', 'HKD', 4, 37200.0, 'N');
f_add_change_contract( 'SQ11-433-01', 'B', 'CEFT07', 'Q', 'CTC', 'MEPH', 'LABE', '2012.02.19', '2012.04.18', 'HKD', 1, 32.8, 'N');
f_add_change_contract( 'SQ10-433-01', '', 'EPTI01', 'Q', 'DKSH', 'SPDS', 'TPMI', '2011.07.30', '2012.07.29', 'HKD', 1, 956.0, 'N');
f_add_change_contract( 'SQ11-187-01', '', 'FLUM02', 'Q', 'DKSH', 'ROCE', 'CENE', '2011.12.14', '2012.12.13', 'HKD', 5, 1600.0, 'N');
f_add_change_contract( 'SQ10-470-01', '', 'FLUO10', 'Q', 'DKSH', 'AHKL', 'IMS', '2011.07.13', '2012.07.12', 'HKD', 1, 108.0, 'N');
f_add_change_contract( 'HOC258-10', 'A', 'IDAR01', 'C', 'ZUEL', 'PFIZ', 'ACTA', '2011.03.18', '2013.03.17', 'HKD', 1, 1725.0, 'N');
f_add_change_contract( 'HOC033-11', 'A', 'MEGL07', 'C', 'HW', 'GUER', 'GUER', '2011.07.01', '2012.06.30', 'HKD', 306, 31200.0, 'N');
f_add_change_contract( 'HOC284-10', 'A', 'TIEN01', 'C', 'DKSH', 'MSD', 'MSD', '2011.03.19', '2013.03.18', 'HKD', 1, 62.0, 'N');
f_add_change_contract( 'HOC122-11', 'C', 'ESOM03', 'C', 'ZUEL', 'ASTZ', 'ASAB', '2011.12.22', '2013.12.21', 'HKD', 1, 62.5, 'N');
f_add_change_contract( 'SQ11-369-01', '', 'LANR01', 'Q', 'LFHC', 'BI', 'IPB', '2012.02.24', '2013.02.23', 'HKD', 1, 3200.0, 'N');
f_add_change_contract( 'HOC159-11', 'A1', 'RISP06', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 1, 976.0, 'N');
f_add_change_contract( 'HOC159-11', 'A', 'RISP06', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 11, 9760.0, 'N');
f_add_change_contract( 'HOC159-11', 'B1', 'RISP07', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 1, 1470.0, 'N');
f_add_change_contract( 'HOC159-11', 'B', 'RISP07', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 11, 14700.0, 'N');
f_add_change_contract( 'HOC159-11', 'C1', 'RISP08', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 1, 1952.0, 'N');
f_add_change_contract( 'HOC159-11', 'C', 'RISP08', 'C', 'DKSH', 'JP', 'CILA', '2011.11.10', '2013.11.09', 'HKD', 11, 19520.0, 'N');
f_add_change_contract( 'HOC115-11', 'A', 'TRIP05', 'C', 'LFHC', 'BI', 'IPB', '2011.10.08', '2013.10.07', 'HKD', 7, 18240.0, 'N');
f_add_change_contract( 'DQ11-102-01', '', 'AGAL01', 'C', 'LFHC', 'GENZ', 'GENZ', '2012.03.08', '2012.06.30', 'HKD', 1, 4759.0, 'N');
f_add_change_contract( 'DQ11-103-01', '', 'AGAL02', 'C', 'LFHC', 'GENZ', 'GENZ', '2012.03.08', '2012.06.30', 'HKD', 1, 33313.0, 'N');
f_add_change_contract( 'SQ11-377-01', 'A1', 'ALGL02', 'Q', 'LFHC', 'GENZ', 'GENY', '2012.03.01', '2013.02.17', 'HKD', 1, 6165.0, 'N');
f_add_change_contract( 'HOC288-10', 'A1', 'ANID02', 'C', 'ZUEL', 'PFIZ', 'PUJL', '2012.02.24', '2013.02.23', 'HKD', 35, 30000.0, 'N');
f_add_change_contract( 'SQ10-450-01', '', 'ATOS02', 'Q', 'FERR', 'FERR', 'FERR', '2011.07.20', '2012.07.19', 'HKD', 1, 650.0, 'N');
f_add_change_contract( 'HOC139-11', 'B', 'AZIT03', 'C', 'ZUEL', 'PFIZ', 'PFII', '2012.01.21', '2014.01.20', 'HKD', 1, 155.0, 'N');
f_add_change_contract( 'HOC067-10', 'A', 'BEVA01', 'C', 'DKSH', 'ROCE', 'FHR', '2010.08.29', '2012.08.28', 'HKD', 1, 3600.0, 'N');
f_add_change_contract( 'SQ10-375-01', 'A1', 'BUSU03', 'Q', 'LFHC', 'OTSA', 'DSM', '2011.07.01', '2012.06.09', 'HKD', 1, 1755.0, 'N');
f_add_change_contract( 'HOC109-09', 'A2', 'CASP01', 'C', 'DKSH', 'MERK', 'LMSD', '2012.02.29', '2012.06.20', 'HKD', 1, 1521.0, 'N');
f_add_change_contract( 'SQ11-051-01', '', 'CASP02', 'Q', 'DKSH', 'MERK', 'LMSD', '2011.10.14', '2012.10.13', 'HKD', 1, 1980.0, 'N');
f_add_change_contract( 'HOC196-10', 'A', 'CETU03', 'C', 'ZUEL', 'MERS', 'MERK', '2011.01.01', '2012.12.31', 'HKD', 1, 2524.0, 'N');
f_add_change_contract( 'HOC196-11', 'A', 'DEXM01', 'C', 'ZUEL', 'HOSP', 'HOSP', '2012.02.03', '2014.02.02', 'HKD', 5, 1350.0, 'N');
f_add_change_contract( 'HOC269-10', 'A', 'DEXT18', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 10, 45.0, 'N');
f_add_change_contract( 'HOC213-10', 'B', 'DEXT1C', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.04.01', '2013.03.31', 'HKD', 24, 178.08, 'N');
f_add_change_contract( 'SQ11-239-01', '', 'DEXT28', 'Q', 'DKSH', 'BB', 'BB', '2012.03.20', '2013.03.19', 'HKD', 20, 48.0, 'N');
f_add_change_contract( 'SQ11-423-01', '', 'DEXT68', 'Q', 'BAXT', 'BAXT', 'BAXT', '2012.02.23', '2013.02.22', 'HKD', 24, 492.0, 'N');
f_add_change_contract( 'HOC214-10', 'A', 'DEXT72', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 10, 140.0, 'N');
f_add_change_contract( 'HOC269-10', 'B', 'DEXT75', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 10, 300.0, 'N');
f_add_change_contract( 'SQ11-240-01', '', 'DEXT76', 'Q', 'LCH', 'OTSU', 'OTSU', '2012.04.01', '2013.03.31', 'HKD', 40, 508.0, 'N');
f_add_change_contract( 'HOC213-10', 'A', 'DEXT78', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.04.01', '2013.03.31', 'HKD', 96, 644.16, 'N');
f_add_change_contract( 'DQ11-111-01', '', 'DEXT84', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.16', '2012.05.15', 'HKD', 60, 306.0, 'N');
f_add_change_contract( 'HOC268-10', 'A', 'DEXT98', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.04.13', '2013.04.12', 'HKD', 14, 203.14, 'N');
f_add_change_contract( 'HOC268-10', 'B', 'DEXT99', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.04.01', '2013.03.31', 'HKD', 24, 192.72, 'N');
f_add_change_contract( 'SQ11-177-01', '', 'DIPE02', 'Q', 'ZUEL', 'FK', 'FK', '2011.12.04', '2012.12.03', 'HKD', 1, 262.0, 'N');
f_add_change_contract( 'HOC065-11', 'A', 'DISO10', 'C', 'ZUEL', 'NOVA', 'NOVI', '2011.09.20', '2013.09.19', 'HKD', 4, 2150.0, 'N');
f_add_change_contract( 'HOC066-11', 'A', 'DISO17', 'C', 'ZUEL', 'HOSP', 'HOSP', '2011.09.20', '2013.09.19', 'HKD', 1, 435.0, 'N');
f_add_change_contract( 'HOC066-11', 'B', 'DISO18', 'C', 'ZUEL', 'HOSP', 'HOSP', '2011.09.20', '2013.09.19', 'HKD', 1, 870.0, 'N');
f_add_change_contract( 'SQ10-434-01', '', 'EPTI02', 'Q', 'DKSH', 'SPDS', 'TPMI', '2011.07.30', '2012.07.29', 'HKD', 1, 3126.0, 'N');
f_add_change_contract( 'SQ10-471-01', '', 'FLUC04', 'Q', 'ZUEL', 'PFIZ', 'PFIZ', '2011.07.18', '2012.07.17', 'HKD', 1, 108.0, 'N');
f_add_change_contract( 'SQ11-062-01', '', 'FLUC05', 'Q', 'UACL', 'CLAS', 'CLAS', '2011.10.08', '2012.10.07', 'HKD', 1, 80.9, 'N');
f_add_change_contract( 'SQ11-252-01', '', 'FRUS04', 'Q', 'LFHC', 'SA', 'SAAD', '2012.02.23', '2013.02.22', 'HKD', 5, 360.0, 'N');
f_add_change_contract( 'SQ10-484-01', '', 'GALS02', 'Q', 'DKSH', 'BIOP', 'BIOP', '2011.08.09', '2012.08.08', 'HKD', 1, 13349.0, 'N');
f_add_change_contract( 'HOC078-10', '', 'GELO01', 'C', 'DKSH', 'BB', 'BB', '2010.08.28', '2012.08.27', 'HKD', 10, 335.0, 'N');
f_add_change_contract( 'SQ10-436-01', '', 'GLYC23', 'Q', 'ZUEL', 'SCHW', 'SCHW', '2011.07.20', '2012.07.19', 'HKD', 10, 60.0, 'N');
f_add_change_contract( 'HOC214-10', 'C', 'HART05', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 10, 50.0, 'N');
f_add_change_contract( 'SQ10-226-01', 'A1', 'IDUR01', 'Q', 'LFHC', 'GENZ', 'SHIR', '2012.03.01', '2012.07.19', 'HKD', 1, 16880.0, 'N');
f_add_change_contract( 'SQ10-495-01', '', 'ILOP02', 'Q', 'ZUEL', 'BH', 'BERL', '2011.08.18', '2012.08.17', 'HKD', 5, 1700.0, 'N');
f_add_change_contract( 'SQ11-515-01', '', 'IRIN01', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2012.04.17', '2012.06.16', 'HKD', 1, 220.0, 'N');
f_add_change_contract( 'SQ11-516-01', '', 'IRIN02', 'Q', 'ZUEL', 'HOSP', 'HOSP', '2012.04.17', '2012.06.16', 'HKD', 1, 120.0, 'N');
f_add_change_contract( 'SQ11-157-01', 'S', 'ITRA03', 'Q', 'DKSH', 'JP', 'JP', '2012.01.01', '2012.12.31', 'HKD', 1, 868.0, 'N');
f_add_change_contract( 'SQ11-157-01', '', 'ITRA03', 'Q', 'DKSH', 'JP', 'JP', '2012.01.01', '2012.12.31', 'HKD', 3, 1736.0, 'N');
f_add_change_contract( 'HOC114-10', 'A1', 'KABI01', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 1, 273.0, 'N');
f_add_change_contract( 'HOC114-10', 'A', 'KABI01', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 68, 16380.0, 'N');
f_add_change_contract( 'HOC114-10', 'B1', 'KABI02', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 1, 341.0, 'N');
f_add_change_contract( 'HOC114-10', 'B', 'KABI02', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 46, 13640.0, 'N');
f_add_change_contract( 'HOC114-10', 'C', 'KABI03', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 1, 310.0, 'N');
f_add_change_contract( 'HOC114-10', 'D1', 'KABI04', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 1, 257.0, 'N');
f_add_change_contract( 'HOC114-10', 'D', 'KABI04', 'C', 'ZUEL', 'FK', 'FK', '2010.11.17', '2012.11.16', 'HKD', 46, 10280.0, 'N');
f_add_change_contract( 'SQ11-233-01', 'A1', 'LARO02', 'Q', 'LFHC', 'GENZ', 'BIOP', '2012.03.01', '2012.10.13', 'HKD', 1, 6630.5, 'N');
f_add_change_contract( 'SQ11-344-01', '', 'LIPO04', 'Q', 'DKSH', 'BB', 'BB', '2012.01.06', '2013.01.05', 'HKD', 10, 800.0, 'N');
f_add_change_contract( 'HOC289-10', 'A', 'METR03', 'C', 'DKSH', 'BB', 'BB', '2011.04.18', '2013.04.17', 'HKD', 50, 205.0, 'N');
f_add_change_contract( 'SQ10-489-01', '', 'MOXI02', 'Q', 'ZUEL', 'BH', 'BAYS', '2011.08.23', '2012.08.22', 'HKD', 1, 270.0, 'N');
f_add_change_contract( 'SQ11-058-01', '', 'MYCO07', 'Q', 'DKSH', 'ROCE', 'ROCH', '2011.10.17', '2012.10.16', 'HKD', 4, 504.0, 'N');
f_add_change_contract( 'CRC1/11', '.', 'NORM15', 'C', 'RC', 'CSL', 'CSL', '2011.08.01', '2012.07.31', 'HKD', 1, 613.0, 'N');
f_add_change_contract( 'HOC206-11', 'A', 'OXAL04', 'C', 'TIMC', 'TEVA', 'PBV', '2012.03.20', '2014.03.19', 'HKD', 1, 85.0, 'N');
f_add_change_contract( 'HOC206-11', 'B', 'OXAL07', 'C', 'TIMC', 'TEVA', 'PBV', '2012.03.20', '2014.03.19', 'HKD', 1, 148.2, 'N');
f_add_change_contract( 'HOC082-11', 'A', 'PEME01', 'C', 'YCW', 'EL', 'LILL', '2011.08.19', '2014.08.18', 'HKD', 1, 9000.0, 'N');
f_add_change_contract( 'HOC082-11', 'B', 'PEME04', 'C', 'YCW', 'EL', 'LILL', '2011.08.19', '2014.08.18', 'HKD', 1, 2220.0, 'N');
f_add_change_contract( 'HOC271-10', 'A', 'POTA57', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 271.2, 'N');
f_add_change_contract( 'HOC203-11', 'A', 'POTA57', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.13', '2014.04.12', 'HKD', 40, 280.8, 'N');
f_add_change_contract( 'HOC271-10', 'B', 'POTA58', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 280.0, 'N');
f_add_change_contract( 'HOC203-11', 'B', 'POTA58', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.13', '2014.04.12', 'HKD', 40, 290.0, 'N');
f_add_change_contract( 'HOC271-10', 'C', 'POTA59', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 271.2, 'N');
f_add_change_contract( 'HOC203-11', 'C', 'POTA59', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.13', '2014.04.12', 'HKD', 40, 280.8, 'N');
f_add_change_contract( 'HOC271-10', 'D', 'POTA60', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 280.0, 'N');
f_add_change_contract( 'HOC271-10', 'E', 'POTA65', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 387.6, 'N');
f_add_change_contract( 'HOC203-11', 'D', 'POTA65', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.13', '2014.04.12', 'HKD', 40, 401.6, 'N');
f_add_change_contract( 'HOC271-10', 'F', 'POTA66', 'C', 'LCH', 'OTSU', 'OTSU', '2011.04.13', '2012.04.12', 'HKD', 40, 398.0, 'N');
f_add_change_contract( 'HOC203-11', 'E', 'POTA66', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.13', '2014.04.12', 'HKD', 40, 412.4, 'N');
f_add_change_contract( 'SQ11-387-01', '', 'RASB01', 'Q', 'LFHC', 'SA', 'GL', '2012.02.25', '2013.02.24', 'HKD', 3, 2056.0, 'N');
f_add_change_contract( 'SQ11-388-01', '', 'SALB05', 'Q', 'ZUEL', 'GSK', 'GSK', '2012.02.25', '2013.02.24', 'HKD', 10, 1291.0, 'N');
f_add_change_contract( 'HOC211-10', 'A', 'SODI07', 'C', 'DKSH', 'BB', 'BB', '2011.01.01', '2012.12.31', 'HKD', 20, 310.0, 'N');
f_add_change_contract( 'HOC214-10', 'B', 'SODI14', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 50, 210.0, 'N');
f_add_change_contract( 'HOC269-10', 'C', 'SODI16', 'C', 'DKSH', 'BB', 'BB', '2011.02.16', '2013.02.15', 'HKD', 10, 42.0, 'N');
f_add_change_contract( 'DQ11-108-01', '', 'SODI16', 'C', 'LCH', 'OTSU', 'OTSU', '2012.04.03', '2012.04.12', 'HKD', 30, 189.0, 'N');
f_add_change_contract( 'HOC202-11', 'A', 'SODI18', 'C', 'DKSH', 'BB', 'BB', '2012.05.19', '2013.05.18', 'HKD', 10, 83.0, 'N');
f_add_change_contract( 'SQ11-194-01', '', 'SODI18', 'Q', 'LCH', 'OTSU', 'OTSU', '2011.08.19', '2012.05.18', 'HKD', 20, 169.8, 'N');
f_add_change_contract( 'SQ11-301-01', '', 'SODI20', 'Q', 'DKSH', 'BB', 'BB', '2012.01.06', '2013.01.05', 'HKD', 20, 65.0, 'N');
f_add_change_contract( 'SQ11-348-01', '', 'SODI5E', 'Q', 'ZUEL', 'FK', 'FK', '2012.01.06', '2013.01.05', 'HKD', 10, 270.0, 'N');
f_add_change_contract( 'HOC268-10', 'C', 'SODI9E', 'C', 'BAXT', 'BAXT', 'BAXT', '2011.04.13', '2013.04.12', 'HKD', 12, 153.48, 'N');
f_add_change_contract( '8011016619', 'J', 'SODIB5', 'C', 'GAMB', 'BAXT', 'BAXT', '2010.08.13', '2022.08.12', 'HKD', 1, 13.0, 'N');
f_add_change_contract( '8011016618', 'F', 'SODIB5', 'C', 'FMCL', 'BAXT', 'BAXT', '2010.08.13', '2022.08.12', 'HKD', 1, 24.0, 'N');
f_add_change_contract( 'HOC142-11', 'A', 'SOLU04', 'C', 'ZUEL', 'FK', 'SINO', '2012.02.10', '2014.02.09', 'HKD', 10, 497.2, 'N');
f_add_change_contract( 'SQ10-472-01', '', 'TIGE02', 'Q', 'ZUEL', 'WYET', 'WYET', '2011.07.13', '2012.07.12', 'HKD', 10, 3330.0, 'N');
f_add_change_contract( 'SQ11-076-01', '', 'TOCI03', 'Q', 'DKSH', 'ROCE', 'CHUG', '2011.10.07', '2012.10.06', 'HKD', 1, 8900.0, 'N');
f_add_change_contract( 'HOC183-11', 'A', 'VERT01', 'C', 'ZUEL', 'NOVA', 'JHP', '2012.01.21', '2014.01.20', 'HKD', 1, 10200.0, 'N');
f_add_change_contract( 'HOC044-10', 'B', 'VORI02', 'C', 'ZUEL', 'PFIZ', 'PFIZ', '2010.07.25', '2012.07.24', 'HKD', 1, 810.0, 'N');
f_add_change_contract( 'HOC076-10', 'A', 'ZOLE01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.08.23', '2012.08.22', 'HKD', 9, 17000.0, 'N');
f_add_change_contract( 'HOC015-11', 'A', 'BECL02', 'C', 'HKMS', 'IVAX', 'IVAX', '2011.08.10', '2012.08.09', 'HKD', 1, 35.0, 'N');
f_add_change_contract( 'HOC015-11', 'B', 'BECL06', 'C', 'HKMS', 'IVAX', 'IVAX', '2011.08.10', '2012.08.09', 'HKD', 1, 31.0, 'N');
f_add_change_contract( 'SQ11-385-01', '', 'FLUT12', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.02.25', '2013.02.24', 'HKD', 1, 103.0, 'N');
f_add_change_contract( 'SQ11-165-01', '', 'FLUT19', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.12.27', '2012.12.26', 'HKD', 1, 153.0, 'N');
f_add_change_contract( 'SQ10-382-01', '', 'FLUT20', 'Q', 'ZUEL', 'GSK', 'GWCL', '2011.06.14', '2012.06.13', 'HKD', 1, 401.0, 'N');
f_add_change_contract( 'SQ11-416-01', '', 'FLUT20', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.06.14', '2013.06.13', 'HKD', 1, 413.0, 'N');
f_add_change_contract( 'HOC295-10', 'G', 'NICO17', 'C', 'LFHC', 'JJ', 'MCNE', '2011.06.23', '2013.06.22', 'HKD', 1, 92.2, 'N');
f_add_change_contract( 'SQ11-421-01', 'S', 'SALM01', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.05.26', '2013.05.25', 'HKD', 1, 311.0, 'N');
f_add_change_contract( 'SQ11-421-01', '', 'SALM01', 'Q', 'ZUEL', 'GSK', 'GWCL', '2012.05.26', '2013.05.25', 'HKD', 110, 31100.0, 'N');
f_add_change_contract( 'HOC142-09', 'C', 'SALM07', 'C', 'ZUEL', 'GSK', 'GWCL', '2010.04.07', '2012.07.06', 'HKD', 17, 1680.0, 'N');
f_add_change_contract( 'HOC142-09', 'B', 'SALM08', 'C', 'ZUEL', 'GSK', 'GWCL', '2010.04.07', '2012.07.06', 'HKD', 25, 6840.0, 'N');
f_add_change_contract( 'HOC142-09', 'A', 'SALM09', 'C', 'ZUEL', 'GSK', 'GWCL', '2010.04.07', '2012.07.06', 'HKD', 34, 15600.0, 'N');
f_add_change_contract( 'HOC188-11', 'A', 'ABCI01', 'C', 'YCW', 'EL', 'CENT', '2012.03.01', '2014.02.28', 'HKD', 1, 4445.0, 'N');
f_add_change_contract( 'HOC140-11', 'A', 'ACYC02', 'C', 'ZUEL', 'GSK', 'GSKM', '2012.01.25', '2014.01.24', 'HKD', 5, 78.9, 'N');
f_add_change_contract( 'SQ10-482-01', '', 'ADDA02', 'Q', 'ZUEL', 'FK', 'FK', '2011.08.23', '2012.08.22', 'HKD', 20, 800.0, 'N');
f_add_change_contract( 'SQ11-130-01', '', 'ADEN06', 'Q', 'LFHC', 'SA', 'SA', '2011.11.04', '2012.11.03', 'HKD', 6, 860.0, 'N');
f_add_change_contract( 'HOC280-10', 'A', 'ADRE03', 'C', 'ZUEL', 'HOSP', 'HAME', '2011.03.03', '2013.03.02', 'HKD', 5, 96.5, 'N');
f_add_change_contract( 'SQ11-216-01', '', 'ADRE12', 'Q', 'HKMS', 'WEIM', 'WEIM', '2012.01.08', '2013.01.07', 'EUR', 10, 4.67, 'N');
f_add_change_contract( 'CRC3/11', '', 'ALBU06', 'C', 'RC', 'CSLB', 'CSLB', '2012.03.30', '2012.07.31', 'HKD', 1, 106.0, 'N');
f_add_change_contract( 'HOC032-10', 'A', 'ALFE01', 'C', 'DKSH', 'JP', 'JP', '2010.07.01', '2012.09.30', 'HKD', 10, 850.0, 'N');
f_add_change_contract( 'HOC034-11', 'A', 'ALTE01', 'C', 'ZUEL', 'BOEH', 'BOEH', '2011.05.15', '2013.05.14', 'HKD', 1, 5200.0, 'N');
f_add_change_contract( 'HOC226-10', 'A', 'AMIK02', 'C', 'DKSH', 'BMS', 'BMS', '2010.12.22', '2012.12.21', 'HKD', 1, 41.2, 'N');
f_add_change_contract( 'HOC226-10', 'B', 'AMIK03', 'C', 'DKSH', 'BMS', 'BMS', '2010.12.22', '2012.12.21', 'HKD', 1, 60.0, 'N');
f_add_change_contract( 'SQ10-409-01', '', 'AMIN27', 'Q', 'ZUEL', 'OTSU', 'OTSU', '2011.07.13', '2012.07.12', 'HKD', 1, 93.0, 'N');
f_add_change_contract( 'SQ11-160-01', '', 'AMIO02', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.12.07', '2012.12.06', 'HKD', 5, 24.2, 'N');
f_add_change_contract( 'HOC309-10', 'A', 'AMPH01', 'C', 'DKSH', 'BMS', 'FAMI', '2011.03.21', '2013.03.20', 'HKD', 1, 195.0, 'N');
f_add_change_contract( 'HOC116-11', 'A', 'AMPH03', 'C', 'LFHC', 'GILE', 'GILE', '2011.10.01', '2013.09.30', 'HKD', 1, 1665.0, 'N');
f_add_change_contract( 'HOC189-10', 'A', 'AMPI05', 'C', 'SKYL', 'CZPS', 'CZPS', '2010.11.09', '2012.11.08', 'HKD', 50, 85.0, 'N');
f_add_change_contract( 'HOC059-11', 'A', 'ANTI12', 'C', 'ZUEL', 'PFIZ', 'PUJL', '2011.07.09', '2013.07.08', 'HKD', 5, 9240.0, 'N');
f_add_change_contract( 'HOC076-11', 'A1', 'ANTI15', 'C', 'LFHC', 'GENP', 'GENP', '2012.03.01', '2013.10.28', 'HKD', 1, 1854.0, 'N');
f_add_change_contract( 'HOC005-11', 'A', 'ATRA01', 'C', 'ZUEL', 'HAME', 'GSKM', '2011.07.15', '2013.07.14', 'HKD', 5, 38.0, 'N');
f_add_change_contract( 'HOC005-11', 'B', 'ATRA02', 'C', 'ZUEL', 'HAME', 'GSKM', '2011.07.15', '2013.07.14', 'HKD', 5, 68.2, 'N');
f_add_change_contract( 'SQ10-427-01', '', 'ATRO03', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.14', '2012.07.13', 'HKD', 50, 400.0, 'N');
f_add_change_contract( 'HOC145-10', 'A1', 'AUGM04', 'C', 'ZUEL', 'GSK', 'SB', '2011.12.22', '2013.01.08', 'HKD', 10, 78.0, 'N');
f_add_change_contract( 'HOC064-10', 'A', 'BASI01', 'C', 'ZUEL', 'NOVA', 'NOVA', '2010.09.06', '2012.09.05', 'HKD', 1, 15225.0, 'N');
f_add_change_contract( 'SQ11-084-01', '', 'BENZ21', 'Q', 'ZUEL', 'LUND', 'HOSP', '2011.10.23', '2012.10.22', 'HKD', 5, 181.0, 'N');
f_add_change_contract( 'SQ11-203-01', '', 'BLEO02', 'Q', 'ML', 'NK', 'NK', '2012.01.30', '2013.01.29', 'HKD', 1, 279.0, 'N');
f_add_change_contract( 'HOC199-10', 'A1', 'BOTU02', 'C', 'LFHC', 'BI', 'IB', '2011.07.01', '2012.11.30', 'HKD', 6, 12750.0, 'N');
f_add_change_contract( 'HOC329-10', 'A1', 'BOTU03', 'C', 'DKSH', 'AAL', 'AAL', '2011.04.21', '2013.04.20', 'HKD', 1, 2145.0, 'N');
f_add_change_contract( 'HOC329-10', 'A', 'BOTU03', 'C', 'DKSH', 'AAL', 'AAL', '2011.04.21', '2013.04.20', 'HKD', 11, 21450.0, 'N');
f_add_change_contract( 'HOC155-11', 'A', 'BUPI04', 'C', 'ZUEL', 'ASTZ', 'CENE', '2011.12.10', '2013.12.09', 'HKD', 5, 350.0, 'N');
f_add_change_contract( 'SQ10-451-01', '', 'BUPI07', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.07.20', '2012.07.19', 'HKD', 5, 285.0, 'N');
f_add_change_contract( 'SQ11-223-01', '', 'BUPI08', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.03', '2012.10.02', 'HKD', 5, 280.0, 'N');
f_add_change_contract( 'SQ11-085-01', '', 'BUPI12', 'Q', 'ZUEL', 'ASTZ', 'ASTZ', '2011.10.19', '2012.10.18', 'HKD', 5, 441.0, 'N');
f_add_change_contract( 'SQ11-112-01', '', 'CALC16', 'Q', 'DKSH', 'GSK', 'BB', '2011.11.08', '2012.11.07', 'HKD', 20, 90.0, 'N');
f_add_change_contract( 'SQ11-003-01', '', 'CALC32', 'Q', 'ZUEL', 'EBEW', 'EBEW', '2011.09.24', '2012.09.23', 'HKD', 1, 47.1, 'N');
f_add_change_contract( 'SQ10-412-01', '', 'CALC34', 'Q', 'ZUEL', 'ABBO', 'HOSP', '2011.07.15', '2012.07.14', 'HKD', 25, 4725.0, 'N');
f_add_change_contract( 'HOC307-10', 'A', 'CARB71', 'C', 'TIMC', 'PBV', 'PBV', '2011.06.08', '2013.06.07', 'HKD', 1, 237.5, 'N');
f_add_change_contract( 'SQ10-452-01', '', 'CARB72', 'Q', 'ZUEL', 'PFIZ', 'PUJL', '2011.07.23', '2012.07.22', 'HKD', 10, 8500.0, 'N');


commit;

end;
/



set scan on
set serveroutput on

