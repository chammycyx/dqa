alter table institution disable constraint FK_INSTITUTION_01; 
alter table stock_monthly_exp disable constraint FK_STOCK_MONTHLY_EXP_01;
alter table sample_test_schedule disable constraint FK_SAMPLE_TEST_SCHEDULE_04;

set scan off

declare

	procedure f_ins_inst_contact(
		ls_code in varchar2,
		ls_name in varchar2,
		ls_cluster in varchar2,
		ls_pcu_code in varchar2,
		ls_gopc_flag in varchar2,
		ls_pcu_flag in varchar2,
		ls_first_name in varchar2,
		ls_last_name in varchar2,
		ls_position in varchar2,
		ls_title in varchar2,
		ls_office_phone in varchar2,
		ls_fax in varchar2,
		ls_email in varchar2,
		ls_address1 in varchar2,
		ls_address2 in varchar2)
  	is
  		ll_contact_id	number;
  		
	begin
		select contact_id
		into   ll_contact_id
		from   institution
		where  institution_code = ls_code;
		
		update institution
		set institution_name = ls_name,
			institution_cluster = ls_cluster,
			pcu_code = ls_pcu_code,
			gopc_flag = ls_gopc_flag,
			pcu_flag = ls_pcu_flag,
			record_status = 'A',
			modify_date = sysdate,
			version = version + 1
		where institution_code = ls_code;
		
		update contact
		set first_name = nvl(ls_first_name, '-'),
			last_name = ls_last_name,
			position = ls_position,
			title = ls_title,
			office_phone = ls_office_phone,
			fax = ls_fax,
			email = ls_email,
			address1 = ls_address1,
			address2 = ls_address2,
			modify_date = sysdate,
			version = version + 1
		where contact_id = ll_contact_id;		
				
	exception when NO_DATA_FOUND then
	
		insert into contact (
			contact_id, first_name, last_name, position, title, office_phone, fax,
			email, address1, address2,
			create_user, create_date, modify_user, modify_date, version
		) values (
			seq_contact.nextval, nvl(ls_first_name, '-'), ls_last_name, ls_position, ls_title, ls_office_phone, ls_fax, 
			ls_email, ls_address1, ls_address2,
			'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);
		
		insert into institution (
			institution_code, institution_name, institution_cluster, pcu_code, gopc_flag, pcu_flag, contact_id,
			record_status, create_user, create_date, modify_user, modify_date, version
		) values (
			ls_code, ls_name, ls_cluster, ls_pcu_code, ls_gopc_flag, ls_pcu_flag, seq_contact.currval,
			'A', 'dqaadmin', sysdate, 'dqaadmin', sysdate, 1
		);	
					
	end;
	
begin

delete contact where contact_id in ( select contact_id from institution );
delete institution;

f_ins_inst_contact('ABC', 'ANNA BLACK HEALTH CENTER', 'HEC', 'RH', 'Y', 'N', 'ALBERTA', 'LIN', 'PHARMACIST', '-', '22149181', '', '', 'PHARMACY,140 TSAT TSZ MUI ROAD', 'NORTH POINT HK');
f_ins_inst_contact('AHN', 'ALICE H M L NETHERSOLE HOSP.', 'NEC', 'AHN', 'N', 'Y', 'KANDY', 'LAI', 'DM (PHARM)', 'MS', '26892696', '', '', 'MAIN PHARMACY 11 CHUEN ON ROAD', 'TAI PO N.T. ( PHARMACY )');
f_ins_inst_contact('AJC', 'ABERDEEN JOCKEY CLUB CLINIC', 'HWC', 'TWH', 'Y', 'N', 'LILIAN', 'WONG', 'PHARMACIST', '-', '25553091', '', '', 'PHARMACY, 10 ABERDEEN', 'RESERVOIR ROAD, ABERDEEN, HK');
f_ins_inst_contact('ALC', 'AP LEI CHAU CLINIC', 'HWC', 'TWH', 'Y', 'N', 'ALICE', 'HUI', 'DISPENSER', '-', '25185608', '', '', 'PHAR. 161 AP LEI CHAU MAIN ST', 'AP LEI CHAU, HK');
f_ins_inst_contact('BBH', 'BRADBURY HOPSITAL', 'NEC', 'PWH', 'N', 'N', '', '', '', '-', '', '', '', 'BRADBURY HOPSITAL', '');
f_ins_inst_contact('BH', 'HONG KONG BUDDHIST HOSP.(PHAR)', 'KCC', 'QEH', 'N', 'N', 'TS YUEN', 'YC CHAN', 'SEN. DISP', '-', '29586210', '', '', '10 HENG LAM STREET', 'LOK FU KOWLOON');
f_ins_inst_contact('BTS', 'HONG KONG RED CROSS BLOOD TRANSFUSION SERVICE', 'KCC', 'QEH', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('CDC', 'CENTRAL DISTRICT HEALTH CENTRE', 'HWC', 'TWH', 'Y', 'N', 'TERESA', 'LEE', 'PHARMACIST', '-', '25444645', '', '', 'PHARMACY,1 KAU U FONG, CENTRAL', 'HONG KONG');
f_ins_inst_contact('CHC', 'CHESIRE HOME CHUNG HOM KOK', 'HEC', 'PYN', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('CHS', 'CHESIRE HOME SHATIN', 'NEC', 'PWH', 'N', 'N', '', '', '', '-', '', '', '', 'CHESIRE HOME SHATIN', '');
f_ins_inst_contact('CKC', 'CENTRAL KOWLOON HEALTH CENTRE', 'KCC', 'KH', 'Y', 'N', 'KAN YING', 'CHEUNG', 'SEN DISPENSER', '-', '31296676', '', '', 'PHARMACY, 147A ARGYLE STREET', 'KOWLOON');
f_ins_inst_contact('CMC', 'CARITAS MEDICAL CTR. PHARMACY', 'KWC', 'CMC', 'N', 'Y', 'CHING YEE', 'LAM', 'DM (PHARM)', 'MS', '29901050', '', '', '111, WING HONG STREET,', 'SHAM SHUI PO, KOWLOON.');
f_ins_inst_contact('CPH', 'CASTLE PEAK HOSPITAL PHARMACY', 'NWC', 'CPH', 'N', 'Y', 'DYLAN', 'TSE', 'P I/C', 'MR', '24567643', '', '', 'G/F THE JOCKEY CLUB SERENE HSE', '13 CHING CHUNG KOON RD. N.T.');
f_ins_inst_contact('CPM', 'HOSPITAL AUTHORITY', 'HOC', 'CPM', 'N', 'Y', 'ANNA', 'LEE', 'CP, HA', 'MS', '39122456', '', '', 'CHIEF PHARMACIST OFFICE', '10/F BLK A, MULTICENTRE, PYNEH');
f_ins_inst_contact('CPP', 'HOSPITAL AUTHORITY**', 'HOC', 'CPM', 'N', 'N', 'K C', 'TAM', 'CHIEF DISPENSER', 'MR', '39122456', '', '', 'CHIEF PHARMACIST OFFICE', '10/F BLK A, MULTICENTRE, PYNEH');
f_ins_inst_contact('CPT', 'HOSPITAL AUTHORITY*', 'HOC', 'CPM', 'N', 'N', 'KEN', 'LEE', 'PHARMACIST', 'MR', '39125131', '', '', 'CHIEF PHARMACIST OFFICE', '10/F BLK A, MULTICENTRE, PYNEH');
f_ins_inst_contact('CSW', 'CHEUNG SHA WAN JC GOPC', 'KWC', 'CMC', 'N', 'N', '', '', '', '-', '', '', '', '2 KWONG LEE ROAD,', 'CHEUNG SHA WAN, KOWLOON.');
f_ins_inst_contact('CWC', 'CHAI WAN HEALTH CENTRE GOPC', 'HEC', 'PYN', 'Y', 'N', 'PRISCILLA', 'NGAI', 'PHARMACIST', '-', '25561448', '', '', 'PHARMACY,1 HONG MAN STREET,', 'CHAI WAN, HK');
f_ins_inst_contact('DKH', 'THE DUCHESS OF KENT CH`S HOSP', 'HWC', 'DKH', 'N', 'Y', 'LILY', 'CHU', 'P I/C', 'MS', '29740230', '', '', '12 SANDY BAY ROAD', 'HONG KONG ( PHARMACY )');
f_ins_inst_contact('EKC', 'OLMH-EAST KOWLOON GOPC', 'KWC', 'OLM', 'Y', 'N', 'MEI HA', 'CHUNG', 'SEN DISPENSER', '-', '23227865', '', '', '160 HAMMERHILL ROAD,', 'DIAMOND HILL, KOWLOON');
f_ins_inst_contact('EKS', 'EAST KOWLOON SP CL PHARMACY', 'KWC', 'EKS', 'N', 'Y', 'SUI CHUNG', 'CHU', 'P I/C', 'MR', '29901050', '', '', 'HAMMER HILL ROAD', 'KOWLOON');
f_ins_inst_contact('FMC', 'FANLING HEALTH CENTRE', 'NEC', 'NDH', 'Y', 'N', 'STANLEY', 'WONG', 'SEN. DISPENSER', 'MR', '26394618', '', '', 'PHARMACY, 1/F, FANLING HEALTH', 'CENTRE, 2 PIK FUNG RD. FANLING');
f_ins_inst_contact('FYK', 'FUNG YIU KING HOPSITAL', 'HWC', 'DKH', 'N', 'N', 'JOHNNY', 'LAI', 'SEN. DISPENSER', 'MR', '28556104', '', '', '9 SANDY BAY ROAD,', 'HONG KONG');
f_ins_inst_contact('GH', 'GRANTHAM HOSPITAL ( PHARMACY )', 'HWC', 'GH', 'N', 'Y', 'TAMMY', 'SEE', 'DM (PHARM)', 'MS', '25182653', '', '', '125 WONG CHUK HANG ROAD', 'ABERDEEN HONG KONG.');
f_ins_inst_contact('HEC', 'HKE CLUSTER', 'HEC', 'PYN', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('HHC', 'HUNG HOM CLINIC', 'KCC', 'HKE', 'Y', 'N', 'MISS SALLY', 'LAW', 'R. PHARMACIST', '-', '27730758', '', '', 'PHARMACY, 22 STATION LANE', 'HUNGHOM, KOWLOON');
f_ins_inst_contact('HHH', 'HAVEN OF HOPE HOSP. PHARMACY', 'KEC', 'HHH', 'N', 'Y', 'JOSEPH', 'LAU', 'P I/C', 'MR', '27038331', '', '', 'G/F, 8 HAVEN OF HOPE ROAD,', 'TSEUNG KWAN O, KOWLOON, HK');
f_ins_inst_contact('HKC', 'HA KWAI CHUNG POLYCLINIC', 'KWC', 'PMH', 'Y', 'N', 'IVY', 'CHUNG', 'RES. PHARMACIST', '-', '36515402', '', '', 'PHARMCY, 77 LAI CHO ROAD', 'KWAI CHUNG,  NT');
f_ins_inst_contact('HKE', 'HONG KONG EYE HOSP. (PHARMACY)', 'KCC', 'HKE', 'N', 'Y', 'CHI MING', 'NG', 'DM (PHARM)', 'MR', '27623056', '', '', 'L/G 147K ARGYLE STREET,', 'KOWLOON CITY, KOWLOON');
f_ins_inst_contact('KBC', 'KOWLOON BAY HEALTH CENTRE', 'KEC', 'UCH', 'Y', 'N', 'RAYMOND', 'CHEUNG', 'SEN DISPENSER', '-', '21170892', '', '', 'PHARMACY 9 KAI YAN STREET', 'KOWLOON BAY, KOWLOON');
f_ins_inst_contact('KCC', 'KOWLOON CENTRAL CLUSTER', 'KCC', 'QEH', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('KCH', 'KWAI CHUNG HOSPITAL PHARMACY', 'KWC', 'PMH', 'N', 'N', 'TSZ HANG', 'LAI', 'SEN. DISPENSER', '-', '', '', '', 'G/F ADMIN. BLOCK,', '3-15 KWAI CHUNG HOSPITAL RD.');
f_ins_inst_contact('KH', 'KOWLOON HOSPITAL PHARMACY', 'KCC', 'KH', 'N', 'Y', 'MICHAEL', 'CHAN', 'DM (PHARM)', 'MR', '31296676', '', '', 'WEST WING, 147A ARGYLE STREET,', 'KOWLOON, HONG KONG.');
f_ins_inst_contact('KJC', 'KENNEDY TOWN JCC', 'HWC', 'TWH', 'Y', 'N', 'TERESA', 'LEE', 'PHARMACIST', '-', '28176874', '', '', 'PHARMACY, 45 VICTORIA ROAD', 'KENNEDY TOWN HK');
f_ins_inst_contact('KTC', 'KAM TIN CLINIC', 'NWC', 'POH', 'N', 'N', 'CHI PING', 'WONG', 'SEN. DISPENSER', '-', '24686857', '', '', '2/F, PHARMACY, POK OI HOSP.', 'AU TAU, YUEN LONG, N.T.');
f_ins_inst_contact('KTJ', 'KWUN TONG JCC HEALTH CENTRE', 'KEC', 'UCH', 'Y', 'N', 'JOE', 'YIU', 'SEN DISPENSER', '-', '23898701', '', '', 'PHARMACY, 457 KWUN TONG RD', 'KOWLOON');
f_ins_inst_contact('KWC', 'KOWLOON WEST CLUSTER', 'KWC', 'PMH', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('KWH', 'KWONG WAH HOSPITAL PHARMACY', 'KWC', 'KWH', 'N', 'Y', 'MICHAEL', 'LING', 'DM (PHARM)', 'MR', '35175022', '', '', '25 WATERLOO ROAD', 'KOWLOON');
f_ins_inst_contact('LKK', 'LEUNG KAU KUI CLINIC', 'KCC', 'HKE', 'Y', 'N', 'YUET CHEUNG', 'CHEUNG', 'R. PHARMACIST', 'MS', '27126192', '', '', 'PHARMACY, 2/F, TO KWA WAN', 'MARKET & GOVT. OFFICE BLUD.');
f_ins_inst_contact('LKM', 'LEE KEE MEMORIAL DISPENSARY', 'KCC', 'HKE', 'Y', 'N', 'CHRISTINA', 'YAU', 'R. PHARMACIST', '-', '27188427', '', '', 'PHARMACY, 99 CARPENTER RD.', 'KOWLOON CITY, KOWLOON');
f_ins_inst_contact('LPC', 'LI PO CHUN HEALTH CENTRE', 'KWC', 'KWH', 'Y', 'N', 'LIN KUT', 'CHOI', 'SEN. DISPENSER', 'MR', '35175022', '', '', 'PHARMACY, 22 ARRAN STREET', 'KOWLOON');
f_ins_inst_contact('LTC', 'LAM TIN POLYCLINIC', 'KEC', 'UCH', 'Y', 'N', 'SHIRLEY TONG', 'TONG', 'SEN DISPENSER', '-', '29525002', '', '', 'PHARMACY, 99 KAI TIN STREET', 'KWUN TONG KOWLOON');
f_ins_inst_contact('LTP', 'LADY TRENCH GOPC PHARMACY', 'KWC', 'PMH', 'Y', 'N', 'YUK YIN', 'YU', 'DISPENSER', '-', '29426779', '', '', 'PHARMACY, 213 SHA TSUI RD,', 'TSUEN WAN, N.T.');
f_ins_inst_contact('LYH', 'LEK YUEN HEALTH CENTRE', 'NEC', 'PWH', 'Y', 'N', 'PHILOMENA', 'CHAN', 'SEN. DISPENSER', '-', '26928740', '', '', 'PHARMACY, 9 LEK YUEN STREET', 'SHATIN N.T.');
f_ins_inst_contact('MFC', 'MONA FONG GOPC', 'KEC', 'TKO', 'Y', 'N', 'W K', 'SHIU', 'SEN. DISPENSER', '-', '21639227', '', '', 'PHARMACY, 23 MAN NIN STREET', 'SAI KUNG, NT');
f_ins_inst_contact('ML', 'MALEHOLE MEDICAL REHA CENTRE', 'HWC', 'DKH', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('MOS', 'MA ON SHAN CLINIC', 'NEC', 'PWH', 'Y', 'N', 'TOMMY', 'TANG', 'SEN. DISPENSER', '-', '26414221', '', '', 'PHARMACY, 609 SAI SHA ROAD', 'MA ON SHAN, N.T.');
f_ins_inst_contact('MWC', 'MUI WO CLINIC', 'KWC', 'PMH', 'Y', 'N', 'KIN CHUNG', 'HO', 'SEN. DISPENSER', '-', '29843311', '', '', 'PHARMACY, LG2, MAIN BLOCK,', 'PMH, LAI CHI KOK');
f_ins_inst_contact('MWH', 'MRS WU YORK YU GOPC PHARMACY', 'KWC', 'PMH', 'Y', 'N', 'YUK LING', 'CHAN', 'SEN. DISPENSER', '-', '24818987', '', '', 'PHARMACY, 310 WO YI HOP ROAD', 'LEI MUK SHUE KWAI CHUNG NT');
f_ins_inst_contact('NDH', 'NORTH DISTRICT HOSPITAL', 'NEC', 'NDH', 'N', 'Y', 'STEPHEN', 'CHEN', 'DM (PHARM)', 'MR', '26837020', '', '', 'LG CENTRAL PHARMACY,', '9 PO KIN ROAD,SHEUNG SHUI , NT');
f_ins_inst_contact('NKC', 'NORTH KWAI CHUNG GOPC', 'KWC', 'PMH', 'Y', 'N', 'CHUNG YING', 'CHAN', 'RES. PHARMACIST', '-', '24251569', '', '', 'PHARMACY, G/F, 125 TAI PAK TIN', 'STREET, NORTH KWAI CHUNG, TW');
f_ins_inst_contact('NLC', 'NORTH LAMMA CLINIC', 'HEC', 'RH', 'Y', 'N', 'BARRY', 'AU', 'SEN. DISPENSER', '-', '22912076', '', '', 'RH(PHARMCY) 266 QUEEN`S ROAD E', 'WAN CHAI H.K.');
f_ins_inst_contact('NSH', 'NAM SHAN GOPC', 'KWC', 'PMH', 'Y', 'N', 'GRACE', 'LEUNG', 'RES. PHARMACIST', '-', '27780606', '', '', 'PHARMACY, G/F, NAM YIU HOUSE', 'NAM SHAN ESTATE KWOLOON');
f_ins_inst_contact('NTK', 'NGAU TAU KOK JCC', 'KEC', 'UCH', 'Y', 'N', 'ANGELA', 'WONG', 'SEN DISPENSER', '-', '27505418', '', '', 'PHARMACY, 60 TING ON STREET', 'NGAU TAU KOK, KOWLOON');
f_ins_inst_contact('OLM', 'OUR LADY OF MARYKNOLL HOSPITAL', 'KWC', 'OLM', 'N', 'Y', 'MARY', 'LAU', 'DM (PHARM)', 'MS', '23542446', '', '', '118 SHATIN PASS ROAD,WONG TAI', 'SIN KLN , HK ( PHARMACY )');
f_ins_inst_contact('PCC', 'PENG CHAU CLINIC', 'HEC', 'RH', 'Y', 'N', 'BARRY', 'AU', 'SEN. DISPENSER', '-', '22912076', '', '', 'RH(PHARMACY), 266 QUEEN`S RD E', 'WAN CHAI H.K.');
f_ins_inst_contact('PHQ', 'PHARMACEUTICAL HEADQUARTER', 'HOC', 'PHQ', 'N', 'Y', 'S. C.', 'CHIANG', 'SP (CPO)', 'MS', '25151638', '', '', '10/F , BLOCK A , MULTICENTRE', 'CENTRE, PYNEH , CHAIWAN H.K.');
f_ins_inst_contact('PMH', 'PRIN. MARGARET HOSP. PHARMACY', 'KWC', 'PMH', 'N', 'Y', 'ROSA', 'YAO', 'DM (PHARM)', 'MS', '29901051', '', '', 'LAI KING HILL ROAD,', 'LAI CHI KOK,NTW H.K.');
f_ins_inst_contact('PNC', 'TKO PO NING RD HEALTH CENTRE', 'KEC', 'TKO', 'Y', 'N', 'W F', 'NG', 'SEN. DISPENSER', 'MS', '21911017', '', '', 'PHARMACY, 28 PO NING ROAD', 'TSEUNG KWAN O, NT');
f_ins_inst_contact('POH', 'POK OI HOSPITAL', 'NWC', 'POH', 'N', 'Y', 'AGNES', 'TAM', 'P I/C', 'MS', '24868557', '', '', 'M/F PHARMACY, POK OI HOSPITAL', 'AU TAU, YUEN LONG, N.T.');
f_ins_inst_contact('PWH', 'PRINCE OF WALES HOSP. PHARMACY', 'NEC', 'PWH', 'N', 'Y', 'BENJAMIN', 'LEE', 'DM (PHARM)', 'MR', '26322240', '', '', 'G/F, 30-32 NGAN SHING STREET', 'SHATIN, N.T.E.');
f_ins_inst_contact('PYN', 'PAMELA YOUDE NETHERSOLE EASTERN HOSPITAL', 'HEC', 'PYN', 'N', 'Y', 'SAI LEUNG', 'CHAN', 'DM (PHARM)', 'MR', '25956128', '', '', 'PHARMACY, MAIN BLOCK,', '3,LOK MAN RD, CHAIWAN, HK');
f_ins_inst_contact('QEH', 'QUEEN ELIZABETH HOSP. PHARMACY', 'KCC', 'QEH', 'N', 'Y', 'KENNETH', 'LAW', 'DM (PHARM)', 'MR', '29586210', '', '', 'WYLIE ROAD, KOWLOON', 'HONG KONG');
f_ins_inst_contact('QES', 'QEH SPECIALIST CLINIC PHARMACY', 'KCC', 'QES', 'N', 'Y', 'CAROL', 'LAU', 'P I/C', 'MS', '29585943', '', '', '1 WYLIE ROAD', 'KOWLOON');
f_ins_inst_contact('QMH', 'QUEEN MARY HOSP. PHARMACY', 'HWC', 'QMH', 'N', 'Y', 'WILLIAM', 'CHUI', 'DM (PHARM)', 'MR', '28553233', '', '', 'BLOCK K LG1', 'POKFULAM ROAD , HONG KONG.');
f_ins_inst_contact('RBC', 'ROBERT BLACK HEALTH CENTRE', 'KWC', 'KWH', 'Y', 'N', 'LIN KUT', 'CHOI', 'SEN. DISPENSER', 'MR', '35175022', '', '', 'PHARMACY, 600 PRINCE EDWARD E,', 'SAN PO KONG KWOLOON');
f_ins_inst_contact('RH', 'RUTTONJEE HOSPITAL PHARMACY', 'HEC', 'RH', 'N', 'Y', 'AMBROSE', 'KWAN', 'P I/C', 'MR', '22912078', '', '', '266 QUEEN`S ROAD EAST', 'WAN CHAI, HONG KONG');
f_ins_inst_contact('SH', 'SHATIN HOSPITAL PHARMACY', 'NEC', 'PWH', 'N', 'N', '', '', '', '-', '', '', '', '33 A KUNG KOK STREET,', 'MA ON SHAN, SHATIN, N.T.');
f_ins_inst_contact('SJH', 'ST JOHN HOSPITAL', 'HEC', 'PYN', 'N', 'N', 'KAREN', 'CHOY', 'SENIOR DISPENSER', '-', '29810379', '', '', 'CHEUNG CHAU HOSPITAL ROAD,', 'TUNG WAN, CHEUNG CHAU');
f_ins_inst_contact('SKJ', 'SOUTH KWAI CHUNG JCC GOPC', 'KWC', 'PMH', 'Y', 'N', 'DAISY', 'LAM', 'RES. PHARMACIST', '-', '24067992', '', '', 'PHARMACY,310 KWAISHING CIRCUIT', 'KWAI CHUNG, TSUEN WAN, NT');
f_ins_inst_contact('SKM', 'SHEK KIP MEI GOPC', 'KWC', 'PMH', 'Y', 'N', 'PETER', 'TSE', 'PHARMACIST', '-', '27797359', '', '', 'PHARMACY, 2 BERWICK STREET', 'SHEK KIP MEI, KOWLOON');
f_ins_inst_contact('SKW', 'SHAUKEIWAN JOCKEY CLUB GOPC', 'HEC', 'PYN', 'Y', 'N', 'MS. S Y', 'AU', 'PHARMACIST', '-', '25600214', '', '', 'PHARMACY, 8 CHAI WAN ROAD', 'SHAU KI WAN, HK');
f_ins_inst_contact('SLG', 'SHUN LEE GOVERNMENT CLINIC', 'KEC', 'UCH', 'Y', 'N', 'GRACE', 'HO', 'SEN DISPENSER', '-', '23894258', '', '', 'PHARMACY, G/F, LEE FOO HOUSE', 'SHUN LEE ESTATE KOWLOON');
f_ins_inst_contact('SLH', 'SIU LAM HOSPITAL', 'NWC', 'TMH', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('SPD', 'STANLEY PUBLIC DISPENSARY', 'HEC', 'PYN', 'Y', 'N', 'RICHARD', 'CHAN', 'DISPENSER', 'MR', '28136240', '', '', 'PHARMACY, 14 WONG MA KOK ROAD', 'STANLEY HONG KONG');
f_ins_inst_contact('STC', 'SHATIN CLINIC', 'NEC', 'PWH', 'Y', 'N', 'ANDY', 'LI', 'SEN. DISPENSER', '-', '26911647', '', '', 'PHARMACY, 3-9 MAN LAI ROAD,', 'TAI WAI SHATIN N.T.');
f_ins_inst_contact('STK', 'SHA TAU KOK CLINIC', 'NEC', 'NDH', 'Y', 'N', 'CHI MAN', 'TSANG', 'SEN. DISPENSER', '-', '26711623', '', '', 'PHARMACY, 58 SHA TAU KOK ROAD,', 'SHA TAU KOK N.T.');
f_ins_inst_contact('SWC', 'SOK KWU WAN CLINIC', 'HEC', 'RH', 'Y', 'N', 'BARRY', 'AU', 'SEN. DISPENSER', '-', '22912076', '', '', 'RH(PHARMACY) 266 QUEEN`S RD E', 'WAN CHAI H.K.');
f_ins_inst_contact('SWH', 'SAI WAN HO HEALTH CENTRE', 'HEC', 'PYN', 'Y', 'N', 'Y K', 'HO', 'SEN. DISPENSER', '-', '25355200', '', '', 'PHARMACY 1/F., 28 TAI HONG ST', 'SAI WAN HO, HONG KONG');
f_ins_inst_contact('SWJ', 'SHEK WU HUI JCC', 'NEC', 'NDH', 'Y', 'N', 'CHI MAN', 'TSANG', 'SEN. DISPENSER', '-', '26711623', '', '', 'PHARMACY, JOCKEY CLUB RD,', 'SHEK WU HUI, SHEUNG SHUI, NT');
f_ins_inst_contact('SYG', 'SAI YING PUN J C GOPC', 'HWC', 'SYG', 'N', 'Y', 'JANET', 'PANG', 'DM (PHARM)', 'MS', '25898395', '', '', '1/F, 134 QUEEN`S ROAD WEST', 'SAI YING PUN, HONG KONG');
f_ins_inst_contact('TCH', 'TUNG CHUNG HEALTH CENTRE', 'KWC', 'PMH', 'Y', 'N', 'MICHAEL', 'LI', 'RES. PHARMACIST', '-', '21090008', '', '', 'PHARMACY, FU TUNG STREET,', 'TUNG CHUNG, LANTAU ISLAND');
f_ins_inst_contact('TKJ', 'TSEUNG KWAN O JC CLINIC', 'KEC', 'TKO', 'N', 'N', '', '', '', '-', '25152479', '', '', '99 PO LAM NORTH ROAD,', 'TSEUNG KWAN O');
f_ins_inst_contact('TKL', 'TA KWU LING CLINIC', 'NEC', 'NDH', 'Y', 'N', 'CHI MAN', 'TSANG', 'SEN. DISPENSER', '-', '26711623', '', '', 'C/O PHARMACY, JOCKEY CLUB RD,', 'SHEK WU HUI, SHEUNG SHUI, NT');
f_ins_inst_contact('TKO', 'TSEUNG KWAN O HOSPITAL', 'KEC', 'TKO', 'N', 'Y', 'KATHY', 'MAK', 'DM (PHARM)', 'MS', '22080044', '', '', 'L/G FLOOR, 2 PO NING LANE', 'HANG HAU, TSEUNG KWAN O, KLN');
f_ins_inst_contact('TMC', 'TUEN MUN CLINIC', 'NWC', 'TMH', 'Y', 'N', 'KWOK', 'KWOK', 'SD', 'MR', '24685138', '', '', 'PHARMACY, 11 TSING YIN STREET', 'TUEN MUN SAN HUI TUEN MUN');
f_ins_inst_contact('TMH', 'TUEN MUN HOSPITAL PHARMACY', 'NWC', 'TMH', 'N', 'Y', 'PAULINE', 'CHU', 'DM (PHARM)', 'MS', '24685138', '', '', 'G/F,MAIN BLK,TUEN MUN HOSPITAL', 'CHING CHUNG KOON RD,TUEN MUN.');
f_ins_inst_contact('TOC', 'TAI O JOCKEY CLUB CLINIC', 'KWC', 'PMH', 'Y', 'N', 'CHUNG HOI', 'LEE', 'DISPENSER', '-', '29857204', '', '', 'PHARMACY, LG2, MAIN BLOCK,', 'PMH, LAI CHI KOK');
f_ins_inst_contact('TPC', 'TAI PO JCC', 'NEC', 'AHN', 'Y', 'N', 'MS. T', 'CHAN', 'SEN. DISPENSER', '-', '26899515', '', '', 'PHARMACY, 37 TING KOK ROAD', 'TAI PO, NT');
f_ins_inst_contact('TPH', 'TAI PO HOSPITAL PHARMACY', 'NEC', 'AHN', 'N', 'N', '', '', '', '-', '', '', '', 'G/F, WING B, MAIN BUILDING,', '9 CHUEN ON ROAD, TAI PO, N.T.');
f_ins_inst_contact('TSW', 'TIN SHUI WAI HEALTH CENTRE', 'NWC', 'POH', 'Y', 'N', 'CHI PING', 'WONG', 'SEN. DISPENSER', '-', '24686857', '', '', 'PHARMACY, AREA 26,', '3 TIN SHUI ROAD, TIN SHUI WAI');
f_ins_inst_contact('TWE', 'TUNG WAH EASTERN HOSPITAL', 'HEC', 'TWE', 'N', 'Y', 'KIT WING', 'LO', 'P I/C', 'MR', '21626016', '', '', '19 EASTERN HOSPITAL ROAD,', 'HONG KONG.( PHARMACY )');
f_ins_inst_contact('TWH', 'TUNG WAH HOSPITAL ( PHARMACY )', 'HWC', 'TWH', 'N', 'Y', 'JANET', 'PANG', 'DM (PHARM)', 'MS', '25898395', '', '', '12 PO YAN STREET', 'HONG KONG');
f_ins_inst_contact('TYC', 'TSING YI CHEUNG HONG CLINIC', 'KWC', 'PMH', 'Y', 'N', 'VINCENT', 'FUNG', 'RES. PHARMACIST', '-', '24363451', '', '', 'PHARMACY, 3/F CHEUNG HONG COMM', 'COMPLEX, CHEUNG HONG ESTATE');
f_ins_inst_contact('TYH', 'TSAN YUK HOSPITAL , PHARMACY', 'HWC', 'TYH', 'N', 'Y', 'ALLEN', 'LEUNG', 'DM (PHARM)', 'MR', '25892329', '', '', '30 HOSPITAL ROAD, HONG KONG.', 'NULL');
f_ins_inst_contact('TYT', 'TSING YI TOWN CLINIC', 'KWC', 'PMH', 'Y', 'N', 'TSZ NOK', 'WONG', 'RES. PHARMACIST', '-', '24337150', '', '', 'PHARMACY, TSING LUK STREET', 'TSING YI, N.T.');
f_ins_inst_contact('UCH', 'UNITED CHRISTIAN HOSP PHARMACY', 'KEC', 'UCH', 'N', 'Y', 'KATHY', 'MAK', 'DM (PHARM)', 'MS', '35134173', '', '', '130 HIP WO STREET,', 'KOWLOON');
f_ins_inst_contact('VPC', 'VIOLET PEEL HEALTH CENTRE', 'HEC', 'RH', 'Y', 'N', 'CLARA', 'IP', 'SEN. DISPENSER', '-', '35533109', '', '', 'PHARM.L/G, TANG SHIU KIN HOSP.', '282 QUEEN`S ROAD EAST, WANCHAI');
f_ins_inst_contact('WCH', 'WONG CHUK HANG HOSPITAL', 'HEC', 'PYN', 'N', 'N', '', '', '', '-', '', '', '', '', '');
f_ins_inst_contact('WHC', 'TUEN MUN WU HONG CLINIC', 'NWC', 'TMH', 'Y', 'N', 'KWOK', 'KWOK', 'SD', 'MR', '24685138', '', '', 'PHARMACY AREA 44, 2 WU HONG ST', 'TUEN MUN N.T.');
f_ins_inst_contact('WKC', 'WEST KOWLOON GOPC', 'KWC', 'PMH', 'Y', 'N', 'JOSEPHINE', 'LUI', 'PHARMACIST', '-', '21520030', '', '', 'PHARMACY, G/F, CHEUNG SHA WAN', 'GOV OFF 303 CHEUNG SHA WAN RD');
f_ins_inst_contact('WPC', 'WESTERN PSYCHIATRIC CENTRE', 'HWC', 'WPC', 'N', 'Y', 'ELKI', 'SIU', 'P I/C', 'MS', '28583827', '', '', '5/F,PHARMACY,DAVID TRENCH', 'REHAB.CENTRE,9B,BONHAM RD, HK');
f_ins_inst_contact('WSC', 'TAI PO WONG SIU CHING CLINIC', 'NEC', 'AHN', 'Y', 'N', 'WILLIAM', 'WONG', 'PHARMACIST', 'MR', '26568006', '', '', 'PHARMACY, 1 PO WU LANE,', 'TAI PO N.T.');
f_ins_inst_contact('WTG', 'WAN TSUI GOVERNMENT CLINIC', 'HEC', 'PYN', 'Y', 'N', 'GRACE', 'CHEUNG', 'PHARMACIST', 'MS', '28975519', '', '', 'PHAR G/F, BLOCK 12 LEE TSUI HS', 'WAN TSUI ESTATE CHAI WAN HK');
f_ins_inst_contact('WTH', 'WANG TAU HOM JCC', 'KWC', 'KWH', 'Y', 'N', 'LIN KUT', 'CHOI', 'SEN. DISPENSER', 'MR', '35175022', '', '', 'PHARMACY 200 JUNCTION RD', 'WANG TAU HOM KWOLOON');
f_ins_inst_contact('WTS', 'WONG TAI SIN HOSPITAL PHARMACY', 'KWC', 'KWH', 'N', 'N', '', '', '', '-', '35175022', '', '', '124 SHATIN PASS ROAD,', 'KOWLOON.');
f_ins_inst_contact('WYY', 'WU YORK YU HEALTH CENTRE', 'KWC', 'KWH', 'Y', 'N', 'LIN KUT', 'CHOI', 'SEN. DISPENSER', 'MR', '35175022', '', '', 'PHARMACY, 55 SHEUNG FUNG ST.', 'TSZ WAN SHAN KOWLOON');
f_ins_inst_contact('YCH', 'YAN CHAI HOSPITAL ,', 'KWC', 'YCH', 'N', 'Y', 'KWOK WAH', 'WAN', 'DM (PHARM)', 'MR', '29901050', '', '', 'B4 PHARMACY , 7-11 YAN CHAI', 'STREET , TSUEN WAN , N.T.');
f_ins_inst_contact('YCK', 'YUEN CHAU KOK CLINIC', 'NEC', 'PWH', 'Y', 'N', 'SAM', 'WONG', 'SEN. DISPENSER', '-', '26867409', '', '', 'PHARMACY,31-33 CHAP WAI KON ST', 'SHATIN N.T.');
f_ins_inst_contact('YFC', 'MADAM YUNG FUNG SHEE HC', 'NWC', 'POH', 'Y', 'N', 'CHI PING', 'WONG', 'SEN. DISPENSER', '-', '24868557', '', '', 'PHARMACY, 26 SAI CHING STREET,', 'YUEN LONG, N.T');
f_ins_inst_contact('YFS', 'YUNG FUNG SHEE SP CL PHARMACY', 'KEC', 'YFS', 'N', 'Y', 'CHI KEUNG', 'CHAN', 'P I/C', 'MR', '27278224', '', '', '4 CHA KOW LING ROAD, KWUN TONG', 'KOWLOON');
f_ins_inst_contact('YJC', 'YAU MA TEI JCC', 'KCC', 'YSC', 'N', 'N', 'H L', 'FUNG', 'SEN DISPENSER', 'MS', '23844384', '', '', '1/F, PHARMACY , OLD WING, 145', 'BATTERY STREET, YAU MA TEI');
f_ins_inst_contact('YLC', 'YUEN LONG JCC HEALTH CLINIC', 'NWC', 'POH', 'Y', 'N', 'CHI PING', 'WONG', 'SEN. DISPENSER', '-', '24868557', '', '', 'PHARMACY, 269 CASTLE PEAK ROAD', 'YUEN LONG N.T.');
f_ins_inst_contact('YOP', 'TMH YAN OI GOPC PHARMACY', 'NWC', 'TMH', 'Y', 'N', 'KWOK', 'KWOK', 'SD', 'MR', '24685138', '', '', '1/F, TUEN MUN EYE CENTRE,', '4 TUEN LEE ST. TUEN MUN, N.T.');
f_ins_inst_contact('YPC', 'WEST KOWLOON PSYCHIATRIC CTR.', 'KWC', 'YPC', 'N', 'Y', 'YAO', 'YAO', 'DM (PHARM)', 'MS', '29901051', '', '', 'PHARMACY, 10/F, BLOCK K', 'PMH, LAI KING HILL ROAD. KLN.');
f_ins_inst_contact('YSC', 'YAUMATEI SPEC.CLINIC(PHARMACY)', 'KCC', 'YSC', 'N', 'Y', 'KENNETH', 'LAW', 'DM (PHARM)', 'MR', '23594416', '', '', '143, BATTERY STREET,', 'YAU MA TEI, KOWLOON');


commit;
 
end;
/

set scan on

alter table institution enable constraint FK_INSTITUTION_01;
alter table stock_monthly_exp enable constraint FK_STOCK_MONTHLY_EXP_01;
alter table sample_test_schedule enable constraint FK_SAMPLE_TEST_SCHEDULE_04;
