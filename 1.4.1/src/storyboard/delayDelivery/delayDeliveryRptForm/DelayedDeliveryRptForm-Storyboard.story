<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="rGPjxYryjnORq_LsSNw7n2JS9pI=" x="675" y="1275">
    <screen href="Drafted.screen#/"/>
  </panels>
  <panels id="LbHYuzsdQgx4whVTKF3lLhCYNdU=" x="975" y="1275">
    <screen href="Drafted_Enquiry-InstLookup.screen#/"/>
  </panels>
  <panels id="g99eDHYnENDkhlc1asHREvFdBdo=" x="75" y="1575">
    <screen href="Drafted_Enquiry-ItemCodeLookup.screen#/"/>
  </panels>
  <panels id="zEC6_BfIrXHtQRb9Ng8FV-YtMaw=" x="375" y="1575">
    <screen href="Enquiry.screen#/"/>
  </panels>
  <panels id="RRXhzE2BtaCBaBLzJqVarsbdxWY=" x="675" y="1575">
    <screen href="Enquiry-StatusLookup.screen#/"/>
  </panels>
  <panels id="TD2yQl3wfuIrdXtTbrdKlcpaTVI=" x="975" y="1575">
    <screen href="Drafted-DelayDeliveryRptForm.screen#/"/>
  </panels>
  <panels id="DGi42DG7P-qMPSh_gsOd1dNVJ9M=" x="75" y="1875">
    <screen href="Drafted-RptForm-BpaLookup.screen#/"/>
  </panels>
  <panels id="YjfCr2HvIT9sbx5x8R8BQ1JrWnw=" x="375" y="1875">
    <screen href="Drafted-RptForm-SupplierCodeLookup.screen#/"/>
  </panels>
  <panels id="L0qFRw2GeA8zP25LhMYcpc9EssM=" x="675" y="1875">
    <screen href="Drafted-RptForm-ManufCodeLookup.screen#/"/>
  </panels>
  <panels id="QnPAFlSjeMa-RW-MEngjZENP-LU=" x="975" y="1875">
    <screen href="Drafted-RptForm-AddPoDetail.screen#/"/>
  </panels>
  <panels id="3i4GKPsZdNzUuNtddEKHYYdxxNI=" x="75" y="2175">
    <screen href="Drafted-RptForm-AddContact.screen#/"/>
  </panels>
  <panels id="k_dGRM_EjZtRpC-04YbQHDzDRLc=" x="375" y="2175">
    <screen href="Drafted-RptForm-SelectRecentContact.screen#/"/>
  </panels>
  <panels id="1zXwKOXTFv7bjyKGw09hrM_Z5f0=" x="675" y="2175">
    <screen href="Drafted-RptForm-DeclarationOfConflictOfInterest.screen#/"/>
  </panels>
  <panels id="hh7fe8KwWvtDxsu_oMqNespfCMU=" x="975" y="2175">
    <screen href="Drafted-RptForm-DeclarationOfConflictOfInterest-UploadFile.screen#/"/>
  </panels>
  <panels id="LgCtZb8E1X-dkiJ3X7m91S-k3nY=" x="75" y="75">
    <screen href="Enquiry-DelayDeliveryRptForm.screen#/"/>
  </panels>
</story:Storyboard>
