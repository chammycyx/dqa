<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="75" y="75">
    <screen href="maintenance/supplierPerformance/Nature%20Of%20Problem%20Maintenance.screen#/"/>
  </panels>
  <panels x="75" y="2175">
    <screen href="supplierPerformance/Problem%20Form%20(Pharm%20User)%20-%20Drafts.screen#/"/>
  </panels>
  <panels x="375" y="2175">
    <screen href="supplierPerformance/Problem%20Form%20(Pharm%20User)%20-%20Submitted.screen#/"/>
  </panels>
  <panels x="375" y="75">
    <screen href="supplierPerformance/Problem%20Form%20(Pharm%20User)%20-%20Nature%20of%20Problem%20Popup.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20New%20Problem.screen#/"/>
  </panels>
  <panels x="75" y="375">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20Problem%20Form%20Enquiry.screen#/"/>
  </panels>
  <panels x="375" y="375">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20Case%20Enquiry.screen#/"/>
  </panels>
  <panels x="675" y="375">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20Batch%20Suspension%20Institution%20List.screen#/"/>
  </panels>
  <panels x="75" y="675">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20Confirm%20Problem%20Form-%20Copy%20Batch%20Number.screen#/"/>
  </panels>
  <panels x="975" y="375">
    <screen href="supplierPerformance/Problem%20Form%20(QA%20User)%20-%20Case%20Enquiry%20-%20Fax%20Summary.screen#/"/>
  </panels>
  <panels x="975" y="675">
    <screen href="supplierPerformance/Email%20Option%20-%20Initial%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="75" y="975">
    <screen href="supplierPerformance/Email%20Option%20-%20Initial%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="375" y="975">
    <screen href="supplierPerformance/Email%20Option%20-%20Interim%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="675" y="975">
    <screen href="supplierPerformance/Email%20Option%20-%20Interim%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="375" y="675">
    <screen href="supplierPerformance/Email%20Option%20-%20Final%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="675" y="675">
    <screen href="supplierPerformance/Email%20Option%20-%20Final%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="975" y="975">
    <screen href="supplierPerformance/Fax%20Enquiry%20-%20Fax%20Detail%20Log.screen#/"/>
  </panels>
  <panels x="75" y="1275">
    <screen href="supplierPerformance/Fax%20Enquiry%20-%20Fax%20Summary.screen#/"/>
  </panels>
  <panels x="375" y="1275">
    <screen href="supplierPerformance/Supplier%20Performance%20File%20Enquiry.screen#/"/>
  </panels>
  <panels x="675" y="1275">
    <screen href="supplierPerformance/Batch%20Suspension%20Quantity.screen#/"/>
  </panels>
  <panels x="975" y="1875">
    <screen href="supplierPerformance/Pharm%20Problem%20Report.screen#/"/>
  </panels>
  <panels x="375" y="1575">
    <screen href="supplierPerformance/Fax%20Template%20-%20Initial%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="75" y="1575">
    <screen href="supplierPerformance/Fax%20Template%20-%20Initial%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="975" y="1575">
    <screen href="supplierPerformance/Fax%20Template%20-%20Interim%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="675" y="1575">
    <screen href="supplierPerformance/Fax%20Template%20-%20Interim%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="375" y="1875">
    <screen href="supplierPerformance/Fax%20Template%20-%20Final%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="75" y="1875">
    <screen href="supplierPerformance/Fax%20Template%20-%20Final%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="675" y="1875">
    <screen href="supplierPerformance/Case%20Final%20Report.screen#/"/>
  </panels>
</story:Storyboard>
