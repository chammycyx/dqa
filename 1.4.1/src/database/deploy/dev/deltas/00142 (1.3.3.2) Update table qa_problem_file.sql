alter table QA_PROBLEM_FILE add SHARE_FLAG varchar2(1) default 'N';
comment on column QA_PROBLEM_FILE.SHARE_FLAG is 'Share to Pharmacist Problem Form';

--//@UNDO
alter table QA_PROBLEM_FILE drop column SHARE_FLAG;
--//
