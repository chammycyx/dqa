create table system_message (	 
	application_id number(10) not null,
	function_id number(10) not null,
	operation_id number(10) null,
	severity_code varchar2(1) not null,
	message_code varchar2(5) not null, 
	locale varchar2(5) not null,
	main_msg varchar2(300) not null, 
	supp_msg varchar2(255) null, 
	details varchar2(255) null, 
	btn_yes_caption varchar2(30) null, 
	btn_yes_size varchar2(1) null, 
	btn_yes_shortcut varchar2(1) null, 
	btn_no_caption varchar2(30) null, 
	btn_no_size varchar2(1) null, 
	btn_no_shortcut varchar2(1) null, 
	btn_cancel_caption varchar2(30) null,
	btn_cancel_size varchar2(1) null,
	btn_cancel_shortcut varchar2(1) null, 		 
	effective timestamp null,
	expiry timestamp null, 	
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null, 	
	constraint pk_system_message primary key (message_code) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table system_message;
--//
