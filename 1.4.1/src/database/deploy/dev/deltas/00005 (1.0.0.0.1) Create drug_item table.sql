create table drug_item (
	item_code varchar2(6) not null, 
	item_name varchar2(200) not null, 
	suspend_flag varchar2(1) null, 
	base_unit varchar2(4) null,		
	commodity_group varchar2(1) null, 	
	order_type varchar2(1) null, 	
	upload_date timestamp not null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_drug_item primary key (item_code) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table drug_item;
--//
