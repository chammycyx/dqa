create table supplier (
	supplier_code varchar2(5) not null, 
	supplier_name varchar2(100) not null,
	contact_id number(19) null,
	suspend_flag varchar2(1) null,
	upload_date timestamp null,
	create_user varchar2(15) not null, 
	create_date timestamp not null,
	modify_user varchar2(15) not null, 
	modify_date timestamp not null,  
	version number(19) not null, 
	constraint pk_supplier primary key (supplier_code) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table supplier;
--//