create table DLD_RPT_FORM (
REF_NUM number(9,0) not null,
DLD_RPT_FORM_VER_ID number(19,0) ,
INSTITUTION_CODE varchar2(3) not null,
CONTRACT_ID number(19,0),
REPORT_DATE timestamp(6),
ON_HAND_QTY number(12,0) ,
AVG_MTH_CONSUMPT_QTY number(12,0) ,
ON_HAND_LAST_FOR number(12,2) ,
CONFLICT_MSP_NAME varchar2(15),
CONFLICT_RELATIONSHIP varchar2(150),
CONFLICT_BY_NAME varchar2(150),
CONFLICT_BY_RANK varchar2(19),
CONFLICT_DATE timestamp(6) ,
CONFLICT_FLAG number(1,0) not null,
CONFLICT_FILE_ID number(19,0),
CREATE_USER	varchar2(15) not null,
CREATE_DATE	timestamp(6) not null,
MODIFY_USER	varchar2(15) not null,
MODIFY_DATE	timestamp(6) not null,
VERSION	number(19,0) not null,
constraint PK_DLD_RPT_FORM primary key(REF_NUM) using index tablespace DQA_INDX_01)
tablespace DQA_DATA_01;

create table DLD_RPT_FORM_PO (
DLD_RPT_FORM_PO_ID number(19,0) not null,
REF_NUM number(9,0) not null,
PO_NUM number(19,0) not null,
APPROVE_DATE timestamp(6) not null,
ORDER_QTY number(12,0) not null,
OUTSTAND_QTY number(12,0) not null,
QUOTATION_LEAD_TIME number(12,0),
SUPPLIER_REPLY_DATE timestamp(6),
FILE_ID number(19,0),
CREATE_USER	varchar2(15) not null,
CREATE_DATE	timestamp(6) not null,
MODIFY_USER	varchar2(15) not null,
MODIFY_DATE	timestamp(6) not null,
VERSION	number(19,0) not null,
constraint PK_DLD_RPT_FORM_PO primary key(DLD_RPT_FORM_PO_ID) using index tablespace DQA_INDX_01)
tablespace DQA_DATA_01;

create table DLD_RPT_FORM_VER (
DLD_RPT_FORM_VER_ID number(19,0) not null,
REF_NUM number(9,0) not null,
STATUS varchar2(1) not null,
REASON varchar2(500),
CREATE_USER	varchar2(15) not null,
CREATE_DATE	timestamp(6) not null,
MODIFY_USER	varchar2(15) not null,
MODIFY_DATE	timestamp(6) not null,
VERSION	number(19,0) not null,
constraint PK_DLD_RPT_FORM_VER primary key(DLD_RPT_FORM_VER_ID) using index tablespace DQA_INDX_01)
tablespace DQA_DATA_01;

create table DLD_RPT_FORM_CONTACT (
DLD_RPT_FORM_CONTACT_ID number(19,0) not null,
REF_NUM number(9,0) not null,
CONTACT_ID number(19,0)  not null,
PRIORITY number(3,0)  not null,
CREATE_USER	varchar2(15 ) not null,
CREATE_DATE	timestamp(6) not null,
MODIFY_USER	varchar2(15 ) not null,
MODIFY_DATE	timestamp(6) not null,
VERSION	number(19,0) not null,
constraint PK_DLD_RPT_FORM_CONTACT primary key(DLD_RPT_FORM_CONTACT_ID) using index tablespace DQA_INDX_01)
tablespace DQA_DATA_01;

create sequence SEQ_DLD_RPT_FORM increment by 50 start with 10049;
create sequence SEQ_DLD_RPT_FORM_PO increment by 50 start with 10049;
create sequence SEQ_DLD_RPT_FORM_VER increment by 50 start with 10049;
create sequence SEQ_DLD_RPT_FORM_CONTACT increment by 50 start with 10049;

insert into FUNC_SEQ_NUM (FUNC_CODE, FUNC_DESC, CREATE_USER, END_NUM, MODIFY_USER, START_NUM, FUNC_PREFIX, LAST_NUM, MODIFY_DATE, CREATE_DATE, VERSION, YEAR) values ('DDN', 'DELAY DELIVERY RPT FORM NUMBER', 'dqaadmin', '99999', 'dqaadmin', '1', 'DDN', '0', sysdate, sysdate, '1', '2016');

insert into LETTER_TEMPLATE (CODE, NAME, CONTENT, RECORD_STATUS, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION, JAXB_FLAG) values ('DLD_DOCI', 'Declaration of Conflict of Interest', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><LetterContent><subjectInfo>Pharmaceutical Delayed Delivery Report  (Declaration of Conflict of Interest)</subjectInfo><paragraph>In order to uphold and protect HA''s and your reputation, you should avoid obligations to business associates resulting from advantages, gifts or entertainment received in, or due to your official capacity which could compromise your position in any way. As a personal responsibility, you should declare any potential conflict of interest and abstain from engaging in situations that may lead to perceived bias in decision-making during the reporting.</paragraph><paragraph>In respect of the pharmaceutical delayed delivery reporting, are you or any member of your family or close personal friends (e.g. friends who know each other for a long time and have frequent social relationships), a director, officer, sole owner, partner, employee, consultant or advisor to any of the Company/Manufacturer/Supplier under this report:</paragraph></LetterContent>', 'A', 'dqaadmin', sysdate, 'dqaadmin', sysdate, '1', '1');

insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0149', 'en_US', 'Please select institution', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'Q', '0150', 'en_US', 'Confirm to submit?', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0151', 'en_US', 'Please input PO No.', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0152', 'en_US', 'Invalid PO No.', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0153', 'en_US', 'Please input supplier''s reply on delivery date', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0154', 'en_US', 'Duplicated PO No.', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0155', 'en_US', 'Please input approval date', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0156', 'en_US', 'Approval date should be equal to or earlier than today', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'Q', '0157', 'en_US', 'Approval date is less than 7 days. Are you sure to proceed?', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0158', 'en_US', 'Please input order quanitity', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0159', 'en_US', 'Please input outstanding quantity', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0160', 'en_US', 'Please input delivery lead time on quotation', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0161', 'en_US', 'Duplicate contact person', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0162', 'en_US', 'Invalid email', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0163', 'en_US', 'Please remove all PO(s) before you change order type', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0164', 'en_US', 'Please input title', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0165', 'en_US', 'Please input first name', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0166', 'en_US', 'Please input last name', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0167', 'en_US', 'Invalid tel. no', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0168', 'en_US', 'Please sign, then upload the declaration of conflict of interest form', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'I', '0169', 'en_US', 'The delayed delivery report form [#0] has been sent to CPO for further actions', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0170', 'en_US', 'Please input valid repot date on range', 'itd', sysdate, 'itd', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0171', 'EN_US', 'Please input stock on hand quanitity', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0172', 'EN_US', 'Please input avg monthly consumption quanitity', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0173', 'EN_US', 'Please input purchase order', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0174', 'EN_US', 'Please input contact person', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'I', '0175', 'EN_US', 'The delayed delivery report form [#0] status has been updated [#1]', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0176', 'EN_US', 'System support maximum 5 contact persons', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0177', 'EN_US', 'Please select status', 'ITD', sysdate, 'ITD', sysdate, '1');

--//@UNDO
drop table DLD_RPT_FORM ;
drop table DLD_RPT_FORM_VER ;
drop table DLD_RPT_FORM_CONTACT ;
drop table DLD_RPT_FORM_PO ;

drop sequence SEQ_DLD_RPT_FORM;
drop sequence SEQ_DLD_RPT_FORM_PO;
drop sequence SEQ_DLD_RPT_FORM_VER;
drop sequence SEQ_DLD_RPT_FORM_CONTACT; 


delete from FUNC_SEQ_NUM where FUNC_CODE ='DDN';
delete from LETTER_TEMPLATE where CODE ='DLD_DOCI';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0149';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0150';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0151';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0152';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0153';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0154';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0155';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0156';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0157';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0158';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0159';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0160';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0161';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0162';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0163';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0164';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0165';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0166';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0167';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0168';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0169';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0170';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0171';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0172';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0173';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0174';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0175';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0176';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0177';
--//



