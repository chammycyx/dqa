create index i_supplier_01 on supplier(contact_id) tablespace dqa_indx_01;
create index i_supplier_contact_01 on supplier_contact(supplier_code) tablespace dqa_indx_01;
create index i_supplier_contact_02 on supplier_contact(contact_id) tablespace dqa_indx_01;
create index i_contract_01 on contract(item_code) tablespace dqa_indx_01;
create index i_contract_02 on contract(supplier_code) tablespace dqa_indx_01;
create index i_coa_item_01 on coa_item(contract_id) tablespace dqa_indx_01;
create index i_coa_item_02 on coa_item(coa_batch_id) tablespace dqa_indx_01;
create index i_coa_batch_01 on coa_batch(coa_item_id) tablespace dqa_indx_01;
create index i_coa_batch_02 on coa_batch(coa_batch_ver_id) tablespace dqa_indx_01;
create index i_coa_batch_ver_01 on coa_batch_ver(coa_batch_id) tablespace dqa_indx_01;
create index i_coa_discrepancy_01 on coa_discrepancy(coa_batch_id) tablespace dqa_indx_01;
create index i_coa_discrepancy_02 on coa_discrepancy(parent_coa_discrepancy) tablespace dqa_indx_01;
create index i_coa_discrepancy_03 on coa_discrepancy(discrepancy_id) tablespace dqa_indx_01;
create index i_coa_file_folder_01 on coa_file_folder(coa_batch_id) tablespace dqa_indx_01;
create index i_coa_file_folder_02 on coa_file_folder(file_id) tablespace dqa_indx_01;
create index i_sample_item_01 on sample_item(item_code) tablespace dqa_indx_01;
create index i_exclusion_test_01 on exclusion_test(sample_item_id) tablespace dqa_indx_01;

--//@UNDO
drop index i_supplier_01;
drop index i_supplier_contact_01;
drop index i_supplier_contact_02;
drop index i_contract_01;
drop index i_contract_02;
drop index i_coa_item_01;
drop index i_coa_item_02;
drop index i_coa_batch_01;
drop index i_coa_batch_02;
drop index i_coa_batch_ver_01;
drop index i_coa_discrepancy_01;
drop index i_coa_discrepancy_02;
drop index i_coa_discrepancy_03;
drop index i_coa_file_folder_01;
drop index i_coa_file_folder_02;
drop index i_sample_item_01;
drop index i_exclusion_test_01;
--//
