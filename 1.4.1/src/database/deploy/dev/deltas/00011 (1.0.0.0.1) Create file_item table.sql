create table file_item (
	file_id number(19) not null, 
	file_name varchar2(200) not null, 
	file_type varchar2(5) null, 
	file_path varchar2(256) null, 
	file_desc varchar2(300) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 	
	modify_user varchar2(15) not null, 	
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_file_item primary key (file_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table file_item;
--//
