create table coa_batch_ver (
	coa_batch_ver_id number(19) not null, 
	coa_batch_id number(19) null, 
	coa_status varchar2(1) not null, 
	discrepancy_status varchar2(1) not null, 	
	reminder_num number(10) null, 
	record_status varchar2(1) not null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 	
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_coa_batch_ver primary key (coa_batch_ver_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_batch_ver;
--//
