update problem_nature_cat set cat_desc = 'Batch/ expiry information' where problem_nature_cat_id = 1;
update problem_nature_cat set cat_desc = 'Drug Identity' where problem_nature_cat_id = 5;
update problem_nature_cat set cat_desc = 'Drug Content' where problem_nature_cat_id = 6;
update problem_nature_cat set cat_desc = 'Look-alike, Sound-alike' where problem_nature_cat_id = 16;
update problem_nature_cat set cat_desc = 'Odor/ taste' where problem_nature_cat_id = 17;
update problem_nature_cat set cat_desc = 'Weight/ dimension' where problem_nature_cat_id = 18;

update problem_nature_sub_cat set display_order = 7 where problem_nature_sub_cat_id = 13;

insert into problem_nature_sub_cat (problem_nature_sub_cat_id,sub_cat_desc,display_order,record_status,problem_nature_cat_id,modify_user,modify_date,create_user,create_date,version) values
(75,'Illegible',6,'A',3,'dqaadmin',sysdate,'dqaadmin',sysdate,0);

update problem_nature_sub_cat set sub_cat_desc = 'Black spot/ dirt on drug' where problem_nature_sub_cat_id = 21;
update problem_nature_sub_cat set sub_cat_desc = 'Color stain/ mark/ spot on drug' where problem_nature_sub_cat_id = 22;
update problem_nature_sub_cat set sub_cat_desc = 'Uneven surface/ texture/ crack' where problem_nature_sub_cat_id = 24;
update problem_nature_sub_cat set sub_cat_desc = 'Broken tablet/ capsule' where problem_nature_sub_cat_id = 26;
update problem_nature_sub_cat set sub_cat_desc = 'Foreign matter in injection/ infusion' where problem_nature_sub_cat_id = 41;
update problem_nature_sub_cat set sub_cat_desc = 'Syringe/ needle problem' where problem_nature_sub_cat_id = 51;
update problem_nature_sub_cat set sub_cat_desc = 'Label' where problem_nature_sub_cat_id = 53;
update problem_nature_sub_cat set sub_cat_desc = 'Product' where problem_nature_sub_cat_id = 54;
update problem_nature_sub_cat set sub_cat_desc = 'Odd odor/ taste' where problem_nature_sub_cat_id = 56;
update problem_nature_sub_cat set sub_cat_desc = 'Inconsistent weight/ dimension' where problem_nature_sub_cat_id = 58;
update problem_nature_sub_cat set sub_cat_desc = 'Fail to provide support/ feedback on incident investigation' where problem_nature_sub_cat_id = 71;


--//@UNDO
update problem_nature_sub_cat set sub_cat_desc = 'Fail to provide support/feedback on incident investigation' where problem_nature_sub_cat_id = 71;
update problem_nature_sub_cat set sub_cat_desc = 'Inconsistent weight/dimension' where problem_nature_sub_cat_id = 58;
update problem_nature_sub_cat set sub_cat_desc = 'Odd odor/taste' where problem_nature_sub_cat_id = 56;
update problem_nature_sub_cat set sub_cat_desc = 'LASA' where problem_nature_sub_cat_id = 54;
update problem_nature_sub_cat set sub_cat_desc = 'Illegible' where problem_nature_sub_cat_id = 53;
update problem_nature_sub_cat set sub_cat_desc = 'Syringe/needle problem' where problem_nature_sub_cat_id = 51;
update problem_nature_sub_cat set sub_cat_desc = 'Foreign matter in injection/infusion' where problem_nature_sub_cat_id = 41;
update problem_nature_sub_cat set sub_cat_desc = 'Broken tablet/capsule' where problem_nature_sub_cat_id = 26;
update problem_nature_sub_cat set sub_cat_desc = 'Uneven surface/crack/texture' where problem_nature_sub_cat_id = 24;
update problem_nature_sub_cat set sub_cat_desc = 'Color stain/mark/spot on drug' where problem_nature_sub_cat_id = 22;
update problem_nature_sub_cat set sub_cat_desc = 'Black spot/dirt on drug' where problem_nature_sub_cat_id = 21;

delete from problem_nature_sub_cat where problem_nature_sub_cat_id = 75;

update problem_nature_sub_cat set display_order = 6 where problem_nature_sub_cat_id = 13;

update problem_nature_cat set cat_desc = 'Weight/dimension' where problem_nature_cat_id = 18;
update problem_nature_cat set cat_desc = 'Odor/taste' where problem_nature_cat_id = 17;
update problem_nature_cat set cat_desc = 'Label' where problem_nature_cat_id = 16;
update problem_nature_cat set cat_desc = 'Product specification' where problem_nature_cat_id = 6;
update problem_nature_cat set cat_desc = 'Drug content' where problem_nature_cat_id = 5;
update problem_nature_cat set cat_desc = 'Batch/expiry information' where problem_nature_cat_id = 1;
--//