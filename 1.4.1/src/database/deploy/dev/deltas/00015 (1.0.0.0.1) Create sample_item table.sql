create table sample_item (
	sample_item_id number(19) not null, 
	item_code varchar2(6) null, 
	risk_level varchar2(3) null, 
	reg_status varchar2(1) null, 
	chem_analysis_qty_req varchar2(4) null, 
	chem_analysis_qty_req_desc varchar2(80) null, 	
	micro_bio_test_qty_req varchar2(4) null, 
	micro_bio_test_qty_req_desc varchar2(10) null, 
	valid_test_qty_req varchar2(4) null, 
	valid_test_qty_req_desc varchar2(10) null, 
	chem_analysis_remark varchar2(80) null, 
	micro_bio_test_remark varchar2(80) null,	
	remark varchar2(80) null, 
	record_status varchar2(1) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 	
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 	
	version number(19) not null, 
	constraint pk_sample_item primary key (sample_item_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table sample_item;
--//
