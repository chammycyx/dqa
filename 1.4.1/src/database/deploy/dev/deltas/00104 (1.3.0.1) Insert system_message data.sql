insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values ( 1006, 0, null, 'W', '0124', 'en_US', 'Deletion Abort. User record exist in SendCollectSample.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values ( 1006, 0, null, 'W', '0125', 'en_US', 'Deletion Abort. User record exist in FaxInitSupp.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values ( 1006, 0, null, 'W', '0126', 'en_US', 'Deletion Abort. User record exist in FaxDetail.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);



--//@UNDO
delete system_message where message_code = '0124';
delete system_message where message_code = '0125';
delete system_message where message_code = '0126';
--//