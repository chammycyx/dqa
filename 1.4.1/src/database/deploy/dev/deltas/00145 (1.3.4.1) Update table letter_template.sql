alter table LETTER_TEMPLATE add JAXB_FLAG NUMBER(1) default 0 NOT NULL;
comment on column LETTER_TEMPLATE.JAXB_FLAG is 'Indication on compatible JAXB';

--//@UNDO
alter table LETTER_TEMPLATE drop column JAXB_FLAG;
--//
