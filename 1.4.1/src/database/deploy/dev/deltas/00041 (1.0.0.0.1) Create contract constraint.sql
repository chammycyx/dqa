alter table contract add constraint fk_contract_01 foreign key (item_code) references drug_item (item_code);
alter table contract add constraint fk_contract_02 foreign key (supplier_code) references supplier (supplier_code);

--//@UNDO
alter table contract drop constraint fk_contract_01;
alter table contract drop constraint fk_contract_02;
--//