create table letter_template (
	code varchar2(10) not null, 
	name varchar2(100) null, 
	subject varchar2(200) null, 
	content clob null, 
	record_status varchar2(1) not null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 	
	modify_date timestamp not null, 		
	version number(19) not null, 
	constraint pk_letter_template primary key (code) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table letter_template;
--//
