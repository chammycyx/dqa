create table contact (
	contact_id number(19) not null, 
	name varchar2(100) not null, 
	title varchar2(5) null, 
	position varchar2(50) null, 
	office_phone varchar2(20) null, 
	mobile_phone varchar2(20) null, 
	fax varchar2(20) null, 
	email varchar2(256) null, 
	address1 varchar2(100) null, 
	address2 varchar2(100) null, 
	address3 varchar2(100) null, 
	opt_contact_point varchar2(1) null, 	
	remark varchar2(300) null, 			
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_contact primary key (contact_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;


--//@UNDO
drop table contact;
--//
