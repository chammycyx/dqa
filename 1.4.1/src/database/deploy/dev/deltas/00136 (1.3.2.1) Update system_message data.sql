update system_message set main_msg = 'Required Quantity is not equal to the Actual Required Quantity. Are you sure to continue?', severity_code = 'Q' where message_code = '0041';
update system_message set main_msg = 'Retention Quantity is not equal to the Actual Retention Quantity. Are you sure to continue?', severity_code = 'Q' where message_code = '0042';

insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values ( 1006, 0, null, 'W', '0128', 'en_US', 'End date should be later than or equal to start date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);

--//@UNDO
update system_message set main_msg = 'Required Quantity should be equal to Actual Required Quantity', severity_code = 'W' where message_code = '0041';
update system_message set main_msg = 'Retention Quantity should be equal to Actual Retention Quantity', severity_code = 'W' where message_code = '0042';

delete system_message where message_code = '0128';
--//
