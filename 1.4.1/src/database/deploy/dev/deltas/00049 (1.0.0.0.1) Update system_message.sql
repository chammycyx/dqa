update system_message 
set suppl_msg = 'Last unsuccessful logon: #{uam.getSamLastFailLogonTime()} from #{uam.getSamLastFailLogonWorkstation()} <br>Last success logon: #{uam.getSamLastSuccessLogonTime()} from #{uam.getSamLastSuccessLogonWorkstation()}'
where application_id = 1006
and message_code = '0001';


update system_message 
set main_msg = 'The batch COA has discrepancy(s) that is not passed, it would stay in ''discrepancy'' stage'
where application_id = 1006
and message_code = '0013';


--//@UNDO
update system_message 
set suppl_msg = 'Last unsuccessful logon: #{uam.getSamLastFailLogonTime()} from #{uam.getSamLastFailLogonWorkstation()} &lt;br&gt;Last success logon: #{uam.getSamLastSuccessLogonTime()} from #{uam.getSamLastSuccessLogonWorkstation()}'
where application_id = 1006
and message_code = '0001';

update system_message 
set main_msg = 'The batch COA has discrepancy(s) that is not passed, it would stay in &quot;discrepancy&quot; stage'
where application_id = 1006
and message_code = '0013';
--//
