create table coa_file_folder (
	coa_file_folder_id number(19) not null, 
	coa_batch_id number(19) null, 	
	file_id number(19) null, 
	file_cat varchar2(1) not null, 
	effective_date timestamp null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_coa_file_folder primary key (coa_file_folder_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_file_folder;
--//
