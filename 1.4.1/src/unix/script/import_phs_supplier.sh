#!/bin/ksh
####################################################################
#
# Script Name  :   import_phs_supplier.sh
#
# Purpose      :   import PHS Supplier to DQA
#
# History      :   Date        Who         Description
#                  20110607    David Pang  Initial version
#
####################################################################

script_name=`basename $0`

echo
echo `date $LOG_DT_FMT`:" $script_name: start"

$JAVA_HOME/bin/java -jar -Dconfig="${JAVAPATH}/conf/pms-dqa-batch-connection.properties" $JAVAPATH/pms-dqa-ejb-client.jar -m importPhsSupplier D_PHS_SUPPLIER

status=$?

echo
if [ $status -eq 0 ]
then
        echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
        echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: end"

exit $status