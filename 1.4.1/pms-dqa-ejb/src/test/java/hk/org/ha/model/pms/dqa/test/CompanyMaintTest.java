package hk.org.ha.model.pms.dqa.test;

import hk.org.ha.model.pms.dqa.biz.CompanyContactServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CompanyMaintTest extends SeamTest 
{
	@Test 
	public void testCompanyComponent() throws Exception
	{
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{
				//Login 
	            invokeMethod("#{identity.login}");
	            
	            // retrieveCompanyCodeList
	            invokeMethod("#{companyListService.retrieveCompanyListLikeCompanyCode('HK')}");
	            List<String> companyCodeList = (List<String>) getValue("#{companyList}");
				assert companyCodeList.size() > 0;
				
				// retrieveCompany
				invokeMethod("#{companyService.retrieveCompanyByCompanyCode('HKOA')}");
				Company company = (Company) getValue("#{company}");
				assert  getValue("#{company.companyName}").equals("HONG KONG OXYGEN & ACETYLENE CO., LTD");
				
				assert company.getCompanyContactList().size() == 2;
				
				
				// Add Company Contact
				invokeMethod("#{companyContactService.addCompanyContact()}");
				
				CompanyContact companyContact = (CompanyContact) getValue("#{companyContact}");
				
				companyContact.getContact().setMobilePhone("20012002");
				companyContact.getContact().setOfficePhone("20010002");
				companyContact.getContact().setOptContactPoint("N");
				companyContact.getContact().setTitle(TitleType.Mr);
				companyContact.getContact().setEmail("john@hkoa.com");
				companyContact.getContact().setFirstName("John");
				companyContact.getContact().setLastName("W");
				Contexts.getSessionContext().set("companyContact", companyContact);
				invokeMethod("#{companyContactService.createCompanyContact()}");
				
				// checking
				invokeMethod("#{companyService.retrieveCompanyByCompanyCode('HKOA')}");
				company = (Company) getValue("#{company}");
			
				assert company.getCompanyContactList().size() == 3;
				
				// retrieve Company Contact
				invokeMethod("#{companyContactService.retrieveCompanyContactByCompanyCodeContactId(company.getCompanyCode(), " +
						"company.getCompanyContactList().get(0).getContact().getContactId())}");				
				companyContact = (CompanyContact) getValue("#{companyContact}");
				
				assert companyContact != null;
				
				Date modifyDate = companyContact.getContact().getModifyDate();
				
				// update Company Contact
				companyContact.getContact().setLastName("Wong");
				companyContact.getContact().setFirstName("WW");
				companyContact.getContact().setEmail("abc@abc.com");
				Contexts.getSessionContext().set("companyContact ", companyContact);
				invokeMethod("#{companyContactService.updateCompanyContact()}");
				
				//CompanyContactServiceLocal companyContactService = (CompanyContactServiceLocal) this.getValue("#{companyContactService}");
				//companyContactService.retrieveCompanyContactByCompanyCodeContactId(company.getCompanyCode(), company.getCompanyContactList().get(0).getContact().getContactId());
				
				invokeMethod("#{companyContactService.retrieveCompanyContactByCompanyCodeContactId(company.getCompanyCode(), " +
				"company.getCompanyContactList().get(0).getContact().getContactId())}");				
				companyContact = (CompanyContact) getValue("#{companyContact}");

				Date modifyDate2 = companyContact.getContact().getModifyDate();
				
				assert companyContact.getContact().getLastName().equals("Wong");
				
				assert modifyDate2.compareTo(modifyDate) > 0;
				
				// Delete company Contact
				/*invokeMethod("#{companyContactService.deleteCompanyContact(companyContact)}");
				
				invokeMethod("#{companyService.retrieveCompanyByCompanyCode('HKOA')}");
				company = (Company) getValue("#{company}");

				assert company.getCompanyContactList().size() == 2;*/
				
				// retrieve pharmCompanyList
				invokeMethod("#{pharmCompanyListService.retrievePharmCompanyList()}");
		        List<Company> pharmCompanyListFind = (List<Company>)getValue("#{pharmCompanyList}");
		        assert pharmCompanyListFind.size() >0;

		        // retrieve company for PHS
		        Company companyFind = (Company)invokeMethod("#{companyService.retrieveCompanyForPhs('3M')}");
		        assert companyFind!=null;
		        
		        //retrieveManufacturerList
		        invokeMethod("#{companyListService.retrieveManufacturerList()}");
		        List<Company> manufacturerListFind = (List<Company>)getValue("#{companyList}");
		        assert manufacturerListFind.size() >0;
		        
		        //addCompanyContact
		        Contexts.getSessionContext().set("company", new Company());
		        invokeMethod("#{companyContactService.addCompanyContact()}");
		        company = (Company) getValue("#{company}");
		        assert company.getContact()==null;
		        
		        //retrieveCompanyContactByCompanyCodeContactId
		        invokeMethod("#{companyContactService.retrieveCompanyContactByCompanyCodeContactId('3M',1)}");
		        CompanyContact companyContactOut = (CompanyContact) getValue("#{companyContact}");
		        assert companyContactOut==null;
		        
		        //findPharmCompanyByCompanyCode
		        Contexts.getSessionContext().set("companyCode", "3M");
		        invokeMethod("#{pharmCompanyService.findPharmCompanyByCompanyCode(companyCode)}");
		        
		        //findManufacturerByCompanyCode
		        invokeMethod("#{companyService.findManufacturerByCompanyCode(companyCode)}");
		        
		        //retrievePharmCompanyListLike
		        invokeMethod("#{pharmCompanyListService.retrievePharmCompanyListLike(companyCode)}");
		        
		        //retrieveManufacturerListLike
		        invokeMethod("#{companyListService.retrieveManufacturerListLike(companyCode)}");
		        
		        Contexts.getSessionContext().set("YesFlag", YesNoFlag.Yes);
		        
		        //retrieveCompanyByCompanyCodeManufFlag
	            invokeMethod("#{companyService.retrieveCompanyByCompanyCodeManufFlag('AAA', YesFlag)}");
	            
	            //retrieveCompanyByCompanyCodePharmCompanyFlag
	            invokeMethod("#{companyService.retrieveCompanyByCompanyCodePharmCompanyFlag('AAA', YesFlag)}");
	            
	            //retrieveCompanyByCompanyCode
	            invokeMethod("#{companyService.retrieveCompanyByCompanyCode('AAA')}");
	            
	            //retrieveCompanyForPhs
	            invokeMethod("#{companyService.retrieveCompanyForPhs('AAA')}");
	            
	            //findManufacturerByCompanyCode
	            invokeMethod("#{companyService.findManufacturerByCompanyCode('AAA')}");
	            
	            //findPharmCompanyByCompanyCode
	            invokeMethod("#{pharmCompanyService.findPharmCompanyByCompanyCode('AAA')}");
	            
	            
	            
			}			
		}.run();
	}
}
