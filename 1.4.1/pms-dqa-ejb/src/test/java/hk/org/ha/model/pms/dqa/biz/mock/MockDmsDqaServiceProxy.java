package hk.org.ha.model.pms.dqa.biz.mock;



import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dms.persistence.DmInstDrugPK;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dms.persistence.Institution;
import hk.org.ha.model.pms.dms.vo.dqa.DqaDrugSupplier;
import hk.org.ha.model.pms.dqa.cacher.mock.MockDmDrugCacher.InnerDmDrug;

import hk.org.ha.service.biz.pms.dms.interfaces.DmsDqaServiceJmsRemote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

@Startup
@Name("dmsDqaServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockDmsDqaServiceProxy implements DmsDqaServiceJmsRemote {
	

	@Override
	public List<DmDrug> retrieveDmDrugList() {
		// TODO Auto-generated method stub
		
		List<DmDrug> dmList = new ArrayList<DmDrug>();
		
		// 01
		DmDrug dg01 = new DmDrug();
		dg01.setItemCode("PARA01");
		DmProcureInfo dg01ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier = new DqaDrugSupplier();
		dg01ProcureInfo.setOrderType("C");
		dqaDrugSupplier.setItemCode("PARA01");
		dqaDrugSupplier.setRegStatus("N");
		dqaDrugSupplier.setReleaseIndicator("Y");
		dg01.setDmProcureInfo(dg01ProcureInfo);
		dg01.setDqaDrugSupplier(dqaDrugSupplier);
		
		dmList.add(dg01);
		
		// 02
		DmDrug dg02 = new DmDrug();
		dg02.setItemCode("5FLU05");
		
		DmProcureInfo dg02ProcureInfo = new DmProcureInfo();
		dg02ProcureInfo.setOrderType("C");
		dg02.setDmProcureInfo(dg02ProcureInfo);
		
		dmList.add(dg02);
		
		//03
		DmDrug dg03 = new DmDrug();
		dg03.setItemCode("PARA02");
		
		DmProcureInfo dg03ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier3 = new DqaDrugSupplier();
		dg03ProcureInfo.setOrderType("D");
		dqaDrugSupplier3.setItemCode("PARA02");
		dqaDrugSupplier3.setRegStatus("N");
		dqaDrugSupplier3.setReleaseIndicator("Y");
		dg03.setDmProcureInfo(dg03ProcureInfo);
		dg03.setDqaDrugSupplier(dqaDrugSupplier3);
		
		dmList.add(dg03);
		
		//04
		DmDrug dg04 = new DmDrug();
		dg04.setItemCode("OXYG02");
		
		DmProcureInfo dg04ProcureInfo = new DmProcureInfo();
		dg04ProcureInfo.setOrderType("C");
		dg04.setDmProcureInfo(dg04ProcureInfo);
		
		dmList.add(dg04);
		
		//05
		InnerDmDrug dg05 = new InnerDmDrug();
		dg05.setItemCode("ALBU01");
		
		DqaDrugSupplier dqaDrugSupplier5 = new DqaDrugSupplier();
		DmProcureInfo dg05ProcureInfo = new DmProcureInfo();
		dg05ProcureInfo.setOrderType("D");
		dqaDrugSupplier5.setItemCode("ALBU01");
		dqaDrugSupplier5.setRegStatus("N");
		dqaDrugSupplier5.setReleaseIndicator("Y");
		dg05.setDmProcureInfo(dg05ProcureInfo);
		dg05.setDqaDrugSupplier(dqaDrugSupplier5);
		
		dmList.add(dg05);
		
		//06
		DmDrug dg06 = new DmDrug();
		dg06.setItemCode("AUGM01");
		
		DmProcureInfo dg06ProcureInfo = new DmProcureInfo();
		dg06ProcureInfo.setOrderType("C");
		dg06.setDmProcureInfo(dg06ProcureInfo);
		
		dmList.add(dg06);
		
		//07
		DmDrug dg07 = new DmDrug();
		dg07.setItemCode("INFL05");
		
		DmProcureInfo dg07ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier7 = new DqaDrugSupplier();
		dg07ProcureInfo.setOrderType("D");
		dqaDrugSupplier7.setItemCode("INFL05");
		dqaDrugSupplier7.setRegStatus("N");
		dqaDrugSupplier7.setReleaseIndicator("Y");
		dg07.setDmProcureInfo(dg07ProcureInfo);
		dg07.setDqaDrugSupplier(dqaDrugSupplier7);
		
		dmList.add(dg07);
		
		//08
		DmDrug dg08 = new DmDrug();
		dg08.setItemCode("MAGN08");
		
		DmProcureInfo dg08ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier8 = new DqaDrugSupplier();
		dg08ProcureInfo.setOrderType("D");
		dqaDrugSupplier8.setItemCode("MAGN08");
		dqaDrugSupplier8.setRegStatus("N");
		dqaDrugSupplier8.setReleaseIndicator("Y");
		dg08.setDmProcureInfo(dg08ProcureInfo);
		dg08.setDqaDrugSupplier(dqaDrugSupplier8);
		
		dmList.add(dg08);
		
		return dmList;
	}

	@Override
	public List<DmInstDrug> retrieveDmInstDrugList(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<String, List<DmInstDrug>> retrieveDmInstDrugListMap( List<String> itemCodeList ) {
		Map<String, List<DmInstDrug>> dmInstDrugListMap = new HashMap<String, List<DmInstDrug>>();
		
		List<DmInstDrug> dmInstDrugList = new ArrayList();
		
		DmInstDrug dmInstDrug = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK = new DmInstDrugPK();
		Institution institution = new Institution();
		dmInstDrugPK.setItemCode("PARA01");
		dmInstDrugPK.setInstCode("PYN");
		institution.setInstitutionCode("PYN");
		dmInstDrug.setCompId(dmInstDrugPK);
		dmInstDrug.setInstAvailability("Y");
		dmInstDrug.setInstitution(institution);
		dmInstDrug.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug);
		
		DmInstDrug dmInstDrug2 = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK2 = new DmInstDrugPK();
		Institution institution2 = new Institution();
		dmInstDrugPK2.setItemCode("PARA01");
		dmInstDrugPK2.setInstCode("PMH");
		institution2.setInstitutionCode("PMH");
		dmInstDrug2.setCompId(dmInstDrugPK2);
		dmInstDrug2.setInstAvailability("Y");
		dmInstDrug2.setInstitution(institution2);
		dmInstDrug2.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug2);
		
		DmInstDrug dmInstDrug3 = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK3 = new DmInstDrugPK();
		Institution institution3 = new Institution();
		dmInstDrugPK3.setItemCode("PARA01");
		dmInstDrugPK3.setInstCode("QMH");
		institution3.setInstitutionCode("QMH");
		dmInstDrug3.setCompId(dmInstDrugPK3);
		dmInstDrug3.setInstAvailability("Y");
		dmInstDrug3.setInstitution(institution3);
		dmInstDrug3.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug3);
		
		DmInstDrug dmInstDrug4 = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK4 = new DmInstDrugPK();
		Institution institution4 = new Institution();
		dmInstDrugPK4.setItemCode("PARA01");
		dmInstDrugPK4.setInstCode("TMH");
		institution4.setInstitutionCode("TMH");
		dmInstDrug4.setCompId(dmInstDrugPK4);
		dmInstDrug4.setInstAvailability("Y");
		dmInstDrug4.setInstitution(institution4);
		dmInstDrug4.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug4);
		
		DmInstDrug dmInstDrug5 = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK5= new DmInstDrugPK();
		Institution institution5 = new Institution();
		dmInstDrugPK5.setItemCode("PARA01");
		dmInstDrugPK5.setInstCode("QEH");
		institution5.setInstitutionCode("QEH");
		dmInstDrug5.setCompId(dmInstDrugPK5);
		dmInstDrug5.setInstAvailability("Y");
		dmInstDrug5.setInstitution(institution5);
		dmInstDrug5.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug5);
		
		DmInstDrug dmInstDrug6 = new DmInstDrug();
		DmInstDrugPK dmInstDrugPK6= new DmInstDrugPK();
		Institution institution6 = new Institution();
		dmInstDrugPK6.setItemCode("PARA01");
		dmInstDrugPK6.setInstCode("CMC");
		institution6.setInstitutionCode("CMC");
		dmInstDrug6.setCompId(dmInstDrugPK6);
		dmInstDrug6.setInstAvailability("Y");
		dmInstDrug6.setInstitution(institution6);
		dmInstDrug6.setInstSuspend("N");
		
		dmInstDrugList.add(dmInstDrug6);
		
		dmInstDrugListMap.put("PARA01", dmInstDrugList);
		
		return dmInstDrugListMap;
	}
	
}
