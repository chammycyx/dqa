package hk.org.ha.model.pms.dqa.coa.test;

import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaDiscrepancyTest extends SeamTest {
	@Test
	public void testCoaDiscrepancyPassCoaComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				
				// Login
	            invokeMethod("#{identity.login}");
	            /*
	             * Provided : - Batch# 101136 with COA Status = P
	             *            - coaBatchId# 10
	             *            - Parent COA has Discrepancy Code: 1,2,3,4,5,6
	             *            - Parent COA Discrepancy Code to Batch# mapping 
	             *              |------------------|---------------|
	             *              | Discrepancy Code | Batch#        |
	             *              |------------------|---------------|
	             *              | 1				   | 101137, 101138|
	             *              |------------------|---------------|
	             *              | 2				   | 101137, 101138|
	             *              |------------------|---------------|
	             *              | 3  			   | 101138        |
	             *              |------------------|---------------|
	             *              | 4				   |               |
	             *              |------------------|---------------|
	             *              | 5				   |               |
	             *              |------------------|---------------|
	             *              | 6				   |               |
	             *              |------------------|---------------|
	             *          
	             *            - Batch # to coaBatchId mapping
	             *              |-------------|---------------|----------|---------------------|
	             *              | Batch#      |  coaBatchId   |COA Status| Discrepancy Code(s) |
	             *              |-------------|---------------|----------|---------------------|
	             *              | 101137      |  11           |    G     | 1, 2                |
	             *              |-------------|---------------|----------|---------------------|
	             *              | 101138      |  12           |    G     | 1, 2, 3             |
	             *              |-------------|---------------|----------|---------------------|
	            */
	            
	            // Retrieve ( Discrepancy Under Group ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            List<CoaBatch> coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            // retrieve coaBatch ( Select a record from Pharmacist Follow Up )
	            Long coaBatchIdForDiscrepancy = 10L;//Batch 101136
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            	            
	            boolean hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101136 should be listed in Pharmacist Follow Up	            	            
	            
	            // Retrieve CoaDiscrepancyList ( Pass COA )
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListByCoaBatch(coaBatch)}");
	            List<CoaDiscrepancy> coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() > 0;	            
	            	            	
            	for( CoaDiscrepancy parentCd:coaDiscrepancyList ) { //pass Inconsistent specifications            		
            		if( parentCd.getCoaDiscrepancyKey().getDiscrepancyId().intValue() == 3 ){
            			parentCd.setEnable(true);
            			for( CoaDiscrepancy childCd:parentCd.getChildCoaDiscrepancyList() ) {
            				childCd.setEnable(true);
            			}
            			break;
            		}            		
            	}
            		            
	            invokeMethod("#{coaDiscrepancyListService.updateCoaDiscrepancyList(coaDiscrepancyList)}");	            	                      
	            
	            coaBatchIdForDiscrepancy = 11L;//Batch 101137
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");	            
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.DiscrepancyClarificationInProgress 
	            			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.DiscrepancyUnderGrouping;	            
	            
	            coaBatchIdForDiscrepancy = 12L;//Batch 101138
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");	            
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.DiscrepancyClarificationInProgress 
    			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.DiscrepancyUnderGrouping;	            
	            	            	            
	            coaBatchIdForDiscrepancy = 10L;//Batch 101136
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.DiscrepancyClarificationInProgress 
    			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.PharmacistFollowUp;
	            
	            // retrieve CoaDiscrepancyList ( Pass COA )
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListByCoaBatch(coaBatch)}");
	            coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() > 0;
	            
	            for( CoaDiscrepancy cd:coaDiscrepancyList ) {
	            	if( cd.getCoaDiscrepancyKey().getDiscrepancyId().intValue() != 3 ) {
	            		cd.setEnable(true);
	            	}	            	
	            }
	            
	            invokeMethod("#{coaDiscrepancyListService.updateCoaDiscrepancyList(coaDiscrepancyList)}");	            
	            
	            coaBatchIdForDiscrepancy = 10L;//Batch 101136
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.Pass
    			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.PharmacistFollowUp;
	            
	            coaBatchIdForDiscrepancy = 11L;//Batch 101137
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.DiscrepancyClarificationInProgress
    			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.PharmacistFollowUp;
	            
	            coaBatchIdForDiscrepancy = 12L;//Batch 101138
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.DiscrepancyClarificationInProgress
    			&& coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.PharmacistFollowUp;
			}
		}.run();
	}
	
	@Test
	public void testCoaDiscrepancyFailCoaComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				/*       
	             * Provided : - Batch# 101147
	             *            - coaBatchId# 13
	             *            - COA Status P
	             *            - Parent COA Discrepancy Code: 1,3,5
	             *            - Parent COA Discrepancy Code to Batch# mapping 
	             *              |------------------|---------------|
	             *              | Discrepancy Code | Batch#        |
	             *              |------------------|---------------|
	             *              | 1				   | 101147        |              
	             *              |------------------|---------------|
	             *              | 3  			   | 101147,101148 |
	             *              |------------------|---------------|
	             *              | 5				   | 101148        |
	             *              |------------------|---------------|
	             *          
	             *            - Batch # to coaBatchId mapping
	             *              |-------------|---------------|----------|---------------------|
	             *              | Batch#      |  coaBatchId   |COA Status| Discrepancy Code(s) |
	             *              |-------------|---------------|----------|---------------------|
	             *              | 101147      |  14           |    G     | 1,3                 |
	             *              |-------------|---------------|--------------------------------|
	             *              | 101148      |  15           |    G     | 3,5                 |
	             *              |-------------|---------------|--------------------------------|
				 * */
				
				// Login
	            invokeMethod("#{identity.login}");
				
				// Retrieve ( Discrepancy Under Group ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.DiscrepancyUnderGrouping + "')}");
	            List<CoaBatch> coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            // retrieve coaBatch ( Select a record from Discrepancy Under Group )
	            Long coaBatchIdForDiscrepancy = 14L;//Batch 101147
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatch( coaBatch )}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            	            
	            boolean hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101147 should be listed in Discrepancy under grouping
	            
	            coaBatch.setFailRemark("Fail Batch : "+coaBatch.getBatchNum());	            
	            invokeMethod("#{coaBatchService.updateCoaBatchForFailCoa(coaBatch)}");
	            
	            coaBatchIdForDiscrepancy = coaBatch.getCoaBatchId();//Batch 101147
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            	            
	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus().equals( CoaStatus.Fail );
	            
	            boolean hasParent = false;
	            for( CoaDiscrepancy cd:coaBatch.getCoaDiscrepancyList() ) {
	            	if(cd.getParentCoaDiscrepancy() != null) {
	            		hasParent = true;
	            		break;
	            	}
	            }
	            
	            assert !hasParent;
	            
				// Retrieve ( Pharmacist follow up ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            coaBatchIdForDiscrepancy = 13L;//Batch 101146
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            
	            coaBatch.setFailRemark("Fail Batch : "+coaBatch.getBatchNum());
	            invokeMethod("#{coaBatchService.updateCoaBatchForFailCoa(coaBatch)}");
	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatch.getCoaBatchId() + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus().equals( CoaStatus.Fail );
	            
				// Retrieve ( Pharmacist follow up ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	
	            
	            // retrieve coaBatch ( Select a record from Pharmacist follow up )
	            coaBatchIdForDiscrepancy = 15L;//Batch 101148
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            	            
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101148 should be listed in Pharmacist follow up
	            
	            for( CoaDiscrepancy cd:coaBatch.getCoaDiscrepancyList() ) {
	            	assert cd.getParentCoaDiscrepancy() == null;
	            }
			}
		}.run();
	}
	@Test
	public void testCoaDiscrepancyEditFollowUpDateComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				// Login
	            invokeMethod("#{identity.login}");
				
				// Retrieve ( Pharmacist follow up ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            List<CoaBatch> coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	
	            
	            // retrieve coaBatch ( Select a record from Pharmacist follow up )
	            Long coaBatchIdForDiscrepancy = 16L;//Batch 101120
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);

	            
	            boolean hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101120 should be listed in Pharmacist follow up
	            
	            Date currentDate = new Date();
	            
	            coaBatch.setFollowUpDate( currentDate );
	            
	            invokeMethod("#{coaBatchService.updateCoaBatch()}");
	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            
	            assert coaBatch.getFollowUpDate().equals(currentDate);
			}
		}.run();
	}
	
	@Test
	public void testCoaDiscrepancyEmailComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				// Login
	            invokeMethod("#{identity.login}");
				
	            /*
	             * 
	             * Process Email to supplier
	             * 
	             * */
	            
				// Retrieve ( Pharmacist follow up ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            List<CoaBatch> coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	
	            
	            // retrieve coaBatch ( Select a record from Pharmacist follow up )
	            Long coaBatchId = 17L;//Batch 101121
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);

	            boolean hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101121 should be listed in Pharmacist follow up
	            
	            invokeMethod("#{coaLetterTemplateService.retrieveLetterTemplateByDiscrepancyStatusReminderNum('" + DiscrepancyStatus.EmailToSupplier + "', coaBatch.getCoaBatchVer().getReminderNum())}");
	            
	            LetterTemplate letterTemplate = (LetterTemplate) getValue("#{letterTemplate}");
	            assert "ETS".equals(letterTemplate.getCode());
	            
	            Calendar cal = Calendar.getInstance();
	            cal.add(Calendar.MONTH, 1);
	            Date newFollowUpDate = cal.getTime();
	            coaBatch.setFollowUpDate(newFollowUpDate);
	            
	            invokeMethod("#{coaBatchService.updateCoaBatchStatus(coaBatch, coaBatch.getCoaBatchVer().getCoaStatus(), '"+DiscrepancyStatus.EmailToSupplier+"')}");
	            
	            // Retrieve ( Email to supplier ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.EmailToSupplier + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101121 should be listed in Email to supplier
	            
	            assert coaBatch.getFollowUpDate().equals(newFollowUpDate);
	            
	            /*
	             * 
	             * Process Reminder to supplier
	             * 
	             * */
	            
	            invokeMethod("#{coaLetterTemplateService.retrieveLetterTemplateByDiscrepancyStatusReminderNum('" + DiscrepancyStatus.ReminderToSupplier + "', coaBatch.getCoaBatchVer().getReminderNum())}");
	            
	            letterTemplate = (LetterTemplate) getValue("#{letterTemplate}");
	            assert "RTS".equals(letterTemplate.getCode());
	            
	            cal = Calendar.getInstance();
	            cal.add(Calendar.DATE, 14);
	            newFollowUpDate = cal.getTime();
	            coaBatch.setFollowUpDate(newFollowUpDate);
	            
	            invokeMethod("#{coaBatchService.updateCoaBatchStatus(coaBatch, coaBatch.getCoaBatchVer().getCoaStatus(), '"+DiscrepancyStatus.ReminderToSupplier+"')}");
	            
	            // Retrieve ( Reminder to supplier ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.ReminderToSupplier + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            assert coaBatch.getCoaBatchVer().getReminderNum().intValue() == 1;
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101121 should be listed in Reminder to supplier
	            
	            assert coaBatch.getFollowUpDate().equals(newFollowUpDate);
	            
	            /*
	             * 
	             * Process Reminder two to supplier
	             * 
	             * */	            
	            invokeMethod("#{coaLetterTemplateService.retrieveLetterTemplateByDiscrepancyStatusReminderNum('" + DiscrepancyStatus.ReminderToSupplier + "', coaBatch.getCoaBatchVer().getReminderNum())}");
	            
	            letterTemplate = (LetterTemplate) getValue("#{letterTemplate}");
	            assert "RTSM".equals(letterTemplate.getCode());
	            
	            cal = Calendar.getInstance();
	            cal.add(Calendar.DATE, 14);
	            newFollowUpDate = cal.getTime();
	            coaBatch.setFollowUpDate(newFollowUpDate);
	            
	            invokeMethod("#{coaBatchService.updateCoaBatchStatus(coaBatch, coaBatch.getCoaBatchVer().getCoaStatus(), '"+DiscrepancyStatus.ReminderToSupplier+"')}");
	            
	            // Retrieve ( Reminder to supplier ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.ReminderToSupplier + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            assert coaBatch.getCoaBatchVer().getReminderNum().intValue() == 2;
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101121 should be listed in Reminder to supplier
	            
	            assert coaBatch.getFollowUpDate().equals(newFollowUpDate);
			}
		}.run();
	}

	@Test
	public void testCoaDiscrepancyRevertComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				/* 
	             * Provided : - Batch# 101156
	             *            - coaBatchId# 18
	             *            - COA Status P
	             *            - Parent COA Discrepancy Code: 1,2,3
	             *            - Parent COA Discrepancy Code to Batch# mapping 
	             *              |------------------|---------------|
	             *              | Discrepancy Code |        Batch# |
	             *              |------------------|---------------|
	             *              | 1				   | 101157        |              
	             *              |------------------|---------------|
	             *              | 2  			   | 101157,101158 |
	             *              |------------------|---------------|
	             *              | 3				   | 101158        |
	             *              |------------------|---------------|
	             *          
	             *            - Batch # to coaBatchId mapping
	             *              |-------------|---------------|----------|--------------------|
	             *              | Batch#      |  coaBatchId   |COA Status| Discrepancy Code(s)|
	             *              |-------------|---------------|----------|--------------------|
	             *              | 101157      |  19           |    G     | 1, 2               |
	             *              |-------------|---------------|----------|--------------------|
	             *              | 101158      |  20           |    P     | 1, 2, 3            |
	             *              |-------------|---------------|----------|--------------------|
				 * */
				
				// Login
	            invokeMethod("#{identity.login}");
	            
				// Retrieve ( Pharmacist follow up ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            List<CoaBatch> coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	
	            
	            // retrieve coaBatch ( Select a record from Reminder to supplier )
	            Long coaBatchId = 18L;//Batch 101156
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);

	            boolean hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101156 should be listed in Reminder to supplier           
	            
	            invokeMethod("#{coaBatchVerListService.retrieveCoaBatchVerListByCoaBatch(coaBatch)}");	            
	            /*
	             * 
	             *  Revert Batch 101156 from "Reminder to supplier" to "Email to supplier"
	             *
	             **/
	            
	            invokeMethod("#{coaBatchService.updateCoaBatchForRevert(coaBatchVerList.get(coaBatchVerList.size() - 1))}");	 
	            
	            // Retrieve ( Pharmacist follow up )
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.PharmacistFollowUp + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;
	            
	            coaBatchId = 19L;//Batch 101157
	            // retrieve coaBatch ( Select a record from Reminder to supplier )	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101157 should be listed in Pharmacist follow up
	            
	            assert coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.PharmacistFollowUp;
	           
	            for(CoaDiscrepancy cd : coaBatch.getCoaDiscrepancyList()) {	            	
	            	assert cd.getParentCoaDiscrepancy() == null;
	            }
	            
	            // Retrieve ( Reminder to supplier ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.ReminderToSupplier + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	            
	            
	            coaBatchId = 20L;//Batch 101158
	            // retrieve coaBatch ( Select a record from Pharmacist follow up )	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            
	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101158 should be listed in Pharmacist follow up	            	            
	           
	            for(CoaDiscrepancy cd : coaBatch.getCoaDiscrepancyList()) {	            	
	            	assert cd.getParentCoaDiscrepancy() == null;
	            }	            
	            
	            coaBatchId = 18L;//Batch 101156
	            // retrieve coaBatch ( Select a record from Reminder to supplier )	            
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            	            
	            assert coaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.VerificationInProgress;
	            
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListByCoaBatch(coaBatch)}");
	            List<CoaDiscrepancy> coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() == 0;
	            /*
	             * 
	             *  Revert Batch 101158 from "Reminder to supplier" to "Email to supplier"
	             *
	             **/
	           
	            // Retrieve ( Reminder to supplier ) 	            
	            invokeMethod("#{coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus('" + CoaStatus.DiscrepancyClarificationInProgress + "', '" + DiscrepancyStatus.ReminderToSupplier + "')}");
	            coaBatchList = (List<CoaBatch>) getValue("#{coaBatchList}");
	            assert coaBatchList.size() > 0;	
	            
	            // retrieve coaBatch ( Select a record from Reminder to supplier )
	            coaBatchId = 20L;//Batch 101158
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);

	            hasIt = false;
	            for( CoaBatch c:coaBatchList ) {
	            	if(c.getCoaBatchId().equals( coaBatch.getCoaBatchId() )) {
	            		hasIt = true;	            		
	            		break;
	            	}
	            }
	            assert hasIt; //Batch 101158 should be listed in Reminder to supplier
	            
	            invokeMethod("#{coaBatchVerListService.retrieveCoaBatchVerListByCoaBatch(coaBatch)}");	          
	            
	            invokeMethod("#{coaBatchService.updateCoaBatchForRevert(coaBatchVerList.get(coaBatchVerList.size() - 3))}");
	            
	            // retrieve coaBatch ( Select a record from Email to supplier )
	            coaBatchId = 20L;//Batch 101158
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchId + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchId);
	            
	            assert coaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.EmailToSupplier;	            	            	            
	            
			}
		}.run();
	}

}
