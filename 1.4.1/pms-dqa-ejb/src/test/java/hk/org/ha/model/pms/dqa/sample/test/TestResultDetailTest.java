package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class TestResultDetailTest extends SeamTest {
	@Test
	public void testResultDetailComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve All Schedule Record with schedule status = "TR"
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.TestResult);
	            Contexts.getSessionContext().set("fileCatIn", SampleTestFileCat.TestResultReport);
	            
	            invokeMethod("#{sampleTestScheduleFileListService.retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat(recordStatusIn,scheduleStatusIn,fileCatIn)}");
	            
	            List<SampleTestScheduleFile> sTSFFind = (List<SampleTestScheduleFile>)getValue("#{sampleTestScheduleFileList}");
	            assert sTSFFind.size() >0;
	            
	            //Retrieve one of the record with schedule status = "TR"	
	            
	            Contexts.getSessionContext().set("scheduleIdIn", sTSFFind.get(0).getSampleTestSchedule().getScheduleId());
	            Contexts.getSessionContext().set("fileCatIn", SampleTestFileCat.TestResultReport);
	            	            
	            invokeMethod("#{sampleTestScheduleFileService.findSampleTestScheduleFileByScheduleIdFileCat(scheduleIdIn,fileCatIn)}");
	            
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileByScheduleIdFileCat(scheduleIdIn,fileCatIn)}");
	            SampleTestScheduleFile sTSFDFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFile}");
	            
	            assert getValue("#{sampleTestScheduleFile==null}").equals(false); 
	            
	            
	            
	            SampleTestScheduleFile sampleTestScheduleFileConfirm = sTSFFind.get(0);
	            SampleTestScheduleFile sampleTestScheduleFileDelete = sTSFFind.get(1);
	            SampleTestScheduleFile sampleTestScheduleFileReverse = sTSFFind.get(2);
	            
	            //Confirm
	            sampleTestScheduleFileConfirm.getSampleTestSchedule().setTestReportDate(currentDate);
	            sampleTestScheduleFileConfirm.getSampleTestSchedule().setTestResult("O");
	            sampleTestScheduleFileConfirm.getSampleTestSchedule().setTestResultDesc("Test Case");
	            
	            SampleTestFile sampleTestFileIn = new SampleTestFile();
	            FileItem fileItemIn = new FileItem();
	            fileItemIn.setFileName("Test Case File");
	            fileItemIn.setFileType("pdf");
	            fileItemIn.setFilePath("\\Test Path");
	            fileItemIn.setFileDesc("Test Case File Desc");
	            fileItemIn.setModuleType(ModuleType.SampleTest);
	            
	            sampleTestFileIn.setFileCat(SampleTestFileCat.TestResultReport);
	            sampleTestFileIn.setFileItem(fileItemIn);
	            sampleTestFileIn.setSupplier(sampleTestScheduleFileConfirm.getSampleTestSchedule().getContract().getSupplier());
	            sampleTestFileIn.setManufacturer(sampleTestScheduleFileConfirm.getSampleTestSchedule().getContract().getManufacturer());
	            sampleTestFileIn.setPharmCompany(sampleTestScheduleFileConfirm.getSampleTestSchedule().getContract().getPharmCompany());
	            sampleTestFileIn.setItemCode(sampleTestScheduleFileConfirm.getSampleTestSchedule().getSampleItem().getItemCode());
	            
	            
	            Contexts.getSessionContext().set("sampleTestScheduleFileIn", sampleTestScheduleFileConfirm);
	            Contexts.getSessionContext().set("sampleTestFileIn", sampleTestFileIn);
	            
	            
	            invokeMethod("#{sampleTestScheduleFileService.updateSampleTestScheduleFileForTestResult(sampleTestScheduleFileIn, sampleTestFileIn, 'Confirm')}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleFileConfirm.getSampleTestSchedule().getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.Payment);
				
	            //Delete
	            
	            Contexts.getSessionContext().set("deleteReasonIn", CancelSchedReason.ReferenceStandardNotAvailable);
	            Contexts.getSessionContext().set("deleteSampleTestScheduleIn", sampleTestScheduleFileDelete.getSampleTestSchedule());
	            
	            invokeMethod("#{sampleTestScheduleService.deleteSampleTestSchedule(deleteSampleTestScheduleIn,deleteReasonIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleFileDelete.getSampleTestSchedule().getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.recordStatus}").equals(RecordStatus.Deleted);
	            
	            //Reverse
	            
	            Contexts.getSessionContext().set("reverseSampleTestScheduleIn", sampleTestScheduleFileReverse.getSampleTestSchedule());
	            
	            invokeMethod("#{sampleTestScheduleService.reverseSampleTestScheduleStatus(reverseSampleTestScheduleIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleFileReverse.getSampleTestSchedule().getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.LabCollection);
	            
	            //Check Next Schedule
	            Contexts.getSessionContext().set("nextScheduleSampleTestScheduleFileIn", sTSFFind.get(3));
	            SampleTestSchedule nextSampleTestScheduleOut = (SampleTestSchedule)invokeMethod("#{sampleTestScheduleNextService.retrieveNextSampleTestSchedule(nextScheduleSampleTestScheduleFileIn)}");
	            
	            assert nextSampleTestScheduleOut==null;
			}
		}.run();
	}

}
