package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Collection;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleMovementReportTest extends SeamTest {
	@Test
	public void testSampleTestReportComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				invokeMethod("#{identity.login}");
				
				Collection<Long> keyList = new ArrayList<Long>();
				
				keyList.add(1L);
				keyList.add(2L);
				
				Contexts.getSessionContext().set("sampleTestInventoryList", keyList);
	            invokeMethod("#{sampleTestRptService.retrieveSampleMovementList(sampleTestInventoryList)}");
			}
		}.run();
	}
	
	@Test
	public void testSampleTestStockOnhandReportComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            invokeMethod("#{sampleTestStockOnhandRptService.retrieveSampleMovementStockOnhandList()}");
	            
	            invokeMethod("#{sampleTestStockOnhandRptService.getSampleTestInventoryStockOnhandList()}");
			}
		}.run();
	}
}