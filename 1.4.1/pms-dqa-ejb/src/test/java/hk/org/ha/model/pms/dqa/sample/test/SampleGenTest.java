package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleGenTest extends SeamTest {
	@Test
	public void testSampleGenComponent() throws Exception{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
	            
	            //retrieve
	            SampleTestScheduleCriteria sc = new SampleTestScheduleCriteria();
	            sc.setTestCode("MICRO");
	            sc.setScheduleStatus(ScheduleStatus.ScheduleGen);
	            
	            Contexts.getSessionContext().set("sampleTestScheduleCriteriaIn", sc);
				
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteria(sampleTestScheduleCriteriaIn)}");
	            
	            List<SampleTestSchedule> sampleTestScheduleListFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            assert sampleTestScheduleListFind.size() >0;
	            
	            SampleTestSchedule sampleTestScheduleUpdate = sampleTestScheduleListFind.get(0);
	            SampleTestSchedule sampleTestScheduleCopy = sampleTestScheduleListFind.get(2);
	            
	            //update
	            invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('ABBO')}");
	            Supplier supplierFind = (Supplier) getValue("#{supplier}");
	           
	            sampleTestScheduleUpdate.getContract().setSupplier(supplierFind);
	            
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleUpdate);
	            
	            invokeMethod("#{sampleTestScheduleService.updateSampleTestSchedule(sampleTestScheduleIn, false)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleUpdate.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.ScheduleGen);
	            assert getValue("#{sampleTestSchedule.contract.supplier.supplierCode}").equals("ABBO");
	            
	            //copy
	            Collection<SampleTestSchedule> sampleTestScheduleListCopy = new ArrayList();
	            
	            sampleTestScheduleListCopy.add(sampleTestScheduleCopy);
	            
	            Contexts.getSessionContext().set("sampleTestSchedule", (SampleTestSchedule)getValue("#{sampleTestSchedule}"));
	            Contexts.getSessionContext().set("sampleTestScheduleListCopy", sampleTestScheduleListCopy);

	            invokeMethod("#{sampleTestScheduleListService.copySampleTestSchedule(sampleTestScheduleListCopy)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleCopy.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.contract.supplier.supplierCode}").equals("ABBO");
	            
	            //confirm
	            SampleTestSchedule sampleTestScheduleConfirm = (SampleTestSchedule)getValue("#{sampleTestSchedule}");
	            
	            Contexts.getSessionContext().set("YesFlag", YesNoFlag.Yes);
	            
	            Company pharmCompanyFind = (Company)invokeMethod("#{companyService.retrieveCompanyByCompanyCodePharmCompanyFlag('VPL', YesFlag)}");
	            
	            Company manufacturerFind = (Company)invokeMethod("#{companyService.retrieveCompanyByCompanyCodeManufFlag('VPL', YesFlag)}");
	            
	            sampleTestScheduleConfirm.getContract().setPharmCompany(pharmCompanyFind);
	            sampleTestScheduleConfirm.getContract().setManufacturer(manufacturerFind);
	            
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleConfirm);
	            invokeMethod("#{sampleTestScheduleService.updateSampleTestSchedule(sampleTestScheduleIn, true)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleConfirm.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.Schedule);
	            assert getValue("#{sampleTestSchedule.contract.pharmCompany.companyCode}").equals("VPL");
	           
	            
	            //schedule generation
	            Contexts.getSessionContext().set("testCodeIn", "MICRO");
	            Contexts.getSessionContext().set("orderTypeIn", OrderType.DirectPurchase);
	            
	            invokeMethod("#{sampleTestScheduleService.scheduleGeneration(testCodeIn, orderTypeIn)}");
	            
	            Contexts.getSessionContext().set("sampleTestScheduleCriteriaIn", sc);
	            
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteria(sampleTestScheduleCriteriaIn)}");
	            
	            sampleTestScheduleListFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            assert sampleTestScheduleListFind.size() >0;
	            
			}
		}.run();
	}
}
