package hk.org.ha.model.pms.dqa.suppperf.test;

import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.Classification;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxFinalHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInitHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInitSupp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInterimHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInterimSupp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxSummary;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemHeader;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemInstitution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.SendCollectSample;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.SystemManualFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.FaxType;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemBy;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspQtyRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.CaseFinalRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxDetailLogRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxFinalHospRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxFinalSuppRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxInitHospRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxInitSuppRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxInterimHospRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxInterimSuppRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxSummaryRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class QaProblemTest extends SeamTest {
	@Test
	public void QaProblemComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//Login
	            invokeMethod("#{identity.login}");
			
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("31/12/2012");
	            Date problemDate = dfm.parse("1/12/2011");
				
	           
	            
	            ///////////////////////////////////////////////////////////////////////////////////////////////////
	            //Start create pharmProblem
	            ///////////////////////////////////////////////////////////////////////////////////////////////////
	            ProblemHeader problemHeaderIn= new ProblemHeader();
				Contract contractIn= new Contract();
				Supplier supplierIn= new Supplier();
				Company manufacturerIn= new Company();
				Company pharmCompanyIn= new Company();
				List<PharmProblemNature> pharmProblemNatureListIn = new ArrayList<PharmProblemNature>();
				List<String> pharmBatchNumListIn = new ArrayList<String>();
				List<String> countryListIn = new ArrayList<String>();
				PharmProblemNature pharmProblemNatureIn= new PharmProblemNature();
				SendCollectSample sendCollectSampleIn= new SendCollectSample();
				PharmProblem pharmProblemIn= new PharmProblem();
				Institution institutionIn = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('PYN')}");
				
				supplierIn.setSupplierCode("ZUEL");
				manufacturerIn.setCompanyCode("3M");
				pharmCompanyIn.setCompanyCode("3M");
				
				contractIn.setContractNum("HOC100-36-1");
				contractIn.setContractSuffix("");
				contractIn.setContractType("C");
				contractIn.setModuleType(ModuleType.SuppPerf);
				contractIn.setItemCode("PARA01");
				contractIn.setSupplier(supplierIn);
				contractIn.setManufacturer(manufacturerIn);
				contractIn.setPharmCompany(pharmCompanyIn);
				
				problemHeaderIn.setItemCode("PARA01");
				problemHeaderIn.setOrderType(OrderTypeAll.Contract);
				
				sendCollectSampleIn=null;
				
				pharmProblemIn.setAffectQty(100);
				pharmProblemIn.setRecordStatus(RecordStatus.Active);
				pharmProblemIn.setProblemByName("NAME1");
				pharmProblemIn.setProblemByRank("RANK1");
				pharmProblemIn.setProblemByType(ProblemBy.Pharmacy);
				pharmProblemIn.setProblemByTypeDesc(ProblemBy.Pharmacy.getDisplayValue());
				pharmProblemIn.setProblemDate(problemDate);
				pharmProblemIn.setCoordinatorName("NAEM1");
				pharmProblemIn.setCoordinatorRank(Rank.P);
				pharmProblemIn.setCoordinatorPhone("12345678");
				pharmProblemIn.setProblemDetail("Test Detail");
				pharmProblemIn.setInstitution(institutionIn);
				pharmProblemIn.setSendCollectSample(sendCollectSampleIn);
				pharmProblemIn.setProblemHeader(problemHeaderIn);
				pharmProblemIn.setContract(contractIn);
				
				//select ProblemNatureSubCat
				ProblemNatureSubCat problemNatureSubCatIn = new ProblemNatureSubCat();
				problemNatureSubCatIn.setProblemNatureSubCatId(Long.valueOf(1));
				Contexts.getSessionContext().set("problemNatureSubCatIn", problemNatureSubCatIn);
	            invokeMethod("#{problemNatureSubCatService.retrieveProblemNatureSubCatByProblemNatureSubCatId(problemNatureSubCatIn)}");
	            problemNatureSubCatIn = (ProblemNatureSubCat)getValue("#{problemNatureSubCat}");
	            
				pharmProblemNatureIn = new PharmProblemNature();
				pharmProblemNatureIn.setProblemNatureSubCat(problemNatureSubCatIn);
				pharmProblemNatureIn.setPharmProblem(pharmProblemIn);
				pharmProblemNatureIn.setRecordStatus(RecordStatus.Active);
				pharmProblemNatureListIn.add(pharmProblemNatureIn);
				
				pharmBatchNumListIn.add("BATCH-TEST-1");
				countryListIn.add("HK"); 
	            
	            
				Contexts.getSessionContext().set("pharmProblemIn", pharmProblemIn);
				Contexts.getSessionContext().set("pharmBatchNumList", pharmBatchNumListIn);
				Contexts.getSessionContext().set("countryList", countryListIn);
				Contexts.getSessionContext().set("pharmProblemNatureList", pharmProblemNatureListIn);
				Contexts.getSessionContext().set("pharmProblemFileUploadDataList", null);
				Contexts.getSessionContext().set("sendEmailBoolean", false);
		        invokeMethod("#{pharmProblemService.createPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList, sendEmailBoolean)}");
		        ///////////////////////////////////////////////////////////////////////////////////////////////////
	            //Finish create pharmProblem
	            ///////////////////////////////////////////////////////////////////////////////////////////////////   
				
	            PharmProblemCriteria pharmProblemCriteriaIn = new PharmProblemCriteria();
	            pharmProblemCriteriaIn.setFromProblemDate(fromDate);
	            pharmProblemCriteriaIn.setToProblemDate(toDate);
	            pharmProblemCriteriaIn.setInstitution(institutionIn);
	            pharmProblemCriteriaIn.setItemCode("PARA01");
	            pharmProblemCriteriaIn.setOrderType(OrderTypeAll.Contract);
	            pharmProblemCriteriaIn.setSupplierCode("ZUEL");
	            pharmProblemCriteriaIn.setManufCode("3M");
	            pharmProblemCriteriaIn.setPharmCompanyCode("3M");
	            
	            
	            // retrieve by criteria
	            Contexts.getSessionContext().set("pharmProblemCriteriaIn", pharmProblemCriteriaIn);
	            invokeMethod("#{pharmProblemListService.retrievePharmProblemListByPharmProblemCriteria(pharmProblemCriteriaIn)}");
	            		
	            assert getValue("#{pharmProblemList!=null}").equals(true);
	            
	            List<PharmProblem> pharmProblemListFind = (List<PharmProblem>)getValue("#{pharmProblemList}");
	            PharmProblem pharmProblemFind = pharmProblemListFind.get(0);
	            
	            QaProblem qaProblemSave = new QaProblem();
	            qaProblemSave.setCaseNum("TEST1");
				
	            pharmProblemFind.setProblemStatus(ProblemStatus.Active);
	            
	            Contexts.getSessionContext().set("pharmProblemIn", pharmProblemFind);
				Contexts.getSessionContext().set("qaProblemIn", qaProblemSave);
				Contexts.getSessionContext().set("caseTypeIn", "NewCase");
				Contexts.getSessionContext().set("qaBatchNumListIn", null);
				invokeMethod("#{pharmProblemNewService.updateNewPharmProblemWithQaProblem(pharmProblemIn, qaProblemIn, caseTypeIn, qaBatchNumListIn)}");
	            
				////////////////////////////////////////////////////////
				// retrieve by criteria
	            QaProblemCriteria qaProblemCriteriaIn = new QaProblemCriteria();
	            qaProblemCriteriaIn.setFromCreateDate(fromDate);
	            qaProblemCriteriaIn.setToCreateDate(toDate);
	            qaProblemCriteriaIn.setItemCode("PARA01");
	            qaProblemCriteriaIn.setOrderType(OrderTypeAll.Contract);
	            qaProblemCriteriaIn.setSupplierCode("ZUEL");
	            qaProblemCriteriaIn.setManufCode("3M");
	            qaProblemCriteriaIn.setPharmCompanyCode("3M");
	            
	            Contexts.getSessionContext().set("qaProblemCriteriaIn", qaProblemCriteriaIn);
	            Contexts.getSessionContext().set("screenNameIn", "qaProblemCaseEnquiryView");
	            invokeMethod("#{qaProblemListService.retrieveQaProblemListByQaProblemCriteria(qaProblemCriteriaIn, screenNameIn)}");
	            		
	            assert getValue("#{qaProblemList!=null}").equals(true);
	            List<QaProblem> qaProblemListFind = (List<QaProblem>)getValue("#{qaProblemList}");
	            
	            //retrieveFaxSummaryRptByQaProblemList
	            FaxSummary faxSummaryIn = new FaxSummary();
	            faxSummaryIn.setFinalHCount(0);
	            faxSummaryIn.setFinalSCount(0);
	            faxSummaryIn.setInitHCount(0);
	            faxSummaryIn.setInitSCount(0);
	            faxSummaryIn.setInterimHCount(0);
	            faxSummaryIn.setInterimSCount(0);
	            faxSummaryIn.setFinalHUpdateDate(problemDate);
	            faxSummaryIn.setFinalSUpdateDate(problemDate);
	            faxSummaryIn.setInitHUpdateDate(problemDate);
	            faxSummaryIn.setInitSUpdateDate(problemDate);
	            faxSummaryIn.setInterimHUpdateDate(problemDate);
	            faxSummaryIn.setInterimSUpdateDate(problemDate);
	            
	            for(QaProblem qc : qaProblemListFind)
	            {
	            	qc.setFaxSummary(faxSummaryIn);
	            }
	            
	            Contexts.getSessionContext().set("qaProblemListFind", qaProblemListFind);
	            invokeMethod("#{qaProblemListService.retrieveFaxSummaryRptByQaProblemList(qaProblemListFind)}");
	            
	            
	            //retrieve by number of String
	            Contexts.getSessionContext().set("caseNum", "TEST1");
	            Contexts.getSessionContext().set("itemCode", "PARA01");
	            Contexts.getSessionContext().set("supplier", "ZUEL");
	            Contexts.getSessionContext().set("manufacturer", "3M");
	            Contexts.getSessionContext().set("pharmCompany", "3M");
	            invokeMethod("#{qaProblemListForCaseNumService.retrieveQaProblemListLikeCaseNumByItemSMP(caseNum, itemCode, supplier, manufacturer, pharmCompany)}");
	            assert getValue("#{qaProblemListForCaseNum!=null}").equals(true);
	            
	            Contexts.getSessionContext().set("nullInst", null);
	            invokeMethod("#{qaProblemListForCaseNumService.retrieveQaProblemListLikeCaseNum(caseNum, nullInst, nullInst, nullInst)}");
	            assert getValue("#{qaProblemListForCaseNum!=null}").equals(true);
	            
	            //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            QaProblem qaProblemIn = (QaProblem)getValue("#{qaProblem}");
	            
	            //get by caseNum
	            invokeMethod("#{qaProblemService.getQaProblemByCaseNum(caseNum)}");
	            
	            //retrieve by qaProblem
	            Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
	            invokeMethod("#{qaProblemService.retrieveQaProblemByQaProblem(qaProblemIn)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            
	            //retrieve by pharmProblem By Criteria
	            /*Contexts.getSessionContext().set("pharmProblemCriteriaIn", pharmProblemCriteriaIn);
	            invokeMethod("#{pharmProblemListService.retrievePharmProblemListByPharmProblemCriteria(pharmProblemCriteriaIn)}");
	            		
	            pharmProblemListFind = (List<PharmProblem>)getValue("#{pharmProblemList}");
	            pharmProblemFind = pharmProblemListFind.get(0);
	            
	            QaProblem qaProblemChange = new QaProblem();
	            qaProblemChange.setCaseNum("TEST1A");
	            
	            Contexts.getSessionContext().set("pharmProblemIn", pharmProblemFind);
				Contexts.getSessionContext().set("qaProblemIn", qaProblemChange);
				Contexts.getSessionContext().set("caseTypeIn", "NewCase");
				Contexts.getSessionContext().set("qaBatchNumListIn", null);
				invokeMethod("#{pharmProblemNewService.updateNewPharmProblemWithQaProblem(pharmProblemIn, qaProblemIn, caseTypeIn, qaBatchNumListIn)}");
				
				 //retrieve by caseNum
				Contexts.getSessionContext().set("pharmProblemCriteriaIn", pharmProblemCriteriaIn);
	            invokeMethod("#{pharmProblemListService.retrievePharmProblemListByPharmProblemCriteria(pharmProblemCriteriaIn)}");
	            		
	            pharmProblemListFind = (List<PharmProblem>)getValue("#{pharmProblemList}");
	            pharmProblemFind = pharmProblemListFind.get(0);
				
				Contexts.getSessionContext().set("caseNum", "TEST1A");
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
	            
				
				qaProblemIn.setCaseNum("TEST1");
	            
	            Contexts.getSessionContext().set("pharmProblemIn", pharmProblemFind);
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("caseTypeIn", "OldCase");
				Contexts.getSessionContext().set("qaBatchNumListIn", null);
				invokeMethod("#{pharmProblemNewService.updateNewPharmProblemWithQaProblem(pharmProblemIn, qaProblemIn, caseTypeIn, qaBatchNumListIn)}");
				
				 //retrieve by caseNum
				Contexts.getSessionContext().set("caseNum", "TEST1");
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");*/
	            ////////////////////////////////////////////////////////////////////
	            ///// Update qaProblem
	            ////////////////////////////////////////////////////////
	            qaProblemIn.setCaseStatus(CaseStatus.Active);
					
	            Classification classificationIn = new Classification();
	            
				if(qaProblemIn.getClassification()!=null)
				{
					classificationIn = qaProblemIn.getClassification();
				}
				
				classificationIn.setClassificationCode(ProblemClassification.C0);
				classificationIn.setActionTaken("action taken test");
				qaProblemIn.setClassification(classificationIn);
				
				QaProblemNature qaProblemNatureIn = new QaProblemNature();
				if(qaProblemIn.getQaProblemNature() != null)
				{
					qaProblemNatureIn = qaProblemIn.getQaProblemNature();
				}
				
				qaProblemNatureIn.setPhysicalDefectFlag(YesNoFlag.Yes);
				qaProblemNatureIn.setProblemDetail("Problem XXX Test");
				
				qaProblemIn.setQaProblemNature(qaProblemNatureIn);
				
				qaProblemIn.setCaseSummary("Case Summary Test");
				
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("qaProblemFileUploadDataIn", null);
				Contexts.getSessionContext().set("qaProblemInstitutionListIn", new ArrayList<QaProblemInstitution>());
				Contexts.getSessionContext().set("qaProblemInstitutionAllInstitutionFlagIn", YesNoFlag.No);
		        invokeMethod("#{qaProblemService.updateQaProblemWithQaProblem(qaProblemIn,qaProblemFileUploadDataIn,qaProblemInstitutionListIn,qaProblemInstitutionAllInstitutionFlagIn)}");
				
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
	            
	            //////////////////////////////////////////////////
	            //// CreateBatchSusp
	            //////////////////////////////////////////////////
	            List<BatchSusp> batchSuspListIn = new ArrayList();
	            BatchSusp batchSuspIn = new BatchSusp();
	            batchSuspIn.setInstitution(institutionIn);
	            batchSuspIn.setBatchSuspQtyOnHand(10);
	            batchSuspIn.setQaBatchNum(qaProblemIn.getQaBatchNumList().get(0));
	            batchSuspListIn.add(batchSuspIn);
	            
	            Contexts.getSessionContext().set("batchSuspListIn", batchSuspListIn);
		        invokeMethod("#{batchSuspService.createBatchSusp(batchSuspListIn)}");
		        
		        BatchSuspCriteria batchSuspCriteriaIn = new BatchSuspCriteria();
	            batchSuspCriteriaIn.setCaseNum("TEST1");
	            batchSuspCriteriaIn.setPcu(institutionIn);
	            batchSuspCriteriaIn.setInstitution(institutionIn);
	            
	            // retrieve by criteria
	            Contexts.getSessionContext().set("batchSuspCriteriaIn", batchSuspCriteriaIn);
	            invokeMethod("#{batchSuspListService.retrieveBatchSuspListByBatchSuspCriteria(batchSuspCriteriaIn)}");
	            
	            assert getValue("#{batchSuspList!=null}").equals(true);
	            batchSuspListIn =  (List<BatchSusp>)getValue("#{batchSuspList}");
	            
	            //retrieveBatchSuspQtyRptByBatchSuspList
	            Contexts.getSessionContext().set("batchSuspListIn", batchSuspListIn);
	            invokeMethod("#{batchSuspListService.retrieveBatchSuspQtyRptByBatchSuspList(batchSuspListIn)}");
	            
	            Contexts.getSessionContext().set("batchSuspIn", batchSuspListIn.get(0));
	            invokeMethod("#{batchSuspService.retrieveBatchSuspByBatchSusp(batchSuspIn)}");
	            
	            //////////////////////////////////////////////////
	            //// UpdateBatchSusp
	            //////////////////////////////////////////////////
	            batchSuspIn = batchSuspListIn.get(0);
	            batchSuspIn.setBatchSuspQtyOnHand(15);
	            
	            Contexts.getSessionContext().set("batchSuspIn", batchSuspIn);
		        invokeMethod("#{batchSuspService.updateBatchSusp(batchSuspIn)}");
		        
		        Contexts.getSessionContext().set("batchSuspCriteriaIn", batchSuspCriteriaIn);
	            invokeMethod("#{batchSuspListService.retrieveBatchSuspListByBatchSuspCriteria(batchSuspCriteriaIn)}");
	            
	            batchSuspListIn =  (List<BatchSusp>)getValue("#{batchSuspList}");
	            batchSuspIn = batchSuspListIn.get(0);
		        //////////////////////////////////////////////////
	            //// DeleteBatchSusp
	            //////////////////////////////////////////////////
	            Contexts.getSessionContext().set("batchSuspIn", batchSuspIn);
		        invokeMethod("#{batchSuspService.deleteBatchSusp(batchSuspIn)}");
		        
	            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        /////////////////////////////////////////////////////
		        //// Create FaxFinalSupp
		        /////////////////////////////////////////////////////
		        invokeMethod("#{userInfoListService.retrieveUserInfoList()}");
	            List<UserInfo> userInfoListFind = (List<UserInfo>)getValue("#{userInfoList}");
	            UserInfo userInfoFind = userInfoListFind.get(0);
		        
	            FaxDetail faxDetail1 = new FaxDetail();
					
	            faxDetail1.setFaxTo("faxTo");
	            faxDetail1.setFaxNum("12345678");
	            faxDetail1.setFaxAttn("");
	            faxDetail1.setFaxCcEncl("");
	            faxDetail1.setFaxType(FaxType.FS);
	            faxDetail1.setFaxSender(userInfoFind);
	            faxDetail1.setSuppManufFlag("S");
	            
	            EventLog eventLogIn1 = new EventLog();
	            eventLogIn1.setEventLogDate(problemDate);
	            eventLogIn1.setEventDesc((FaxType.FS).getDisplayValue());
	            eventLogIn1.setEventType(SystemManualFlag.System);
	            eventLogIn1.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn1.setQaProblem(qaProblemIn);
	            eventLogIn1.setFaxDetail(faxDetail1);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn1 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn1.setFaxDetail(faxDetail1);
	            faxDetailPharmProblemIn1.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn1 = new ArrayList();
				faxDetailPharmProblemListIn1.add(faxDetailPharmProblemIn1);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn1 = new FaxSummary();
					faxSummaryIn1.setFinalSCount(1);
					faxSummaryIn1.setFinalSUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn1); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getFinalSCount()==null || qaProblemIn.getFaxSummary().getFinalSCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setFinalSCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setFinalSCount(qaProblemIn.getFaxSummary().getFinalSCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setFinalSUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn1);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn1);
		        invokeMethod("#{faxFinalSuppService.createFaxFinalSupp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
				
		        /////////////////////////////////////////////////////
		        //// Create FaxFinalSuppRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn1);
				Contexts.getSessionContext().set("refNumIn", "REF1");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxFinalSuppService.createFaxFinalSuppRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
  		      	//retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxFinalHosp
		        /////////////////////////////////////////////////////
		        FaxFinalHosp faxFinalHospIn = new FaxFinalHosp();
		        faxFinalHospIn.setNoActionFlag(YesNoFlag.Yes);
		        
	            FaxDetail faxDetail2 = new FaxDetail();
					
	            faxDetail2.setFaxTo("faxTo");
	            faxDetail2.setFaxNum("23456789");
	            faxDetail2.setFaxAttn("");
	            faxDetail2.setFaxCcEncl("");
	            faxDetail2.setFaxType(FaxType.FH);
	            faxDetail2.setFaxSender(userInfoFind);
	            faxDetail2.setSuppManufFlag("S");
	            faxDetail2.setFaxFinalHosp(faxFinalHospIn);
	            
	            EventLog eventLogIn2 = new EventLog();
	            eventLogIn2.setEventLogDate(problemDate);
	            eventLogIn2.setEventDesc((FaxType.FH).getDisplayValue());
	            eventLogIn2.setEventType(SystemManualFlag.System);
	            eventLogIn2.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn2.setQaProblem(qaProblemIn);
	            eventLogIn2.setFaxDetail(faxDetail2);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn2 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn2.setFaxDetail(faxDetail2);
	            faxDetailPharmProblemIn2.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn2 = new ArrayList();
				faxDetailPharmProblemListIn2.add(faxDetailPharmProblemIn2);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn2 = new FaxSummary();
					faxSummaryIn2.setFinalHCount(1);
					faxSummaryIn2.setFinalHUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn2); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getFinalHCount()==null || qaProblemIn.getFaxSummary().getFinalHCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setFinalHCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setFinalHCount(qaProblemIn.getFaxSummary().getFinalHCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setFinalHUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn2);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn2);
		        invokeMethod("#{faxFinalHospService.createFaxFinalHosp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxFinalHospRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn2);
				Contexts.getSessionContext().set("refNumIn", "REF2");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxFinalHospService.createFaxFinalHospRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitHosp
		        /////////////////////////////////////////////////////
		        FaxInitHosp faxInitHospIn = new FaxInitHosp();
		        faxInitHospIn.setIndex0Flag(YesNoFlag.Yes);
		        
	            FaxDetail faxDetail3 = new FaxDetail();
					
	            faxDetail3.setFaxTo("faxTo");
	            faxDetail3.setFaxNum("34567890");
	            faxDetail3.setFaxAttn("");
	            faxDetail3.setFaxCcEncl("");
	            faxDetail3.setFaxType(FaxType.IH);
	            faxDetail3.setFaxSender(userInfoFind);
	            faxDetail3.setSuppManufFlag("S");
	            faxDetail3.setFaxInitHosp(faxInitHospIn);
	            
	            EventLog eventLogIn3 = new EventLog();
	            eventLogIn3.setEventLogDate(problemDate);
	            eventLogIn3.setEventDesc((FaxType.IH).getDisplayValue());
	            eventLogIn3.setEventType(SystemManualFlag.System);
	            eventLogIn3.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn3.setQaProblem(qaProblemIn);
	            eventLogIn3.setFaxDetail(faxDetail3);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn3 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn3.setFaxDetail(faxDetail3);
	            faxDetailPharmProblemIn3.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn3 = new ArrayList();
				faxDetailPharmProblemListIn3.add(faxDetailPharmProblemIn3);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn3 = new FaxSummary();
					faxSummaryIn3.setInitHCount(1);
					faxSummaryIn3.setInitHUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn3); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getInitHCount()==null || qaProblemIn.getFaxSummary().getInitHCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setInitHCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setInitHCount(qaProblemIn.getFaxSummary().getInitHCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setInitHUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn3);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn3);
		        invokeMethod("#{faxInitHospService.createFaxInitHosp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitHospRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn3);
				Contexts.getSessionContext().set("refNumIn", "REF3");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxInitHospService.createFaxInitHospRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitSupp
		        /////////////////////////////////////////////////////
		        FaxInitSupp faxInitSuppIn = new FaxInitSupp();
		        faxInitSuppIn.setProvideExplainDate(problemDate);
		        
	            FaxDetail faxDetail4 = new FaxDetail();
					
	            faxDetail4.setFaxTo("faxTo");
	            faxDetail4.setFaxNum("45678912");
	            faxDetail4.setFaxAttn("");
	            faxDetail4.setFaxCcEncl("");
	            faxDetail4.setFaxType(FaxType.IS);
	            faxDetail4.setFaxSender(userInfoFind);
	            faxDetail4.setSuppManufFlag("S");
	            faxDetail4.setFaxInitSupp(faxInitSuppIn);
	            
	            EventLog eventLogIn4 = new EventLog();
	            eventLogIn4.setEventLogDate(problemDate);
	            eventLogIn4.setEventDesc((FaxType.IS).getDisplayValue());
	            eventLogIn4.setEventType(SystemManualFlag.System);
	            eventLogIn4.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn4.setQaProblem(qaProblemIn);
	            eventLogIn4.setFaxDetail(faxDetail4);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn4 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn4.setFaxDetail(faxDetail4);
	            faxDetailPharmProblemIn4.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn4 = new ArrayList();
				faxDetailPharmProblemListIn4.add(faxDetailPharmProblemIn4);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn4 = new FaxSummary();
					faxSummaryIn4.setInitSCount(1);
					faxSummaryIn4.setInitSUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn4); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getInitSCount()==null || qaProblemIn.getFaxSummary().getInitSCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setInitSCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setInitSCount(qaProblemIn.getFaxSummary().getInitSCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setInitSUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn4);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn4);
		        invokeMethod("#{faxInitSuppService.createFaxInitSupp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitSuppRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn4);
				Contexts.getSessionContext().set("refNumIn", "REF4");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxInitSuppService.createFaxInitSuppRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        
	            /////////////////////////////////////////////////////
		        //// Create FaxInterimHosp
		        /////////////////////////////////////////////////////
		        FaxInterimHosp faxInterimHospIn = new FaxInterimHosp();
		        faxInterimHospIn.setInvestRptFlag(YesNoFlag.Yes);
		        
	            FaxDetail faxDetail5 = new FaxDetail();
					
	            faxDetail5.setFaxTo("faxTo");
	            faxDetail5.setFaxNum("56789123");
	            faxDetail5.setFaxAttn("");
	            faxDetail5.setFaxCcEncl("");
	            faxDetail5.setFaxType(FaxType.MH);
	            faxDetail5.setFaxSender(userInfoFind);
	            faxDetail5.setSuppManufFlag("S");
	            faxDetail5.setFaxInterimHosp(faxInterimHospIn);
	            
	            EventLog eventLogIn5 = new EventLog();
	            eventLogIn5.setEventLogDate(problemDate);
	            eventLogIn5.setEventDesc((FaxType.MH).getDisplayValue());
	            eventLogIn5.setEventType(SystemManualFlag.System);
	            eventLogIn5.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn5.setQaProblem(qaProblemIn);
	            eventLogIn5.setFaxDetail(faxDetail5);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn5 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn5.setFaxDetail(faxDetail5);
	            faxDetailPharmProblemIn5.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn5 = new ArrayList();
				faxDetailPharmProblemListIn5.add(faxDetailPharmProblemIn5);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn5 = new FaxSummary();
					faxSummaryIn5.setInterimHCount(1);
					faxSummaryIn5.setInterimHUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn5); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getInterimHCount()==null || qaProblemIn.getFaxSummary().getInterimHCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setInterimHCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setInterimHCount(qaProblemIn.getFaxSummary().getInterimHCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setInterimHUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn5);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn5);
		        invokeMethod("#{faxInterimHospService.createFaxInterimHosp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitSuppRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn5);
				Contexts.getSessionContext().set("refNumIn", "REF5");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxInterimHospService.createFaxInterimHospRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
	            
	            /////////////////////////////////////////////////////
		        //// Create FaxInterimSupp
		        /////////////////////////////////////////////////////
		        FaxInterimSupp faxInterimSuppIn = new FaxInterimSupp();
		        faxInterimSuppIn.setInvestRptFlag(YesNoFlag.Yes);
		        
	            FaxDetail faxDetail6 = new FaxDetail();
					
	            faxDetail6.setFaxTo("faxTo");
	            faxDetail6.setFaxNum("67891234");
	            faxDetail6.setFaxAttn("");
	            faxDetail6.setFaxCcEncl("");
	            faxDetail6.setFaxType(FaxType.MS);
	            faxDetail6.setFaxSender(userInfoFind);
	            faxDetail6.setSuppManufFlag("S");
	            faxDetail6.setFaxInterimSupp(faxInterimSuppIn);
	            
	            EventLog eventLogIn6 = new EventLog();
	            eventLogIn6.setEventLogDate(problemDate);
	            eventLogIn6.setEventDesc((FaxType.MS).getDisplayValue());
	            eventLogIn6.setEventType(SystemManualFlag.System);
	            eventLogIn6.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn6.setQaProblem(qaProblemIn);
	            eventLogIn6.setFaxDetail(faxDetail6);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn6 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn6.setFaxDetail(faxDetail6);
	            faxDetailPharmProblemIn6.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn6 = new ArrayList();
				faxDetailPharmProblemListIn6.add(faxDetailPharmProblemIn6);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn6 = new FaxSummary();
					faxSummaryIn6.setInterimSCount(1);
					faxSummaryIn6.setInterimSUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn6); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getInterimSCount()==null || qaProblemIn.getFaxSummary().getInterimSCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setInterimSCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setInterimSCount(qaProblemIn.getFaxSummary().getInterimSCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setInterimSUpdateDate(problemDate);
				}
		        
				Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn6);
				Contexts.getSessionContext().set("faxDetailPharmProblemListIn", faxDetailPharmProblemListIn6);
		        invokeMethod("#{faxInterimSuppService.createFaxInterimSupp(qaProblemIn, eventLogIn, faxDetailPharmProblemListIn)}");
		        
		        /////////////////////////////////////////////////////
		        //// Create FaxInitSuppRpt
		        /////////////////////////////////////////////////////
		        Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
				Contexts.getSessionContext().set("eventLogIn", eventLogIn6);
				Contexts.getSessionContext().set("refNumIn", "REF6");
				Contexts.getSessionContext().set("reGenFlag", false);
		        invokeMethod("#{faxInterimSuppService.createFaxInterimSuppRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag)}");
		        
		      //retrieve by caseNum
		        invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	            
	            invokeMethod("#{userInfoListService.retrieveUserInfoList()}");
	            userInfoListFind = (List<UserInfo>)getValue("#{userInfoList}");
	            userInfoFind = userInfoListFind.get(0);
		        
	            FaxDetail faxDetail7 = new FaxDetail();
					
	            faxDetail7.setFaxTo("faxTo");
	            faxDetail7.setFaxNum("78912345");
	            faxDetail7.setFaxAttn("");
	            faxDetail7.setFaxCcEncl("");
	            faxDetail7.setFaxType(FaxType.FS);
	            faxDetail7.setFaxSender(userInfoFind);
	            faxDetail7.setSuppManufFlag("S");
	            
	            EventLog eventLogIn7 = new EventLog();
	            eventLogIn7.setEventLogDate(problemDate);
	            eventLogIn7.setEventDesc((FaxType.FS).getDisplayValue());
	            eventLogIn7.setEventType(SystemManualFlag.System);
	            eventLogIn7.setFaxLogFlag(YesNoFlag.Yes);
	            eventLogIn7.setQaProblem(qaProblemIn);
	            eventLogIn7.setFaxDetail(faxDetail7);
	            
	            List<EventLog> els = new ArrayList();
	            els.add(eventLogIn7);
	            
	            qaProblemIn.setEventLogList(els);
	            
	            FaxDetailPharmProblem faxDetailPharmProblemIn7 = new FaxDetailPharmProblem();
	            faxDetailPharmProblemIn7.setFaxDetail(faxDetail7);
	            faxDetailPharmProblemIn7.setPharmProblem(pharmProblemFind);
				List<FaxDetailPharmProblem> faxDetailPharmProblemListIn7 = new ArrayList();
				faxDetailPharmProblemListIn7.add(faxDetailPharmProblemIn7);
	            
				if(qaProblemIn.getFaxSummary()==null)
				{
					FaxSummary faxSummaryIn7 = new FaxSummary();
					faxSummaryIn7.setFinalSCount(1);
					faxSummaryIn7.setFinalSUpdateDate(problemDate);
					
					qaProblemIn.setFaxSummary(faxSummaryIn7); 
				}
				else
				{
					if(qaProblemIn.getFaxSummary().getFinalSCount()==null || qaProblemIn.getFaxSummary().getFinalSCount().toString()=="")
					{
						qaProblemIn.getFaxSummary().setFinalSCount(1);
					}
					else
					{
						qaProblemIn.getFaxSummary().setFinalSCount(qaProblemIn.getFaxSummary().getFinalSCount()+1);
					}
					
					qaProblemIn.getFaxSummary().setFinalSUpdateDate(problemDate);
				}
	            
	            /////////////////////////////////////////////////////
		        //// Create CaseFinalRpt
		        /////////////////////////////////////////////////////
				
				SupplierContact supplierContactIn = new SupplierContact();
				CompanyContact companyContactIn = new CompanyContact();
				
				Supplier supplierRptIn = new Supplier();
				Company companyRptIn = new Company();
				
				Contact contact1In = new Contact();
				Contact contact2In = new Contact();
				
				supplierRptIn.setSupplierCode("3M");
				supplierRptIn.setSupplierName("3M");
				
				companyRptIn.setCompanyCode("3M");
				companyRptIn.setCompanyName("3M");
				
				contact1In.setOfficePhone("1234");
				contact1In.setMobilePhone("1234");
				contact1In.setFax("1234");
				contact1In.setFirstName("1234");
				contact1In.setLastName("1234");
				
				contact2In.setOfficePhone("1234");
				contact2In.setMobilePhone("1234");
				contact2In.setFax("1234");
				contact2In.setFirstName("1234");
				contact2In.setLastName("1234");
				
				supplierContactIn.setSupplier(supplierRptIn);
				supplierContactIn.setContact(contact1In);
				
				companyContactIn.setCompany(companyRptIn);
				companyContactIn.setContact(contact2In);
				
	            Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
	            Contexts.getSessionContext().set("supplierContactIn", supplierContactIn);
	            Contexts.getSessionContext().set("companyContactIn", companyContactIn);
	            invokeMethod("#{qaProblemService.createCaseFinalRpt(qaProblemIn,supplierContactIn,companyContactIn)}");
	            //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	            Contexts.getSessionContext().set("qaProblemCriteriaIn", qaProblemCriteriaIn);
	            invokeMethod("#{eventLogListService.retrieveEventLogListByQaProblemCriteria(qaProblemCriteriaIn)}");
	            
	            Contexts.getSessionContext().set("eventLogList", qaProblemIn.getEventLogList());
	            invokeMethod("#{eventLogListService.retrieveFaxDetailLogByEventLogList(eventLogList)}");
		        

	            //////////////////////////////////////
	            // delete qaProblem
	            ////////////////////////////////////////
		        //retrieve by caseNum
	            invokeMethod("#{qaProblemService.retrieveQaProblemByCaseNum(caseNum)}");
	            assert getValue("#{qaProblem!=null}").equals(true);
	            qaProblemIn = (QaProblem)getValue("#{qaProblem}");
		        
	            Contexts.getSessionContext().set("qaProblemFind", qaProblemIn);
	            Contexts.getSessionContext().set("deleteReason", "Test Delete");
	 	        invokeMethod("#{qaProblemService.deleteQaProblem(qaProblemFind, deleteReason)}");
	            
	            /////////////////////////////////////////////////////////////////
	 	        // VO init
	 	        /////////////////////////////////////////////////////////////////////
	 	        QaProblemFileUploadData qaProblemFileUploadDataInit = new QaProblemFileUploadData(); 
	 	        qaProblemFileUploadDataInit.setRefUploadFileName("");
	 	        qaProblemFileUploadDataInit.setItemCode("");
	 	        qaProblemFileUploadDataInit.setSupplierCode("");
	 	        qaProblemFileUploadDataInit.setManufCode("");
	 	        qaProblemFileUploadDataInit.setPharmCompanyCode("");
	 	        qaProblemFileUploadDataInit.setActionType("");
	 	        
	 	        qaProblemFileUploadDataInit.getRefUploadFileName();
	 	        qaProblemFileUploadDataInit.getItemCode();
	 	        qaProblemFileUploadDataInit.getSupplierCode();
	 	        qaProblemFileUploadDataInit.getManufCode();
	 	        qaProblemFileUploadDataInit.getPharmCompanyCode();
	 	        qaProblemFileUploadDataInit.getActionType();
	 	        
	 	        /////////////////////////////////////////////////////////////////////
	 	        PharmProblemFileUploadData pharmProblemFileUploadDataInit = new PharmProblemFileUploadData();
	 	        pharmProblemFileUploadDataInit.setRefUploadFileName("");
	 	        pharmProblemFileUploadDataInit.setInstitutionCode("");
	 	        pharmProblemFileUploadDataInit.setItemCode("");
	 	        pharmProblemFileUploadDataInit.setSupplierCode("");
	 	        pharmProblemFileUploadDataInit.setManufCode("");
	 	        pharmProblemFileUploadDataInit.setPharmCompanyCode("");
	 	        
	 	        pharmProblemFileUploadDataInit.getRefUploadFileName();
	 	        pharmProblemFileUploadDataInit.getInstitutionCode();
	 	        pharmProblemFileUploadDataInit.getItemCode();
	 	        pharmProblemFileUploadDataInit.getSupplierCode();
	 	        pharmProblemFileUploadDataInit.getManufCode();
	 	        pharmProblemFileUploadDataInit.getPharmCompanyCode();
	 	        /////////////////////////////////////////////////////////////////////
	 	        FaxFinalHospRpt faxFinalHospRptInit = new FaxFinalHospRpt();
	 	        faxFinalHospRptInit.setInitFaxCreateDate("");
	 	        faxFinalHospRptInit.setFinalFaxCreateDate("");
	 	        faxFinalHospRptInit.setFaxTo("");
	 	        faxFinalHospRptInit.setContractNo("");
	 	        faxFinalHospRptInit.setRefNum("");
	 	        faxFinalHospRptInit.setItemCode("");
	 	        faxFinalHospRptInit.setFullDrugDesc("");
	 	        faxFinalHospRptInit.setManuf("");
	 	        faxFinalHospRptInit.setProblemDetail("");
	 	        faxFinalHospRptInit.setBatchNum("");
	 	        faxFinalHospRptInit.setBatchSuspDate("");
	 	        faxFinalHospRptInit.setProductRecallDesc("");
	 	        faxFinalHospRptInit.setIndex0Flag(true);
	 	        faxFinalHospRptInit.setIndex1Flag(true);
	 	        faxFinalHospRptInit.setResponseRptDate("30-JUL-2012");
	 	        faxFinalHospRptInit.setInspectAllFlag(DqaRptUtil.getBoolean(YesNoFlag.Yes));
	 	        faxFinalHospRptInit.setRandomInspectFlag(DqaRptUtil.getBoolean(YesNoFlag.No));
	 	        faxFinalHospRptInit.setRptFindingFlag(DqaRptUtil.getBoolean(YesNoFlag.Yes));
	 	        faxFinalHospRptInit.setIndex1Date("02-AUG-2012");
	 	        faxFinalHospRptInit.setIndex2Flag(true);
	 	        faxFinalHospRptInit.setBatchSuspFlag(true);
	 	        faxFinalHospRptInit.setProductRecallFlag(true);
	 	        faxFinalHospRptInit.setInitLevelFlag(true);
	 	        faxFinalHospRptInit.setPatLevelFlag(true);
	 	        faxFinalHospRptInit.setFaxFrom("");
	 	        faxFinalHospRptInit.setInvestRptFlag(true);
	 	        faxFinalHospRptInit.setRelUseOfHoldFlag(true);
	 	        faxFinalHospRptInit.setStkReplaceFlag(true);
	 	        faxFinalHospRptInit.setInvestRptFlag(true);
	 	        faxFinalHospRptInit.setOtherFlag(true);
	 	        
	 	     	faxFinalHospRptInit.getInitFaxCreateDate();
	 	     	faxFinalHospRptInit.getFinalFaxCreateDate();
	 	        faxFinalHospRptInit.getFaxTo();
	 	        faxFinalHospRptInit.getContractNo();
	 	        faxFinalHospRptInit.getRefNum();
	 	        faxFinalHospRptInit.getItemCode();
	 	        faxFinalHospRptInit.getFullDrugDesc();
	 	        faxFinalHospRptInit.getManuf();
	 	        faxFinalHospRptInit.getProblemDetail();
	 	        faxFinalHospRptInit.getBatchNum();
	 	        faxFinalHospRptInit.getBatchSuspDate();
	 	        faxFinalHospRptInit.getProductRecallDesc();
	 	        faxFinalHospRptInit.getIndex0Flag();
	 	        faxFinalHospRptInit.getIndex1Flag();
	 	        faxFinalHospRptInit.getResponseRptDate();
	 	        faxFinalHospRptInit.getInspectAllFlag();
	 	        faxFinalHospRptInit.getRandomInspectFlag();
	 	        faxFinalHospRptInit.getRptFindingFlag();
	 	        faxFinalHospRptInit.getIndex1Date();
	 	        faxFinalHospRptInit.getIndex2Flag();
	 	        faxFinalHospRptInit.getBatchSuspFlag();
	 	        faxFinalHospRptInit.getProductRecallFlag();
	 	        faxFinalHospRptInit.getInitLevelFlag();
	 	        faxFinalHospRptInit.getPatLevelFlag();
	 	        faxFinalHospRptInit.getFaxFrom();
	 	        faxFinalHospRptInit.getInvestRptFlag();
	 	        faxFinalHospRptInit.getRelUseOfHoldFlag();
	 	        faxFinalHospRptInit.getStkReplaceFlag();
	 	        faxFinalHospRptInit.getInvestRptFlag();
	 	        faxFinalHospRptInit.getOtherFlag();
	 	        	
	 	        /////////////////////////////////////////////////////////////////////
	 	        FaxSummaryRpt faxSummaryRptInit = new FaxSummaryRpt();
	 	        faxSummaryRptInit.setCaseStatus("");
	 	        faxSummaryRptInit.setCaseNum("");
	 	        faxSummaryRptInit.setClassification("");
	 	        faxSummaryRptInit.setItemCode("");
	 	        faxSummaryRptInit.setItemDesc("");
	 	        faxSummaryRptInit.setOrderType("");
	 	        faxSummaryRptInit.setSupplier("");
	 	        faxSummaryRptInit.setManuf("");
	 	        faxSummaryRptInit.setPharmCom("");
	 	        faxSummaryRptInit.setProblemDetail("");
	 	        faxSummaryRptInit.setLastInitHospDate(problemDate);
	 	        faxSummaryRptInit.setInitHospCount("");
	 	        faxSummaryRptInit.setLastInitSuppDate(problemDate);
	 	        faxSummaryRptInit.setInitSuppCount("");
	 	        faxSummaryRptInit.setLastInterimHospDate(problemDate);
	 	        faxSummaryRptInit.setInterimHospCount("");
	 	        faxSummaryRptInit.setLastInterimSuppDate(problemDate);
	 	        faxSummaryRptInit.setInterimSuppCount("");
	 	        faxSummaryRptInit.setLastFinalHospDate(problemDate);
	 	        faxSummaryRptInit.setFinalHospCount("");
	 	        faxSummaryRptInit.setLastFinalSuppDate(problemDate);
	 	        faxSummaryRptInit.setFinalSuppCount("");
	 	        faxSummaryRptInit.setCaseCreateDate(problemDate);
	 	        
	 	        faxSummaryRptInit.getCaseStatus();
	 	        faxSummaryRptInit.getCaseNum();
	 	        faxSummaryRptInit.getClassification();
	 	        faxSummaryRptInit.getItemCode();
	 	        faxSummaryRptInit.getItemDesc();
	 	        faxSummaryRptInit.getOrderType();
	 	        faxSummaryRptInit.getSupplier();
	 	        faxSummaryRptInit.getManuf();
	 	        faxSummaryRptInit.getPharmCom();
	 	        faxSummaryRptInit.getProblemDetail();
	 	        faxSummaryRptInit.getLastInitHospDate();
	 	        faxSummaryRptInit.getInitHospCount();
	 	        faxSummaryRptInit.getLastInitSuppDate();
	 	        faxSummaryRptInit.getInitSuppCount();
	 	        faxSummaryRptInit.getLastInterimHospDate();
	 	        faxSummaryRptInit.getInterimHospCount();
	 	        faxSummaryRptInit.getLastInterimSuppDate();
	 	        faxSummaryRptInit.getInterimSuppCount();
	 	        faxSummaryRptInit.getLastFinalHospDate();
	 	        faxSummaryRptInit.getFinalHospCount();
	 	        faxSummaryRptInit.getLastFinalSuppDate();
	 	        faxSummaryRptInit.getFinalSuppCount();
	 	        faxSummaryRptInit.getCaseCreateDate();
	 	        
	 	        /////////////////////////////////////////////////////////////////////
	 	        FaxDetailLogRpt faxDetailLogRptInit = new FaxDetailLogRpt();
	 	        faxDetailLogRptInit.setCaseStatus("");
	 	        faxDetailLogRptInit.setCaseNum("");
	 	        faxDetailLogRptInit.setRefNum("");
	 	        faxDetailLogRptInit.setClassification("");
	 	        faxDetailLogRptInit.setItemCode("");
	 	        faxDetailLogRptInit.setItemDesc("");
	 	        faxDetailLogRptInit.setOrderType("");
	 	        faxDetailLogRptInit.setSupplier("");
	 	        faxDetailLogRptInit.setManuf("");
	 	        faxDetailLogRptInit.setPharmCom("");
	 	        faxDetailLogRptInit.setFaxType("");
	 	        faxDetailLogRptInit.setFrom("");
	 	        faxDetailLogRptInit.setTo("");
	 	        faxDetailLogRptInit.setProblemDetail("");
	 	        faxDetailLogRptInit.setCaseCreateDate(problemDate);
	 	        faxDetailLogRptInit.setFaxGenerationDate(problemDate);
	 	        
	 	        faxDetailLogRptInit.getCaseStatus();
	 	        faxDetailLogRptInit.getCaseNum();
	 	        faxDetailLogRptInit.getRefNum();
	 	        faxDetailLogRptInit.getClassification();
	 	        faxDetailLogRptInit.getItemCode();
	 	        faxDetailLogRptInit.getItemDesc();
	 	        faxDetailLogRptInit.getOrderType();
	 	        faxDetailLogRptInit.getSupplier();
	 	        faxDetailLogRptInit.getManuf();
	 	        faxDetailLogRptInit.getPharmCom();
	 	        faxDetailLogRptInit.getFaxType();
	 	        faxDetailLogRptInit.getFrom();
	 	        faxDetailLogRptInit.getTo();
	 	        faxDetailLogRptInit.getProblemDetail();
	 	        faxDetailLogRptInit.getCaseCreateDate();
	 	        faxDetailLogRptInit.getFaxGenerationDate();
	 	        
	 	        /////////////////////////////////////////////////////////////////////
	 	        FaxInterimHospRpt faxInterimHospRptInit = new FaxInterimHospRpt();
	 	        faxInterimHospRptInit.setFaxCreateDate("");
	 	        faxInterimHospRptInit.setFaxTo("");
	 	        faxInterimHospRptInit.setContractNo("");
	 	        faxInterimHospRptInit.setRefNum("");
	 	        faxInterimHospRptInit.setItemCode("");
	 	        faxInterimHospRptInit.setFullDrugDesc("");
	 	        faxInterimHospRptInit.setManuf("");
	 	        faxInterimHospRptInit.setProblemDetail("");
	 	        faxInterimHospRptInit.setBatchNum("");
	 	        faxInterimHospRptInit.setFaxFrom("");
	 	        faxInterimHospRptInit.setCaseNum("");
	 	        faxInterimHospRptInit.setOrderType("");
	 	        faxInterimHospRptInit.setSimlarProblem("");
	 	        faxInterimHospRptInit.setInvestRptFlag(true);
	 	        faxInterimHospRptInit.setWithHoldUseFlag(true);
	 	        faxInterimHospRptInit.setStkReplaceFlag(true);
	 	        faxInterimHospRptInit.setOtherFlag(true);
	 	        faxInterimHospRptInit.setOtherDesc("");
	 	        
	 	        faxInterimHospRptInit.getFaxCreateDate();
	 	        faxInterimHospRptInit.getFaxTo();
	 	        faxInterimHospRptInit.getContractNo();
	 	        faxInterimHospRptInit.getRefNum();
	 	        faxInterimHospRptInit.getItemCode();
	 	        faxInterimHospRptInit.getFullDrugDesc();
	 	        faxInterimHospRptInit.getManuf();
	 	        faxInterimHospRptInit.getProblemDetail();
	 	        faxInterimHospRptInit.getBatchNum();
	 	        faxInterimHospRptInit.getFaxFrom();
	 	        faxInterimHospRptInit.getCaseNum();
	 	        faxInterimHospRptInit.getOrderType();
	 	        faxInterimHospRptInit.getSimlarProblem();
	 	        faxInterimHospRptInit.getInvestRptFlag();
	 	        faxInterimHospRptInit.getWithHoldUseFlag();
	 	        faxInterimHospRptInit.getStkReplaceFlag();
	 	        faxInterimHospRptInit.getOtherFlag();
	 	        faxInterimHospRptInit.getOtherDesc();
	 	        ///////////////////////////////////////////////////////////////////
	 	        BatchSuspQtyRpt batchSuspQtyRptInit = new BatchSuspQtyRpt();
	 	        batchSuspQtyRptInit.setPcuCode("");
	 	        batchSuspQtyRptInit.setInst("");
	 	        batchSuspQtyRptInit.setCaseNum("");
	 	        batchSuspQtyRptInit.setItemCode("");
	 	        batchSuspQtyRptInit.setItemDesc("");
	 	        batchSuspQtyRptInit.setClassification("");
	 	        batchSuspQtyRptInit.setBatchNum("");
	 	        batchSuspQtyRptInit.setQtyOnHand("");
	 	        batchSuspQtyRptInit.setUpdateDate(problemDate);
	 	        
	 	        batchSuspQtyRptInit.getPcuCode();
	 	        batchSuspQtyRptInit.getInst();
	 	        batchSuspQtyRptInit.getCaseNum();
	 	        batchSuspQtyRptInit.getItemCode();
	 	        batchSuspQtyRptInit.getItemDesc();
	 	        batchSuspQtyRptInit.getClassification();
	 	        batchSuspQtyRptInit.getBatchNum();
	 	        batchSuspQtyRptInit.getQtyOnHand();
	 	        batchSuspQtyRptInit.getUpdateDate();
	 	        ///////////////////////////////////////////////////////////////////
	 	        FaxInitSuppRpt faxInitSuppRptInit = new FaxInitSuppRpt(); 
	 	        faxInitSuppRptInit.setFaxCreateDate("");
	 	        faxInitSuppRptInit.setFaxTo("");
	 	        faxInitSuppRptInit.setRefNum("");
	 	        faxInitSuppRptInit.setFullDrugDesc("");
	 	        faxInitSuppRptInit.setInst("");
	 	        faxInitSuppRptInit.setInstRptName("PHARMACEUTICAL HEADQUARTER PHARMACY");
	 	        faxInitSuppRptInit.setInstRptDate("02-AUG-2012");
	 	        faxInitSuppRptInit.setDistributeListDate("");
	 	        faxInitSuppRptInit.setDistributeListFlag(true);
	 	        faxInitSuppRptInit.setConfirmAvailDate("");
	 	        faxInitSuppRptInit.setConfirmAvailFlag(true);
	 	        faxInitSuppRptInit.setCollectSampleDate("");
	 	        faxInitSuppRptInit.setCollectSampleFlag(true);
	 	        faxInitSuppRptInit.setReferSampleFlag(true);
	 	        faxInitSuppRptInit.setProvideExplainDate("");
	 	        faxInitSuppRptInit.setProvideExplainFlag(true);
	 	        faxInitSuppRptInit.setContractNo("");
	 	        faxInitSuppRptInit.setFaxFromUser("");
	 	        faxInitSuppRptInit.setFaxFromPhone("");
	 	        faxInitSuppRptInit.setFaxFromEmail("");
	 	        faxInitSuppRptInit.setInstContactUser("");
	 	        faxInitSuppRptInit.setInstContactRank("");
	 	        faxInitSuppRptInit.setInstContactPhone("");
	 	        faxInitSuppRptInit.setInstContactEmail("");
	 	        
	 	        faxInitSuppRptInit.getFaxCreateDate();
	 	        faxInitSuppRptInit.getFaxTo();
	 	        faxInitSuppRptInit.getRefNum();
	 	        faxInitSuppRptInit.getFullDrugDesc();
	 	        faxInitSuppRptInit.getInst();
	 	        faxInitSuppRptInit.getInstRptName();
	 	        faxInitSuppRptInit.getInstRptDate();
	 	        faxInitSuppRptInit.getDistributeListDate();
	 	        faxInitSuppRptInit.getDistributeListFlag();
	 	        faxInitSuppRptInit.getConfirmAvailDate();
	 	        faxInitSuppRptInit.getConfirmAvailFlag();
	 	        faxInitSuppRptInit.getCollectSampleDate();
	 	        faxInitSuppRptInit.getCollectSampleFlag();
	 	        faxInitSuppRptInit.getReferSampleFlag();
	 	        faxInitSuppRptInit.getProvideExplainDate();
	 	        faxInitSuppRptInit.getProvideExplainFlag();
	 	        faxInitSuppRptInit.getContractNo();
	 	        faxInitSuppRptInit.getFaxFromUser();
	 	        faxInitSuppRptInit.getFaxFromPhone();
	 	        faxInitSuppRptInit.getFaxFromEmail();
	 	        faxInitSuppRptInit.getInstContactUser();
	 	        faxInitSuppRptInit.getInstContactRank();
	 	        faxInitSuppRptInit.getInstContactPhone();
	 	        faxInitSuppRptInit.getInstContactEmail();
	 	        ///////////////////////////////////////////////////////////////////
	 	        FaxInterimSuppRpt faxInterimSuppRptInit = new FaxInterimSuppRpt();
	 	        faxInterimSuppRptInit.setFaxCreateDate("");
	 	        faxInterimSuppRptInit.setRefNum("");
	 	        faxInterimSuppRptInit.setFullDrugDesc("");
	 	        faxInterimSuppRptInit.setManuf("");
	 	        faxInterimSuppRptInit.setFaxFrom("");
	 	        faxInterimSuppRptInit.setContractNo("");
	 	        faxInterimSuppRptInit.setItemCode("");
	 	        faxInterimSuppRptInit.setProblemDetail("");
	 	        faxInterimSuppRptInit.setBatchNum("");
	 	        faxInterimSuppRptInit.setCaseNum("");
	 	        faxInterimSuppRptInit.setOrderType("");
	 	        faxInterimSuppRptInit.setSimlarProblem("");
	 	        faxInterimSuppRptInit.setPreviousRefNum("");
	 	        faxInterimSuppRptInit.setRefNumDate("");
	 	        faxInterimSuppRptInit.setDocOnBeforeDate("");
	 	        faxInterimSuppRptInit.setInvestRptFlag(true);
	 	        faxInterimSuppRptInit.setCoaFlag(true);
	 	        faxInterimSuppRptInit.setImpMeasureFlag(true);
	 	        faxInterimSuppRptInit.setOtherDesc("");
	 	        faxInterimSuppRptInit.setOtherFlag(true);
	 	        
	 	        faxInterimSuppRptInit.getFaxCreateDate();
	 	        faxInterimSuppRptInit.getRefNum();
	 	        faxInterimSuppRptInit.getFullDrugDesc();
	 	        faxInterimSuppRptInit.getManuf();
	 	        faxInterimSuppRptInit.getFaxFrom();
	 	        faxInterimSuppRptInit.getContractNo();
	 	        faxInterimSuppRptInit.getItemCode();
	 	        faxInterimSuppRptInit.getProblemDetail();
	 	        faxInterimSuppRptInit.getBatchNum();
	 	        faxInterimSuppRptInit.getCaseNum();
	 	        faxInterimSuppRptInit.getOrderType();
	 	        faxInterimSuppRptInit.getSimlarProblem();
	 	        faxInterimSuppRptInit.getPreviousRefNum();
	 	        faxInterimSuppRptInit.getRefNumDate();
	 	        faxInterimSuppRptInit.getDocOnBeforeDate();
	 	        faxInterimSuppRptInit.getInvestRptFlag();
	 	        faxInterimSuppRptInit.getCoaFlag();
	 	        faxInterimSuppRptInit.getImpMeasureFlag();
	 	        faxInterimSuppRptInit.getOtherDesc();
	 	        faxInterimSuppRptInit.getOtherFlag();
	 	        ///////////////////////////////////////////////////////////////////
	 	        FaxInitHospRpt faxInitHospRptInit = new FaxInitHospRpt();
	 	        faxInitHospRptInit.setInitFaxCreateDate("");
	 	        faxInitHospRptInit.setFaxTo("");
	 	        faxInitHospRptInit.setContractNo("");
	 	        faxInitHospRptInit.setRefNum("");
	 	        faxInitHospRptInit.setItemCode("");
	 	        faxInitHospRptInit.setFullDrugDesc("");
	 	        faxInitHospRptInit.setManuf("");
	 	        faxInitHospRptInit.setProblemDetail("");
	 	        faxInitHospRptInit.setBatchNum("");
	 	        faxInitHospRptInit.setBatchSuspDate("");
	 	        faxInitHospRptInit.setProductRecallDesc("");
	 	        faxInitHospRptInit.setIndex0Flag(true);
	 	        faxInitHospRptInit.setIndex1Flag(true);
	 	        faxInitHospRptInit.setResponseRptDate("30-JUL-2012");
	 	        faxInitHospRptInit.setInspectAllFlag(DqaRptUtil.getBoolean(YesNoFlag.Yes));
	 	        faxInitHospRptInit.setRandomInspectFlag(DqaRptUtil.getBoolean(YesNoFlag.No));
	 	        faxInitHospRptInit.setRptFindingFlag(DqaRptUtil.getBoolean(YesNoFlag.Yes));
	 	        faxInitHospRptInit.setIndex1Date("02-AUG-2012");
	 	        faxInitHospRptInit.setIndex2Flag(true);
	 	        faxInitHospRptInit.setBatchSuspFlag(true);
	 	        faxInitHospRptInit.setProductRecallFlag(true);
	 	        faxInitHospRptInit.setInitLevelFlag(true);
	 	        faxInitHospRptInit.setPatLevelFlag(true);
	 	        faxInitHospRptInit.setFaxFrom("");
	 	        
	 	        faxInitHospRptInit.getInitFaxCreateDate();
	 	        faxInitHospRptInit.getFaxTo();
	 	        faxInitHospRptInit.getContractNo();
	 	        faxInitHospRptInit.getRefNum();
	 	        faxInitHospRptInit.getItemCode();
	 	        faxInitHospRptInit.getFullDrugDesc();
	 	        faxInitHospRptInit.getManuf();
	 	        faxInitHospRptInit.getProblemDetail();
	 	        faxInitHospRptInit.getBatchNum();
	 	        faxInitHospRptInit.getBatchSuspDate();
	 	        faxInitHospRptInit.getProductRecallDesc();
	 	        faxInitHospRptInit.getIndex0Flag();
	 	        faxInitHospRptInit.getIndex1Flag();
	 	        faxInitHospRptInit.getResponseRptDate();
	 	        faxInitHospRptInit.getInspectAllFlag();
	 	        faxInitHospRptInit.getRandomInspectFlag();
	 	        faxInitHospRptInit.getRptFindingFlag();
	 	        faxInitHospRptInit.getIndex1Date();
	 	        faxInitHospRptInit.getIndex2Flag();
	 	        faxInitHospRptInit.getBatchSuspFlag();
	 	        faxInitHospRptInit.getProductRecallFlag();
	 	        faxInitHospRptInit.getInitLevelFlag();
	 	        faxInitHospRptInit.getPatLevelFlag();
	 	        faxInitHospRptInit.getFaxFrom();
	 	        ///////////////////////////////////////////////////////////////////
	 	        FaxFinalSuppRpt faxFinalSuppRptInit = new FaxFinalSuppRpt();
	 	        faxFinalSuppRptInit.setFaxCreateDate("");
	 	        faxFinalSuppRptInit.setRefNum("");
	 	        faxFinalSuppRptInit.setFullDrugDesc("");
	 	        faxFinalSuppRptInit.setManuf("");
	 	        faxFinalSuppRptInit.setFaxFrom("");
	 	        faxFinalSuppRptInit.setContractNo("");
	 	        faxFinalSuppRptInit.setItemCode("");
	 	        faxFinalSuppRptInit.setProblemDetail("");
	 	        faxFinalSuppRptInit.setBatchNum("");
	 	        faxFinalSuppRptInit.setCaseNum("");
	 	        faxFinalSuppRptInit.setOrderType("");
	 	        faxFinalSuppRptInit.setSimlarProblem("");
	 	        
	 	        faxFinalSuppRptInit.getFaxCreateDate();
	 	        faxFinalSuppRptInit.getRefNum();
	 	        faxFinalSuppRptInit.getFullDrugDesc();
	 	        faxFinalSuppRptInit.getManuf();
	 	        faxFinalSuppRptInit.getFaxFrom();
	 	        faxFinalSuppRptInit.getContractNo();
	 	        faxFinalSuppRptInit.getItemCode();
	 	        faxFinalSuppRptInit.getProblemDetail();
	 	        faxFinalSuppRptInit.getBatchNum();
	 	        faxFinalSuppRptInit.getCaseNum();
	 	        faxFinalSuppRptInit.getOrderType();
	 	        faxFinalSuppRptInit.getSimlarProblem();
	 	        ///////////////////////////////////////////////////////////////////
	 	        PharmProblemCriteria pharmProblemCriteriaInit = new PharmProblemCriteria();
	 	        pharmProblemCriteriaInit.setProblemNum("");
	 	        pharmProblemCriteriaInit.setProblemClassification(ProblemClassification.C0);
	 	        pharmProblemCriteriaInit.setProblemStatus(ProblemStatus.Active);
	 	        pharmProblemCriteriaInit.setCaseNum("");
	 	        
	 	        pharmProblemCriteriaInit.getProblemNum();
	 	        pharmProblemCriteriaInit.getProblemClassification();
	 	        pharmProblemCriteriaInit.getProblemStatus();
	 	        pharmProblemCriteriaInit.getCaseNum();
	 	        ///////////////////////////////////////////////////////////////////
	 	        QaProblemCriteria qaProblemCriteriaInit = new QaProblemCriteria();
	 	        qaProblemCriteriaInit.setCaseNum("");
	 	        qaProblemCriteriaInit.setCaseClassification(ProblemClassification.C0);
	 	        qaProblemCriteriaInit.setCaseStatus(CaseStatus.Active);
	 	        qaProblemCriteriaInit.setFaxType(FaxType.FH);
	 	        
	 	        qaProblemCriteriaInit.getCaseNum();
	 	        qaProblemCriteriaInit.getCaseClassification();
	 	        qaProblemCriteriaInit.getCaseStatus();
	 	        qaProblemCriteriaInit.getFaxType();
	 	        ///////////////////////////////////////////////////////////////////
	 	        CaseFinalRpt caseFinalRptInit = new CaseFinalRpt();
	 	        caseFinalRptInit.setCaseNum("");
	 	        caseFinalRptInit.setCaseStatus("");
	 	        caseFinalRptInit.setItemCode("");
	 	        caseFinalRptInit.setClassification("");
	 	        caseFinalRptInit.setBatchNum("");
	 	        caseFinalRptInit.setSupplier("");
	 	        caseFinalRptInit.setSupplierContact("");
	 	        caseFinalRptInit.setSupplierTel("");
	 	        caseFinalRptInit.setSupplierFax("");
	 	        caseFinalRptInit.setManuf("");
	 	        caseFinalRptInit.setManufContact("");
	 	        caseFinalRptInit.setManufTel("");
	 	        caseFinalRptInit.setManufFax("");
	 	        caseFinalRptInit.setPhysicalDefectFlag(true);
	 	        caseFinalRptInit.setPackagingDefectFlag(true);
	 	        caseFinalRptInit.setAppearDiscFlag(true);
	 	        caseFinalRptInit.setForeignMatterFlag(true);
	 	        caseFinalRptInit.setOtherFlag(true);
	 	        caseFinalRptInit.setInstitutions("");
	 	        caseFinalRptInit.setProblemDetails("");
	 	        caseFinalRptInit.setEventLogs("");
	 	        caseFinalRptInit.setSummary("");
	 	        caseFinalRptInit.setGenerationDate("");
	 	        caseFinalRptInit.setEventLogDate("");
	 	
	 	      	caseFinalRptInit.getCaseNum();
	 	        caseFinalRptInit.getCaseStatus();
	 	        caseFinalRptInit.getItemCode();
	 	        caseFinalRptInit.getClassification();
	 	        caseFinalRptInit.getBatchNum();
	 	        caseFinalRptInit.getSupplier();
	 	        caseFinalRptInit.getSupplierContact();
	 	        caseFinalRptInit.getSupplierTel();
	 	        caseFinalRptInit.getSupplierFax();
	 	        caseFinalRptInit.getManuf();
	 	        caseFinalRptInit.getManufContact();
	 	        caseFinalRptInit.getManufTel();
	 	        caseFinalRptInit.getManufFax();
	 	        caseFinalRptInit.getPhysicalDefectFlag();
	 	        caseFinalRptInit.getPackagingDefectFlag();
	 	        caseFinalRptInit.getAppearDiscFlag();
	 	        caseFinalRptInit.getForeignMatterFlag();
	 	        caseFinalRptInit.getOtherFlag();
	 	        caseFinalRptInit.getInstitutions();
	 	        caseFinalRptInit.getProblemDetails();
	 	        caseFinalRptInit.getEventLogs();
	 	        caseFinalRptInit.getSummary();
	 	        caseFinalRptInit.getGenerationDate();
	 	        caseFinalRptInit.getEventLogDate();
	 	        
	 	        
	       }
		}.run();
	}

}
