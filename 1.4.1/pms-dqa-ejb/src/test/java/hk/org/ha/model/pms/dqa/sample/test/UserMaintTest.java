package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class UserMaintTest extends SeamTest {
	@Test
	public void userMaintComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //insert
	            UserInfo userInfoIn = new UserInfo();
	            Contact contactIn = new Contact();
	            
	            contactIn.setTitle(TitleType.Mr);
	            contactIn.setFirstName("TEST");
	            contactIn.setLastName("CASE");
	            contactIn.setPosition("SA");
	            contactIn.setOfficePhone("22222222");
	            contactIn.setFax("21111111");
	            contactIn.setEmail("testcase@testcase.com");
	            contactIn.setAddress1("A1");
	            contactIn.setAddress2("A2");
	            contactIn.setAddress3("A3");
	            userInfoIn.setUserCode("testc");
	            userInfoIn.setContact(contactIn);
	            
	            Contexts.getSessionContext().set("userInfoIn", userInfoIn);
	            invokeMethod("#{userInfoService.createUserInfo(userInfoIn)}");
	            
	            //add
	            UserInfo userInfoAdd = new UserInfo();
	            Contexts.getSessionContext().set("userInfo", userInfoAdd);
	            invokeMethod("#{userInfoService.addUserInfo()}");
	            
	            //select
	            invokeMethod("#{userInfoService.retrieveUserInfoByUserInfo(userInfoIn)}");
	            assert getValue("#{userInfo!=null}").equals(true);
	            
	            //update
	            contactIn.setFirstName("TEST2");
	            contactIn.setLastName("CASE2");
	            contactIn.setPosition("SA2");
	            contactIn.setOfficePhone("222222223");
	            contactIn.setFax("211111113");
	            contactIn.setEmail("testcase2@testcase.com");
	            contactIn.setAddress1("A12");
	            contactIn.setAddress2("A22");
	            contactIn.setAddress3("A32");
	            userInfoIn.setContact(contactIn);
	            
	            Contexts.getSessionContext().set("userInfoIn", userInfoIn);
	            invokeMethod("#{userInfoService.updateUserInfo(userInfoIn)}");
	            
	            invokeMethod("#{userInfoService.retrieveUserInfoByUserInfo(userInfoIn)}");
	            assert getValue("#{userInfo.contact.firstName=='TEST2'}").equals(true);
	            
	            
	            //delete
	            Contexts.getSessionContext().set("userInfoIn", userInfoIn);
	            invokeMethod("#{userInfoService.deleteUserInfo(userInfoIn)}");
	            
	            invokeMethod("#{userInfoService.retrieveUserInfoByUserInfo(userInfoIn)}");
	            assert getValue("#{userInfo==null}").equals(true);
	            
	            //retrieve userInfo List
	            invokeMethod("#{userInfoListService.retrieveUserInfoList()}");
	            List<UserInfo> userInfoListFind = (List<UserInfo>)getValue("#{userInfoList}");
	            assert userInfoListFind.size() >0;
	            
	       }
		}.run();
	}

}
