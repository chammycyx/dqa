package hk.org.ha.model.pms.dqa.sample.test;

import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleItemReportTest extends SeamTest {
	@Test
	public void testSampleItemReportComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            Collection<Long> keyList = new ArrayList<Long>();
	            
	            keyList.add(1L);
	            keyList.add(2L);
	            
	            Contexts.getSessionContext().set("sampleItemKeyList", keyList);
	            invokeMethod("#{sampleItemFrequencyRptService.retrieveSampleItemFrequencyList(sampleItemKeyList)}");
	            
	            //retrieveSampleItemFullList
	            invokeMethod("#{sampleItemFullRptService.retrieveSampleItemFullList()}");
	            List<SampleItem> sampleItemListFind = (List<SampleItem>)getValue("#{sampleItemFullReportList}");
	            assert sampleItemListFind==null;
	            
	            
			}
		}.run();
	}
	
	@Test
	public void testSampleItemFullListReportComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            invokeMethod("#{sampleItemFullRptService.retrieveSampleItemFullList()}");
				}
		}.run();
	}
}
