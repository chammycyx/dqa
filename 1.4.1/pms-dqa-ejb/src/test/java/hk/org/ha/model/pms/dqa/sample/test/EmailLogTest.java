package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;


import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EmailLogTest extends SeamTest {
	@Test
	public void emailLogComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //create
	            List<CoaFileFolder> coaFileFolderList = new ArrayList<CoaFileFolder>();
	            coaFileFolderList.add(new CoaFileFolder());
	            LetterTemplate letterTemplateInsert= new LetterTemplate();
	            letterTemplateInsert.setToList(new String[]{"david"});
	            letterTemplateInsert.setBccList(new String[]{"david"});
	            letterTemplateInsert.setCcList(new String[]{"david"});
	            letterTemplateInsert.setSubject("ABC");
	            letterTemplateInsert.setContent("XXX");
	            letterTemplateInsert.setModuleType("S");
	            letterTemplateInsert.setRecordStatus(RecordStatus.Active);
	            letterTemplateInsert.setCoaFileFolderList(coaFileFolderList);
	            
	            Contexts.getSessionContext().set("letterTemplateIn", letterTemplateInsert);
	            Contexts.getSessionContext().set("emailTypeIn", EmailType.Outgoing);
	            
	            invokeMethod("#{emailLogManager.createEmailLogByLetterTemplate(letterTemplateIn, emailTypeIn)}");
	            
	       }
		}.run();
	}

}
