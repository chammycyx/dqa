package hk.org.ha.model.pms.dqa.sample.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EnquiryDetailTest extends SeamTest {
	@Test
	public void enquiryDetailComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("01/01/2012");
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve
	            SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria = new SampleEnquiryDetailCriteria();
	            
				sampleEnquiryDetailCriteria.setItemCode("PARA01");
				sampleEnquiryDetailCriteria.setFromScheduleMonth(fromDate);
				sampleEnquiryDetailCriteria.setToScheduleMonth(toDate);
				sampleEnquiryDetailCriteria.setRecordStatus(RecordStatus.Active);
				List<ScheduleStatus> ssCollection= new ArrayList<ScheduleStatus>();
				ssCollection.add(ScheduleStatus.ScheduleGen);
				sampleEnquiryDetailCriteria.setScheduleStatusList(ssCollection);
				
	            Contexts.getSessionContext().set("sampleEnquiryDetailCriteriaIn", sampleEnquiryDetailCriteria);
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByEnquiryDetailCriteria(sampleEnquiryDetailCriteriaIn)}");
	            
	            List<SampleTestSchedule> sTSFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            assert sTSFind.size() >0;
	            
			}    
		}.run();
	}

}
