package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleType;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleTestScheduleFileTest extends SeamTest {
	@Test
	public void sampleTestScheduleFileComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //retrieve SampleTestScheduleList
	            List<SampleTestScheduleFile> sampleTestScheduleFileListFind = (List<SampleTestScheduleFile>)invokeMethod("#{sampleTestScheduleFileListService.retrieveSampleTestScheduleFileListBySmpleTestFileId(1)}");
	            assert sampleTestScheduleFileListFind.size()>0;
	            
	            //retrieveSampleTestScheduleFileForDocument
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileForDocument(1)}");
	            SampleTestScheduleFile stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileMOA}");
	            assert stsfFind!=null;
	            
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileForDocument(2)}");
	            stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileFPS}");
	            assert stsfFind!=null;
	            
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileForDocument(20)}");
	            stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileVR}");
	            assert stsfFind!=null;
	            
	            //retrieveSampleTestScheduleFileFullDetailForDocument
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileFullDetailForDocument(1)}");
	            stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileFullDetailMOA}");
	            assert stsfFind!=null;
	            
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileFullDetailForDocument(2)}");
	            stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileFullDetailFPS}");
	            assert stsfFind!=null;
	            
	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileFullDetailForDocument(20)}");
	            stsfFind = (SampleTestScheduleFile)getValue("#{sampleTestScheduleFileFullDetailVR}");
	            assert stsfFind!=null;

	       }
		}.run();
	}

}
