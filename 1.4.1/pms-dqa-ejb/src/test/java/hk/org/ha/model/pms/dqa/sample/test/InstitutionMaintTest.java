package hk.org.ha.model.pms.dqa.sample.test;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class InstitutionMaintTest extends SeamTest {
	@Test
	public void institutionMaintComponent() throws Exception{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //insert
	            Institution institutionIn = new Institution();
	            Contact contactIn = new Contact();
	            
	            contactIn.setTitle(TitleType.Mr);
	            contactIn.setFirstName("TEST");
	            contactIn.setLastName("CASE");
	            contactIn.setPosition("SA");
	            contactIn.setOfficePhone("22222222");
	            contactIn.setFax("21111111");
	            contactIn.setEmail("testcase@testcase.com");
	            contactIn.setAddress1("A1");
	            contactIn.setAddress2("A2");
	            contactIn.setAddress3("A3");
	            institutionIn.setInstitutionCode("XXX");
	            institutionIn.setInstitutionName("XXX HOSP");
	            institutionIn.setGopcFlag("N");
	            institutionIn.setInstitutionCluster("YYY");
	            institutionIn.setPcuFlag(YesNoFlag.Yes);
	            institutionIn.setPcuCode("ZZZ");
	            institutionIn.setContact(contactIn);
	            
	            Contexts.getSessionContext().set("institutionIn", institutionIn);
	            invokeMethod("#{institutionService.createInstitution(institutionIn)}");
	            
	            
	            //select
	            invokeMethod("#{institutionService.retrieveInstitutionByInstitution(institutionIn)}");
	            assert getValue("#{institution!=null}").equals(true);
	            
	            //update
	            contactIn.setFirstName("TEST2");
	            contactIn.setLastName("CASE2");
	            contactIn.setPosition("SA2");
	            contactIn.setOfficePhone("222222223");
	            contactIn.setFax("211111113");
	            contactIn.setEmail("testcase2@testcase.com");
	            contactIn.setAddress1("A12");
	            contactIn.setAddress2("A22");
	            contactIn.setAddress3("A32");
	            institutionIn.setInstitutionName("XXX HOSP2");
	            institutionIn.setGopcFlag("Y");
	            institutionIn.setInstitutionCluster("YY2");
	            institutionIn.setPcuFlag(YesNoFlag.No);
	            institutionIn.setPcuCode("ZZZ");
	            institutionIn.setContact(contactIn);
	            
	            Contexts.getSessionContext().set("institutionIn", institutionIn);
	            invokeMethod("#{institutionService.updateInstitution(institutionIn)}");
	            
	            invokeMethod("#{institutionService.retrieveInstitutionByInstitution(institutionIn)}");
	            assert getValue("#{institution.contact.firstName=='TEST2'}").equals(true);
	            
	            
	            //delete
	            Contexts.getSessionContext().set("institutionIn", institutionIn);
	            invokeMethod("#{institutionService.deleteInstitution(institutionIn)}");
	            
	            invokeMethod("#{institutionService.retrieveInstitutionByInstitution(institutionIn)}");
	            assert getValue("#{institution.recordStatus}").equals(RecordStatus.Deleted.getDataValue());
	            
	            //retrieve institutionList
	            Contexts.getSessionContext().set("YesFlag", YesNoFlag.Yes);
	            
	            invokeMethod("#{institutionListService.retreiveInstitutionListByPcuFlag(YesFlag)}");
	            List<Institution> institutionListFind = (List<Institution>)getValue("#{institutionList}");
	            assert institutionListFind.size()>0;
	            
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            
	            invokeMethod("#{institutionListService.retrieveInstitutionListByPcuFlagRecordStatus(YesFlag, recordStatusIn)}");
	            institutionListFind = (List<Institution>)getValue("#{institutionListByRecordStatus}");
	            assert institutionListFind.size()>0;
	            
	            //add institution
	            Contexts.getSessionContext().set("institutionIn", new Institution());
	            invokeMethod("#{institutionService.addInstitution()}");
	            
	            //retrievePcuListForPopup
	            invokeMethod("#{pcuListForPopupService.retrievePcuListForPopup()}");
	            
	            //retrieveInstitutionListByPcu
	            invokeMethod("#{institutionListByPcuService.retrieveInstitutionListByPcu('PYN')}");
	           
	            //retrievePcuList
	            invokeMethod("#{pcuListService.retrievePcuList()}");
	            
	            //retrieveInstitutionListByPcuForPopup
	            invokeMethod("#{institutionListByPcuForPopupService.retrieveInstitutionListByPcuForPopup('PYN')}");
	            
	            //retrieveFullInstitutionList
	            invokeMethod("#{fullInstitutionListService.retrieveFullInstitutionList()}");
	       }
		}.run();
	}

}
