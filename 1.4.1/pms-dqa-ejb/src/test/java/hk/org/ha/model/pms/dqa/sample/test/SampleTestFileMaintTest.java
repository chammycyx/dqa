package hk.org.ha.model.pms.dqa.sample.test;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestFileCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleTestFileMaintTest extends SeamTest {
	@Test
	public void testSampleTestFileMainComponet() throws Exception{
		new ComponentTest() {
//			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
//	            Long sampleTestFlieId = 2L;
//	            invokeMethod("#{sampleTestScheduleFileService.retrieveSampleTestScheduleFileList('"+sampleTestFlieId+"')}");
	            
	            Supplier supplier = new Supplier();
	            invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('3M')}");
	            supplier = (Supplier)getValue("#{supplier}");
	            
	            
	            invokeMethod("#{sampleTestFileService.addSampleTestFile()}");
	            
	            Company manuf = new Company();
	            manuf.setCompanyCode("3M");
	            
	            Company pharmCompany = new Company();
	            pharmCompany.setCompanyCode("3M");
	            
	            SampleTestFile stf = new SampleTestFile();
	            FileItem fItem = new FileItem();
	            fItem.setModuleType(ModuleType.SampleTest);
	            fItem.setFilePath("sampleTest/template1");
	            fItem.setFileName("sampleTest");
	            fItem.setFileType("pdf");
	            
	            stf.setFileItem(fItem);
	            stf.setManufacturer(manuf);
	            stf.setPharmCompany(pharmCompany);
	            stf.setItemCode("PARA01");
	            stf.setSupplier(supplier);
	            stf.setFileCat(SampleTestFileCat.MethodofAssay);
	            
	            Contexts.getSessionContext().set("sampleTestFile", stf);
	            invokeMethod("#{sampleTestFileService.createSampleTestFile(sampleTestFile)}");
	            
	            
	            fItem.setFileName("sampleTest 2");
	            Contexts.getSessionContext().set("sampleTestFileIn", stf);
	            invokeMethod("#{sampleTestFileService.updateSampleTestFile(sampleTestFileIn)}");
	            
	            Contexts.getSessionContext().set("sampleTestFileIn", stf);
	            invokeMethod("#{sampleTestFileService.deleteSampleTestFile(sampleTestFileIn)}");
	            
	            
	            //retrieveSampleTestFileList
	            SampleTestFileCriteria sampleTestFileCriteriaIn = new SampleTestFileCriteria();
	            sampleTestFileCriteriaIn.setSupplierCode("ABBO");
	            sampleTestFileCriteriaIn.setItemCode("PARA01");
	            sampleTestFileCriteriaIn.setManufCode("3M");
	            sampleTestFileCriteriaIn.setPharmCode("3M");
	            sampleTestFileCriteriaIn.setDocType(SampleTestFileCat.MethodofAssay);
	            
	            Contexts.getSessionContext().set("sampleTestFileCriteriaIn", sampleTestFileCriteriaIn);
	            invokeMethod("#{sampleTestFileListService.retrieveSampleTestFileList(sampleTestFileCriteriaIn)}");
	            
	            List<SampleTestFile> sampleTestFileListFind = (List<SampleTestFile>)getValue("#{sampleTestFileList}");
	            assert sampleTestFileListFind.size()==0;
	            

	            //retrieveSampleTestFileListForSchedConfirm
	            invokeMethod("#{sampleTestFileListService.retrieveSampleTestFileListForSchedConfirm('PARA01','ABBO','3M','3M')}");
	            sampleTestFileListFind = (List<SampleTestFile>)getValue("#{sampleTestFileList}");
	            assert sampleTestFileListFind==null;
	            
	            	 
	            //retrieveSampleTestFileListBySampleTestFileCat
	            
	            Contexts.getSessionContext().set("sampleTestFileCatIn", SampleTestFileCat.MethodofAssay);
	            
	            invokeMethod("#{sampleTestFileListService.retrieveSampleTestFileListBySampleTestFileCat(sampleTestFileCatIn)}");
	            sampleTestFileListFind = (List<SampleTestFile>)getValue("#{sampleTestFileList}");
	            assert sampleTestFileListFind.size()>0;
	            	 
	            	 
	            //retrieveSampleTestFileListBySampleTestFileCatContract
	            Contract contractIn = new Contract();
	            contractIn.setSupplier(supplier);
	            contractIn.setPharmCompany(pharmCompany);
	            contractIn.setManufacturer(manuf);
	  	 
	            Contexts.getSessionContext().set("sampleTestFileCatIn", SampleTestFileCat.MethodofAssay);
	            Contexts.getSessionContext().set("contractIn", contractIn);
	            
	            invokeMethod("#{sampleTestFileListService.retrieveSampleTestFileListBySampleTestFileCatContract(sampleTestFileCatIn, contractIn)}");
	            sampleTestFileListFind = (List<SampleTestFile>)getValue("#{sampleTestFileList}");
	            assert sampleTestFileListFind.size()==0;


			}
		}.run();
	}
}
