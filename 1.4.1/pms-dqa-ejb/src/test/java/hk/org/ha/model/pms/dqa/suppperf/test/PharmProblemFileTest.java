package hk.org.ha.model.pms.dqa.suppperf.test;

import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PharmProblemFileTest extends SeamTest {
	@Test
	public void PharmProblemFileComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//Login
	            invokeMethod("#{identity.login}");

				QaProblem qaProblemIn = new QaProblem();
				qaProblemIn.setCaseNum("CASE1");
				
				
	            // retrieve by qaProblem
	            Contexts.getSessionContext().set("qaProblemIn", qaProblemIn);
	            invokeMethod("#{pharmProblemFileListService.retrievePharmProblemFileListByQaProblem(qaProblemIn)}");
	            		
	            assert getValue("#{pharmProblemFileList==null}").equals(true);
	            
	            	            
	       }
		}.run();
	}

}
