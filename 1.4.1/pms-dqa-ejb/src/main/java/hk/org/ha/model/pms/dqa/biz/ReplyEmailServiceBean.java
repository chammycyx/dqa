package hk.org.ha.model.pms.dqa.biz;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaInEmailAttManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaInEmailManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaItemManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaOutstandBatchManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaReplyEmailManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchManagerBean.CoaBatchResult;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.coa.AttRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ReplyEmailTo;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;
import hk.org.ha.model.pms.dqa.udt.ModuleType;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;
import org.joda.time.format.DateTimeFormat;

@Stateless
@Name("replyEmailService")
public class ReplyEmailServiceBean implements ReplyEmailServiceLocal {

	@Logger
	private Log logger;

	@In(create = true)
	private CoaInEmailManagerLocal coaInEmailManager;

	@In(create = true)
	private CoaInEmailAttManagerLocal coaInEmailAttManager;

	@In(create = true)
	private CoaReplyEmailManagerLocal coaReplyEmailManager;

	@In(create = true)
	private EmailLogManagerLocal emailLogManager; 

	@In(create = true)
	private ContractManagerLocal contractManager;

	@In(create = true)
	private SupplierManagerLocal supplierManager;

	@In(create = true)
	private LetterTemplateManagerLocal letterTemplateManager;	

	@In(create = true)
	private CoaOutstandBatchManagerLocal coaOutstandBatchManager;

	@In(create = true)
	private CoaItemManagerLocal coaItemManager;

	@In(create = true)
	private CoaBatchManagerLocal coaBatchManager;

	@In(create = true)
	private CoaFileFolderManagerLocal coaFileFolderManager;

	@In
	private Interpolator interpolator;	

	private static final String BR = "<br>";
	private static final String UNCHECKED = "unchecked";
	private static final String D_PARAM = "%d,"; 
	
	private EmailLog emailLog;
	private String fromEmail;
	private String supportEmail;
	private String problemLines;
	private StringBuffer problemFiles;
	private String toSupportProblemLines;
	
	private String emailSubSupplierCode;
	private ValidateResultFlag validateSubjectResult;	
	private Marshaller marshaller = null;
	private boolean isReply;
	private boolean toSupport;
	private Map<String , byte[]> coaItemNotExistAttHashMap;
	
	private static final String DATE_PATTERN = "dd/MM/yyyy";
	
	@PostConstruct
	public void init() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(EaiMessage.class);
			marshaller = jaxbContext.createMarshaller();
		} catch (JAXBException e) {
			logger.error("unable to create jaxb marshaller! #0", e);
		}
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setSupportEmail(String supportEmail) {
		this.supportEmail = supportEmail;
	}

	private String constructReplyContent() {
		logger.debug("constructReplyContent #0 #1", problemFiles, problemLines);
		LetterTemplate lt=null;		
		lt = letterTemplateManager.retrieveLetterTemplateByCode("RCER");
		if (StringUtils.isBlank(problemLines)){
			problemLines = "";
		}
		
		return interpolator.interpolate( lt.getContent(), new Object[]{ problemFiles.toString(), problemLines } );				
	}
	
	private String constructSupportMsgContent(String letterCode,  String pLinesIn ) {
		logger.debug("constructReplyMsgContent #0", pLinesIn);
		LetterTemplate lt = letterTemplateManager.retrieveLetterTemplateByCode(letterCode);
		String pLines;
		if (StringUtils.isBlank(pLinesIn)){
			pLines = "";
		}else{
			pLines = pLinesIn;
		} 
		
		return interpolator.interpolate( lt.getContent(), new Object[]{ pLines } );
		
	}
	
	private List<String> retrieveOCPEmailList(String subjectStr){
		List<String> ocpEmailList = new ArrayList<String>();
		String [] subjectSplit = subjectStr.split(" COA ");		
		if( subjectSplit.length == 2 ) {		
			Supplier supplier = supplierManager.retrieveSupplierForEmail( subjectSplit[0].trim() );
			
			if (supplier!=null && supplier.getSupplierContactList()!=null){
				Contact ct;
				for (SupplierContact sc: supplier.getSupplierContactList()){
					ct = sc.getContact();
					if (YesNoFlag.Yes.getDataValue().equals(ct.getOptContactPoint())){
						ocpEmailList.add(ct.getEmail());
					}
				}
			}
		}
		return ocpEmailList;
	}

	private void constructReplyFormat(final EaiMessage eaiMessage, String content) {
		logger.debug("constructReplyFormat");
		String tos="";
		String ccs=( eaiMessage.getCcs() != null )?"":null;
		String orgEmailFrom = eaiMessage.getFrom();
		String orgEmailSubject = eaiMessage.getSubject();
		String orgEmailContent = eaiMessage.getContent();
		int len;
		
		eaiMessage.setCcs(new String[]{""});
		eaiMessage.setBccs(new String[]{""});
		
		List<String> ocpEmailList = retrieveOCPEmailList(orgEmailSubject);
		
		len = eaiMessage.getTos().length;
		StringBuffer tosBuf = new StringBuffer();

		for( int i=0; i<len; i++ ) {
			tosBuf.append((eaiMessage.getTos())[i]);
			tosBuf.append("; ");
		}
		
		tos = tosBuf.toString();		
		tos = tos.substring(0, tos.lastIndexOf("; "));

		if(ccs != null && ccs.length() != 0) {
			len = eaiMessage.getTos().length;
			StringBuffer ccsBuf = new StringBuffer();
			for( int i=0; i<len; i++ ) {
				ccsBuf.append((eaiMessage.getTos())[i]);
				ccsBuf.append("; ");
			}
			ccs +=ccsBuf.toString();
			ccs = ccs.substring(0, ccs.lastIndexOf("; "));
		} else {
			ccs = "";
		}

		StringBuffer sb = new StringBuffer();
		sb.append(content)
		.append(BR)
		.append("<hr/>")
		.append("<b>From: </b>").append(eaiMessage.getFrom()).append(BR)
		.append("<b>Sent: </b>").append(eaiMessage.getSentDate()).append(BR)
		.append("<b>To: </b>").append(tos).append(BR);
		if(ccs != null && ccs.length() != 0) {
			sb.append("<b>Cc: </b>").append(ccs).append(BR);
		}
		sb.append("<b>Subject: </b>")
		.append( ( eaiMessage.getSubject() == null || "".equals( eaiMessage.getSubject().trim() ) )?"No Subject":eaiMessage.getSubject())
		.append(BR)
		.append( orgEmailContent );				

		if(validateSubjectResult == ValidateResultFlag.Fail) {		
			eaiMessage.setTos( new String[]{ supportEmail } );
			
			logger.debug("invalid subject : #0", supportEmail);
		} else {

			ocpEmailList.addAll(Arrays.asList(orgEmailFrom));

			String[] emailArray = (String[]) ocpEmailList.toArray(new String[ocpEmailList.size()]);
			eaiMessage.setTos( emailArray  );
			eaiMessage.setBccs( new String[] {supportEmail} );
			
			logger.debug("invalid attach : #0", supportEmail);
		}
		eaiMessage.setFrom( fromEmail );
		eaiMessage.setSubject( "RE:" + orgEmailSubject );
		eaiMessage.setContent( sb.toString() );
		eaiMessage.setMimeType("text/html; charset=UTF-8");
	}

	private EaiMessage copyEaiMessage(EaiMessage eaiMsg){
		
		EaiMessage outMsg = new EaiMessage();
		
		outMsg.setFrom(eaiMsg.getFrom());
		outMsg.setSubject(eaiMsg.getSubject());
		outMsg.setContent(eaiMsg.getContent());
		outMsg.setMimeType("text/html; charset=UTF-8");
		outMsg.setTos(eaiMsg.getTos());
		outMsg.setReceivedDate(eaiMsg.getReceivedDate());
		outMsg.setSentDate(new Date());
		outMsg.setType(eaiMsg.getType());
		return outMsg;
		
	}
	
	@SuppressWarnings(UNCHECKED)
	private MessageCreator getCoaItemNotExistReplyMsg( CoaInEmail coaInEmail, final EaiMessage eaiMsg, final Map attHashMap ) throws JAXBException {
		logger.debug("getCoaItemNotExistReplyMsg");
		
			final StringWriter writer = new StringWriter();
			if( toSupportProblemLines != null && !"".equals( toSupportProblemLines ) ) {				
				toSupportProblemLines = "<p>COA Exception Report problem:<br />-Lines " + toSupportProblemLines + "<br /></p>";
			}
			String content = constructSupportMsgContent("RTQA", toSupportProblemLines); 
			
			String tos="";
			String ccs=( eaiMsg.getCcs() != null )?"":null;
			String orgEmailSubject = eaiMsg.getSubject();
			String orgEmailContent = eaiMsg.getContent();
			int len;
		
			len = eaiMsg.getTos().length;
			StringBuffer tosBuf = new StringBuffer();

			for( int i=0; i<len; i++ ) {
				tosBuf.append((eaiMsg.getTos())[i]);
				tosBuf.append("; ");
			}
			
			tos = tosBuf.toString();		
			tos = tos.substring(0, tos.lastIndexOf("; "));

			if(ccs != null && ccs.length() != 0) {
				len = eaiMsg.getTos().length;
				StringBuffer ccsBuf = new StringBuffer();
				for( int i=0; i<len; i++ ) {
					ccsBuf.append((eaiMsg.getTos())[i]);
					ccsBuf.append("; ");
				}
				ccs +=ccsBuf.toString();
				ccs = ccs.substring(0, ccs.lastIndexOf("; "));
			} else {
				ccs = "";
			}

			StringBuffer sb = new StringBuffer();
			sb.append(content)
			.append(BR)
			.append("<hr/>")
			.append("<b>From: </b>").append(eaiMsg.getFrom()).append(BR)
			.append("<b>Sent: </b>").append(eaiMsg.getSentDate()).append(BR)
			.append("<b>To: </b>").append(tos).append(BR);
			if(ccs != null && ccs.length() != 0) {
				sb.append("<b>Cc: </b>").append(ccs).append(BR);
			}
			sb.append("<b>Subject: </b>")
			.append( ( eaiMsg.getSubject() == null || "".equals( eaiMsg.getSubject().trim() ) )?"No Subject":eaiMsg.getSubject())
			.append(BR)
			.append( orgEmailContent );				

			eaiMsg.setTos( new String[]{ supportEmail } );
			logger.debug("coa item not exist , send to : #0", supportEmail);
			
			eaiMsg.setFrom( fromEmail );
			eaiMsg.setSubject( "RE:" + orgEmailSubject );
			eaiMsg.setContent( sb.toString() );

			emailLog = emailLogManager.createEmailLog(eaiMsg, attHashMap, EmailType.Outgoing);
			coaReplyEmailManager.createCoaReplyEmail( coaInEmail, emailLog , ReplyEmailTo.CoaSupportTeam ); 
						
			marshaller.marshal(eaiMsg, writer);
			logger.debug("Mail for coa item not exist ,send out: #0", writer.toString());
			
			return new InnerMessageCreator(eaiMsg.getType() ,writer.toString(), attHashMap);	
	}
	
	private ValidateResultFlag validateSubject(String subject) {
		logger.debug("validateSubject #0", subject);
		String [] subjectSplit = subject.split(" COA ");		
		if( subjectSplit.length != 2 ) { 
			logger.info("Invalid email subject #0", subject);			
			return ValidateResultFlag.Fail;			
		}		
		//validate supplier code		
		emailSubSupplierCode = subjectSplit[0].trim();			
		Supplier supplier = supplierManager.retrieveSupplierForEmail( emailSubSupplierCode );
		if( supplier == null ) { 
			logger.info("Supplier does not exist #0", emailSubSupplierCode);
			return ValidateResultFlag.Fail; 
		}
		//validate sent date 
		//DQA-34		
		subjectSplit[1] = subjectSplit[1].trim();		
		try {
			DateTimeFormat.forPattern(DATE_PATTERN).parseDateTime(subjectSplit[1]);
		} catch (IllegalArgumentException e) {
			logger.info("Invalid send date in the email subject #0", subject);
			return ValidateResultFlag.Fail;
		}	

		return ValidateResultFlag.Pass;
	}

	@SuppressWarnings(UNCHECKED)
	private boolean verifyPdfFile( CoaInEmailAtt emailAtt, Map.Entry attachment  ) {
		CoaInEmailAtt att = emailAtt;
		String fileName =  (String)attachment.getKey();	
		logger.debug("verifyPdfFile , validateFileName #0", fileName);		
		String [] nameComponents = fileName.substring(0, fileName.lastIndexOf('.')).split("_");
		if( nameComponents.length != 4 ) { 
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.InvalidBatchCoaFileName);
			return false; 
		}
		String batchNum = nameComponents[0];
		String itemCode = nameComponents[1];
		String contractNum = nameComponents[2];
		String supplierCode = nameComponents[3];

		//check batch no & the existence of the supplier
		Supplier supplier = supplierManager.retrieveSupplierForEmail( supplierCode );
		
		if(StringUtils.isBlank(batchNum) || batchNum.length() > 20 || supplier == null || !StringUtils.equalsIgnoreCase(supplierCode, emailSubSupplierCode.trim())) { 
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.InvalidBatchCoaFileName);
			return false; 
		}
		
		//check the existence of the contract - support
		Contract contract = contractManager.retrieveContractForCoaEmail( contractNum, itemCode, supplierCode );
		if( contract == null ) {
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.CoaItemNotExist);
			coaItemNotExistAttHashMap.put(fileName, (byte[])attachment.getValue());
			return false;
		} else {
			//check expiration of the contract/ bypass Direct Purchase contract - support 
			if( contract.getContractNum() != null && contractManager.isExpiredForCoaEmail( contract ) ) {
				coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.ContractOfCoaItemExpired);
				coaItemNotExistAttHashMap.put(fileName, (byte[])attachment.getValue());
				return false;
			}
		}
				
		//Create CoaBatch, CoaFileFolder and FileItem
		CoaItem coaItem = coaItemManager.retrieveCoaItemByContract( contract );
		if(coaItem == null) {
			logger.debug("CoaItem is null");
			return false;
		}

		CoaBatch coaBatchIn = new CoaBatch();
		coaBatchIn.setBatchNum( batchNum );
		coaBatchIn.setCoaItem( coaItem );
		coaBatchIn.setCoaBatchVerCount(1);
		CoaBatchResult coaBatchResult = coaBatchManager.createCoaBatchForEmail(coaBatchIn);

		if( !coaBatchResult.isDuplicate() ) {
			FileItem file = new FileItem();

			String fileNameStr = fileName.substring( 0, (fileName.lastIndexOf('.')==-1?(fileName.length()-1):fileName.lastIndexOf('.')) );	 
			file.setFileName( fileNameStr );
			file.setFileType( fileName.substring( fileName.lastIndexOf('.')+1 ) );			
			file.setModuleType( ModuleType.COA );

			CoaFileFolder folder = new CoaFileFolder();
			folder.setCoaBatch( coaBatchResult.getCoaBatch() );
			folder.setFileCat( FileCategory.Batch );
			folder.setFileItem( file );

			String physicalFilePath = coaFileFolderManager.doUploadForEmail( (byte[])attachment.getValue(), fileName, folder );
			coaFileFolderManager.createCoaFileFolderForEmail( folder );
			if( physicalFilePath != null ) {
				folder.getFileItem().setFilePath( physicalFilePath );
				folder = coaFileFolderManager.updateCoaFileFolderForEmail( folder );				
				coaBatchManager.updateCoaBatchForEmailLog( coaBatchResult.getCoaBatch() );
				CoaOutstandBatch coaOutstandBatch = coaOutstandBatchManager.isOutstandBatchExist(contract, batchNum);
				if( coaOutstandBatch != null ) {
					coaOutstandBatchManager.updateCoaOutstandBatch( coaOutstandBatch, folder, new Date() );	//changed
				}
				att = coaInEmailAttManager.updateCoaInEmailAtt( att, folder );
				coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.Pass);
				return true;				
			} else {
				logger.info("Fail in upload attachment #0", fileName);
				coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.UploadFileFailure);
				return false;
			}
		} else {
			logger.info("Duplicate CoaBatch #0 - Contract #1 #2", batchNum, contract.getContractNum(), contract.getItemCode());
			coaInEmailAttManager.updateCoaInEmailAttResult(att,ValidateResultFlag.Pass, AttRemarkType.SameBatchCOAExist);			
			return true;
		}
	}

	private void verifyAttachedExcel( CoaInEmailAtt att, ByteArrayInputStream bais ) {
		logger.debug("validateAttachedExcel");
		StringBuffer strBuf = new StringBuffer();
		StringBuffer supportStrBuf = new StringBuffer();
		String itemCode, contractNum, batchNum, itemDesc;
		Contract contract;
		AttRemarkType remarkType = null;
		try {
			
			Workbook workbook = Workbook.getWorkbook( bais );
			Sheet sheet = workbook.getSheet( 0 );	
			logger.debug("sheet.getRows() #0", sheet.getRows());
			for(int i=1; i < sheet.getRows(); i++) {
				itemCode = sheet.getCell(0, i).getContents().trim();
				contractNum = sheet.getCell(2, i).getContents().trim();
				batchNum = sheet.getCell(3, i).getContents().trim();					
				itemDesc = sheet.getCell(1, i).getContents().trim();
				
				logger.debug("itemCode:#0, contractNum:#1, batchNum:#2, supplierCode:#3", itemCode, contractNum, batchNum, emailSubSupplierCode);
				
				if (StringUtils.isBlank(itemCode) 
						&& StringUtils.isBlank(contractNum) 
						&& StringUtils.isBlank(batchNum) 
						&& StringUtils.isBlank(itemDesc) ) {
					continue;
				}
				
				contract = contractManager.retrieveContractForCoaEmail( contractNum, itemCode, emailSubSupplierCode );											
				if( contract == null ) { // to support								
					toSupport = true;
					supportStrBuf.append( String.format(D_PARAM, i+1) );
					remarkType = AttRemarkType.CoaItemNotExist;									
					continue;
				}
				
				if( contractManager.isExpiredForCoaEmail( contract ) ) { // to support									
					toSupport = true;
					supportStrBuf.append( String.format(D_PARAM, i+1) );
					remarkType = AttRemarkType.ContractOfCoaItemExpired;					
					continue;
				}	
				
				if( batchNum == null || batchNum.length() == 0 ) {
					isReply = true;
					strBuf.append( String.format(D_PARAM, i+1) );
					remarkType = AttRemarkType.InvalidFileContent;
					continue;
				}
				
				CoaOutstandBatch coaOutstandBatch = coaOutstandBatchManager.isOutstandBatchExist(contract, batchNum);
				if( coaOutstandBatch != null ) {
					logger.info("outStanding batch exist #0 #1", contract.getContractNum(), batchNum);
					continue;
				}
				coaOutstandBatchManager.createCoaOutstandBatch(contract, att, batchNum);
			}				
			workbook.close();
			
			if( remarkType != null ) {				
				coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, remarkType);
				if (toSupport){
					toSupportProblemLines = supportStrBuf.substring(0, supportStrBuf.length()-1);
				}
				if (isReply){
					problemLines = strBuf.substring(0, strBuf.length()-1);
				}
			} else {
				coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.Pass);
			}
			
		} catch (IOException e1) {
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.CoaExceptionReportFileCorrupt);
			isReply = true;
			problemFiles.append("-File is corrupt");
			logger.warn("error", e1);
		} catch (BiffException e2) {
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.CoaExceptionReportFileCorrupt);
			isReply = true;
			problemFiles.append("-File is corrupt");
			logger.warn("error", e2);			
		}
	}	

	@SuppressWarnings(UNCHECKED)
	private void verifyAttachment( CoaInEmail coaInEmail, Map.Entry attachment ){
		logger.debug("verifyAttachment");
		boolean isValidPdfFile = true;
		String attachmentName =  ((String)attachment.getKey()).trim();		
		String lowerCaseName = attachmentName.toLowerCase(Locale.ENGLISH);		
		//Create passed attachment first
		CoaInEmailAtt att = coaInEmailAttManager.createCoaInEmailAtt(coaInEmail, attachmentName);		
		if(lowerCaseName.endsWith(".xls") ) {

			ByteArrayInputStream bais = new ByteArrayInputStream( (byte[])attachment.getValue() );//changed
			verifyAttachedExcel( att, bais );	
			if (toSupport){
				logger.info("Send validate result to support #0", attachmentName);
				coaItemNotExistAttHashMap.put(attachmentName, (byte[])attachment.getValue());
			}
		} else if( lowerCaseName.endsWith(".pdf") ) {
			isValidPdfFile = verifyPdfFile( att, attachment );
		} else {
			isValidPdfFile = false;
			coaInEmailAttManager.updateCoaInEmailAttVerifyResult(att, AttRemarkType.InvalidBatchCoaFileName);			
		}

		if( !isValidPdfFile ){
			logger.info("Validate attachment failure - #0", attachmentName);
			problemFiles.append( "-" ).append( attachmentName ).append("<br />");			
			isReply = true;
		}
	}

	private CoaInEmail createCoaInEmail( ValidateResultFlag validateSubjectResult ){//changed
		return coaInEmailManager.createCoaInEmail( validateSubjectResult, emailLog );
	}

	@SuppressWarnings({ UNCHECKED })
	private MessageCreator constructReplyMessage( CoaInEmail coaInEmail, final EaiMessage eaiMessage, final Map attHashMap ) throws JAXBException {
		logger.debug("constructReplyMessage");
		
		String content;
		final StringWriter writer = new StringWriter();
		if( validateSubjectResult == ValidateResultFlag.Fail ) {
			content = "<p>DQA has encountered problems on the subject line, please follow up the issue.</p>";
		} else {

			if( problemLines != null && !"".equals( problemLines ) ) {				
				problemLines = "<p>COA Exception Report problem:<br />-Lines " + problemLines + "<br /></p>";
			}

			if( problemFiles != null && !"".equals( problemFiles.toString() ) ) {				
				problemFiles.insert( 0, "<p>PDF filename problem:<br />" );
				problemFiles.append( "</p>" );			
			}
			content = constructReplyContent();
			logger.debug("content: #0", content);
		}
		constructReplyFormat( eaiMessage, content );
		emailLog = emailLogManager.createEmailLog(eaiMessage, attHashMap, EmailType.Outgoing);
		coaReplyEmailManager.createCoaReplyEmail( coaInEmail, emailLog, null );
					
		marshaller.marshal(eaiMessage, writer);
		logger.debug("Mail send out: #0", writer.toString());
		
		return new InnerMessageCreator(eaiMessage.getType() ,writer.toString(), attHashMap);
	}
	
	@SuppressWarnings(UNCHECKED)
	public static class InnerMessageCreator implements MessageCreator {
		private String type;
		private String xml;
		private Map attHashMap;
			
		public InnerMessageCreator(String type, String xml, Map attHashMap) {
			this.type = type;
			this.xml = xml;
			this.attHashMap = attHashMap;
		}
		
		public Message createMessage(Session session)
		throws JMSException {
			MapMessage out = session.createMapMessage();

			out.setString(type, xml);

			// copy the attachments to out message
			Iterator it = attHashMap.entrySet().iterator();
			while( it.hasNext() ) {
				Map.Entry entry = (Map.Entry)it.next();
				out.setBytes( (String)entry.getKey(), (byte[])entry.getValue() );
			}
			return out;
		}
	};
	
	
	@SuppressWarnings(UNCHECKED)
	public List<MessageCreator> replyMessage( final EaiMessage eaiMessage, final Map attHashMap ) throws JMSException, JAXBException {
		logger.debug("replyMessage");

		List<MessageCreator> msgCreatorList = new ArrayList<MessageCreator>();
		
		isReply = false;
		toSupport = false;
		problemFiles = new StringBuffer();
		problemLines = null;			
		coaItemNotExistAttHashMap = new HashMap();
		
		validateSubjectResult = validateSubject( eaiMessage.getSubject() );//changed

		emailLog = emailLogManager.createEmailLog(eaiMessage, attHashMap, EmailType.Incoming);//changed				
		CoaInEmail coaInEmail = createCoaInEmail( validateSubjectResult );//changed

		
		List<Map.Entry> sortedAttList = new ArrayList<Map.Entry>();
		List<Map.Entry> sortedTailList = new ArrayList<Map.Entry>();
		EaiMessage orgEaiMsg = copyEaiMessage(eaiMessage);
		
		if( validateSubjectResult == ValidateResultFlag.Pass ) {
			// validate excel of COA Exception Report
			// validate pdf file name								
			Iterator it = attHashMap.entrySet().iterator();
			while ( it.hasNext() ) {
				
				Map.Entry attachment = (Map.Entry)it.next();
				String attachmentName =  (String)attachment.getKey();				
				if ("email".equals(attachmentName)) {
					continue;
				}
				
				if (attachmentName.toLowerCase(Locale.ENGLISH).endsWith(".xls")){
					sortedAttList.add(attachment);
				}else {
					sortedTailList.add(attachment);
				}
			}	
			if (!sortedTailList.isEmpty()){
				sortedAttList.addAll(sortedTailList);
			}
			
			for (Map.Entry att: sortedAttList){
				String attachmentName  = (String)att.getKey();	
				logger.debug("attachmentName: #0", attachmentName);
				verifyAttachment( coaInEmail, att );//changed
			}
			
			if (isReply || toSupport){ 
				logger.info("attachements have error");
				if ( logger.isDebugEnabled() ) {
					logger.debug("EAI message #0", eaiMessage.toString());
				}
				coaInEmail.setResultFlag(ValidateResultFlag.Fail);
				coaInEmailManager.updateCoaInEmail(coaInEmail);//changed
			}
		} else {
			isReply = true;
		}

		if (isReply) {
			msgCreatorList.add(constructReplyMessage(coaInEmail, eaiMessage, attHashMap));//changed
		}
		if (toSupport || !coaItemNotExistAttHashMap.isEmpty()){
			msgCreatorList.add(getCoaItemNotExistReplyMsg(coaInEmail, orgEaiMsg, coaItemNotExistAttHashMap));
		}
		
		if (!msgCreatorList.isEmpty()){	
			return msgCreatorList;		
		} else {
			logger.info("No reply to email : #0", eaiMessage.getSubject());
			return null;
		}
	}

	//For Seam test case only
	@SuppressWarnings(UNCHECKED)
	public Boolean testReplyMessage(String fileName) {	
		logger.info("testReplyMessage #0", fileName);
		final HashMap attHashMap = new HashMap();
		EaiMessage eaiMessage = new EaiMessage();
		eaiMessage.setTos( new String[] { fromEmail } );
		eaiMessage.setFrom( "virtual_kfs@hotmail.com" );
		eaiMessage.setCcs( new String[] {"dqa_support@hotmail.com"} );
		eaiMessage.setBccs( new String[] {"dqa_in@hotmail.com"} );
		eaiMessage.setSubject( "DKSH COA 31/12/2010" );
		eaiMessage.setContent( "Testing content" );
		eaiMessage.setSentDate( new Date() );
		eaiMessage.setMimeType( "text/html; charset=us-ascii" );
		
		FileInputStream fis=null;
		try {	
			fis = new FileInputStream("src/test/resources/testdata/"+fileName);
			attHashMap.put(fileName, IOUtils.toByteArray( fis ));	
			
			replyMessage(eaiMessage, attHashMap);
			return true;
		} catch (IOException e1) {
			logger.error("Fail to read file.", e1);
		} catch (JMSException e2) {
			logger.error("error ", e2);
		} catch (JAXBException e3) {
			logger.error("error ", e3);
		}
		return false;
	}

}
