package hk.org.ha.model.pms.dqa.vo.sample;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestInventoryStockOnhand {
	

	private String itemCode;
	private String itemDesc;
	private Integer onHandQty;
	private String baseUnit;
	private String specialCat;
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Integer getOnHandQty() {
		return onHandQty;
	}
	
	public void setOnHandQty(Integer onHandQty) {
		this.onHandQty = onHandQty;
	}
	
	public String getSpecialCat() {
		return specialCat;
	}

	public void setSpecialCat(String specialCat) {
		this.specialCat = specialCat;
	}

}
