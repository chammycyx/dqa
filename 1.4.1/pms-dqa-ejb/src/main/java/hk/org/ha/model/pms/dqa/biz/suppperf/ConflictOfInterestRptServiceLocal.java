package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.vo.suppperf.ConflictOfInterestRpt;

import javax.ejb.Local;

@Local
public interface ConflictOfInterestRptServiceLocal {
	void createConflictOfInterestRpt(ConflictOfInterestRpt conflictOfInterestRptIn);
	void generateConflictOfInterestRpt();
	void destroy();
}
