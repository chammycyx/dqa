package hk.org.ha.model.pms.biz.security;

import javax.ejb.Local;

@Local
public interface ScreenIdleServiceLocal {
	
	int syncLockScreenSessionTimeout();
	
	void refreshSessionTimeOut(String userId);
	
	int getSessionTimeout();
	
	void destroy();
}
