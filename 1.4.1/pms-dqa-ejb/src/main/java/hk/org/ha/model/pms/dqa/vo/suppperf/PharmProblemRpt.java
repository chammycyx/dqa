package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmProblemRpt {
	private String problemNum;
	private String problemStatus;
	private String createDate;
	private String problemDate;
	private String caseNum;
	private String institutionCode;
	private String itemCode;
	private String orderType;
	private String contractNum;
	private String affectQty;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	private String batchNum;
	private String country;
	private String natureOfProblem;
	private String problemDetail;
	private String sendToType;
	private String sendToSuppCode;
	private String suppCollectDate;
	private String sendToQaCode;
	private String qaCollectDate;
	private String pharmSendMethod;
	private String sampleQty;
	private String problemByName;
	private String problemByRank;
	private String problemByTypeDesc;
	private String coordinatorName;
	private String coordinatorRank;
	private String coordinatorPhone;
	
	
	public String getProblemNum() {
		return problemNum;
	}
	public void setProblemNum(String problemNum) {
		this.problemNum = problemNum;
	}
	public String getProblemStatus() {
		return problemStatus;
	}
	public void setProblemStatus(String problemStatus) {
		this.problemStatus = problemStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getProblemDate() {
		return problemDate;
	}
	public void setProblemDate(String problemDate) {
		this.problemDate = problemDate;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getAffectQty() {
		return affectQty;
	}
	public void setAffectQty(String affectQty) {
		this.affectQty = affectQty;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNatureOfProblem() {
		return natureOfProblem;
	}
	public void setNatureOfProblem(String natureOfProblem) {
		this.natureOfProblem = natureOfProblem;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public String getSendToType() {
		return sendToType;
	}
	public void setSendToType(String sendToType) {
		this.sendToType = sendToType;
	}
	public String getSendToSuppCode() {
		return sendToSuppCode;
	}
	public void setSendToSuppCode(String sendToSuppCode) {
		this.sendToSuppCode = sendToSuppCode;
	}
	public String getSuppCollectDate() {
		return suppCollectDate;
	}
	public void setSuppCollectDate(String suppCollectDate) {
		this.suppCollectDate = suppCollectDate;
	}
	public String getSendToQaCode() {
		return sendToQaCode;
	}
	public void setSendToQaCode(String sendToQaCode) {
		this.sendToQaCode = sendToQaCode;
	}
	public String getQaCollectDate() {
		return qaCollectDate;
	}
	public void setQaCollectDate(String qaCollectDate) {
		this.qaCollectDate = qaCollectDate;
	}
	public String getPharmSendMethod() {
		return pharmSendMethod;
	}
	public void setPharmSendMethod(String pharmSendMethod) {
		this.pharmSendMethod = pharmSendMethod;
	}
	public String getSampleQty() {
		return sampleQty;
	}
	public void setSampleQty(String sampleQty) {
		this.sampleQty = sampleQty;
	}
	public String getProblemByName() {
		return problemByName;
	}
	public void setProblemByName(String problemByName) {
		this.problemByName = problemByName;
	}
	public String getProblemByRank() {
		return problemByRank;
	}
	public void setProblemByRank(String problemByRank) {
		this.problemByRank = problemByRank;
	}
	public String getProblemByTypeDesc() {
		return problemByTypeDesc;
	}
	public void setProblemByTypeDesc(String problemByTypeDesc) {
		this.problemByTypeDesc = problemByTypeDesc;
	}
	public String getCoordinatorName() {
		return coordinatorName;
	}
	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}
	public String getCoordinatorRank() {
		return coordinatorRank;
	}
	public void setCoordinatorRank(String coordinatorRank) {
		this.coordinatorRank = coordinatorRank;
	}
	public String getCoordinatorPhone() {
		return coordinatorPhone;
	}
	public void setCoordinatorPhone(String coordinatorPhone) {
		this.coordinatorPhone = coordinatorPhone;
	}
	

	
	
}