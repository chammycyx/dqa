package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum NatureOfTrx implements StringValuedEnum {
	
	H("H", "Transfer From Hospital"),
	L("L", "Transfer To Laboratory");
	
    private final String dataValue;
    private final String displayValue;
        
    NatureOfTrx(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static NatureOfTrx findByDataValue(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(NatureOfTrx.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<NatureOfTrx> {

		private static final long serialVersionUID = -4844612573024106631L;

		@Override
    	public Class<NatureOfTrx> getEnumClass() {
    		return NatureOfTrx.class;
    	}
    }
}
