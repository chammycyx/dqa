package hk.org.ha.model.pms.dqa.persistence.coa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COA_OUTSTAND_BATCH")
@NamedQueries( {
	@NamedQuery(name = "CoaOutstandBatch.findForOutstand", query = "select o from CoaOutstandBatch o where o.contract = :contract and o.batchNum = :batchNum and o.receiveDate is null "),
	@NamedQuery(name = "CoaOutstandBatch.findByCoaOutstandBatchId", query = "select o from CoaOutstandBatch o where o.coaOutstandBatchId = :coaOutstandBatchId "),
	@NamedQuery(name = "CoaOutstandBatch.findForOutstandBatchCoa", query = "select o from CoaOutstandBatch o where " +
																" (:fromReceiveDate is null or o.receiveDate >= :fromReceiveDate) " +
																" and (:toReceiveDate is null or o.receiveDate < :toReceiveDate) " +
																" and (:fromCreateDate is null or o.createDate >= :fromCreateDate ) " +
																" and (:toCreateDate is null or o.createDate < :toCreateDate) " +																
																" and (:isOutstand is null or o.receiveDate is null) "
																),
    @NamedQuery(name = "CoaOutstandBatch.findForCoaFileFolder", query = "select o from CoaOutstandBatch o where o.coaFileFolder.coaFileFolderId = :coaFileFolderId")
    		                                                         
})
@Customizer(AuditCustomizer.class)
public class CoaOutstandBatch extends VersionEntity {

	private static final long serialVersionUID = -1058570489885782020L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaOutstandBatchSeq")
	@SequenceGenerator(name="coaOutstandBatchSeq", sequenceName="SEQ_COA_OUTSTAND_BATCH", initialValue=10000)
	@Column(name="COA_OUTSTAND_BATCH_ID")
	private Long coaOutstandBatchId;

	@Column(name="BATCH_NUM", length=20)
	private String batchNum;
	
	@Column(name="RECEIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date receiveDate;	
	
	@ManyToOne
	@JoinColumn(name="CONTRACT_ID")
	private Contract contract;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COA_IN_EMAIL_ATT_ID")
	private CoaInEmailAtt coaInEmailAtt;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COA_FILE_FOLDER_ID")	
	private CoaFileFolder coaFileFolder;
	
	public void setCoaOutstandBatchId(Long coaOutstandBatchId) {
		this.coaOutstandBatchId = coaOutstandBatchId;
	}

	public Long getCoaOutstandBatchId() {
		return coaOutstandBatchId;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setReceiveDate(Date receiveDate) {
		if (receiveDate != null) {
			this.receiveDate = new Date(receiveDate.getTime());
		} else {
			this.receiveDate = null;
		}
	}

	public Date getReceiveDate() {
		return (receiveDate != null) ? new Date(receiveDate.getTime()) : null;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Contract getContract() {
		return contract;
	}

	public void setCoaInEmailAtt(CoaInEmailAtt coaInEmailAtt) {
		this.coaInEmailAtt = coaInEmailAtt;
	}

	public CoaInEmailAtt getCoaInEmailAtt() {
		return coaInEmailAtt;
	}

	public void setCoaFileFolder(CoaFileFolder coaFileFolder) {
		this.coaFileFolder = coaFileFolder;
	}

	public CoaFileFolder getCoaFileFolder() {
		return coaFileFolder;
	}

	
}
