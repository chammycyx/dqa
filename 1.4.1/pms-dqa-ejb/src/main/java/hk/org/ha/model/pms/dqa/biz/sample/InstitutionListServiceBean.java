package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("institutionListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class InstitutionListServiceBean implements InstitutionListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Institution> institutionListByRecordStatus;
	
	@Out(required = false)
	private List<Institution> institutionList;
	
	@SuppressWarnings("unchecked")
	public void retreiveInstitutionListByPcuFlag(YesNoFlag pcuFlag){
		logger.debug("retreiveInstitutionListByPcuFlag");
		
		institutionList = em.createNamedQuery("Institution.findByPcuFlag")
			.setParameter("pcuFlag", pcuFlag)
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveInstitutionListByPcuFlagRecordStatus(YesNoFlag pcuFlag, RecordStatus recordStatus){
		
		logger.debug("retrieveInstitutionList");
		
		institutionListByRecordStatus = em.createNamedQuery("Institution.findByPcuFlagRecordStatus")
			.setParameter("pcuFlag", pcuFlag)
			.setParameter("recordStatus", recordStatus.getDataValue())
			.getResultList();
		
	}

	@Remove
	public void destroy(){
		
		if (institutionListByRecordStatus != null){
			institutionListByRecordStatus = null;
		}
		
		if (institutionList != null){
			institutionList = null;
		}
	}
	
}
