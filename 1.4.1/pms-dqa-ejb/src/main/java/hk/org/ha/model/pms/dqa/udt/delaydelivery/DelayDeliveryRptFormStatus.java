package hk.org.ha.model.pms.dqa.udt.delaydelivery;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum DelayDeliveryRptFormStatus implements StringValuedEnum {
	
	Drafted("D","Drafted"),
	Submitted("S", "Submitted"),
	CpoProcessing("P", "CPO Processing"),
	Closed("C", "Closed"),
	ReturnedForFurtherActions("R", "Returned For Further Actions"),
	Deleted("X", "Deleted");
	
    private final String dataValue;
    private final String displayValue;
    
    DelayDeliveryRptFormStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<DelayDeliveryRptFormStatus> {

	
		private static final long serialVersionUID = -548201793656293205L;

		@Override
    	public Class<DelayDeliveryRptFormStatus> getEnumClass() {
    		return DelayDeliveryRptFormStatus.class;
    	}
    }
}
