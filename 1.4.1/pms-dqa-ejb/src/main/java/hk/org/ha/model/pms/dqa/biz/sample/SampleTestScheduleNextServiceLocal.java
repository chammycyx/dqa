package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleNextServiceLocal {

	SampleTestSchedule retrieveNextSampleTestSchedule(SampleTestScheduleFile sampleTestScheduleFileIn);
	
	boolean isSuccess();
	
	SampleTestSchedule getSampleTestScheduleNext();
	
	SampleTestSchedule checkForInsertNextSampleTestScheduleByErpContractTestCode(hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl, String testCode);
	
	SampleTestSchedule retrieveLatestPCSampleTestSchedule(String itemCode, String testCode);
		
	void destory();
}
