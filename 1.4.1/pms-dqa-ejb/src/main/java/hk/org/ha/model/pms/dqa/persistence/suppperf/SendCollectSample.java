package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.udt.suppperf.SampleSendMethod;
import hk.org.ha.model.pms.dqa.udt.suppperf.SupplierCpoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SEND_COLLECT_SAMPLE")
@NamedQueries({
	@NamedQuery(name = "SendCollectSample.findByUserCode", query = "select o from SendCollectSample o where o.sendToQa.userCode = :userCode ")
})
@Customizer(AuditCustomizer.class)
public class SendCollectSample extends VersionEntity {

	private static final long serialVersionUID = -1752753378045408759L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sendCollectSampleSeq")
	@SequenceGenerator(name = "sendCollectSampleSeq", sequenceName = "SEQ_SEND_COLLECT_SAMPLE", initialValue=10000)
	@Id
	@Column(name="SEND_COLLECT_SAMPLE_ID", nullable=false)
	private Long sendCollectSampleId;
	
	@Converter(name = "SendCollectSample.pharmSendMethod", converterClass = SampleSendMethod.Converter.class )
	@Convert("SendCollectSample.pharmSendMethod")
	@Column(name="PHARM_SEND_METHOD", length=1)
	private SampleSendMethod pharmSendMethod;
	
	@Column(name="PHARM_SEND_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date pharmSendDate;
	
	
	@Converter(name = "SendCollectSample.sendToType", converterClass = SupplierCpoFlag.Converter.class )
	@Convert("SendCollectSample.sendToType")
	@Column(name="SEND_TO_TYPE", length=1)
	private SupplierCpoFlag sendToType;
	
	@Column(name="SAMPLE_QTY")
    private Integer sampleQty;
	
	@Column(name="QA_COLLECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date qaCollectDate;
	
	@Column(name="SUPP_COLLECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date suppCollectDate;
	
	
	@ManyToOne
	@JoinColumn(name="SUPPLIER_CODE")
	private Supplier sendToSupp;
	
	@ManyToOne
	@JoinColumn(name="USER_CODE")
	private UserInfo sendToQa;
	
	
	public Long getSendCollectSampleId() {
		return sendCollectSampleId;
	}

	public void setSendCollectSampleId(Long sendCollectSampleId) {
		this.sendCollectSampleId = sendCollectSampleId;
	}

	public SampleSendMethod getPharmSendMethod() {
		return pharmSendMethod;
	}

	public void setPharmSendMethod(SampleSendMethod pharmSendMethod) {
		this.pharmSendMethod = pharmSendMethod;
	}

	public Date getPharmSendDate() {
		return (pharmSendDate!=null)?new Date(pharmSendDate.getTime()):null;
	}

	public void setPharmSendDate(Date pharmSendDate) {
		if (pharmSendDate!=null) {
			this.pharmSendDate = new Date(pharmSendDate.getTime());
		} else {
			this.pharmSendDate = null;
		}
	}

	public SupplierCpoFlag getSendToType() {
		return sendToType;
	}

	public void setSendToType(SupplierCpoFlag sendToType) {
		this.sendToType = sendToType;
	}

	public Integer getSampleQty() {
		return sampleQty;
	}

	public void setSampleQty(Integer sampleQty) {
		this.sampleQty = sampleQty;
	}

	public Date getQaCollectDate() {
		return (qaCollectDate!=null)?new Date(qaCollectDate.getTime()):null;
	}

	public void setQaCollectDate(Date qaCollectDate) {
		if (qaCollectDate!=null) {
			this.qaCollectDate = new Date(qaCollectDate.getTime());
		} else {
			this.qaCollectDate = null;
		}
	}

	public Date getSuppCollectDate() {
		return (suppCollectDate!=null)?new Date(suppCollectDate.getTime()):null;
	}

	public void setSuppCollectDate(Date suppCollectDate) {
		if (suppCollectDate!=null) {
			this.suppCollectDate = new Date(suppCollectDate.getTime());
		} else {
			this.suppCollectDate = null;
		}
	}

	public Supplier getSendToSupp() {
		return sendToSupp;
	}

	public void setSendToSupp(Supplier sendToSupp) {
		this.sendToSupp = sendToSupp;
	}

	public UserInfo getSendToQa() {
		return sendToQa;
	}

	public void setSendToQa(UserInfo sendToQa) {
		this.sendToQa = sendToQa;
	}
}
