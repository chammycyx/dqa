package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ChemTests implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	IDandAssay("CT1", "Active Ingredient(s) Identification and Assay"),
	FullSpecification("CT2", "Full Specification"),
	Other("O", "Other");
	
    private final String dataValue;
    private final String displayValue;
        
    ChemTests(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ChemTests> {

		private static final long serialVersionUID = -1623620317837922548L;

		@Override
    	public Class<ChemTests> getEnumClass() {
    		return ChemTests.class;
    	}
    }
}
