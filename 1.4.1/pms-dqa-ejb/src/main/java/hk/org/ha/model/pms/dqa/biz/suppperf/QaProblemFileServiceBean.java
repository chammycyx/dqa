package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FileItemManagerLocal;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemFile;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;

import java.io.File;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qaProblemFileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class QaProblemFileServiceBean implements QaProblemFileServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private FileItemManagerLocal fileItemManager;
	
	private static final String FILE_PATH_PREFIX ="SUPPPERF";
	private static final String PATH_DELIMITER ="/";
		
	private boolean success;
	
	private String errorCode;
	
	private String uploadPath;

	public QaProblemFile uploadFile(QaProblemFileUploadData qaProblemFileUploadDataIn, QaProblem qaProblemIn){
		logger.debug("uploadFile");
		
		QaProblemFile qaProblemFileSave = new QaProblemFile();
		
		FileItem fileItemIn = qaProblemFileUploadDataIn.getFileItem();
		String fileStrParam = qaProblemFileUploadDataIn.getRefUploadFileName();
		byte[] bytes = qaProblemFileUploadDataIn.getFileData();
		
		qaProblemFileSave.setFileItem(fileItemIn);
		qaProblemFileSave.setQaProblem(qaProblemIn);
		qaProblemFileSave.setShareFlag(qaProblemFileUploadDataIn.isShareFlag()?YesNoFlag.Yes:YesNoFlag.No);	
		
		String fileStrIn = fileStrParam.trim();
		logger.debug("uploadFile : #0 ", fileStrIn);
		success = false;
		errorCode ="0019";
		
		if (fileStrIn!=null && bytes !=null){

			String ext = (fileStrIn.lastIndexOf('.')==-1)?"":fileStrIn.substring(fileStrIn.lastIndexOf('.')+1,fileStrIn.length());
			String dbfileName = formatFileName(fileItemIn);
			String dbFilePath = formatFilePath(qaProblemFileUploadDataIn);

			String diskFilePath = uploadPath + PATH_DELIMITER + dbFilePath ;

			qaProblemFileSave.getFileItem().setFileName(dbfileName);
			
			
			File fDir = new File(diskFilePath);

			if (!fDir.exists()){
				boolean dirCreated = fDir.mkdirs();
				logger.debug("new folder : #0 , #1",dirCreated, fDir.getPath());
			}

			File f = new File(diskFilePath, dbfileName+"."+ext);
			
			logger.debug("file path : "+ f.getPath());
			int fileNum = 1;
			String newFileName ="";
			if (fDir.exists()){
				while(f.exists()){
					newFileName = dbfileName+"("+ fileNum +")."+ ext;

					f = new File(diskFilePath, newFileName);
					fileNum++;
				}
				
				if (fileItemManager.uploadFile(f, bytes)){
					logger.info("File uploaded : #0", f.getPath());
					
					qaProblemFileSave.getFileItem().setFileName(dbfileName);
					qaProblemFileSave.getFileItem().setFilePath(dbFilePath + PATH_DELIMITER+f.getName());
					
					try {
						createQaProblemFile(qaProblemFileSave);
						success = true;
						errorCode = null;
						
					}finally{
						if (!success){
							if (qaProblemFileSave!=null){
								qaProblemFileSave.getFileItem().setFileName(fileStrIn.substring(0, fileStrIn.indexOf('.')-1));
								qaProblemFileSave.getFileItem().setFilePath(fileStrIn);
							}
							if (f.exists()){
								fileItemManager.deleteFile(f);
								logger.info("Process failure , File deleted : #0", f.getPath());
							}
						}
					}
				}
			}
		}
		
		return qaProblemFileSave;
	}
	
	
	public void deleteFile(QaProblemFileUploadData qaProblemFileUploadDataIn, QaProblem qaProblemIn){
		logger.debug("deleteFile");
		
		success = false;
		errorCode ="0019";
		

		String diskFilePath = uploadPath + PATH_DELIMITER + qaProblemFileUploadDataIn.getFileItem().getFilePath() ;

		File f = new File(diskFilePath);
		
		logger.debug("file path : "+ f.getPath());
		
		fileItemManager.deleteFile(f);
		
		for(QaProblemFile qcf:qaProblemIn.getQaProblemFileList())
		{
			if(qcf.getFileItem()!=null)
			{
				if(qcf.getFileItem().getFileId().equals(qaProblemFileUploadDataIn.getFileItem().getFileId()))
				{
					FileItem fiFind = qcf.getFileItem();
					
					qcf.setFileItem(null);
					QaProblemFile qcfFind =em.merge(qcf); 
					em.remove(qcfFind);
					em.flush();
					
					fiFind = em.merge(fiFind);
					em.remove(fiFind);
					em.flush();
				}
			}
		}

		success = true;
		errorCode = null;
	}
	
	private String formatFileName(FileItem fileItemIn){
		
		String dbfileName=fileItemIn.getFileName();
	
		return dbfileName;
	}
	
	
	private String formatFilePath(QaProblemFileUploadData qaProblemFileUploadDataIn){
		
		String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + qaProblemFileUploadDataIn.getItemCode()
		+ PATH_DELIMITER + qaProblemFileUploadDataIn.getSupplierCode() 
		+ PATH_DELIMITER + qaProblemFileUploadDataIn.getManufCode();
		
		return dbFilePath;
	}
	
	
		
	public void createQaProblemFile(QaProblemFile qaProblemFileIn){
		logger.debug("createQaProblemFile");
		
		em.persist(qaProblemFileIn.getFileItem());
		em.flush();
		
		em.persist(qaProblemFileIn);
		em.flush();
	}
	
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
	}
}
