package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.model.pms.dqa.biz.coa.CoaResultServiceBean;
import hk.org.ha.model.pms.dqa.biz.coa.CoaResultServiceLocal;
import hk.org.ha.model.pms.dqa.interfaces.DqaService;

import javax.ejb.Stateless;

import org.jboss.seam.Component;

@Stateless(mappedName="DqaService")
@SuppressWarnings("unchecked")
public class DqaServiceBean implements DqaService {
	
	public Boolean getCoaResult(String jobId) {
		return getCoaResultServiceBean().getCoaResult(jobId);
	}
	
	public Boolean importErpContract(String jobId) {
		return getErpContractImportServiceBean().importErpContract(jobId);
	}
	
	public Boolean importPhsSupplier(String jobId) {
		return getPhsImportServiceBean().importSupplier();
	}
	
	public Boolean importPhsCompany(String jobId) {
		return getPhsImportServiceBean().importCompany();
	}
	
	public Boolean importPhsInstitution(String jobId) {
		return getPhsImportServiceBean().importInstitution();
	}
	
	public Boolean importPhsStockMonthlyExp(String jobId) {
		return getPhsImportServiceBean().importStockMonthlyExp();	
	}
	
	public Boolean updateFuncSeqNumYear(String jobId) {
		return getPhsImportServiceBean().updateFuncSeqNumYear();
	}
	
	
	private CoaResultServiceLocal getCoaResultServiceBean() {
		return (CoaResultServiceLocal) lookupComponent( CoaResultServiceBean.class );
	}
	
	private ErpContractImportServiceLocal getErpContractImportServiceBean() {
		return (ErpContractImportServiceLocal) lookupComponent( ErpContractImportServiceBean.class );
	}
	
	private PhsImportServiceLocal getPhsImportServiceBean() {
		return (PhsImportServiceLocal) lookupComponent( PhsImportServiceBean.class );
	}
	
	private Object lookupComponent(Class clazz) {		
		return Component.getInstance(clazz, true);
	}   

}

