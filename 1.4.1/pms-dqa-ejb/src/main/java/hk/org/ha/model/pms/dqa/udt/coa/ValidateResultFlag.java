package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ValidateResultFlag implements StringValuedEnum {
	
	All("A", "All"),//for front-end display and should not be persisted in DB
	Pass("Y", "Pass"),
	Fail("N", "Fail");
	
    private final String dataValue;
    private final String displayValue;
        
    ValidateResultFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ValidateResultFlag> {

		private static final long serialVersionUID = 6368919235452551382L;

		@Override
    	public Class<ValidateResultFlag> getEnumClass() {
    		return ValidateResultFlag.class;
    	}
    }
}
