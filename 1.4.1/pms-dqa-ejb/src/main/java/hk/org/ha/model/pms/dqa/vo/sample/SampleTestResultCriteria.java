package hk.org.ha.model.pms.dqa.vo.sample;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.sample.Result;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestResultCriteria {
	
	private RecordStatus recordStatus;
	private String itemCode;
	private String testCode;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	private Result testResult;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromTestReportDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toTestReportDate;

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public Result getTestResult() {
		return testResult;
	}

	public void setTestResult(Result testResult) {
		this.testResult = testResult;
	}

	public Date getFromTestReportDate() {
		return (fromTestReportDate != null) ? new Date(fromTestReportDate.getTime()) : null;
	}

	public void setFromTestReportDate(Date fromTestReportDate) {
		if (fromTestReportDate != null) {
			this.fromTestReportDate = new Date(fromTestReportDate.getTime());
		}
	}

	public Date getToTestReportDate() {
		return (toTestReportDate != null) ? new Date(toTestReportDate.getTime()) : null;
	}

	public void setToTestReportDate(Date toTestReportDate) {
		if (toTestReportDate != null){
			this.toTestReportDate = new Date(toTestReportDate.getTime());
		}	
	}
	
	
	
}
