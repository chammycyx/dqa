package hk.org.ha.model.pms.dqa.vo.sample;

import hk.org.ha.model.pms.dqa.udt.OrderType;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class InstAssignmentCriteria {
	
	private Date scheduleMonth;
	private String itemCode;
	private String testCode;
	private OrderType orderType;
	private String riskLevelCode;
	
	public Date getScheduleMonth() {
		return (scheduleMonth != null) ? new Date(scheduleMonth.getTime()) : null;
	}
	public void setScheduleMonth(Date scheduleMonth) {
		if (scheduleMonth != null){
			this.scheduleMonth = new Date(scheduleMonth.getTime());
		}
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public String getTestCode() {
		return testCode;
	}
	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}
	public String getRiskLevelCode() {
		return riskLevelCode;
	}
}
