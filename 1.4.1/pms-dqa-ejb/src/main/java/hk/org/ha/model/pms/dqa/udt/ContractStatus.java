package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ContractStatus implements StringValuedEnum {
	
	Approved("APPROVED", "Approved"),
	Rejected("REJECTED", "Rejected"),
	InProcess("IN PROCESS", "In Process"),
	Preapproved("PRE-APPROVED", "Pre-approved"),
	RequiresReapproval("REQUIRES REAPPROVAL", "Requires reapproval");	
	
    private final String dataValue;
    private final String displayValue;
        
    ContractStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ContractStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<ContractStatus> getEnumClass() {
    		return ContractStatus.class;
    	}
    }
}
