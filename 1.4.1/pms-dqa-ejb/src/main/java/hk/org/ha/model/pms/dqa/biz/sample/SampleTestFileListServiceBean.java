package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestFileCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestFileListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestFileListServiceBean implements SampleTestFileListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<SampleTestFile> sampleTestFileList;
	
	private static final String O_SUPPLIER = "o.supplier";
	private static final String O_MANUFACTURER = "o.manufacturer";
	private static final String O_PHARMCOMPANY = "o.pharmCompany";
	private static final String O_FILE_ITEM = "o.fileItem";
	
    @SuppressWarnings("unchecked")
	public void retrieveSampleTestFileList(SampleTestFileCriteria sampleTestScheduleDocumentCriteria){
    	logger.debug("retrieveSampleTestFileList");
    	
    	sampleTestFileList = em.createNamedQuery("SampleTestFile.findByCriteria")
	    	.setParameter("supplierCode", sampleTestScheduleDocumentCriteria.getSupplierCode())
	    	.setParameter("itemCode", sampleTestScheduleDocumentCriteria.getItemCode())
	    	.setParameter("manufCode", sampleTestScheduleDocumentCriteria.getManufCode())
	    	.setParameter("pharmCode", sampleTestScheduleDocumentCriteria.getPharmCode())
	    	.setParameter("fileCat", sampleTestScheduleDocumentCriteria.getDocType())
	    	.setParameter("fileCatStr", (sampleTestScheduleDocumentCriteria.getDocType()==null?"All":sampleTestScheduleDocumentCriteria.getDocType().getDataValue()))
	    	.setHint(QueryHints.FETCH, O_SUPPLIER)
	    	.setHint(QueryHints.FETCH, O_MANUFACTURER)
	    	.setHint(QueryHints.FETCH, O_PHARMCOMPANY)
	    	.setHint(QueryHints.FETCH, O_FILE_ITEM)
	    	.getResultList();
    }
    
    
    @SuppressWarnings("unchecked")
	private List<SampleTestFile> retrieveSampleTestFileListByItemCodeSuppCodeManufCodePharmCode(String itemCode, String supplierCode, String manufCode, String pharmCode){
    	
    	return  em.createNamedQuery("SampleTestFile.findByItemCodeSuppCodeManufCodePharmCode")
		.setParameter("itemCode", itemCode)
		.setParameter("supplierCode", supplierCode)
		.setParameter("manufCode", manufCode)
		.setParameter("pharmCode", pharmCode)
		.setHint(QueryHints.FETCH, O_SUPPLIER)
    	.setHint(QueryHints.FETCH, O_MANUFACTURER)
    	.setHint(QueryHints.FETCH, O_PHARMCOMPANY)
    	.setHint(QueryHints.FETCH, O_FILE_ITEM)
		.getResultList();
    	
    }
      
	public void retrieveSampleTestFileListForSchedConfirm(String itemCode, String supplierCode, String manufCode, String pharmCode){
		logger.debug("retrieveSampleTestScheduleFileListForSchedConfirm : #0 #1 #2 ", itemCode, supplierCode, manufCode);
		
		List<SampleTestFile> fileList = retrieveSampleTestFileListByItemCodeSuppCodeManufCodePharmCode(itemCode, supplierCode, manufCode, pharmCode);

		HashMap<String, SampleTestFile> sampleTestFileHM = new HashMap<String , SampleTestFile>();
		
		for (SampleTestFile stFile:fileList){
			String catKey = stFile.getFileCat().getDataValue();
			
			if (!sampleTestFileHM.containsKey(catKey)){
				sampleTestFileHM.put(catKey, stFile);
			}
		}
		
		if (!fileList.isEmpty() ){
			sampleTestFileList = new ArrayList<SampleTestFile>();
			for (SampleTestFileCat fileCat:SampleTestFileCat.values()){
				
				if (sampleTestFileHM.containsKey(fileCat.getDataValue())){
					SampleTestFile stFile = (SampleTestFile)sampleTestFileHM.get(fileCat.getDataValue());
			
					sampleTestFileList.add(stFile);
					
				}
			}
		}else {
			sampleTestFileList = null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestFileListBySampleTestFileCat(SampleTestFileCat fileCat){
		logger.debug("retrieveSampleTestFileListBySampleTestFileCat #0", fileCat);
		
		List<SampleTestFile> fileList= em.createNamedQuery("SampleTestFile.findBySampleTestFileCat")
		.setParameter("fileCat", fileCat)
		.setHint(QueryHints.FETCH, O_SUPPLIER)
		.setHint(QueryHints.FETCH, "o.supplier.contact")
    	.setHint(QueryHints.FETCH, O_MANUFACTURER)
    	.setHint(QueryHints.FETCH, O_PHARMCOMPANY)
    	.setHint(QueryHints.FETCH, O_FILE_ITEM)
		.getResultList();
		
		for (SampleTestFile stf : fileList){
			stf.getSupplier();
			stf.getSupplier().getContact();
			stf.getDmDrug();
			stf.getManufacturer();
			stf.getManufacturer().getContact();
			stf.getPharmCompany().getContact();
		}
		sampleTestFileList = fileList;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestFileListBySampleTestFileCatContract(SampleTestFileCat fileCat, Contract contractIn){
		logger.debug("retrieveSampleTestFileListBySampleTestFileCat #0", fileCat);
		
		List<SampleTestFile> fileList= em.createNamedQuery("SampleTestFile.findBySampleTestFileCatSMP")
		.setParameter("fileCat", fileCat)
		.setParameter("supplierCode", contractIn.getSupplier().getSupplierCode())
		.setParameter("manufacturerCode", contractIn.getManufacturer().getCompanyCode())
		.setParameter("pharmCompanyCode", contractIn.getPharmCompany().getCompanyCode())
		.setHint(QueryHints.FETCH, O_SUPPLIER)
		.setHint(QueryHints.FETCH, "o.supplier.contact")
    	.setHint(QueryHints.FETCH, O_MANUFACTURER)
    	.setHint(QueryHints.FETCH, O_PHARMCOMPANY)
    	.setHint(QueryHints.FETCH, O_FILE_ITEM)
		.getResultList();
		
		for (SampleTestFile stf : fileList){
			stf.getSupplier();
			stf.getSupplier().getContact();
			stf.getDmDrug();
			stf.getManufacturer();
			stf.getPharmCompany();
		}
		sampleTestFileList = fileList;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestFileListBySampleTestSchedule(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("retrieveSampleTestFileListBySampleTestSchedule");
		sampleTestFileList = null;
		SampleTestSchedule lazySampleTestScheduleIn;
		
		List<SampleTestFileCat> fileCatList = new ArrayList<SampleTestFileCat>();
		fileCatList.add(SampleTestFileCat.MethodofAssay);
		fileCatList.add(SampleTestFileCat.FinishedProductSpecification);
		fileCatList.add(SampleTestFileCat.ValidationReport);
		
		if(sampleTestScheduleIn==null)
		{
			sampleTestFileList=null;
		}
		else
		{
			lazySampleTestScheduleIn = getLazySampleTestSchedule(sampleTestScheduleIn);
			sampleTestFileList = em.createNamedQuery("SampleTestFile.findByItemSupplierManufacturerPharmCompanyFileCatList")
											.setParameter("itemCode", lazySampleTestScheduleIn.getSampleItem().getItemCode())
											.setParameter("supplierCode", lazySampleTestScheduleIn.getContract().getSupplier().getSupplierCode())
											.setParameter("manufacturerCode", lazySampleTestScheduleIn.getContract().getManufacturer().getCompanyCode())
											.setParameter("pharmCompanyCode", lazySampleTestScheduleIn.getContract().getPharmCompany().getCompanyCode())
											.setParameter("fileCatList", fileCatList)
											.getResultList();
			
		}
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
    
    @Remove
	public void destroy(){
    	if (sampleTestFileList != null){
    		sampleTestFileList = null;
    	}
    }
}
