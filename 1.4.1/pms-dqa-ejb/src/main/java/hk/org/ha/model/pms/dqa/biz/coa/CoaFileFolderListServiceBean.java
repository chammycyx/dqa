package hk.org.ha.model.pms.dqa.biz.coa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaFileFolderListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaFileFolderListServiceBean implements CoaFileFolderListServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private CoaDiscrepancyListManagerLocal coaDiscrepancyListManager;
	
	@Out(required = false)
	private List<CoaFileFolder> coaFileFolderOrgCoaList;
	
	@Out(required = false)
	private List<CoaFileFolder> coaFileFolderBatchCoaList;
	
	private void buildDiscrepancyKeyBatchNumMap(Map<CoaDiscrepancyKey,StringBuffer> discrepancyKeyBatchNumMap, CoaDiscrepancy discrepancy) {
		StringBuffer batchNumBuffer = discrepancyKeyBatchNumMap.get(discrepancy.getCoaDiscrepancyKey());
		if (batchNumBuffer == null) {
			batchNumBuffer = new StringBuffer();
			discrepancyKeyBatchNumMap.put(discrepancy.getCoaDiscrepancyKey(), batchNumBuffer);
		} else {
			batchNumBuffer.append(", ");
		}
		batchNumBuffer.append(discrepancy.getCoaBatch().getBatchNum());
	}
	
	private String buildCombinedDiscrepancyDescForEmailToSupp(List<CoaDiscrepancy> allRelatedDiscrepancyList, CoaBatch coaBatchIn) {
		
		Set<String> batchNumSet = new TreeSet<String>();
		Map<String, List<CoaDiscrepancy>> batchNumDiscrepancyMap = new HashMap<String, List<CoaDiscrepancy>>();
		for (CoaDiscrepancy discrepancy:allRelatedDiscrepancyList) {

			List<CoaDiscrepancy> discrepancyList = batchNumDiscrepancyMap.get(discrepancy.getCoaBatch().getBatchNum());
			if (discrepancyList == null) {
				discrepancyList = new ArrayList<CoaDiscrepancy>();
				batchNumDiscrepancyMap.put(discrepancy.getCoaBatch().getBatchNum(), discrepancyList);
			}
			discrepancyList.add(discrepancy);
			
			if (discrepancy.getCoaBatch().getCoaBatchId().equals(coaBatchIn.getCoaBatchId()) 
					|| (discrepancy.getParentCoaDiscrepancy() != null && discrepancy.getParentCoaDiscrepancy().getCoaBatch().getBatchNum().equals(coaBatchIn.getBatchNum()))) {
				batchNumSet.add(discrepancy.getCoaBatch().getBatchNum());				
			}		
		}
		
		Map<CoaDiscrepancyKey,StringBuffer> discrepancyKeyBatchNumMap = new HashMap<CoaDiscrepancyKey,StringBuffer>();
		for (String batchNumStr:new ArrayList<String>(batchNumSet)) {
			for (CoaDiscrepancy discrepancy:batchNumDiscrepancyMap.get(batchNumStr)) {
				buildDiscrepancyKeyBatchNumMap(discrepancyKeyBatchNumMap, discrepancy);
			}
		}
		
		StringBuffer combinedDiscrepancyDesc = new StringBuffer();
		List<Map.Entry<CoaDiscrepancyKey, StringBuffer>> list = new ArrayList<Map.Entry<CoaDiscrepancyKey, StringBuffer>>(discrepancyKeyBatchNumMap.entrySet());
		Collections.sort(list, EntryComparator.getInstance());
		for (Map.Entry<CoaDiscrepancyKey, StringBuffer> entry : list) {
			combinedDiscrepancyDesc.append("- ");
			combinedDiscrepancyDesc.append(entry.getKey().getDiscrepancyDesc());
			combinedDiscrepancyDesc.append(" (");
			combinedDiscrepancyDesc.append(entry.getValue().toString());
			combinedDiscrepancyDesc.append(")");
			combinedDiscrepancyDesc.append("<br>");
		}
		
		return combinedDiscrepancyDesc.toString();
	}
	
	private List<CoaFileFolder> getCoaFileFolderListForEmail(List<CoaDiscrepancy> allRelatedDiscrepancyList, CoaBatch coaBatchIn) {
		
		List<CoaFileFolder> batchCoaFolderList = em.createQuery("select o from CoaFileFolder o " +
				"where o.coaBatch.coaItem = :coaItem " +
				"order by o.coaFileFolderId asc")
				.setHint(QueryHints.FETCH, "o.fileItem")
				.setHint(QueryHints.FETCH, "o.coaBatch")
				.setParameter("coaItem", coaBatchIn.getCoaItem())
				.getResultList();
		
		Map<String,CoaBatch> groupedMap = new HashMap<String,CoaBatch>();
		List<CoaFileFolder> coaFileFolderList = new ArrayList<CoaFileFolder>();
		
		for (CoaDiscrepancy discrepancy:allRelatedDiscrepancyList) {
			if (discrepancy.getCoaBatch().getCoaBatchId().equals(coaBatchIn.getCoaBatchId()) 
					|| (discrepancy.getParentCoaDiscrepancy() != null && discrepancy.getParentCoaDiscrepancy().getCoaBatch().getBatchNum().equals(coaBatchIn.getBatchNum()))) {
				groupedMap.put(discrepancy.getCoaBatch().getBatchNum(), discrepancy.getCoaBatch());
			}		
		}
		
		for (CoaFileFolder cff:batchCoaFolderList) {
			if (StringUtils.isBlank(cff.getCoaBatch().getBatchNum())) {
				coaFileFolderList.add(cff);
			} else if (groupedMap.get(cff.getCoaBatch().getBatchNum()) != null) {
				coaFileFolderList.add(cff);
			} 
		}
		return coaFileFolderList;
	}
		
	private UserInfo retrieveEmailUserInfo() {
		UamInfo uamInfo = (UamInfo) Contexts.getSessionContext().get("uamInfo");
		return em.find(UserInfo.class, uamInfo.getUserId());
	}
	
	private void retrieveCoaFileFolderListForEmail(CoaBatch coaBatchIn, boolean isEmailToSupp) {
		logger.debug("retrieveCoaFileFolderListForEmailToSupp #0", coaBatchIn);
		
		List<CoaDiscrepancy> allRelatedDiscrepancyList = coaDiscrepancyListManager.getAllRelatedCoaDiscrepancyListByCoaBatchPassFlag(coaBatchIn.getCoaItem(), PassFlag.NotYetPassed);
		
		coaFileFolderBatchCoaList = getCoaFileFolderListForEmail(allRelatedDiscrepancyList, coaBatchIn);
		if (isEmailToSupp) {
			Contexts.getSessionContext().set("combinedDiscrepancyDesc", buildCombinedDiscrepancyDescForEmailToSupp(allRelatedDiscrepancyList, coaBatchIn));
			Contexts.getSessionContext().set("emailUserInfo", retrieveEmailUserInfo());
		}
	}
	
	public void retrieveCoaFileFolderListForEmailToSupp(CoaBatch coaBatchIn) {
		retrieveCoaFileFolderListForEmail(coaBatchIn, true);
	}
	
	public void retrieveCoaFileFolderListForReminderToSupp(CoaBatch coaBatchIn) {
		retrieveCoaFileFolderListForEmail(coaBatchIn, false);
	}
	
	public void retrieveCoaFileFolderListWithoutBatchOthers(Long coaBatchId) {
		logger.debug("retrieveCoaFileFolderListWithoutBatchOthers #0", coaBatchId);
		CoaBatch coaBatchInDB = em.find(CoaBatch.class, coaBatchId);
		coaFileFolderOrgCoaList = retrieveCoaFileFolderOrgCoaList(coaBatchInDB);
		coaFileFolderBatchCoaList = em.createNamedQuery("CoaFileFolder.findBatchCoaWithoutOthersByCoaBatch")
									  .setHint(QueryHints.FETCH, "o.fileItem")
									  .setParameter("coaBatch", coaBatchInDB)
									  .setParameter("fileCat", FileCategory.Batch)
									  .getResultList();		
	}
	
	private List<CoaFileFolder> retrieveCoaFileFolderOrgCoaList(CoaBatch coaBatch)
	{
		return em.createNamedQuery("CoaFileFolder.findOrgCoaByCoaItem")																
									.setHint(QueryHints.FETCH, "o.fileItem")
									.setParameter("coaItem", coaBatch.getCoaItem())
									.getResultList();
	}
	
	@Remove
	public void destroy(){
		if(coaFileFolderOrgCoaList != null) {
			coaFileFolderOrgCoaList = null;  
		}
		if(coaFileFolderBatchCoaList != null) {
			coaFileFolderBatchCoaList = null;  
		}		
	}
	
	private static class EntryComparator implements Comparator<Map.Entry<CoaDiscrepancyKey, StringBuffer>>, Serializable
	{
		private static final long serialVersionUID = 1L;

		private static EntryComparator instance = null;
		
		@Override
		public int compare(Entry<CoaDiscrepancyKey, StringBuffer> e1,
				Entry<CoaDiscrepancyKey, StringBuffer> e2) {
			return e1.getKey().getOrderSeq().compareTo(e2.getKey().getOrderSeq());
		}

		public static EntryComparator getInstance() {
			if (instance == null) {
				instance = new EntryComparator();
			}
			return instance;
		}			
	}
	
}
