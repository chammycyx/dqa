package hk.org.ha.model.pms.dqa.persistence;


import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.persistence.OneToOne;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.hibernate.validator.Length;

@Entity
@NamedQueries({
	@NamedQuery(name = "Supplier.findAllSupplierCode", query = "select o.supplierCode from Supplier o order by o.supplierCode"),
	@NamedQuery(name = "Supplier.findBySupplierCode", query = "select o from Supplier o where o.supplierCode = :supplierCode order by o.supplierCode"),
	@NamedQuery(name = "Supplier.findNonSuspendSupplierList", query = "select o from Supplier o where o.suspendFlag = :suspendFlag order by o.supplierCode"),
	@NamedQuery(name = "Supplier.findAll", query = "select o from Supplier o order by o.supplierCode"),
	@NamedQuery(name = "Supplier.findLikeSupplierCode", query = "select o from Supplier o where o.supplierCode like :supplierCode order by o.supplierCode")
	})
@Table(name = "SUPPLIER")
public class Supplier extends VersionEntity {

	private static final long serialVersionUID = -5793514404731348906L;
	
	@Id
    @Column(name="SUPPLIER_CODE", length = 5, nullable = false)
    @Length(max = 5)
    private String supplierCode;
	
	@Column(name="SUPPLIER_NAME", length = 100, nullable = false)
    private String supplierName;
	
	@Converter(name = "Supplier.suspendFlag", converterClass = SuspendFlag.Converter.class )
	@Convert("Supplier.suspendFlag")
	@Column(name="SUSPEND_FLAG", length = 1)
    private SuspendFlag suspendFlag;

    @Column(name="UPLOAD_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;
    
    @OneToMany(mappedBy="supplier")
    private List<SupplierContact> supplierContactList;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;

	public Date getUploadDate() {
		return (uploadDate != null) ? new Date(uploadDate.getTime()) : null;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = new Date(uploadDate.getTime());
	}

	public void setSupplierContactList(List<SupplierContact> supplierContactList) {
		this.supplierContactList = supplierContactList;
	}

	public List<SupplierContact> getSupplierContactList() {

		if (supplierContactList == null) {
	    	supplierContactList = new ArrayList<SupplierContact>();
	    }
		return supplierContactList;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSuspendFlag(SuspendFlag suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public SuspendFlag getSuspendFlag() {
		return suspendFlag;
	}
}

