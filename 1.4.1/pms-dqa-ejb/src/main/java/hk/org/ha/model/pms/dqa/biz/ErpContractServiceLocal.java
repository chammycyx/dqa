package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import java.util.Date;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface ErpContractServiceLocal {

	void createContract( hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl );
	
	void updateContract( Contract c, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl );
	
	Contract retrieveContractByErpContractLineId( Long erpContractLineId );
	
	void updateContractForSuspension ( Contract contract );
	
	void updateContractStatus( hk.org.ha.model.pms.dqa.vo.Contract ct );
	
	void updateContractLatestEndDate( hk.org.ha.model.pms.dqa.vo.Contract ch, Map<String, Date> endDateMap );
	
	void updateContractForSampleTest( SampleTestSchedule sampleTestSchedule, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl);
	
	
}
