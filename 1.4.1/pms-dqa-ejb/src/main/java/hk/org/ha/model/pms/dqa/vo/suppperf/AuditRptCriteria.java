package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AuditRptCriteria {
	
	private Date startDate;
	private Date endDate;
	private String institutionCode;
	
	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()): null;
	}
	public void setStartDate(Date startDate) {
		if (startDate != null){
			this.startDate = new Date(startDate.getTime());
		}
	}
	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}
	public void setEndDate(Date endDate) {
		if (endDate != null){
			this.endDate = new Date(endDate.getTime());
		}
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	
	
	
}
