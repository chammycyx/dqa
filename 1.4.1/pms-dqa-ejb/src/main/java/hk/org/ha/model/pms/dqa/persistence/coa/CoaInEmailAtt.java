package hk.org.ha.model.pms.dqa.persistence.coa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.coa.AttRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COA_IN_EMAIL_ATT")
@NamedQueries( {
	@NamedQuery(name = "CoaInEmailAtt.findForCoaFileFolder", query = "select o from CoaInEmailAtt o where o.coaFileFolder.coaFileFolderId = :coaFileFolderId")
})
@Customizer(AuditCustomizer.class)
public class CoaInEmailAtt extends VersionEntity {

	private static final long serialVersionUID = 4043577671973281609L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaInEmailAttSeq")
	@SequenceGenerator(name="coaInEmailAttSeq", sequenceName="SEQ_COA_IN_EMAIL_ATT", initialValue=10000)
	@Column(name="COA_IN_EMAIL_ATT_ID")
	private Long coaInEmailAttId;

	@Column(name="FILE_NAME", length=200)
	private String fileName;
		
	@Converter(name = "CoaInEmailAtt.validateResultFlag", converterClass = ValidateResultFlag.Converter.class )
	@Convert("CoaInEmailAtt.validateResultFlag")
	@Column(name="RESULT_FLAG", length=1)
	private ValidateResultFlag resultFlag;
	
	@Converter(name = "CoaInEmailAtt.remarkType", converterClass = AttRemarkType.Converter.class )
	@Convert("CoaInEmailAtt.remarkType")
	@Column(name="REMARK_TYPE", length=1)
	private AttRemarkType remarkType;
	
	@ManyToOne
	@JoinColumn(name="COA_IN_EMAIL_ID")
	private CoaInEmail coaInEmail;
	
	@OneToOne
	@JoinColumn(name="COA_FILE_FOLDER_ID")
	private CoaFileFolder coaFileFolder;
	
	public void setCoaInEmailAttId(Long coaInEmailAttId) {
		this.coaInEmailAttId = coaInEmailAttId;
	}

	public Long getCoaInEmailAttId() {
		return coaInEmailAttId;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setResultFlag(ValidateResultFlag resultFlag) {
		this.resultFlag = resultFlag;
	}

	public ValidateResultFlag getResultFlag() {
		return resultFlag;
	}

	public void setRemarkType(AttRemarkType remarkType) {
		this.remarkType = remarkType;
	}

	public AttRemarkType getRemarkType() {
		return remarkType;
	}

	public void setCoaInEmail(CoaInEmail coaInEmail) {
		this.coaInEmail = coaInEmail;
	}

	public CoaInEmail getCoaInEmail() {
		return coaInEmail;
	}

	public void setCoaFileFolder(CoaFileFolder coaFileFolder) {
		this.coaFileFolder = coaFileFolder;
	}

	public CoaFileFolder getCoaFileFolder() {
		return coaFileFolder;
	}
	
}
