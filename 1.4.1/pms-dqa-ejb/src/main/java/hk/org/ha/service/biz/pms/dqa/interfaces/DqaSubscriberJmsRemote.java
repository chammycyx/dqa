package hk.org.ha.service.biz.pms.dqa.interfaces;

import org.jboss.seam.annotations.async.Asynchronous;
import org.osoa.sca.annotations.OneWay;

public interface DqaSubscriberJmsRemote {
	
	@OneWay
	@Asynchronous
	void initCache();
	
	@OneWay
	@Asynchronous
	void initJmsServiceProxy();	
	
}
