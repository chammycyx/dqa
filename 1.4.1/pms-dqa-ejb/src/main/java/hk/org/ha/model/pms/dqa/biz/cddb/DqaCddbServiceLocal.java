package hk.org.ha.model.pms.dqa.biz.cddb;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.dqa.interfaces.DqaCddbServiceJmsRemote;

@Local
public interface DqaCddbServiceLocal extends DqaCddbServiceJmsRemote {

}
