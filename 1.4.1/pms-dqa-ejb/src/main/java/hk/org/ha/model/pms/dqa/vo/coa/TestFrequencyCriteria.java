package hk.org.ha.model.pms.dqa.vo.coa;

import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;


@ExternalizedBean(type=DefaultExternalizer.class)
public class TestFrequencyCriteria {

	private String orderType;
	private String riskLevelCode;
	private String testCode;
	private RecordStatus recordStatus;
	
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
	public String getRiskLevelCode() {
		return riskLevelCode;
	}
	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}
	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}
	public String getTestCode() {
		return testCode;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
}
