package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFrequency;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleItemFrequencyRptServiceLocal {

	void retrieveSampleItemFrequencyList(Collection<Long> sampleItemKeyList);
	
	List<SampleItemFrequency> getSampleItemFrequencyReportList();
	
	void destroy();
	
}
