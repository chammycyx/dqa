package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Date;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;

import javax.ejb.Local;

@Local
public interface CoaOutstandBatchManagerLocal {
	
	CoaOutstandBatch isOutstandBatchExist( Contract contract, String batchNum );
	
	void createCoaOutstandBatch(Contract c, CoaInEmailAtt att, String batchNum);
	
	void updateCoaOutstandBatch( CoaOutstandBatch coaOutstandBatch, CoaFileFolder folder, Date receiveDate );
	
	CoaOutstandBatch retrieveCoaOutStandBatchForCoaFileFolder(CoaFileFolder coaFileFolder);
}
