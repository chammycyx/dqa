package hk.org.ha.model.pms.dqa.biz.sample;
import java.util.Collection;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleServiceLocal {

	void retrieveSampleTestScheduleByScheduleId(Long scheduleId);
	
	void retrieveSampleTestScheduleForDocument(Long scheduleId);
	
	void retrieveSampleTestScheduleForPaymentByScheduleId(Long scheduleId);
	
	void addSampleTestSchedule();
	
	void createSampleTestSchedule(boolean isConfirm);
	
	void createNextSchedule(SampleTestSchedule sampleTestScheduleIn);
	
	void deleteSampleTestSchedule(SampleTestSchedule sampleTestScheduleIn, CancelSchedReason reason);
	
	void updateSampleTestSchedule(SampleTestSchedule sampleTestScheduleIn, boolean isConfirm);
	
	void updateSampleTestScheduleForSchedConfirm(SampleTestSchedule sampleTestScheduleIn, Collection<SampleTestFile> fileList);
	
	void updateSampleTestScheduleForDocument(SampleTestSchedule sampleTestScheduleIn, List<SampleTestScheduleFile> sampleTestScheduleFileList, String actionType, String isClick);
	
	void updateSampleTestScheduleForInstAssignment(SampleTestSchedule sampleTestScheduleIn, String actionType);
	
	void updateSampleTestScheduleForPayment(SampleTestSchedule sampleTestScheduleIn, String actionType);
	
	void reverseSampleTestScheduleStatus(SampleTestSchedule sampleTestScheduleIn);
	
	void scheduleGeneration(String testCodeIn, OrderType orderTypeIn);
	
	Boolean createNextSampleTestScheduleByErpContract(SampleTestSchedule sampleTestScheduleIn, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl);
	
	Boolean updateNextSampleTestScheduleByErpContract(SampleTestSchedule sampleTestScheduleIn, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl);
	
	void retrieveSampleTestScheduleAndShowScheduleConfirmationPopup(SampleTestSchedule sampleTestScheduleIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destory();
}
