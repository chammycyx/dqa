package hk.org.ha.model.pms.dqa.persistence.coa;

import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "COA_CHECKLIST_KEY")
@NamedQuery(name = "CoaChecklistKey.findAll", query = "select o from CoaChecklistKey o where o.recordStatus = :recordStatus order by o.orderSeq")
public class CoaChecklistKey extends VersionEntity {

	private static final long serialVersionUID = 805712642603954519L;

	@Id
	@Column(name="COA_CHECKLIST_ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaChecklistKeySeq")
	@SequenceGenerator(name="coaChecklistKeySeq", sequenceName="SEQ_COA_CHECKLIST_KEY", initialValue=10000)
	private Long coaChecklistId;

	@Column(name="NAME", length=200, nullable=false)
	private String name;

	@Column(name="ORDER_SEQ")
	private Integer orderSeq;

	@Converter(name = "CoaChecklistKey.recordStatus", converterClass = RecordStatus.Converter.class)
	@Convert("CoaChecklistKey.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	@Transient
	private Integer displayNum;
	
	public CoaChecklistKey() {
	}

	public void setOrderSeq(Integer orderSeq) {
		this.orderSeq = orderSeq;
	}

	public Integer getOrderSeq() {
		return orderSeq;
	}

	public void setCoaChecklistId(Long coaChecklistId) {
		this.coaChecklistId = coaChecklistId;
	}

	public Long getCoaChecklistId() {
		return coaChecklistId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setDisplayNum(Integer displayNum) {
		this.displayNum = displayNum;
	}

	public Integer getDisplayNum() {
		return displayNum;
	}

}

