package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.ArrayList;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaDiscrepancyAnalysisRptData {
	
	private String supplierCode;
	
	private String itemCode;
	
	private List<Integer> discDataList;
	
	private Integer total;
	
	public CoaDiscrepancyAnalysisRptData() {
		super();
		
		this.supplierCode = "";
		this.itemCode ="";
		this.discDataList = new ArrayList<Integer>();
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public List<Integer> getDiscDataList() {
		return discDataList;
	}

	public void setDiscDataList(List<Integer> discDataList) {
		this.discDataList = discDataList;
	}
	
	

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}



	public static class DiscrepancyReportData
	{
		private String discrepancyName;
		private Integer cnt;
	
		public DiscrepancyReportData() {
			super();
		}

		public DiscrepancyReportData(String discrepancyName) {
			super();
			this.discrepancyName = discrepancyName;
		}

		public String getDiscrepancyName() {
			return discrepancyName;
		}

		public void setDiscrepancyName(String discrepancyName) {
			this.discrepancyName = discrepancyName;
		}

		public Integer getCnt() {
			return cnt;
		}

		public void setCnt(Integer cnt) {
			this.cnt = cnt;
		}

	}

}
