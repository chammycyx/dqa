package hk.org.ha.model.pms.dqa.biz;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.jboss.seam.contexts.Lifecycle;

public class SeamInterceptor {

	@AroundInvoke
	public Object process(InvocationContext invocation) throws Exception{
		try {
			Lifecycle.beginCall();
			return invocation.proceed();
		} finally {
			Lifecycle.endCall();
		}		
	}
}
