package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventoryStockOnhand;
import hk.org.ha.model.pms.dqa.vo.sample.DrugItemCodeModifyDate;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestStockOnhandRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestStockOnhandRptServiceBean implements SampleTestStockOnhandRptServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	private static final String UNCHECKED = "unchecked";
	
	private List<SampleTestInventoryStockOnhand> sampleTestInventoryStockOnhandList;
	
	@SuppressWarnings(UNCHECKED)
	private List<SampleMovement> retrieveSampleMovementDataList(){
		
			List<SampleMovement> dataList = new ArrayList<SampleMovement>();
		
			// Get the list of maximum modify date of each item code
			List<DrugItemCodeModifyDate> drugItemCodeModifyDateList = em.createNamedQuery("SampleMovement.findModifyDateByItemCode")
			    .getResultList();

			for (DrugItemCodeModifyDate drugItemCodeModifyDate : drugItemCodeModifyDateList) {
				List<SampleMovement> sampleMovementList = em.createNamedQuery("SampleMovement.findByItemCodeModifyDate")
					.setParameter("itemCode", drugItemCodeModifyDate.getItemCode() )
					.setParameter("modifyDate", drugItemCodeModifyDate.getModifyDate() )
					.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
				    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
			    	.getResultList();
				
				if ( sampleMovementList.size() > 0 ) {
					dataList.add(sampleMovementList.get(0));
				}
			}
		
		return dataList;
	}
	
    @SuppressWarnings(UNCHECKED)
	public void retrieveSampleMovementStockOnhandList(){
    	
    	sampleTestInventoryStockOnhandList = new ArrayList<SampleTestInventoryStockOnhand>();
    	
    	List<SampleMovement> sampleMovementList = retrieveSampleMovementDataList();
    	
    	if (sampleMovementList.size() > 0){
    		
	    	for (SampleMovement sampleMovement : sampleMovementList ){
	    		SampleTestInventoryStockOnhand tmpSampleTestInventory = constructSampleTestInventoryRptData(sampleMovement);
	    		
	    		sampleTestInventoryStockOnhandList.add(tmpSampleTestInventory);
	    	}
    	}
    }
   
	private SampleTestInventoryStockOnhand constructSampleTestInventoryRptData(SampleMovement sampleMovement) {

		SampleTestInventoryStockOnhand sampleTestInventory = new SampleTestInventoryStockOnhand();
		
		sampleTestInventory.setItemCode(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode());
		sampleTestInventory.setItemDesc(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getFullDrugDesc());
		sampleTestInventory.setOnHandQty(sampleMovement.getOnHandQty());
		sampleTestInventory.setBaseUnit(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getBaseUnit());
		sampleTestInventory.setSpecialCat(sampleMovement.getSampleTestSchedule().getSampleItem().getSpeicalCat().getDataValue());
		
		return sampleTestInventory;
	}
    
    public List<SampleTestInventoryStockOnhand> getSampleTestInventoryStockOnhandList(){
    	return sampleTestInventoryStockOnhandList;
    }
    
    @Remove
	public void destroy(){
    	sampleTestInventoryStockOnhandList = null;
	}

}