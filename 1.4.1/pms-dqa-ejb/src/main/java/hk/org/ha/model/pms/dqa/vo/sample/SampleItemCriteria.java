package hk.org.ha.model.pms.dqa.vo.sample;


import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleItemCriteria {
	
	private RecordStatus recordStatus;
	private String itemCode;
	private OrderType orderType;
	private String riskLevelCode;
	private String testCode;
	private String exclusionCode;
	
	
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	public String getRiskLevelCode() {
		return riskLevelCode;
	}
	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}
	public String getTestCode() {
		return testCode;
	}
	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}
	public String getExclusionCode() {
		return exclusionCode;
	}
	public void setExclusionCode(String exclusionCode) {
		this.exclusionCode = exclusionCode;
	}
}
