package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum SuspendFlag implements StringValuedEnum {
	
	Suspended("Y", "Suspended"),
	Unsuspended("N", "Unsuspended");	
	
    private final String dataValue;
    private final String displayValue;
        
    SuspendFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<SuspendFlag> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<SuspendFlag> getEnumClass() {
    		return SuspendFlag.class;
    	}
    }
}
