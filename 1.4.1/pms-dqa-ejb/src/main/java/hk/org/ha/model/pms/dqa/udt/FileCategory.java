package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum FileCategory implements StringValuedEnum {
	
	ALL("-", "All"),
	Original("O", "Original"),
	RevisedOriginal("R","Revised Original"),
	AlternativeOriginal("A", "Alternative Original"),
	SupplInfo("S", "Suppl. Info."),
	Batch("B", "Batch"),
	BatchOthers("T", "Batch (Others)");
	
    private final String dataValue;
    private final String displayValue;
        
    FileCategory(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<FileCategory> {

		private static final long serialVersionUID = 3953755632126477582L;

		@Override
    	public Class<FileCategory> getEnumClass() {
    		return FileCategory.class;
    	}
    }
}
