package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFull;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleItemFullRptServiceLocal {

	void retrieveSampleItemFullList();
	
	List<SampleItemFull> getSampleItemFullReportList();
	
	void destroy();
	
}