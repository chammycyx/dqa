package hk.org.ha.model.pms.dqa.persistence.sample;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SAMPLE_TEST_FILE")
@NamedQueries({
	@NamedQuery(name = "SampleTestFile.findByCriteria", query = "select o from SampleTestFile o where " +
		"(o.supplier.supplierCode = :supplierCode or :supplierCode is null)" +
		"and (o.itemCode = :itemCode or :itemCode is null)" +
		"and (o.manufacturer.companyCode = :manufCode or :manufCode is null)" +
		"and (o.pharmCompany.companyCode = :pharmCode or :pharmCode is null)" +
		"and (o.fileCat = :fileCat or :fileCatStr ='All' ) order by o.fileItem.createDate DESC"),
		
	@NamedQuery(name = "SampleTestFile.findForSampleTestScheduleConfirm", query = "select o from SampleTestFile o " +
				"where o.itemCode = :itemCode and o.supplier.supplierCode = :supplierCode " +
				"and o.manufacturer.companyCode = :manufCode and o.pharmCompany.companyCode = :pharmCode order by o.fileItem.createDate DESC, o.fileCat DESC"
			),
	
	@NamedQuery(name = "SampleTestFile.findByItemCodeSuppCodeManufCodePharmCode", query = "select o from SampleTestFile o " +
			"where o.itemCode = :itemCode and o.supplier.supplierCode = :supplierCode " +
			"and o.manufacturer.companyCode = :manufCode and o.pharmCompany.companyCode = :pharmCode order by o.fileItem.createDate DESC, o.fileCat DESC"
		),
	@NamedQuery(name = "SampleTestFile.findForValidation", query = "select o from SampleTestFile o " +
			"where o.itemCode = :itemCode and o.supplier.supplierCode = :supplierCode " +
			"and o.manufacturer.companyCode = :manufCode and o.pharmCompany.companyCode = :pharmCode " +
			"and o.fileCat = :fileCat and o.createDate >= :startDate and o.createDate < :endDate order by o.createDate desc"
		),
	@NamedQuery(name = "SampleTestFile.findBySampleTestFileCat", query = "select o from SampleTestFile o " +
			"where o.fileCat = :fileCat order by o.createDate desc"
		),
	@NamedQuery(name = "SampleTestFile.findBySampleTestFileCatSMP", query = "select o from SampleTestFile o " +
			"where o.fileCat = :fileCat " +
			"and o.supplier.supplierCode = :supplierCode " +
			"and o.manufacturer.companyCode = :manufacturerCode " +
			"and o.pharmCompany.companyCode = :pharmCompanyCode " +
			"order by o.createDate desc"
		),
	@NamedQuery(name = "SampleTestFile.findByItemSupplierManufacturerPharmCompanyFileCatList", query ="select o from SampleTestFile o where " +
			"o.itemCode = :itemCode " +
			"and o.supplier.supplierCode = :supplierCode " +
			"and o.manufacturer.companyCode = :manufacturerCode " +
			"and o.pharmCompany.companyCode = :pharmCompanyCode " +
			"and o.fileCat in :fileCatList order by o.createDate desc")
		
})
@Customizer(AuditCustomizer.class)
public class SampleTestFile extends VersionEntity{

	private static final long serialVersionUID = -8038574389342143971L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleTestDocSeq")
	@SequenceGenerator(name = "sampleTestDocSeq", sequenceName = "SEQ_SAMPLE_TEST_FILE", initialValue=10000)
	@Id
	@Column(name="SAMPLE_TEST_FILE_ID", nullable = false)
	private Long sampleTestFileId;
	
	@Converter(name = "SampleTestFile.SampleTestFileCat", converterClass = SampleTestFileCat.Converter.class )
	@Convert("SampleTestFile.SampleTestFileCat")
	@Column(name="FILE_CAT", length = 3, nullable = false)
	private SampleTestFileCat fileCat;
		
	@OneToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="FILE_ID")
	private FileItem fileItem;

	@Column(name="ITEM_CODE" , length = 6)
	private String itemCode;
	
	@Transient
	private DmDrug dmDrug = null;
	
	@Transient
	private boolean selected;
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null) {
			setDmDrug(DmDrugCacher.instance().getDrugByItemCode(itemCode));
		}
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SUPPLIER_CODE")
	private Supplier supplier;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MANUF_CODE", referencedColumnName="COMPANY_CODE")
	private Company manufacturer;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PHARM_COMPANY_CODE", referencedColumnName="COMPANY_CODE")
	private Company pharmCompany;
	
	public Long getSampleTestFileId() {
		return sampleTestFileId;
	}

	public void setSampleTestFileId(Long sampleTestFileId) {
		this.sampleTestFileId = sampleTestFileId;
	}

	
	public SampleTestFileCat getFileCat() {
		return fileCat;
	}

	public void setFileCat(SampleTestFileCat fileCat) {
		this.fileCat = fileCat;
	}

	public FileItem getFileItem() {
		return fileItem;
	}

	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setManufacturer(Company manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Company getManufacturer() {
		return manufacturer;
	}

	public Company getPharmCompany() {
		return pharmCompany;
	}

	public void setPharmCompany(Company pharmCompany) {
		this.pharmCompany = pharmCompany;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}
	
	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}
}
