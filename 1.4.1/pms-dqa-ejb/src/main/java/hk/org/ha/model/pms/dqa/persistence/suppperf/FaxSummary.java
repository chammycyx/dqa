package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_SUMMARY")
@Customizer(AuditCustomizer.class)
public class FaxSummary extends VersionEntity {

	private static final long serialVersionUID = -2109269509364493398L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxSummarySeq")
	@SequenceGenerator(name = "faxSummarySeq", sequenceName = "SEQ_FAX_SUMMARY", initialValue=10000)
	@Id
	@Column(name="FAX_SUMMARY_ID", nullable=false)
	private Long faxSummaryId;
	
	@Column(name="INIT_H_COUNT")
    private Integer initHCount;
	
	@Column(name="INIT_S_COUNT")
    private Integer initSCount;
	
	@Column(name="INTERIM_H_COUNT")
    private Integer interimHCount;
	
	@Column(name="INTERIM_S_COUNT")
    private Integer interimSCount;
	
	@Column(name="FINAL_H_COUNT")
    private Integer finalHCount;
	
	@Column(name="FINAL_S_COUNT")
    private Integer finalSCount;
	
	@Column(name="INIT_H_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date initHUpdateDate;
	
	@Column(name="INIT_S_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date initSUpdateDate;
	
	@Column(name="INTERIM_H_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date interimHUpdateDate;
	
	@Column(name="INTERIM_S_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date interimSUpdateDate;
	
	@Column(name="FINAL_H_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date finalHUpdateDate;
	
	@Column(name="FINAL_S_UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date finalSUpdateDate;

	public Long getFaxSummaryId() {
		return faxSummaryId;
	}

	public void setFaxSummaryId(Long faxSummaryId) {
		this.faxSummaryId = faxSummaryId;
	}

	public Integer getInitHCount() {
		return initHCount;
	}

	public void setInitHCount(Integer initHCount) {
		this.initHCount = initHCount;
	}

	public Integer getInitSCount() {
		return initSCount;
	}

	public void setInitSCount(Integer initSCount) {
		this.initSCount = initSCount;
	}

	public Integer getInterimHCount() {
		return interimHCount;
	}

	public void setInterimHCount(Integer interimHCount) {
		this.interimHCount = interimHCount;
	}

	public Integer getInterimSCount() {
		return interimSCount;
	}

	public void setInterimSCount(Integer interimSCount) {
		this.interimSCount = interimSCount;
	}

	public Integer getFinalHCount() {
		return finalHCount;
	}

	public void setFinalHCount(Integer finalHCount) {
		this.finalHCount = finalHCount;
	}

	public Integer getFinalSCount() {
		return finalSCount;
	}

	public void setFinalSCount(Integer finalSCount) {
		this.finalSCount = finalSCount;
	}

	public Date getInitHUpdateDate() {
		return (initHUpdateDate!=null)?new Date(initHUpdateDate.getTime()):null;
	}

	public void setInitHUpdateDate(Date initHUpdateDate) {
		if (initHUpdateDate!=null) {
			this.initHUpdateDate = new Date(initHUpdateDate.getTime());
		} else {
			this.initHUpdateDate = null;
		}
	}

	public Date getInitSUpdateDate() {
		return (initSUpdateDate!=null)?new Date(initSUpdateDate.getTime()):null;
	}

	public void setInitSUpdateDate(Date initSUpdateDate) {
		if (initSUpdateDate!=null) {
			this.initSUpdateDate = new Date(initSUpdateDate.getTime());
		} else {
			this.initSUpdateDate = null;
		}
	}

	public Date getInterimHUpdateDate() {
		return (interimHUpdateDate!=null)?new Date(interimHUpdateDate.getTime()):null;
	}

	public void setInterimHUpdateDate(Date interimHUpdateDate) {
		if (interimHUpdateDate!=null) {
			this.interimHUpdateDate = new Date(interimHUpdateDate.getTime());
		} else {
			this.interimHUpdateDate = null;
		}
	}

	public Date getInterimSUpdateDate() {
		return (interimSUpdateDate!=null)?new Date(interimSUpdateDate.getTime()):null;
	}

	public void setInterimSUpdateDate(Date interimSUpdateDate) {
		if (interimSUpdateDate!=null) {
			this.interimSUpdateDate = new Date(interimSUpdateDate.getTime());
		} else {
			this.interimSUpdateDate = null;
		}
	}

	public Date getFinalHUpdateDate() {
		return (finalHUpdateDate!=null)?new Date(finalHUpdateDate.getTime()):null;
	}

	public void setFinalHUpdateDate(Date finalHUpdateDate) {
		if (finalHUpdateDate!=null) {
			this.finalHUpdateDate = new Date(finalHUpdateDate.getTime());
		} else {
			this.finalHUpdateDate = null;
		}
	}

	public Date getFinalSUpdateDate() {
		return (finalSUpdateDate!=null)?new Date(finalSUpdateDate.getTime()):null;
	}

	public void setFinalSUpdateDate(Date finalSUpdateDate) {
		if (finalSUpdateDate!=null) {
			this.finalSUpdateDate = new Date(finalSUpdateDate.getTime());
		} else {
			this.finalSUpdateDate = null;
		}
	}
	
}
