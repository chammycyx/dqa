package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.vo.coa.MissingCoaItemRptData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface MissingCoaItemRptServiceLocal {
	
	void retrieveMissingCoaItemRptList();
	
	List<MissingCoaItemRptData> getMissingCoaItemRptList();
	
	String getReportName();
	
	String getReportParameters();
	
	String getRetrieveDate();
	
	void destroy();

}
