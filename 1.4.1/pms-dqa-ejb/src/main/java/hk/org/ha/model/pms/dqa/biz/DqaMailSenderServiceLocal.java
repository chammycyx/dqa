package hk.org.ha.model.pms.dqa.biz;

import java.util.Map;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;

import javax.ejb.Local;

@Local
public interface DqaMailSenderServiceLocal {
	
	void sendEmail(LetterTemplate letterTemplate);
	
	@SuppressWarnings("unchecked")
	void sendBackupInEmail(final Object eaibackupInMessage, final Map attHashMap, String backupInEmail);
	
	void destroy();
}
