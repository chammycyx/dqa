package hk.org.ha.model.pms.dqa.persistence.delaydelivery;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.delaydelivery.DelayDeliveryRptFormStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;


@Entity
@Table(name = "DLD_RPT_FORM_VER")
@Customizer(AuditCustomizer.class)
public class DelayDeliveryRptFormVer extends VersionEntity {
	
	private static final long serialVersionUID = 1331765457575707036L;

	@Id
	@Column(name="DLD_RPT_FORM_VER_ID",nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "delayDeliveryRptFormVerSeq")
	@SequenceGenerator(name="delayDeliveryRptFormVerSeq", sequenceName="SEQ_DLD_RPT_FORM_VER", initialValue=10000)
	private Long delayDeliveryRptFormVerId;
	
	@Converter(name = "DelayDeliveryRptFormVer.status", converterClass = DelayDeliveryRptFormStatus.Converter.class)
	@Convert("DelayDeliveryRptFormVer.status")
	@Column(name="STATUS", length=1, nullable=false)
	private DelayDeliveryRptFormStatus status;
	
	@Column(name="REASON", length=500)
	private String reason;
	
	@ManyToOne
	@JoinColumn(name="REF_NUM")
	private DelayDeliveryRptForm delayDeliveryRptForm;
	
	public Long getDelayDeliveryRptFormVerId() {
		return delayDeliveryRptFormVerId;
	}
	public void setDelayDeliveryRptFormVerId(Long delayDeliveryRptFormVerId) {
		this.delayDeliveryRptFormVerId = delayDeliveryRptFormVerId;
	}
	public DelayDeliveryRptForm getDelayDeliveryRptForm() {
		return delayDeliveryRptForm;
	}
	public void setDelayDeliveryRptForm(DelayDeliveryRptForm delayDeliveryRptForm) {
		this.delayDeliveryRptForm = delayDeliveryRptForm;
	}
	public DelayDeliveryRptFormStatus getStatus() {
		return status;
	}
	public void setStatus(DelayDeliveryRptFormStatus status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
}
