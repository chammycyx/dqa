package hk.org.ha.model.pms.dqa.persistence.delaydelivery;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;
@Entity
@Table(name = "DLD_RPT_FORM_PO")
@Customizer(AuditCustomizer.class)
public class DelayDeliveryRptFormPo extends VersionEntity {
	private static final long serialVersionUID = -5765369536097154482L;
	
	
	@Id
	@Column(name = "DLD_RPT_FORM_PO_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "delayDeliveryRptFormPoSeq")
	@SequenceGenerator(name = "delayDeliveryRptFormPoSeq", sequenceName = "SEQ_DLD_RPT_FORM_PO", initialValue = 10000)
	private Long delayDeliveryRptFormPoId;
	
	@Column(name = "PO_NUM", nullable = false)
	private Long poNum;
	
	@Column(name = "APPROVE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	@Column(name = "ORDER_QTY", nullable = false)
	private Long orderQty;
	
	@Column(name = "OUTSTAND_QTY", nullable = false)
	private Long outstandQty;
	
	@Column(name = "PACK_SIZE")
	private String packSize;
	
	@Column(name = "QUOTATION_LEAD_TIME", nullable = false)
	private Long quotationLeadTime;
	
	@Column(name = "SUPPLIER_REPLY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date supplierReplyDate;
	
	@OneToOne(cascade=CascadeType.PERSIST)	
	@JoinColumn(name = "FILE_ID")
	private FileItem fileId;
	
	@ManyToOne
	@JoinColumn(name="REF_NUM")
	private DelayDeliveryRptForm delayDeliveryRptForm;
	
	
	public Long getDelayDeliveryRptFormPoId() {
		return delayDeliveryRptFormPoId;
	}
	public void setDelayDeliveryRptFormPoId(Long delayDeliveryRptFormPoId) {
		this.delayDeliveryRptFormPoId = delayDeliveryRptFormPoId;
	}
	public FileItem getFileId() {
		return fileId;
	}
	public void setFileId(FileItem fileId) {
		this.fileId = fileId;
	}
	public Long getPoNum() {
		return poNum;
	}
	public void setPoNum(Long poNum) {
		this.poNum = poNum;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public Long getOutstandQty() {
		return outstandQty;
	}
	public void setOutstandQty(Long outstandQty) {
		this.outstandQty = outstandQty;
	}
	public Long getQuotationLeadTime() {
		return quotationLeadTime;
	}
	public void setQuotationLeadTime(Long quotationLeadTime) {
		this.quotationLeadTime = quotationLeadTime;
	}
	public Date getSupplierReplyDate() {
		return supplierReplyDate;
	}
	public void setSupplierReplyDate(Date supplierReplyDate) {
		this.supplierReplyDate = supplierReplyDate;
	}
	public Long getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(Long orderQty) {
		this.orderQty = orderQty;
	}
	public DelayDeliveryRptForm getDelayDeliveryRptForm() {
		return delayDeliveryRptForm;
	}
	public void setDelayDeliveryRptForm(DelayDeliveryRptForm delayDeliveryRptForm) {
		this.delayDeliveryRptForm = delayDeliveryRptForm;
	}
	public String getPackSize() {
		return packSize;
	}
	public void setPackSize(String packSize) {
		this.packSize = packSize;
	}
	
	
}
