package hk.org.ha.model.pms.dqa.cacher;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.fmk.pms.util.AbstractCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsDqaServiceJmsRemote;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@AutoCreate
@Name("dmDrugCacher")
@Scope(ScopeType.APPLICATION)
public class DmDrugCacher extends BaseCacher implements DmDrugCacherInf{
	
	@In
	private DmsDqaServiceJmsRemote dmsDqaServiceProxy;

	private InnerCacher cacher;
	
	public void load() {
		 getDrugList();
	}
	
	public void clear() {
		synchronized (this) {
			cacher = null;
		}
	}
	         
	public DmDrug getDrugByItemCode(String itemCode) {
		return (DmDrug) this.getCacher().get(itemCode);
	}
		
	public List<DmDrug> getDrugListByItemCode(String prefixItemCode) {
		List<DmDrug> ret = new ArrayList<DmDrug>();
		for (DmDrug dmDrug : getDrugList()) {
			if(dmDrug!=null)
			{
				if (dmDrug.getItemCode().startsWith(prefixItemCode)) {
					ret.add(dmDrug);
				}
			}
		}
		return ret;
	}
	
	public void initDrugCache(List<DmDrug> dmDrugList) {
		synchronized (this) {
			this.clear();
			this.initDrugList(dmDrugList);
		}
	}	
	
	public List<DmDrug> getDrugList() {
		return initDrugList(null);
	}

	public List<DmDrug> initDrugList(List<DmDrug> dmDrugList) {
		synchronized (this)
		{		
			InnerCacher cacher = this.getCacher();
			cacher.setDmDrugCollection(dmDrugList);
			return (List<DmDrug>) cacher.getAll();
		}
	}
	
	private InnerCacher getCacher() {
		synchronized (this) {			
			if (cacher == null) {
				cacher = new InnerCacher(this.getExpireTime());
			}
			return cacher;
		}
	}

	private class InnerCacher extends AbstractCacher<String, DmDrug>
	{		
		private Collection<DmDrug> dmDrugList;
		
		public void setDmDrugCollection(Collection<DmDrug> dmDrugList) {
			this.dmDrugList = dmDrugList;
		}		
		
        public InnerCacher(int expireTime) {
              super(expireTime);
        }
		
		public DmDrug create(String itemCode) {	
			this.getAll();
			return this.internalGet(itemCode);
		}

		public Collection<DmDrug> createAll() {
			if (dmDrugList != null) {
				Collection<DmDrug> tempDmDrugList = dmDrugList;
				dmDrugList = null;
				return tempDmDrugList;
			} else {
				return dmsDqaServiceProxy.retrieveDmDrugList();
			}
		}

		public String retrieveKey(DmDrug dmDrug) {
			return dmDrug.getItemCode();
		}		
	}
	
	public static DmDrugCacherInf instance()
	{
		  if (!Contexts.isApplicationContextActive())
	      {
	         throw new IllegalStateException("No active application scope");
	      }
	      return (DmDrugCacherInf) Component.getInstance("dmDrugCacher", ScopeType.APPLICATION);
    }	
}
