package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CancelSchedReason implements StringValuedEnum{
	
	NullValue("N", "Null value"),
	OpertionalInfeasible("CR1", "Opertional Infeasible"),
	ReferenceStandardNotAvailable("CR2", "Reference Standard Not Available"),
	DocumentNotAvailable("CR3", "Document Not Available"),
	LaboratoryTechnicalInfeasible("CR4", "Laboratory Technical Infeasible"),
	DangerousDrug("CR6", "Dangerous Drug"),
	ExpensiveItem("CR7", "Expensive Item"),
	Exclusion("CR8", "Exclusion"),
	Others("CR5", "Others");

	private final String dataValue;
	private final String displayValue;

	CancelSchedReason(final String dataValue, final String displayValue){
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
	public String getDataValue() {
		return this.dataValue;
	}

	@Override
	public String getDisplayValue() {
		return this.displayValue;
	}    

	public static class Converter extends StringValuedEnumConverter<CancelSchedReason> {

		private static final long serialVersionUID = -8786795635285586670L;

		@Override
		public Class<CancelSchedReason> getEnumClass() {
			return CancelSchedReason.class;
		}
	}

}
