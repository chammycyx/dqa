package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmProblemCriteria {
	
	private Institution institution;
	private String itemCode;
	private OrderTypeAll orderType;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromProblemDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toProblemDate;
	private String problemNum;
	private ProblemClassification problemClassification;
	private ProblemStatus problemStatus;
	private String caseNum;
	private RecordStatus recordStatus;
	
	public Institution getInstitution() {
		return institution;
	}
	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public OrderTypeAll getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public Date getFromProblemDate() {
		return (fromProblemDate != null) ? new Date(fromProblemDate.getTime()) :null ;
	}
	public void setFromProblemDate(Date fromProblemDate) {
		if (fromProblemDate != null) {
			this.fromProblemDate = new Date(fromProblemDate.getTime());
		}
	}
	public Date getToProblemDate() {
		return (toProblemDate != null) ? new Date(toProblemDate.getTime()) :null ;
	}
	public void setToProblemDate(Date toProblemDate) {
		if (toProblemDate != null) {
			this.toProblemDate = new Date(toProblemDate.getTime());
		}
	}
	
	public String getProblemNum() {
		return problemNum;
	}
	public void setProblemNum(String problemNum) {
		this.problemNum = problemNum;
	}
	public ProblemClassification getProblemClassification() {
		return problemClassification;
	}
	public void setProblemClassification(
			ProblemClassification problemClassification) {
		this.problemClassification = problemClassification;
	}
	public ProblemStatus getProblemStatus() {
		return problemStatus;
	}
	public void setProblemStatus(ProblemStatus problemStatus) {
		this.problemStatus = problemStatus;
	}
	
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

}
