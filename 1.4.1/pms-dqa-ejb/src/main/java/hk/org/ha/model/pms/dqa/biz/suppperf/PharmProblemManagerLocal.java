package hk.org.ha.model.pms.dqa.biz.suppperf;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFormData;

import javax.ejb.Local;

@Local
public interface PharmProblemManagerLocal {
	void setUploadPath(String uploadPathIn);
	void setSuppPerfNotificationToEmail(String suppPerfNotificationToEmail);
	PharmProblem addPharmProblem();
	boolean createPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean);
	PharmProblem createUpdateDraftPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList);
	PharmProblem updateToNullProblemStausPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean);
	Contract constructContractSave(PharmProblem pharmProblemIn);
	PharmProblemFormData retrievePharmProblemByPharmProblem(PharmProblem pharmProblemIn);
}
