package hk.org.ha.model.pms.dqa.util;

import java.text.SimpleDateFormat;
  
import java.util.Date;   
import java.util.Locale;
  
import javax.xml.bind.annotation.adapters.XmlAdapter;   

public class DateAdapter extends XmlAdapter<String, Date> {   
	
	private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
	
    public Date unmarshal(String v) {
        SimpleDateFormat inputFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        try {
	        Date date = inputFormatter.parse(v);
	        return new Date(date.getTime());
        } catch (Exception e) {
        	return null;
        }
    }   
  
    public String marshal(Date v) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        return formatter.format(v);
    }     
}  