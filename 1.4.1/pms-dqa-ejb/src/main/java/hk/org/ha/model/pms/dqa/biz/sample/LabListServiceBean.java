package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("labListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LabListServiceBean implements LabListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Lab> labList;
	
	@SuppressWarnings("unchecked")
	public void retrieveLabList(){
		logger.debug("retrieveLabList");
		
		labList = em.createNamedQuery("Lab.findAll")
			.getResultList();
	}
	
	
	@Remove
	public void destroy(){
		if (labList != null){
			labList = null;
		}
	}
	
}
