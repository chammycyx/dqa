package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PROBLEM_NATURE_CAT")
@NamedQueries({
	@NamedQuery(name = "ProblemNatureCat.findByProblemNatureCatId", query = "select o from ProblemNatureCat o where o.problemNatureCatId = :problemNatureCatId and o.recordStatus = :recordStatus order by o.displayOrder, o.catDesc "),
	@NamedQuery(name = "ProblemNatureCat.findByCatDescParamId", query = "select o from ProblemNatureCat o join o.problemNatureParam p where o.catDesc = :catDesc and p.problemNatureParamId = :problemNatureParamId and o.recordStatus = :recordStatus and p.recordStatus = :recordStatus order by o.displayOrder, o.catDesc "),
	@NamedQuery(name = "ProblemNatureCat.findByParamIdRecordStatus", query = "select o from ProblemNatureCat o join o.problemNatureParam p where p.problemNatureParamId = :problemNatureParamId and o.recordStatus = :recordStatus and p.recordStatus = :recordStatus order by o.displayOrder, o.catDesc "),
	@NamedQuery(name = "ProblemNatureCat.deleteByProblemNatureParamId", query = "update ProblemNatureCat o set o.recordStatus = :recordStatus where o.problemNatureParam.problemNatureParamId = :problemNatureParamId "),
	@NamedQuery(name = "ProblemNatureCat.deleteByProblemNatureCatId", query = "update ProblemNatureCat o set o.recordStatus = :recordStatus where o.problemNatureCatId = :problemNatureCatId ")
})
@Customizer(AuditCustomizer.class)
public class ProblemNatureCat extends VersionEntity {

	private static final long serialVersionUID = 6837664064964402597L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "problemNatureCatSeq")
	@SequenceGenerator(name = "problemNatureCatSeq", sequenceName = "SEQ_PROBLEM_NATURE_CAT", initialValue=10000)
	@Id
	@Column(name="PROBLEM_NATURE_CAT_ID", nullable=false)
	private Long problemNatureCatId;

	@Column(name="CAT_DESC", length=100, nullable=false)
	private String catDesc;
	
	@Column(name="DISPLAY_ORDER", nullable=false)
    private Integer displayOrder;
	
	@Converter(name = "ProblemNatureCat.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("ProblemNatureCat.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@ManyToOne
	@JoinColumn(name="PROBLEM_NATURE_PARAM_ID")
	private ProblemNatureParam problemNatureParam;
	

	public Long getProblemNatureCatId() {
		return problemNatureCatId;
	}

	public void setProblemNatureCatId(Long problemNatureCatId) {
		this.problemNatureCatId = problemNatureCatId;
	}

	public String getCatDesc() {
		return catDesc;
	}

	public void setCatDesc(String catDesc) {
		this.catDesc = catDesc;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public ProblemNatureParam getProblemNatureParam() {
		return problemNatureParam;
	}

	public void setProblemNatureParam(ProblemNatureParam problemNatureParam) {
		this.problemNatureParam = problemNatureParam;
	}

}
