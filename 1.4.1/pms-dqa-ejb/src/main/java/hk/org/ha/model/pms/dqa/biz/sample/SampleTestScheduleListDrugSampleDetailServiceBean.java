package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;


import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleListDrugSampleDetailService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleListDrugSampleDetailServiceBean implements SampleTestScheduleListDrugSampleDetailServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestSchedule> sampleTestScheduleDetailList;
	
	@In(create =true)
	private SampleMovementServiceLocal sampleMovementService;
	
	
	private static final String UNCHECKED = "unchecked";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String O_CONTRACT = "o.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.contract.supplier";
	private static final String O_CONTRACT_MANUFACTURER = "o.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.contract.pharmCompany";
	private static final String O_SAMPLE_ITEM = "o.sampleItem";
	private static final String O_SAMPLE_ITEM_RISK_LEVEL = "o.sampleItem.riskLevel";
	private static final String O_INSTITUTION = "o.institution";
	private static final String O_LAB = "o.lab";
	
	private boolean success;
	
	private String errorCode = null;
	
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForDrugSampleByInstitutionItem(String institutionCode, String itemCode){
		logger.debug("retrieveSampleTestScheduleListForDrugSampleByInstitutionItem" );
		errorCode=null;
		sampleTestScheduleDetailList= em.createNamedQuery("SampleTestSchedule.findByInstitutionItemScheduleStatusRecordStatus")
			.setParameter("institutionCode", institutionCode)
			.setParameter("itemCode", itemCode)
			.setParameter("scheduleStatus", ScheduleStatus.DrugSample)
			.setParameter("recordStatus", RecordStatus.Active)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			//.setHint(QueryHints.FETCH, O_CONTRACT_DRUG_ITEM) // need tuning
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			//.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_DRUG_ITEM)
			.setHint(QueryHints.FETCH, O_INSTITUTION)
			.setHint(QueryHints.FETCH, O_LAB)
			.getResultList();
		
		if(sampleTestScheduleDetailList==null || sampleTestScheduleDetailList.size()==0)
		{
			errorCode = "0008";
		}
		else
		{
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleDetailList ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				sampleTestScheduleFind.getContract().getSupplier().getContact();
				sampleTestScheduleFind.getContract().getManufacturer();
				sampleTestScheduleFind.getContract().getManufacturer().getContact();
				sampleTestScheduleFind.getContract().getPharmCompany();
				sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
		}
	}
	
	
	@SuppressWarnings(UNCHECKED)
	public void updateSampleTestScheduleForDrugSample(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("updateSampleTestScheduleForDrugSample" );
		success = false;
		errorCode = null;
		
		for ( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleListIn){
			sampleTestScheduleFind = getLazySampleTestSchedule(sampleTestScheduleFind);
			sampleTestScheduleFind.setScheduleStatus(ScheduleStatus.LabTest);
			em.merge(sampleTestScheduleFind);
			em.flush();
			sampleMovementService.createSampleMovementForNormalSchedule(sampleTestScheduleFind, ScheduleStatus.DrugSample);
		}
		
		success = true;	
	}
	
	public boolean isSuccess() {
		return success;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}

	@Remove
	public void destroy() {
		if (sampleTestScheduleDetailList !=null){
			sampleTestScheduleDetailList = null;
		}
	}
}
