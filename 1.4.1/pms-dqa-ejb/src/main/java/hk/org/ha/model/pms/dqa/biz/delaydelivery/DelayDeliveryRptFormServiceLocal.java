package hk.org.ha.model.pms.dqa.biz.delaydelivery;

import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface DelayDeliveryRptFormServiceLocal {
	
	String createUpdateDelayDeliveryRptForm(DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			Boolean sendEmailBoolean);
	
	String createDelayDeliveryRptForm(DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
								List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactList,
								List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
								Boolean sendEmailBoolean);
	
	String updateDelayDeliveryRptForm(DelayDeliveryRptFormData delayDeliveryRptFormDataIn, 
				List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactList,
				List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
				Boolean sendEmailBoolean);
	DelayDeliveryRptFormData updateDelayDeliveryRptFormStatus(DelayDeliveryRptFormData delayDeliveryRptFormDataIn);

	boolean deleteDelayDeliveryRptForm(DelayDeliveryRptFormData delayDeliveryRptFormDataIn, String deleteReason);
	
	void destroy();
}
