package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.util.StringArray;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.vo.coa.MissingCoaItemRptData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("missingCoaItemRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MissingCoaItemRptServiceBean implements MissingCoaItemRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String REPORT_NAME = "Missing COA Item Report";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	private static final String TODAY_FORMAT = "yyyy-MM-dd";
	
	private String reportParameters = "";
	private String retrieveDate;
	private SimpleDateFormat sdf;
	private Date today;
	
	private List<MissingCoaItemRptData> missingCoaItemRptList;
		
			
	@SuppressWarnings("unchecked")
	public void retrieveMissingCoaItemRptList() {
		logger.debug("retrieveMissingCoaItemRptList");
		
		Map<String, Contract> map;
		
		String sqlStr = "select o " +
				"from Contract o " +
				"where o.moduleType =:contractModuleType " +
				"and (o.contractType = 'C' or o.contractType = 'Q') " +
				"and o.suspendFlag =:suspendFlag and o.startDate <= :today and o.endDate >= :today " +
				"and not exists (select c.contractId " +
				"				 from Contract c " +
				"				 where c.moduleType = :coaModuleType " +
				"				 and c.contractNum = o.contractNum " + 
				"                and c.itemCode = o.itemCode " +
				"				 and c.supplier.supplierCode = o.supplier.supplierCode " +
				"				 and c.manufacturer.companyCode = o.manufacturer.companyCode " +
				"				 and c.pharmCompany.companyCode = o.pharmCompany.companyCode) " +
				"order by o.contractNum, o.contractType, o.itemCode, o.supplier.supplierCode, o.manufacturer.companyCode, o.pharmCompany.companyCode, o.createDate";
		
		try {
			SimpleDateFormat todayFormat = new SimpleDateFormat(TODAY_FORMAT);
			today = todayFormat.parse(todayFormat.format(new Date()));
		} catch (ParseException e) {
			logger.error("Parse Exception Error", e);
		} 
	
		List<Contract> reportList = (List<Contract>) em.createQuery(sqlStr)
			.setParameter("contractModuleType", ModuleType.All)
			.setParameter("coaModuleType", ModuleType.COA)
			.setParameter("suspendFlag", SuspendFlag.Unsuspended)
			.setParameter("today", today)
			.setHint(QueryHints.FETCH, "o.supplier")
			.setHint(QueryHints.FETCH, "o.manufacturer")
			.setHint(QueryHints.FETCH, "o.pharmCompany")
			.getResultList();
		
		String key;
		map = new TreeMap<String, Contract>();
		for (Contract o : reportList){
			key = o.getContractNum() +
				  o.getContractType()+
				  o.getItemCode()+
				  o.getSupplier().getSupplierCode()+
				  o.getManufacturer().getCompanyCode()+
				  o.getPharmCompany().getCompanyCode();
			
				if ( map.get(key) == null ) {
					map.put(key, o);
				}
		}

		MissingCoaItemRptData ocpData;
		missingCoaItemRptList = new ArrayList<MissingCoaItemRptData>();

		Iterator<Entry<String, Contract>> iterator = map.entrySet().iterator();
		Contract contract;
		while ( iterator.hasNext() ) {
			contract = iterator.next().getValue();
			ocpData = new MissingCoaItemRptData();
			
			ocpData.setContractNum(contract.getContractNum());
			ocpData.setOrderType(contract.getContractType());
			ocpData.setItemCode(contract.getItemCode());
			ocpData.setItemDesc((contract.getDmDrug() == null?"":contract.getDmDrug().getFullDrugDesc()));
			ocpData.setSupplierCode(contract.getSupplier().getSupplierCode());
			ocpData.setManufacturerCode(contract.getManufacturer().getCompanyCode());
			ocpData.setPharmCompanyCode(contract.getPharmCompany().getCompanyCode());
			ocpData.setUploadDate(contract.getCreateDate());
			ocpData.setStartDate(contract.getStartDate());
			ocpData.setEndDate(contract.getEndDate());
			
			missingCoaItemRptList.add(ocpData);
		}
		
		logger.debug("missingCoaItemRptList report list size : #0", missingCoaItemRptList.size());
	}
	
	public List<MissingCoaItemRptData> getMissingCoaItemRptList() {
		return missingCoaItemRptList;
	}

	public String getReportName() {
		return REPORT_NAME;
	}


	public String getReportParameters() {
		return reportParameters;
	}


	public String getRetrieveDate() {
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}


	@Remove
	public void destroy(){
		
	}
	
}
