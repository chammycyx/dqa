package hk.org.ha.model.pms.dqa.biz.coa;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FileItemManagerLocal;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.FileCategory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;

import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("coaFileFolderManager")
@MeasureCalls
public class CoaFileFolderManagerBean implements CoaFileFolderManagerLocal {

	private static final String FILE_PATH_PREFIX ="COA";

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;

	@In(create=true)
	private CoaFileFolderListForFileItemMaintManagerLocal coaFileFolderListForFileitemMaintManager;

	@In(create=true)
	private FileItemManagerLocal fileItemManager;
	
	private String uploadPath;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public CoaFileFolderResult upload(byte[] bytes, String fileIn, CoaFileFolder coaFileFolderIn) 
	{  
		boolean success = false;
		boolean duplicated = true;
		String physicalFilePath = null;

		String dbFilePath;
		String diskFilePath;
		
		List<CoaFileFolder> filelist = coaFileFolderListForFileitemMaintManager.retrieveCoaFileFolderForFileItemMaint(coaFileFolderIn);

		CoaBatch tmpCoaBatch = coaFileFolderIn.getCoaBatch(); 
		CoaItem tmpCoaItem = tmpCoaBatch.getCoaItem(); 
		Contract tmpContract = (tmpCoaItem==null?null:tmpCoaItem.getContract());
		String tmpItemCode = (tmpContract!=null?tmpContract.getItemCode():null); 
		
		if (filelist.size()==0){
			logger.debug("CoaFileFolder list is empty");
			duplicated = false;			
		}
		
		if (duplicated && (coaFileFolderIn.getFileCat() == FileCategory.Batch || coaFileFolderIn.getFileCat() == FileCategory.BatchOthers))
		{
			logger.debug("Allow File Category Batch / Batch (Others) to input");
			duplicated = false;		
		}
		
		if (!fileIn.trim().equals("")){
			logger.debug("fileIn is empty");
		}

		if(!duplicated && !fileIn.trim().equals(""))
		{ 
			logger.debug("Upload the file");
			dbFilePath = "/"+ FILE_PATH_PREFIX ; 

			if (tmpCoaItem != null && tmpItemCode != null ){
				Calendar cal=Calendar.getInstance();
				cal.setTime(tmpCoaItem.getCreateDate());

				dbFilePath = dbFilePath +"/"+ tmpCoaItem.getOrderType().getDataValue()+"/"+cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+tmpItemCode;		
			}
			diskFilePath = uploadPath +dbFilePath;
			File fDir = new File(diskFilePath);
			if (!fDir.exists()){
				success = fDir.mkdirs();
			}
			
			File f = new File(diskFilePath,genFileNameWithTimestamp(fileIn));
			String tmpfile = "";
			if (fDir.exists()) {
				while (f.exists()){	
					tmpfile = genFileNameWithTimestamp(fileIn);
					f = new File(diskFilePath,tmpfile);
				}
				
				if (fileItemManager.uploadFile(f, bytes)){
					physicalFilePath = dbFilePath + "/" + f.getName();
					success = true;
				}
			}
			return new CoaFileFolderResult(physicalFilePath, success, duplicated);
			
		} else {
			return new CoaFileFolderResult(null, success, duplicated);
		}
	}
	
	private String genFileNameWithTimestamp(String fileIn)
	{
		String tmpfile;
		String ext = (fileIn.lastIndexOf('.')==-1)?"":fileIn.substring(fileIn.lastIndexOf('.')+1,fileIn.length());
		String fileNewName = fileIn.substring( 0, (fileIn.lastIndexOf('.')==-1?(fileIn.length()-1):fileIn.lastIndexOf('.')) );	   
		tmpfile = fileNewName+ "." + sdf.format(new Date()) +"."+ext;
		return tmpfile;
	}
	
	public String doUploadForEmail( byte[] bytes, String fileIn, CoaFileFolder coaFileFolderIn ) {
		logger.debug( "doUploadForEmail #0 #1", coaFileFolderIn.getCoaFileFolderId(), fileIn );
		CoaFileFolderResult coaFileFolderResult = upload( bytes, fileIn, coaFileFolderIn );
		return coaFileFolderResult.physicalFilePath;
	}
	
	public void createCoaFileFolderForEmail( CoaFileFolder coaFileFolderIn ) {
		logger.debug("createCoaFileFolderForEmail #0", coaFileFolderIn.getCoaFileFolderId());
		createCoaFileFolder( coaFileFolderIn );
	}
	
	public boolean createCoaFileFolder(CoaFileFolder coaFileFolderin){
		logger.debug("createCoaFileFolder");		
		boolean success = false;
		//update batch to batch order if batch num and batch category already exists
		if (coaFileFolderin.getFileCat() == FileCategory.Batch)
		{
			CoaFileFolder orignalCoaFileFolder = retrieveBatchCategoryCoaFileFolder(coaFileFolderin);
			if (orignalCoaFileFolder != null)
			{
				orignalCoaFileFolder.setFileCat(FileCategory.BatchOthers);
				em.merge(orignalCoaFileFolder);
				em.flush();
			}
		}
		
		em.persist(coaFileFolderin.getFileItem());
		em.persist(coaFileFolderin);
		em.flush();		
		success = true;
		return success;
	}
	
	@SuppressWarnings("unchecked")
	private CoaFileFolder retrieveBatchCategoryCoaFileFolder(CoaFileFolder coaFileFolderIn)
	{
		List<CoaFileFolder> coaFileFolderList = em.createQuery("select o from CoaFileFolder o" +
													" where o.coaBatch.batchNum = :batchNum" +
													" and o.fileCat = :fileCat" +
													" and o.coaBatch.coaItem = :coaItem")
													.setParameter("batchNum", coaFileFolderIn.getCoaBatch().getBatchNum())
													.setParameter("fileCat", FileCategory.Batch)
													.setParameter("coaItem", coaFileFolderIn.getCoaBatch().getCoaItem())
													.setHint(QueryHints.BATCH, "o.coaBatch.coaItem")
													.getResultList();
		
		if (!coaFileFolderList.isEmpty())
		{
			return coaFileFolderList.get(0);
		}
		
		return null;
	}
	
	public CoaFileFolder updateCoaFileFolderForEmail( CoaFileFolder coaFileFolderIn ) {
		logger.debug("updateCoaFileFolderForEmail #0", coaFileFolderIn.getCoaFileFolderId());
		CoaFileFolder folder;
		em.merge(coaFileFolderIn.getFileItem());
		folder = em.merge(coaFileFolderIn);
		em.flush();
		return folder;
	}
	
	public boolean deletePhysicalFile(String fileName) 
	{  
		try{  
			logger.debug("deletePhysicalFile :" + (uploadPath + fileName) );
			File f = new File(uploadPath,fileName);
			
			boolean success = fileItemManager.deleteFile(f);
			logger.debug("delete file success: #0", success);
			return success;
		}catch(Exception e){
			logger.error("delete file error : #0",e);
			return false;
		}
	}
	
	public static class CoaFileFolderResult
	{
		private String physicalFilePath;
		private boolean success;
		private boolean duplicate;
		
		public CoaFileFolderResult(String physicalFilePathIn, boolean successIn, boolean duplicateIn)
		{
			this.physicalFilePath = physicalFilePathIn;
			this.success = successIn;
			this.duplicate = duplicateIn;
		}

		public String getPhysicalFilePath() {
			return physicalFilePath;
		}

		public boolean isSuccess() {
			return success;
		}

		public boolean isDuplicate() {
			return duplicate;
		}
	}
}
