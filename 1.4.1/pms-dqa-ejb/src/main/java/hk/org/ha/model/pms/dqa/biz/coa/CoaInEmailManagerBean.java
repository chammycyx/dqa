package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.udt.coa.InEmailRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaInEmailManager")
@MeasureCalls
public class CoaInEmailManagerBean implements CoaInEmailManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	public CoaInEmail createCoaInEmail( ValidateResultFlag validateSubjectResult, EmailLog emailLog ) {
		logger.debug("createCoaInEmail #0 #1", validateSubjectResult, emailLog.getEmailId());
				
		CoaInEmail inEmail = new CoaInEmail();		
		if( validateSubjectResult == ValidateResultFlag.Fail ) {
			inEmail.setRemarkType( InEmailRemarkType.InvalidEmailSubject );
		} else {
			inEmail.setRemarkType( InEmailRemarkType.NullValue );
		}
		inEmail.setEmailLog( emailLog );		
		inEmail.setResultFlag(validateSubjectResult);		
		em.persist( inEmail );		
		em.flush();
		
		return inEmail;
	}
	
	public void updateCoaInEmail(CoaInEmail inEmail){
		logger.debug("updateCoaInEmail");
		
		em.merge(inEmail);
		em.flush();
	}
}
