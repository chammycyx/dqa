package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ReplyEmailTo implements StringValuedEnum {
	
	Supplier("P", "Supplier"),
	CoaSupportTeam("S", "COA support team");	
	
    private final String dataValue;
    private final String displayValue;
        
    ReplyEmailTo(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ReplyEmailTo> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<ReplyEmailTo> getEnumClass() {
    		return ReplyEmailTo.class;
    	}
    }
}
