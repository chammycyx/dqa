package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaReplyEmail;
import hk.org.ha.model.pms.dqa.udt.coa.InEmailRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ReplyEmailTo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaReplyEmailManager")
@MeasureCalls
public class CoaReplyEmailManagerBean implements CoaReplyEmailManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	public void createCoaReplyEmail( CoaInEmail coaInEmail, EmailLog emailLog, ReplyEmailTo replyEmailTo ) {
		logger.debug("createCoaReplyEmail #0 #1", coaInEmail.getCoaInEmailId(), emailLog.getEmailId());
		
		CoaReplyEmail replyEmail = new CoaReplyEmail();
		replyEmail.setCoaInEmail( coaInEmail );
		replyEmail.setEmailLog( emailLog );
		
		if (replyEmailTo != null){
			replyEmail.setReplyEmailTo( replyEmailTo );	
			
		}else {
			if( coaInEmail.getRemarkType() == InEmailRemarkType.InvalidEmailSubject ) {
				replyEmail.setReplyEmailTo( ReplyEmailTo.CoaSupportTeam );	
			} else {
				replyEmail.setReplyEmailTo( ReplyEmailTo.Supplier );
			}		
		}
		em.persist( replyEmail );
		em.flush();
	}
	
}
