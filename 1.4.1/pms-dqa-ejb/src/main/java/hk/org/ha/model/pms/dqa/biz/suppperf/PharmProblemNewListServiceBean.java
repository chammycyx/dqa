package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemNewListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemNewListServiceBean implements PharmProblemNewListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	@Out(required = false)
	private List<PharmProblem> pharmProblemNewList;
	
	
		
	public void retrievePharmProblemListWithNullProblemStatus(){
		logger.debug("retrievePharmProblemWithNullProblemStatus");
		
				
		pharmProblemNewList = em.createNamedQuery("PharmProblem.findByNullProblemStatus")
								.setParameter("recordStatus", RecordStatus.Active)
								.setHint(QueryHints.FETCH, "o.problemHeader")
								.setHint(QueryHints.FETCH, "o.institution")
								.setHint(QueryHints.FETCH, "o.contract")
								.setHint(QueryHints.FETCH, "o.contract.supplier")
								.setHint(QueryHints.FETCH, "o.contract.manufacturer")
								.setHint(QueryHints.FETCH, "o.contract.pharmCompany")
								.setHint(QueryHints.FETCH, "o.pharmBatchNumList")
								.setHint(QueryHints.BATCH, "o.pharmProblemCountryList")
								.setHint(QueryHints.FETCH, "o.pharmProblemNatureList")
								.setHint(QueryHints.BATCH, "o.sendCollectSample")
								.setHint(QueryHints.BATCH, "o.pharmProblemFileList")
								.setHint(QueryHints.BATCH, "o.qaProblem")
								.getResultList();
		
		if(pharmProblemNewList!=null && pharmProblemNewList.size()>0)
		{
			for(PharmProblem pc:pharmProblemNewList)
			{
				getLazyPharmProblem(pc);
			}
		}
		else
		{
			pharmProblemNewList=null;
		}
		
	}
	
	public PharmProblem getLazyPharmProblem(PharmProblem pharmProblemLazy){
		// lazy
		pharmProblemLazy.getPharmProblemCountryList();
		pharmProblemLazy.getPharmProblemCountryList().size();
		pharmProblemLazy.getPharmBatchNumList();
		pharmProblemLazy.getPharmBatchNumList().size();
		pharmProblemLazy.getPharmProblemNatureList();
		pharmProblemLazy.getPharmProblemNatureList().size();
		pharmProblemLazy.getPharmProblemFileList();
		pharmProblemLazy.getPharmProblemFileList().size();
		pharmProblemLazy.getSendCollectSample();
		pharmProblemLazy.getInstitution();
		pharmProblemLazy.getProblemHeader();
		pharmProblemLazy.getQaProblem();
		pharmProblemLazy.getContract();
		if(pharmProblemLazy.getContract()!=null)
		{
			pharmProblemLazy.getContract().getSupplier();
			pharmProblemLazy.getContract().getManufacturer();
			pharmProblemLazy.getContract().getPharmCompany();
			
			if(pharmProblemLazy.getContract().getSupplier()!=null)
			{
				pharmProblemLazy.getContract().getSupplier().getContact();
			}
			if(pharmProblemLazy.getContract().getManufacturer()!=null)
			{
				pharmProblemLazy.getContract().getManufacturer().getContact();
			}
			if(pharmProblemLazy.getContract().getPharmCompany()!=null)
			{
				pharmProblemLazy.getContract().getPharmCompany().getContact();
			}
		}
		
		if(pharmProblemLazy.getSendCollectSample()!=null)
		{
			pharmProblemLazy.getSendCollectSample().getSendToQa();
			pharmProblemLazy.getSendCollectSample().getSendToSupp();
		}
		
		return pharmProblemLazy;
	}
	

	
		
	@Remove
	public void destroy(){
		if (pharmProblemNewList != null){
			pharmProblemNewList = null;
		}
	}
}
