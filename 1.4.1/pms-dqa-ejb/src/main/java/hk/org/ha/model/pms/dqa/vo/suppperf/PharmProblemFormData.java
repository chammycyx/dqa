package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemBy;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.SampleSendMethod;
import hk.org.ha.model.pms.dqa.udt.suppperf.SupplierCpoFlag;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmProblemFormData {
	
	private Long pharmProblemId;
	private Institution institution;
	private Institution rptInstitution;
	private String itemCode;
	private String fullDrugDesc;
	private Integer affectQty;
	private String baseUnit;
	private String problemNum;
	private ProblemStatus problemStatus;
	private String caseNum;
	private CaseStatus caseStatus;
	private String problemDetail;
	private ProblemClassification classificationCode;
	private Long problemHeaderId;
	
	//Contract / Quotation / DP
	private Long contractId;
	private OrderTypeAll orderType;
	private String contractNum;
	private String contractSuffix;
	private String supplierCode;
	private String supplierName;
	private String manufCode;
	private String manufName;
	private String pharmCompanyCode;
	private String pharmCompanyName;
	
	//Discovered / Reported Staff
	private String problemByName;
	private String problemByRank;
	private ProblemBy problemByType;
	private String problemByTypeDesc;
	private Date problemDate;
	private String coordinatorName;//subject officer
	private Rank coordinatorRank;//subject officer's rank
	private String coordinatorPhone;

	//Sample
	private SupplierCpoFlag sendToType;
	private String sendSupplierCode;
	private SampleSendMethod pharmSendMethod;
	private Integer sampleSendQty;
	private Date sampleSendDate;
	private Date supplierCollectDate;
	private Date cpoCollectDate;
	
	List<String> pharmBatchNumList;
	List<String> countryList;
	List<PharmProblemNature> pharmProblemNatureList;
	List<FileItem> fileItemList;

	//Conflict of interest
    private Boolean conflictFlag;
	private String conflictMspName;
	private String conflictRelationship;
	private String conflictByName;
	private String conflictByRank;
	private Date conflictDate;
	
	public PharmProblemFormData() {
		pharmBatchNumList = new ArrayList<String>();
		countryList = new ArrayList<String>();
		pharmProblemNatureList = new ArrayList<PharmProblemNature>();
		fileItemList = new ArrayList<FileItem>();
	}
	
	public void setPharmProblemId(Long pharmProblemId) {
		this.pharmProblemId = pharmProblemId;
	}

	public Long getPharmProblemId() {
		return pharmProblemId;
	}

	public void setRptInstitution(Institution rptInstitution) {
		this.rptInstitution = rptInstitution;
	}

	public Institution getRptInstitution() {
		return rptInstitution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void addPharmBatchNum(String pharmBatchNum) {
		if (pharmBatchNumList == null) {
			pharmBatchNumList = new ArrayList<String>();
		}
		pharmBatchNumList.add(pharmBatchNum);
	}
	
	public void addCountry(String countryCode) {
		if (countryList == null) {
			countryList = new ArrayList<String>();
		}
		countryList.add(countryCode);
	}
	
	public void addPharmProblemNature(PharmProblemNature pharmProblemNature) {
		if (pharmProblemNatureList == null) {
			pharmProblemNatureList = new ArrayList<PharmProblemNature>();
		}
		pharmProblemNatureList.add(pharmProblemNature);	
	}
	
	public void addFileItem(FileItem fileItem) {
		if (fileItemList == null) {
			fileItemList = new ArrayList<FileItem>();	
		}
		fileItemList.add(fileItem);
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public Integer getAffectQty() {
		return affectQty;
	}
	public void setAffectQty(Integer affectQty) {
		this.affectQty = affectQty;
	}
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public String getProblemNum() {
		return problemNum;
	}
	public void setProblemNum(String problemNum) {
		this.problemNum = problemNum;
	}
	public ProblemStatus getProblemStatus() {
		return problemStatus;
	}
	public void setProblemStatus(ProblemStatus problemStatus) {
		this.problemStatus = problemStatus;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public CaseStatus getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(CaseStatus caseStatus) {
		this.caseStatus = caseStatus;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}

	public String getProblemDetail() {
		return problemDetail;
	}

	public void setClassificationCode(ProblemClassification classificationCode) {
		this.classificationCode = classificationCode;
	}

	public ProblemClassification getClassificationCode() {
		return classificationCode;
	}

	public void setProblemHeaderId(Long problemHeaderId) {
		this.problemHeaderId = problemHeaderId;
	}

	public Long getProblemHeaderId() {
		return problemHeaderId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}
	public OrderTypeAll getOrderType() {
		return orderType;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getContractSuffix() {
		return contractSuffix;
	}
	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getManufName() {
		return manufName;
	}
	public void setManufName(String manufName) {
		this.manufName = manufName;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public String getPharmCompanyName() {
		return pharmCompanyName;
	}
	public void setPharmCompanyName(String pharmCompanyName) {
		this.pharmCompanyName = pharmCompanyName;
	}
	public String getProblemByName() {
		return problemByName;
	}
	public void setProblemByName(String problemByName) {
		this.problemByName = problemByName;
	}
	public String getProblemByRank() {
		return problemByRank;
	}
	public void setProblemByRank(String problemByRank) {
		this.problemByRank = problemByRank;
	}
	public ProblemBy getProblemByType() {
		return problemByType;
	}
	public void setProblemByType(ProblemBy problemByType) {
		this.problemByType = problemByType;
	}
	public Date getProblemDate() {
		return problemDate!=null?new Date(problemDate.getTime()):null;
	}
	public void setProblemDate(Date problemDate) {
		if (problemDate != null) {
			this.problemDate = new Date(problemDate.getTime());
		} else {
			this.problemDate = null;
		}
	}
	public String getCoordinatorName() {
		return coordinatorName;
	}
	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}
	public Rank getCoordinatorRank() {
		return coordinatorRank;
	}
	public void setCoordinatorRank(Rank coordinatorRank) {
		this.coordinatorRank = coordinatorRank;
	}
	public String getCoordinatorPhone() {
		return coordinatorPhone;
	}
	public void setCoordinatorPhone(String coordinatorPhone) {
		this.coordinatorPhone = coordinatorPhone;
	}
	public SupplierCpoFlag getSendToType() {
		return sendToType;
	}
	public void setSendSupplierCode(String sendSupplierCode) {
		this.sendSupplierCode = sendSupplierCode;
	}

	public String getSendSupplierCode() {
		return sendSupplierCode;
	}

	public void setSendToType(SupplierCpoFlag sendToType) {
		this.sendToType = sendToType;
	}
	public SampleSendMethod getPharmSendMethod() {
		return pharmSendMethod;
	}
	public void setPharmSendMethod(SampleSendMethod pharmSendMethod) {
		this.pharmSendMethod = pharmSendMethod;
	}
	public Integer getSampleSendQty() {
		return sampleSendQty;
	}
	public void setSampleSendQty(Integer sampleSendQty) {
		this.sampleSendQty = sampleSendQty;
	}
	public Date getSampleSendDate() {
		return sampleSendDate!=null?new Date(sampleSendDate.getTime()):null;
	}
	public void setSampleSendDate(Date sampleSendDate) {
		if (sampleSendDate != null) {
			this.sampleSendDate = new Date(sampleSendDate.getTime());
		} else {
			this.sampleSendDate = null;
		}
	}
	public void setSupplierCollectDate(Date supplierCollectDate) {
		this.supplierCollectDate = supplierCollectDate;
	}

	public Date getSupplierCollectDate() {
		return supplierCollectDate;
	}

	public void setCpoCollectDate(Date cpoCollectDate) {
		if (cpoCollectDate != null) {
			this.cpoCollectDate = new Date(cpoCollectDate.getTime());
		} else {
			this.cpoCollectDate = null;
		}
	}

	public Date getCpoCollectDate() {
		return cpoCollectDate;
	}

	public List<String> getPharmBatchNumList() {
		return pharmBatchNumList;
	}
	public void setPharmBatchNumList(List<String> pharmBatchNumList) {
		this.pharmBatchNumList = pharmBatchNumList;
	}
	public List<String> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<String> countryList) {
		this.countryList = countryList;
	}
	public List<PharmProblemNature> getPharmProblemNatureList() {
		return pharmProblemNatureList;
	}
	public void setPharmProblemNatureList(
			List<PharmProblemNature> pharmProblemNatureList) {
		this.pharmProblemNatureList = pharmProblemNatureList;
	}
	public List<FileItem> getFileItemList() {
		return fileItemList;
	}
	public void setFileItemList(List<FileItem> fileItemList) {
		this.fileItemList = fileItemList;
	}

	public void setConflictFlag(Boolean conflictFlag) {
		this.conflictFlag = conflictFlag;
	}

	public Boolean getConflictFlag() {
		return conflictFlag;
	}

	public void setConflictMspName(String conflictMspName) {
		this.conflictMspName = conflictMspName;
	}

	public String getConflictMspName() {
		return conflictMspName;
	}

	public void setConflictRelationship(String conflictRelationship) {
		this.conflictRelationship = conflictRelationship;
	}

	public String getConflictRelationship() {
		return conflictRelationship;
	}

	public void setConflictByName(String conflictByName) {
		this.conflictByName = conflictByName;
	}

	public String getConflictByName() {
		return conflictByName;
	}

	public void setConflictByRank(String conflictByRank) {
		this.conflictByRank = conflictByRank;
	}

	public String getConflictByRank() {
		return conflictByRank;
	}

	public void setConflictDate(Date conflictDate) {
		if (conflictDate != null) {
			this.conflictDate = new Date(conflictDate.getTime());
		} else {
			this.conflictDate = null;
		}
	}

	public Date getConflictDate() {
		return conflictDate!=null?new Date(conflictDate.getTime()):null;
	}

	public String getProblemByTypeDesc() {
		return problemByTypeDesc;
	}

	public void setProblemByTypeDesc(String problemByTypeDesc) {
		this.problemByTypeDesc = problemByTypeDesc;
	}
	
}
