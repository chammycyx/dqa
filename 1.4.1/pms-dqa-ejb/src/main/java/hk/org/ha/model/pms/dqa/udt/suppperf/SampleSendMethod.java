package hk.org.ha.model.pms.dqa.udt.suppperf;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum SampleSendMethod implements StringValuedEnum {
	
	InternalMail("I", "Internal Mail"),
	Hand("H", "Hand");	
	
    private final String dataValue;
    private final String displayValue;
        
    SampleSendMethod(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<SampleSendMethod> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<SampleSendMethod> getEnumClass() {
    		return SampleSendMethod.class;
    	}
    }
}
