package hk.org.ha.model.pms.dqa.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "EMAIL_LOG")
@Customizer(AuditCustomizer.class)
public class EmailLog extends VersionEntity {

	private static final long serialVersionUID = -6979976569261494354L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emailLogSeq")
	@SequenceGenerator(name="emailLogSeq", sequenceName="SEQ_EMAIL_LOG", initialValue=10000)
	@Column(name="EMAIL_ID")
	private Long emailId;

	@Column(name="EMAIL_FROM", length=100)
	private String emailFrom;
	
	@Column(name="EMAIL_TO", length=500)
	private String emailTo;
	
	@Column(name="EMAIL_CC", length=500)
	private String emailCc;
	
	@Column(name="EMAIL_BCC", length=500)
	private String emailBcc;
	
	@Column(name="SUBJECT", length=300)
	private String subject;
	
	@Lob
	@Column(name="CONTENT")
	private String content;	
	
	@Column(name="ATT_FILE_NAMES", length=3000)
	private String attFileNames;
	
	
	@Converter(name = "EmailLog.emailType", converterClass = EmailType.Converter.class )
	@Convert("EmailLog.emailType")
	@Column(name="EMAIL_TYPE", length=1)
	private EmailType emailType;
	
	@Converter(name = "EmailLog.moduleType", converterClass = ModuleType.Converter.class )
	@Convert("EmailLog.moduleType")
	@Column(name="MODULE_TYPE", length=1)
	private ModuleType moduleType;	
	
	public void setEmailId(Long emailId) {
		this.emailId = emailId;
	}

	public Long getEmailId() {
		return emailId;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}

	public String getEmailCc() {
		return emailCc;
	}

	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}

	public String getEmailBcc() {
		return emailBcc;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setAttFileNames(String attFileNames) {
		this.attFileNames = attFileNames;
	}

	public String getAttFileNames() {
		return attFileNames;
	}

	public void setEmailType(EmailType emailType) {
		this.emailType = emailType;
	}

	public EmailType getEmailType() {
		return emailType;
	}

	public void setModuleType(ModuleType moduleType) {
		this.moduleType = moduleType;
	}

	public ModuleType getModuleType() {
		return moduleType;
	}
	
}
