package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventory;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleTestRptServiceLocal {

	void retrieveSampleMovementList(Collection<Long> sampleMovementKeyList);
	
	List<SampleTestInventory> getSampleTestInventoryList();
	
	void destroy();
	
}
