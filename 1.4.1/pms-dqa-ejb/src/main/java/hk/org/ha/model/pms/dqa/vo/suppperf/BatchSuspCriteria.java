package hk.org.ha.model.pms.dqa.vo.suppperf;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;

import javax.persistence.Transient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BatchSuspCriteria {
	
	private Institution pcu;
	private Institution institution;
	private String caseNum;
	
	public Institution getPcu() {
		return pcu;
	}
	public void setPcu(Institution pcu) {
		this.pcu = pcu;
	}
	public Institution getInstitution() {
		return institution;
	}
	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	
}
