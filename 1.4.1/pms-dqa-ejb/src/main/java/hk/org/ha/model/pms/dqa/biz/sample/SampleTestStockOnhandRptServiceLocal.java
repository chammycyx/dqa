package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventoryStockOnhand;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleTestStockOnhandRptServiceLocal {

	void retrieveSampleMovementStockOnhandList();
	
	List<SampleTestInventoryStockOnhand> getSampleTestInventoryStockOnhandList();
	
	void destroy();
	
}
