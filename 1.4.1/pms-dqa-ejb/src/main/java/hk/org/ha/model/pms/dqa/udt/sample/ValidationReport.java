package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ValidationReport implements StringValuedEnum {
	
	AC("AC", "Available (CPO)"),
	AS("AS", "Available (Supplier)"),
	NA("NA", "Not Available"),
	AP("AP", "Available Pending"),
	NAPP("NAPP", "Not Applicable");
	
    private final String dataValue;
    private final String displayValue;
        
    ValidationReport(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ValidationReport> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<ValidationReport> getEnumClass() {
    		return ValidationReport.class;
    	}
    }
}
