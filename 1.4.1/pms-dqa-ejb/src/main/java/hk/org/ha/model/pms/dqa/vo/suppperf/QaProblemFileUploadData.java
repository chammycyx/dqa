package hk.org.ha.model.pms.dqa.vo.suppperf;

import javax.persistence.Transient;

import hk.org.ha.model.pms.dqa.persistence.FileItem;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class QaProblemFileUploadData {
	
	private byte[] fileData;
	private FileItem fileItem;
	private String refUploadFileName;
	private String itemCode;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	private boolean shareFlag;
	@Transient
	private String actionType;
	
	
	public byte[] getFileData() {
		return (fileData!=null)?(fileData.clone()):(new byte[0]);
	}
	public void setFileData(byte[] fileData) {
		if(fileData!=null)
		{
			this.fileData = fileData.clone();
		}
	}
	public FileItem getFileItem() {
		return fileItem;
	}
	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}
	public String getRefUploadFileName() {
		return refUploadFileName;
	}
	public void setRefUploadFileName(String refUploadFileName) {
		this.refUploadFileName = refUploadFileName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public void setShareFlag(boolean shareFlag) {
		this.shareFlag = shareFlag;
	}
	public boolean isShareFlag() {
		return shareFlag;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	
}
