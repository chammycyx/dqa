package hk.org.ha.model.pms.dqa.vo.sample;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestFileCriteria {
	
	private String itemCode;
	private String supplierCode;
	private String manufCode;
	private String pharmCode;
	private SampleTestFileCat docType;
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public void setPharmCode(String pharmCode) {
		this.pharmCode = pharmCode;
	}
	public String getPharmCode() {
		return pharmCode;
	}
	public SampleTestFileCat getDocType() {
		return docType;
	}
	public void setDocType(SampleTestFileCat docType) {
		this.docType = docType;
	}
}
