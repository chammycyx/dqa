package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.model.pms.dqa.persistence.CompanyContact;

import javax.ejb.Local;

@Local
public interface CompanyContactServiceLocal {
	void addCompanyContact();
	void createCompanyContact();
	void retrieveCompanyContactByCompanyCodeContactId(String companyCode, Long contactId);
	void updateCompanyContact();
	void deleteCompanyContact(CompanyContact companyContact);
	boolean isUpdateSucceed();
	
	void destroy();
}