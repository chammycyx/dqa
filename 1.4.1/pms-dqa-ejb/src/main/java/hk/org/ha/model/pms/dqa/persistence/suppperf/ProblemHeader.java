package hk.org.ha.model.pms.dqa.persistence.suppperf;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PROBLEM_HEADER")
@Customizer(AuditCustomizer.class)
public class ProblemHeader extends VersionEntity {

	private static final long serialVersionUID = -4813338845986378977L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "problemHeaderSeq")
	@SequenceGenerator(name = "problemHeaderSeq", sequenceName = "SEQ_PROBLEM_HEADER", initialValue=10000)
	@Id
	@Column(name="PROBLEM_HEADER_ID", nullable=false)
	private Long problemHeaderId;

	@Column(name="ITEM_CODE", length=10, nullable=false)
	private String itemCode;
	
	@Converter(name = "ProblemHeader.orderType", converterClass = OrderTypeAll.Converter.class )
	@Convert("ProblemHeader.orderType")
	@Column(name="ORDER_TYPE", length=1, nullable=false)
	private OrderTypeAll orderType;
	
	@Transient
	private DmDrug dmDrug = null;
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null) {
			dmDrug = DmDrugCacher.instance().getDrugByItemCode(itemCode);
		}
	}
	
	
	
	public Long getProblemHeaderId() {
		return problemHeaderId;
	}

	public void setProblemHeaderId(Long problemHeaderId) {
		this.problemHeaderId = problemHeaderId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public OrderTypeAll getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}
	
	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}

}
