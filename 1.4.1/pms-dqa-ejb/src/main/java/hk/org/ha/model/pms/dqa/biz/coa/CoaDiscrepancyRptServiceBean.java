package hk.org.ha.model.pms.dqa.biz.coa;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.ReportDateType;
import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptData;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaDiscrepancyRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaDiscrepancyRptServiceBean implements CoaDiscrepancyRptServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String REPORTNAME = "COA Discrepancy Report";
	private static final String DATE_FORMAT = "dd-MMM-yyyy";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	
	private static final String SQL_SUPPLIER = "o.coaBatch.coaItem.contract.supplier.supplierCode ";
	private static final String SQL_ITEMCODE = "o.coaBatch.coaItem.contract.itemCode ";
	
	private List<CoaDiscrepancyRptData> coaDiscrepancyRptList;
	
	private String reportParameters;
	
	private DateFormat sdf;

	@PostConstruct
	public void init() {
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
	}
	
	@SuppressWarnings("unchecked")
	private List retrieveCoaBatchVerList(CoaDiscrepancyRptCriteria coaDiscrepancyRptCriteria){
		logger.debug("retrieveCoaBatchVerList");
		
		ReportGroupByType groupType =  coaDiscrepancyRptCriteria.getGroupType();
		ReportDateType dateType = coaDiscrepancyRptCriteria.getReportDateType();

		String supplierCodeIn = coaDiscrepancyRptCriteria.getSupplierCode();
		String itemCodeIn = coaDiscrepancyRptCriteria.getItemCode();
		Date startDate = coaDiscrepancyRptCriteria.getStartDate();

		Calendar c = Calendar.getInstance(); 
		c.setTime(coaDiscrepancyRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
	
		StringBuilder sqlSelectBr = new  StringBuilder("Select ");
		StringBuilder sqlFromStrBr = new StringBuilder("from CoaBatchVer o where o.coaStatus = :coaStatus and o.recordStatus = :recordStatus ");
		StringBuilder sqlWhereBr = new StringBuilder("");
		StringBuilder sqlGroupBr = new StringBuilder("group by ");
		StringBuilder sqlOrderBr = new StringBuilder("order by ");
		
		String sqlDiscrepancyStr = "o.discrepancyStatus ";
		
		if (dateType == ReportDateType.ActionDate){
			sqlWhereBr.append("and o.createDate >=:startDate and o.createDate < :endDate ");
		}else if (dateType == ReportDateType.CreateDate){
			sqlWhereBr.append("and o.coaBatch.createDate >=:startDate and o.coaBatch.createDate < :endDate ");
		}
		
		sqlFromStrBr.append(sqlWhereBr);
		sqlFromStrBr.append(" and (o.coaBatch.coaItem.contract.itemCode = :itemCode or :itemCode is null) " +
			"and (o.coaBatch.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' = :supplierCode ) ");
		
		if (groupType == ReportGroupByType.SupplierCode){
			sqlSelectBr.append(SQL_SUPPLIER);
			sqlGroupBr.append(SQL_SUPPLIER);
			sqlOrderBr.append(SQL_SUPPLIER);
			
		}else if (groupType == ReportGroupByType.ItemCode){
			sqlSelectBr.append(SQL_ITEMCODE);
			sqlGroupBr.append(SQL_ITEMCODE);
			sqlOrderBr.append(SQL_ITEMCODE);
		}else {
			sqlSelectBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
			sqlGroupBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
			sqlOrderBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
		}
			sqlSelectBr.append(", " + sqlDiscrepancyStr+", count(o) ");
			sqlGroupBr.append(", " + sqlDiscrepancyStr);
			sqlOrderBr.append(", " + sqlDiscrepancyStr);
			
			sqlSelectBr.append(sqlFromStrBr);
			sqlSelectBr.append(sqlGroupBr);
			sqlSelectBr.append(sqlOrderBr);

		List coaBatchVerList = em.createQuery(sqlSelectBr.toString())
			.setParameter("startDate", startDate, TemporalType.DATE)
			.setParameter("endDate", sqlEndDate, TemporalType.DATE)
			.setParameter("itemCode", itemCodeIn)
			.setParameter("supplierCode", supplierCodeIn)
			.setParameter("coaStatus", CoaStatus.DiscrepancyClarificationInProgress)
			.setParameter("recordStatus", RecordStatus.Active)
			.getResultList();
		
		logger.debug("coaBatchVerList size: #0", coaBatchVerList.size());
		return coaBatchVerList;
	}

	@SuppressWarnings("unchecked")
	private List retrieveCoaBatchVerReminderList(CoaDiscrepancyRptCriteria coaDiscrepancyRptCriteria){
		
		ReportGroupByType groupType = coaDiscrepancyRptCriteria.getGroupType();
		ReportDateType dateType = coaDiscrepancyRptCriteria.getReportDateType();
		
		String supplierCodeIn = coaDiscrepancyRptCriteria.getSupplierCode();
		String itemCodeIn = coaDiscrepancyRptCriteria.getItemCode();
		Date startDate = coaDiscrepancyRptCriteria.getStartDate();

		Calendar c = Calendar.getInstance(); 
		c.setTime(coaDiscrepancyRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
		
		StringBuilder sqlSelectBr = new StringBuilder("Select "); 

		StringBuilder sqlStrBr = new StringBuilder("from CoaBatchVer o where o.coaStatus = :coaStatus " +
													"and o.discrepancyStatus = :discrepancyStatus " +
													"and o.recordStatus = :recordStatus ");
		StringBuilder sqlWhereBr = new StringBuilder("");
		StringBuilder sqlGroupBr = new StringBuilder("group by ");
		StringBuilder sqlOrderBr = new StringBuilder("order by "); 
				
		if (dateType == ReportDateType.ActionDate){
			sqlWhereBr.append("and o.createDate >=:startDate and o.createDate < :endDate ");
		}else if (dateType == ReportDateType.CreateDate){
			sqlWhereBr.append("and o.coaBatch.createDate >=:startDate and o.coaBatch.createDate < :endDate ");
		}
		sqlStrBr.append(sqlWhereBr);
		sqlStrBr.append("and (o.coaBatch.coaItem.contract.itemCode = :itemCode or :itemCode is null) " +
						"and (o.coaBatch.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' = :supplierCode ) ");

		if (groupType == ReportGroupByType.SupplierCode){			
			sqlSelectBr.append(SQL_SUPPLIER);
			sqlGroupBr.append(SQL_SUPPLIER);
			sqlOrderBr.append(SQL_SUPPLIER);
			
		}else if (groupType == ReportGroupByType.ItemCode){
			sqlSelectBr.append(SQL_ITEMCODE);
			sqlGroupBr.append(SQL_ITEMCODE);
			sqlOrderBr.append(SQL_ITEMCODE);
		}else {
			sqlSelectBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
			sqlGroupBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
			sqlOrderBr.append(SQL_SUPPLIER +","+ SQL_ITEMCODE);
		}
			sqlSelectBr.append(", o.reminderNum, count(o) ");
			sqlGroupBr.append(" , o.reminderNum ");
		
			sqlSelectBr.append(sqlStrBr);  
			sqlSelectBr.append(sqlGroupBr);
			sqlSelectBr.append(sqlOrderBr);
	
			List coaBatchVerReminderList = em.createQuery(sqlSelectBr.toString())
				.setParameter("coaStatus", CoaStatus.DiscrepancyClarificationInProgress)
				.setParameter("discrepancyStatus", DiscrepancyStatus.ReminderToSupplier)
				.setParameter("recordStatus", RecordStatus.Active)
				.setParameter("startDate", startDate, TemporalType.DATE)
				.setParameter("endDate", sqlEndDate, TemporalType.DATE)
				.setParameter("itemCode", itemCodeIn)
				.setParameter("supplierCode", supplierCodeIn)
				.getResultList();
		
		logger.debug("coaBatchVerReminderList size: #0", coaBatchVerReminderList.size());
		return coaBatchVerReminderList;
	}
	
	private void setReportParameters(CoaDiscrepancyRptCriteria coaDiscrepancyRptCriteria){
		ReportGroupByType groupType = coaDiscrepancyRptCriteria.getGroupType();
		ReportDateType dateType = coaDiscrepancyRptCriteria.getReportDateType();
		String supplierCodeIn = coaDiscrepancyRptCriteria.getSupplierCode();
		String itemCodeIn = coaDiscrepancyRptCriteria.getItemCode();
		Date startDate = coaDiscrepancyRptCriteria.getStartDate();
		Date endDate = coaDiscrepancyRptCriteria.getEndDate();
		
		StringBuilder reportParamBr = new StringBuilder();
		
		if (groupType == ReportGroupByType.SupplierCode){
		
			reportParamBr.append("Supplier Code=");
			reportParamBr.append(supplierCodeIn);
		}else if (groupType == ReportGroupByType.ItemCode){
			reportParamBr.append("Item Code=");
			reportParamBr.append((itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn));
		}else {
			reportParamBr.append("Supplier Code=");
			reportParamBr.append(supplierCodeIn);
			reportParamBr.append(", Item Code=");
			reportParamBr.append((itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn));
		}
		
		if (dateType == ReportDateType.ActionDate){
			reportParamBr.append(", COA Batch Action Date=");
		}else {
			reportParamBr.append(", COA Batch Create Date=");
		}
		
		sdf = new SimpleDateFormat(DATE_FORMAT);
		String startDateStr = (sdf.format(startDate));
		String endDateStr = (sdf.format(endDate));

		reportParamBr.append(startDateStr);
		reportParamBr.append(" to ");
		reportParamBr.append(endDateStr);
		reportParamBr.append(", Group by=");
		reportParamBr.append(groupType.getDisplayValue());
		
		reportParameters = reportParamBr.toString();
	}

	public void retrieveCoaDiscrepancyList(CoaDiscrepancyRptCriteria coaDiscrepancyRptCriteria){
		logger.debug("retrieveCoaDiscrepancyList");

		DiscrepancyStatus discrepancyStatus;
		CoaDiscrepancyRptData cd;
		Param param;
		String hashKey;
		int freq; 
		
		coaDiscrepancyRptList = new ArrayList<CoaDiscrepancyRptData>();
		
		List<?> coaBatchVerList = retrieveCoaBatchVerList(coaDiscrepancyRptCriteria);
		List<?> coaBatchVerReminderList = retrieveCoaBatchVerReminderList(coaDiscrepancyRptCriteria);

		ReportGroupByType groupType = coaDiscrepancyRptCriteria.getGroupType();

		setReportParameters(coaDiscrepancyRptCriteria);
		
		Map<String, CoaDiscrepancyRptData> coaDiscrepancyHM = new HashMap<String , CoaDiscrepancyRptData>(); 
		
		// get grouped "Reminder to supplier" frequency
		if (coaBatchVerReminderList !=null ){
			coaDiscrepancyHM = constructCoaBathcReminderFreqData(coaBatchVerReminderList,groupType);
		}
		
		// set all other groups frequency
		if (coaBatchVerList.size() > 0){

			for (Object o : coaBatchVerList){
				if (o.getClass().isArray()){	
					param = constructParam(groupType, o);
					hashKey = retrieveReportHashKey(groupType, param);
					
					if (coaDiscrepancyHM.containsKey(hashKey)){
						cd = (CoaDiscrepancyRptData) coaDiscrepancyHM.get(hashKey);
						logger.debug("Discrepancy is null :", (cd==null));
					}else {
						cd = new CoaDiscrepancyRptData();
						coaDiscrepancyHM.put(hashKey, cd);
						coaDiscrepancyRptList.add(cd);
					}
					if (cd!=null){
						cd.setSupplierCode(param.getSupplierCode());
						cd.setItemCode(param.getItemCode());
					
						if (Array.get(o, param.getStatusIdx()) != null){
							discrepancyStatus = (DiscrepancyStatus) Array.get(o, param.getStatusIdx());
							freq = ((Long)Array.get(o, param.getFreqIdx())).intValue();
							
							cd = setStatusFrequence(discrepancyStatus, cd , freq);
							logger.debug("frequence updated :#0", (cd==null));
						}
					}
				}
			}
		}
		logger.debug("coaDiscrepancyRptList report list size : #0",coaDiscrepancyRptList.size());
	}
	
	private Param constructParam(ReportGroupByType groupType, Object o){	
		Param param; 
		if (groupType == ReportGroupByType.SupplierCode){
			param = new Param(Array.get(o, 0).toString(),"", 1, 2 );	
		}else if (groupType == ReportGroupByType.ItemCode){
			param = new Param("", Array.get(o, 0).toString(), 1, 2 );			
		}else {	
			param = new Param(Array.get(o, 0).toString(), Array.get(o, 1).toString(),  2, 3 );

		}
		return param;
	}
	
	private String retrieveReportHashKey(ReportGroupByType groupType, Param param ){
		if (groupType == ReportGroupByType.SupplierCode){
			return param.getSupplierCode();
		}else if (groupType == ReportGroupByType.ItemCode){
			return param.getItemCode();
		}else {
			return param.getItemCode() + param.getSupplierCode();
		}
	}
	
	private CoaDiscrepancyRptData setStatusFrequence(DiscrepancyStatus discrepancyStatus, CoaDiscrepancyRptData cd , int freq){
		if (discrepancyStatus == DiscrepancyStatus.DiscrepancyUnderGrouping){
			cd.setDiscrepancyUnderGrouping(freq);
		}else if (discrepancyStatus == DiscrepancyStatus.PharmacistFollowUp){
			cd.setPharmacistFollowUp(freq);
		}else if (discrepancyStatus == DiscrepancyStatus.EmailToSupplier){
			cd.setEmailToSupplier(freq);
		}
		return cd;
	}
	
	private Map<String, CoaDiscrepancyRptData> constructCoaBathcReminderFreqData(List<?> coaBatchVerReminderList, ReportGroupByType groupType){
		Map<String, CoaDiscrepancyRptData> coaDiscrepancyHM = new HashMap<String , CoaDiscrepancyRptData>();

		String hashKey;
		CoaDiscrepancyRptData cd;
		int reminderNo;
		int freq;

		if (coaBatchVerReminderList != null){	
			coaDiscrepancyHM = new HashMap<String , CoaDiscrepancyRptData>();

			Param param;

			for (Object o : coaBatchVerReminderList){
				if (o.getClass().isArray()){
					param = constructParam(groupType, o);
					hashKey = retrieveReportHashKey(groupType, param);
					if (coaDiscrepancyHM.containsKey(hashKey)){
						cd = (CoaDiscrepancyRptData) coaDiscrepancyHM.get(hashKey);
					}else {
						cd = new CoaDiscrepancyRptData();
						coaDiscrepancyHM.put(hashKey, cd);
						coaDiscrepancyRptList.add(cd);
					}

					reminderNo = ((Integer)Array.get(o, param.getStatusIdx())).intValue();
					freq = ((Long)Array.get(o, param.getFreqIdx())).intValue();

					if (reminderNo == 1){
						cd.setReminderToSupplier1st(freq);
					}else if (reminderNo == 2){
						cd.setReminderToSupplier2nd(freq);					
					}else if (reminderNo > 2) {
						freq += cd.getReminderToSupplierFuther(); 
						cd.setReminderToSupplierFuther(freq);
					}
				}
			}
		}
		return coaDiscrepancyHM;
	}

	public List<CoaDiscrepancyRptData> getCoaDiscrepancyRptList() {
		return coaDiscrepancyRptList;
	}
	
	public String getReportName() {
		return REPORTNAME;
	}

	public String getReportParameters() {
		return reportParameters;
	}

	public String getRetrieveDate() {		
		return sdf.format(new Date());
	}
	
	@Remove
	public void destroy(){
		
	}
	
	public static class Param
	{
		private String supplierCode ; 
		private String itemCode;
		private int statusIdx;
		private int freqIdx;

		public Param(String itemCode, String supplierCode) {
			super();
			this.itemCode = itemCode;
			this.supplierCode = supplierCode;
		}

		public Param(String supplierCode, String itemCode,
				int statusIdx, int freqIdx) 
		{
			super();
			this.supplierCode = supplierCode;
			this.itemCode = itemCode;
			this.statusIdx = statusIdx;
			this.freqIdx = freqIdx;
		}
		
		public String getSupplierCode() {
			return supplierCode;
		}
		public void setSupplierCode(String supplierCode) {
			this.supplierCode = supplierCode;
		}
		public String getItemCode() {
			return itemCode;
		}
		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}

		public int getStatusIdx() {
			return statusIdx;
		}
		public void setStatusIdx(int statusIdx) {
			this.statusIdx = statusIdx;
		}
		public int getFreqIdx() {
			return freqIdx;
		}
		public void setFreqIdx(int freqIdx) {
			this.freqIdx = freqIdx;
		}	
	}

}
