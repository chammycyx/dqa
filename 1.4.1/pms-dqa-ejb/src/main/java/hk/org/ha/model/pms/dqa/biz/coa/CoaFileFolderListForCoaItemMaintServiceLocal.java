package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;

import javax.ejb.Local;

@Local
public interface CoaFileFolderListForCoaItemMaintServiceLocal {
	
	void retrieveCoaFileFolderListForCoaItemMaint(CoaItem coaItem);
	
	void destroy();
}
