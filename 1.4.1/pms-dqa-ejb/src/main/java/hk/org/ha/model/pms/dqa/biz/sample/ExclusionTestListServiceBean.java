package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("exclusionTestListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ExclusionTestListServiceBean implements ExclusionTestListServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<ExclusionTest> exclusionTestList;
	

	@SuppressWarnings("unchecked")
	public void retrieveExclusionTestList(){
		logger.debug("retrieveExclusionTestList");
		
		exclusionTestList = null;
		
		exclusionTestList = em.createNamedQuery("ExclusionTest.findByRecordStatus")
					.setParameter("recordStatus", RecordStatus.Active)
					.setHint(QueryHints.FETCH, "o.sampleItem")
					 .getResultList();
		
		for (ExclusionTest exTest :  exclusionTestList){
			exTest.getSampleItem();
//			exTest.getSampleItem().postLoad();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ExclusionTest> retrieveExclusionTestListByItemCode(String code){
		logger.debug("retrieveExclusionTestList");

		List<ExclusionTest>  exList = em.createNamedQuery("ExclusionTest.findByItemCode")
		.setParameter("itemCode", code)
		.setParameter("recordStatus", RecordStatus.Active)
		.setHint(QueryHints.FETCH, "o.sampleItem")
		.getResultList();
		
		for (ExclusionTest exTest :  exList){
			exTest.getSampleItem();
//			exTest.getSampleItem().postLoad();
		}
		return exList;
	}
	
		
	@Remove
	public void destroy(){
		exclusionTestList = null;
	}
}
