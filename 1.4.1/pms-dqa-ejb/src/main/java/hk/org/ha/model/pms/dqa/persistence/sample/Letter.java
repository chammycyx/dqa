package hk.org.ha.model.pms.dqa.persistence.sample;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "LETTER")
@Customizer(AuditCustomizer.class)
public class Letter extends VersionEntity {
		
	private static final long serialVersionUID = -3553575998216999860L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "letterSeq")
	@SequenceGenerator(name = "letterSeq", sequenceName = "SEQ_LETTER", initialValue=10000)
	@Id
	@Column(name="LETTER_ID", nullable=false)
	private Long letterId;
	
	@Column(name="LETTER_FROM", length=100)
	private String letterFrom;
	
	@Column(name="LETTER_TO", length=500)
	private String letterTo;
	
	@Column(name="LETTER_CC", length=500)
	private String letterCc;
	
	@Column(name="SUBJECT", length=300)
	private String subject;
	
	@Lob
	@Column(name="CONTENT")
	private String content;
	
	@Column(name="ATT_FILE_NAMES", length=3000)
	private String attFileNames;
	
	@Column(name="LETTER_TYPE", length=1)
	private String letterType;
	
	@Column(name="REF_NUM", length=15)
	private String refNum;
	
	@Column(name="SEND_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;

	@Converter(name="Letter.sendAtSchedStatus", converterClass = ScheduleStatus.Converter.class )
	@Convert("Letter.sendAtSchedStatus")
	@Column(name="SEND_AT_SCHED_STATUS", length=2)
	private ScheduleStatus sendAtSchedStatus;
	
	@Column(name="LETTER_CODE", length=10)
	private String letterCode;

	public Long getLetterId() {
		return letterId;
	}

	public void setLetterId(Long letterId) {
		this.letterId = letterId;
	}

	public String getLetterTo() {
		return letterTo;
	}

	public void setLetterTo(String letterTo) {
		this.letterTo = letterTo;
	}

	public Date getSendDate() {
		return (sendDate != null) ? new Date(sendDate.getTime()) : null;
	}

	public void setSendDate(Date sendDate) {
		if (sendDate != null) {
			this.sendDate = new Date(sendDate.getTime());
		} else {
			this.sendDate = null;
		}
	}

	public String getLetterFrom() {
		return letterFrom;
	}

	public void setLetterFrom(String letterFrom) {
		this.letterFrom = letterFrom;
	}

	public String getLetterCc() {
		return letterCc;
	}

	public void setLetterCc(String letterCc) {
		this.letterCc = letterCc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttFileNames() {
		return attFileNames;
	}

	public void setAttFileNames(String attFileNames) {
		this.attFileNames = attFileNames;
	}

	public String getLetterType() {
		return letterType;
	}

	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public ScheduleStatus getSendAtSchedStatus() {
		return sendAtSchedStatus;
	}

	public void setSendAtSchedStatus(ScheduleStatus sendAtSchedStatus) {
		this.sendAtSchedStatus = sendAtSchedStatus;
	}

	public String getLetterCode() {
		return letterCode;
	}

	public void setLetterCode(String letterCode) {
		this.letterCode = letterCode;
	}
		
}
