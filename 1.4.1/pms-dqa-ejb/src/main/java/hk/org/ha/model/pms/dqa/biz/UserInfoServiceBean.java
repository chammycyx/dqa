package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.SendCollectSample;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("userInfoService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UserInfoServiceBean implements UserInfoServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private UserInfo userInfo;
	
	private boolean success;

	private String errorCode;
	
	public void retrieveUserInfoByUserInfo(UserInfo userInfoIn){
		logger.debug("retrieveUserInfoByUserInfo");
		userInfo = retrieveUserInfoByUserInfoForValidation(userInfoIn.getUserCode());
	}
	
	private UserInfo retrieveUserInfoByUserInfoForValidation(String  userCode){
		logger.debug("retrieveUserInfoByUserInfoForValidation");		
		return em.find(UserInfo.class, userCode);
	}
	
	public void addUserInfo(){
		logger.debug("addUserInfo");
		
		userInfo = new UserInfo();
		userInfo.setContact(new Contact());
		
	}


	public void createUserInfo(UserInfo userInfoIn){
		logger.debug("createUserInfo");
		success = false;
		errorCode = null;
		
		List<UserInfo> userInfoList = null;
 	
		userInfoList = em.createNamedQuery("UserInfo.findByUserCode")
		.setParameter("userCode", userInfoIn.getUserCode())
		.getResultList();
		
		if (userInfoList == null || userInfoList.size() == 0){
			em.persist(userInfoIn);
			em.flush();
			success = true;
		}else {
			errorCode = "0007";
		}
	}

	public void updateUserInfo(UserInfo userInfoIn){
		logger.debug("updateUserInfo");
		success = false;
		errorCode = null;

		em.merge(userInfoIn);		
		em.flush();
		success = true;		
	}
	
	public void deleteUserInfo(UserInfo userInfoIn){
		logger.debug("deleteUserInfo");
		success = false;
		errorCode = null;
		
		List<SendCollectSample> sendCollectSamples = em.createNamedQuery("SendCollectSample.findByUserCode").setParameter("userCode", userInfoIn.getUserCode()).getResultList();
		
		List<FaxDetail> faxDetails = em.createNamedQuery("FaxDetail.findByUserCode").setParameter("userCode", userInfoIn.getUserCode()).getResultList();

		UserInfo userInfoFind = retrieveUserInfoByUserInfoForValidation(userInfoIn.getUserCode());
		
		if(sendCollectSamples!=null && sendCollectSamples.size()>0)
		{
			errorCode = "0124";
		}
		else if(faxDetails!=null && faxDetails.size()>0)
		{
			errorCode = "0126";
		}
		else
		{
			if (userInfoFind != null){
				em.remove(userInfoFind);
				em.flush();
				success = true;		
			}
			else
			{
				errorCode = "0008";
			}
		}
	}
	
	public boolean isSuccess(){
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	@Remove
	public void destroy(){
		if (userInfo != null){
			userInfo = null;
		}
	}
}
