package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum InEmailRemarkType implements StringValuedEnum {
	
	NullValue("N", "Null value"),
	InvalidEmailSubject("1", "Invalid email subject");
	
    private final String dataValue;
    private final String displayValue;
        
    InEmailRemarkType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<InEmailRemarkType> {

		private static final long serialVersionUID = 6368919235452551382L;

		@Override
    	public Class<InEmailRemarkType> getEnumClass() {
    		return InEmailRemarkType.class;
    	}
    }
}
