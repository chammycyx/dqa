package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;
import hk.org.ha.fmk.pms.web.MeasureCalls;


import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleMovementService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleMovementServiceBean implements SampleMovementServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private SampleMovement sampleMovement;
	
	@Out(required = false)
	private SampleMovement sampleMovementNewest;
	
	private boolean success;

			
	@SuppressWarnings("unchecked")
	public void retrieveSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("retrieveSampleMovement" );

		List<SampleMovement> sampleMovementList = em.createNamedQuery("SampleMovement.findBySampleLogId") 
		.setParameter("sampleLogId", sampleMovementIn.getSampleLogId())
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
        .getResultList();
		
		if (sampleMovementList==null || sampleMovementList.size()==0)
		{
			sampleMovement = null;
		}
		else
		{
			sampleMovement = sampleMovementList.get(0);
			retrieveSampleMovementNewestByItemCode(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode());
		}
		
		

	}
	
	@SuppressWarnings("unchecked")
	public SampleMovement retrieveSampleMovementNewestByItemCode(String itemCode){
		logger.debug("retrieveSampleMovementNewestByItemCode" );

		List<SampleMovement> sampleMovementNewestList = em.createNamedQuery("SampleMovement.findNewestByItemCode") 
		.setParameter("itemCode", itemCode)
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
        .getResultList();
		
		if (sampleMovementNewestList==null || sampleMovementNewestList.size()==0){
			sampleMovementNewest = null;}
		else{
			sampleMovementNewest = sampleMovementNewestList.get(0);}
		
		return sampleMovementNewest;
	}
	
	public void createSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("createSampleMovement");
		success = false;
		
		em.persist(sampleMovementIn);		

		em.flush();
		success = true;	
	}
	
	public void updateSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("updateSampleMovement");
		success = false;
		
		em.merge(sampleMovementIn);		

		em.flush();
		success = true;	
	}
	
	@SuppressWarnings("unchecked")
	public void createSampleMovementForReverseSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn){       
		SampleMovement sampleMovementSave = new SampleMovement();

		SampleMovement sampleMovementNewestIn = retrieveSampleMovementNewestByItemCode(sampleTestScheduleIn.getSampleItem().getItemCode());
		
		if(scheduleStatusIn==ScheduleStatus.LabTest){
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getInstitution().getInstitutionCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getRecQty()*-1);
			
			if(sampleMovementNewestIn==null){
				sampleMovementSave.setOnHandQty((sampleTestScheduleIn.getRecQty()*-1));}
			else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getRecQty()*-1));}
			
			sampleMovementSave.setAdjustFlag("Y");
			sampleMovementSave.setAdjustReason("Reverse from LT to DS");
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		else if (scheduleStatusIn==ScheduleStatus.TestResult)
		{
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getLab().getLabCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getLabCollectQty());
			
			if(sampleMovementNewestIn==null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getLabCollectQty());}
			else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getLabCollectQty());}
			
			sampleMovementSave.setAdjustFlag("Y");
			sampleMovementSave.setAdjustReason("Reverse from TR to LC");
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
	
		em.persist(sampleMovementSave);
	}
	
	@SuppressWarnings("unchecked")
	public void createSampleMovementForNormalSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn){
		SampleMovement sampleMovementSave = new SampleMovement();
		SampleMovement sampleMovementNewestIn = retrieveSampleMovementNewestByItemCode(sampleTestScheduleIn.getSampleItem().getItemCode());
		
		if (scheduleStatusIn==ScheduleStatus.DrugSample)
		{ 
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getInstitution().getInstitutionCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getRecQty());

			if(sampleMovementNewestIn==null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getRecQty());}
			else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getRecQty());}
			
			
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		else if (scheduleStatusIn==ScheduleStatus.LabCollection)
		{ 
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getLab().getLabCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getLabCollectQty() * -1);
			
			if(sampleMovementNewestIn==null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getLabCollectQty() * -1);}
			else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getLabCollectQty() * -1));}
			
			
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		
		em.persist(sampleMovementSave);
	}
	
	public boolean isSuccess(){
		return success;
	}
	
	@Remove
	public void destroy() {
		if (sampleMovement !=null){
			sampleMovement = null;
		}
		if (sampleMovementNewest !=null){
			sampleMovementNewest = null;
		}
	}
}
