package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Calendar;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleType;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleNextService")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleNextServiceBean implements SampleTestScheduleNextServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	private boolean success;
	
	@Out(required = false)
	private SampleTestSchedule sampleTestScheduleNext;
	
	@In(create = true)
	private SampleTestScheduleServiceLocal sampleTestScheduleService;
	
	@In(create =true)
	private TestFrequencyListServiceLocal testFrequencyListService;
	
	private static final String ITEM_CODE = "itemCode";
	private static final String TEST_CODE = "testCode";
	private static final String RECORD_STATUS = "recordStatus";
	
	
	@SuppressWarnings("unchecked")
	public SampleTestSchedule retrieveNextSampleTestSchedule(SampleTestScheduleFile sampleTestScheduleFileIn){
		logger.debug("retrieveNextSampleTestSchedule");
		success = false;
		
		SampleTestSchedule sampleTestScheduleIn = getLazySampleTestSchedule(sampleTestScheduleFileIn.getSampleTestSchedule());
		
		List<SampleTestSchedule> sampleTestScheduleListFind;
		SampleItem sampleItemFind;
		List<TestFrequency> testFrequencyListFind;
		RiskLevel riskLevelFind;
		List<Contract> liveContractListFind = null;
		
		Double frequencyPerYear;
		
		Calendar currentScheduleMonthCal = Calendar.getInstance(); 
		Calendar nextScheduleMonthCal = Calendar.getInstance();
		Calendar nowDateCal = Calendar.getInstance();
		Calendar todayDate = Calendar.getInstance();
		
		Calendar nextScheduleMonthCalForNextContract = Calendar.getInstance();
		Calendar nowDateCalForNextContract = Calendar.getInstance();
		Calendar queryScheduleMonthForNextContract;
		
		SampleTestSchedule sampleTestScheduleFind=null;
		
		////////////////////////////////////////////
		/// Check if there is outstanding schedule
		////////////////////////////////////////////
		sampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findOutstandingScheduleByItemCodeTestCodeRecordStatus")
									.setParameter(ITEM_CODE, sampleTestScheduleIn.getSampleItem().getItemCode())
									.setParameter(TEST_CODE, sampleTestScheduleIn.getSampleTest().getTestCode())
									.setParameter(RECORD_STATUS, RecordStatus.Active)
									.setParameter("scheduleStatusP",ScheduleStatus.Payment)
									.setParameter("scheduleStatusC", ScheduleStatus.Complete)
									.setParameter("actionFlag", "Y")
									.setHint(QueryHints.FETCH, "o.sampleTest")
									.setHint(QueryHints.FETCH, "o.contract")
									.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
									.setHint(QueryHints.BATCH, "o.institution")
									.setHint(QueryHints.BATCH, "o.contract.supplier")
									.setHint(QueryHints.BATCH, "o.contract.manufacturer")
									.setHint(QueryHints.BATCH, "o.contract.pharmCompany")
									.getResultList();
		
		if (sampleTestScheduleListFind==null || sampleTestScheduleListFind.size()==0)
		{
			sampleTestScheduleFind = sampleTestScheduleIn;
		}
		else if(sampleTestScheduleListFind.size()==1)
		{
			if (sampleTestScheduleListFind.get(0).getScheduleId().longValue() == sampleTestScheduleIn.getScheduleId().longValue()){
				sampleTestScheduleFind = getLazySampleTestSchedule(sampleTestScheduleListFind.get(0));
			}
			else{
				//System.out.println("Error!!! Outstanding Schedule exist.");
				sampleTestScheduleNext=null;
				return sampleTestScheduleNext;
			}
		}
		else if(sampleTestScheduleListFind.size()>1)
		{
			//System.out.println("Error!!! Outstanding Schedule exist.");
			sampleTestScheduleNext=null;
			return sampleTestScheduleNext;
		}
		
		////////////////////////////////////////////
		/// Check if item exist
		////////////////////////////////////////////
		sampleItemFind = checkSampleItemValid(sampleTestScheduleIn.getSampleItem().getItemCode(), RecordStatus.Active);
		if(sampleItemFind==null)
		{	
			sampleTestScheduleNext=null;
			return sampleTestScheduleNext;
		}
		
		
		////////////////////////////////////////////
		/// Check if there is exclusion record
		////////////////////////////////////////////
		if(!checkExclusionTestValid(sampleTestScheduleIn.getSampleItem().getItemCode(), sampleTestScheduleIn.getSampleTest().getTestCode(), RecordStatus.Active))
		{
			sampleTestScheduleNext=null;
			return sampleTestScheduleNext;
		}
		
		
		/////////////////////////////////////////////////////////////
		/// Check if the risk level proceedSampleTestFlag is Y
		//////////////////////////////////////////////////////////////
		riskLevelFind = checkRiskLevelValid(sampleTestScheduleFind.getSampleItem().getRiskLevel().getRiskLevelCode());
		if(riskLevelFind==null)
		{	
			sampleTestScheduleNext=null;
			return sampleTestScheduleNext;
		}
		
		////////////////////////////////////////////////////////////////////////
		/// retrieve test frequency by test code, order type, risk level
		///////////////////////////////////////////////////////////////////
		if(sampleTestScheduleFind.getSampleItem().getDmDrug()==null)
		{	testFrequencyListFind=null;
		}
		else
		{
			testFrequencyListFind = testFrequencyListService.checkTestFrequencyValid(sampleTestScheduleFind.getSampleTest().getTestCode(), sampleTestScheduleFind.getSampleItem().getRiskLevel().getRiskLevelCode(), sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType());
		}
		
		if(testFrequencyListFind==null || testFrequencyListFind.size()==0)
		{
			sampleTestScheduleNext=null;
			return sampleTestScheduleNext;
		}
		else{
			frequencyPerYear = Double.parseDouble(testFrequencyListFind.get(0).getFrequencyPerYear().getDataValue());
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		/// check contract period for order type C,Q and the select percentage of order type D
		/////////////////////////////////////////////////////////////////////////////////////////////
		
		currentScheduleMonthCal.setTime(sampleTestScheduleFind.getScheduleMonth());
		nextScheduleMonthCal.setTime(sampleTestScheduleFind.getScheduleMonth());
		nextScheduleMonthCal.add(Calendar.MONTH, (int)(12/frequencyPerYear));
		
		
		////////////////////////////////////////////////////////////////////////////////
		/// Find Next Schedule Month for query the next live contract
		////////////////////////////////////////////////////////////////////////////////
		nextScheduleMonthCalForNextContract.setTime(sampleTestScheduleFind.getScheduleMonth());
		nextScheduleMonthCalForNextContract.add(Calendar.MONTH, (int)(12/frequencyPerYear));
		nextScheduleMonthCalForNextContract.set(nextScheduleMonthCalForNextContract.get(Calendar.YEAR), nextScheduleMonthCalForNextContract.get(Calendar.MONTH), 1);
		nowDateCalForNextContract.set(nowDateCalForNextContract.get(Calendar.YEAR), nowDateCalForNextContract.get(Calendar.MONTH), 1);
		
		queryScheduleMonthForNextContract = constructScheduleMonthForNextContract(nextScheduleMonthCalForNextContract, nowDateCalForNextContract);

		////////////////////////////
		
		
		if(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType().equals(OrderType.Contract.getDataValue()) ||
		   sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType().equals(OrderType.StandardQuotation.getDataValue()) )
		{
			Calendar contractEndDateCal = Calendar.getInstance();
			
			/////////////////////////////////
			// if order type change
			/////////////////////////////////
			if(!sampleTestScheduleFind.getOrderType().getDataValue().equals(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType()))
			{
				//System.out.println("Find next live contract.");
				
				liveContractListFind = findNextLiveContract(sampleTestScheduleFind, queryScheduleMonthForNextContract);
				
				if (liveContractListFind==null || liveContractListFind.size()==0)
				{
					//System.out.println("No next live contract.");
					sampleTestScheduleNext=null;
					return sampleTestScheduleNext;
				}
			}
			else
			{
				if(sampleTestScheduleFind.getContract().getSuspendFlag()==SuspendFlag.Suspended)
				{
					//System.out.println("Error!!! Contract Suspended.");
					//System.out.println("Find next live contract.");
					
					liveContractListFind = findNextLiveContract(sampleTestScheduleFind, queryScheduleMonthForNextContract);
					
					if (liveContractListFind==null || liveContractListFind.size()==0)
					{
						//System.out.println("No next live contract.");
						sampleTestScheduleNext=null;
						return sampleTestScheduleNext;
					}
				}
				else
				{
					contractEndDateCal.setTime(sampleTestScheduleFind.getContract().getEndDate());
					
					if(nextScheduleMonthCal.compareTo(contractEndDateCal)>=0)
					{
						//System.out.println("Error!!! Next Schedule Month > Contract End Date.");
						//System.out.println("Find next live contract.");
						
						liveContractListFind = findNextLiveContract(sampleTestScheduleFind, queryScheduleMonthForNextContract);
						
						if (liveContractListFind==null || liveContractListFind.size()==0)
						{
							//System.out.println("No next live contract.");
							sampleTestScheduleNext=null;
							return sampleTestScheduleNext;
						}
					}
				}
			}
		}
		else if(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType().equals(OrderType.DirectPurchase.getDataValue()))
		{
			if(testFrequencyListFind.get(0).getPercentage()!=100){
				//System.out.println("Error!!! Selection Percentage not 100%.");
				sampleTestScheduleNext=null;
				return sampleTestScheduleNext;
			}
		}
		
		////////////////////////////////////////////
		/// Start to set Next Schedule
		////////////////////////////////////////////
		sampleTestScheduleNext = constructSampleTestScheduleNext(sampleTestScheduleFind, testFrequencyListFind, liveContractListFind, nextScheduleMonthCal, nowDateCal, todayDate);
		
		success = true;	
		return sampleTestScheduleNext;
	}
	
	public Calendar constructScheduleMonthForNextContract(Calendar nextScheduleMonthCalForNextContract, Calendar nowDateCalForNextContract) 
	{
		if(nextScheduleMonthCalForNextContract.compareTo(nowDateCalForNextContract)>=0){
			return nextScheduleMonthCalForNextContract;			
		}
		else{
			return nowDateCalForNextContract;
		}
	}
	
	private SampleTestSchedule constructSampleTestScheduleNext(SampleTestSchedule sampleTestScheduleFind, List<TestFrequency> testFrequencyListFind, List<Contract> liveContractListFind, Calendar nextScheduleMonthCal, Calendar nowDateCal, Calendar todayDate)
	{
		SampleTestSchedule sampleTestScheduleNextIn = new SampleTestSchedule();
		sampleTestScheduleNextIn.setSampleItem(sampleTestScheduleFind.getSampleItem());
		sampleTestScheduleNextIn.setTestFrequency(testFrequencyListFind.get(0));
		sampleTestScheduleNextIn.setSampleTest(sampleTestScheduleFind.getSampleTest());
		
		if (liveContractListFind==null || liveContractListFind.size()==0)
		{
			sampleTestScheduleNextIn.setContract(sampleTestScheduleFind.getContract());
		}
		else
		{
			sampleTestScheduleNextIn.setContract(liveContractListFind.get(0));
		}
		
		if(!sampleTestScheduleFind.getOrderType().getDataValue().equals(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType()))
		{
			if(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType().equals( OrderType.DirectPurchase.getDataValue()) )
			{
				sampleTestScheduleNextIn.getContract().setContractNum(null);
				sampleTestScheduleNextIn.getContract().setContractSuffix(null);
				sampleTestScheduleNextIn.getContract().setStartDate(null);
				sampleTestScheduleNextIn.getContract().setEndDate(null);
				sampleTestScheduleNextIn.getContract().setPackSize(null);
				sampleTestScheduleNextIn.getContract().setPackPrice(null);
			}
			
			sampleTestScheduleNextIn.getContract().setContractType(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType());
		}
		
		nextScheduleMonthCal.set(nextScheduleMonthCal.get(Calendar.YEAR), nextScheduleMonthCal.get(Calendar.MONTH), 1);
		nowDateCal.set(nowDateCal.get(Calendar.YEAR), nowDateCal.get(Calendar.MONTH), 1);
		
		
		if(nextScheduleMonthCal.compareTo(nowDateCal)>=0){
			sampleTestScheduleNextIn.setScheduleMonth(nextScheduleMonthCal.getTime());			
		}
		else{
			sampleTestScheduleNextIn.setScheduleMonth(nowDateCal.getTime());
		}
			
		sampleTestScheduleNextIn.setScheduleStatus(ScheduleStatus.Schedule);
		sampleTestScheduleNextIn.setScheduleType(ScheduleType.System);
		sampleTestScheduleNextIn.setRecordStatus(RecordStatus.Active);
		sampleTestScheduleNextIn.setOrderType(OrderType.findByDataValue(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType()));
		sampleTestScheduleNextIn.setUnitPrice(sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureSummary().getAverageUnitPrice());
		sampleTestScheduleNextIn.setScheduleConfirmDate(todayDate.getTime());
		sampleTestScheduleNextIn.setLastSchedule(sampleTestScheduleFind.getScheduleMonth());
		sampleTestScheduleNextIn.setLastDocRequestDate(sampleTestScheduleFind.getLastDocRequestDate());
		sampleTestScheduleNextIn.setCancelSchedReason(CancelSchedReason.NullValue);
			
		if(sampleTestScheduleNextIn.getContract().getSupplier()!= null)
		{
			sampleTestScheduleNextIn.getContract().getSupplier().getContact();
		}
		
		if(sampleTestScheduleNextIn.getContract().getManufacturer()!= null)
		{
			sampleTestScheduleNextIn.getContract().getManufacturer().getContact();
		}
		
		if(sampleTestScheduleNextIn.getContract().getPharmCompany()!= null)
		{
			sampleTestScheduleNextIn.getContract().getPharmCompany().getContact();
		}
		
		sampleTestScheduleNextIn.setInstitution(null);
		sampleTestScheduleNextIn.setPayment(null);
		sampleTestScheduleNextIn.setLab(null);
		sampleTestScheduleNextIn.setMicroBioTest(null);
		sampleTestScheduleNextIn.setChemicalAnalysis(null);
		
		return sampleTestScheduleNextIn;
	}
	
	public List<Contract> findNextLiveContract(SampleTestSchedule sampleTestScheduleFind, Calendar queryScheduleMonthForNextContract){
		List<Contract> liveContractListFind = em.createNamedQuery("Contract.findNextLiveContractByItemCodeOrderTypeScheduleMonthModuleType")
			.setParameter(ITEM_CODE, sampleTestScheduleFind.getSampleItem().getItemCode())
			.setParameter("orderType", sampleTestScheduleFind.getSampleItem().getDmDrug().getDmProcureInfo().getOrderType())
			.setParameter("scheduleMonth", queryScheduleMonthForNextContract.getTime())
			.setParameter("moduleType", ModuleType.All)
			.setParameter("suspendFlag", SuspendFlag.Unsuspended)
			.setHint(QueryHints.FETCH, "o.supplier")
			.setHint(QueryHints.FETCH, "o.manufacturer")
			.setHint(QueryHints.FETCH, "o.pharmCompany")
			.getResultList();
			
			return liveContractListFind;
	}
	
	@SuppressWarnings("unchecked")
	public SampleTestSchedule checkForInsertNextSampleTestScheduleByErpContractTestCode(hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl, String testCode){
		logger.debug("checkForInsertNextSampleTestScheduleByErpContractTestCode");
		SampleTestSchedule sampleTestScheduleNew = new SampleTestSchedule();
		List<SampleTestSchedule> sampleTestScheduleListFind = null;
		List<TestFrequency> testFrequencyListFind = null;
		SampleItem sampleItemFind = null;
		RiskLevel riskLevelFind = null;
		SampleTestSchedule latestPCSampleTestScheduleFind = null; 
		Double frequencyPerYear;
		
		Calendar contractStartDateCal = Calendar.getInstance();
		Calendar contractEndDateCal = Calendar.getInstance();
		Calendar todayDate = Calendar.getInstance();
		Calendar nextScheduleMonthCal = Calendar.getInstance();
		////////////////////////////////////////////
		/// Check if there is outstanding schedule
		////////////////////////////////////////////
		sampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findOutstandingScheduleByItemCodeTestCodeRecordStatus")
									.setParameter(ITEM_CODE, cl.getItemCode())
									.setParameter(TEST_CODE, testCode)
									.setParameter(RECORD_STATUS, RecordStatus.Active)
									.setParameter("scheduleStatusP",ScheduleStatus.Payment)
									.setParameter("scheduleStatusC", ScheduleStatus.Complete)
									.setParameter("actionFlag", "Y")
									.setHint(QueryHints.FETCH, "o.sampleTest")
									.setHint(QueryHints.FETCH, "o.contract")
									.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
									.setHint(QueryHints.BATCH, "o.institution")
									.setHint(QueryHints.BATCH, "o.contract.supplier")
									.setHint(QueryHints.BATCH, "o.contract.manufacturer")
									.setHint(QueryHints.BATCH, "o.contract.pharmCompany")
									.getResultList();
		
		if(sampleTestScheduleListFind!=null)
		{
			if(sampleTestScheduleListFind.size()>0)
			{
				//System.out.println("Error!!! Outstanding Schedule exist.");
				return null;
			}
		}
		
	
		////////////////////////////////////////////
		/// Check if item exist and valid
		////////////////////////////////////////////
		sampleItemFind = checkSampleItemValid(cl.getItemCode(), RecordStatus.Active);
		if(sampleItemFind==null)
		{	
			sampleTestScheduleNew=null;
			return null;
		}
		
		
		////////////////////////////////////////////
		/// Check if there is exclusion record
		////////////////////////////////////////////
		if(!checkExclusionTestValid(cl.getItemCode(), testCode, RecordStatus.Active))
		{
			sampleTestScheduleNew=null;
			return null;
		}
		
		
		/////////////////////////////////////////////////////////////
		/// Check if the risk level proceedSampleTestFlag is Y
		//////////////////////////////////////////////////////////////
		riskLevelFind = checkRiskLevelValid(sampleItemFind.getRiskLevel().getRiskLevelCode());
		if(riskLevelFind==null)
		{	
			sampleTestScheduleNew=null;
			return null;
		}
		
		////////////////////////////////////////////////////////////////////////
		/// retrieve test frequency by test code, order type, risk level
		///////////////////////////////////////////////////////////////////
		testFrequencyListFind = testFrequencyListService.checkTestFrequencyValid(testCode, sampleItemFind.getRiskLevel().getRiskLevelCode(), sampleItemFind.getDmDrug().getDmProcureInfo().getOrderType());
		
		if(testFrequencyListFind==null || testFrequencyListFind.size()==0)
		{
			sampleTestScheduleNew=null;
			return null;
		}
		else{
			frequencyPerYear = Double.parseDouble(testFrequencyListFind.get(0).getFrequencyPerYear().getDataValue());
		}
		
		//////////////////////////////////////////
		/// Check for Contract period
		//////////////////////////////////////////
		contractStartDateCal.setTime(ch.getEffectiveStartDate());
		
		if(cl.getEffectiveEndDate()==null)
		{
			contractEndDateCal.setTime(ch.getEffectiveEndDate());
		}
		else
		{
			contractEndDateCal.setTime(cl.getEffectiveEndDate());
		}
		
		if(todayDate.compareTo(contractEndDateCal)>=0)
		{
			//System.out.println("Error!!! contract expire.");
			sampleTestScheduleNew=null;
			return null;
		}
		
		///////////////////////////////////////////////////////
		///// Find Latest Schedule (P,C) by itemcode, testcode
		///////////////////////////////////////////////////////
		latestPCSampleTestScheduleFind = retrieveLatestPCSampleTestSchedule(cl.getItemCode(), testCode);
		if(latestPCSampleTestScheduleFind!=null)
		{
			nextScheduleMonthCal.setTime(latestPCSampleTestScheduleFind.getScheduleMonth());
			nextScheduleMonthCal.add(Calendar.MONTH, (int)(12/frequencyPerYear));
		}
		
		if(nextScheduleMonthCal.compareTo(contractEndDateCal)>=0)
		{
			//System.out.println("Error!!! next schedule month > contract end date.");
			sampleTestScheduleNew=null;
			return null;
		}
		
		if(todayDate.compareTo(nextScheduleMonthCal)>=0)
		{
			nextScheduleMonthCal = Calendar.getInstance();
		}
		
		if(contractStartDateCal.compareTo(nextScheduleMonthCal)>=0)
		{
			nextScheduleMonthCal.setTime(ch.getEffectiveStartDate());
		}
		
		nextScheduleMonthCal.set(nextScheduleMonthCal.get(Calendar.YEAR), nextScheduleMonthCal.get(Calendar.MONTH), 1);

		///////////////////////////////////////////////////////////////////////////////////////
		////////Check if it is the smallest pack size of the contract of same item in same day
		//////////////////////////////////////////////////////////////////////////////////////
		SampleTestSchedule sampleTestScheduleWithActionFlag = retrieveSampleTestScheduleByItemTestActionFlag(cl.getItemCode(), testCode, "Y");
		{
			Calendar scheduleMonthWithActionFlagCal = Calendar.getInstance();
			
			if(sampleTestScheduleWithActionFlag!=null)
			{
				scheduleMonthWithActionFlagCal.setTime(sampleTestScheduleWithActionFlag.getScheduleMonth());
				if(sampleTestScheduleWithActionFlag.getContract().getContractNum().equals(ch.getContractNum()))
				{
					if(cl.getPackSize() < sampleTestScheduleWithActionFlag.getContract().getPackSize())
					{
						sampleTestScheduleService.updateNextSampleTestScheduleByErpContract(sampleTestScheduleWithActionFlag, ch, cl);
						return null;
					}
				}
				else
				{
					if(scheduleMonthWithActionFlagCal.compareTo(nextScheduleMonthCal)>0)
					{
						sampleTestScheduleService.updateNextSampleTestScheduleByErpContract(sampleTestScheduleWithActionFlag, ch, cl);
						return null;
					}
				}
			}
		}

		////////////////////////////////////
		/// Pass and Create Next Schedule 
		////////////////////////////////////
		sampleTestScheduleNew.setScheduleMonth(nextScheduleMonthCal.getTime());
		sampleTestScheduleNew.setScheduleStatus(ScheduleStatus.ScheduleGen);
		sampleTestScheduleNew.setScheduleType(ScheduleType.System);
		sampleTestScheduleNew.setOrderType(OrderType.findByDataValue(sampleItemFind.getDmDrug().getDmProcureInfo().getOrderType()));
		sampleTestScheduleNew.setUnitPrice(sampleItemFind.getDmDrug().getDmProcureSummary().getAverageUnitPrice());
		if(latestPCSampleTestScheduleFind!=null)
		{
			sampleTestScheduleNew.setLastSchedule(latestPCSampleTestScheduleFind.getScheduleMonth());
			sampleTestScheduleNew.setLastDocRequestDate(latestPCSampleTestScheduleFind.getLastDocRequestDate());
		}
		sampleTestScheduleNew.setActionFlag("Y");
		sampleTestScheduleNew.setRecordStatus(RecordStatus.Active);
		sampleTestScheduleNew.setCancelSchedReason(CancelSchedReason.NullValue);
		
		sampleTestScheduleNew.setSampleItem(sampleItemFind);
		sampleTestScheduleNew.setTestFrequency(testFrequencyListFind.get(0));
		sampleTestScheduleNew.setSampleTest(testFrequencyListFind.get(0).getSampleTest());
		
		sampleTestScheduleNew.setInstitution(null);
		sampleTestScheduleNew.setPayment(null);
		sampleTestScheduleNew.setLab(null);
		sampleTestScheduleNew.setMicroBioTest(null);
		sampleTestScheduleNew.setChemicalAnalysis(null);
		
		return sampleTestScheduleNew;
	}
	
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	public SampleItem checkSampleItemValid(String itemCode, RecordStatus recordStatus){
		List<SampleItem> sampleItemListFind = em.createNamedQuery("SampleItem.findByItemCodeRecordStatus")
				.setParameter(ITEM_CODE, itemCode)
				.setParameter(RECORD_STATUS, recordStatus)
				.getResultList();
		
		if (sampleItemListFind==null || sampleItemListFind.size()==0){
			//System.out.println("Error!!! Sample Item not exist.");
			return null;
		}
		else{
			////////////////////////////////////////////
			/// Check RegStatus
			////////////////////////////////////////////
			SampleItem sampleItemFind = sampleItemListFind.get(0);
			
			if(sampleItemFind.getDmDrug()==null)
			{
				return null;
			}
			else if(!("N").equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getRegStatus()) && !("R").equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getRegStatus()))
			{
				//System.out.println("Invalid RegStatus.");
				return null;
			}
			else if(!("Y").equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getReleaseIndicator()) && !("X").equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getReleaseIndicator()))
			{
				//System.out.println("Invalid Pcu Release Flag.");
				return null;
			}
			else{
				return sampleItemFind;}
		}
	}
	
	public Boolean checkExclusionTestValid(String itemCode, String testCode, RecordStatus recordStatus){
		List<ExclusionTest> exclusionTestListFind = em.createNamedQuery("ExclusionTest.findByItemCodeTestCodeRecordStatus")
				.setParameter(ITEM_CODE, itemCode)
				.setParameter(TEST_CODE, testCode)
				.setParameter(RECORD_STATUS, recordStatus)
				.getResultList();
		
		if (exclusionTestListFind!=null){ 
			if (exclusionTestListFind.size()>0){
				//System.out.println("No New Schedule Insert!!! Exclusion record exist.");
				return false;
			}
		}
		return true;
	}
	
	public RiskLevel checkRiskLevelValid(String riskLevelCode){
		List<RiskLevel> riskLevelListFind = em.createNamedQuery("RiskLevel.findByRiskLevelCode")
				.setParameter("riskLevelCode", riskLevelCode)
				.getResultList();
		
		if(riskLevelListFind==null || riskLevelListFind.size()==0)
		{
			//System.out.println("Error!!! Risk Level not found.");
			return null;
		}
		else
		{
			if(riskLevelListFind.get(0).getRecordStatus().equals(RecordStatus.Deleted))
			{
				//System.out.println("Error!!! Risk Level is deleted.");
				return null;
			}
			else if (riskLevelListFind.get(0).getProceedSampleTestFlag().equals(YesNoFlag.No))
			{
				//System.out.println("Error!!! Risk Level - ProceedSampleTest is No.");
				return null;
			}
			else
			{
				return riskLevelListFind.get(0);
			}
		}
	}
	
	public SampleTestSchedule retrieveLatestPCSampleTestSchedule(String itemCode, String testCode){
		List<SampleTestSchedule> latestSampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findByItemCodeTestCodeScheduleStatusRecordStatus")
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter("scheduleStatusP", ScheduleStatus.Payment)
			.setParameter("scheduleStatusC", ScheduleStatus.Complete)
			.setParameter(TEST_CODE, testCode)
			.setParameter(ITEM_CODE, itemCode)
			.getResultList();
	
		if(latestSampleTestScheduleListFind==null || latestSampleTestScheduleListFind.size()==0)
		{
			return null;
		}
		else
		{
			return getLazySampleTestSchedule(latestSampleTestScheduleListFind.get(0));
		}
	}
	
	public SampleTestSchedule retrieveSampleTestScheduleByItemTestActionFlag(String itemCode, String testCode, String actionFlag){
		Calendar modifyDateSearch = Calendar.getInstance();
		modifyDateSearch.add(Calendar.DATE, -1);
		List<SampleTestSchedule> sampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findByItemTestActionFlagModifyDate")
			.setParameter("actionFlag",actionFlag)
			.setParameter("modifyDate", modifyDateSearch.getTime())
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter("scheduleStatus", ScheduleStatus.ScheduleGen)
			.setParameter(ITEM_CODE, itemCode)
			.setParameter(TEST_CODE, testCode)
			.setHint(QueryHints.FETCH, "o.sampleTest")
			.setHint(QueryHints.FETCH, "o.contract")
			.setHint(QueryHints.FETCH, "o.contract.supplier")
			.setHint(QueryHints.FETCH, "o.contract.manufacturer")
			.setHint(QueryHints.FETCH, "o.contract.pharmCompany")
			.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
			.getResultList();
	
		if(sampleTestScheduleListFind==null || sampleTestScheduleListFind.size()==0)
		{
			return null;
		}
		else
		{
			return getLazySampleTestSchedule(sampleTestScheduleListFind.get(0));
		}
	}
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public SampleTestSchedule getSampleTestScheduleNext(){
		return sampleTestScheduleNext;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	@Remove
	public void destory(){
		if (sampleTestScheduleNext!=null){
			sampleTestScheduleNext = null;
		}
	}
	
}
