package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemCriteria;

import javax.ejb.Local;

@Local
public interface PharmProblemListServiceLocal {
	
	void retrievePharmProblemListByPharmProblemCriteria(PharmProblemCriteria pharmProblemCriteriaIn);
	void destroy();

}
