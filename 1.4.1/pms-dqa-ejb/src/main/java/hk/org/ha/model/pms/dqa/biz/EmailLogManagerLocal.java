package hk.org.ha.model.pms.dqa.biz;
import java.util.Map;

import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.udt.EmailType;

import javax.ejb.Local;

@Local
public interface EmailLogManagerLocal {

	EmailLog createEmailLog(final EaiMessage eaiMessage, final Map attHashMap, EmailType emailType );
	
	EmailLog createEmailLogByLetterTemplate(final LetterTemplate letterTemplate, EmailType emailType);
	
}
