package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspQtyRpt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("batchSuspListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class BatchSuspListServiceBean implements BatchSuspListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<BatchSusp> batchSuspList;
	
	private static final String REPORT_NAME = "Batch Suspension Quantity Report";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	
	private String reportParameters = "";
	private String retrieveDate;
	private SimpleDateFormat sdf;
	
	private List<BatchSuspQtyRpt> batchSuspQtyRptList;

	@SuppressWarnings("unchecked")
	public void retrieveBatchSuspListByBatchSuspCriteria(BatchSuspCriteria batchSuspCriteria){
		logger.debug("retrieveBatchSuspListByBatchSuspCriteria");
		
		StringBuffer batchSuspListSql = new StringBuffer();
		batchSuspListSql.append("select o from BatchSusp o "); 
		batchSuspListSql.append("where (o.qaBatchNum.qaProblem.caseNum = :caseNum or :caseNum is null) ");
		batchSuspListSql.append("and o.qaBatchNum.qaProblem.recordStatus = :recordStatus ");
		
		if(batchSuspCriteria.getPcu()!=null){
			batchSuspListSql.append("and (o.institution.pcuCode = :pcuCode or :pcuCode is null) ");}
		if(batchSuspCriteria.getInstitution()!=null){
			batchSuspListSql.append("and (o.institution.institutionCode = :institutionCode or :institutionCode is null) ");}

		batchSuspListSql.append("order by o.institution.institutionCode asc, o.qaBatchNum.batchNum asc ");
		
		Query q = em.createQuery(batchSuspListSql.toString())
		.setParameter("caseNum", batchSuspCriteria.getCaseNum())
		.setParameter("recordStatus", RecordStatus.Active);

		if(batchSuspCriteria.getPcu()!=null){
			q=q.setParameter("pcuCode", batchSuspCriteria.getPcu().getPcuCode());}			
		if(batchSuspCriteria.getInstitution()!=null){
			q=q.setParameter("institutionCode", batchSuspCriteria.getInstitution().getInstitutionCode());}
		
		q=q.setHint(QueryHints.FETCH, "o.institution");
		q=q.setHint(QueryHints.FETCH, "o.qaBatchNum");
		q=q.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem");
		q=q.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.problemHeader");
		
		batchSuspList = q.getResultList();
		
		if(batchSuspList==null || batchSuspList.size()==0)
		{
			batchSuspList=null;
		}
	}
	
	public void retrieveBatchSuspQtyRptByBatchSuspList(List<BatchSusp> batchSuspList)
	{
		batchSuspQtyRptList = new ArrayList<BatchSuspQtyRpt>();
		for (BatchSusp batchSusp:batchSuspList)
		{
			BatchSuspQtyRpt rpt = new BatchSuspQtyRpt();
			rpt.setPcuCode(batchSusp.getInstitution().getPcuCode());
			rpt.setInst(batchSusp.getInstitution().getInstitutionCode());
			rpt.setCaseNum(batchSusp.getQaBatchNum().getQaProblem().getCaseNum());
			rpt.setItemCode(batchSusp.getQaBatchNum().getQaProblem().getProblemHeader().getItemCode());
			rpt.setItemDesc(batchSusp.getQaBatchNum().getQaProblem().getProblemHeader().getDmDrug().getFullDrugDesc());
			rpt.setClassification(batchSusp.getQaBatchNum().getQaProblem().getClassification()==null?"":(batchSusp.getQaBatchNum().getQaProblem().getClassification().getClassificationCode()==null?"":(batchSusp.getQaBatchNum().getQaProblem().getClassification().getClassificationCode().getDataValue())));
			rpt.setBatchNum(batchSusp.getQaBatchNum().getBatchNum());
			rpt.setQtyOnHand(batchSusp.getBatchSuspQtyOnHand().toString());
			rpt.setUpdateDate(batchSusp.getModifyDate());
			
			batchSuspQtyRptList.add(rpt);
		}
	}
	
	public List<BatchSuspQtyRpt> getBatchSuspQtyRptList()
	{
		return batchSuspQtyRptList;
	}
	
	public String getReportName() 
	{
		return REPORT_NAME;
	}

	public String getReportParameters() 
	{
		return reportParameters;
	}

	public String getRetrieveDate() 
	{
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}
	
    @Remove
	public void destroy(){	
		if (batchSuspList != null){
			batchSuspList = null;
		}
	}

}
