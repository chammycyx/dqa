package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.udt.coa.MissingBatchCoaDateType;
import hk.org.ha.model.pms.dqa.vo.coa.MissingBatchCoaRptData;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface MissingBatchCoaRptServiceLocal {
	
	void retrieveMissingBatchCoaRptList(MissingBatchCoaDateType dateType, Date fromDate, Date toDate);
	
	List<MissingBatchCoaRptData> getMissingBatchCoaRptList();
	
	String getReportName();
	
	String getReportParameters();
	
	String getRetrieveDate();
	
	void destroy();
	
}
