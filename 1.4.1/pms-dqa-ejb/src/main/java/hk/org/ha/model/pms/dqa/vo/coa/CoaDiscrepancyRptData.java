package hk.org.ha.model.pms.dqa.vo.coa;

public class CoaDiscrepancyRptData {
	
	private String supplierCode = "";
	private String itemCode = "";
	private int	   discrepancyUnderGrouping = 0;
	private int    pharmacistFollowUp = 0;
	private int    emailToSupplier = 0;
	private int    reminderToSupplier1st = 0;
	private int    reminderToSupplier2nd = 0;
	private int    reminderToSupplierFuther = 0;	
	
	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getDiscrepancyUnderGrouping() {
		return discrepancyUnderGrouping;
	}

	public void setDiscrepancyUnderGrouping(int discrepancyUnderGrouping) {
		this.discrepancyUnderGrouping = discrepancyUnderGrouping;
	}

	public int getPharmacistFollowUp() {
		return pharmacistFollowUp;
	}

	public void setPharmacistFollowUp(int pharmacistFollowUp) {
		this.pharmacistFollowUp = pharmacistFollowUp;
	}

	public int getEmailToSupplier() {
		return emailToSupplier;
	}

	public void setEmailToSupplier(int emailToSupplier) {
		this.emailToSupplier = emailToSupplier;
	}

	public int getReminderToSupplier1st() {
		return reminderToSupplier1st;
	}

	public void setReminderToSupplier1st(int reminderToSupplier1st) {
		this.reminderToSupplier1st = reminderToSupplier1st;
	}

	public int getReminderToSupplier2nd() {
		return reminderToSupplier2nd;
	}

	public void setReminderToSupplier2nd(int reminderToSupplier2nd) {
		this.reminderToSupplier2nd = reminderToSupplier2nd;
	}

	public int getReminderToSupplierFuther() {
		return reminderToSupplierFuther;
	}

	public void setReminderToSupplierFuther(int reminderToSupplierFuther) {
		this.reminderToSupplierFuther = reminderToSupplierFuther;
	}
	
}
