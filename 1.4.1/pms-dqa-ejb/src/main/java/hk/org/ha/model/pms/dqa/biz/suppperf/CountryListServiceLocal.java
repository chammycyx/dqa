package hk.org.ha.model.pms.dqa.biz.suppperf;
import javax.ejb.Local;

@Local
public interface CountryListServiceLocal {

	void retrieveCountryListLikeCountryCode(String countryCode);
	
    void destroy();
}
