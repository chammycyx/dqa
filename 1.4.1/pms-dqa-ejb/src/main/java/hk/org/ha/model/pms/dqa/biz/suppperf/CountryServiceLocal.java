package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;

import javax.ejb.Local;


@Local
public interface CountryServiceLocal {
	
	void retrieveCountryByCountryCode(String countryCode);
	
	Country findCountryByCountryCode(String countryCode);
	
    void destroy();
}
