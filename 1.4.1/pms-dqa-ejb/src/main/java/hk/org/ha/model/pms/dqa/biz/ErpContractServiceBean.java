package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.CancelFlag;
import hk.org.ha.model.pms.dqa.udt.ClosureStatus;
import hk.org.ha.model.pms.dqa.udt.ContractStatus;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("erpContractService")
@RemoteDestination
@MeasureCalls
public class ErpContractServiceBean implements ErpContractServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;	
	
	@In(create=true)
	private SupplierServiceLocal supplierService;

	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In(create=true)
	private CompanyServiceLocal companyService;
	
	@In(create=true)
	private ErpContractMspServiceLocal erpContractMspService;
	
	@In(create=true)
	private ErpContractListServiceLocal erpContractListService;
	
	private static final String DOES_NOT_EXIST = " does not exist";
	
	public void createContract( hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl ) { 
		logger.debug("createContract #0 #1", ch.getContractHeaderId(), cl.getContractLineId());
		Contract c = new Contract();
		logger.debug("ch.getContractNum() #0", ch.getContractNum());
		logger.debug("ch.getPhsContractNum() #0", ch.getPhsContractNum());
		c.setBuyQty( cl.getBuyQty() );
		c.setContractNum( ch.getContractNum() );
		c.setPhsContractNum( ch.getPhsContractNum() );
		c.setContractStatus( ch.getContractStatus() );
		logger.debug("check value of ch.getContractNum() : #0", ch.getContractNum());
		
		c.setContractSuffix( cl.getContractLineNum() );
		c.setContractType( ch.getContractType() );
		c.setCurrencyCode( ch.getCurrency() );
		
		DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(cl.getItemCode());
		
		if(dmDrug == null) {
			logger.debug("drug item is null");
			throw new NoResultException("drug item "+cl.getItemCode()+" is null");
		}
		c.setItemCode(cl.getItemCode());
		c.postLoad();
		c.setBaseUnit( dmDrug.getBaseUnit());
		c.setStartDate( ch.getEffectiveStartDate() );
		
		if( cl.getEffectiveEndDate() == null ) {
			c.setEndDate( ch.getEffectiveEndDate() );
		} else {
			c.setEndDate( cl.getEffectiveEndDate() );
		}
		
		c.setErpContractLineId( cl.getContractLineId() );
		c.setGetFreeQty( cl.getGetFreeQty() );
		
		String manufCode = cl.getContractMsps().get(0).getManufCode();
		Company manufacturer = companyService.retrieveCompanyByCompanyCodeManufFlag( manufCode, YesNoFlag.Yes );		
		if( manufacturer == null ) { 
			logger.error("Manufacturer code:#0 does not exist", manufCode); 
			throw new NoResultException("Manufacturer code:"+manufCode+DOES_NOT_EXIST);
		}		
		c.setManufacturer( manufacturer );
		
		String pharmCompanyCode = cl.getContractMsps().get(0).getPharmCompanyCode();
		Company pharmCompany = companyService.retrieveCompanyByCompanyCodePharmCompanyFlag( pharmCompanyCode, YesNoFlag.Yes );
		if( pharmCompany == null ) { 
			logger.error("Pharm company code:#0 does not exist", pharmCompanyCode); 
			throw new NoResultException("Pharm company code:"+pharmCompanyCode+DOES_NOT_EXIST);
		}
		c.setPharmCompany( pharmCompany );
		
		c.setModuleType( ModuleType.All );
		c.setPackPrice( cl.getUnitPrice().doubleValue() );
		c.setPackSize( cl.getPackSize() );
		c.setPaymentTerm( ch.getPaymentTerms() );
		
		Supplier supplier = supplierService.retrieveSupplierForErp( ch.getSupplierCode() );
		if( supplier == null ) { 
			logger.error("supplier code:#0 does not exist", ch.getSupplierCode()); 
			throw new NoResultException("supplier code:"+ch.getSupplierCode()+DOES_NOT_EXIST);
		}
		c.setSupplier( supplier );
		
		c.setSuspendFlag( SuspendFlag.Unsuspended );
		c.setUomCode( cl.getUnitOfMeasure() );				
		
		em.persist( c );		
		em.flush();
		
		for( hk.org.ha.model.pms.dqa.vo.ContractMsp cm:cl.getContractMsps() ) {
			erpContractMspService.createContractMsp( c, cl, cm );
		}
		
	}			
	
	public void updateContract( Contract c, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl ) {
		logger.debug("updateContract #0 #1 #2", c.getContractNum(), ch.getContractHeaderId(), cl.getContractLineId());	
		c.setBuyQty( cl.getBuyQty() );
		c.setContractType( ch.getContractType() );
		c.setPhsContractNum( ch.getPhsContractNum() );
		c.setCurrencyCode( ch.getCurrency() );
		c.setStartDate( ch.getEffectiveStartDate() );
		if( cl.getEffectiveEndDate() == null ) {
			c.setEndDate( ch.getEffectiveEndDate() );
		} else {
			c.setEndDate( cl.getEffectiveEndDate() );
		}
		
		String manufCode = cl.getContractMsps().get(0).getManufCode();
		Company manufacturer = companyService.retrieveCompanyByCompanyCodeManufFlag( manufCode, YesNoFlag.Yes );		
		if( manufacturer == null ) { 
			logger.error("Manufacturer code:#0 does not exist", manufCode); 
			throw new NoResultException("Manufacturer code:"+manufCode+DOES_NOT_EXIST);
		}		
		c.setManufacturer( manufacturer );
		
		String pharmCompanyCode = cl.getContractMsps().get(0).getPharmCompanyCode();
		Company pharmCompany = companyService.retrieveCompanyByCompanyCodePharmCompanyFlag( pharmCompanyCode, YesNoFlag.Yes );
		if( pharmCompany == null ) { 
			logger.error("Pharm company code:#0 does not exist", pharmCompanyCode); 
			throw new NoResultException("Pharm company code:"+pharmCompanyCode+DOES_NOT_EXIST);
		}
		c.setPharmCompany( pharmCompany );
		
		Supplier supplier = supplierService.retrieveSupplierForErp( ch.getSupplierCode() );
		if( supplier == null ) { 
			logger.error("supplier code:#0 does not exist", ch.getSupplierCode()); 
			throw new NoResultException("supplier code:"+ch.getSupplierCode()+DOES_NOT_EXIST);
		}
		c.setSupplier( supplier );
		
		c.setGetFreeQty( cl.getGetFreeQty() );
		c.setPackSize(  cl.getPackSize().intValue() );
		c.setPackPrice( cl.getUnitPrice().doubleValue() );
		c.setUomCode( cl.getUnitOfMeasure() );
		c.setPaymentTerm( ch.getPaymentTerms() );
		em.merge( c );
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public Contract retrieveContractByErpContractLineId( Long erpContractLineId ) {
		logger.debug("retrieveContractByErpContractLineId #0", erpContractLineId);
		List<Contract> contractList = em.createNamedQuery("Contract.findByErpContractLineIdModuleType")
								.setParameter("erpContractLineId", erpContractLineId)
								.setParameter("moduleType", ModuleType.All)
								.getResultList();
		return ( contractList.size() > 0 ) ? contractList.get( 0 ): null;
	}
	

	public void updateContractForSuspension ( Contract contract ) {
		logger.debug("updateContractForSuspension #0#1", contract.getContractNum()+contract.getContractSuffix());
		contract.setSuspendFlag( SuspendFlag.Suspended );
		em.merge( contract );
		em.flush();
	}
	
	public void updateContractStatus( hk.org.ha.model.pms.dqa.vo.Contract ct ) {
		logger.debug("updateContractStatus");
		for( ContractLine cl : ct.getContractLines() ) {
			Contract c = retrieveContractByErpContractLineId( cl.getContractLineId() );
			if( c != null ){
				c.setContractStatus( ct.getContractStatus() );			
				em.merge( c );
			} else {
				logger.info("Contract Line ID #0 not exist", cl.getContractLineId());
			}
		}
		em.flush();
	}
	
	public void updateContractLatestEndDate( hk.org.ha.model.pms.dqa.vo.Contract ch, Map<String, Date> endDateMap ) {
		logger.debug("updateContractForLatestEndDate #0 ", ch.getContractHeaderId());
		
		List<Contract> contractList = erpContractListService.retrieveContractListByContractNumModuleType( 
				ch.getContractNum().trim(), 
				ModuleType.COA );
		
		for (Contract c:contractList) {
			if (endDateMap.containsKey(c.getItemCode())) {
				logger.debug("contractNum #0, itemCode #1, contractId #2 update endDate #3 to #4 ",
						c.getContractNum(), c.getItemCode(), c.getContractId(), c.getEndDate(), endDateMap.get(c.getItemCode()) );
				
				c.setEndDate(endDateMap.get(c.getItemCode()));
				em.merge( c );
			}
		}
		
		em.flush();
	}
	
	public void createContractForSampleTest( hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl ) { 
		
	}
	
	public void updateContractForSampleTest( SampleTestSchedule sampleTestSchedule, hk.org.ha.model.pms.dqa.vo.Contract ch, ContractLine cl ) {
		logger.debug("updateContractForSampleTest #0 #1 #2", sampleTestSchedule.getContract().getContractNum(), ch.getContractHeaderId(), cl.getContractLineId());	
		
		Contract c = sampleTestSchedule.getContract();
		c.setContractStatus(ch.getContractStatus());
		
		if( ContractStatus.Approved.getDataValue().equals( ch.getContractStatus() ) )
		{
			if( CancelFlag.Y == cl.getCancelFlag() || ClosureStatus.CLOSED == cl.getClosureStatus() ) {
				c.setSuspendFlag(SuspendFlag.Suspended);
			}
			else{
				c.setSuspendFlag(SuspendFlag.Unsuspended);
			}
		}
		
		c.setStartDate( ch.getEffectiveStartDate() );
		if( cl.getEffectiveEndDate() == null ) {
			c.setEndDate( ch.getEffectiveEndDate() );
		} else {
			c.setEndDate( cl.getEffectiveEndDate() );
		}
		
		if(sampleTestSchedule.getScheduleStatus()== ScheduleStatus.ScheduleGen ||
		   sampleTestSchedule.getScheduleStatus()== ScheduleStatus.Schedule )
		{
			c.setContractType( ch.getContractType() );
			c.setCurrencyCode( ch.getCurrency() );
			
			String manufCode = cl.getContractMsps().get(0).getManufCode();
			Company manufacturer = companyService.retrieveCompanyByCompanyCodeManufFlag( manufCode, YesNoFlag.Yes );		
			if( manufacturer == null ) { 
				logger.error("Manufacturer code:#0 does not exist", manufCode); 
				throw new NoResultException("Manufacturer code:"+manufCode+DOES_NOT_EXIST);
			}		
			c.setManufacturer( manufacturer );
			
			String pharmCompanyCode = cl.getContractMsps().get(0).getPharmCompanyCode();
			Company pharmCompany = companyService.retrieveCompanyByCompanyCodePharmCompanyFlag( pharmCompanyCode, YesNoFlag.Yes );
			if( pharmCompany == null ) { 
				logger.error("Pharm company code:#0 does not exist", pharmCompanyCode); 
				throw new NoResultException("Pharm company code:"+pharmCompanyCode+DOES_NOT_EXIST);
			}
			c.setPharmCompany( pharmCompany );
			
			Supplier supplier = supplierService.retrieveSupplierForErp( ch.getSupplierCode() );
			if( supplier == null ) { 
				logger.error("supplier code:#0 does not exist", ch.getSupplierCode()); 
				throw new NoResultException("supplier code:"+ch.getSupplierCode()+DOES_NOT_EXIST);
			}
			c.setSupplier( supplier );
			
			c.setGetFreeQty( cl.getGetFreeQty() );
			c.setPackSize(  cl.getPackSize().intValue() );
			c.setPackPrice( cl.getUnitPrice().doubleValue() );
			c.setUomCode( cl.getUnitOfMeasure() );
			c.setPaymentTerm( ch.getPaymentTerms() );
		}
		em.merge( c );
		em.flush();
	}
}
