package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum EmailType implements StringValuedEnum {
	
	Incoming("I", "Incoming"),
	Outgoing("O", "Outgoing");	
	
    private final String dataValue;
    private final String displayValue;
        
    EmailType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<EmailType> {

		private static final long serialVersionUID = -4096337290798069102L;

		@Override
    	public Class<EmailType> getEnumClass() {
    		return EmailType.class;
    	}
    }
}
