package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.DocStatus;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioAcceptCriteria;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioNationalPharmStd;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestParam;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "MICRO_BIO_TEST")
@Customizer(AuditCustomizer.class)
public class MicroBioTest extends VersionEntity {

	private static final long serialVersionUID = -7478592805018091618L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "microBioTestSeq")
	@SequenceGenerator(name = "microBioTestSeq", sequenceName = "SEQ_MICRO_BIO_TEST", initialValue=10000)
	@Id
	@Column(name="MICRO_BIO_TEST_ID", nullable=false)
	private Long microBioTestId;

	
	@Converter(name = "MicroBioTest.rawMaterial", converterClass = YesNoFlag.Converter.class )
	@Convert("MicroBioTest.rawMaterial")
	@Column(name="RAW_MATERIAL")
	private YesNoFlag rawMaterial;

	@Converter(name = "MicroBioTest.validationRpt", converterClass = DocStatus.Converter.class )
	@Convert("MicroBioTest.validationRpt")
	@Column(name="VALIDATION_RPT")
	private DocStatus validationRpt;
	
	@Converter(name = "MicroBioTest.proceedSampleTestFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("MicroBioTest.proceedSampleTestFlag")
	@Column(name="VALIDATION_RPT_VALID_FLAG")
	private YesNoFlag validationRptValidityFlag;

	@Converter(name = "MicroBioTest.testParam", converterClass = MicroBioTestParam.Converter.class )
	@Convert("MicroBioTest.testParam")
	@Column(name="TEST_PARAM")
	private MicroBioTestParam testParam;
	
	@Converter(name = "MicroBioTest.nationalPharmStd", converterClass = MicroBioNationalPharmStd.Converter.class )
	@Convert("MicroBioTest.nationalPharmStd")
	@Column(name="NATIONAL_PHARM_STD")
	private MicroBioNationalPharmStd nationalPharmStd;
	
	@Column(name="NATIONAL_PHARM_STD_DESC")
	private String nationalPharmStdDesc;
	
	@Converter(name = "MicroBioTest.acceptCriteria", converterClass = MicroBioAcceptCriteria.Converter.class )
	@Convert("MicroBioTest.acceptCriteria")
	@Column(name="ACCEPT_CRITERIA")
	private MicroBioAcceptCriteria acceptCriteria;
	
	@Column(name="ACCEPT_CRITERIA_DESC")
	private String acceptCriteriaDesc;
	
	@Column(name="SAVE_BEFORE_FLAG")
	private Boolean saveBeforeFlag;
	
	public void setMicroBioTestId(Long microBioTestId) {
		this.microBioTestId = microBioTestId;
	}

	public Long getMicroBioTestId() {
		return microBioTestId;
	}

	public YesNoFlag getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(YesNoFlag rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public DocStatus getValidationRpt() {
		return validationRpt;
	}

	public void setValidationRpt(DocStatus validationRpt) {
		this.validationRpt = validationRpt;
	}

	public void setValidationRptValidityFlag(YesNoFlag validationRptValidityFlag) {
		this.validationRptValidityFlag = validationRptValidityFlag;
	}

	public YesNoFlag getValidationRptValidityFlag() {
		return validationRptValidityFlag;
	}

	public MicroBioNationalPharmStd getNationalPharmStd() {
		return nationalPharmStd;
	}

	public void setNationalPharmStd(MicroBioNationalPharmStd nationalPharmStd) {
		this.nationalPharmStd = nationalPharmStd;
	}
	
	public String getNationalPharmStdDesc() {
		return nationalPharmStdDesc;
	}

	public void setNationalPharmStdDesc(String nationalPharmStdDesc) {
		this.nationalPharmStdDesc = nationalPharmStdDesc;
	}

	public MicroBioAcceptCriteria getAcceptCriteria() {
		return acceptCriteria;
	}

	public void setAcceptCriteria(MicroBioAcceptCriteria acceptCriteria) {
		this.acceptCriteria = acceptCriteria;
	}

	public String getAcceptCriteriaDesc() {
		return acceptCriteriaDesc;
	}

	public void setAcceptCriteriaDesc(String acceptCriteriaDesc) {
		this.acceptCriteriaDesc = acceptCriteriaDesc;
	}

	public void setTestParam(MicroBioTestParam testParam) {
		this.testParam = testParam;
	}

	public MicroBioTestParam getTestParam() {
		return testParam;
	}
	
	public Boolean getSaveBeforeFlag() {
		return saveBeforeFlag;
	}

	public void setSaveBeforeFlag(Boolean saveBeforeFlag) {
		this.saveBeforeFlag = saveBeforeFlag;
	}
}
