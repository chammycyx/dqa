package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Local;

@Local
public interface CompanyServiceLocal {

	Company retrieveCompanyByCompanyCodeManufFlag( String manufCode, YesNoFlag manufFlag );
	
	Company retrieveCompanyByCompanyCodePharmCompanyFlag( String pharmCompanyCode, YesNoFlag pharmCompanyFlag );
	
	void retrieveCompanyByCompanyCode( String pharmCompanyCode );
	
	Company retrieveCompanyForPhs( String companyCode );
	
	Company findManufacturerByCompanyCode(String companyCode);
	
	void destroy();
}
