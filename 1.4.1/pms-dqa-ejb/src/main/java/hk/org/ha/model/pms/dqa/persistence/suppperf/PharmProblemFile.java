package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PHARM_PROBLEM_FILE")
@NamedQueries({
	@NamedQuery(name = "PharmProblemFile.findByCaseNumRecordStatus", query = "select o from PharmProblemFile o where " +
																		"o.pharmProblem.qaProblem.caseNum=:caseNum and " +
																		"o.pharmProblem.qaProblem.recordStatus=:recordStatus and " +
																		"o.pharmProblem.recordStatus=:recordStatus " +
																		"order by o.pharmProblem.problemNum asc ")
})
@Customizer(AuditCustomizer.class)
public class PharmProblemFile extends VersionEntity {

	private static final long serialVersionUID = 5459368650824901823L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmProblemFileSeq")
	@SequenceGenerator(name = "pharmProblemFileSeq", sequenceName = "SEQ_PHARM_PROBLEM_FILE", initialValue=10000)
	@Id
	@Column(name="PHARM_PROBLEM_FILE_ID", nullable=false)
	private Long pharmProblemFileId;
	
	@ManyToOne
	@JoinColumn(name="PHARM_PROBLEM_ID")
	private PharmProblem pharmProblem;
	
	@ManyToOne
	@JoinColumn(name="FILE_ID")
	private FileItem fileItem;
	

	public Long getPharmProblemFileId() {
		return pharmProblemFileId;
	}

	public void setPharmProblemFileId(Long pharmProblemFileId) {
		this.pharmProblemFileId = pharmProblemFileId;
	}

	public PharmProblem getPharmProblem() {
		return pharmProblem;
	}

	public void setPharmProblem(PharmProblem pharmProblem) {
		this.pharmProblem = pharmProblem;
	}

	public FileItem getFileItem() {
		return fileItem;
	}

	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}

}
