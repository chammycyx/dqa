package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OrderType implements StringValuedEnum {
	
	Contract("C", "Contract"),
	StandardQuotation("Q", "Standard Quotation"),
	DirectPurchase("D", "Direct Purchase");
	
    private final String dataValue;
    private final String displayValue;
        
    OrderType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static OrderType findByDataValue(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(OrderType.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<OrderType> {

		private static final long serialVersionUID = -3701625740094095965L;

		@Override
    	public Class<OrderType> getEnumClass() {
    		return OrderType.class;
    	}
    }
}
