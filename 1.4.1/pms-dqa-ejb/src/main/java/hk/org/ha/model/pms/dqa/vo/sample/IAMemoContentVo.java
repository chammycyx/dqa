package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Date;

import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class IAMemoContentVo {
	
	private UserInfo userInfoFax; 
	private SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactFax; 
	private SampleTestSchedule sampleTestScheduleFax;
	private String refNum;
	private String faxMemoParagraph1;
	private String faxMemoParagraph2;
	private String faxMemoParagraph3;
	private String faxMemoParagraph4;
	private String faxMemoParagraphOther;
	private Date sendDate;//DQA-61

	public UserInfo getUserInfoFax() {
		return userInfoFax;
	}
	public void setUserInfoFax(UserInfo userInfoFax) {
		this.userInfoFax = userInfoFax;
	}
	public SupplierPharmCompanyManufacturerContact getSupplierPharmCompanyManufacturerContactFax() {
		return supplierPharmCompanyManufacturerContactFax;
	}
	public void setSupplierPharmCompanyManufacturerContactFax(
			SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactFax) {
		this.supplierPharmCompanyManufacturerContactFax = supplierPharmCompanyManufacturerContactFax;
	}
	public SampleTestSchedule getSampleTestScheduleFax() {
		return sampleTestScheduleFax;
	}
	public void setSampleTestScheduleFax(SampleTestSchedule sampleTestScheduleFax) {
		this.sampleTestScheduleFax = sampleTestScheduleFax;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getFaxMemoParagraph1() {
		return faxMemoParagraph1;
	}
	public void setFaxMemoParagraph1(String faxMemoParagraph1) {
		this.faxMemoParagraph1 = faxMemoParagraph1;
	}
	public String getFaxMemoParagraph2() {
		return faxMemoParagraph2;
	}
	public void setFaxMemoParagraph2(String faxMemoParagraph2) {
		this.faxMemoParagraph2 = faxMemoParagraph2;
	}
	public String getFaxMemoParagraph3() {
		return faxMemoParagraph3;
	}
	public void setFaxMemoParagraph3(String faxMemoParagraph3) {
		this.faxMemoParagraph3 = faxMemoParagraph3;
	}
	public String getFaxMemoParagraph4() {
		return faxMemoParagraph4;
	}
	public void setFaxMemoParagraph4(String faxMemoParagraph4) {
		this.faxMemoParagraph4 = faxMemoParagraph4;
	}
	public String getFaxMemoParagraphOther() {
		return faxMemoParagraphOther;
	}
	public void setFaxMemoParagraphOther(String faxMemoParagraphOther) {
		this.faxMemoParagraphOther = faxMemoParagraphOther;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Date getSendDate() {
		return sendDate;
	}		
}
