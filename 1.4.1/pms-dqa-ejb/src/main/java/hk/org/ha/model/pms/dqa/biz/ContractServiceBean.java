package hk.org.ha.model.pms.dqa.biz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("contractService")
@RemoteDestination
@MeasureCalls
public class ContractServiceBean implements ContractServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private Contract contract;
	
	private static final String UNCHECKED ="unchecked";
	private static final String O_SUPPLIER = "o.supplier";
	private static final String CONTRACT_NUM = "contractNum";
	private static final String ITEM_CODE = "itemCode";
	private static final String MODULE_TYPE = "moduleType";
	
	private boolean success;
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveContractForCoaLastestLiveContract(String contractNum, String itemCode){
		logger.debug( "retrieveContractForCoaLastestLiveContract #0 #1", contractNum, itemCode );
		
		List<Contract> contractList = em.createNamedQuery("Contract.findForCoaLastestLiveContract")
		.setHint(QueryHints.FETCH, O_SUPPLIER)
		.setParameter(CONTRACT_NUM, contractNum.trim())
		.setParameter(ITEM_CODE, itemCode.trim())
		.setParameter("moduleList", new ArrayList<ModuleType>(Arrays.asList(ModuleType.All, ModuleType.COA)))
		.getResultList();
		
		if (contractList.size()>0){
			contract = contractList.get(0);
			contract.getSupplier();
		}else {
			contract = null;
		}
	}
		
	public void retrieveContractForSampleTest(String contractNum, String contractSuffix, OrderType orderType, String itemCode){
		logger.debug("retrieveContractForSampleTest");
		contract = retrieveContractForSampleTestValidation(contractNum, contractSuffix, orderType, itemCode, ModuleType.All );
	}
	
	@SuppressWarnings(UNCHECKED)
	public Contract retrieveContractForSampleTestValidation(String contractNum, String contractSuffix, OrderType orderType, String itemCode, ModuleType moduleType){
		logger.debug("retrieveContractForSampleTestValidation #0 #1", contractNum, contractSuffix);
		Contract ct = null;
		
		List<Contract> contractList;
		if(contractSuffix==null || ("").equals(contractSuffix.trim()))
		{
			contractList = em.createNamedQuery("Contract.findForSampleTestWithNoSuffix")
			.setParameter(CONTRACT_NUM, (contractNum==null?null:contractNum.trim()))
			.setParameter("contractType", orderType.getDataValue() )
			.setParameter(ITEM_CODE, itemCode.trim())
			.setParameter(MODULE_TYPE, moduleType)
			.setParameter("currentDate", new Date(), TemporalType.DATE)
			.setHint(QueryHints.FETCH, O_SUPPLIER)
			.setHint(QueryHints.FETCH, "o.manufacturer")
			.getResultList();
		}
		else
		{
			contractList = em.createNamedQuery("Contract.findForSampleTestWithSuffix")
			.setParameter(CONTRACT_NUM, (contractNum==null?null:contractNum.trim()))
			.setParameter("contractSuffix", contractSuffix.trim())
			.setParameter("contractType", orderType.getDataValue() )
			.setParameter(ITEM_CODE, itemCode.trim())
			.setParameter(MODULE_TYPE, moduleType)
			.setParameter("currentDate", new Date(), TemporalType.DATE)
			.setHint(QueryHints.FETCH, O_SUPPLIER)
			.setHint(QueryHints.FETCH, "o.manufacturer")
			.getResultList();
		}
		
		
		if (contractList.size()>0){
			ct = contractList.get(0);
		}
		return ct;
	}
	
	@SuppressWarnings(UNCHECKED)
	public Contract findContractByItemContractNumContractSuffixContractTypeModuleType(
			String itemCode,
			String contractNum,
			String contractSuffix,
			String contractType, 
			ModuleType moduleType){
		logger.debug("findContractByItemContractNumContractSuffixContractTypeModuleType #0 #1 #2", itemCode, contractNum, contractSuffix);
		
		List<Contract> contracts = em.createNamedQuery("Contract.findByItemContractNumContractSuffixContractTypeModuleType")
									.setParameter("itemCode", itemCode)
									.setParameter("contractNum", contractNum)
									.setParameter("contractSuffix", contractSuffix)
									.setParameter("contractSuffixStr", contractSuffix)
									.setParameter("contractType", contractType)
									.setParameter("moduleType", moduleType)
									.getResultList();
		
		if(contracts!=null && contracts.size()>0)
		{
			return contracts.get(0);
		}
		else{
			return null;
		}
	}
	
	public Contract copyContract(Contract contractIn, ModuleType moduleTypeIn){
		logger.debug("createNextSchedule");
		success = false;
		
		
		Contract contractCopy = new Contract();
		
		contractCopy.setContractNum(contractIn.getContractNum());
		contractCopy.setContractStatus(contractIn.getContractStatus());
		contractCopy.setContractSuffix(contractIn.getContractSuffix());
		contractCopy.setContractType(contractIn.getContractType());
		contractCopy.setCurrencyCode(contractIn.getCurrencyCode());
		contractCopy.setPhsContractNum(contractIn.getPhsContractNum());
		contractCopy.setErpContractLineId(contractIn.getErpContractLineId());
		contractCopy.setGetFreeQty(contractIn.getGetFreeQty());
		contractCopy.setItemCode(contractIn.getItemCode());
		contractCopy.setStartDate(contractIn.getStartDate());
		contractCopy.setEndDate(contractIn.getEndDate());
		contractCopy.setSupplier(contractIn.getSupplier());
		contractCopy.setManufacturer(contractIn.getManufacturer());
		contractCopy.setPharmCompany(contractIn.getPharmCompany());
		contractCopy.setPackPrice(contractIn.getPackPrice());
		contractCopy.setPackSize(contractIn.getPackSize());
		contractCopy.setPaymentTerm(contractIn.getPaymentTerm());
		contractCopy.setUomCode(contractIn.getUomCode());
		contractCopy.setUploadDate(contractIn.getUploadDate());
		contractCopy.setModuleType(moduleTypeIn);
		contractCopy.setSuspendFlag(SuspendFlag.Unsuspended);
		contractCopy.setContractId(null);
		
		return contractCopy;
	}
	
	public Contract getLazyContract(Contract contractLazy){
		contractLazy.getSupplier();
		contractLazy.getManufacturer();
		contractLazy.getPharmCompany();
		
		if(contractLazy.getSupplier()!=null)
		{
			contractLazy.getSupplier().getContact();
		}
		
		if(contractLazy.getManufacturer()!=null)
		{
			contractLazy.getManufacturer().getContact();						
		}
		
		if(contractLazy.getPharmCompany()!=null)
		{
			contractLazy.getPharmCompany().getContact();						
		}
		
		return contractLazy;
	}
	
	public boolean isSuccess() {
		return success;
	}

	@Remove
	public void destroy(){
		if (contract != null){
			contract = null;
		}
	}

}
