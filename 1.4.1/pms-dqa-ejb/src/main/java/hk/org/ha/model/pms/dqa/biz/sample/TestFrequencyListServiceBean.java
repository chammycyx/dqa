package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("testFrequencyListService")
@RemoteDestination
@MeasureCalls
public class TestFrequencyListServiceBean implements TestFrequencyListServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<TestFrequency> testFrequencyList;
	
	private static final String RECORD_STATUS = "recordStatus";
	private static final String TEST_CODE = "testCode";
	private static final String ORDER_TYPE = "orderType";
	
	
	@SuppressWarnings("unchecked")
	public List<TestFrequency> retrieveTestFrequencyListByRecordStatusRiskLevelCode(RecordStatus recordStatus, String riskLevelCode){
		logger.debug("getTestFrequencyByRecordStatusRiskLevelCode");
		List<TestFrequency> resultList = em.createNamedQuery("TestFrequency.findByRecordStatusRiskLevelCode")
			.setParameter(RECORD_STATUS, recordStatus)
			.setParameter("riskLevelCode", riskLevelCode)
			.setHint(QueryHints.FETCH, "o.riskLevel")
			.getResultList();
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveTestFrequencyListByRecordStatus(RecordStatus recordStatus){
		logger.debug("retrieveTestFrequencyListByRecordStatus");
	
		testFrequencyList = em.createNamedQuery("TestFrequency.findByRecordStatus")
			.setParameter(RECORD_STATUS, recordStatus)
			.setHint(QueryHints.BATCH, "o.riskLevel")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TestFrequency> retrieveTestFrequencyListByTestCodeOrderTypeFullPercentage(String testCodeIn, OrderType orderTypeIn, Boolean fullPercentage){
		logger.debug("retrieveTestFrequencyListByTestCodeOrderTypeFullPercentage");
		List<TestFrequency> testFrequencyListOut = null;
		if(fullPercentage)
		{
			testFrequencyListOut = em.createNamedQuery("TestFrequency.findByTestCodeOrderTypeFullPercentage")
				.setParameter(TEST_CODE, testCodeIn)
				.setParameter(ORDER_TYPE, orderTypeIn.getDataValue())
				.setParameter("percentage", 100)
				.setParameter("proceedSampleTestFlag", YesNoFlag.Yes)
				.setParameter(RECORD_STATUS, RecordStatus.Active)
				.getResultList();
		}
		else
		{
			testFrequencyListOut = em.createNamedQuery("TestFrequency.findByTestCodeOrderTypeNotFullPercentage")
				.setParameter(TEST_CODE, testCodeIn)
				.setParameter(ORDER_TYPE, orderTypeIn.getDataValue())
				.setParameter("percentage", 100)
				.setParameter("proceedSampleTestFlag", YesNoFlag.Yes)
				.setParameter(RECORD_STATUS, RecordStatus.Active)
				.getResultList();
		}
		
		return testFrequencyListOut;
	}
	
	public List<TestFrequency> checkTestFrequencyValid(String testCode, String riskLevelCode, String orderType){
		List<TestFrequency> testFrequencyListFind = null;
		testFrequencyListFind = em.createNamedQuery("TestFrequency.findByRecordStatusOrderTypeRiskLevelCodeTestCode")
				.setParameter(TEST_CODE, testCode)
				.setParameter("riskLevelCode", riskLevelCode)
				.setParameter(ORDER_TYPE, orderType)
				.setParameter(RECORD_STATUS, RecordStatus.Active)
				.getResultList();
		
		
		if(testFrequencyListFind==null || testFrequencyListFind.size()==0)
		{
		
			testFrequencyListFind = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeRiskLevelCodeNullOrderType")
					.setParameter(TEST_CODE, testCode)
					.setParameter("riskLevelCode", riskLevelCode)
					.setParameter(RECORD_STATUS, RecordStatus.Active)
					.getResultList();
			if(testFrequencyListFind==null || testFrequencyListFind.size()==0)
			{
		
				testFrequencyListFind = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeOrderTypeNullRiskLevel")
							.setParameter(TEST_CODE, testCode)
							.setParameter(ORDER_TYPE, orderType)
							.setParameter(RECORD_STATUS, RecordStatus.Active)
							.getResultList();
				
				if(testFrequencyListFind==null || testFrequencyListFind.size()==0)
				{
					//System.out.println("Error!!! Test Frequency record not exist.");
					return null;
				}
				else{
					return testFrequencyListFind;
				}
			}
			else{
				return testFrequencyListFind;
			}
		}
		else{
			return testFrequencyListFind;
		}
	}

    
    @Remove
    public void destroy(){
    	if (testFrequencyList!=null){
    		testFrequencyList = null;
    	}
    }
}
