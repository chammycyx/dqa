package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface ErpContractImportServiceLocal {
	
	Boolean importErpContractTest( String fileName, String jobId );
	
	Boolean importErpContract( String jobId );
	
}
