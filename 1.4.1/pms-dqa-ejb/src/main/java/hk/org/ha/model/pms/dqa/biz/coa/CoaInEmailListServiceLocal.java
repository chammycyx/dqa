package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import java.util.Date;

import javax.ejb.Local;

@Local
public interface CoaInEmailListServiceLocal {

	void retrieveCoaInEmailListForInEmailEnq(Date fromReceiveDate, Date toReceiveDate, ValidateResultFlag resultFlag);
	
	Integer getMaxReceiveDateMonthRangeForInEmailEnq();
	
	Integer getDefaultReceiveDateMonthRangeForInEmailEnq();
	
	void testRetrieveCoaInEmailListForInEmailEnq();
}
