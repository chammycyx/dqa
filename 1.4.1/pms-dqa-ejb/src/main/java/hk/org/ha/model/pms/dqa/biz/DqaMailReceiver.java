package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;
import hk.org.ha.model.pms.dqa.exception.DqaException;

import java.io.StringReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Name("dqaMailReceiver")
public class DqaMailReceiver implements MessageListener {

	@Logger
	private Log logger;
	
	@In(create = true)
	private ReplyEmailServiceLocal replyEmailService;
	
	@In(create = true)
	private DqaMailSenderServiceLocal dqaMailSenderService;
	
	@In
    private MessageProducerInf dqaMailServiceMessageProducer;
	
	private Unmarshaller unmarshaller = null;
	
	private String backupInEmail;
	
	public void setBackupInEmail(String backupInEmail) {
		this.backupInEmail = backupInEmail;
	}

	@PostConstruct
	public void init() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(EaiMessage.class);	
			unmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			logger.error("Unable to do marshall",e);
		}
	}	

	@SuppressWarnings("unchecked")
	public void onMessage(Message message) {
		logger.debug("onMessage");		
		final HashMap attHashMap = new HashMap();
		try {
			if (message instanceof MapMessage) {				
				
				final MapMessage in = (MapMessage) message;				
				String xmlEaiMessage = in.getString("email");				
				
				Enumeration e = in.getMapNames();
				while (e.hasMoreElements()) {
					String attachmentName = (String) e.nextElement();
					if ("email".equals(attachmentName)) { continue; }
					attHashMap.put(attachmentName, in.getBytes(attachmentName));
				}
				// parse the incoming EaiMessage
				final EaiMessage eaiMessage = (EaiMessage) unmarshaller.unmarshal(
						new StringReader(xmlEaiMessage));
				
				List<MessageCreator> msgCreatorList = replyEmailService.replyMessage( eaiMessage, attHashMap );

				if (msgCreatorList!=null){
					for (MessageCreator msgCreator : msgCreatorList){
						dqaMailServiceMessageProducer.send( msgCreator );
					}
				}
			}
		} catch (JAXBException e1) {
			logger.error("JAXB error", e1);
			throw new DqaException(e1);
		} catch (JMSException e2) {			
			logger.error("JMS error",e2);
			throw new DqaException(e2);
		} catch (Exception e3) {
			logger.error("Error ",e3);
			throw new DqaException(e3);
		} finally {
			if (!backupInEmail.isEmpty()) {
				final MapMessage in = (MapMessage) message;				
				String xmlEaiMessage;
				try {
					xmlEaiMessage = in.getString("email");
					
					Enumeration e = in.getMapNames();
					while (e.hasMoreElements()) {
						String attachmentName = (String) e.nextElement();
						if ("email".equals(attachmentName)) { continue; }
						attHashMap.put(attachmentName, in.getBytes(attachmentName));
					}
					final HashMap attBackUpHashMap = (HashMap) attHashMap.clone();
					
					EaiMessage backUpEaiMessage = (EaiMessage) unmarshaller.unmarshal(
							new StringReader(xmlEaiMessage));
					dqaMailSenderService.sendBackupInEmail(backUpEaiMessage, attBackUpHashMap, backupInEmail);
				} catch (Exception e1) {
					logger.error("Cannot backup email", e1);
				}	
			}
		}
	}
}
