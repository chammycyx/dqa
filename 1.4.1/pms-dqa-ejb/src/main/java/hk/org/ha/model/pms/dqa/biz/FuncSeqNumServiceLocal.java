package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface FuncSeqNumServiceLocal {

	String getNextFuncSeqNumString(String funcCode, String paddingZero);
	
	Integer retrieveNextFuncSeqNum(String funcCode);
	
	Integer getNextFuncSeqNum(String funcCode);
	
	String retrieveNextFRNSeqNum();

}
