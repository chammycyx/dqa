package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Calendar;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.vo.coa.CoaFileFolderCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaFileFolderInventoryListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaFileFolderInventoryListServiceBean implements CoaFileFolderInventoryListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<CoaFileFolder> coaFileFolderInventoryList;


	public void retrieveCoaFileFolderListByCriteria(CoaFileFolderCriteria coaFileFolderCriteria)
	{   
		logger.debug("retrieveCoaFileFolderListByCriteria");

		
		Calendar cal=Calendar.getInstance();
		cal.setTime(coaFileFolderCriteria.getEndDate());
		cal.add(Calendar.DATE, 1);

		coaFileFolderInventoryList =  em.createNamedQuery("CoaFileFolder.findCoaFileFolderListByCriteria")
			.setParameter("itemCode", ("".equals(coaFileFolderCriteria.getItemCode())?null:(coaFileFolderCriteria.getItemCode())) )
			.setParameter("supplierCode",  coaFileFolderCriteria.getSupplierCode().trim())			  
			.setParameter("category", coaFileFolderCriteria.getFileCat() )
			.setParameter("categoryStr", coaFileFolderCriteria.getFileCat().getDataValue().toString()  ) 
			.setParameter("orderType", coaFileFolderCriteria.getOrderType() )  
			.setParameter("contractNum", ("".equals(coaFileFolderCriteria.getContractNum())?null:("%"+coaFileFolderCriteria.getContractNum().trim()+"%")) )
			.setParameter("batchNum", ("".equals(coaFileFolderCriteria.getBatchNum() )?null:("%"+coaFileFolderCriteria.getBatchNum().trim() +"%")) )
			.setParameter("startDate", coaFileFolderCriteria.getStartDate() ,TemporalType.DATE  )
			.setParameter("endDate", cal.getTime()  ,TemporalType.DATE)
			.setHint(QueryHints.FETCH, "o.fileItem")
			.setHint(QueryHints.FETCH, "o.coaBatch")
			.setHint(QueryHints.FETCH, "o.coaBatch.coaItem.contract")
			.getResultList();

		if (coaFileFolderInventoryList.size() > 0) {
			for (  CoaFileFolder coaFileFolder : coaFileFolderInventoryList){
				coaFileFolder.getCoaBatch();
				if (coaFileFolder.getCoaBatch()!=null){
					coaFileFolder.getCoaBatch().getCoaBatchVer();
				}
				coaFileFolder.getFileItem();
			}
		}else{
			coaFileFolderInventoryList = null;
		} 
	}

	@Remove
	public void destroy(){  
		if(coaFileFolderInventoryList!=null) {
			coaFileFolderInventoryList = null;
		}
	}
}
