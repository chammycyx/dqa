package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_INIT_SUPP")
/*@NamedQueries({
	@NamedQuery(name = "FaxInitSupp.findByUserCode", query = "select o from FaxInitSupp o where o.initSContactPerson.userCode = :userCode ")
})*/


@Customizer(AuditCustomizer.class)
public class FaxInitSupp extends VersionEntity {
	
	private static final long serialVersionUID = 7747037175936151143L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxInitSuppSeq")
	@SequenceGenerator(name = "faxInitSuppSeq", sequenceName = "SEQ_FAX_INIT_SUPP", initialValue=10000)
	@Id
	@Column(name="FAX_INIT_SUPP_ID", nullable=false)
	private Long faxInitSuppId;
	
	@Column(name="DISTRIBUTE_LIST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date distributeListDate;
	
	@Column(name="CONFIRM_AVAIL_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date confirmAvailDate;
	
	@Column(name="COLLECT_SAMPLE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date collectSampleDate;
	
	@Column(name="PROVIDE_EXPLAIN_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date provideExplainDate;
	
	@Column(name="INST_CONTACT_USER", length=100)
	private String instContactUser;
	
	@Column(name="INST_CONTACT_RANK", length=100)
	private String instContactRank;
	
	@Column(name="INST_CONTACT_PHONE", length=100)
	private String instContactPhone;
	
	@Column(name="INST_CONTACT_EMAIL", length=100)
	private String instContactEmail;
	
	@Column(name="INST_NAME", length=100)
	private String instName;

	@Column(name="INST_RPT_NAME", length=100)
	private String instRptName;
	
	@Column(name="INST_RPT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date instRptDate;

	@Converter(name = "FaxInitSupp.referToEmailFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitSupp.referToEmailFlag")
	@Column(name="REFER_TO_EMAIL_FLAG", length=1)
	private YesNoFlag referToEmailFlag;
	
	public Long getFaxInitSuppId() {
		return faxInitSuppId;
	}

	public void setFaxInitSuppId(Long faxInitSuppId) {
		this.faxInitSuppId = faxInitSuppId;
	}

	public Date getDistributeListDate() {
		return (distributeListDate!=null)?new Date(distributeListDate.getTime()):null;
	}

	public void setDistributeListDate(Date distributeListDate) {
		if(distributeListDate!=null) {
			this.distributeListDate = new Date(distributeListDate.getTime());
		} else {
			this.distributeListDate = null;
		}
	}

	public Date getConfirmAvailDate() {
		return (confirmAvailDate!=null)?new Date(confirmAvailDate.getTime()):null;
	}

	public void setConfirmAvailDate(Date confirmAvailDate) {
		if(confirmAvailDate!=null) {
			this.confirmAvailDate = new Date(confirmAvailDate.getTime());
		} else {
			this.confirmAvailDate = null;
		}
	}

	public Date getCollectSampleDate() {
		return (collectSampleDate!=null)?new Date(collectSampleDate.getTime()):null;
	}

	public void setCollectSampleDate(Date collectSampleDate) {
		if (collectSampleDate!=null) {
			this.collectSampleDate = new Date(collectSampleDate.getTime());
		} else {
			this.collectSampleDate = null;
		}
	}

	public Date getProvideExplainDate() {
		return (provideExplainDate!=null)?new Date(provideExplainDate.getTime()):null;
	}

	public void setProvideExplainDate(Date provideExplainDate) {
		if (provideExplainDate!=null) {
			this.provideExplainDate = new Date(provideExplainDate.getTime());
		} else {
			this.provideExplainDate = null;
		}
	}
	
	public String getInstContactUser() {
		return instContactUser;
	}

	public void setInstContactUser(String instContactUser) {
		this.instContactUser = instContactUser;
	}

	public String getInstContactRank() {
		return instContactRank;
	}

	public void setInstContactRank(String instContactRank) {
		this.instContactRank = instContactRank;
	}

	public String getInstContactPhone() {
		return instContactPhone;
	}

	public void setInstContactPhone(String instContactPhone) {
		this.instContactPhone = instContactPhone;
	}

	public String getInstContactEmail() {
		return instContactEmail;
	}

	public void setInstContactEmail(String instContactEmail) {
		this.instContactEmail = instContactEmail;
	}
	
	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}
	
	public String getInstRptName() {
		return instRptName;
	}

	public void setInstRptName(String instRptName) {
		this.instRptName = instRptName;
	}

	public Date getInstRptDate() {
		return instRptDate;
	}

	public void setInstRptDate(Date instRptDate) {
		this.instRptDate = instRptDate;
	}

	public YesNoFlag getReferToEmailFlag() {
		return referToEmailFlag;
	}

	public void setReferToEmailFlag(YesNoFlag referToEmailFlag) {
		this.referToEmailFlag = referToEmailFlag;
	}
	
}
