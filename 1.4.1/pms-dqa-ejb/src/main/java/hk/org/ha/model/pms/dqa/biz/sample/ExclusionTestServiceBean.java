package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Collection;


import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("exclusionTestService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ExclusionTestServiceBean implements ExclusionTestServiceLocal{
	@Logger
	private Log logger;

	@In(required = false)
	@Out(required = false)
	private ExclusionTest exclusionTest;

	private boolean success;
	
	private String errorCode;
	
	public void checkDuplicatedExclusionTest(ExclusionTest exclusionTestIn, Collection<ExclusionTest> exclusionTestListIn )
	{
		logger.debug("checkDuplicatedExclusionTest");
		success = false;
		errorCode = null;
		boolean duplicated = false;
	
		if (exclusionTestListIn!=null){
			for (ExclusionTest tmpExclusion: exclusionTestListIn ){
				if (tmpExclusion.getSampleTest().getTestCode().equals(exclusionTestIn.getSampleTest().getTestCode())){
					duplicated = true;
				}
			}
			
			if (!duplicated){
				success = true;
			}else {
				errorCode = "0007";
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
	@Remove
	public void destroy(){
		if (exclusionTest!=null){
			exclusionTest = null;
		}
	}

}
