package hk.org.ha.fmk.pms.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

public final class QueryUtils {

	private static final int IN_LIMIT = 900;
	
	private QueryUtils() {}
	
	public static <E> List<List<E>> split(Collection<E> collection, int limit) {

		if (collection == null) {
			throw new IllegalArgumentException("Collection cannot be null");
		}

		if (limit <= 0) {
			throw new IllegalArgumentException("Limit must be greater than 0");
		}

		List<List<E>> result = new ArrayList<List<E>>();
		List<E> part = new ArrayList<E>();

		for (E obj : collection) {
			if (part.size() >= limit) {
				result.add(part);
				part = new ArrayList<E>();
			}
			part.add(obj);
		}
		if (part.size() > 0) {
			result.add(part);
		}
		return result;
	}

	public static <E> Set<E> toSet(Collection<E> collection) {

		Set<E> result = new LinkedHashSet<E>();
		result.addAll(collection);
		return result;
	}

	@SuppressWarnings("unchecked")
	public static <E> List splitExecute(Query query, String name, Collection<E> valueList) {
		
		return splitExecute(query, name, valueList, IN_LIMIT);
	}
	
	@SuppressWarnings("unchecked")
	public static <E> List splitExecute(Query query, String name, Collection<E> valueList, int limit) {

		List resultList = new ArrayList();
		List<List<E>> splitList = split(toSet(valueList), limit);
		for (List<E> partialList : splitList) {
			resultList.addAll(query.setParameter(name, partialList).getResultList());
		}
		return resultList;
	}
	
	public static <E> int splitUpdate(Query query, String name, Collection<E> valueList) {
		return splitUpdate(query, name, valueList, IN_LIMIT);
	}
	
	public static <E> int splitUpdate(Query query, String name, Collection<E> valueList, int limit) {
		int updateCount = 0;
		List<List<E>> splitList = split(toSet(valueList), limit);
		for (List<E> partialList : splitList) {
			updateCount += query.setParameter(name, partialList).executeUpdate();
		}
		return updateCount;
	}
}
