package hk.org.ha.model.pms.dqa.persistence.coa;

import java.util.List;

import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "COA_DISCREPANCY")
@NamedQueries({
	@NamedQuery(name = "CoaDiscrepancy.findForReportDiscrepancy", query = "select o from CoaDiscrepancy o where o.parentCoaDiscrepancy is null and o.coaBatch.coaItem.orderType = :orderType and o.passFlag <> :passFlag and o.coaBatch.coaBatchVer.coaStatus = :coaStatus and o.coaBatch.coaItem.contract.itemCode = :itemCode and ( ( o.coaBatch.coaItem.orderType <> :orderTypeD and o.coaBatch.coaItem.contract.contractNum = :contractNum ) or ( o.coaBatch.coaItem.orderType = :orderTypeD and  o.coaBatch.coaItem.contract.supplier.supplierCode = :supplierCode) ) order by o.coaBatch.batchNum"),
	@NamedQuery(name = "CoaDiscrepancy.findByCoaBatchPassFlag", query = "select o from CoaDiscrepancy o where o.coaBatch = :coaBatch and o.passFlag <> :passFlag order by o.coaDiscrepancyId"),
	@NamedQuery(name = "CoaDiscrepancy.findByCoaBatchId", query = "select o from CoaDiscrepancy o where o.coaBatch.coaBatchId = :coaBatchId order by o.coaDiscrepancyId")
})
public class CoaDiscrepancy extends VersionEntity {

	private static final long serialVersionUID = -2068397770874564073L;

	@Id
	@Column(name="COA_DISCREPANCY_ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaDiscrepancySeq")
	@SequenceGenerator(name="coaDiscrepancySeq", sequenceName="SEQ_COA_DISCREPANCY", initialValue=10000)
	private Long coaDiscrepancyId;

	@Column(name="OVERWRITE_REMARK", length=300, nullable=true)
	private String overwriteRemark;
	
	@Converter(name="CoaDiscrepancy.passFlag", converterClass=PassFlag.Converter.class)
	@Convert("CoaDiscrepancy.passFlag")
	@Column(name="PASS_FLAG", length=1, nullable=false)
	private PassFlag passFlag;

	@ManyToOne
	@JoinColumn(name="COA_BATCH_ID" , nullable=true)
	private CoaBatch coaBatch;

	@ManyToOne	
	@JoinColumn(name="DISCREPANCY_ID")
	private CoaDiscrepancyKey coaDiscrepancyKey;

	@OneToMany(mappedBy="parentCoaDiscrepancy")
	private List<CoaDiscrepancy> childCoaDiscrepancyList;
	
	@ManyToOne
	@JoinColumn(name="PARENT_COA_DISCREPANCY", nullable=true)
	private CoaDiscrepancy parentCoaDiscrepancy;
	
	@Transient
	private boolean enable;
	
	public void setCoaBatch(CoaBatch coaBatch) {
		this.coaBatch = coaBatch;
	}

	public CoaBatch getCoaBatch() {
		return coaBatch;
	}

	public void setCoaDiscrepancyKey(CoaDiscrepancyKey coaDiscrepancyKey) {
		this.coaDiscrepancyKey = coaDiscrepancyKey;
	}

	public CoaDiscrepancyKey getCoaDiscrepancyKey() {
		return coaDiscrepancyKey;
	}

	public void setParentCoaDiscrepancy(CoaDiscrepancy parentCoaDiscrepancy) {
		this.parentCoaDiscrepancy = parentCoaDiscrepancy;
	}

	public CoaDiscrepancy getParentCoaDiscrepancy() {
		return parentCoaDiscrepancy;
	}

	public void setChildCoaDiscrepancyList(List<CoaDiscrepancy> childCoaDiscrepancyList) {
		this.childCoaDiscrepancyList = childCoaDiscrepancyList;
	}

	public List<CoaDiscrepancy> getChildCoaDiscrepancyList() {
		return childCoaDiscrepancyList;
	}
	
	public void addChildCoaDiscrepancy(CoaDiscrepancy coaDiscrepancy) {
		this.childCoaDiscrepancyList.add(coaDiscrepancy);
		coaDiscrepancy.setParentCoaDiscrepancy(this);
	}

	public void setPassFlag(PassFlag passFlag) {
		this.passFlag = passFlag;
	}

	public PassFlag getPassFlag() {
		return passFlag;
	}

	public void setCoaDiscrepancyId(Long coaDiscrepancyId) {
		this.coaDiscrepancyId = coaDiscrepancyId;
	}

	public Long getCoaDiscrepancyId() {
		return coaDiscrepancyId;
	}

	public void setOverwriteRemark(String overwriteRemark) {
		this.overwriteRemark = overwriteRemark;
	}

	public String getOverwriteRemark() {
		return overwriteRemark;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
}
