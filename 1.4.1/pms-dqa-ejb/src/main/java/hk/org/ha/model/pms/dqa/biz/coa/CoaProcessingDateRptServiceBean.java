package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.ConfirmCheckListFlag;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptData;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaProcessingDateRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaProcessingDateRptServiceBean implements CoaProcessingDateRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	private static final String DATE_FORMAT = "dd-MMM-yyyy";
	private static final String PASS_CHECKED = "Pass - Checked";
	private static final String PASS_WITHOUT_CHECKING = "Pass - Without Checking";

	private String reportName = "COA Processing Date Report";

	private String reportParameters;

	private SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
	
	private List<CoaProcessingDateRptData> coaProcessingDateRptList; 

	private Map<Long, List<CoaDiscrepancy>> batchIdToCoaDiscrepancyMap;

	private CoaDiscrepancyComparator discrepancyComparator = new CoaDiscrepancyComparator();
	
	@SuppressWarnings("unchecked")
	private void buildFollowUpResultMap(List<Long> coaBatchIdList) {
		List<CoaDiscrepancy> coaDiscrepancyList = QueryUtils.splitExecute(em.createQuery("select o from CoaDiscrepancy o" +
														" where o.coaBatch.coaBatchId in :coaBatchIdList" +
														" and o.passFlag = :passFlag")																										 	
												 	.setParameter("passFlag", PassFlag.Passed)
												 	.setHint(QueryHints.FETCH, "o.coaBatch")
												 	.setHint(QueryHints.FETCH, "o.coaDiscrepancyKey"), "coaBatchIdList", coaBatchIdList);
		batchIdToCoaDiscrepancyMap = new HashMap<Long, List<CoaDiscrepancy>>();
		Long coaBatchId;
		List<CoaDiscrepancy> discrepancyList;
		
		for (CoaDiscrepancy coaDiscrepancy:coaDiscrepancyList) {
			coaBatchId = coaDiscrepancy.getCoaBatch().getCoaBatchId();
			if (batchIdToCoaDiscrepancyMap.get(coaBatchId) == null) {
				discrepancyList = new ArrayList<CoaDiscrepancy>();
				discrepancyList.add(coaDiscrepancy);
				batchIdToCoaDiscrepancyMap.put(coaBatchId,  discrepancyList);
			} else {
				discrepancyList = batchIdToCoaDiscrepancyMap.get(coaBatchId);				
				discrepancyList.add(coaDiscrepancy);
			}
		}
		
	}
	
	private String getFollowUpResult(List<CoaDiscrepancy> coaDiscrepancyList) {
		if (coaDiscrepancyList == null) {
			return "";
		} else {
			Collections.sort(coaDiscrepancyList,discrepancyComparator);
			StringBuffer resultBuffer = new StringBuffer();
			for (CoaDiscrepancy coaDiscrepancy:coaDiscrepancyList) {
				resultBuffer.append(coaDiscrepancy.getCoaDiscrepancyKey().getDiscrepancyDesc());
				resultBuffer.append(": ");
				resultBuffer.append(coaDiscrepancy.getOverwriteRemark());
				resultBuffer.append("\r\n");
			}
			return resultBuffer.toString();
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<CoaBatchVer> retrieveDataList(CoaProcessingDateRptCriteria coaProcessingDateRptCriteria){

		Calendar c = Calendar.getInstance(); 
		c.setTime(coaProcessingDateRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
		
		String sqlStr = "select o from CoaBatch o" +
		" where o.coaBatchVer.recordStatus = :recordStatus" +
		" and o.batchNum is not null" +
		" and (o.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' = :supplierCode)" +
		" and (o.coaItem.contract.itemCode = :itemCode or :itemCode is null)" +
		" and (o.coaItem.contract.contractNum = :contractNum or :contractNum is null)" +
		" and (o.coaItem.contract.moduleType = :moduleType)" +
		" and o.createDate >= :startDate and o.createDate < :endDate" +				
		" order by o.coaItem.contract.supplier.supplierCode, o.coaItem.contract.manufacturer.companyCode," +
		" o.coaItem.contract.itemCode, o.coaItem.orderType," +
		" o.coaItem.contract.contractNum";

		List<CoaBatch> coaBatchList = em.createQuery(sqlStr)
		.setParameter("recordStatus", RecordStatus.Active)
		.setParameter("moduleType", ModuleType.COA)
		.setParameter("supplierCode", coaProcessingDateRptCriteria.getSupplierCode())
		.setParameter("itemCode", coaProcessingDateRptCriteria.getItemCode())
		.setParameter("contractNum", coaProcessingDateRptCriteria.getContractNum())
		.setParameter("startDate", coaProcessingDateRptCriteria.getStartDate(), TemporalType.DATE)
		.setParameter("endDate", sqlEndDate, TemporalType.DATE)
		.setHint(QueryHints.FETCH, "o.coaBatchVer")
		.setHint(QueryHints.LEFT_FETCH, "o.coaBatchVer.emailLog")
		.setHint(QueryHints.BATCH, "o.coaBatchVerList")
		.setHint(QueryHints.BATCH, "o.coaItem.contract.manufacturer")
		.setHint(QueryHints.BATCH, "o.coaItem.contract.pharmCompany")
		.setHint(QueryHints.BATCH, "o.coaItem.contract.supplier")
		.getResultList();
		List<CoaBatchVer> coaBatchVerList = new ArrayList<CoaBatchVer>();
		List<Long> coaBatchIdList = new ArrayList<Long>();
		for(CoaBatch coaBatch:coaBatchList) {
			coaBatchVerList.add(coaBatch.getCoaBatchVer());
			coaBatchVerList.addAll(coaBatch.getCoaBatchVerList());
			coaBatchIdList.add(coaBatch.getCoaBatchId());
		}

		buildFollowUpResultMap(coaBatchIdList);
		
		return coaBatchVerList;
	}

	public void retrieveCoaProcessingDateRptList(CoaProcessingDateRptCriteria coaProcessingDateRptCriteria){
		logger.debug("retrieveCoaProcessingDateRptList");

		coaProcessingDateRptList = new ArrayList<CoaProcessingDateRptData>();	
		List<CoaBatchVer> dataList = retrieveDataList(coaProcessingDateRptCriteria);
		HashMap<String, CoaProcessingDateRptData> coaProcessingDateHM = new HashMap<String , CoaProcessingDateRptData>();

		setReportParameters(coaProcessingDateRptCriteria);		
		String hashKey;	
		CoaProcessingDateRptData cpData; 

		for (CoaBatchVer cbv :dataList ){			

			CoaBatch coaBatch = cbv.getCoaBatch();
			CoaItem coaItem = coaBatch.getCoaItem();
			Contract contract = coaItem.getContract();

			OrderType orderType = coaItem.getOrderType();
			String supplierCode = contract.getSupplier().getSupplierCode();
			String manuCode = contract.getManufacturer().getCompanyCode();
			String pharmCompanyCode = contract.getPharmCompany().getCompanyCode();
			String itemCode = contract.getItemCode();
			String contractNum = contract.getContractNum();
			String batchNum = coaBatch.getBatchNum();
			CoaStatus coaStatus = cbv.getCoaStatus();
			DiscrepancyStatus discStatus = cbv.getDiscrepancyStatus();
			Date recordDate = cbv.getCreateDate();
			

			StringBuffer sb = constructHashKey(supplierCode, manuCode, pharmCompanyCode, itemCode, orderType.getDataValue(), batchNum, contractNum );
			hashKey = sb.toString();

			if (coaProcessingDateHM.containsKey(hashKey)){
				cpData = (CoaProcessingDateRptData) coaProcessingDateHM.get(hashKey);	
			}else {
				cpData = constructRptData(supplierCode, manuCode, pharmCompanyCode, itemCode, orderType.getDataValue(), contractNum, batchNum);
				coaProcessingDateHM.put(hashKey, cpData);
				coaProcessingDateRptList.add(cpData);
			}		
			
			cpData.setItemDesc(contract.getDmDrug().getFullDrugDesc());
			
			if (contract.getStartDate() != null) {
				cpData.setContractStartDate(contract.getStartDate());
			} else {
				cpData.setContractStartDate(null);
			}
			if (contract.getEndDate() != null) {
				cpData.setContractEndDate(contract.getEndDate());	
			} else {
				cpData.setContractEndDate(null);
			}
			
			if (coaStatus == CoaStatus.VerificationInProgress){
				cpData.setVerDate(recordDate);
			}else if (coaStatus == CoaStatus.DiscrepancyClarificationInProgress){
				if (discStatus == DiscrepancyStatus.DiscrepancyUnderGrouping){
					cpData.setDiscGroupDate(recordDate); 
				}else if (discStatus == DiscrepancyStatus.PharmacistFollowUp){			
					cpData.setPharmDate(recordDate);
				}else if (discStatus == DiscrepancyStatus.EmailToSupplier){
					cpData.setEmailToSupplierDate(recordDate);
				}else if (discStatus == DiscrepancyStatus.ReminderToSupplier && cbv.getReminderNum() == 1){
					cpData.setFirstReminderDate(recordDate);
				}else if (discStatus == DiscrepancyStatus.ReminderToSupplier && cbv.getReminderNum() == 2){
					cpData.setSecondReminderDate(recordDate);
				}
				
				cpData.setDiscrepancyUser(cbv.getCreateUser());
				cpData.setDiscrepancyRemark(coaBatch.getDiscrepancyRemark());
			}else if (coaStatus == CoaStatus.Pass){
				cpData.setPassDate(recordDate);
				cpData.setPassUser(cbv.getCreateUser());
			}else if (coaStatus == CoaStatus.Fail){
				cpData.setFailDate(recordDate);
				cpData.setFailUser(cbv.getCreateUser());
				cpData.setFailRemark(coaBatch.getFailRemark());
			}

			cpData.setFollowUpResult(getFollowUpResult(batchIdToCoaDiscrepancyMap.get(coaBatch.getCoaBatchId())));
			
			constructCoaStatus(cpData, coaStatus, coaBatch.getCfmChecklistFlag());
		}
		logger.debug("coaProcessingDateRptList report list size : #0", coaProcessingDateRptList.size());
	}

	private void constructCoaStatus(CoaProcessingDateRptData cpData, CoaStatus coaStatus, ConfirmCheckListFlag ctmChecklistFlag)
	{
		if (coaStatus == CoaStatus.Pass)
		{
			if (ctmChecklistFlag == ConfirmCheckListFlag.YES)
			{
				cpData.setCoaStatus(PASS_CHECKED);
			}
			else
			{
				cpData.setCoaStatus(PASS_WITHOUT_CHECKING);
			}
		}
		else if (coaStatus == CoaStatus.Fail)
		{
			cpData.setCoaStatus(CoaStatus.Fail.getDisplayValue());
		}
		else if (coaStatus == CoaStatus.DiscrepancyClarificationInProgress && 
				(cpData.getCoaStatus() != CoaStatus.Fail.getDisplayValue() && 
						cpData.getCoaStatus() != PASS_CHECKED && cpData.getCoaStatus() != PASS_WITHOUT_CHECKING))
		{
			cpData.setCoaStatus(CoaStatus.DiscrepancyClarificationInProgress.getDisplayValue());
		}
		else if (coaStatus == CoaStatus.VerificationInProgress && StringUtils.isBlank(cpData.getCoaStatus()))
		{
			cpData.setCoaStatus(CoaStatus.VerificationInProgress.getDisplayValue());
		}
	}

	private CoaProcessingDateRptData constructRptData(String supplierCode, String manuCode, String pharmCompanyCode, String itemCode, String orderType, 
			String contractNum, String batchNum){
		CoaProcessingDateRptData cpData = new CoaProcessingDateRptData();
		cpData.setSupplierCode(supplierCode);
		cpData.setManuCode(manuCode);
		cpData.setPharmCompanyCode(pharmCompanyCode);
		cpData.setItemCode(itemCode);
		cpData.setOrderType(orderType);
		cpData.setContractNum(contractNum);
		cpData.setBatchNum(batchNum);
		return cpData;
	}

	private StringBuffer constructHashKey(String supplierCode, String manuCode, String pharmCompanyCode, String itemCode,
			String orderType, String batchNum, String contractNum){

		StringBuffer sb =  new StringBuffer();
		sb.append(supplierCode);
		sb.append(':');
		sb.append(manuCode);
		sb.append(':');
		sb.append(pharmCompanyCode);
		sb.append(':');
		sb.append(itemCode);
		sb.append(':');
		sb.append(orderType);
		sb.append(':');
		sb.append(batchNum);
		sb.append(':');
		sb.append(contractNum);

		return sb;
	}

	private void setReportParameters(CoaProcessingDateRptCriteria coaProcessingDateRptCriteria){
		String supplierCodeIn = coaProcessingDateRptCriteria.getSupplierCode();
		String itemCodeIn = coaProcessingDateRptCriteria.getItemCode();
		String contractNoIn = coaProcessingDateRptCriteria.getContractNum();
		Date startDate = coaProcessingDateRptCriteria.getStartDate();
		Date endDate = coaProcessingDateRptCriteria.getEndDate();

		itemCodeIn = (itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn);
		contractNoIn = (contractNoIn==null||contractNoIn.trim().equals("")?"All":contractNoIn);

		String startDateStr = (dateFormatter.format(startDate));
		String endDateStr = (dateFormatter.format(endDate));

		reportParameters = "Supplier Code="+ supplierCodeIn
		+", Item Code=" +  itemCodeIn
		+", Contract No.=" +  contractNoIn 
		+", Batch COA Create Date=" + startDateStr+ " to " +endDateStr;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CoaProcessingDateRptData> getCoaProcessingDateRptList() {
		return coaProcessingDateRptList;
	}

	public String getReportName() {
		return reportName;
	}

	public String getReportParameters() {
		return reportParameters;
	}

	public Date getRetrieveDate() {
		return new Date();
	}

	public static class CoaDiscrepancyComparator implements Comparator<CoaDiscrepancy>,Serializable
	{

		private static final long serialVersionUID = 8032402230292096246L;

		public int compare(CoaDiscrepancy c1, CoaDiscrepancy c2){
			return c1.getCoaDiscrepancyKey()
					 .getOrderSeq()
					 .compareTo( 
								c2.getCoaDiscrepancyKey()
								  .getOrderSeq() 
				      );
		}						
	}

	@Remove
	public void destroy(){

	}

}
