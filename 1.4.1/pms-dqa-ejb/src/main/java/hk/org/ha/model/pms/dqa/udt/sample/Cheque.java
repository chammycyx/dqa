package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Cheque implements StringValuedEnum {
	
	Required("REQUIRED", "Cheque Rqd"),
	Letter("LETTER", "Letter"),
	Received("RECEIVED", "Cheque Receive"),
	Fin("FIN", "Cheque to Finance"),
	NA("NA", "NA");
	
    private final String dataValue;
    private final String displayValue;
        
    Cheque(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Cheque> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<Cheque> getEnumClass() {
    		return Cheque.class;
    	}
    }
}
