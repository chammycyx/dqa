package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureSubCatService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class ProblemNatureSubCatServiceBean implements ProblemNatureSubCatServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private ProblemNatureSubCat problemNatureSubCat;
	
	private boolean success;
	
	private String errorCode;

		
	public void addProblemNatureSubCat(){
		logger.debug("addProblemNatureSubCat");
		
		problemNatureSubCat = new ProblemNatureSubCat();
	}
	
	public void createProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn){
		logger.debug("createProblemNatureSubCat");
		success = false;
		
		List<ProblemNatureSubCat> problemNatureSubCats = em.createNamedQuery("ProblemNatureSubCat.findBySubCatDescCatId")
												.setParameter("subCatDesc", problemNatureSubCatIn.getSubCatDesc())
												.setParameter("problemNatureCatId", problemNatureSubCatIn.getProblemNatureCat().getProblemNatureCatId())
												.setParameter("recordStatus", RecordStatus.Active)
												.setHint(QueryHints.FETCH, "o.problemNatureCat")
												.setHint(QueryHints.FETCH, "o.problemNatureCat.problemNatureParam")
												.getResultList();
		
		if(problemNatureSubCats==null || problemNatureSubCats.size()==0)
		{
			em.persist(problemNatureSubCatIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
		
	}
	
	public void deleteProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn){
		logger.debug("deleteProblemNatureSubCat");
		
		em.createNamedQuery("ProblemNatureSubCat.deleteByProblemNatureSubCatId")
										.setParameter("problemNatureSubCatId", problemNatureSubCatIn.getProblemNatureSubCatId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
	}
	
	public void updateProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn){
		logger.debug("updateProblemNatureSubCat");
		success = false;
		
		List<ProblemNatureSubCat> problemNatureSubCats = em.createNamedQuery("ProblemNatureSubCat.findBySubCatDescCatId")
												.setParameter("subCatDesc", problemNatureSubCatIn.getSubCatDesc())
												.setParameter("problemNatureCatId", problemNatureSubCatIn.getProblemNatureCat().getProblemNatureCatId())
												.setParameter("recordStatus", RecordStatus.Active)
												.setHint(QueryHints.FETCH, "o.problemNatureCat")
												.setHint(QueryHints.FETCH, "o.problemNatureCat.problemNatureParam")
												.getResultList();
		
		if(problemNatureSubCats==null || problemNatureSubCats.size()==0)
		{
			em.merge(problemNatureSubCatIn);
			em.flush();
			
			success = true;
		}
		else if(problemNatureSubCats.get(0).getProblemNatureSubCatId().equals(problemNatureSubCatIn.getProblemNatureSubCatId()))
		{
			em.merge(problemNatureSubCatIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
	}
	
	public void retrieveProblemNatureSubCatByProblemNatureSubCatId(ProblemNatureSubCat problemNatureSubCatIn){
		logger.debug("retrieveProblemNatureSubCatByProblemNatureSubCatId");
		
		List<ProblemNatureSubCat> problemNatureSubCats = em.createNamedQuery("ProblemNatureSubCat.findByProblemNatureSubCatId")
											.setParameter("problemNatureSubCatId", problemNatureSubCatIn.getProblemNatureSubCatId())
											.setParameter("recordStatus", RecordStatus.Active)
											.setHint(QueryHints.FETCH, "o.problemNatureCat")
											.setHint(QueryHints.FETCH, "o.problemNatureCat.problemNatureParam")
											.getResultList();

		if(problemNatureSubCats==null || problemNatureSubCats.size()==0)
		{
			problemNatureSubCat = null;
		}
		else
		{
			problemNatureSubCat = problemNatureSubCats.get(0);
		}
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
		if (problemNatureSubCat != null){
			problemNatureSubCat = null;
		}
	}
}
