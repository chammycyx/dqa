package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemCriteria;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemListServiceBean implements PharmProblemListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	@Out(required = false)
	private List<PharmProblem> pharmProblemList;
	
	@Out(required = false)
	private List<PharmProblem> draftPharmProblemList;
	
	
	@SuppressWarnings("unchecked")
	public void retrievePharmProblemListByPharmProblemCriteria(PharmProblemCriteria pharmProblemCriteriaIn){
		
		if(pharmProblemCriteriaIn.getProblemStatus() != null && 
				pharmProblemCriteriaIn.getProblemStatus().equals(ProblemStatus.Drafted)){
			draftPharmProblemList = retrieveDraftPharmProblemList( pharmProblemCriteriaIn );
		}else{
			pharmProblemList = retrievePharmProblemList( pharmProblemCriteriaIn );
			
		}
	}
	@SuppressWarnings("unchecked")
	public List<PharmProblem> retrieveDraftPharmProblemList(PharmProblemCriteria pharmProblemCriteriaIn ){
		logger.debug("retrieveDraftPharmProblemListByPharmProblemCriteria");
		
		StringBuffer pharmProblemListSql = new StringBuffer();
		pharmProblemListSql.append("select o from PharmProblem o "); 
		pharmProblemListSql.append("where o.recordStatus = :recordStatus ");
		pharmProblemListSql.append("and o.problemStatus = :problemStatus  ");
		
		if(pharmProblemCriteriaIn.getInstitution()!=null){
			
			if ("*".equals(pharmProblemCriteriaIn.getInstitution().getInstitutionCode())) {
				pharmProblemListSql.append("and (o.institution.institutionCode = :institutionCode or o.institution.pcuCode = :institutionCode) ");
			} else {
				pharmProblemListSql.append("and (o.institution.institutionCode = :institutionCode or :institutionCode is null) ");
			}
		}
		
		Query q = em.createQuery(pharmProblemListSql.toString())
		.setParameter("recordStatus", RecordStatus.Active)
		.setParameter("problemStatus", ProblemStatus.Drafted);
		
		if(pharmProblemCriteriaIn.getInstitution()!=null){
			if ("*".equals(pharmProblemCriteriaIn.getInstitution().getInstitutionCode())) {
				q=q.setParameter("institutionCode", pharmProblemCriteriaIn.getInstitution().getPcuCode());
			} else {
				q=q.setParameter("institutionCode", pharmProblemCriteriaIn.getInstitution().getInstitutionCode());
			}
		}
		
		
		List<PharmProblem> resultList = q.getResultList();
		
		if(resultList!=null && resultList.size()>0)
		{
			for(PharmProblem pc:resultList)
			{
				getLazyPharmProblem(pc);
			}
			Collections.sort(resultList, new ProblemNumComparator());
			
		}
		else
		{
			resultList=null;
		}
		
		return resultList;
	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PharmProblem> retrievePharmProblemList(PharmProblemCriteria pharmProblemCriteriaIn ){
		logger.debug("retrievePharmProblemListByPharmProblemCriteria");
		
		StringBuffer pharmProblemListSql = new StringBuffer();
		pharmProblemListSql.append("select o from PharmProblem o "); 
		pharmProblemListSql.append("where (o.problemDate >= :fromProblemDate or :fromProblemDate is null) ");
		pharmProblemListSql.append("and (o.problemDate < :toProblemDate or :toProblemDate is null) ");
		pharmProblemListSql.append("and o.recordStatus = :recordStatus ");
		
		if(pharmProblemCriteriaIn.getInstitution()!=null){
			
			if ("*".equals(pharmProblemCriteriaIn.getInstitution().getInstitutionCode())) {
				pharmProblemListSql.append("and (o.institution.institutionCode = :institutionCode or o.institution.pcuCode = :institutionCode) ");
			} else {
				pharmProblemListSql.append("and (o.institution.institutionCode = :institutionCode or :institutionCode is null) ");
			}
		}
		if(pharmProblemCriteriaIn.getItemCode()!=null){
			pharmProblemListSql.append("and (o.problemHeader.itemCode = :itemCode or :itemCode is null) ");
		}
		if(pharmProblemCriteriaIn.getOrderType()!=null){
			pharmProblemListSql.append("and (o.problemHeader.orderType = :orderType or :orderTypeDataValue is null) ");}
		if(pharmProblemCriteriaIn.getSupplierCode()!=null){
			pharmProblemListSql.append("and (o.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) ");}
		if(pharmProblemCriteriaIn.getManufCode()!=null){
			pharmProblemListSql.append("and (o.contract.manufacturer.companyCode = :manufCode or :manufCode is null) ");}
		if(pharmProblemCriteriaIn.getPharmCompanyCode()!=null){
			pharmProblemListSql.append("and (o.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");}
		if(pharmProblemCriteriaIn.getProblemNum()!=null){
			pharmProblemListSql.append("and (o.problemNum = :problemNum or :problemNum is null) ");}
		if(pharmProblemCriteriaIn.getProblemClassification()!=null){
			pharmProblemListSql.append("and (o.qaProblem.classification.classificationCode = :problemClassification or :problemClassificationDataValue is null) ");}
		if(pharmProblemCriteriaIn.getProblemStatus()!=null){
			pharmProblemListSql.append("and (o.problemStatus = :problemStatus or :problemStatusDataValue is null) ");}
		else{
			pharmProblemListSql.append("and (o.problemStatus != :draftedProblemStatus or o.problemStatus is null) ");
		}
		if(pharmProblemCriteriaIn.getCaseNum()!=null){
			pharmProblemListSql.append("and (o.qaProblem.caseNum = :caseNum or :caseNum is null) ");}
	
		pharmProblemListSql.append("order by o.createDate desc ");
		
		Query q = em.createQuery(pharmProblemListSql.toString())
		.setParameter("fromProblemDate", pharmProblemCriteriaIn.getFromProblemDate(), TemporalType.DATE)
		.setParameter("toProblemDate", pharmProblemCriteriaIn.getToProblemDate(), TemporalType.DATE)
		.setParameter("recordStatus", RecordStatus.Active);

		if(pharmProblemCriteriaIn.getInstitution()!=null){
			if ("*".equals(pharmProblemCriteriaIn.getInstitution().getInstitutionCode())) {
				q=q.setParameter("institutionCode", pharmProblemCriteriaIn.getInstitution().getPcuCode());
			} else {
				q=q.setParameter("institutionCode", pharmProblemCriteriaIn.getInstitution().getInstitutionCode());
			}
		}
		if(pharmProblemCriteriaIn.getItemCode()!=null){
			q=q.setParameter("itemCode", pharmProblemCriteriaIn.getItemCode());}			
		if(pharmProblemCriteriaIn.getOrderType()!=null){
			q=q.setParameter("orderType", pharmProblemCriteriaIn.getOrderType());
			q=q.setParameter("orderTypeDataValue", pharmProblemCriteriaIn.getOrderType().getDataValue());}
		if(pharmProblemCriteriaIn.getSupplierCode()!=null){
			q=q.setParameter("supplierCode", pharmProblemCriteriaIn.getSupplierCode());}
		if(pharmProblemCriteriaIn.getManufCode()!=null){
			q=q.setParameter("manufCode", pharmProblemCriteriaIn.getManufCode());}
		if(pharmProblemCriteriaIn.getPharmCompanyCode()!=null){
			q=q.setParameter("pharmCompanyCode", pharmProblemCriteriaIn.getPharmCompanyCode());}
		if(pharmProblemCriteriaIn.getProblemNum()!=null){
			q=q.setParameter("problemNum", pharmProblemCriteriaIn.getProblemNum());}
		if(pharmProblemCriteriaIn.getProblemClassification()!=null){
			q=q.setParameter("problemClassification", pharmProblemCriteriaIn.getProblemClassification());
			q=q.setParameter("problemClassificationDataValue", pharmProblemCriteriaIn.getProblemClassification().getDataValue());}
		if(pharmProblemCriteriaIn.getProblemStatus()!=null){
			q=q.setParameter("problemStatus", pharmProblemCriteriaIn.getProblemStatus());
			q=q.setParameter("problemStatusDataValue", pharmProblemCriteriaIn.getProblemStatus().getDataValue());}
		else{
			q=q.setParameter("draftedProblemStatus", ProblemStatus.Drafted);
		}
		if(pharmProblemCriteriaIn.getCaseNum()!=null){
			q=q.setParameter("caseNum", pharmProblemCriteriaIn.getCaseNum());
		}
		
		
		
		q=q.setHint(QueryHints.FETCH, "o.problemHeader");
		q=q.setHint(QueryHints.FETCH, "o.institution");
		q=q.setHint(QueryHints.FETCH, "o.contract");
		q=q.setHint(QueryHints.FETCH, "o.contract.supplier");
		q=q.setHint(QueryHints.FETCH, "o.contract.manufacturer");
		q=q.setHint(QueryHints.FETCH, "o.contract.pharmCompany");
		q=q.setHint(QueryHints.FETCH, "o.pharmBatchNumList");
		q=q.setHint(QueryHints.BATCH, "o.pharmProblemCountryList");
		q=q.setHint(QueryHints.FETCH, "o.pharmProblemNatureList");
		q=q.setHint(QueryHints.BATCH, "o.sendCollectSample");
		q=q.setHint(QueryHints.BATCH, "o.pharmProblemFileList");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.classification");
		
		List<PharmProblem> resultList = q.getResultList();
		
		if(resultList!=null && resultList.size()>0)
		{
			for(PharmProblem pc:resultList)
			{
				getLazyPharmProblem(pc);
			}
			
			Collections.sort(resultList, new ProblemNumComparator());
			
		}
		else
		{
			resultList=null;
		}
		
		
		return resultList;
		
	}
	
	public PharmProblem getLazyPharmProblem(PharmProblem pharmProblemLazy){
		// lazy
		pharmProblemLazy.getPharmProblemCountryList();
		pharmProblemLazy.getPharmProblemCountryList().size();
		pharmProblemLazy.getPharmBatchNumList();
		pharmProblemLazy.getPharmBatchNumList().size();
		pharmProblemLazy.getPharmProblemNatureList();
		pharmProblemLazy.getPharmProblemNatureList().size();
		pharmProblemLazy.getPharmProblemFileList();
		pharmProblemLazy.getPharmProblemFileList().size();
		pharmProblemLazy.getSendCollectSample();
		pharmProblemLazy.getInstitution();
		pharmProblemLazy.getProblemHeader();
		pharmProblemLazy.getQaProblem();
		pharmProblemLazy.getContract();
		if(pharmProblemLazy.getContract()!=null)
		{
			pharmProblemLazy.getContract().getSupplier();
			pharmProblemLazy.getContract().getManufacturer();
			pharmProblemLazy.getContract().getPharmCompany();
			
			if(pharmProblemLazy.getContract().getSupplier()!=null)
			{
				pharmProblemLazy.getContract().getSupplier().getContact();
			}
			if(pharmProblemLazy.getContract().getManufacturer()!=null)
			{
				pharmProblemLazy.getContract().getManufacturer().getContact();
			}
			if(pharmProblemLazy.getContract().getPharmCompany()!=null)
			{
				pharmProblemLazy.getContract().getPharmCompany().getContact();
			}
		}
		
		if(pharmProblemLazy.getSendCollectSample()!=null)
		{
			pharmProblemLazy.getSendCollectSample().getSendToQa();
			pharmProblemLazy.getSendCollectSample().getSendToSupp();
		}
		
		return pharmProblemLazy;
	}
	
	public static class ProblemNumComparator implements Comparator<PharmProblem>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		public int compare(PharmProblem c1, PharmProblem c2){
			return c1.getProblemNum()
			.compareTo( 
					c2.getProblemNum()
			);
		}						
	}
	
		
	@Remove
	public void destroy(){
		if (pharmProblemList != null){
			pharmProblemList = null;
		}
		if (draftPharmProblemList != null){
			draftPharmProblemList = null;
		}
	}
}
