package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MicroBioTestQtyReq  implements StringValuedEnum {
	
	Is4Units("MR1", "4 units"),
	Is5Units("MR2", "5 units"),
	Is7Units("MR3", "7 units"),
	Is8Units("MR4", "8 units"),
	Is14Units("MR5", "14 units"),
	Is28Units("MR6", "28 units"),
	Is10g("MR7", "10g"),
	Is20g("MR8", "20g"),
	Is20gto30g("MR9", "20-30g"),
	Other("O", "Other");
	
	private final String dataValue;
    private final String displayValue;
	
    MicroBioTestQtyReq(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static class Converter extends StringValuedEnumConverter<MicroBioTestQtyReq> {

		private static final long serialVersionUID = 3601829209059683642L;

		@Override
    	public Class<MicroBioTestQtyReq> getEnumClass() {
    		return MicroBioTestQtyReq.class;
    	}
    }
}
