package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.udt.coa.AttRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import javax.ejb.Local;

@Local
public interface CoaInEmailAttManagerLocal {	 
	
	CoaInEmailAtt createCoaInEmailAtt( CoaInEmail coaInEmail, String fileName );
	
	CoaInEmailAtt updateCoaInEmailAttVerifyResult(CoaInEmailAtt att, AttRemarkType remarkType);
	
	CoaInEmailAtt updateCoaInEmailAtt(CoaInEmailAtt att, CoaFileFolder folder);
	
	CoaInEmailAtt updateCoaInEmailAttResult(CoaInEmailAtt emailAtt, ValidateResultFlag validateFlag,AttRemarkType remarkType);
	
	CoaInEmailAtt retrieveCoaInEmailAttForCoaFileFolder(CoaFileFolder coaFileFolder);
}
