package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_PROBLEM_INSTITUTION")
@NamedQueries({
	@NamedQuery(name = "QaProblemInstitution.findByQaProblemId", query = "select o from QaProblemInstitution o where o.qaProblem.qaProblemId = :qaProblemId "),
	@NamedQuery(name = "QaProblemInstitution.findByQaProblemIdAllInstitutionFlag", query = "select o from QaProblemInstitution o where o.qaProblem.qaProblemId = :qaProblemId and o.allInstitutionFlag = :allInstitutionFlag "),
	@NamedQuery(name = "QaProblemInstitution.deleteByQaProblemIdAllInstitutionFlagN", query = "delete from QaProblemInstitution o where o.qaProblem.qaProblemId = :qaProblemId and (o.allInstitutionFlag = :allInstitutionFlag or o.allInstitutionFlag is null) "),
	@NamedQuery(name = "QaProblemInstitution.findLikeCaseNumPcuCode", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum like :caseNum and o.qaProblem.recordStatus = :recordStatus and o.institution.pcuCode = :pcuCode  "),
	@NamedQuery(name = "QaProblemInstitution.findLikeCaseNumInstitutionCode", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum like :caseNum and o.qaProblem.recordStatus = :recordStatus and o.institution.institutionCode = :institutionCode "),
	@NamedQuery(name = "QaProblemInstitution.findLikeCaseNumAllInstitutionFlag", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum like :caseNum and o.qaProblem.recordStatus = :recordStatus and o.allInstitutionFlag = :allInstitutionFlag "),
	@NamedQuery(name = "QaProblemInstitution.findByCaseNumPcuCode", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum = :caseNum and o.qaProblem.recordStatus = :recordStatus and o.institution.pcuCode = :pcuCode  "),
	@NamedQuery(name = "QaProblemInstitution.findByCaseNumInstitutionCode", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum = :caseNum and o.qaProblem.recordStatus = :recordStatus and o.institution.institutionCode = :institutionCode "),
	@NamedQuery(name = "QaProblemInstitution.findByCaseNumAllInstitutionFlag", query = "select distinct o.qaProblem from QaProblemInstitution o where o.qaProblem.caseNum = :caseNum and o.qaProblem.recordStatus = :recordStatus and o.allInstitutionFlag = :allInstitutionFlag ")
})
@Customizer(AuditCustomizer.class)
public class QaProblemInstitution extends VersionEntity {

	private static final long serialVersionUID = -7171365832963169929L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaProblemInstitutionSeq")
	@SequenceGenerator(name = "qaProblemInstitutionSeq", sequenceName = "SEQ_QA_PROBLEM_INSTITUTION", initialValue=10000)
	@Id
	@Column(name="QA_PROBLEM_INSTITUTION_ID", nullable=false)
	private Long qaProblemInstitutionId;

	
	@Converter(name = "QaProblemInstitution.allInstitutionFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemInstitution.allInstitutionFlag")
	@Column(name="ALL_INSTITUTION_FLAG", length=1)
	private YesNoFlag allInstitutionFlag;
	
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	
	@ManyToOne
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;

	@Transient
	private boolean selected;
	
	public Long getQaProblemInstitutionId() {
		return qaProblemInstitutionId;
	}

	public void setQaProblemInstitutionId(Long qaProblemInstitutionId) {
		this.qaProblemInstitutionId = qaProblemInstitutionId;
	}

	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	
	public YesNoFlag getAllInstitutionFlag() {
		return allInstitutionFlag;
	}

	public void setAllInstitutionFlag(YesNoFlag allInstitutionFlag) {
		this.allInstitutionFlag = allInstitutionFlag;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}
	
}
