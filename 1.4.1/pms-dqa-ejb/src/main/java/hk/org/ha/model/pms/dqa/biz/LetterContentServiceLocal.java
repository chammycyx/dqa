package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.vo.letter.LetterContent;

import javax.ejb.Local;

@Local
public interface LetterContentServiceLocal {
	LetterContent retrieveConflictOfInterest();
	LetterContent retrieveDelayDeliveryRptFormConflictOfInterest();
	void destroy();
}
