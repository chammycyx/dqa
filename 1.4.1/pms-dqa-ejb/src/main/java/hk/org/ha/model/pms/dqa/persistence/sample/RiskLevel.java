package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import org.hibernate.validator.Length;

@Entity
@Table(name = "RISK_LEVEL")
@NamedQueries({
	@NamedQuery(name = "RiskLevel.findByRecordStatus", query = "select o from RiskLevel o where o.recordStatus = :recordStatus order by o.riskLevelCode "),
	@NamedQuery(name = "RiskLevel.findByRiskLevelCode", query = "select o from RiskLevel o where o.riskLevelCode = :riskLevelCode order by o.riskLevelCode ")
})
@Customizer(AuditCustomizer.class)
public class RiskLevel extends VersionEntity{

	private static final long serialVersionUID = 8198988568117409298L;

	@Id
	@Column(name="RISK_LEVEL_CODE", length=3)
	@Length(min = 1, max = 3)
	private String riskLevelCode;

	@Column(name="RISK_LEVEL_DESC", length=40)
	@Length(min = 1, max = 40)
	private String riskLevelDesc;

	@Converter(name = "RiskLevel.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("RiskLevel.recordStatus")
	@Column(name="RECORD_STATUS", length = 1)
    private RecordStatus recordStatus;
	
	
	@Converter(name = "RiskLevel.proceedSampleTestFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("RiskLevel.proceedSampleTestFlag")
	@Column(name="PROCEED_SAMPLE_TEST_FLAG", length = 1)
	private YesNoFlag proceedSampleTestFlag;
	
	
	public String getRiskLevelCode() {
		return riskLevelCode;
	}

	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}

	
	public String getRiskLevelDesc() {
		return riskLevelDesc;
	}

	public void setRiskLevelDesc(String riskLevelDesc) {
		this.riskLevelDesc = riskLevelDesc;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setProceedSampleTestFlag(YesNoFlag proceedSampleTestFlag) {
		this.proceedSampleTestFlag = proceedSampleTestFlag;
	}

	public YesNoFlag getProceedSampleTestFlag() {
		return proceedSampleTestFlag;
	}

}
