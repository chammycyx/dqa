package hk.org.ha.model.pms.dqa.biz.sample;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import javax.ejb.Local;

@Local
public interface SampleTestEmailFaxManagerLocal {
	LetterTemplate constructLetterTemplateForSchedule(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, SampleTestSchedule sampleTestScheduleIn);
	void retrieveLetterTemplateForLabTest(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn);
	void retrieveLetterTemplateForSchedule(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn);
	void retrieveLetterTemplateForInstitutionAssignment(SampleTestSchedule sampleTestScheduleIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, UserInfo defaultCpoContactUserInfo, UserInfo cpoSignatureUserInfo);
}
