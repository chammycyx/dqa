package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.coa.ReportDateType;
import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaDiscrepancyRptCriteria {
	
	private String supplierCode;
	private String itemCode;
	private ReportGroupByType groupType;
	private Date startDate;
	private Date endDate;
	private ReportDateType reportDateType; 
	
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public ReportGroupByType getGroupType() {
		return groupType;
	}
	public void setGroupType(ReportGroupByType groupType) {
		this.groupType = groupType;
	}
	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()) : null;
	}
	public void setStartDate(Date startDate) {
		if (startDate != null){
			this.startDate = new Date(startDate.getTime());
		}
	}
	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}
	public void setEndDate(Date endDate) {
		if (endDate != null){
			this.endDate = new Date(endDate.getTime());
		}
	}
	public ReportDateType getReportDateType() {
		return reportDateType;
	}
	public void setReportDateType(ReportDateType reportDateType) {
		this.reportDateType = reportDateType;
	}
	
	
}
