package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface CompanyListServiceLocal {
	
	void retrieveManufacturerList();
	
	void retrieveManufacturerListLike(String manufacturerCode);
	
	void retrieveCompanyListLikeCompanyCode(String companyCode);
	
	void destroy();

}
