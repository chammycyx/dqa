package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFormData;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemDraftSaveService")
@RemoteDestination
@MeasureCalls
public class PharmProblemDraftSaveServiceBean implements PharmProblemDraftSaveServiceLocal {
	
	@In
	private PharmProblemManagerLocal pharmProblemManager;
	
	private String uploadPath;

	private String suppPerfNotificationToEmail;	
	
	public PharmProblemFormData createPharmProblemFormData() {
		UamInfo uamInfo = (UamInfo) Contexts.getSessionContext().get("uamInfo");
		PharmProblemFormData data = new PharmProblemFormData();
		data.setCoordinatorName(uamInfo.getUserId());
		return data;
	}
	
	public Contract constructContractSave(PharmProblem pharmProblemIn) {
		return pharmProblemManager.constructContractSave(pharmProblemIn);
	}

	public PharmProblem addPharmProblem() {
		return pharmProblemManager.addPharmProblem();
	}
	
	public boolean createPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean) {
		pharmProblemManager.setUploadPath(uploadPath);
		pharmProblemManager.setSuppPerfNotificationToEmail(suppPerfNotificationToEmail);
		return pharmProblemManager.createPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList, sendEmailBoolean);
	}

	public boolean createUpdateDraftPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList) {
		pharmProblemManager.setUploadPath(uploadPath);
		pharmProblemManager.setSuppPerfNotificationToEmail(suppPerfNotificationToEmail);
		PharmProblem pharmProblemInDB = pharmProblemManager.createUpdateDraftPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList);
		if (pharmProblemInDB != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean updateToNullProblemStausPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean) {
		pharmProblemManager.setUploadPath(uploadPath);
		pharmProblemManager.setSuppPerfNotificationToEmail(suppPerfNotificationToEmail);
		PharmProblem pharmProblemInDB = pharmProblemManager.updateToNullProblemStausPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList, sendEmailBoolean);
		if (pharmProblemInDB != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public PharmProblemFormData retrievePharmProblemFormData(PharmProblem pharmProblemIn) {
		return pharmProblemManager.retrievePharmProblemByPharmProblem(pharmProblemIn);
	}
	
	@Remove
	public void destroy(){		
		uploadPath = null;
		suppPerfNotificationToEmail = null;
	}
}
