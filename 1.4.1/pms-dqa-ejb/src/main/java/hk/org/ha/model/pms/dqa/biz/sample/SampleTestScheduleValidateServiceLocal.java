package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleValidateServiceLocal {
	
	void validateSampleTestScheduleForCreate(SampleTestSchedule sampleTestScheduleIn);
	
	void validateSampleTestScheduleForUpdate(SampleTestSchedule sampleTestScheduleIn);
	
	void destory();
	
	boolean isSuccess();
	
	String getErrorCode();
}
