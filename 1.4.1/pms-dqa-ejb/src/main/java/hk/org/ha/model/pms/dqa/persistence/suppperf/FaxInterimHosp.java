package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_INTERIM_HOSP")
@Customizer(AuditCustomizer.class)
public class FaxInterimHosp extends VersionEntity {

	private static final long serialVersionUID = -1769307709021363792L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxInterimHospSeq")
	@SequenceGenerator(name = "faxInterimHospSeq", sequenceName = "SEQ_FAX_INTERIM_HOSP", initialValue=10000)
	@Id
	@Column(name="FAX_INTERIM_HOSP_ID", nullable=false)
	private Long faxInterimHospId;
	
	@Converter(name = "FaxInterimHosp.investRptFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimHosp.investRptFlag")
	@Column(name="INVEST_RPT_FLAG", length=1)
	private YesNoFlag investRptFlag;
	
	@Converter(name = "FaxInterimHosp.withHoldUseFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimHosp.withHoldUseFlag")
	@Column(name="WITH_HOLD_USE_FLAG", length=1)
	private YesNoFlag withHoldUseFlag;
	
	@Converter(name = "FaxInterimHosp.stkReplaceFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimHosp.stkReplaceFlag")
	@Column(name="STK_REPLACE_FLAG", length=1)
	private YesNoFlag stkReplaceFlag;
	
	@Column(name="OTHER_DESC", length=200)
	private String otherDesc;

	public Long getFaxInterimHospId() {
		return faxInterimHospId;
	}

	public void setFaxInterimHospId(Long faxInterimHospId) {
		this.faxInterimHospId = faxInterimHospId;
	}

	public YesNoFlag getInvestRptFlag() {
		return investRptFlag;
	}

	public void setInvestRptFlag(YesNoFlag investRptFlag) {
		this.investRptFlag = investRptFlag;
	}

	public YesNoFlag getWithHoldUseFlag() {
		return withHoldUseFlag;
	}

	public void setWithHoldUseFlag(YesNoFlag withHoldUseFlag) {
		this.withHoldUseFlag = withHoldUseFlag;
	}

	public YesNoFlag getStkReplaceFlag() {
		return stkReplaceFlag;
	}

	public void setStkReplaceFlag(YesNoFlag stkReplaceFlag) {
		this.stkReplaceFlag = stkReplaceFlag;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}

	

}
