package hk.org.ha.model.pms.dqa.biz;

import javax.ejb.Local;

@Local
public interface PhsImportServiceLocal {

	Boolean importSupplier();
	
	Boolean importCompany();
	
	Boolean importInstitution();
	
	Boolean importStockMonthlyExp();
	
	Boolean updateFuncSeqNumYear();
}
