package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PROBLEM_NATURE_SUB_CAT")
@NamedQueries({
	@NamedQuery(name = "ProblemNatureSubCat.findAll", query = "select o from ProblemNatureSubCat o where o.recordStatus = :recordStatus order by o.problemNatureCat.problemNatureParam.displayOrder, o.problemNatureCat.problemNatureParam.paramDesc, o.problemNatureCat.displayOrder, o.problemNatureCat.catDesc, o.displayOrder, o.subCatDesc "),
	@NamedQuery(name = "ProblemNatureSubCat.findByProblemNatureSubCatId", query = "select o from ProblemNatureSubCat o where o.problemNatureSubCatId = :problemNatureSubCatId and o.recordStatus = :recordStatus order by o.displayOrder, o.subCatDesc "),
	@NamedQuery(name = "ProblemNatureSubCat.findBySubCatDescCatId", query = "select o from ProblemNatureSubCat o join o.problemNatureCat p where o.subCatDesc = :subCatDesc and p.problemNatureCatId = :problemNatureCatId and o.recordStatus = :recordStatus and p.recordStatus = :recordStatus order by o.displayOrder, o.subCatDesc "),
	@NamedQuery(name = "ProblemNatureSubCat.findByCatIdRecordStatus", query = "select o from ProblemNatureSubCat o join o.problemNatureCat p where p.problemNatureCatId = :problemNatureCatId and o.recordStatus = :recordStatus and p.recordStatus = :recordStatus order by o.displayOrder, o.subCatDesc "),
	@NamedQuery(name = "ProblemNatureSubCat.deleteByProblemNatureParamId", query = "update ProblemNatureSubCat o set o.recordStatus = :recordStatus where o.problemNatureCat.problemNatureParam.problemNatureParamId = :problemNatureParamId "),
	@NamedQuery(name = "ProblemNatureSubCat.deleteByProblemNatureCatId", query = "update ProblemNatureSubCat o set o.recordStatus = :recordStatus where o.problemNatureCat.problemNatureCatId = :problemNatureCatId "),
	@NamedQuery(name = "ProblemNatureSubCat.deleteByProblemNatureSubCatId", query = "update ProblemNatureSubCat o set o.recordStatus = :recordStatus where o.problemNatureSubCatId = :problemNatureSubCatId ")
})
@Customizer(AuditCustomizer.class)
public class ProblemNatureSubCat extends VersionEntity {

	private static final long serialVersionUID = 5050787606154123942L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "problemNatureSubCatSeq")
	@SequenceGenerator(name = "problemNatureSubCatSeq", sequenceName = "SEQ_PROBLEM_NATURE_SUB_CAT", initialValue=10000)
	@Id
	@Column(name="PROBLEM_NATURE_SUB_CAT_ID", nullable=false)
	private Long problemNatureSubCatId;

	@Column(name="SUB_CAT_DESC", length=100, nullable=false)
	private String subCatDesc;
	
	@Column(name="DISPLAY_ORDER", nullable=false)
    private Integer displayOrder;
	
	@Converter(name = "ProblemNatureSubCat.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("ProblemNatureSubCat.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@Transient
	private boolean selected;
	
	@Transient
	private boolean showParam;
	
	@Transient
	private boolean showCat;
	
	@ManyToOne
	@JoinColumn(name="PROBLEM_NATURE_CAT_ID")
	private ProblemNatureCat problemNatureCat;
	

	public Long getProblemNatureSubCatId() {
		return problemNatureSubCatId;
	}

	public void setProblemNatureSubCatId(Long problemNatureSubCatId) {
		this.problemNatureSubCatId = problemNatureSubCatId;
	}

	public String getSubCatDesc() {
		return subCatDesc;
	}

	public void setSubCatDesc(String subCatDesc) {
		this.subCatDesc = subCatDesc;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public ProblemNatureCat getProblemNatureCat() {
		return problemNatureCat;
	}

	public void setProblemNatureCat(ProblemNatureCat problemNatureCat) {
		this.problemNatureCat = problemNatureCat;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

	public boolean isShowParam() {
		return showParam;
	}

	public void setShowParam(boolean showParam) {
		this.showParam = showParam;
	}

	public boolean isShowCat() {
		return showCat;
	}

	public void setShowCat(boolean showCat) {
		this.showCat = showCat;
	}

}
