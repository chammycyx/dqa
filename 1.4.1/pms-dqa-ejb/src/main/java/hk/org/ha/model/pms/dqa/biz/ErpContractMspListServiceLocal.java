package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ErpContractMspListServiceLocal {

	List<ContractMsp> retrieveContractMspListByContractId( Long contractId );
	
	void updateContractMspListForLogicallyDeleted( Long contractId );
	
}
