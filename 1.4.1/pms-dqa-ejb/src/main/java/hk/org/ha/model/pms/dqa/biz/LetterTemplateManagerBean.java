package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.vo.letter.LetterContent;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("letterTemplateManager")
public class LetterTemplateManagerBean implements LetterTemplateManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String JAXB_CONTEXT_LETTER = "hk.org.ha.model.pms.vo.letter";
	
	private JaxbWrapper<LetterContent> rxJaxbWrapper;
	
	private void initJaxbWrapper() {
		if (rxJaxbWrapper == null) {
			rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LETTER);
		}
	}
	
	public LetterTemplate retrieveLetterTemplateByCode(String code) {
		logger.debug("retrieveLetterTemplateByCode #0", code);
		LetterTemplate tmpLt = em.find(LetterTemplate.class, code);
		
		if( tmpLt != null ) {
			LetterTemplate lt = new LetterTemplate();		
			lt.setCode(tmpLt.getCode());
			lt.setName(tmpLt.getName());
			lt.setSubject(tmpLt.getSubject());
			lt.setContent(tmpLt.getContent());
			return lt;
		}
		return null;
	}
	
	public LetterContent retrieveLetterContentByCode(String code) {
		initJaxbWrapper();
		logger.debug("retrieveLetterContentByCode #0", code);
		LetterTemplate template = retrieveLetterTemplateByCode(code);
		return rxJaxbWrapper.unmarshall(template.getContent());	
	}
}
