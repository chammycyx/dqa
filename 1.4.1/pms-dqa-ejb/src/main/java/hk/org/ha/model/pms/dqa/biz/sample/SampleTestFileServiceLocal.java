package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;

import javax.ejb.Local;

@Local
public interface SampleTestFileServiceLocal {
	
	void retrieveSampleTestFileBySampleTestFileId(Long sampleTestFileId);
	
	SampleTestFile uploadFile(byte[] bytes, String fileIn, SampleTestFile sampleTestFileIn);
	
	void addSampleTestFile();
	
	void addSampleTestFileForTestResult(SampleTestFile sampleTestFileIn);
	
	void createSampleTestFile(SampleTestFile sampleTestFileIn);
	
	void updateSampleTestFile();
	
	void deleteSampleTestFile(SampleTestFile sampleTestFileIn);
	
	boolean isSuccess();

	String getErrorCode();
	
	void destroy();

}
