package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
	
@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestScheduleTestResult {	

	private String scheduleStatus;
	private Date scheduleMonth;
	private String scheduleNum;
	private Date testReportDate;
	private String itemCode;
	private String itemDesc;
	private String testCode;
	private String orderType;
	private String riskLevelCode;
	private String labCode;
	private Double testPrice;
	private Date prIssueDate;
	private String poNum;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	private String testResult;
	
	private Date scheduleConfirmDate;
	private Date scheduleStartDate;
	private String recordStatus;
	private String cancelReason;
	private String baseUnit;
	private String contractNum;
	private Integer packSize;
	private Double packPrice;
	private String currencyCode;
	private String batchNum;
	private String expiryDate;
	private Integer reqQty;
	private Integer recQty;
	private String labCollectNum;
	private Date labCollectDate;
	private String testResultDesc;
	private Double refDrugCost;
	private String institutionCode;
	private String rawMaterial;
	private String validationRpt;
	private String validationRptValidityFlag;
	private String microTestParam;
	private String microNationalPharmStd;
	private String acceptCriteria;
	private String acceptCriteriaDesc;
	private String specification;
	private String moa;
	private String fps;
	private String refStd;
	private String refStdReqFlag;
	private Integer retentionQty;
	private String chemNationalPharmStd;
	private String chemTest;
	private String chemTestDesc;
	private Double chequeAmount;
	private String chequeNum;
	private Date chequeReceiveDate;
	private Date chequeSendDate;
	private String invRefNum;
	private Date invReceiveDate;
	private Date invSendDate;
	private String paymentRemark;

	
	public String getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}
	
	public Date getScheduleMonth() {
		return (scheduleMonth != null) ? new Date(scheduleMonth.getTime()) : null;
	}
	public void setScheduleMonth(Date scheduleMonth) {
		if (scheduleMonth != null){
			this.scheduleMonth = new Date(scheduleMonth.getTime());
		}
	}
	public String getScheduleNum() {
		return scheduleNum;
	}
	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}
	public Date getTestReportDate() {
		return (testReportDate != null) ? new Date(testReportDate.getTime()) : null;
	}
	public void setTestReportDate(Date testReportDate) {
		if (testReportDate != null){
			this.testReportDate = new Date(testReportDate.getTime());
		}
	}
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getTestCode() {
		return testCode;
	}
	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}
	
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getRiskLevelCode() {
		return riskLevelCode;
	}
	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}
	public String getLabCode() {
		return labCode;
	}
	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}
	public Double getTestPrice() {
		return testPrice;
	}
	public void setTestPrice(Double testPrice) {
		this.testPrice = testPrice;
	}
	public Date getPrIssueDate() {
		return (prIssueDate != null) ? new Date(prIssueDate.getTime()) : null;
	}
	public void setPrIssueDate(Date prIssueDate) {
		if (prIssueDate != null){
			this.prIssueDate = new Date(prIssueDate.getTime());
		}
	}
	public String getPoNum() {
		return poNum;
	}
	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}

	
	
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	
	public Date getScheduleConfirmDate() {
		return (scheduleConfirmDate != null) ? new Date(scheduleConfirmDate.getTime()) : null;
	}
	public void setScheduleConfirmDate(Date scheduleConfirmDate) {
		if (scheduleConfirmDate != null){
			this.scheduleConfirmDate = new Date(scheduleConfirmDate.getTime());
		}
	}
	public Date getScheduleStartDate() {
		return (scheduleStartDate != null) ? new Date(scheduleStartDate.getTime()) : null;
	}
	public void setScheduleStartDate(Date scheduleStartDate) {
		if (scheduleStartDate != null){
			this.scheduleStartDate = new Date(scheduleStartDate.getTime());
		}
	}
	public String getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getBaseUnit() {
		return baseUnit;
	}
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public Integer getPackSize() {
		return packSize;
	}
	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}
	public Double getPackPrice() {
		return packPrice;
	}
	public void setPackPrice(Double packPrice) {
		this.packPrice = packPrice;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Integer getReqQty() {
		return reqQty;
	}
	public void setReqQty(Integer reqQty) {
		this.reqQty = reqQty;
	}
	public Integer getRecQty() {
		return recQty;
	}
	public void setRecQty(Integer recQty) {
		this.recQty = recQty;
	}
	public String getLabCollectNum() {
		return labCollectNum;
	}
	public void setLabCollectNum(String labCollectNum) {
		this.labCollectNum = labCollectNum;
	}
	public Date getLabCollectDate() {
		return (labCollectDate != null) ? new Date(labCollectDate.getTime()) : null;
	}
	public void setLabCollectDate(Date labCollectDate) {
		if (labCollectDate != null){
			this.labCollectDate = new Date(labCollectDate.getTime());
		}
	}
	public String getTestResultDesc() {
		return testResultDesc;
	}
	public void setTestResultDesc(String testResultDesc) {
		this.testResultDesc = testResultDesc;
	}
	public Double getRefDrugCost() {
		return refDrugCost;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public void setRefDrugCost(Double refDrugCost) {
		this.refDrugCost = refDrugCost;
	}
	public String getRawMaterial() {
		return rawMaterial;
	}
	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}
	public String getValidationRpt() {
		return validationRpt;
	}
	public void setValidationRpt(String validationRpt) {
		this.validationRpt = validationRpt;
	}
	public String getValidationRptValidityFlag() {
		return validationRptValidityFlag;
	}
	public void setValidationRptValidityFlag(String validationRptValidityFlag) {
		this.validationRptValidityFlag = validationRptValidityFlag;
	}
	public String getMicroTestParam() {
		return microTestParam;
	}
	public void setMicroTestParam(String microTestParam) {
		this.microTestParam = microTestParam;
	}
	public String getMicroNationalPharmStd() {
		return microNationalPharmStd;
	}
	public void setMicroNationalPharmStd(String microNationalPharmStd) {
		this.microNationalPharmStd = microNationalPharmStd;
	}
	public String getAcceptCriteria() {
		return acceptCriteria;
	}
	public void setAcceptCriteria(String acceptCriteria) {
		this.acceptCriteria = acceptCriteria;
	}
	public String getAcceptCriteriaDesc() {
		return acceptCriteriaDesc;
	}
	public void setAcceptCriteriaDesc(String acceptCriteriaDesc) {
		this.acceptCriteriaDesc = acceptCriteriaDesc;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getMoa() {
		return moa;
	}
	public void setMoa(String moa) {
		this.moa = moa;
	}
	public String getFps() {
		return fps;
	}
	public void setFps(String fps) {
		this.fps = fps;
	}
	public String getRefStd() {
		return refStd;
	}
	public void setRefStd(String refStd) {
		this.refStd = refStd;
	}
	public String getRefStdReqFlag() {
		return refStdReqFlag;
	}
	public void setRefStdReqFlag(String refStdReqFlag) {
		this.refStdReqFlag = refStdReqFlag;
	}
	public Integer getRetentionQty() {
		return retentionQty;
	}
	public void setRetentionQty(Integer retentionQty) {
		this.retentionQty = retentionQty;
	}
	public String getChemNationalPharmStd() {
		return chemNationalPharmStd;
	}
	public void setChemNationalPharmStd(String chemNationalPharmStd) {
		this.chemNationalPharmStd = chemNationalPharmStd;
	}
	public String getChemTest() {
		return chemTest;
	}
	public void setChemTest(String chemTest) {
		this.chemTest = chemTest;
	}
	public String getChemTestDesc() {
		return chemTestDesc;
	}
	public void setChemTestDesc(String chemTestDesc) {
		this.chemTestDesc = chemTestDesc;
	}
	public Double getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(Double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	public String getChequeNum() {
		return chequeNum;
	}
	public void setChequeNum(String chequeNum) {
		this.chequeNum = chequeNum;
	}
	public Date getChequeReceiveDate() {
		return (chequeReceiveDate != null) ? new Date(chequeReceiveDate.getTime()) : null;
	}
	public void setChequeReceiveDate(Date chequeReceiveDate) {
		if (chequeReceiveDate != null){
			this.chequeReceiveDate = new Date(chequeReceiveDate.getTime());
		}
	}
	public Date getChequeSendDate() {
		return (chequeSendDate != null) ? new Date(chequeSendDate.getTime()) : null;
	}
	public void setChequeSendDate(Date chequeSendDate) {
		if (chequeSendDate != null){
			this.chequeSendDate = new Date(chequeSendDate.getTime());
		}
	}
	public String getInvRefNum() {
		return invRefNum;
	}
	public void setInvRefNum(String invRefNum) {
		this.invRefNum = invRefNum;
	}
	public Date getInvReceiveDate() {
		return (invReceiveDate != null) ? new Date(invReceiveDate.getTime()) : null;
	}
	public void setInvReceiveDate(Date invReceiveDate) {
		if (invReceiveDate != null){
			this.invReceiveDate = new Date(invReceiveDate.getTime());
		}
	}
	public Date getInvSendDate() {
		return (invSendDate != null) ? new Date(invSendDate.getTime()) : null;
	}
	public void setInvSendDate(Date invSendDate) {
		if (invSendDate != null){
			this.invSendDate = new Date(invSendDate.getTime());
		}
	}
	public String getPaymentRemark() {
		return paymentRemark;
	}
	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}

}
