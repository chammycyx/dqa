package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Result implements StringValuedEnum {
	
	FAIL("F", "Fail"),
	PASS("P", "Pass"),
	//VTS("VTS", "Validation Test Satisfactory"),
	//VTU("VTU", "Validation Test Unsatisfactory"),
	OTHER("O", "Other");
	
    private final String dataValue;
    private final String displayValue;
        
    Result(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Result> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<Result> getEnumClass() {
    		return Result.class;
    	}
    }
}
