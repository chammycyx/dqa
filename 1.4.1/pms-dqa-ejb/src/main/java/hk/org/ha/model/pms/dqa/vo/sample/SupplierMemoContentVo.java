package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Date;

import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.IAMemoContent;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
import org.jboss.seam.core.Interpolator;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SupplierMemoContentVo {
	
	private UserInfo userInfoFax; 
	private SampleTestSchedule sampleTestScheduleFax;
	private String toSupplier;
	private String toSupplierContactPerson;
	private String contractNum;
	private String receiverTitle;
	private String receiverFirstName;
	private String receiverLastName;
	private String position;
	private String address1;
	private String address2;
	private String address3;
	private String receiverTel;
	private String receiverFax;
	private String senderTitle;
	private String senderFirstName;
	private String senderLastName;	
	private String memoDate;
	private String itemCode;
	private String drugDesc;
	private String requiredQty;
	private String refNum;
	private String faxMemoParagraph1;
	private String faxMemoParagraph2;
	private String faxMemoParagraph3;
	private String faxMemoParagraph4;
	private String faxMemoParagraph5;
	private Date sendDate;//DQA-61
	private String supplierName;//DQA-59
	private String senderInfo;
	
	public void constructParagraph2() {
		String instName = sampleTestScheduleFax.getInstitution().getInstitutionName();
		
		faxMemoParagraph2 = Interpolator.instance().interpolate(IAMemoContent.IS2.getDisplayValue(), new Object[] {
			sampleTestScheduleFax.getTestPrice(),
			(StringUtils.contains(instName.toUpperCase(), "PHARMACY")?"":"PHARMACY, ") + instName
		});
	}
	
	public void constructParagraph3() {
		faxMemoParagraph3 = Interpolator.instance().interpolate(IAMemoContent.IS3.getDisplayValue(), new Object[] {
			contractNum,
			drugDesc,
			requiredQty
		});
	}
	
	public void builderSenderInfo() {
		StringBuffer resultBuffer = new StringBuffer();
		
		if (!StringUtils.isBlank(receiverTitle)) {
			resultBuffer.append(receiverTitle);
			resultBuffer.append(" ");
		}
		
		if (!StringUtils.isBlank(receiverFirstName)) {
			resultBuffer.append(receiverFirstName);
			resultBuffer.append(" ");
		}
		
		if (!StringUtils.isBlank(receiverLastName)) {
			resultBuffer.append(receiverLastName);			
		}
		
		resultBuffer.append('\n');
		
		if (!StringUtils.isBlank(position)) {
			resultBuffer.append(position);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(supplierName)) {
			resultBuffer.append(supplierName);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(address1)) {
			resultBuffer.append(address1);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(address2)) {
			resultBuffer.append(address2);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(address3)) {
			resultBuffer.append(address3);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(receiverFax)) {
			resultBuffer.append("Fax: ");
			resultBuffer.append(receiverFax);
			resultBuffer.append('\n');
		}
		
		if (!StringUtils.isBlank(receiverTel)) {
			resultBuffer.append("Tel: ");
			resultBuffer.append(receiverTel);
		}
		
		int lastIndexOfNewLine = resultBuffer.lastIndexOf("\n");
		
		if (lastIndexOfNewLine == (resultBuffer.length() - 1)) {
			resultBuffer.replace(lastIndexOfNewLine, lastIndexOfNewLine+1, "");
		}		
		
		setSenderInfo(resultBuffer.toString());
	}
	
	public void setUserInfoFax(UserInfo userInfoFax) {
		this.userInfoFax = userInfoFax;
	}
	public UserInfo getUserInfoFax() {
		return userInfoFax;
	}
	public void setSampleTestScheduleFax(SampleTestSchedule sampleTestScheduleFax) {
		this.sampleTestScheduleFax = sampleTestScheduleFax;
	}
	public SampleTestSchedule getSampleTestScheduleFax() {
		return sampleTestScheduleFax;
	}
	public void setToSupplier(String toSupplier) {
		this.toSupplier = toSupplier;
	}

	public String getToSupplier() {
		return toSupplier;
	}

	public void setToSupplierContactPerson(String toSupplierContactPerson) {
		this.toSupplierContactPerson = toSupplierContactPerson;
	}

	public String getToSupplierContactPerson() {
		return toSupplierContactPerson;
	}

	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getReceiverTitle() {
		return receiverTitle;
	}
	public void setReceiverTitle(String receiverTitle) {
		this.receiverTitle = receiverTitle;
	}
	public String getReceiverFirstName() {
		return receiverFirstName;
	}
	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}
	public String getReceiverLastName() {
		return receiverLastName;
	}
	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getReceiverTel() {
		return receiverTel;
	}
	public void setReceiverTel(String receiverTel) {
		this.receiverTel = receiverTel;
	}
	public String getReceiverFax() {
		return receiverFax;
	}
	public void setReceiverFax(String receiverFax) {
		this.receiverFax = receiverFax;
	}
	public String getSenderTitle() {
		return senderTitle;
	}
	public void setSenderTitle(String senderTitle) {
		this.senderTitle = senderTitle;
	}
	public String getSenderFirstName() {
		return senderFirstName;
	}
	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}
	public String getSenderLastName() {
		return senderLastName;
	}
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}
	public String getMemoDate() {
		return memoDate;
	}
	public void setMemoDate(String memoDate) {
		this.memoDate = memoDate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getDrugDesc() {
		return drugDesc;
	}
	public void setDrugDesc(String drugDesc) {
		this.drugDesc = drugDesc;
	}
	public String getRequiredQty() {
		return requiredQty;
	}
	public void setRequiredQty(String requiredQty) {
		this.requiredQty = requiredQty;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getFaxMemoParagraph1() {
		return faxMemoParagraph1;
	}
	public void setFaxMemoParagraph1(String faxMemoParagraph1) {
		this.faxMemoParagraph1 = faxMemoParagraph1;
	}
	public String getFaxMemoParagraph2() {
		return faxMemoParagraph2;
	}
	public void setFaxMemoParagraph2(String faxMemoParagraph2) {
		this.faxMemoParagraph2 = faxMemoParagraph2;
	}
	public String getFaxMemoParagraph3() {
		return faxMemoParagraph3;
	}
	public void setFaxMemoParagraph3(String faxMemoParagraph3) {
		this.faxMemoParagraph3 = faxMemoParagraph3;
	}
	public String getFaxMemoParagraph4() {
		return faxMemoParagraph4;
	}
	public void setFaxMemoParagraph4(String faxMemoParagraph4) {
		this.faxMemoParagraph4 = faxMemoParagraph4;
	}
	public String getFaxMemoParagraph5() {
		return faxMemoParagraph5;
	}
	public void setFaxMemoParagraph5(String faxMemoParagraph5) {
		this.faxMemoParagraph5 = faxMemoParagraph5;
	}

	public void setSendDate(Date sendDate) {
		if (sendDate != null) {
			this.sendDate = new Date(sendDate.getTime());
		} else {
			this.sendDate = null;
		}
	}

	public Date getSendDate() {
		return sendDate != null?new Date(sendDate.getTime()):null;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSenderInfo(String senderInfo) {
		this.senderInfo = senderInfo;
	}

	public String getSenderInfo() {
		return senderInfo;
	}
}
