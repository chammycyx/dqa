package hk.org.ha.model.pms.dqa.udt.suppperf;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ProblemBy implements StringValuedEnum {
	
	Pharmacy("P", "Pharmacy"),
	Nurse("N", "Nurse"),
	Doctor("D", "Doctor"),
	Other("O", "Other");	
	
    private final String dataValue;
    private final String displayValue;
        
    ProblemBy(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ProblemBy> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<ProblemBy> getEnumClass() {
    		return ProblemBy.class;
    	}
    }
}
