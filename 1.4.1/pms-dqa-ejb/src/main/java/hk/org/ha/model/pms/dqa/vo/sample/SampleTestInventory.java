package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestInventory {
	
	private Date invDate;
	private String itemCode;
	private String itemDesc;
	private String test;
	private String baseUnit;
	private Integer movementQty;
	private Double cdp;
	private String location;
	private String batchNum;
	private String expiryDate;
	private String orderType;
	private String supplier;
	private String manufacturer;
	private String pharmCompany;
	private String scheduleNum;
	private String adjustFlag;
	private String adjustReason;
		
	public Date getInvDate() {		
		return invDate!=null?new Date(invDate.getTime()):null;
	}
	
	public void setInvDate(Date invDate) {
		if (invDate != null) {
			this.invDate = new Date(invDate.getTime());
		}
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getTest() {
		return test;
	}
	
	public void setTest(String test) {
		this.test = test;
	}

	public String getBaseUnit() {
		return baseUnit;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Integer getMovementQty() {
		return movementQty;
	}
	
	public void setMovementQty(Integer movementQty) {
		this.movementQty = movementQty;
	}

	public Double getCdp() {
		return cdp;
	}
	
	public void setCdp(Double cdp) {
		this.cdp = cdp;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public String getBatchNum() {
		return batchNum;
	}
	
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getOrderType() {
		return orderType;
	}
	
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getSupplier() {
		return supplier;
	}
	
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getPharmCompany() {
		return pharmCompany;
	}
	
	public void setPharmCompany(String pharmCompany) {
		this.pharmCompany = pharmCompany;
	}

	public String getScheduleNum() {
		return scheduleNum;
	}
	
	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}

	public String getAdjustFlag() {
		return adjustFlag;
	}
	
	public void setAdjustFlag(String adjustFlag) {
		this.adjustFlag = adjustFlag;
	}

	public String getAdjustReason() {
		return adjustReason;
	}
	
	public void setAdjustReason(String adjustReason) {
		this.adjustReason = adjustReason;
	}
}
