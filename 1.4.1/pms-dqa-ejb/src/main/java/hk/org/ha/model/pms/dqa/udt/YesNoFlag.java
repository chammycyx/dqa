package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum YesNoFlag implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	Yes("Y", "Yes"),
	No("N", "No");	
	
    private final String dataValue;
    private final String displayValue;
        
    YesNoFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<YesNoFlag> {

		private static final long serialVersionUID = -4286190467093280043L;

		@Override
    	public Class<YesNoFlag> getEnumClass() {
    		return YesNoFlag.class;
    	}
    }
}
