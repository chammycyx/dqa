package hk.org.ha.model.pms.dqa.vo.delaydelivery;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormContact;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormPo;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormVer;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.delaydelivery.DelayDeliveryRptFormStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@ExternalizedBean(type=DefaultExternalizer.class)
public class DelayDeliveryRptFormData {

	//delaydeliveryRptForm
	private Long refNum;
	private Institution institution;
	private Date reportDate;
	private DelayDeliveryRptFormStatus delayDeliveryRptFormStatus;
	private Long onHandQty;
	private Long avgMthConsumptQty;
	private BigDecimal onHandLastFor;
	private String itemCode;
	private String fullDrugDesc;
	private String baseUnit;
	
	//contract
	private Long contractId;
	private OrderTypeAll orderType;
	private String contractNum;
	private String contractSuffix;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	
	//conflict of interest
	private String conflictMspName;
	private String conflictRelationship;
	private String conflictByName;
	private String conflictByRank;
	private Boolean conflictFlag;
	private Date conflictDate;
	
	private byte[] fileData;
	private FileItem fileItem;
	private String refUploadFileName;
	
	private Long version;
	
	private List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList;
	
	private List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList;

	private boolean cpoActionFlag;

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public Long getOnHandQty() {
		return onHandQty;
	}

	public void setOnHandQty(Long onHandQty) {
		this.onHandQty = onHandQty;
	}

	public Long getAvgMthConsumptQty() {
		return avgMthConsumptQty;
	}

	public void setAvgMthConsumptQty(Long avgMthConsumptQty) {
		this.avgMthConsumptQty = avgMthConsumptQty;
	}

	public BigDecimal getOnHandLastFor() {
		return onHandLastFor;
	}

	public void setOnHandLastFor(BigDecimal onHandLastFor) {
		this.onHandLastFor = onHandLastFor;
	}

	public String getConflictMspName() {
		return conflictMspName;
	}

	public void setConflictMspName(String conflictMspName) {
		this.conflictMspName = conflictMspName;
	}

	public String getConflictRelationship() {
		return conflictRelationship;
	}

	public void setConflictRelationship(String conflictRelationship) {
		this.conflictRelationship = conflictRelationship;
	}

	public String getConflictByName() {
		return conflictByName;
	}

	public void setConflictByName(String conflictByName) {
		this.conflictByName = conflictByName;
	}

	public String getConflictByRank() {
		return conflictByRank;
	}

	public void setConflictByRank(String conflictByRank) {
		this.conflictByRank = conflictByRank;
	}

	public Boolean getConflictFlag() {
		return conflictFlag;
	}

	public void setConflictFlag(Boolean conflictFlag) {
		this.conflictFlag = conflictFlag;
	}

	
	public List<DelayDeliveryRptFormPoData> getDelayDeliveryRptFormPoDataList() {
		return delayDeliveryRptFormPoDataList;
	}

	public void setDelayDeliveryRptFormPoDataList(
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList) {
		this.delayDeliveryRptFormPoDataList = delayDeliveryRptFormPoDataList;
	}


	public List<DelayDeliveryRptFormContactData> getDelayDeliveryRptFormContactDataList() {
		return delayDeliveryRptFormContactDataList;
	}

	public void setDelayDeliveryRptFormContactDataList(
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList) {
		this.delayDeliveryRptFormContactDataList = delayDeliveryRptFormContactDataList;
	}

	public DelayDeliveryRptFormStatus getDelayDeliveryRptFormStatus() {
		return delayDeliveryRptFormStatus;
	}

	public void setDelayDeliveryRptFormStatus(
			DelayDeliveryRptFormStatus delayDeliveryRptFormStatus) {
		this.delayDeliveryRptFormStatus = delayDeliveryRptFormStatus;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getContractSuffix() {
		return contractSuffix;
	}

	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}



	public FileItem getFileItem() {
		return fileItem;
	}

	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getFullDrugDesc() {
		return fullDrugDesc;
	}

	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}

	public Long getRefNum() {
		return refNum;
	}

	public void setRefNum(Long refNum) {
		this.refNum = refNum;
	}

	public OrderTypeAll getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public Date getConflictDate() {
		return conflictDate;
	}

	public void setConflictDate(Date conflictDate) {
		this.conflictDate = conflictDate;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public String getRefUploadFileName() {
		return refUploadFileName;
	}

	public void setRefUploadFileName(String refUploadFileName) {
		this.refUploadFileName = refUploadFileName;
	}

	public boolean isCpoActionFlag() {
		return cpoActionFlag;
	}

	public void setCpoActionFlag(boolean cpoActionFlag) {
		this.cpoActionFlag = cpoActionFlag;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	
	

}
