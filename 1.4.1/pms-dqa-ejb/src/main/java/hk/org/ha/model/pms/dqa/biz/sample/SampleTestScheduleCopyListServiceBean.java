package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleCopyListService")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleCopyListServiceBean implements SampleTestScheduleCopyListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestSchedule> sampleTestScheduleCopyList;

	private static final String UNCHECKED = "unchecked";
	private static final String RECORD_STATUS = "recordStatus";
	private static final String ITEM_CODE = "itemCode";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String O_CONTRACT = "o.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.contract.supplier";
	private static final String O_CONTRACT_MANUFACTURER = "o.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.contract.pharmCompany";
	private static final String O_SAMPLE_ITEM = "o.sampleItem";
	private static final String O_SAMPLE_ITEM_RISK_LEVEL = "o.sampleItem.riskLevel";
	private static final String O_INSTITUTION = "o.institution";
	private static final String SCHE_STATUS_LIST = "scheduleStatusList";
	
	
	private boolean success;
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForCopy(Long scheduleId, String itemCode){
		logger.debug("retrieveSampleTestScheduleListForCopy :#0 #1", itemCode, scheduleId );
		success = false;
		
		sampleTestScheduleCopyList= em.createNamedQuery("SampleTestSchedule.findForCopySchedule")
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHE_STATUS_LIST, new ArrayList<ScheduleStatus>(Arrays.asList(ScheduleStatus.Schedule, ScheduleStatus.ScheduleGen)))
			.setParameter("scheduleId", scheduleId)
			.setParameter(ITEM_CODE, itemCode)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.BATCH, O_INSTITUTION)
			.getResultList();
		
		if (!sampleTestScheduleCopyList.isEmpty()){
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleCopyList ) {
				getLazySampleTestSchedule(sampleTestScheduleFind);
			}
			success = true;
		}
	}
	
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	public boolean isSuccess() {
		return success;
	}

	@Remove
	public void destroy() {
		if (sampleTestScheduleCopyList !=null){
			sampleTestScheduleCopyList = null;
		}
	}
}
