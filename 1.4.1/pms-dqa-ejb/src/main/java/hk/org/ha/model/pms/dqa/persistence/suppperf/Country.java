package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "COUNTRY")
@NamedQueries({
	@NamedQuery(name = "Country.findByCountryCode", query = "select o from Country o where o.countryCode = :countryCode and o.recordStatus = :recordStatus order by o.countryCode"),
	@NamedQuery(name = "Country.findLikeCountryCode", query = "select o from Country o where o.countryCode like :countryCode and o.recordStatus = :recordStatus order by o.countryCode")
})
@Customizer(AuditCustomizer.class)
public class Country extends VersionEntity {

	private static final long serialVersionUID = 6097746633337169731L;

	@Id
	@Column(name="COUNTRY_CODE", length=2, nullable=false)
	private String countryCode;

	@Column(name="COUNTRY_NAME", nullable=false)
	private String countryName;

	@Converter(name = "Country.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("Country.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@OneToMany(mappedBy="country")
    private List<PharmProblemCountry> pharmProblemCountryList;
	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public List<PharmProblemCountry> getPharmProblemCountryList() {
		return pharmProblemCountryList;
	}

	public void setPharmProblemCountryList(List<PharmProblemCountry> pharmProblemCountryList) {
		this.pharmProblemCountryList = pharmProblemCountryList;
	}


}
