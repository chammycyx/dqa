package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureCatListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ProblemNatureCatListServiceBean implements ProblemNatureCatListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<ProblemNatureCat> problemNatureCatList;
	
	@SuppressWarnings("unchecked")
	public void retrieveProblemNatureCatList(ProblemNatureParam problemNatureParamIn){
		logger.debug("retrieveProblemNatureCatList");
		
		problemNatureCatList = em.createNamedQuery("ProblemNatureCat.findByParamIdRecordStatus")
		.setParameter("problemNatureParamId", problemNatureParamIn.getProblemNatureParamId())
		.setParameter("recordStatus", RecordStatus.Active)
		.setHint(QueryHints.FETCH, "o.problemNatureParam")
		.getResultList();

	}
	
	@Remove
	public void destroy(){
		if(problemNatureCatList != null) {
			problemNatureCatList = null;  
		}
	}

}
