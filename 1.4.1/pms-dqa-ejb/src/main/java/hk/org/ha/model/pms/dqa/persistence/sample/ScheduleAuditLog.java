package hk.org.ha.model.pms.dqa.persistence.sample;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "SCHEDULE_AUDIT_LOG")
@Customizer(AuditCustomizer.class)
public class ScheduleAuditLog extends VersionEntity {

	private static final long serialVersionUID = 5457478365900773665L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scheduleAuditLogSeq")
	@SequenceGenerator(name = "scheduleAuditLogSeq", sequenceName = "SEQ_SCHEDULE_AUDIT_LOG", initialValue=10000)
	@Column(name="LOG_ID")
	private Long logId;

	@Column(name="ACTION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date actionDate;

	@Column(name="ACTION_TYPE")
	private String actionType;

	@Column(name="REMARK")
	private String remark;

	@Column(name="REFERENCE")
	private String reference;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getActionDate() {
		return (actionDate != null) ? new Date(actionDate.getTime()) : null;
	}

	public void setActionDate(Date actionDate) {
		if (actionDate != null) {
			this.actionDate = new Date(actionDate.getTime());
		} else {
			this.actionDate = null;
		}
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
		
}
