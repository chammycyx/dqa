package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderManagerBean.CoaFileFolderResult;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;

import javax.ejb.Local;

@Local
public interface CoaFileFolderManagerLocal {
	
	CoaFileFolderResult upload(byte[] bytes, String fileName, CoaFileFolder coaFileFolderin);	
	
	String doUploadForEmail( byte[] bytes, String fileIn, CoaFileFolder coaFileFolderIn );
	
	void createCoaFileFolderForEmail( CoaFileFolder coaFileFolderIn );
	
	boolean createCoaFileFolder(CoaFileFolder coaFileFolderin);
	
	CoaFileFolder updateCoaFileFolderForEmail(CoaFileFolder coaFileFolderIn);
	
	boolean deletePhysicalFile(String fileName);
}
