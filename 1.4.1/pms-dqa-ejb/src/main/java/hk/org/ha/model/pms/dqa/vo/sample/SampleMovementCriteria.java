package hk.org.ha.model.pms.dqa.vo.sample;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.OrderType;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleMovementCriteria {
	
	
	private String institutionCode;
	
	private String itemCode;
	private OrderType orderType;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	
	private String labCode;
	private String testCode;
	private String batchNum;
	private String expiryDate;
	private String scheduleNum;
	private String inventorySelectionType;
	

	@Temporal(TemporalType.TIMESTAMP)
	private Date fromInventoryDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toInventoryDate;
	
	
	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getScheduleNum() {
		return scheduleNum;
	}

	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}

	public Date getFromInventoryDate() {
		return (fromInventoryDate != null) ? new Date(fromInventoryDate.getTime()) : null;
	}

	public void setFromInventoryDate(Date fromInventoryDate) {
		if (fromInventoryDate != null) {
			this.fromInventoryDate = new Date(fromInventoryDate.getTime());
		}
	}

	public Date getToInventoryDate() {
		return (toInventoryDate != null) ? new Date(toInventoryDate.getTime()) : null;
	}

	public void setToInventoryDate(Date toInventoryDate) {
		if (toInventoryDate != null) {
			this.toInventoryDate = new Date(toInventoryDate.getTime());
		}
	}
	
	public String getInventorySelectionType() {
		return inventorySelectionType;
	}

	public void setInventorySelectionType(String inventorySelectionType) {
		this.inventorySelectionType = inventorySelectionType;
	}

}
