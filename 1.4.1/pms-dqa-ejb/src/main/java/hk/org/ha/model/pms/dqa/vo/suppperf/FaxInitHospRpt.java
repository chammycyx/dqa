package hk.org.ha.model.pms.dqa.vo.suppperf;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FaxInitHospRpt {
	private String initFaxCreateDate;
	private String faxTo;
	private String responseRptDate;
	private String contractNo;
	private String refNum;
	private String itemCode;
	private String fullDrugDesc;
	private String manuf;
	private String problemDetail;
	private String batchNum;
	private String batchSuspDate;
	private String productRecallDesc;
	private Boolean index0Flag;
	private Boolean index1Flag;
	private Boolean inspectAllFlag;
	private Boolean randomInspectFlag;
	private Boolean rptFindingFlag;
	private String index1Date;
	private Boolean index2Flag;
	private Boolean batchSuspFlag;
	private Boolean productRecallFlag;
	private Boolean initLevelFlag;
	private Boolean patLevelFlag;
	private String faxFrom;
	private String orderType;
	private String caseNum;
	private String qaFax;
	
	
	public String getInitFaxCreateDate() {
		return initFaxCreateDate;
	}
	public void setInitFaxCreateDate(String initFaxCreateDate) {
		this.initFaxCreateDate = initFaxCreateDate;
	}
	public String getFaxTo() {
		return faxTo;
	}
	public void setFaxTo(String faxTo) {
		this.faxTo = faxTo;
	}
	public String getResponseRptDate() {
		return responseRptDate;
	}
	public void setResponseRptDate(String responseRptDate) {
		this.responseRptDate = responseRptDate;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	public String getManuf() {
		return manuf;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getBatchSuspDate() {
		return batchSuspDate;
	}
	public void setBatchSuspDate(String batchSuspDate) {
		this.batchSuspDate = batchSuspDate;
	}
	public String getProductRecallDesc() {
		return productRecallDesc;
	}
	public void setProductRecallDesc(String productRecallDesc) {
		this.productRecallDesc = productRecallDesc;
	}
	public Boolean getIndex0Flag() {
		return index0Flag;
	}
	public void setIndex0Flag(Boolean index0Flag) {
		this.index0Flag = index0Flag;
	}
	public Boolean getIndex1Flag() {
		return index1Flag;
	}
	public void setIndex1Flag(Boolean index1Flag) {
		this.index1Flag = index1Flag;
	}
	public Boolean getInspectAllFlag() {
		return inspectAllFlag;
	}
	public void setInspectAllFlag(Boolean inspectAllFlag) {
		this.inspectAllFlag = inspectAllFlag;
	}
	public Boolean getRandomInspectFlag() {
		return randomInspectFlag;
	}
	public void setRandomInspectFlag(Boolean randomInspectFlag) {
		this.randomInspectFlag = randomInspectFlag;
	}
	public Boolean getRptFindingFlag() {
		return rptFindingFlag;
	}
	public void setRptFindingFlag(Boolean rptFindingFlag) {
		this.rptFindingFlag = rptFindingFlag;
	}
	public String getIndex1Date() {
		return index1Date;
	}
	public void setIndex1Date(String index1Date) {
		this.index1Date = index1Date;
	}
	public Boolean getIndex2Flag() {
		return index2Flag;
	}
	public void setIndex2Flag(Boolean index2Flag) {
		this.index2Flag = index2Flag;
	}
	public Boolean getBatchSuspFlag() {
		return batchSuspFlag;
	}
	public void setBatchSuspFlag(Boolean batchSuspFlag) {
		this.batchSuspFlag = batchSuspFlag;
	}
	public Boolean getProductRecallFlag() {
		return productRecallFlag;
	}
	public void setProductRecallFlag(Boolean productRecallFlag) {
		this.productRecallFlag = productRecallFlag;
	}
	public Boolean getInitLevelFlag() {
		return initLevelFlag;
	}
	public void setInitLevelFlag(Boolean initLevelFlag) {
		this.initLevelFlag = initLevelFlag;
	}
	public Boolean getPatLevelFlag() {
		return patLevelFlag;
	}
	public void setPatLevelFlag(Boolean patLevelFlag) {
		this.patLevelFlag = patLevelFlag;
	}
	public String getFaxFrom() {
		return faxFrom;
	}
	public void setFaxFrom(String faxFrom) {
		this.faxFrom = faxFrom;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getQaFax() {
		return qaFax;
	}
	public void setQaFax(String qaFax) {
		this.qaFax = qaFax;
	}
}