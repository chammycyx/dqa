package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaItemManager")
@MeasureCalls
public class CoaItemManagerBean implements CoaItemManagerLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public CoaItem retrieveCoaItemByContract(Contract contract) {
		logger.debug("retrieveCoaItemByContract");
		List<CoaItem> coaItemList = (List<CoaItem>)em.createNamedQuery("CoaItem.findByContract")
		.setParameter("contract", contract)													 
		.getResultList();
		if( coaItemList.size() > 0 ) {
			return (CoaItem)coaItemList.get(0);
		}
		return null;
	}
}
