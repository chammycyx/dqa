package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.biz.CompanyServiceLocal;
import hk.org.ha.model.pms.dqa.biz.ContractServiceLocal;
import hk.org.ha.model.pms.dqa.biz.DqaMailSenderServiceLocal;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.biz.PharmCompanyServiceLocal;
import hk.org.ha.model.pms.dqa.biz.SupplierServiceLocal;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemCountry;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.HasFlag;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemRpt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemServiceBean implements PharmProblemServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private PharmProblem pharmProblem;
	
	@In(create =true)
	private CountryServiceLocal countryService;
	
	@In(create =true)
	private ContractServiceLocal contractService;
	
	@In(create =true)
	private SupplierServiceLocal supplierService;
	
	@In(create =true)
	private CompanyServiceLocal companyService;
	
	@In(create =true)
	private PharmCompanyServiceLocal pharmCompanyService;
	
	
	@In(create =true)
	private QaProblemServiceLocal qaProblemService;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	   
	@In
	private PharmProblemManagerLocal pharmProblemManager;
	
	private JRDataSource dataSource;

	private String suppPerfNotificationToEmail;
	
	public void setSuppPerfNotificationToEmail(String suppPerfNotificationToEmail) {
		this.suppPerfNotificationToEmail = suppPerfNotificationToEmail;
	}
	
	private boolean success;
	
	private String errorCode;

	private static final String EIGHT_ZERO = "00000000";
	private static final String CONFLICT_MSG = "The reporting staff has declared <font color='red'>conflict of interest</font> with "; 
	private static final String NO_CONFLICT_MSG = "The reporting staff has declared <font color='blue'>no conflict of interest</font> with the company/manufacturer/supplier under this problem report";
		
	public void addPharmProblem(){
		pharmProblem = pharmProblemManager.addPharmProblem();
	}
	
	public void createPharmProblem(PharmProblem pharmProblemIn, 
										List<String> pharmBatchNumList, 
										List<String> countryList, 
										List<PharmProblemNature> pharmProblemNatureList,
										List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
										Boolean sendEmailBoolean){
		
		
		pharmProblemManager.createPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList, sendEmailBoolean);
	}
	
	public void createUpdateDraftPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList){
		pharmProblemManager.createUpdateDraftPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList);
	}
	
	
	



	

	
	public void savePharmProblemCountry(List<PharmProblemCountry> pharmProblemCountrysSave)
	{
		if (pharmProblemCountrysSave!=null && pharmProblemCountrysSave.size()>0)
		{
			for (PharmProblemCountry pcc : pharmProblemCountrysSave)
			{
				em.persist(pcc);
				em.flush();
			}
		}
	}
	
	public String constructEmailContent(PharmProblem pharmProblemIn,
										List<PharmBatchNum> pharmBatchNumListIn, 
										List<PharmProblemCountry> pharmProblemCountryListIn, 
										List<PharmProblemNature> pharmProblemNatureListIn){
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DmDrug dmDrugFind = DmDrugCacher.instance().getDrugByItemCode(pharmProblemIn.getProblemHeader().getItemCode());
		
		
		StringBuffer emailContentSql = new StringBuffer();
		
		emailContentSql.append("<table>");
		emailContentSql.append("<tr><td><font size=4><b>");
		if (pharmProblemIn.getConflictFlag()) {
			emailContentSql.append(CONFLICT_MSG + pharmProblemIn.getConflictMspName());
		} else {
			emailContentSql.append(NO_CONFLICT_MSG);
		}
		emailContentSql.append(".</font></b></td></tr>");
		emailContentSql.append("<tr><td>&nbsp;</td></tr>");
		emailContentSql.append("</table>");
		
		emailContentSql.append("<table>");
		emailContentSql.append("<tr><td><font color='#E42217'><b>Institution</b></font></td><td>:</td><td><font color='#E42217'>"+pharmProblemIn.getInstitution().getInstitutionCode()+"</font></td></tr>");
		emailContentSql.append("<tr><td><b>Problem Number</b></td><td>:</td><td>"+pharmProblemIn.getProblemNum()+"</td></tr>");
		emailContentSql.append("<tr><td><font color='#F75D59'><b>Item Code</b></font></td><td>:</td><td><font color='#F75D59'>"+pharmProblemIn.getProblemHeader().getItemCode()+"</font></td></tr>");
		emailContentSql.append("<tr><td><font color='#F75D59'><b>Item Description</b></font></td><td>:</td><td><font color='#F75D59'>"+dmDrugFind.getFullDrugDesc()+"</font></td></tr>");
		emailContentSql.append("<tr><td><b>Order Type</b></td><td>:</td><td>"+pharmProblemIn.getProblemHeader().getOrderType().getDataValue()+"</td></tr>");
		
		if("C".equals(pharmProblemIn.getProblemHeader().getOrderType().getDataValue()) || 
			"Q".equals(pharmProblemIn.getProblemHeader().getOrderType().getDataValue())
			)
		{
			emailContentSql.append("<tr><td><b>Contract Number</b></td><td>:</td><td>"+pharmProblemIn.getContract().getContractNum()+(pharmProblemIn.getContract().getContractSuffix()==null||"".equals(pharmProblemIn.getContract().getContractSuffix().trim())?"":("-"+pharmProblemIn.getContract().getContractSuffix()))+"</td></tr>");
		}
		else
		{
			emailContentSql.append("<tr><td><b>Contract Number</b></td><td>:</td><td></td></tr>");
		}
		emailContentSql.append("<tr><td><b>Affected Quantity</b></td><td>:</td><td>"+pharmProblemIn.getAffectQty()+ " " + dmDrugFind.getBaseUnit()+ "</td></tr>");
		emailContentSql.append("<tr><td><b>Supplier</b></td><td>:</td><td>"+pharmProblemIn.getContract().getSupplier().getSupplierCode()+"</td></tr>");
		emailContentSql.append("<tr><td><b>Manufacturer</b></td><td>:</td><td>"+pharmProblemIn.getContract().getManufacturer().getCompanyCode()+"</td></tr>");
		emailContentSql.append("<tr><td><b>Pharmaceutical Company</b></td><td>:</td><td>"+pharmProblemIn.getContract().getPharmCompany().getCompanyCode()+"</td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td><font color='#151BD8'><b>Problem By</b></font></td><td>:</td><td><font color='#151BD8'>"+pharmProblemIn.getProblemByName()+"</font></td></tr>");
		emailContentSql.append("<tr><td><b>Rank</b></td><td>:</td><td>"+pharmProblemIn.getProblemByRank()+"</td></tr>");
		emailContentSql.append("<tr><td><b>Type</b></td><td>:</td><td>"+pharmProblemIn.getProblemByTypeDesc()+"</td></tr>");
		emailContentSql.append("<tr><td><b>Date</b></td><td>:</td><td>"+formatter.format(pharmProblemIn.getProblemDate())+"</td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td><font color='#151BD8'><b>Subject Officer</b></font></td><td>:</td><td><font color='#151BD8'>"+pharmProblemIn.getCoordinatorName()+"</font></td></tr>");
		emailContentSql.append("<tr><td><b>Rank</b></td><td>:</td><td>"+pharmProblemIn.getCoordinatorRank()+"</td></tr>");
		emailContentSql.append("<tr><td><b>Telephone</b></td><td>:</td><td>"+pharmProblemIn.getCoordinatorPhone()+"</td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		
		if(pharmBatchNumListIn==null || pharmBatchNumListIn.size()==0)
		{
			emailContentSql.append("<tr><td><b>Batch Number</b></td><td>:</td><td></td></tr>");
		}
		else
		{
			int i=0;
			for(PharmBatchNum pbn:pharmBatchNumListIn)
			{
				if(i==0)
				{
					emailContentSql.append("<tr><td><b>Batch Number</b></td><td>:</td><td>"+pbn.getBatchNum()+"</td></tr>");
				}
				else
				{
					emailContentSql.append("<tr><td></td><td></td><td>"+pbn.getBatchNum()+"</td></tr>");
				}
				i++;	
			}
		}
		
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		if(pharmProblemCountryListIn==null || pharmProblemCountryListIn.size()==0)
		{
			emailContentSql.append("<tr><td><b>Country</b></td><td>:</td><td></td></tr>");
		}
		else
		{
			int j=0;
			for(PharmProblemCountry pcc:pharmProblemCountryListIn)
			{
				if(j==0)
				{
					emailContentSql.append("<tr><td><b>Country</b></td><td>:</td><td>"+pcc.getCountry().getCountryCode()+"</td></tr>");
				}
				else
				{
					emailContentSql.append("<tr><td></td><td></td><td>"+pcc.getCountry().getCountryCode()+"</td></tr>");
				}
				j++;	
			}
		}
		
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		if(pharmProblemNatureListIn==null || pharmProblemNatureListIn.size()==0)
		{
			emailContentSql.append("<tr><td><b>Nature Of Problem</b></td><td>:</td><td></td></tr>");
		}
		else
		{
			int k=0;
			for(PharmProblemNature pcn:pharmProblemNatureListIn)
			{
				if(k==0)
				{
					emailContentSql.append("<tr><td><b>Nature of Problem</b></td><td>:</td><td><table><tr><td>Parameter</td><td>:</td><td>" + pcn.getProblemNatureSubCat().getProblemNatureCat().getProblemNatureParam().getParamDesc()+"</td></tr><tr><td>Category</td><td>:</td><td>" + pcn.getProblemNatureSubCat().getProblemNatureCat().getCatDesc()+"</td></tr><tr><td><font color='#F62217'>Sub-Category</font></td><td>:</td><td><font color='#F62217'>" +pcn.getProblemNatureSubCat().getSubCatDesc() +"</font></td></tr></table><br> </td></tr>");
				}
				else
				{
					emailContentSql.append("<tr><td></td><td></td><td><table><tr><td>Parameter</td><td>:</td><td>" + pcn.getProblemNatureSubCat().getProblemNatureCat().getProblemNatureParam().getParamDesc()+"</td></tr><tr><td>Category</td><td>:</td><td>" + pcn.getProblemNatureSubCat().getProblemNatureCat().getCatDesc()+"</td></tr><tr><td><font color='#F62217'>Sub-Category</font></td><td>:</td><td><font color='#F62217'>" +pcn.getProblemNatureSubCat().getSubCatDesc() +"</font></td></tr></table><br> </td></tr>");
				}
				k++;	
			}
		}
		emailContentSql.append("<tr><td><b>Description</b></td><td>:</td><td>"+pharmProblemIn.getProblemDetail()+"</td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td></td><td></td><td></td></tr>");
		emailContentSql.append("<tr><td><b>Sample Send To</b></td><td>:</td><td>"+(pharmProblemIn.getSendCollectSample()==null?"":(pharmProblemIn.getSendCollectSample().getSendToType()==null?"":(pharmProblemIn.getSendCollectSample().getSendToType().getDisplayValue())))+"</td></tr>");
		emailContentSql.append("<tr><td><b>Send Quantity</b></td><td>:</td><td>"+(pharmProblemIn.getSendCollectSample()==null?"":(pharmProblemIn.getSendCollectSample().getSampleQty()))+"</td></tr>");
		if(pharmProblemIn.getSendCollectSample()!=null)
		{
			if(pharmProblemIn.getSendCollectSample().getPharmSendDate()!=null)
			{
				emailContentSql.append("<tr><td><b>Send Date</b></td><td>:</td><td>"+formatter.format(pharmProblemIn.getSendCollectSample().getPharmSendDate())+"</td></tr>");
			}
		}
		emailContentSql.append("</table>");
		
		return emailContentSql.toString();
	}
	
	
	
	
	
	public Contract constructContractSave(PharmProblem pharmProblemIn){
		
		Contract contractFind;
		Contract contractSave = new Contract();
		Supplier supplierFind;
		Company manufacturerFind;
		Company pharmCompanyFind = new Company();
		boolean validationForDraftPharmProblem = true;
	
		if(pharmProblemIn.getProblemHeader().getOrderType()!=null &&
			(pharmProblemIn.getProblemHeader().getOrderType().equals(OrderTypeAll.Contract) ||
			 pharmProblemIn.getProblemHeader().getOrderType().equals(OrderTypeAll.StandingQuotation))
		   ){
			contractFind = contractService.findContractByItemContractNumContractSuffixContractTypeModuleType(
					pharmProblemIn.getProblemHeader().getItemCode(), 
					pharmProblemIn.getContract().getContractNum(),
					pharmProblemIn.getContract().getContractSuffix(),
					pharmProblemIn.getContract().getContractType(),
					ModuleType.All);
	
			if(pharmProblemIn.getProblemStatus() != null && 
					pharmProblemIn.getProblemStatus().equals(ProblemStatus.Drafted) ){
				validationForDraftPharmProblem = false;
			}
			
			if(validationForDraftPharmProblem){
				if(contractFind == null){
					success = false;
					errorCode = "0031";
					return null;
				}
				else{
					contractFind = contractService.getLazyContract(contractFind);
					if( !contractFind.getSupplier().getSupplierCode().equals(pharmProblemIn.getContract().getSupplier().getSupplierCode()) ||
						!contractFind.getManufacturer().getCompanyCode().equals(pharmProblemIn.getContract().getManufacturer().getCompanyCode()) ||
						!contractFind.getPharmCompany().getCompanyCode().equals(pharmProblemIn.getContract().getPharmCompany().getCompanyCode())
					   )
					{
						success = false;
						errorCode = "0117";
						return null;
					}
					else
					{
						contractSave = contractService.copyContract(contractFind, ModuleType.SuppPerf);
					}
				}
			}else{
				contractFind = contractService.getLazyContract(contractFind);
				contractSave = contractService.copyContract(contractFind, ModuleType.SuppPerf);
			}
		}
		else{
			supplierFind = supplierService.findSupplierBySupplierCode(pharmProblemIn.getContract().getSupplier().getSupplierCode());
			manufacturerFind = companyService.findManufacturerByCompanyCode(pharmProblemIn.getContract().getManufacturer().getCompanyCode());
			if(supplierFind == null){
				success = false;
				errorCode = "0086";
				return null;
			}
			else if(manufacturerFind==null){
				success = false;
				errorCode = "0087";
				return null;
			}
			else if(!("".equals(pharmProblemIn.getContract().getPharmCompany().getCompanyCode())) || pharmProblemIn.getContract().getPharmCompany().getCompanyCode()!=null){
				pharmCompanyFind = pharmCompanyService.findPharmCompanyByCompanyCode(pharmProblemIn.getContract().getPharmCompany().getCompanyCode());
				if(pharmCompanyFind == null){
					success = false;
					errorCode = "0088";
					return null;
				}
			}
			
			if(success){
				contractSave = pharmProblemIn.getContract();
				contractSave.setSupplier(supplierFind);
				contractSave.setManufacturer(manufacturerFind);
				contractSave.setPharmCompany(pharmCompanyFind);
				contractSave.setModuleType(ModuleType.SuppPerf);
				contractSave = contractService.getLazyContract(contractSave);
				
			}
				
		}
		success = true;
		return contractSave;
	}
	
	
	
	public List<PharmProblemCountry> constructPharmProblemCountrysSave(PharmProblem pharmProblemIn, List<String> countryList){
		success=false;
		Country countryFind;
		List<PharmProblemCountry> pharmProblemCountrys = new ArrayList<PharmProblemCountry>();
		PharmProblemCountry pharmProblemCountry;
		
		for (int i=0; i<countryList.size(); i++)
		{
			countryFind = countryService.findCountryByCountryCode(countryList.get(i));
			if (countryFind==null)
			{
				success = false;
				errorCode = "0089";
				return null;
			}
			else
			{
				pharmProblemCountry = new PharmProblemCountry();
				pharmProblemCountry.setCountry(countryFind);
				pharmProblemCountry.setPharmProblem(pharmProblemIn);
				pharmProblemCountrys.add(pharmProblemCountry);
			}
		}
		
		success=true;
		return pharmProblemCountrys;
	}
	
	public List<PharmBatchNum> constructPharmBatchNum(PharmProblem pharmProblemIn, List<String> pharmBatchNumList){
		success=false;
		List<PharmBatchNum> pharmBatchNums = new ArrayList<PharmBatchNum>();
		PharmBatchNum pharmBatchNum;
		
		for (int i=0; i<pharmBatchNumList.size(); i++)
		{
			
				pharmBatchNum = new PharmBatchNum();
				pharmBatchNum.setBatchNum(pharmBatchNumList.get(i));
				pharmBatchNum.setPharmProblem(pharmProblemIn);
				pharmBatchNums.add(pharmBatchNum);
		}
		
		success=true;
		return pharmBatchNums;
	}
	
	
	@SuppressWarnings("unchecked")
	public void retrievePharmProblemByPharmProblem(PharmProblem pharmProblemIn){
		logger.debug("retrievePharmProblemByPharmProblem");
		List<PharmProblem> pharmProblems ;
		
		if(pharmProblemIn.getProblemStatus() != null && pharmProblemIn.getProblemStatus().equals(ProblemStatus.Drafted)){
			pharmProblems = em.createNamedQuery("PharmProblem.findByPharmProblemId")
			   .setParameter("pharmProblemId", pharmProblemIn.getPharmProblemId())
			   .setParameter("recordStatus", RecordStatus.Active)
			   .setHint(QueryHints.FETCH, "o.institution")
			   .getResultList();
			
		}else{
			pharmProblems = em.createNamedQuery("PharmProblem.findByPharmProblemId")
			   .setParameter("pharmProblemId", pharmProblemIn.getPharmProblemId())
			   .setParameter("recordStatus", RecordStatus.Active)
			   .setHint(QueryHints.FETCH, "o.problemHeader")
				.setHint(QueryHints.FETCH, "o.institution")
				.setHint(QueryHints.FETCH, "o.contract")
				.setHint(QueryHints.FETCH, "o.contract.supplier")
				.setHint(QueryHints.FETCH, "o.contract.manufacturer")
				.setHint(QueryHints.FETCH, "o.contract.pharmCompany")
				.setHint(QueryHints.FETCH, "o.pharmBatchNumList")
				.setHint(QueryHints.BATCH, "o.pharmProblemCountryList")
				.setHint(QueryHints.FETCH, "o.pharmProblemNatureList")
				.setHint(QueryHints.BATCH, "o.sendCollectSample")
				.setHint(QueryHints.BATCH, "o.pharmProblemFileList")
				.setHint(QueryHints.BATCH, "o.qaProblem.qaProblemFileList")
			   .getResultList();
		}
		
		if(pharmProblems==null || pharmProblems.size()==0)
		{
			pharmProblem = null;
		}
		else
		{
			pharmProblem = pharmProblems.get(0);
			pharmProblem = getLazyPharmProblem(pharmProblem);
		}
		
	}
	
	public void deletePharmProblem(PharmProblem pharmProblemIn, String deleteReason){
		logger.debug("deletePharmProblem");
		
		PharmProblem pharmProblemInTemp;
		
		if(pharmProblemIn.getProblemStatus() != null && 
				pharmProblemIn.getProblemStatus().equals(ProblemStatus.Drafted)){
			
			pharmProblemInTemp = em.find(PharmProblem.class, pharmProblemIn.getPharmProblemId());
			pharmProblemInTemp.setDeleteReason(deleteReason);
			pharmProblemInTemp.setRecordStatus(RecordStatus.Deleted);	
			
		}else{
			pharmProblemInTemp = getLazyPharmProblem(pharmProblemIn);
			pharmProblemInTemp.setDeleteReason(deleteReason);
			pharmProblemInTemp.setRecordStatus(RecordStatus.Deleted);
				
				if(pharmProblemInTemp.getQaProblem()!=null){
					if(pharmProblemInTemp.getQaProblem().getCaseNum()!=null)
					{
						QaProblem qaProblemFind = qaProblemService.getQaProblemByCaseNum(pharmProblemInTemp.getQaProblem().getCaseNum());
						if(qaProblemFind!=null)
						{
							int noOfPharmProblem=0;
							for (PharmProblem pc : qaProblemFind.getPharmProblemList())
							{
								if(pc.getRecordStatus()==RecordStatus.Active && !pc.getPharmProblemId().equals(pharmProblemInTemp.getPharmProblemId()))
								{
									noOfPharmProblem++;
								}
							}
							
							if(noOfPharmProblem>1)
							{
								qaProblemFind.setSimilarProblem(HasFlag.Has);
							}
							else
							{
								qaProblemFind.setSimilarProblem(HasFlag.HasNot);
							}
							
							//save qaProblem
							qaProblemService.saveQaProblem(qaProblemFind);
						}
					}
					
				}
				em.merge(pharmProblemInTemp);
				em.flush();
					
		}
		
		//pharmProblem = pharmProblemInTemp;
		
	}
	
	public PharmProblem getLazyPharmProblem(PharmProblem pharmProblemLazy){
		// lazy
		pharmProblemLazy.getPharmProblemCountryList();
		pharmProblemLazy.getPharmProblemCountryList().size();
		pharmProblemLazy.getPharmBatchNumList();
		pharmProblemLazy.getPharmBatchNumList().size();
		pharmProblemLazy.getPharmProblemNatureList();
		pharmProblemLazy.getPharmProblemNatureList().size();
		pharmProblemLazy.getPharmProblemFileList();
		pharmProblemLazy.getPharmProblemFileList().size();
		pharmProblemLazy.getSendCollectSample();
		pharmProblemLazy.getInstitution();
		pharmProblemLazy.getProblemHeader();
		if (pharmProblemLazy.getQaProblem() != null && pharmProblemLazy.getQaProblem().getQaProblemFileList() != null) {
			pharmProblemLazy.getQaProblem().getQaProblemFileList().size();
		}
		pharmProblemLazy.getContract();
		
		if(pharmProblemLazy.getContract()!=null)
		{
			pharmProblemLazy.getContract().getSupplier();
			pharmProblemLazy.getContract().getManufacturer();
			pharmProblemLazy.getContract().getPharmCompany();
			
			if(pharmProblemLazy.getContract().getSupplier()!=null)
			{
				pharmProblemLazy.getContract().getSupplier().getContact();
			}
			if(pharmProblemLazy.getContract().getManufacturer()!=null)
			{
				pharmProblemLazy.getContract().getManufacturer().getContact();
			}
			if(pharmProblemLazy.getContract().getPharmCompany()!=null)
			{
				pharmProblemLazy.getContract().getPharmCompany().getContact();
			}
		}
		
		if(pharmProblemLazy.getSendCollectSample()!=null)
		{
			pharmProblemLazy.getSendCollectSample().getSendToQa();
			pharmProblemLazy.getSendCollectSample().getSendToSupp();
		}
		
		return pharmProblemLazy;
	}
	
		
	public void createPharmProblemRpt(PharmProblem pharmProblemIn)
	{
		logger.debug("createPharmProblemRpt");
		
		List<PharmProblemRpt> pharmProblemRptList = new ArrayList<PharmProblemRpt>();
		
		pharmProblemRptList.add(mapPharmProblemRpt(pharmProblemIn));
		
		dataSource = new JRBeanCollectionDataSource(pharmProblemRptList);
	}
	
	private PharmProblemRpt mapPharmProblemRpt(PharmProblem pharmProblemIn)
	{
		DmDrug dmDrugFind = DmDrugCacher.instance().getDrugByItemCode(pharmProblemIn.getProblemHeader().getItemCode());
		
		PharmProblemRpt rpt = new PharmProblemRpt();
		
		rpt.setProblemNum(pharmProblemIn.getProblemNum());
		rpt.setProblemStatus((pharmProblemIn.getProblemStatus())==null?"":(pharmProblemIn.getProblemStatus().getDisplayValue()));
		rpt.setCreateDate((DqaRptUtil.getDateStrDDMMMYYYY(pharmProblemIn.getCreateDate())));
		rpt.setProblemDate((DqaRptUtil.getDateStrDDMMMYYYY(pharmProblemIn.getProblemDate())));
		rpt.setCaseNum((pharmProblemIn.getQaProblem())==null?"":((pharmProblemIn.getQaProblem().getCaseNum())==null?"":(pharmProblemIn.getQaProblem().getCaseNum())));
		rpt.setInstitutionCode(pharmProblemIn.getInstitution().getInstitutionCode());
		rpt.setItemCode(pharmProblemIn.getProblemHeader().getDmDrug().getFullDrugDesc() + " (" + pharmProblemIn.getProblemHeader().getItemCode()  +")");
		rpt.setOrderType(pharmProblemIn.getProblemHeader().getOrderType().getDisplayValue());
		if("C".equals(pharmProblemIn.getProblemHeader().getOrderType().getDataValue()) ||
			"Q".equals(pharmProblemIn.getProblemHeader().getOrderType().getDataValue()))
		{
			rpt.setContractNum(pharmProblemIn.getContract().getContractNum() + ((pharmProblemIn.getContract().getContractSuffix())==null?"":("/"+pharmProblemIn.getContract().getContractSuffix())));
		}
		else
		{
			rpt.setContractNum("");
		}
		
		rpt.setAffectQty(pharmProblemIn.getAffectQty() + " " +dmDrugFind.getBaseUnit());
		rpt.setSupplierCode(pharmProblemIn.getContract().getSupplier().getSupplierCode());
		rpt.setManufCode(pharmProblemIn.getContract().getManufacturer().getCompanyCode());
		rpt.setPharmCompanyCode(pharmProblemIn.getContract().getPharmCompany().getCompanyCode());
		/////
		StringBuffer batchNumBuf = new StringBuffer();
		int bi = 0;
		for (PharmBatchNum batchNum:pharmProblemIn.getPharmBatchNumList())
		{
			batchNumBuf.append(batchNum.getBatchNum());
			if (bi != (pharmProblemIn.getPharmBatchNumList().size() -1)){
				batchNumBuf.append(", ");}
			
			bi++;
		}
		rpt.setBatchNum(batchNumBuf.toString());
		//////
		StringBuffer countryBuf = new StringBuffer();
		int ci = 0;
		for (PharmProblemCountry pcc:pharmProblemIn.getPharmProblemCountryList())
		{
			countryBuf.append(pcc.getCountry().getCountryCode());
			if (ci != (pharmProblemIn.getPharmProblemCountryList().size() -1)){
				countryBuf.append(", ");}
			
			ci++;
		}
		rpt.setCountry(countryBuf.toString());
		//////
		StringBuffer natureOfProblemBuf = new StringBuffer();
		int ni = 0;
		for (PharmProblemNature pcn:pharmProblemIn.getPharmProblemNatureList())
		{
			natureOfProblemBuf.append((ni+1)+". "+ pcn.getProblemNatureSubCat().getProblemNatureCat().getProblemNatureParam().getParamDesc()+" - " + pcn.getProblemNatureSubCat().getProblemNatureCat().getCatDesc()+" - " +pcn.getProblemNatureSubCat().getSubCatDesc());
			if (ni != (pharmProblemIn.getPharmProblemNatureList().size() -1)){
				natureOfProblemBuf.append("\n");}
			
			ni++;
		}
		rpt.setNatureOfProblem(natureOfProblemBuf.toString());
		
		rpt.setProblemDetail(pharmProblemIn.getProblemDetail());
		
		if(pharmProblemIn.getSendCollectSample()!=null)
		{
			rpt.setSendToType((pharmProblemIn.getSendCollectSample().getSendToType())==null?"":(pharmProblemIn.getSendCollectSample().getSendToType().getDisplayValue()));
			rpt.setSendToSuppCode((pharmProblemIn.getSendCollectSample().getSendToSupp())==null?"":(pharmProblemIn.getSendCollectSample().getSendToSupp().getSupplierCode()));
			rpt.setSuppCollectDate((pharmProblemIn.getSendCollectSample().getSuppCollectDate())==null?"":(DqaRptUtil.getDateStrDDMMMYYYY(pharmProblemIn.getSendCollectSample().getSuppCollectDate())));
			rpt.setSendToQaCode((pharmProblemIn.getSendCollectSample().getSendToQa())==null?"":((pharmProblemIn.getSendCollectSample().getSendToQa().getContact())==null?"":((pharmProblemIn.getSendCollectSample().getSendToQa().getContact().getFirstName()) + ((pharmProblemIn.getSendCollectSample().getSendToQa().getContact().getLastName())==null?"":(" "+pharmProblemIn.getSendCollectSample().getSendToQa().getContact().getLastName())))));
			rpt.setQaCollectDate((pharmProblemIn.getSendCollectSample().getQaCollectDate())==null?"":(DqaRptUtil.getDateStrDDMMMYYYY(pharmProblemIn.getSendCollectSample().getQaCollectDate())));
			rpt.setPharmSendMethod((pharmProblemIn.getSendCollectSample().getPharmSendMethod())==null?"":(pharmProblemIn.getSendCollectSample().getPharmSendMethod().getDisplayValue()));
			rpt.setSampleQty((pharmProblemIn.getSendCollectSample().getSampleQty())==null?"":(pharmProblemIn.getSendCollectSample().getSampleQty() + " " + dmDrugFind.getBaseUnit() ));
		}
		else
		{
			rpt.setSendToType("");
			rpt.setSendToSuppCode("");
			rpt.setSuppCollectDate("");
			rpt.setSendToQaCode("");
			rpt.setQaCollectDate("");
			rpt.setPharmSendMethod("");
			rpt.setSampleQty("");
		}
		
		rpt.setProblemByName(pharmProblemIn.getProblemByName());
		rpt.setProblemByRank(pharmProblemIn.getProblemByRank());
		rpt.setProblemByTypeDesc(pharmProblemIn.getProblemByTypeDesc());
		rpt.setCoordinatorName(pharmProblemIn.getCoordinatorName());
		rpt.setCoordinatorRank(pharmProblemIn.getCoordinatorRank().getDataValue());
		rpt.setCoordinatorPhone(pharmProblemIn.getCoordinatorPhone());
		
		
		return rpt;
	}
	
	public void generatePharmProblemRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/pharmProblemRpt.jasper", parameters, dataSource);
		
		reportProvider.redirectReport(contentId);
	}
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
		if (pharmProblem != null){
			pharmProblem = null;
		}
	}

	
}
