package hk.org.ha.model.pms.dqa.vo.coa;

import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaDiscrepancyCriteria {

	private CoaStatus coaStatus;
	
	private OrderType orderType;
	
	private String contractNum;
	
	private String supplierCode;
	
	private String itemCode;
	
	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public void setCoaStatus(CoaStatus coaStatus) {
		this.coaStatus = coaStatus;
	}

	public CoaStatus getCoaStatus() {
		return coaStatus;
	}
	
}
