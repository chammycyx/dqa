package hk.org.ha.model.pms.dqa.exception;

import org.granite.logging.Logger;
import org.granite.messaging.service.ServiceException;
import org.granite.messaging.service.ServiceInvocationContext;
import org.granite.messaging.service.security.SecurityServiceException;
import org.granite.tide.seam21.Seam21ServiceExceptionHandler;

public class DqaServiceExceptionHandler extends Seam21ServiceExceptionHandler {

	private static final Logger log = Logger.getLogger(DqaServiceExceptionHandler.class);
	
	private static final long serialVersionUID = 1L;
	
	public DqaServiceExceptionHandler() {
		super();
	}

	public DqaServiceExceptionHandler(boolean logException) {
		super(logException);
	}

	@Override
    public ServiceException handleInvocationException(ServiceInvocationContext context, Throwable e) {

		if (getLogException()) {
	    	if (e instanceof SecurityServiceException || 
	    			e instanceof ConcurrentUpdateException)
	            log.debug(e, "Could not process remoting message: %s", context.getMessage());
	        else
	            log.error(e, "Could not process remoting message: %s", context.getMessage());
    	}

        return getServiceException(context, e);
	}
	
}
