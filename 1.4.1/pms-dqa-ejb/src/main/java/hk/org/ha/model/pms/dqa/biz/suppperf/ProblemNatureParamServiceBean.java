package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureParamService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class ProblemNatureParamServiceBean implements ProblemNatureParamServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private ProblemNatureParam problemNatureParam;
	
	private boolean success;
	
	private String errorCode;

		
	public void addProblemNatureParam(){
		logger.debug("addProblemNatureParam");
		
		problemNatureParam = new ProblemNatureParam();
	}
	
	public void createProblemNatureParam(ProblemNatureParam problemNatureParamIn){
		logger.debug("createProblemNatureParam");
		success = false;
		
		List<ProblemNatureParam> problemNatureParams = em.createNamedQuery("ProblemNatureParam.findByParamDesc")
												.setParameter("paramDesc", problemNatureParamIn.getParamDesc())
												.setParameter("recordStatus", RecordStatus.Active)
												.getResultList();
		
		if(problemNatureParams==null || problemNatureParams.size()==0)
		{
			em.persist(problemNatureParamIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
		
	}
	
	public void updateProblemNatureParam(ProblemNatureParam problemNatureParamIn){
		logger.debug("updateProblemNatureParam");
		success = false;
		
		List<ProblemNatureParam> problemNatureParams = em.createNamedQuery("ProblemNatureParam.findByParamDesc")
												.setParameter("paramDesc", problemNatureParamIn.getParamDesc())
												.setParameter("recordStatus", RecordStatus.Active)
												.getResultList();
		
		if(problemNatureParams==null || problemNatureParams.size()==0)
		{
			em.merge(problemNatureParamIn);
			em.flush();
			
			success = true;
		}
		else if(problemNatureParams.get(0).getProblemNatureParamId().equals(problemNatureParamIn.getProblemNatureParamId()))
		{
			em.merge(problemNatureParamIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
	}
	
	public void deleteProblemNatureParam(ProblemNatureParam problemNatureParamIn){
		logger.debug("deleteProblemNatureParam");
		
		em.createNamedQuery("ProblemNatureParam.deleteByProblemNatureParamId")
										.setParameter("problemNatureParamId", problemNatureParamIn.getProblemNatureParamId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
		
		em.createNamedQuery("ProblemNatureCat.deleteByProblemNatureParamId")
										.setParameter("problemNatureParamId", problemNatureParamIn.getProblemNatureParamId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
		
		em.createNamedQuery("ProblemNatureSubCat.deleteByProblemNatureParamId")
										.setParameter("problemNatureParamId", problemNatureParamIn.getProblemNatureParamId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
	}
	
	public void retrieveProblemNatureParamByProblemNatureParamId(ProblemNatureParam problemNatureParamIn){
		logger.debug("retrieveProblemNatureParamByProblemNatureParamId");
		
		List<ProblemNatureParam> problemNatureParams = em.createNamedQuery("ProblemNatureParam.findByProblemNatureParamId")
											.setParameter("problemNatureParamId", problemNatureParamIn.getProblemNatureParamId())
											.setParameter("recordStatus", RecordStatus.Active)
											.getResultList();

		if(problemNatureParams==null || problemNatureParams.size()==0)
		{
			problemNatureParam = null;
		}
		else
		{
			problemNatureParam = problemNatureParams.get(0);
		}
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
		
	@Remove
	public void destroy(){
		if (problemNatureParam != null){
			problemNatureParam = null;
		}
	}
}
