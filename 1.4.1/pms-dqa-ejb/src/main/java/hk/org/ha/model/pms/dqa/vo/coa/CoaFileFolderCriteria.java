package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.dqa.udt.OrderType;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaFileFolderCriteria {
	
	private String itemCode;
	private String supplierCode;
	private FileCategory fileCat;
	private OrderType orderType;
	private String contractNum;
	private String batchNum;
	private Date startDate;
	private Date endDate;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public FileCategory getFileCat() {
		return fileCat;
	}
	public void setFileCat(FileCategory fileCat) {
		this.fileCat = fileCat;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()) : null;
	}

	public void setStartDate(Date startDate) {
		if (startDate != null) {
			this.startDate = new Date(startDate.getTime());
		}
	}
	
	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}

	public void setEndDate(Date endDate) {
		if (endDate != null) {
			this.endDate = new Date(endDate.getTime());
		}
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}
}
