package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaBatchManager")
@MeasureCalls
public class CoaBatchManagerBean implements CoaBatchManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private StatusMessages statusMessages;
	
	public CoaBatchResult createCoaBatchForEmail( CoaBatch coaBatchIn ) {
		logger.debug("createCoaBatchForEmail #0", coaBatchIn);				
		return createCoaBatch( coaBatchIn );
	}
	
	public CoaBatchResult createCoaBatch(CoaBatch coaBatchIn)
	{
		logger.debug("createCoaBatch");
		
		CoaBatch coaBatch = retrieveCoaBatchByBatchNumCoaItemId(coaBatchIn.getBatchNum(),coaBatchIn.getCoaItem().getCoaItemId() );

		if (coaBatch == null){	
			CoaBatchVer newCoaBatchVer = new CoaBatchVer();
			coaBatchIn.setInterfaceFlag( InterfaceFlag.No );			
			newCoaBatchVer.setReminderNum(0);
			newCoaBatchVer.setRecordStatus(RecordStatus.Active);
			newCoaBatchVer.setDiscrepancyStatus(DiscrepancyStatus.NullValue);
			newCoaBatchVer.setCoaStatus(CoaStatus.VerificationInProgress);
			newCoaBatchVer.setCoaBatch(coaBatchIn);
			coaBatchIn.setCoaBatchVer(newCoaBatchVer);
			em.persist (coaBatchIn);
			em.flush();
			return new CoaBatchResult(coaBatchIn, false);
		} else {
			logger.debug("Duplicated creation of CoaBatch #0", coaBatch.getBatchNum());
			statusMessages.addToControl("batchNum", "Cannot add batch. A batch no. you specified already exists.");
			return new CoaBatchResult(coaBatch, true);
		}
	}
	
	@SuppressWarnings("unchecked")
	public CoaBatch retrieveCoaBatchByBatchNumCoaItemId(String batchNum, Long coaItemId){
		logger.debug("retrieveCoaBatchByBatchNumCoaItemId #0 #1", batchNum, coaItemId);

		List existingCoaBatchList = em.createNamedQuery("CoaBatch.findByBatchNumCoaItemId")
			.setParameter("batchNum", batchNum)
			.setParameter("coaItemId", coaItemId)
			.setHint(QueryHints.FETCH, "o.coaItem")
			.setHint(QueryHints.FETCH, "o.coaItem.contract")
			.setHint(QueryHints.FETCH, "o.coaItem.contract.supplier")
			.getResultList();

		if (existingCoaBatchList.size() >0){
			return (CoaBatch)existingCoaBatchList.get(0);
		}else  {
			return null;
		}	
	}
	
	public void updateCoaBatchForEmailLog( CoaBatch coaBatchIn ) {
		em.merge( coaBatchIn.getCoaBatchVer() );
		em.flush();
	}
	
	public static class CoaBatchResult
	{		
		private CoaBatch coaBatch;
		
		private boolean duplicate;
		
		public CoaBatchResult(CoaBatch coaBatchIn, boolean duplicateIn)
		{
			this.coaBatch = coaBatchIn;
			this.duplicate = duplicateIn;
		}

		public CoaBatch getCoaBatch() {
			return coaBatch;
		}

		public boolean isDuplicate() {
			return duplicate;
		}
	}
}
