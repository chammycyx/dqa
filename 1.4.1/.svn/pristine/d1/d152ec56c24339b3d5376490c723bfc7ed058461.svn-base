package hk.org.ha.model.pms.dqa.util;

import com.enterprisedt.net.ftp.FTPClient;

import com.enterprisedt.net.ftp.FTPConnectMode;

import com.enterprisedt.net.ftp.FTPFile;
import com.enterprisedt.net.ftp.FTPMessageListener;
import com.enterprisedt.net.ftp.FTPTransferType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FTPHelper implements FTPMessageListener {

    private static final int RETRYCOUNT = 5;
    private static final int RETRYDELAY = 5000; //(ms)
    
	public static final String CONF_DIR = "conf.dir";
	public static final String CONFIG_PROPERTIES = "/config.properties";
    public static final String ERP_FTP_HOST_PROPERTIES = "/erp-ftp-host.properties";
    
    private static Log logger = LogFactory.getLog(FTPHelper.class);

    private FTPClient ftp = new FTPClient();
    
    private Properties profile = null;

    private String host = null;
    private String username = null;
    private String password = null;
    private String port = null;
    private String timeout = null;
    private String rootDir = null;
    private String transferType = null;
    private String connectMode = null;
    
    private boolean logCommand = false;
    private boolean logReply = false;

    private int retryCount = RETRYCOUNT;
    private int retryDelay = RETRYDELAY;
    
    public FTPHelper(String connectType, String configDirSubfolder) throws IOException {
    	
    	String configDir = System.getProperty(CONF_DIR);
    	FileInputStream fis = null;
    	if (configDir != null && configDir.length() > 0) {
	    	 		    			    			    		
	    		logger.debug( configDir + "/" + configDirSubfolder + ERP_FTP_HOST_PROPERTIES );	    		
	    		
	    		profile = new Properties();
	    		fis = new FileInputStream(configDir + "/" + configDirSubfolder + ERP_FTP_HOST_PROPERTIES);
				profile.load(fis);
				
	    		logCommand = Boolean.parseBoolean(profile.getProperty("logCommand"));
	            logReply = Boolean.parseBoolean(profile.getProperty("logReply"));
	            
	            if (!StringUtils.isBlank(profile.getProperty("retryCount"))) {
	                retryCount = Integer.parseInt(profile.getProperty("retryCount"));
	            }
	            
	            if (!StringUtils.isBlank(profile.getProperty("retryDelay"))) {
	                retryDelay = Integer.parseInt(profile.getProperty("retryDelay"));
	            }
	            
	            ftp.setMessageListener(this);  
	      
	        if (!StringUtils.isBlank(connectType)) {            
	            host = profile.getProperty(connectType +".host");
	            port = profile.getProperty(connectType+".port");
	            username = profile.getProperty(connectType+".username");
	            password = profile.getProperty(connectType+".password");
	            rootDir = profile.getProperty(connectType+".rootDir");
	            connectMode = profile.getProperty(connectType+".connectMode");
	            transferType = profile.getProperty(connectType+".transferType");
	            timeout = profile.getProperty(connectType+".timeout");
	
	            connect();                            
	        }
    	} else {
    		logger.debug("can not access conf.dir : " + configDir);
    	}
    }
    
    public void connect() {
        int count = 0;
        long sleepTime = 0;
        boolean isSuccess = false;

        while (!isSuccess && count++ < retryCount) {     
        
            try {
                if (NumberUtils.isDigits(port) && (Integer.parseInt(port) > 0)) {
                    ftp.setRemotePort(Integer.parseInt(port));
                }
                
                if (NumberUtils.isDigits(timeout)) {
                    ftp.setTimeout(Integer.parseInt(timeout));
                }
                                
                ftp.setRetryCount(retryCount);
                ftp.setRetryDelay(retryDelay);
                ftp.setRemoteHost(host);
                ftp.connect();
                ftp.login(username, password);
                
                if (!StringUtils.isBlank(connectMode)) {
                    if (connectMode.equalsIgnoreCase("passive") || connectMode.equalsIgnoreCase("p")){
                        ftp.setConnectMode(FTPConnectMode.PASV);
                    }else{
                        ftp.setConnectMode(FTPConnectMode.ACTIVE);
                    }
                } else {
                    ftp.setConnectMode(FTPConnectMode.ACTIVE);
                }
    
                if (!StringUtils.isBlank(transferType)) {
                    if (transferType.equalsIgnoreCase("ascii") || transferType.equalsIgnoreCase("a")){
                        ftp.setType(FTPTransferType.ASCII);
                    }else{
                        ftp.setType(FTPTransferType.BINARY);
                    }
                } else {
                    ftp.setType(FTPTransferType.BINARY);
                }
                                        
                if (!StringUtils.isBlank(rootDir)) {
                    String path = this.pwd() + "/" + rootDir;
                    this.changeDir(path);
                }
                            
                isSuccess = true;
            } catch (Exception e) {            
                if (count == retryCount) {
                    throw new RuntimeException(e);
                }
                
                sleepTime = count * retryDelay;
                logger.warn("Cannot connect to " + username + "@" + host + ", sleep " + sleepTime + "(ms) and retry the connection");
                
                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e2) {}                
            }
        }
    }

    public void download(String localPath, String remotePath) {
        if (!ftp.connected()) {
            connect();
        }
        
        try {
            ftp.get(localPath, remotePath);    
        } catch ( Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void upload(String localPath, String remotePath) {    
        if (!ftp.connected()) {
            connect();
        }
        
        try {
            ftp.put(localPath, remotePath);
        } catch ( Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
	public void upload(List fileList) {   
        for (int i=0; i<fileList.size(); i++) {
            upload(((File)fileList.get(i)).getAbsolutePath(), ((File)fileList.get(i)).getName());
        }
    }
    
    public void quote(String code ) {
        if (!ftp.connected()) {
            connect();
        }
    
        try {
            ftp.quote(code);
        } catch ( Exception e ) {
            throw new RuntimeException(e);
        }
    }
    
    public void rename(String fromName, String toName) {
        if (!ftp.connected()) {
            connect();
        }
        
        try {
            ftp.rename(fromName, toName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void delete(String remoteFile) {
        if (!ftp.connected()) {
            connect();
        }
        
        try {
            ftp.delete(remoteFile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void changeDir(String remoteDir) {
        if (!ftp.connected()) {
            connect();
        }
        
        try {
            ftp.chdir(remoteDir);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public String pwd() {
        if (!ftp.connected()) {
            connect();
        }
        try {
        	String pwd = ftp.pwd().replace("\\", "/");
            // Windows ftp server will return "is current directory." after the root location
            return pwd.replace(" is current directory.", "");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public boolean fileExists(String remoteFile) {
        if (!ftp.connected()) {
            connect();
        }
        String path = "";
        String currentPwd = "";
        String filename = remoteFile;
        String files[];
        
        try {
            currentPwd = ftp.pwd();
            
            Integer pathIndex = remoteFile.lastIndexOf('/'); 
            if (pathIndex >= 0) {
                path = remoteFile.substring(0, pathIndex);
                if (remoteFile.length() >= pathIndex + 1) {
                    filename = remoteFile.substring(pathIndex+1);
                }
                ftp.chdir(this.pwd() + "/" + path);
            }
                    
            files = ftp.dir();
                                               
            for (int i=0; i<files.length; i++) {                           
                if (filename.equals(files[i])) {
                    return true;
               }
            }
            return false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                ftp.chdir(currentPwd);
            } catch (Exception e) {
            	logger.error("error :", e);
            }
        }
    }
    
    
    public void quit() {
        try {
            ftp.quit();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @SuppressWarnings("unchecked")
	public Map dirDetails(String dir) {
        if (!ftp.connected()) {
            connect();
        }
        
	    HashMap<String, Map> dirMap = new HashMap<String, Map>();
		try {
			FTPFile[] ftpFiles = ftp.dirDetails(dir);
			
	        for (FTPFile ftpFile : ftpFiles) {
	        	HashMap<String, Object> fileMap = new HashMap<String, Object>();
	        	fileMap.put("lastModifiedDate", ftpFile.lastModified());
	        	fileMap.put("size", ftpFile.size());
	        	fileMap.put("isDir", ftpFile.isDir());
	        	fileMap.put("isFile", ftpFile.isFile());
	        	fileMap.put("isLink", ftpFile.isLink());
	
	        	dirMap.put(ftpFile.getName(), fileMap); 
	        }
	        
	        return dirMap;
		} catch (Exception e) {
           throw new RuntimeException(e);
        }
    }
        
    public void setHost(String host) {
        this.host = host;
    }
    
    public void setPort(String port) {
        this.port = port;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }
    
    public void setConnectMode(String connectMode) {        
        this.connectMode = connectMode;
    }
    
    public void setTransferType(String transferType) {    
        this.transferType = transferType;
    }
    
    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public void logCommand(String string) {
        if (logCommand) {
            logger.info("FTP Command : " + string);
        }
    }

    public void logReply(String string) {
        if (logReply) {
            logger.info("FTP Reply : " + string);
        }
    }
}
