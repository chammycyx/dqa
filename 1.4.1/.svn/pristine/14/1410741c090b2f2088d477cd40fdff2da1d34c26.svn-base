<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("conflictOfInterestPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		<s:RadioButtonGroup id="yesNoOption" itemClick="yesNoOptionItemClickHandler(event)"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[		
			import mx.events.ItemClickEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import hk.org.ha.event.pms.dqa.delaydelivery.GenerateDelayDeliveryRptFormConflictOfInterestRptEvent;
			import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormConflictOfInterestEvent;
			import hk.org.ha.event.pms.dqa.delaydelivery.popup.ShowDelayDeliveryRptFormDataPopupFileUploadPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.security.UamInfo;
			import hk.org.ha.model.pms.dqa.persistence.UserInfo;
			import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
			import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormFileUploadData;
			import hk.org.ha.model.pms.dqa.vo.suppperf.ConflictOfInterestRpt;
			import hk.org.ha.model.pms.vo.letter.LetterContent;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			[Embed(source="/assets/file_upload_complete.png")]
			public static const fileUploadComplete:Class;
			
			private var conflictOfInterest:LetterContent;
			[Bindable]
			private var reportName:String;
			[Bindable]
			private var conflictOfInterestContent:String = "";
			
			private var callBackFunc:Function;
			
			[Bindable]
			private var delayDeliveryRptFormData:DelayDeliveryRptFormData;
			
			[Bindable]
			private var delayDeliveryRptFormFileUploadData:DelayDeliveryRptFormFileUploadData = null;
			
			[In][Bindable]
			public var userInfo:UserInfo;
			
			[In]
			public var uamInfo:UamInfo;
			
			[In][Bindable]
			public var sysMsgMap:SysMsgMap;
			
			private var msgProp:SystemMessagePopupProp;
			
			
			public function init(delayDeliveryRptFormDataIn:DelayDeliveryRptFormData, callBackFuncIn:Function):void {
				callBackFunc = callBackFuncIn;
				delayDeliveryRptFormData = delayDeliveryRptFormDataIn;
				delayDeliveryRptFormData.conflictDate = new Date;
				
				if (userInfo) {
					delayDeliveryRptFormData.conflictByName = userInfo.singleLevelContactFirstName + " " + userInfo.singleLevelContactLastName
				} else {
					delayDeliveryRptFormData.conflictByName = uamInfo.userId;
				}
				dispatchEvent(new RetrieveDelayDeliveryRptFormConflictOfInterestEvent(
					function(letterContentIn:LetterContent):void {
						reportName = letterContentIn.subjectInfo;
						for each(var paragraph:String in letterContentIn.paragraphList) {
							conflictOfInterestContent += paragraph + "\n\n";
						}
					}
				));
			}
			
			public function submitHandler():void 
			{
				if (yesNoOption.selection == noOption || validateInput()) {
					callBackFunc(delayDeliveryRptFormData, delayDeliveryRptFormFileUploadData, true);
					closeWin();
				}
			}
			
			public function closeWin():void
			{
				PopUpManager.removePopUp(this);
			}
			
			protected function yesNoOptionItemClickHandler(event:ItemClickEvent):void
			{
				delayDeliveryRptFormData.conflictFlag = yesNoOption.selection == yesOption;
			}
			
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			
			private function printConflictOfInterestRpt():void 
			{
				if (yesNoOption.selection == noOption || validateInput()) {
					var conflictOfInterestRpt:ConflictOfInterestRpt = new ConflictOfInterestRpt;
					
					showMessage("0168");
					
					conflictOfInterestRpt.reportName = reportName;
					conflictOfInterestRpt.reportContent = conflictOfInterestContent;
					
					conflictOfInterestRpt.conflictByName = delayDeliveryRptFormData.conflictByName?delayDeliveryRptFormData.conflictByName:"";
					conflictOfInterestRpt.conflictByRank = delayDeliveryRptFormData.conflictByRank?delayDeliveryRptFormData.conflictByRank:"";
					conflictOfInterestRpt.conflictDate = dateFormatter.format(delayDeliveryRptFormData.conflictDate);
					conflictOfInterestRpt.conflictFlag = delayDeliveryRptFormData.conflictFlag?"Y":"N";
					conflictOfInterestRpt.conflictMspName = delayDeliveryRptFormData.conflictMspName?delayDeliveryRptFormData.conflictMspName:"";
					conflictOfInterestRpt.conflictRelationship = delayDeliveryRptFormData.conflictRelationship?delayDeliveryRptFormData.conflictRelationship:"";
					conflictOfInterestRpt.docNum = "";
					dispatchEvent(new GenerateDelayDeliveryRptFormConflictOfInterestRptEvent(conflictOfInterestRpt));
				}
			}
			
			private function fileUpload():void {
				dispatchEvent(new ShowDelayDeliveryRptFormDataPopupFileUploadPopupEvent(
					delayDeliveryRptFormData, 
					function(delayDeliveryRptFormFileUploadDataIn:DelayDeliveryRptFormFileUploadData):void {
						delayDeliveryRptFormFileUploadData = delayDeliveryRptFormFileUploadDataIn;
					}
				));
			}
			
			private function validateInput():Boolean {
				if (delayDeliveryRptFormData.conflictMspName == null || StringUtil.trim(delayDeliveryRptFormData.conflictMspName).length == 0) {
					mspTextArea.errorString = sysMsgMap.getMessage("0009");
					mspTextArea.setFocus();
					return false;
				}
				if (delayDeliveryRptFormData.conflictRelationship == null || StringUtil.trim(delayDeliveryRptFormData.conflictRelationship).length == 0) {
					relationshipTextArea.errorString = sysMsgMap.getMessage("0009");
					relationshipTextArea.setFocus();
					return false;
				}
				if (delayDeliveryRptFormData.conflictByName == null || StringUtil.trim(delayDeliveryRptFormData.conflictByName).length == 0) {
					nameTextInput.errorString = sysMsgMap.getMessage("0009");
					nameTextInput.setFocus();
					return false;
				}
				if (delayDeliveryRptFormData.conflictByRank == null || StringUtil.trim(delayDeliveryRptFormData.conflictByRank).length == 0) {
					rankTextInput.errorString = sysMsgMap.getMessage("0009");
					rankTextInput.setFocus();
					return false;
				}
				return true;
			}
			
			private function clearErrorString():void {
				nameTextInput.errorString = null;
				rankTextInput.errorString = null;
				mspTextArea.errorString = null;
				relationshipTextArea.errorString = null;
			}
			
			protected function mspTextAreaFocusOut():void
			{
				if (delayDeliveryRptFormData.conflictMspName) {
					delayDeliveryRptFormData.conflictMspName = delayDeliveryRptFormData.conflictMspName.replace(/\n|\r/ig, " ");
				}
			}
			
			protected function relationshipTextAreaFocusOut():void
			{
				if (delayDeliveryRptFormData.conflictRelationship) {
					delayDeliveryRptFormData.conflictRelationship = delayDeliveryRptFormData.conflictRelationship.replace(/\n|\r/ig, " ");
				}
			}
			
			protected function yesOptionClickHandler(event:MouseEvent):void
			{
				if (userInfo) {
					delayDeliveryRptFormData.conflictByName = userInfo.singleLevelContactFirstName + " " + userInfo.singleLevelContactLastName
				} else {
					delayDeliveryRptFormData.conflictByName = uamInfo.userId;
				}
			}
			
			protected function noOptionClickHandler(event:MouseEvent):void
			{
				clearErrorString();
				delayDeliveryRptFormData.conflictMspName = null;
				delayDeliveryRptFormData.conflictRelationship = null;
				delayDeliveryRptFormData.conflictByRank = null;
			}
		]]>
	</fx:Script>
	
	<s:Panel title="Declaration of Conflict of Interest" width="548" height="615">
		<s:layout>
			<s:VerticalLayout gap="5" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
		</s:layout>
		<s:BorderContainer width="520" height="230" borderVisible="true">
			<s:Scroller width="100%" height="100%" verticalScrollPolicy="on">
				<s:VGroup id="declarationGroup" gap="0" paddingTop="5" paddingRight="5" paddingLeft="5" paddingBottom="0">
					<s:RichEditableText editable="false" 
										width="100%" 
										height="50" 
										text="{reportName}" 
										textAlign="center" 
										textDecoration="underline" 
										fontSize="15" 
										fontWeight="bold"/>
					
					<s:RichEditableText id="declarationContentRichText" 
										editable="false"										
										width="100%" 
										height="1000" 
										text="{conflictOfInterestContent}"/>
					
				</s:VGroup>
			</s:Scroller>
		</s:BorderContainer>
		
		<s:RadioButton id="yesOption" 
					   groupName="yesNoOption" 
					   content="Yes (If yes, please specify as below.)" 
					   click="yesOptionClickHandler(event)"
					   fontWeight="bold"/>
		<s:RadioButton id="noOption" 
					   groupName="yesNoOption" 
					   content="No" 
					   click="noOptionClickHandler(event)"
					   fontWeight="bold"/>
		
		<mx:Spacer width="1" height="5"/>
		
		<s:VGroup width="100%" gap="0">
			<s:Label text="Name of the Company / Manufacturer / Supplier" width="100%" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedTextArea id="mspTextArea" 
								 text="@{delayDeliveryRptFormData.conflictMspName}"
								 focusOut="clearErrorString();mspTextAreaFocusOut()"
								 editable="{yesNoOption.selection == yesOption}" enabled="{yesNoOption.selection == yesOption}" 
								 width="100%" height="50" restrict="\u0020-\u007E" maxChars="150"/>	
		</s:VGroup>
		
		<s:VGroup width="100%" gap="0">
			<s:Label text="Relationship" width="100%" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedTextArea id="relationshipTextArea"   
								 text="@{delayDeliveryRptFormData.conflictRelationship}" 
								 focusOut="clearErrorString();relationshipTextAreaFocusOut()"
								 editable="{yesNoOption.selection == yesOption}" enabled="{yesNoOption.selection == yesOption}" 
								 width="100%" height="50" restrict="\u0020-\u007E" maxChars="150"/>
		</s:VGroup>
		
		<mx:Spacer width="1" height="5"/>
		
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Name" width="40" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedTextInput id="nameTextInput"   
								  text="@{delayDeliveryRptFormData.conflictByName}" 
								  focusOut="clearErrorString()"
								  enabled="{yesNoOption.selection == yesOption}" width="100%" restrict="\u0020-\u007E" maxChars="150"/>
		</s:HGroup>
		
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Rank" width="40" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedTextInput id="rankTextInput"
								  text="@{delayDeliveryRptFormData.conflictByRank}" 
								  focusOut="clearErrorString()"
								  enabled="{yesNoOption.selection == yesOption}" width="100%" restrict="\u0020-\u007E" maxChars="50"/>
		</s:HGroup>
		
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Date" width="40" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedAbbDateField selectedDate="@{delayDeliveryRptFormData.conflictDate}" enabled="false" width="120"/>
		</s:HGroup>
		
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Declaration of conflict of interest form" textAlign="left" fontWeight="bold"/>
			<fc:ExtendedButton label="Print" enabled="{yesNoOption.selection == yesOption}" click="printConflictOfInterestRpt()"/>
			<fc:ExtendedButton label="Upload" enabled="{yesNoOption.selection == yesOption}" click="fileUpload()"/>
			<mx:Image source="{fileUploadComplete}" visible="{delayDeliveryRptFormFileUploadData != null}"/>
		</s:HGroup>	
		
		<s:HGroup width="100%" height="100%" horizontalAlign="right" verticalAlign="bottom" paddingBottom="0" paddingLeft="0" paddingRight="0" paddingTop="0">
			<s:Button label="Submit" click="submitHandler()" enabled="{yesNoOption.selection == yesOption &amp;&amp; delayDeliveryRptFormFileUploadData != null || yesNoOption.selection == noOption}"/>
			<s:Button label="Cancel" click="closeWin()"/>
		</s:HGroup>
		
	</s:Panel>
	
</fc:ExtendedNavigatorContent>