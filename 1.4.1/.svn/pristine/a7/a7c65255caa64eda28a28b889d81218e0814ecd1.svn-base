package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.io.File;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FileItemManagerLocal;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemFile;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemFileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemFileServiceBean implements PharmProblemFileServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private FileItemManagerLocal fileItemManager;
	
	private static final String FILE_PATH_PREFIX ="SUPPPERF";
	private static final String PATH_DELIMITER ="/";
		
	private boolean success;
	
	private String errorCode;
	
	private String uploadPath;

	public PharmProblemFile uploadFile(PharmProblemFileUploadData pharmProblemFileUploadDataIn, PharmProblem pharmProblemIn){
		logger.debug("uploadFile");
		
		PharmProblemFile pharmProblemFileSave = new PharmProblemFile();
		
		FileItem fileItemIn = pharmProblemFileUploadDataIn.getFileItem();
		String fileStrParam = pharmProblemFileUploadDataIn.getRefUploadFileName();
		byte[] bytes = pharmProblemFileUploadDataIn.getFileData();
		
		pharmProblemFileSave.setFileItem(fileItemIn);
		pharmProblemFileSave.setPharmProblem(pharmProblemIn);
				
		String fileStrIn = fileStrParam.trim();
		logger.debug("uploadFile : #0 ", fileStrIn);
		success = false;
		errorCode ="0019";
		
		if (fileStrIn!=null && bytes !=null){

			String ext = (fileStrIn.lastIndexOf('.')==-1)?"":fileStrIn.substring(fileStrIn.lastIndexOf('.')+1,fileStrIn.length());
			String dbfileName = formatFileName(fileItemIn);
			String dbFilePath = formatFilePath(pharmProblemFileUploadDataIn);

			String diskFilePath = uploadPath + PATH_DELIMITER + dbFilePath ;

			pharmProblemFileSave.getFileItem().setFileName(dbfileName);
			
			
			File fDir = new File(diskFilePath);

			if (!fDir.exists()){
				boolean dirCreated = fDir.mkdirs();
				logger.debug("new folder : #0 , #1",dirCreated, fDir.getPath());
			}

			File f = new File(diskFilePath, dbfileName+"."+ext);
			
			logger.debug("file path : "+ f.getPath());
			int fileNum = 1;
			String newFileName ="";
			if (fDir.exists()){
				while(f.exists()){
					newFileName = dbfileName+"("+ fileNum +")."+ ext;

					f = new File(diskFilePath, newFileName);
					fileNum++;
				}
				
				if (fileItemManager.uploadFile(f, bytes)){
					logger.info("File uploaded : #0", f.getPath());
					
					pharmProblemFileSave.getFileItem().setFileName(dbfileName);
					pharmProblemFileSave.getFileItem().setFilePath(dbFilePath + PATH_DELIMITER+f.getName());
					
					try {
						createPharmProblemFile(pharmProblemFileSave);
						success = true;
						errorCode = null;
						
					}finally{
						if (!success){
							if (pharmProblemFileSave!=null){
								pharmProblemFileSave.getFileItem().setFileName(fileStrIn.substring(0, fileStrIn.indexOf('.')-1));
								pharmProblemFileSave.getFileItem().setFilePath(fileStrIn);
							}
							if (f.exists()){
								fileItemManager.deleteFile(f);
								logger.info("Process failure , File deleted : #0", f.getPath());
							}
						}
					}
				}
			}
		}
		
		return pharmProblemFileSave;
	}
	
	private String formatFileName(FileItem fileItemIn){
		
		String dbfileName=fileItemIn.getFileName();
	
		return dbfileName;
	}
	
	
	private String formatFilePath(PharmProblemFileUploadData pharmProblemFileUploadDataIn){
		
		String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + pharmProblemFileUploadDataIn.getItemCode()
		+ PATH_DELIMITER + pharmProblemFileUploadDataIn.getSupplierCode() 
		+ PATH_DELIMITER + pharmProblemFileUploadDataIn.getManufCode();
		
		return dbFilePath;
	}
	
	
		
	public void createPharmProblemFile(PharmProblemFile pharmProblemFileIn){
		logger.debug("createPharmProblemFile");
		
		em.persist(pharmProblemFileIn.getFileItem());
		em.flush();
		
		em.persist(pharmProblemFileIn);
		em.flush();
	}
	
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
	}
}
