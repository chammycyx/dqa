package hk.org.ha.model.pms.dqa.persistence;

import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "FILE_ITEM")

public class FileItem extends VersionEntity {

	private static final long serialVersionUID = -8235406508112949818L;
	
	@Id
	@Column(name="FILE_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fileItemSeq")
	@SequenceGenerator(name="fileItemSeq", sequenceName="SEQ_FILE_ITEM", initialValue=10000)
    private Long fileId;
	
	@Column(name="FILE_NAME", length = 200, nullable = false)
    private String fileName;
	
	@Column(name="FILE_TYPE", length = 5, nullable = true)
    private String fileType;
	
	@Column(name="FILE_PATH", length = 256, nullable = true)
    private String filePath;
	
	@Column(name="FILE_DESC", length = 300, nullable = true)
    private String fileDesc;

	@Converter(name = "FileItem.moduleType", converterClass = ModuleType.Converter.class )
	@Convert("FileItem.moduleType")
	@Column(name="MODULE_TYPE", length=1)
    private ModuleType moduleType;
	
	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public ModuleType getModuleType() {
		return moduleType;
	}

	public void setModuleType(ModuleType moduleType) {
		this.moduleType = moduleType;
	}

}