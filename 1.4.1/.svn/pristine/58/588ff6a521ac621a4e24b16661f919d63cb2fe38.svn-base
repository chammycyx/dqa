package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ChemAnalysisQtyReq  implements StringValuedEnum {
	
	Is1PackLessThan60Tab("CR1", "1 pack where each pack contains no less than 60 tablets/capsules"),
	Is2PacksLessThan60Tab("CR2", "2 packs where each pack contains no less than 60 tablets/capsules"),
	Is2PacksLessThan100ml("CR3", "2 packs where each pack contains no less than 100ml"),
	Is2PacksLessThan15g("CR4", "2 packs where each pack contains no less than 15 gram"),
	Is2Units("CR5", "2 units"),
	Is5Units("CR6", "5 units"),
	Is7Units("CR7", "7 units"),
	Is10Units("CR8", "10 units"),
	Is15Units("CR9", "15 units"),
	Is20Units("CR10", "20 units"),
	Other("O", "Other");
	
	private final String dataValue;
    private final String displayValue;
	
	ChemAnalysisQtyReq(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static class Converter extends StringValuedEnumConverter<ChemAnalysisQtyReq> {

		private static final long serialVersionUID = 3601829209059683642L;

		@Override
    	public Class<ChemAnalysisQtyReq> getEnumClass() {
    		return ChemAnalysisQtyReq.class;
    	}
    }
}
