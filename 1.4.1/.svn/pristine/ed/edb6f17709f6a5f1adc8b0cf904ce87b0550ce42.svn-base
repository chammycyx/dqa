package hk.org.ha.model.pms.dqa.exception;
import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class DqaException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7772536878998958275L;
	private String asString;

	public DqaException(String s) {
		super(s);
		asString = s;
	}
    
    public DqaException(Throwable source) {
        super(source.getMessage(), wrap(source.getCause()));
        init(source);
    }

    private void init(Throwable source) {
        asString = source.toString() +  " [Wrapped in DqaException]";
        setStackTrace(source.getStackTrace());
    }

    public String toString() {
        return asString;
    }
    
    /**
     * Wraps a Throwable
     * @param t Throwable to wrap
     * @return a wrapped Throwable
     */
    private static DqaException wrap(Throwable t) {
    	if (t != null) {
    		return new DqaException(t);
    	}
    	return null;
    }    
}
