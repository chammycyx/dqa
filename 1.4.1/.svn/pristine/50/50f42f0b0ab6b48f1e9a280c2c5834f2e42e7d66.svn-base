package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleItemService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class SampleItemServiceBean implements SampleItemServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private SampleItem sampleItem;

	@In
	private StatusMessages statusMessages;
	
	@In(create=true)
	private  ExclusionTestListServiceLocal exclusionTestListService;
	
	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In(create=true)
	private SampleTestListServiceLocal sampleTestListService;
	
	private boolean drugItemNotExist;
	
	private boolean duplicated;


	public SampleItem retrieveSampleItemByItemCode(String itemCodeIn){
		logger.debug("retrieveSampleItemByItemCode");

		List<SampleItem> sampleItemList = em.createNamedQuery("SampleItem.findByItemCode")
					 .setParameter("itemCode", itemCodeIn.trim().toUpperCase(Locale.ENGLISH))
					 .setHint(QueryHints.BATCH, "o.exclusionTestList")
					 .getResultList();

		if (sampleItemList.size()>0) {
				SampleItem tmpSampleItem = sampleItemList.get(0);
				
				tmpSampleItem.getExclusionTestList().size();
				logger.debug("exclusion list size :: #0", tmpSampleItem.getExclusionTestList().size());
				return tmpSampleItem;
		}		
		
		return null;
	}
	
	public void retrieveSampleItemForSchedule(String itemCode){
		
		sampleItem = retrieveSampleItemByItemCode(itemCode);
		
	}
	
	
	public void retrieveSampleItemForSampleTest(String itemCode){
		logger.debug("retrieveSampleItemForSampleTest :#0", itemCode);
		sampleItem = null;
		SampleItem tmpSampleItem = retrieveSampleItemForSampleTestValidation(itemCode, RecordStatus.Active);
		if (tmpSampleItem != null ){
			
			List <SampleTest> sampleTestList = sampleTestListService.retrieveSampleTestListForReport();
			int sampleTestListCnt = sampleTestList.size();
			
			logger.debug("item : #0 ,exclusion size : #1 , test total size : #2 ",tmpSampleItem.getItemCode(), 
					tmpSampleItem.getExclusionTestList().size(),sampleTestListCnt );
			
			if (sampleTestListCnt > tmpSampleItem.getExclusionTestList().size()){
				
				sampleItem = tmpSampleItem;
			}
		}
	}
	
	public void retrieveAllSampleItemByItemCodeRecordStatus(String itemCode, RecordStatus recordStatus){
		logger.debug("retrieveAllSampleItemByItemCodeRecordStatus :#0", itemCode);
		retrieveSampleItemByItemCodeRecordStatus(itemCode, recordStatus);
	}
	
	
	public void retrieveSampleItemByItemCodeRecordStatus(String itemCode, RecordStatus recordStatus){
//		sampleItem = retrieveSampleItemForSampleTestValidation(itemCode, recordStatus);
		logger.debug("retrieveSampleItemByItemCodeRecordStatus");
		
		List<SampleItem> tmpSampleItemList = em.createNamedQuery("SampleItem.findByItemCodeRecordStatus")
		 .setParameter("itemCode", itemCode.trim())
		 .setParameter("recordStatus", recordStatus)
		 .setHint(QueryHints.FETCH, "o.riskLevel")
		 .setHint(QueryHints.BATCH, "o.exclusionTestList")
		 .getResultList();
		
		if (!tmpSampleItemList.isEmpty()) {
			sampleItem =  tmpSampleItemList.get(0);
			sampleItem.getExclusionTestList().size();
//			sampleItem.postLoad(); // ???

//			logger.debug("exclusion list size : #0", tmpSampleItem.getExclusionTestList().size());
		}else
		{
			sampleItem=null;
		}
	}
	
	public SampleItem retrieveSampleItemForSampleTestValidation(String itemCode, RecordStatus recordStatus){
		logger.debug("retrieveSampleItemForSampleTestValidation");
		List<SampleItem> outList = new ArrayList<SampleItem>();
		List<SampleItem> tmpSampleItemList = em.createNamedQuery("SampleItem.findForSampleTestValidation")
					 .setParameter("itemCode", itemCode.trim())
					 .setParameter("recordStatus", recordStatus)
					 .setParameter("proceedSampleTestFlag", YesNoFlag.Yes )
					 .setHint(QueryHints.FETCH, "o.riskLevel")
					 .setHint(QueryHints.BATCH, "o.exclusionTestList")
					 .getResultList();

		if (!tmpSampleItemList.isEmpty()) {
			for (SampleItem sampleItemFind :tmpSampleItemList ){
				if(sampleItemFind.getDmDrug()!=null)
				{
					if( ((RegStatus.PandPRegistrationNotRequired).getDataValue().equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getRegStatus()) ||   
						(RegStatus.PandPRegistrationDrug).getDataValue().equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getRegStatus())) &&
						((ReleaseIndicator.X).getDataValue().equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getReleaseIndicator()) ||   
						(ReleaseIndicator.Yes).getDataValue().equals(sampleItemFind.getDmDrug().getDqaDrugSupplier().getReleaseIndicator()))
					  )
					{
						outList.add(sampleItemFind);
					}
				}
			}
		}
			
			
		if (!outList.isEmpty()) {	
			
			SampleItem tmpSampleItem = outList.get(0);
			tmpSampleItem.getExclusionTestList().size();

//			tmpSampleItem.postLoad(); //???

//			logger.debug("exclusion list size : #0", tmpSampleItem.getExclusionTestList().size());
			 
			return outList.get(0);
		}	
		return null;
	}
	
	public void addSampleItem(){
		logger.debug("addSampleItem");
		sampleItem = new SampleItem();
		
		List<ExclusionTest> exList = new ArrayList<ExclusionTest>();
		
		sampleItem.setExclusionTestList(exList);
//		sampleItem.setDrugItem(new DrugItem());
	}
	
	
	private String retrieveExclusionText(SampleItem sampleItemIn){
		
		StringBuffer sbf = new StringBuffer();
		int cnt = 0;
		for(ExclusionTest ex: sampleItemIn.getExclusionTestList()){
			sbf.append(ex.getSampleTest().getTestCode());
			sbf.append(" - ");
			sbf.append(ex.getExclusionDesc());

			if (sampleItemIn.getExclusionTestList().size()-1 > cnt){
				sbf.append(", ");
			}
			cnt++;	
		}
		
		return sbf.toString();
	}
	
	public void createSampleItem(SampleItem sampleItemIn) {
		logger.debug("createSampleItem");
		drugItemNotExist = true;
		duplicated = true;

		DmDrug dmDrugFind = dmDrugCacher.getDrugByItemCode(sampleItemIn.getItemCode());  

		if (dmDrugFind != null) {
			drugItemNotExist = false;

			SampleItem sampleItemFind = retrieveSampleItemByItemCode(sampleItemIn.getItemCode());		

			if (sampleItemFind==null) {
				
				sampleItemIn.setExclusion(retrieveExclusionText(sampleItemIn));

				if(sampleItemIn.getSpeicalCat()==null || ("").equals(sampleItemIn.getSpeicalCat().getDataValue().trim()))
				{
					sampleItemIn.setSpeicalCat(SpecialCat.Blank);
				}
				
				for (ExclusionTest ex: sampleItemIn.getExclusionTestList()){
					ex.setSampleItem(sampleItemIn);
				}
				
				em.persist(sampleItemIn);
				em.flush();				

				duplicated = false;
			}else {
				if (sampleItemFind.getRecordStatus() == RecordStatus.Deleted){
					sampleItemFind.setRiskLevel(sampleItemIn.getRiskLevel());
					sampleItemFind.setChemAnalysisQtyReq(sampleItemIn.getChemAnalysisQtyReq());
					sampleItemFind.setChemAnalysisQtyReqDesc(sampleItemIn.getChemAnalysisQtyReqDesc());
					sampleItemFind.setMicroBioTestQtyReq(sampleItemIn.getMicroBioTestQtyReq());
					sampleItemFind.setMicroBioTestQtyReqDesc(sampleItemIn.getMicroBioTestQtyReqDesc());
					sampleItemFind.setValidTestQtyReq(sampleItemIn.getValidTestQtyReq());
					sampleItemFind.setValidTestQtyReqDesc(sampleItemIn.getValidTestQtyReqDesc());
					sampleItemFind.setChemAnalysisRemark(sampleItemIn.getChemAnalysisRemark());
					sampleItemFind.setMicroBioTestRemark(sampleItemIn.getMicroBioTestRemark());
					sampleItemFind.setRemark(sampleItemIn.getRemark());
					if(sampleItemIn.getSpeicalCat()==null || ("").equals(sampleItemIn.getSpeicalCat().getDataValue().trim()))
					{
						sampleItemFind.setSpeicalCat(SpecialCat.Blank);
					}
					sampleItemFind.setRecordStatus(RecordStatus.Active);		
					
					sampleItemFind.getExclusionTestList().size();
					sampleItemFind.setExclusion(retrieveExclusionText(sampleItemIn));
					
					for (ExclusionTest ex: sampleItemIn.getExclusionTestList()){
						ex.setSampleItem(sampleItemFind);
						sampleItemFind.getExclusionTestList().add(ex);
					}

					sampleItem = em.merge(sampleItemFind);
					
					if (sampleItem!=null){
						logger.debug("sampleItem updated");	
					}
					em.flush();
					duplicated = false;
				}
			}
		}
		
		if (drugItemNotExist){
			logger.debug("item not exist #0",  sampleItemIn.getItemCode() );
			statusMessages.addToControl("txtSampleItemItemCode", "Item not exist");
			sampleItem = sampleItemIn; 
		}else if (duplicated) {
			sampleItem = sampleItemIn; 
			statusMessages.addToControl("txtSampleItemItemCode", "Item - #{sampleItemIn.getDrugItem().getItemCode()} already exists");
		}
	}
	
	public void updateSampleItem(SampleItem sampleItemIn) {
		logger.debug("updateSampleItem");
		
		DmDrug dmDrugFind = dmDrugCacher.getDrugByItemCode(sampleItemIn.getItemCode());   
		
		if (dmDrugFind != null){	
			
			sampleItemIn.setExclusion(retrieveExclusionText(sampleItemIn));
			if(sampleItemIn.getSpeicalCat()==null || ("").equals(sampleItemIn.getSpeicalCat().getDataValue().trim()))
			{
				sampleItemIn.setSpeicalCat(SpecialCat.Blank);
			}
			
			sampleItem = em.merge(sampleItemIn);
			
			if (sampleItem != null){
				logger.debug("SampleItem updated");
			}		
			em.flush();
		}
	}

	public void deleteSampleItem(SampleItem sampleItemIn){
		logger.debug("deleteSampleItem");
		
		List<ExclusionTest> exList = exclusionTestListService.retrieveExclusionTestListByItemCode(sampleItemIn.getItemCode().trim().toUpperCase(Locale.ENGLISH));
		
		for (ExclusionTest ex : exList){
			em.remove(ex);
		}

		sampleItemIn.setRecordStatus(RecordStatus.Deleted);
		sampleItem = em.merge(sampleItemIn);
		
		if (sampleItem != null){
			logger.debug("SampleItem logically deleted");
		}
		em.flush();
	}

	public boolean isDuplicated(){
		
		return duplicated;
	}
		
	
	public boolean isDrugItemNotExist() {
		return drugItemNotExist;
	}
	
	@Remove
	public void destroy(){
		if (sampleItem != null){
			sampleItem = null;
		}
	}



	
}
