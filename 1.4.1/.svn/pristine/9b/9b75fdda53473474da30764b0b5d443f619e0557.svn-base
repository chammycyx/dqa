<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:tv="org.granite.tide.validators.*"
	xmlns:tsv="org.granite.tide.seam.validators.*"
	xmlns:mdcs="com.iwobanas.controls.*"
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("pharmProblemPopupFileViewPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		<mx:DateFormatter id="dateFormatter" formatString="DD-MMM-YYYY"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[		
			import hk.org.ha.fmk.pms.flex.components.message.*;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			
			import hk.org.ha.event.pms.dqa.suppperf.RefreshPharmProblemPopupFileListEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RefreshPharmProblemPopupFormDataFileListEvent;
			import hk.org.ha.event.pms.dqa.suppperf.popup.ShowPharmProblemPopupFileUploadPopupEvent;
			
			import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemFile;
			import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
			import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
			
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ListEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			import spark.events.TextOperationEvent;
			
			[Bindable]
			public var pharmProblemIn:PharmProblem;
			
			[Bindable]
			protected var gpViewFileContextMenu:ContextMenu = new ContextMenu();
			
			[Bindable]
			public var pharmProblemFileUploadDataList:ArrayCollection = new ArrayCollection();
			
			private var screenName:String="pharmProblemPopupFileViewPopup";
			private var lookupProp:LookupPopupProp = new LookupPopupProp();
			private var msgProp:SystemMessagePopupProp;
			
			protected function addGpViewFileContextMenu():void{
				
				var addFileMenuItem:ContextMenuItem   = new ContextMenuItem("Add File");
				var deleteFileMenuItem:ContextMenuItem   = new ContextMenuItem("Delete File");
				
				addFileMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    addFileMenuClick);
				deleteFileMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    deleteFileMenuClick);
				
				gpViewFileContextMenu.hideBuiltInItems();
				
				if(currentState=="Add"){
					gpViewFileContextMenu.customItems = [addFileMenuItem, deleteFileMenuItem];
				}
			}
			
			protected function addFileMenuClick(evt:ContextMenuEvent):void{
				dispatchEvent(new ShowPharmProblemPopupFileUploadPopupEvent(pharmProblemIn));
			}
			
			protected function deleteFileMenuClick(evt:ContextMenuEvent):void{
				if(gpViewFile.selectedItem !=null){
					deleteFile( gpViewFile.selectedIndex );
				}
				else {
					showMessage("0008");
				}
			}
			
			public function deleteFile(fileIndex:int):void{
				pharmProblemFileUploadDataList.removeItemAt(fileIndex);
			}
			
			
			public function setPharmProblemFileUploadDataList(pharmProblemFileUploadData:PharmProblemFileUploadData):void{
				pharmProblemFileUploadDataList.addItem(pharmProblemFileUploadData);
			}
			
			private function uploadDateLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return dateFormatter.format(item.singleLevelFileItemCreateDate);
			}
			
			protected function save():void{
				
				if(currentState=="Add"){
					dispatchEvent(new RefreshPharmProblemPopupFormDataFileListEvent(pharmProblemFileUploadDataList) );
				}
				closeWin();
			}
			
			public function closeWin():void
			{
				PopUpManager.removePopUp(this);
			}
			
			////////////////
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
		]]>
	</fx:Script>
	
	<fc:states>		
		<s:State name="Add"/>
		<s:State name="View"/>		
		<s:State name="QaView"/>
	</fc:states>
	
	
	<s:VGroup id="vHeaderAll" width="100%" gap="0" paddingBottom="0" paddingTop="0" paddingLeft="0" paddingRight="0">
		<s:HGroup verticalAlign="middle" width="100%" >
			<s:VGroup >
				<s:HGroup verticalAlign="middle" width="100%">
					<s:Panel id="pViewFile" title="View File" mouseEnabled="true" contextMenu="{gpViewFileContextMenu}"  creationComplete="addGpViewFileContextMenu()">
						<s:layout>
							<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
						</s:layout>
						<s:VGroup>
							<s:HGroup verticalAlign="middle" width="100%" visible.Add="false" visible.View="true" visible.QaView="true"  height.Add="0">
								<s:VGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<s:Label text="Institution" width="150" />
										<fc:UppercaseTextInput id="txtInstitutionCode" width="100" enabled="false" text="{pharmProblemIn.institution.institutionCode}" />
									</s:HGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<s:Label text="Item Code" width="150"/>
										<fc:UppercaseTextInput id="txtItemCode" text="{pharmProblemIn.problemHeader.itemCode}" width="100" enabled="false" />
										<s:TextInput id="txtItemDesc"  text="{pharmProblemIn.problemHeader.dmDrug==null?'':pharmProblemIn.problemHeader.dmDrug.fullDrugDesc}" enabled="false" width="410"/>
									</s:HGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<s:Label text="Supplier" width="150"/>
										<fc:UppercaseTextInput id="txtSupplierCode" width="100" enabled="false" text="{pharmProblemIn.contract.supplier.supplierCode}" />
									</s:HGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<s:Label text="Manufacturer" width="150"/>
										<fc:UppercaseTextInput id="txtManufCode" width="100" enabled="false" text="{pharmProblemIn.contract.manufacturer.companyCode}" />
									</s:HGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<s:Label text="Pharmaceutical Company" width="150"/>
										<fc:UppercaseTextInput id="txtPharmCompanyCode" width="100" enabled="false" text="{pharmProblemIn.contract.pharmCompany.companyCode}" />
									</s:HGroup>
								</s:VGroup>
							</s:HGroup>
							<s:HGroup verticalAlign="middle" width="100%" >
								<s:VGroup>
									<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
										<mdcs:MDataGrid id="gpViewFile" width="100%" copyDataProvider="false" verticalScrollPolicy="on" mouseEnabled="true"
														rowCount="4" dataProvider="{pharmProblemFileUploadDataList}">
											<mdcs:columns>
												<mx:DataGridColumn headerText="Document" minWidth="400" width="0.8" dataField="singleLevelFileItemFileName" draggable="false" sortable="false">
													<mx:itemRenderer>
														<fx:Component>
															<s:MXDataGridItemRenderer autoDrawBackground="false" clipAndEnableScrolling="true">
																<mx:LinkButton textAlign="left" label="{data.singleLevelFileItemFileName}" color="blue" textSelectedColor="red" fontWeight="normal" textDecoration="underline"
																			   click="viewFileItem(data.fileItem)" visible="{isNaN(data.fileItem.fileId)?false:true}" includeInLayout="{isNaN(data.fileItem.fileId)?false:true}" >
																	<fx:Script>
																		<![CDATA[   
																			import hk.org.ha.event.pms.dqa.OpenSuppPerfFileItemDocumentEvent;
																			import hk.org.ha.model.pms.dqa.persistence.FileItem;
																			private function viewFileItem(data:FileItem):void {	
																				if(data!=null)
																				{
																					if(!isNaN(data.fileId))
																					{
																						dispatchEvent(new OpenSuppPerfFileItemDocumentEvent(data, "_blank"));
																					}
																				}
																			}		
																		]]>
																	</fx:Script>
																</mx:LinkButton>
																<s:Label   paddingTop="6" paddingLeft="10"  text="{data.singleLevelFileItemFileName}" visible="{isNaN(data.fileItem.fileId)?true:false}" includeInLayout="{isNaN(data.fileItem.fileId)?true:false}" />
															</s:MXDataGridItemRenderer>
														</fx:Component>
													</mx:itemRenderer>
												</mx:DataGridColumn> 
												<mx:DataGridColumn headerText="Type" minWidth="50" width="0.1" dataField="singleLevelFileItemFileType" draggable="false" sortable="false"/>
												<mx:DataGridColumn headerText="Upload Date" minWidth="200" width="0.2" dataField="singleLevelFileItemCreateDate" labelFunction="uploadDateLabelFunc" draggable="false" sortable="false"/>
											</mdcs:columns>
										</mdcs:MDataGrid>
									</s:HGroup>
									<s:HGroup width="100%" horizontalAlign="right">
										<mx:Spacer width="600"/>
										<s:Button label="OK" click="save()" />
										<!--<s:Button label="Cancel" click="closeWin()"/>-->
									</s:HGroup>
								</s:VGroup>
							</s:HGroup>
						</s:VGroup>
					</s:Panel>
				</s:HGroup>
			</s:VGroup>
		</s:HGroup>
	</s:VGroup>
	
</fc:ExtendedNavigatorContent>