/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CoaBatch.as).
 */

package hk.org.ha.model.pms.dqa.persistence.coa {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.udt.coa.ConfirmCheckListFlag;
    import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class CoaBatchBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _batchNum:String;
        private var _cfmChecklistFlag:ConfirmCheckListFlag;
        private var _coaBatchId:Number;
        private var _coaBatchVer:CoaBatchVer;
        private var _coaBatchVerCount:Number;
        private var _coaBatchVerList:ListCollectionView;
        private var _coaDiscrepancyList:ListCollectionView;
        private var _coaFileFolderList:ListCollectionView;
        private var _coaItem:CoaItem;
        private var _combinedDiscrepancy:String;
        private var _discrepancyActionDate:Date;
        private var _discrepancyRemark:String;
        private var _failRemark:String;
        private var _followUpDate:Date;
        private var _formattedCreateDate:String;
        private var _formattedFollowUpDate:String;
        private var _interfaceDate:Date;
        private var _interfaceFlag:InterfaceFlag;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is CoaBatch) || (property as CoaBatch).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set batchNum(value:String):void {
            _batchNum = value;
        }
        public function get batchNum():String {
            return _batchNum;
        }

        public function set cfmChecklistFlag(value:ConfirmCheckListFlag):void {
            _cfmChecklistFlag = value;
        }
        public function get cfmChecklistFlag():ConfirmCheckListFlag {
            return _cfmChecklistFlag;
        }

        public function set coaBatchId(value:Number):void {
            _coaBatchId = value;
        }
        [Id]
        public function get coaBatchId():Number {
            return _coaBatchId;
        }

        public function set coaBatchVer(value:CoaBatchVer):void {
            _coaBatchVer = value;
        }
        public function get coaBatchVer():CoaBatchVer {
            return _coaBatchVer;
        }

        public function set coaBatchVerCount(value:Number):void {
            _coaBatchVerCount = value;
        }
        public function get coaBatchVerCount():Number {
            return _coaBatchVerCount;
        }

        public function set coaBatchVerList(value:ListCollectionView):void {
            _coaBatchVerList = value;
        }
        public function get coaBatchVerList():ListCollectionView {
            return _coaBatchVerList;
        }

        public function set coaDiscrepancyList(value:ListCollectionView):void {
            _coaDiscrepancyList = value;
        }
        public function get coaDiscrepancyList():ListCollectionView {
            return _coaDiscrepancyList;
        }

        public function set coaFileFolderList(value:ListCollectionView):void {
            _coaFileFolderList = value;
        }
        public function get coaFileFolderList():ListCollectionView {
            return _coaFileFolderList;
        }

        public function set coaItem(value:CoaItem):void {
            _coaItem = value;
        }
        public function get coaItem():CoaItem {
            return _coaItem;
        }

        public function set combinedDiscrepancy(value:String):void {
            _combinedDiscrepancy = value;
        }
        public function get combinedDiscrepancy():String {
            return _combinedDiscrepancy;
        }

        public function set discrepancyActionDate(value:Date):void {
            _discrepancyActionDate = value;
        }
        public function get discrepancyActionDate():Date {
            return _discrepancyActionDate;
        }

        public function set discrepancyRemark(value:String):void {
            _discrepancyRemark = value;
        }
        public function get discrepancyRemark():String {
            return _discrepancyRemark;
        }

        public function set failRemark(value:String):void {
            _failRemark = value;
        }
        public function get failRemark():String {
            return _failRemark;
        }

        public function set followUpDate(value:Date):void {
            _followUpDate = value;
        }
        public function get followUpDate():Date {
            return _followUpDate;
        }

        public function set formattedCreateDate(value:String):void {
            _formattedCreateDate = value;
        }
        public function get formattedCreateDate():String {
            return _formattedCreateDate;
        }

        public function set formattedFollowUpDate(value:String):void {
            _formattedFollowUpDate = value;
        }
        public function get formattedFollowUpDate():String {
            return _formattedFollowUpDate;
        }

        public function set interfaceDate(value:Date):void {
            _interfaceDate = value;
        }
        public function get interfaceDate():Date {
            return _interfaceDate;
        }

        public function set interfaceFlag(value:InterfaceFlag):void {
            _interfaceFlag = value;
        }
        public function get interfaceFlag():InterfaceFlag {
            return _interfaceFlag;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_coaBatchId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_coaBatchId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:CoaBatchBase = CoaBatchBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._batchNum, _batchNum, null, this, 'batchNum', function setter(o:*):void{_batchNum = o as String}, false);
               em.meta_mergeExternal(src._cfmChecklistFlag, _cfmChecklistFlag, null, this, 'cfmChecklistFlag', function setter(o:*):void{_cfmChecklistFlag = o as ConfirmCheckListFlag}, false);
               em.meta_mergeExternal(src._coaBatchId, _coaBatchId, null, this, 'coaBatchId', function setter(o:*):void{_coaBatchId = o as Number}, false);
               em.meta_mergeExternal(src._coaBatchVer, _coaBatchVer, null, this, 'coaBatchVer', function setter(o:*):void{_coaBatchVer = o as CoaBatchVer}, false);
               em.meta_mergeExternal(src._coaBatchVerCount, _coaBatchVerCount, null, this, 'coaBatchVerCount', function setter(o:*):void{_coaBatchVerCount = o as Number}, false);
               em.meta_mergeExternal(src._coaBatchVerList, _coaBatchVerList, null, this, 'coaBatchVerList', function setter(o:*):void{_coaBatchVerList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._coaDiscrepancyList, _coaDiscrepancyList, null, this, 'coaDiscrepancyList', function setter(o:*):void{_coaDiscrepancyList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._coaFileFolderList, _coaFileFolderList, null, this, 'coaFileFolderList', function setter(o:*):void{_coaFileFolderList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._coaItem, _coaItem, null, this, 'coaItem', function setter(o:*):void{_coaItem = o as CoaItem}, false);
               em.meta_mergeExternal(src._combinedDiscrepancy, _combinedDiscrepancy, null, this, 'combinedDiscrepancy', function setter(o:*):void{_combinedDiscrepancy = o as String}, false);
               em.meta_mergeExternal(src._discrepancyActionDate, _discrepancyActionDate, null, this, 'discrepancyActionDate', function setter(o:*):void{_discrepancyActionDate = o as Date}, false);
               em.meta_mergeExternal(src._discrepancyRemark, _discrepancyRemark, null, this, 'discrepancyRemark', function setter(o:*):void{_discrepancyRemark = o as String}, false);
               em.meta_mergeExternal(src._failRemark, _failRemark, null, this, 'failRemark', function setter(o:*):void{_failRemark = o as String}, false);
               em.meta_mergeExternal(src._followUpDate, _followUpDate, null, this, 'followUpDate', function setter(o:*):void{_followUpDate = o as Date}, false);
               em.meta_mergeExternal(src._formattedCreateDate, _formattedCreateDate, null, this, 'formattedCreateDate', function setter(o:*):void{_formattedCreateDate = o as String}, false);
               em.meta_mergeExternal(src._formattedFollowUpDate, _formattedFollowUpDate, null, this, 'formattedFollowUpDate', function setter(o:*):void{_formattedFollowUpDate = o as String}, false);
               em.meta_mergeExternal(src._interfaceDate, _interfaceDate, null, this, 'interfaceDate', function setter(o:*):void{_interfaceDate = o as Date}, false);
               em.meta_mergeExternal(src._interfaceFlag, _interfaceFlag, null, this, 'interfaceFlag', function setter(o:*):void{_interfaceFlag = o as InterfaceFlag}, false);
            }
            else {
               em.meta_mergeExternal(src._coaBatchId, _coaBatchId, null, this, 'coaBatchId', function setter(o:*):void{_coaBatchId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _batchNum = input.readObject() as String;
                _cfmChecklistFlag = Enum.readEnum(input) as ConfirmCheckListFlag;
                _coaBatchId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _coaBatchVer = input.readObject() as CoaBatchVer;
                _coaBatchVerCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _coaBatchVerList = input.readObject() as ListCollectionView;
                _coaDiscrepancyList = input.readObject() as ListCollectionView;
                _coaFileFolderList = input.readObject() as ListCollectionView;
                _coaItem = input.readObject() as CoaItem;
                _combinedDiscrepancy = input.readObject() as String;
                _discrepancyActionDate = input.readObject() as Date;
                _discrepancyRemark = input.readObject() as String;
                _failRemark = input.readObject() as String;
                _followUpDate = input.readObject() as Date;
                _formattedCreateDate = input.readObject() as String;
                _formattedFollowUpDate = input.readObject() as String;
                _interfaceDate = input.readObject() as Date;
                _interfaceFlag = Enum.readEnum(input) as InterfaceFlag;
            }
            else {
                _coaBatchId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_batchNum is IPropertyHolder) ? IPropertyHolder(_batchNum).object : _batchNum);
                output.writeObject((_cfmChecklistFlag is IPropertyHolder) ? IPropertyHolder(_cfmChecklistFlag).object : _cfmChecklistFlag);
                output.writeObject((_coaBatchId is IPropertyHolder) ? IPropertyHolder(_coaBatchId).object : _coaBatchId);
                output.writeObject((_coaBatchVer is IPropertyHolder) ? IPropertyHolder(_coaBatchVer).object : _coaBatchVer);
                output.writeObject((_coaBatchVerCount is IPropertyHolder) ? IPropertyHolder(_coaBatchVerCount).object : _coaBatchVerCount);
                output.writeObject((_coaBatchVerList is IPropertyHolder) ? IPropertyHolder(_coaBatchVerList).object : _coaBatchVerList);
                output.writeObject((_coaDiscrepancyList is IPropertyHolder) ? IPropertyHolder(_coaDiscrepancyList).object : _coaDiscrepancyList);
                output.writeObject((_coaFileFolderList is IPropertyHolder) ? IPropertyHolder(_coaFileFolderList).object : _coaFileFolderList);
                output.writeObject((_coaItem is IPropertyHolder) ? IPropertyHolder(_coaItem).object : _coaItem);
                output.writeObject((_combinedDiscrepancy is IPropertyHolder) ? IPropertyHolder(_combinedDiscrepancy).object : _combinedDiscrepancy);
                output.writeObject((_discrepancyActionDate is IPropertyHolder) ? IPropertyHolder(_discrepancyActionDate).object : _discrepancyActionDate);
                output.writeObject((_discrepancyRemark is IPropertyHolder) ? IPropertyHolder(_discrepancyRemark).object : _discrepancyRemark);
                output.writeObject((_failRemark is IPropertyHolder) ? IPropertyHolder(_failRemark).object : _failRemark);
                output.writeObject((_followUpDate is IPropertyHolder) ? IPropertyHolder(_followUpDate).object : _followUpDate);
                output.writeObject((_formattedCreateDate is IPropertyHolder) ? IPropertyHolder(_formattedCreateDate).object : _formattedCreateDate);
                output.writeObject((_formattedFollowUpDate is IPropertyHolder) ? IPropertyHolder(_formattedFollowUpDate).object : _formattedFollowUpDate);
                output.writeObject((_interfaceDate is IPropertyHolder) ? IPropertyHolder(_interfaceDate).object : _interfaceDate);
                output.writeObject((_interfaceFlag is IPropertyHolder) ? IPropertyHolder(_interfaceFlag).object : _interfaceFlag);
            }
            else {
                output.writeObject(_coaBatchId);
            }
        }
    }
}
