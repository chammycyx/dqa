/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ProblemNatureCat.as).
 */

package hk.org.ha.model.pms.dqa.persistence.suppperf {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.udt.RecordStatus;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class ProblemNatureCatBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _catDesc:String;
        private var _displayOrder:Number;
        private var _problemNatureCatId:Number;
        private var _problemNatureParam:ProblemNatureParam;
        private var _recordStatus:RecordStatus;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is ProblemNatureCat) || (property as ProblemNatureCat).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set catDesc(value:String):void {
            _catDesc = value;
        }
        public function get catDesc():String {
            return _catDesc;
        }

        public function set displayOrder(value:Number):void {
            _displayOrder = value;
        }
        public function get displayOrder():Number {
            return _displayOrder;
        }

        public function set problemNatureCatId(value:Number):void {
            _problemNatureCatId = value;
        }
        [Id]
        public function get problemNatureCatId():Number {
            return _problemNatureCatId;
        }

        public function set problemNatureParam(value:ProblemNatureParam):void {
            _problemNatureParam = value;
        }
        public function get problemNatureParam():ProblemNatureParam {
            return _problemNatureParam;
        }

        public function set recordStatus(value:RecordStatus):void {
            _recordStatus = value;
        }
        public function get recordStatus():RecordStatus {
            return _recordStatus;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_problemNatureCatId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_problemNatureCatId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:ProblemNatureCatBase = ProblemNatureCatBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._catDesc, _catDesc, null, this, 'catDesc', function setter(o:*):void{_catDesc = o as String}, false);
               em.meta_mergeExternal(src._displayOrder, _displayOrder, null, this, 'displayOrder', function setter(o:*):void{_displayOrder = o as Number}, false);
               em.meta_mergeExternal(src._problemNatureCatId, _problemNatureCatId, null, this, 'problemNatureCatId', function setter(o:*):void{_problemNatureCatId = o as Number}, false);
               em.meta_mergeExternal(src._problemNatureParam, _problemNatureParam, null, this, 'problemNatureParam', function setter(o:*):void{_problemNatureParam = o as ProblemNatureParam}, false);
               em.meta_mergeExternal(src._recordStatus, _recordStatus, null, this, 'recordStatus', function setter(o:*):void{_recordStatus = o as RecordStatus}, false);
            }
            else {
               em.meta_mergeExternal(src._problemNatureCatId, _problemNatureCatId, null, this, 'problemNatureCatId', function setter(o:*):void{_problemNatureCatId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _catDesc = input.readObject() as String;
                _displayOrder = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _problemNatureCatId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _problemNatureParam = input.readObject() as ProblemNatureParam;
                _recordStatus = Enum.readEnum(input) as RecordStatus;
            }
            else {
                _problemNatureCatId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_catDesc is IPropertyHolder) ? IPropertyHolder(_catDesc).object : _catDesc);
                output.writeObject((_displayOrder is IPropertyHolder) ? IPropertyHolder(_displayOrder).object : _displayOrder);
                output.writeObject((_problemNatureCatId is IPropertyHolder) ? IPropertyHolder(_problemNatureCatId).object : _problemNatureCatId);
                output.writeObject((_problemNatureParam is IPropertyHolder) ? IPropertyHolder(_problemNatureParam).object : _problemNatureParam);
                output.writeObject((_recordStatus is IPropertyHolder) ? IPropertyHolder(_recordStatus).object : _recordStatus);
            }
            else {
                output.writeObject(_problemNatureCatId);
            }
        }
    }
}
