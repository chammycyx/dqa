/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DelayDeliveryRptFormCriteria.as).
 */

package hk.org.ha.model.pms.dqa.vo.delaydelivery {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
    import mx.collections.ListCollectionView;
    import org.granite.util.Enum;

    [Bindable]
    public class DelayDeliveryRptFormCriteriaBase implements IExternalizable {

        private var _allInstitutionFlag:Boolean;
        private var _contractNum:String;
        private var _contractSuffix:String;
        private var _delayDeliveryRptFormStatusList:ListCollectionView;
        private var _fromReportDate:Date;
        private var _institutionList:ListCollectionView;
        private var _itemCode:String;
        private var _orderType:OrderTypeAll;
        private var _toReportDate:Date;

        public function set allInstitutionFlag(value:Boolean):void {
            _allInstitutionFlag = value;
        }
        public function get allInstitutionFlag():Boolean {
            return _allInstitutionFlag;
        }

        public function set contractNum(value:String):void {
            _contractNum = value;
        }
        public function get contractNum():String {
            return _contractNum;
        }

        public function set contractSuffix(value:String):void {
            _contractSuffix = value;
        }
        public function get contractSuffix():String {
            return _contractSuffix;
        }

        public function set delayDeliveryRptFormStatusList(value:ListCollectionView):void {
            _delayDeliveryRptFormStatusList = value;
        }
        public function get delayDeliveryRptFormStatusList():ListCollectionView {
            return _delayDeliveryRptFormStatusList;
        }

        public function set fromReportDate(value:Date):void {
            _fromReportDate = value;
        }
        public function get fromReportDate():Date {
            return _fromReportDate;
        }

        public function set institutionList(value:ListCollectionView):void {
            _institutionList = value;
        }
        public function get institutionList():ListCollectionView {
            return _institutionList;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set orderType(value:OrderTypeAll):void {
            _orderType = value;
        }
        public function get orderType():OrderTypeAll {
            return _orderType;
        }

        public function set toReportDate(value:Date):void {
            _toReportDate = value;
        }
        public function get toReportDate():Date {
            return _toReportDate;
        }

        public function readExternal(input:IDataInput):void {
            _allInstitutionFlag = input.readObject() as Boolean;
            _contractNum = input.readObject() as String;
            _contractSuffix = input.readObject() as String;
            _delayDeliveryRptFormStatusList = input.readObject() as ListCollectionView;
            _fromReportDate = input.readObject() as Date;
            _institutionList = input.readObject() as ListCollectionView;
            _itemCode = input.readObject() as String;
            _orderType = Enum.readEnum(input) as OrderTypeAll;
            _toReportDate = input.readObject() as Date;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_allInstitutionFlag);
            output.writeObject(_contractNum);
            output.writeObject(_contractSuffix);
            output.writeObject(_delayDeliveryRptFormStatusList);
            output.writeObject(_fromReportDate);
            output.writeObject(_institutionList);
            output.writeObject(_itemCode);
            output.writeObject(_orderType);
            output.writeObject(_toReportDate);
        }
    }
}