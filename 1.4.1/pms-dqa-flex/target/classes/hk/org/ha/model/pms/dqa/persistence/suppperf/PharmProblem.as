/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.dqa.persistence.suppperf {

	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmBatchNum;
	
	import mx.formatters.DateFormatter;
	
    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem")]
    public class PharmProblem extends PharmProblemBase {
		
		private static var df:DateFormatter = new DateFormatter();
		private const dateFormatString:String = "DD-MMM-YYYY";
		
		
		public function set affectQtyText(value:String):void {
			super.affectQty = Number(String);
		}
		
		public function get affectQtyText():String {
			if (isNaN(super.affectQty)) {
				return "";
			} else {
				return String(super.affectQty);
			}
		}
		
		public function get itemCodeToolTip():String {
			return problemHeader.dmDrug==null?"":problemHeader.dmDrug.fullDrugDesc;
		}
		
		
		public function get singleLevelProblemStatus():String {
			
			if(problemStatus==null)
			{
				return "";
			}
			else{
				return	problemStatus.dataValue;
			}
		}
		
		public function get singleLevelItemCode():String {
			return	problemHeader.itemCode;
		}
		
		public function get singleLevelOrderType():String {
			return	problemHeader.orderType.dataValue;
		}
		
		public function get singleLevelContractNum():String {
			var finalContractNum:String = "";
			if ( contract.contractNum != null ) {
				finalContractNum=contract.contractNum;
			}
			
			if ( contract.contractSuffix == null ) {
				return finalContractNum;	
			} else {
				return finalContractNum+"/"+contract.contractSuffix;
			}
		}
		
		public function get singleLevelSupplierCode():String {
			return	contract.supplier.supplierCode;
		}
		
		public function get singleLevelManufCode():String {
			return	contract.manufacturer.companyCode;
		}
		
		public function get singleLevelPharmCompanyCode():String {
		
			if(contract.pharmCompany==null)
			{
				return "";
			}
			else
			{
				return	contract.pharmCompany.companyCode;
			}
		}

		public function get singleLevelBatchNum():String {
			
			if(pharmBatchNumList==null || pharmBatchNumList.length==0)
			{
				return "";
			}
			else
			{
				var finalBatchNum:String = "";
				for( var i:int = 0; i < pharmBatchNumList.length; i++ )
				{
					if(i==0)
					{
						finalBatchNum = (pharmBatchNumList[i] as PharmBatchNum).batchNum;
					}
					else
					{
						finalBatchNum = finalBatchNum + "\n" + (pharmBatchNumList[i] as PharmBatchNum).batchNum; 
					}
				}
				return finalBatchNum;
			}
			
		}
		
		public function get singleLevelBaseUnit():String {
			return problemHeader.dmDrug==null?"":problemHeader.dmDrug.baseUnit;
		}
		
		public function get singleLevelInstitutionCode():String {
			return institution.institutionCode;
		}
		
		public function get singleLevelCaseNum():String {
			return (qaProblem==null?"":qaProblem.caseNum);
		}
		
		public function get singleLevelProblemDate():String {
			df.formatString = dateFormatString;
			if ( problemDate == null){
				return "";
			} else {	
				return df.format(problemDate);
			}
		}
		
		public function get singleLevelProblemDateOriginal():Date {
			return problemDate;
		}
		
	}
		
}