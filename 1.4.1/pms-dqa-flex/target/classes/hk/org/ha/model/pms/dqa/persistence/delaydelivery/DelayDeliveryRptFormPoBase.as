/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (DelayDeliveryRptFormPo.as).
 */

package hk.org.ha.model.pms.dqa.persistence.delaydelivery {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.persistence.FileItem;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class DelayDeliveryRptFormPoBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _approveDate:Date;
        private var _delayDeliveryRptForm:DelayDeliveryRptForm;
        private var _delayDeliveryRptFormPoId:Number;
        private var _fileId:FileItem;
        private var _orderQty:Number;
        private var _outstandQty:Number;
        private var _packSize:String;
        private var _poNum:Number;
        private var _quotationLeadTime:Number;
        private var _supplierReplyDate:Date;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is DelayDeliveryRptFormPo) || (property as DelayDeliveryRptFormPo).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set approveDate(value:Date):void {
            _approveDate = value;
        }
        public function get approveDate():Date {
            return _approveDate;
        }

        public function set delayDeliveryRptForm(value:DelayDeliveryRptForm):void {
            _delayDeliveryRptForm = value;
        }
        public function get delayDeliveryRptForm():DelayDeliveryRptForm {
            return _delayDeliveryRptForm;
        }

        public function set delayDeliveryRptFormPoId(value:Number):void {
            _delayDeliveryRptFormPoId = value;
        }
        [Id]
        public function get delayDeliveryRptFormPoId():Number {
            return _delayDeliveryRptFormPoId;
        }

        public function set fileId(value:FileItem):void {
            _fileId = value;
        }
        public function get fileId():FileItem {
            return _fileId;
        }

        public function set orderQty(value:Number):void {
            _orderQty = value;
        }
        public function get orderQty():Number {
            return _orderQty;
        }

        public function set outstandQty(value:Number):void {
            _outstandQty = value;
        }
        public function get outstandQty():Number {
            return _outstandQty;
        }

        public function set packSize(value:String):void {
            _packSize = value;
        }
        public function get packSize():String {
            return _packSize;
        }

        public function set poNum(value:Number):void {
            _poNum = value;
        }
        public function get poNum():Number {
            return _poNum;
        }

        public function set quotationLeadTime(value:Number):void {
            _quotationLeadTime = value;
        }
        public function get quotationLeadTime():Number {
            return _quotationLeadTime;
        }

        public function set supplierReplyDate(value:Date):void {
            _supplierReplyDate = value;
        }
        public function get supplierReplyDate():Date {
            return _supplierReplyDate;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_delayDeliveryRptFormPoId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_delayDeliveryRptFormPoId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:DelayDeliveryRptFormPoBase = DelayDeliveryRptFormPoBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._approveDate, _approveDate, null, this, 'approveDate', function setter(o:*):void{_approveDate = o as Date}, false);
               em.meta_mergeExternal(src._delayDeliveryRptForm, _delayDeliveryRptForm, null, this, 'delayDeliveryRptForm', function setter(o:*):void{_delayDeliveryRptForm = o as DelayDeliveryRptForm}, false);
               em.meta_mergeExternal(src._delayDeliveryRptFormPoId, _delayDeliveryRptFormPoId, null, this, 'delayDeliveryRptFormPoId', function setter(o:*):void{_delayDeliveryRptFormPoId = o as Number}, false);
               em.meta_mergeExternal(src._fileId, _fileId, null, this, 'fileId', function setter(o:*):void{_fileId = o as FileItem}, false);
               em.meta_mergeExternal(src._orderQty, _orderQty, null, this, 'orderQty', function setter(o:*):void{_orderQty = o as Number}, false);
               em.meta_mergeExternal(src._outstandQty, _outstandQty, null, this, 'outstandQty', function setter(o:*):void{_outstandQty = o as Number}, false);
               em.meta_mergeExternal(src._packSize, _packSize, null, this, 'packSize', function setter(o:*):void{_packSize = o as String}, false);
               em.meta_mergeExternal(src._poNum, _poNum, null, this, 'poNum', function setter(o:*):void{_poNum = o as Number}, false);
               em.meta_mergeExternal(src._quotationLeadTime, _quotationLeadTime, null, this, 'quotationLeadTime', function setter(o:*):void{_quotationLeadTime = o as Number}, false);
               em.meta_mergeExternal(src._supplierReplyDate, _supplierReplyDate, null, this, 'supplierReplyDate', function setter(o:*):void{_supplierReplyDate = o as Date}, false);
            }
            else {
               em.meta_mergeExternal(src._delayDeliveryRptFormPoId, _delayDeliveryRptFormPoId, null, this, 'delayDeliveryRptFormPoId', function setter(o:*):void{_delayDeliveryRptFormPoId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _approveDate = input.readObject() as Date;
                _delayDeliveryRptForm = input.readObject() as DelayDeliveryRptForm;
                _delayDeliveryRptFormPoId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _fileId = input.readObject() as FileItem;
                _orderQty = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _outstandQty = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _packSize = input.readObject() as String;
                _poNum = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _quotationLeadTime = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _supplierReplyDate = input.readObject() as Date;
            }
            else {
                _delayDeliveryRptFormPoId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_approveDate is IPropertyHolder) ? IPropertyHolder(_approveDate).object : _approveDate);
                output.writeObject((_delayDeliveryRptForm is IPropertyHolder) ? IPropertyHolder(_delayDeliveryRptForm).object : _delayDeliveryRptForm);
                output.writeObject((_delayDeliveryRptFormPoId is IPropertyHolder) ? IPropertyHolder(_delayDeliveryRptFormPoId).object : _delayDeliveryRptFormPoId);
                output.writeObject((_fileId is IPropertyHolder) ? IPropertyHolder(_fileId).object : _fileId);
                output.writeObject((_orderQty is IPropertyHolder) ? IPropertyHolder(_orderQty).object : _orderQty);
                output.writeObject((_outstandQty is IPropertyHolder) ? IPropertyHolder(_outstandQty).object : _outstandQty);
                output.writeObject((_packSize is IPropertyHolder) ? IPropertyHolder(_packSize).object : _packSize);
                output.writeObject((_poNum is IPropertyHolder) ? IPropertyHolder(_poNum).object : _poNum);
                output.writeObject((_quotationLeadTime is IPropertyHolder) ? IPropertyHolder(_quotationLeadTime).object : _quotationLeadTime);
                output.writeObject((_supplierReplyDate is IPropertyHolder) ? IPropertyHolder(_supplierReplyDate).object : _supplierReplyDate);
            }
            else {
                output.writeObject(_delayDeliveryRptFormPoId);
            }
        }
    }
}
