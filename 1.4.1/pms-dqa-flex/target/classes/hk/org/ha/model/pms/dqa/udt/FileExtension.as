/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.dqa.udt {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.FileExtension")]
    public class FileExtension extends Enum {

        public static const BMP:FileExtension = new FileExtension("BMP", _, ".bmp", "image/bmp");
        public static const GIF:FileExtension = new FileExtension("GIF", _, ".gif", "image/gif");
        public static const JPG:FileExtension = new FileExtension("JPG", _, ".jpg", "image/jpeg");
        public static const JPEG:FileExtension = new FileExtension("JPEG", _, ".jpeg", "image/jpeg");
        public static const PDF:FileExtension = new FileExtension("PDF", _, ".pdf", "application/pdf");
        public static const PNG:FileExtension = new FileExtension("PNG", _, ".png", "image/png");
        public static const TIF:FileExtension = new FileExtension("TIF", _, ".tif", "image/tiff");
        public static const TIFF:FileExtension = new FileExtension("TIFF", _, ".tiff", "image/tiff");

        function FileExtension(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || BMP.name), restrictor, (dataValue || BMP.dataValue), (displayValue || BMP.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [BMP, GIF, JPG, JPEG, PDF, PNG, TIF, TIFF];
        }

        public static function valueOf(name:String):FileExtension {
            return FileExtension(BMP.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):FileExtension {
            return FileExtension(BMP.dataConstantOf(dataValue));
        }
    }
}