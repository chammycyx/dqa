/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (FaxSummary.as).
 */

package hk.org.ha.model.pms.dqa.persistence.suppperf {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class FaxSummaryBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _faxSummaryId:Number;
        private var _finalHCount:Number;
        private var _finalHUpdateDate:Date;
        private var _finalSCount:Number;
        private var _finalSUpdateDate:Date;
        private var _initHCount:Number;
        private var _initHUpdateDate:Date;
        private var _initSCount:Number;
        private var _initSUpdateDate:Date;
        private var _interimHCount:Number;
        private var _interimHUpdateDate:Date;
        private var _interimSCount:Number;
        private var _interimSUpdateDate:Date;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is FaxSummary) || (property as FaxSummary).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set faxSummaryId(value:Number):void {
            _faxSummaryId = value;
        }
        [Id]
        public function get faxSummaryId():Number {
            return _faxSummaryId;
        }

        public function set finalHCount(value:Number):void {
            _finalHCount = value;
        }
        public function get finalHCount():Number {
            return _finalHCount;
        }

        public function set finalHUpdateDate(value:Date):void {
            _finalHUpdateDate = value;
        }
        public function get finalHUpdateDate():Date {
            return _finalHUpdateDate;
        }

        public function set finalSCount(value:Number):void {
            _finalSCount = value;
        }
        public function get finalSCount():Number {
            return _finalSCount;
        }

        public function set finalSUpdateDate(value:Date):void {
            _finalSUpdateDate = value;
        }
        public function get finalSUpdateDate():Date {
            return _finalSUpdateDate;
        }

        public function set initHCount(value:Number):void {
            _initHCount = value;
        }
        public function get initHCount():Number {
            return _initHCount;
        }

        public function set initHUpdateDate(value:Date):void {
            _initHUpdateDate = value;
        }
        public function get initHUpdateDate():Date {
            return _initHUpdateDate;
        }

        public function set initSCount(value:Number):void {
            _initSCount = value;
        }
        public function get initSCount():Number {
            return _initSCount;
        }

        public function set initSUpdateDate(value:Date):void {
            _initSUpdateDate = value;
        }
        public function get initSUpdateDate():Date {
            return _initSUpdateDate;
        }

        public function set interimHCount(value:Number):void {
            _interimHCount = value;
        }
        public function get interimHCount():Number {
            return _interimHCount;
        }

        public function set interimHUpdateDate(value:Date):void {
            _interimHUpdateDate = value;
        }
        public function get interimHUpdateDate():Date {
            return _interimHUpdateDate;
        }

        public function set interimSCount(value:Number):void {
            _interimSCount = value;
        }
        public function get interimSCount():Number {
            return _interimSCount;
        }

        public function set interimSUpdateDate(value:Date):void {
            _interimSUpdateDate = value;
        }
        public function get interimSUpdateDate():Date {
            return _interimSUpdateDate;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_faxSummaryId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_faxSummaryId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:FaxSummaryBase = FaxSummaryBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._faxSummaryId, _faxSummaryId, null, this, 'faxSummaryId', function setter(o:*):void{_faxSummaryId = o as Number}, false);
               em.meta_mergeExternal(src._finalHCount, _finalHCount, null, this, 'finalHCount', function setter(o:*):void{_finalHCount = o as Number}, false);
               em.meta_mergeExternal(src._finalHUpdateDate, _finalHUpdateDate, null, this, 'finalHUpdateDate', function setter(o:*):void{_finalHUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._finalSCount, _finalSCount, null, this, 'finalSCount', function setter(o:*):void{_finalSCount = o as Number}, false);
               em.meta_mergeExternal(src._finalSUpdateDate, _finalSUpdateDate, null, this, 'finalSUpdateDate', function setter(o:*):void{_finalSUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._initHCount, _initHCount, null, this, 'initHCount', function setter(o:*):void{_initHCount = o as Number}, false);
               em.meta_mergeExternal(src._initHUpdateDate, _initHUpdateDate, null, this, 'initHUpdateDate', function setter(o:*):void{_initHUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._initSCount, _initSCount, null, this, 'initSCount', function setter(o:*):void{_initSCount = o as Number}, false);
               em.meta_mergeExternal(src._initSUpdateDate, _initSUpdateDate, null, this, 'initSUpdateDate', function setter(o:*):void{_initSUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._interimHCount, _interimHCount, null, this, 'interimHCount', function setter(o:*):void{_interimHCount = o as Number}, false);
               em.meta_mergeExternal(src._interimHUpdateDate, _interimHUpdateDate, null, this, 'interimHUpdateDate', function setter(o:*):void{_interimHUpdateDate = o as Date}, false);
               em.meta_mergeExternal(src._interimSCount, _interimSCount, null, this, 'interimSCount', function setter(o:*):void{_interimSCount = o as Number}, false);
               em.meta_mergeExternal(src._interimSUpdateDate, _interimSUpdateDate, null, this, 'interimSUpdateDate', function setter(o:*):void{_interimSUpdateDate = o as Date}, false);
            }
            else {
               em.meta_mergeExternal(src._faxSummaryId, _faxSummaryId, null, this, 'faxSummaryId', function setter(o:*):void{_faxSummaryId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _faxSummaryId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _finalHCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _finalHUpdateDate = input.readObject() as Date;
                _finalSCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _finalSUpdateDate = input.readObject() as Date;
                _initHCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _initHUpdateDate = input.readObject() as Date;
                _initSCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _initSUpdateDate = input.readObject() as Date;
                _interimHCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _interimHUpdateDate = input.readObject() as Date;
                _interimSCount = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _interimSUpdateDate = input.readObject() as Date;
            }
            else {
                _faxSummaryId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_faxSummaryId is IPropertyHolder) ? IPropertyHolder(_faxSummaryId).object : _faxSummaryId);
                output.writeObject((_finalHCount is IPropertyHolder) ? IPropertyHolder(_finalHCount).object : _finalHCount);
                output.writeObject((_finalHUpdateDate is IPropertyHolder) ? IPropertyHolder(_finalHUpdateDate).object : _finalHUpdateDate);
                output.writeObject((_finalSCount is IPropertyHolder) ? IPropertyHolder(_finalSCount).object : _finalSCount);
                output.writeObject((_finalSUpdateDate is IPropertyHolder) ? IPropertyHolder(_finalSUpdateDate).object : _finalSUpdateDate);
                output.writeObject((_initHCount is IPropertyHolder) ? IPropertyHolder(_initHCount).object : _initHCount);
                output.writeObject((_initHUpdateDate is IPropertyHolder) ? IPropertyHolder(_initHUpdateDate).object : _initHUpdateDate);
                output.writeObject((_initSCount is IPropertyHolder) ? IPropertyHolder(_initSCount).object : _initSCount);
                output.writeObject((_initSUpdateDate is IPropertyHolder) ? IPropertyHolder(_initSUpdateDate).object : _initSUpdateDate);
                output.writeObject((_interimHCount is IPropertyHolder) ? IPropertyHolder(_interimHCount).object : _interimHCount);
                output.writeObject((_interimHUpdateDate is IPropertyHolder) ? IPropertyHolder(_interimHUpdateDate).object : _interimHUpdateDate);
                output.writeObject((_interimSCount is IPropertyHolder) ? IPropertyHolder(_interimSCount).object : _interimSCount);
                output.writeObject((_interimSUpdateDate is IPropertyHolder) ? IPropertyHolder(_interimSUpdateDate).object : _interimSUpdateDate);
            }
            else {
                output.writeObject(_faxSummaryId);
            }
        }
    }
}
