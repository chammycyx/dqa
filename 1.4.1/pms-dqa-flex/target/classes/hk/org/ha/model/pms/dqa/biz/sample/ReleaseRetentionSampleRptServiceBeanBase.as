/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ReleaseRetentionSampleRptServiceBean.as).
 */

package hk.org.ha.model.pms.dqa.biz.sample {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dqa.persistence.UserInfo;
    import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
    import hk.org.ha.model.pms.dqa.vo.sample.ReleaseRetentionSampleMemo;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class ReleaseRetentionSampleRptServiceBeanBase extends Component {

        public function retrieveReleaseRetentionSampleMemo(arg0:UserInfo, arg1:UserInfo, arg2:SampleTestSchedule, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveReleaseRetentionSampleMemo", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveReleaseRetentionSampleMemo", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveReleaseRetentionSampleMemo", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createReleaseRetentionSampleMemo(arg0:ReleaseRetentionSampleMemo, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createReleaseRetentionSampleMemo", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createReleaseRetentionSampleMemo", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("createReleaseRetentionSampleMemo", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generateReleaseRetentionSampleMemo(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generateReleaseRetentionSampleMemo", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generateReleaseRetentionSampleMemo", resultHandler);
            else if (resultHandler == null)
                callProperty("generateReleaseRetentionSampleMemo");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
