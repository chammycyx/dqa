package {

    import mx.logging.Log;
    import mx.logging.targets.TraceTarget;
    
    import hk.org.ha.control.pms.dqa.CompanyContactServiceCtl;
    import hk.org.ha.control.pms.dqa.CompanyListServiceCtl;
    import hk.org.ha.control.pms.dqa.CompanyServiceCtl;
    import hk.org.ha.control.pms.dqa.ContractListServiceCtl;
    import hk.org.ha.control.pms.dqa.ContractServiceCtl;
    import hk.org.ha.control.pms.dqa.DmDrugListServiceCtl;
    import hk.org.ha.control.pms.dqa.DmDrugServiceCtl;
    import hk.org.ha.control.pms.dqa.DocumentServiceCtl;
    import hk.org.ha.control.pms.dqa.DqaMailSenderServiceCtl;
    import hk.org.ha.control.pms.dqa.FileItemServiceCtl;
    import hk.org.ha.control.pms.dqa.LetterContentServiceCtl;
    import hk.org.ha.control.pms.dqa.LetterTemplateListServiceCtl;
    import hk.org.ha.control.pms.dqa.PharmCompanyListServiceCtl;
    import hk.org.ha.control.pms.dqa.ReportServiceCtl;
    import hk.org.ha.control.pms.dqa.SupplierCodeListServiceCtl;
    import hk.org.ha.control.pms.dqa.SupplierContactServiceCtl;
    import hk.org.ha.control.pms.dqa.SupplierListServiceCtl;
    import hk.org.ha.control.pms.dqa.SupplierServiceCtl;
    import hk.org.ha.control.pms.dqa.UserInfoListServiceCtl;
    import hk.org.ha.control.pms.dqa.UserInfoServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaBatchListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaBatchServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaBatchVerListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaChecklistKeyListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaDiscrepancyAnalysisRptServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaDiscrepancyKeyListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaDiscrepancyListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaDiscrepancyRptServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaFileFolderListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaFileFolderServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaInEmailListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaItemServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaLetterTemplateServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaOutstandBatchListServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaProcessRptServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.CoaProcessingDateRptServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.MissingBatchCoaRptServiceCtl;
    import hk.org.ha.control.pms.dqa.coa.MissingCoaItemRptServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.DdrListRptServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.ExclusionTestListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.ExclusionTestServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.InstitutionListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.InstitutionServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.LabListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.ReleaseRetentionSampleRptServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.RiskLevelListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.RiskLevelServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleItemListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleItemServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleMovementListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleMovementServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestFileListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestFileServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestLetterTemplateServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleDrugSampleServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleFileListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleFileServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleLabServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleLetterListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestSchedulePaymentServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.SampleTestScheduleValidateServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.TestFrequencyListServiceCtl;
    import hk.org.ha.control.pms.dqa.sample.TestFrequencyServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.AuditRptServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.BatchSuspListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.BatchSuspServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ConflictOfInterestRptServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.CountryListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.CountryServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.EventLogListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxFinalHospServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxFinalSuppServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxInitHospServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxInitSuppServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxInterimHospServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.FaxInterimSuppServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.PharmProblemDraftSaveServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.PharmProblemFileListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.PharmProblemListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.PharmProblemServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureCatListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureCatServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureParamListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureParamServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureSubCatListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.ProblemNatureSubCatServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.QaProblemListServiceCtl;
    import hk.org.ha.control.pms.dqa.suppperf.QaProblemServiceCtl;
    import hk.org.ha.control.pms.exception.AccessDeniedExceptionHandler;
    import hk.org.ha.control.pms.exception.ConcurrentUpdateExceptionHandler;
    import hk.org.ha.control.pms.exception.DefaultExceptionHandler;
    import hk.org.ha.control.pms.exception.NotLoggedInExceptionHandler;
    import hk.org.ha.control.pms.exception.OptimisticLockExceptionHandler;
    import hk.org.ha.control.pms.exception.SecurityExceptionHandler;
    import hk.org.ha.control.pms.security.LogonCtl;
    import hk.org.ha.control.pms.sys.SystemMessageServiceCtl;
	import hk.org.ha.control.pms.dqa.delaydelivery.DelayDeliveryRptFormListServiceCtl;
    
    import org.granite.tide.ITideModule;
    import org.granite.tide.Tide;
    import org.granite.tide.validators.ValidatorExceptionHandler;
	
		
    [Bindable]
    public class DqaApplicationModule implements ITideModule 
	{
        
        public function init(tide:Tide):void 
		{
            var t:TraceTarget = new TraceTarget();
            t.filters = ["org.granite.*","hk.org.ha.*"];
            Log.addTarget(t);
                        
			tide.addExceptionHandler(NotLoggedInExceptionHandler);
			tide.addExceptionHandler(AccessDeniedExceptionHandler);
			tide.addExceptionHandler(SecurityExceptionHandler);
			tide.addExceptionHandler(ValidatorExceptionHandler);
			tide.addExceptionHandler(ConcurrentUpdateExceptionHandler);
			tide.addExceptionHandler(OptimisticLockExceptionHandler);
			tide.addExceptionHandler(DefaultExceptionHandler);            			
			
            tide.addComponents(
            	[   
					LogonCtl,
					SystemMessageServiceCtl,
					AuditRptServiceCtl,
					CoaBatchListServiceCtl,
					CoaBatchServiceCtl,
					CoaBatchVerListServiceCtl,
					CoaChecklistKeyListServiceCtl,
					CoaDiscrepancyAnalysisRptServiceCtl,
					CoaDiscrepancyKeyListServiceCtl,
					CoaDiscrepancyListServiceCtl,
					CoaDiscrepancyRptServiceCtl,
					CoaFileFolderListServiceCtl,
					CoaFileFolderServiceCtl,
					CoaItemServiceCtl,
					CoaInEmailListServiceCtl,
					CoaLetterTemplateServiceCtl,
					CoaOutstandBatchListServiceCtl,
					CoaProcessRptServiceCtl,
					CoaProcessingDateRptServiceCtl,
					CompanyListServiceCtl,
					ContractListServiceCtl,
					ContractServiceCtl,
					DocumentServiceCtl,
					DqaMailSenderServiceCtl,
					DmDrugListServiceCtl,
					DmDrugServiceCtl,
					ExclusionTestListServiceCtl,
					ExclusionTestServiceCtl,
					FileItemServiceCtl,
					InstitutionListServiceCtl,
					MissingBatchCoaRptServiceCtl,
					MissingCoaItemRptServiceCtl,
					PharmCompanyListServiceCtl,
					ReportServiceCtl,
					RiskLevelListServiceCtl,
					RiskLevelServiceCtl,
					SampleItemListServiceCtl,
					SampleItemServiceCtl,
					SampleTestFileListServiceCtl,
					SampleTestFileServiceCtl,
					SampleTestListServiceCtl,
					SampleTestScheduleFileServiceCtl,
					SampleTestScheduleListServiceCtl,
					SampleTestScheduleServiceCtl,
					SampleTestScheduleValidateServiceCtl,
					SupplierCodeListServiceCtl,
					SupplierContactServiceCtl,
					SupplierListServiceCtl,
					SupplierServiceCtl,
					TestFrequencyListServiceCtl,
					TestFrequencyServiceCtl,
					UserInfoListServiceCtl,
					UserInfoServiceCtl,
					InstitutionServiceCtl,
					LabListServiceCtl,
					SampleMovementListServiceCtl,
					SampleMovementServiceCtl,
					LetterTemplateListServiceCtl,
					SampleTestScheduleLetterListServiceCtl,
					SampleTestSchedulePaymentServiceCtl,
					SampleTestScheduleFileListServiceCtl,
					SampleTestScheduleLabServiceCtl,
					SampleTestScheduleDrugSampleServiceCtl,
					CompanyServiceCtl,
					CompanyContactServiceCtl,
					SampleTestLetterTemplateServiceCtl,
					DdrListRptServiceCtl,
					PharmProblemDraftSaveServiceCtl,
					ProblemNatureParamServiceCtl,
					ProblemNatureCatServiceCtl,
					ProblemNatureSubCatServiceCtl,
					ProblemNatureParamListServiceCtl,
					ProblemNatureCatListServiceCtl,
					ProblemNatureSubCatListServiceCtl,
					PharmProblemServiceCtl,
					CountryServiceCtl,
					CountryListServiceCtl,
					PharmProblemListServiceCtl,
					QaProblemListServiceCtl,
					QaProblemServiceCtl,
					PharmProblemFileListServiceCtl,
					EventLogListServiceCtl,
					BatchSuspServiceCtl,
					BatchSuspListServiceCtl,
					FaxInitHospServiceCtl,
					FaxInterimHospServiceCtl,
					FaxFinalHospServiceCtl,
					FaxInitSuppServiceCtl,
					FaxInterimSuppServiceCtl,
					FaxFinalSuppServiceCtl,
					ReleaseRetentionSampleRptServiceCtl,
					LetterContentServiceCtl,
					ConflictOfInterestRptServiceCtl,
					DelayDeliveryRptFormListServiceCtl
				]);					
        }
    }
}
