package hk.org.ha.control.pms.dqa.sample{
	
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForLabTestByLabTestOrderTypeEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForLabCollectionEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForLabTestEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListLabDetailServiceBean;
	import hk.org.ha.view.pms.dqa.sample.LabCollectionDetailView;
	import hk.org.ha.view.pms.dqa.sample.LabTestDetailView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleLabCtl", restrict="true")]
	public class SampleTestScheduleLabServiceCtl {
		[In]
		public var sampleTestScheduleListLabDetailService:SampleTestScheduleListLabDetailServiceBean;
		
		[In]
		public var labCollectionDetailView:LabCollectionDetailView;
		
		[In]
		public var labTestDetailView:LabTestDetailView;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNum(evt:RetrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumEvent):void{
			
			event = evt.event;
			In(Object(sampleTestScheduleListLabDetailService).errorCode);
			sampleTestScheduleListLabDetailService.retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNum(evt.labCode, evt.testCode, evt.labCollectNum, retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumResult);
		}
		
		public function retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				if (Object(sampleTestScheduleListLabDetailService).errorCode != null){
					labCollectionDetailView.showMessage(Object(sampleTestScheduleListLabDetailService).errorCode);
				}
				else{
					evt.context.dispatchEvent(event);
				}
			}
		}
		
		
		[Observer]
		public function updateSampleTestScheduleForLabCollection(evt:UpdateSampleTestScheduleForLabCollectionEvent):void{
			
			event = evt.event;
			sampleTestScheduleListLabDetailService.updateSampleTestScheduleForLabCollection(evt.sampleTestScheduleList, updateSampleTestScheduleForLabCollectionResult);
		}
		
		public function updateSampleTestScheduleForLabCollectionResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		
		[Observer]
		public function retrieveSampleTestScheduleListForLabTestByLabTestOrderType(evt:RetrieveSampleTestScheduleListForLabTestByLabTestOrderTypeEvent):void{
			
			event = evt.event;
			In(Object(sampleTestScheduleListLabDetailService).errorCode);
			sampleTestScheduleListLabDetailService.retrieveSampleTestScheduleListForLabTestByLabTestOrderType(evt.labCode, evt.testCode, evt.orderType, retrieveSampleTestScheduleListForLabTestByLabTestOrderTypeResult);
		}
		
		public function retrieveSampleTestScheduleListForLabTestByLabTestOrderTypeResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				if (Object(sampleTestScheduleListLabDetailService).errorCode != null){
					labTestDetailView.showMessage(Object(sampleTestScheduleListLabDetailService).errorCode);
				}
				else{
					evt.context.dispatchEvent(event);
				}
			}
		}
		
		[Observer]
		public function updateSampleTestScheduleForLabTest(evt:UpdateSampleTestScheduleForLabTestEvent):void{
			
			event = evt.event;
			sampleTestScheduleListLabDetailService.updateSampleTestScheduleForLabTest(evt.sampleTestScheduleList, updateSampleTestScheduleForLabTestResult);
		}
		
		public function updateSampleTestScheduleForLabTestResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
	}
}
