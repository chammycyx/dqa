package hk.org.ha.event.pms.dqa
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCompanyContactByCompanyCodeContactIdEvent extends AbstractTideEvent
	{	
		private var _companyCode:String;
		private var _contactId:Number;
		
		public function RetrieveCompanyContactByCompanyCodeContactIdEvent(companyCode:String, contactId:Number):void
		{
			super();	
			_companyCode = companyCode;
			_contactId = contactId;
		}
		
		public function get companyCode():String
		{
			return _companyCode;
		}	
		
		public function get contactId():Number
		{
			return _contactId;
		}
	}
}