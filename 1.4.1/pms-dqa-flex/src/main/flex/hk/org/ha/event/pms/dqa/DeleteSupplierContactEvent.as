package hk.org.ha.event.pms.dqa
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
	
	public class DeleteSupplierContactEvent extends AbstractTideEvent 
	{
		
		private var _supplierContact:SupplierContact;
		
		public function DeleteSupplierContactEvent(supplierContact:SupplierContact):void{
			super();
			_supplierContact = supplierContact;
		}
		
		public function get supplierContact():SupplierContact
		{
			return _supplierContact;
		}
		
	}
}