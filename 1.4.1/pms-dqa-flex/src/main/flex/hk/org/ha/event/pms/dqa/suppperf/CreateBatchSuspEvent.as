package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	import mx.collections.ArrayCollection;	
		
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateBatchSuspEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _batchSuspList:ArrayCollection;
		
		
		public function CreateBatchSuspEvent(batchSuspList:ArrayCollection, event:Event=null):void {
			super();
			_event = event;
			_batchSuspList = batchSuspList;			
		}
		
		public function get batchSuspList():ArrayCollection 
		{
			return _batchSuspList;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}