package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSampleTestInventoryPopupEvent extends AbstractTideEvent 
	{	
		
		private var _action:String;
		
		public function ShowSampleTestInventoryPopupEvent(action:String):void
		{
			super();
			_action = action;	
		}
		
		public function get action():String {
			return _action;
		}
		
	}
}