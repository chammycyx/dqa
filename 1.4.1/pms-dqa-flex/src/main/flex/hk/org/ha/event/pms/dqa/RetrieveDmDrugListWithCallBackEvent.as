package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugListWithCallBackEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;

		private var _callBack:Function;
		
		public function RetrieveDmDrugListWithCallBackEvent(itemCode:String,callBack:Function):void {
			super();
			_itemCode = itemCode;
			_callBack = callBack;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}