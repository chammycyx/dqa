package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateSampleItemEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _sampleItem:SampleItem;
		
		
		public function CreateSampleItemEvent(sampleItem:SampleItem, event:Event=null):void {
			super();
			_event = event;
			_sampleItem = sampleItem;			
		}
		
		public function get sampleItem():SampleItem 
		{
			return _sampleItem;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}