package hk.org.ha.event.pms.dqa.coa {
	
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchListForGroupDiscrepancyEvent extends AbstractTideEvent 
	{
		public function RetrieveCoaBatchListForGroupDiscrepancyEvent():void 
		{
			super();		
		}		
	}
}