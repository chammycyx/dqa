package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptCriteria;
	import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaDiscrepancyAnalysisRptListEvent extends AbstractTideEvent 
	{

		private var _coaDiscrepancyAnalysisRptCriteria:CoaDiscrepancyAnalysisRptCriteria;
		
		private var _event:Event;
										
		public function RetrieveCoaDiscrepancyAnalysisRptListEvent(coaDiscrepancyAnalysisRptCriteria:CoaDiscrepancyAnalysisRptCriteria,  event:Event=null):void 
		{
			super();
			_coaDiscrepancyAnalysisRptCriteria = coaDiscrepancyAnalysisRptCriteria;
			_event = event;
		}
		
		public function get coaDiscrepancyAnalysisRptCriteria():CoaDiscrepancyAnalysisRptCriteria{
			return _coaDiscrepancyAnalysisRptCriteria;
		}

		
		public function get event():Event {
			return _event;
		}
	}
}