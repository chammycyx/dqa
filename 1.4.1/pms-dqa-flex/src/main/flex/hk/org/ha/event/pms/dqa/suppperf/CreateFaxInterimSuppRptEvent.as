package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateFaxInterimSuppRptEvent extends AbstractTideEvent 
	{
		
		private var _event:Event;
		private var _qaProblem:QaProblem; 
		private var _eventLog:EventLog;
		private var _refNum:String; 
		private var _reGenFlag:Boolean;
		
		public function CreateFaxInterimSuppRptEvent(qaProblem:QaProblem, 
												  eventLog:EventLog,
												  refNum:String,
												  reGenFlag:Boolean,
												  event:Event=null):void {
			super();
			_event = event;
			_qaProblem = qaProblem; 
			_eventLog = eventLog;
			_refNum = refNum;
			_reGenFlag = reGenFlag;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get eventLog():EventLog {
			return _eventLog;
		}
		
		public function get refNum():String {
			return _refNum;
		}
		
		public function get reGenFlag():Boolean{
			return _reGenFlag;
		}
	}
}