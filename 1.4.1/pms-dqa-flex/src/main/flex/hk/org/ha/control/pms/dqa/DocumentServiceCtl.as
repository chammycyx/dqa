package hk.org.ha.control.pms.dqa
{					
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.OpenFileItemDocumentEvent;
	import hk.org.ha.event.pms.dqa.OpenPdfDocumentEvent;
	import hk.org.ha.event.pms.dqa.OpenSuppPerfFileItemDocumentEvent;
	import hk.org.ha.model.pms.dqa.biz.DocumentServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.ExtendedExternalInterface;
	import hk.org.ha.model.pms.dqa.persistence.FileItem;
	
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("documentServiceCtl", restrict="false")]
	public class DocumentServiceCtl
	{		 
		[In]
		public var documentService:DocumentServiceBean;
		
		[In]
		public var leftWindowsProperties:String;
		
		[In]
		public var rightWindowsProperties:String;
		
		[In]
		public var normalWindowsProperties:String;
		
		private var event:Event;	
		
		private var winName:String;
		
		private var fileItemIn:FileItem;

		[Observer]
		public function openPdfDocument(evt:OpenPdfDocumentEvent):void
		{
			fileItemIn = evt.fileItem;
			winName = evt.winName;
			event = evt.event;					
			documentService.setFileItem( evt.fileItem, openPdfDocumentResult );							
		}
		
		private function openPdfDocumentResult(evt:TideResultEvent):void
		{
			var argStr:String;
			var url:String = "document.seam?actionMethod=document.xhtml%3AdocumentService.openPdfDocument";
			if( winName == "leftWin" ){
				argStr = leftWindowsProperties;
			} else if( winName == "rightWin" ) {					
				argStr = rightWindowsProperties;
			} else {
				argStr = normalWindowsProperties;
			}
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}				
			ExtendedExternalInterface.call("window.open", url, winName, argStr );
			
			if(event != null) {
				evt.context.dispatchEvent(event);
			}						
		}
		
		[Observer]
		public function openFileItemDocument(evt:OpenFileItemDocumentEvent):void
		{   
			fileItemIn = evt.fileItem;
			winName = evt.winName;
			event = evt.event;					
			documentService.setFileItem( evt.fileItem, openFileItemDocumentResult);
		}
		
		private function openFileItemDocumentResult(evt:TideResultEvent):void
		{
			var argStr:String;
			var url:String = "document.seam?actionMethod=document.xhtml%3AdocumentService.openFileItemDocument";
			 
			if( winName == "leftWin" ){
				argStr = leftWindowsProperties;
			} else if( winName == "rightWin" ) {					
				argStr = rightWindowsProperties;
			} else {
				argStr = normalWindowsProperties;
			}
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}			
			
			ExtendedExternalInterface.call("window.open", url, winName, argStr );
			
			if(event != null) {
				evt.context.dispatchEvent(event);
			}						
		}
		
		[Observer]
		public function openSuppPerfFileItemDocument(evt:OpenSuppPerfFileItemDocumentEvent):void
		{   
			fileItemIn = evt.fileItem;
			winName = evt.winName;
			event = evt.event;					
			documentService.setFileItem( evt.fileItem, openFileItemDocumentResult);
		}

	}
}