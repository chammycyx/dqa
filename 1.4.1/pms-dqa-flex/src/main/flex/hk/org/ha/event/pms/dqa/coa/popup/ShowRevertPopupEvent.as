package hk.org.ha.event.pms.dqa.coa.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowRevertPopupEvent extends AbstractTideEvent 
	{				
		public function ShowRevertPopupEvent():void 
		{
			super();		
		}
	}
}