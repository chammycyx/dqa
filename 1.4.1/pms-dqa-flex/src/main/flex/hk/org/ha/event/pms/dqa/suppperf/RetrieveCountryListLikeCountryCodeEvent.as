package hk.org.ha.event.pms.dqa.suppperf
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCountryListLikeCountryCodeEvent extends AbstractTideEvent 
	{	
		
		private var _countryCode:String;
		
		private var _event:Event;
		
		public function RetrieveCountryListLikeCountryCodeEvent(countryCode:String, event:Event=null):void
		{
			super();
			_countryCode = countryCode;
			_event = event;
		}
		
		public function get countryCode():String 
		{
			return _countryCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}
