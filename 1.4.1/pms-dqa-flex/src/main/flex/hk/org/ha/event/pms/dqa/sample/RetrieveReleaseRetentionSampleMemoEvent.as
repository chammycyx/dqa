package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveReleaseRetentionSampleMemoEvent extends AbstractTideEvent 
	{
		private var _defaultCpoContact:UserInfo;
		private var _cpoSignature:UserInfo;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _setContentToReleaseRetentionSamplePopup:Function;
		
		public function RetrieveReleaseRetentionSampleMemoEvent(defaultCpoContact:UserInfo, cpoSignature:UserInfo, schedule:SampleTestSchedule, setContentToReleaseRetentionSamplePopup:Function):void {
			super();
			_defaultCpoContact = defaultCpoContact;
			_cpoSignature = cpoSignature;
			_sampleTestSchedule = schedule;
			_setContentToReleaseRetentionSamplePopup = setContentToReleaseRetentionSamplePopup;
		}
		
		public function get defaultCpoContact():UserInfo
		{
			return _defaultCpoContact;
		}
		
		public function get cpoSignature():UserInfo
		{
			return _cpoSignature;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule
		{
			return _sampleTestSchedule;
		}
		
		public function get setContentToReleaseRetentionSamplePopup():Function
		{
			return _setContentToReleaseRetentionSamplePopup;
		}
	}
}