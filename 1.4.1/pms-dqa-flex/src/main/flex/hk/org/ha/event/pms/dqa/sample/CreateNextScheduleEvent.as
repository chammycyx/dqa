package hk.org.ha.event.pms.dqa.sample
{
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	public class CreateNextScheduleEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function CreateNextScheduleEvent(sampleTestSchedule:SampleTestSchedule, event:Event=null):void
		{
			super();
			_event = event;
			_sampleTestSchedule = sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
	}
}