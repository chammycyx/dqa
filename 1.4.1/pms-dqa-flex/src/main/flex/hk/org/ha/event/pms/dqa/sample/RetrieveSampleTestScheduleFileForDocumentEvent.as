package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleFileForDocumentEvent extends AbstractTideEvent 
	{
		private var _scheduleId:Number;
		
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleFileForDocumentEvent(scheduleId:Number, event:Event=null):void {
			super();
			_scheduleId = scheduleId;
			_event = event;
		}
		
		public function get scheduleId():Number 
		{
			return _scheduleId;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}