package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListByEnquiryDetailCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleEnquiryDetailCriteria:SampleEnquiryDetailCriteria;
		
		public function RetrieveSampleTestScheduleListByEnquiryDetailCriteriaEvent(sampleEnquiryDetailCriteria:SampleEnquiryDetailCriteria, event:Event=null):void {
			super();
			_sampleEnquiryDetailCriteria = sampleEnquiryDetailCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleEnquiryDetailCriteria():SampleEnquiryDetailCriteria{
			return _sampleEnquiryDetailCriteria;
		}
	}
}