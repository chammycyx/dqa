package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyAnalysisRptListEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyRptListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaDiscrepancyAnalysisRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("coaDiscrepancyAnalysisRptServiceCtl", restrict="false")]
	public class CoaDiscrepancyAnalysisRptServiceCtl
	{
		[In]
		public var coaDiscrepancyAnalysisRptService:CoaDiscrepancyAnalysisRptServiceBean;
		
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaDiscrepancyAnalysisRptList(evt:RetrieveCoaDiscrepancyAnalysisRptListEvent):void
		{
			event = evt.event;
			coaDiscrepancyAnalysisRptService.retrieveCoaDiscrepancyAnalysisList(evt.coaDiscrepancyAnalysisRptCriteria, retrieveCoaDiscrepancyAnalysisRptListResult);
		}
		
		public function retrieveCoaDiscrepancyAnalysisRptListResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}	
}