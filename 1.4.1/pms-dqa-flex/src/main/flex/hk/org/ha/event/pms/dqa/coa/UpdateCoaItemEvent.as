package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	
	
	public class UpdateCoaItemEvent extends AbstractTideEvent 
	{
		private var _coaItem:CoaItem;
		
		
		public function UpdateCoaItemEvent(coaItem:CoaItem):void {
			super();
			_coaItem = coaItem;			
		}
		
		public function get coaItem():CoaItem 
		{
			return _coaItem;
		}
		
	}
}