package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.udt.OrderType;
	
	public class RetrieveSampleTestScheduleListForDrugSampleByInstitutionItemEvent extends AbstractTideEvent 
	{
		private var _institutionCode:String;
		private var _itemCode:String;
				
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleListForDrugSampleByInstitutionItemEvent(institutionCode:String, itemCode:String, event:Event=null):void {
			super();
			_institutionCode = institutionCode;
			_itemCode = itemCode;
			_event = event;
		}
		
		public function get institutionCode():String 
		{
			return _institutionCode;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}