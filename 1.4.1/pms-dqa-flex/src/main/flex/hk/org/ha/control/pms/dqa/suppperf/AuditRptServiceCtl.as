package hk.org.ha.control.pms.dqa.suppperf
{		
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveAuditRptExcelEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.AuditRptServiceBean;
	import flash.events.Event;
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("auditRptServiceCtl", restrict="false")]
	public class AuditRptServiceCtl
	{
		
		[In]
		public var auditRptService:AuditRptServiceBean;
		
		[Observer]
		public function retrieveAuditRptExcel(evt:RetrieveAuditRptExcelEvent):void{
			auditRptService.retrieveAuditRpt(evt.auditRptCriteria, 
				function(event:TideResultEvent):void {
					if (evt.callBack != null) {
						evt.callBack();
					}
				}
			);
		}					
	}	
}
