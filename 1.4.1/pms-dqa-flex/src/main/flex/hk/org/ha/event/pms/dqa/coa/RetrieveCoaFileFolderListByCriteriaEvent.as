package hk.org.ha.event.pms.dqa.coa
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.vo.coa.CoaFileFolderCriteria;
	
	
	
	public class RetrieveCoaFileFolderListByCriteriaEvent  extends AbstractTideEvent 
	{	
		
		private var _coaFileFolderCriteria:CoaFileFolderCriteria;
		
		public function RetrieveCoaFileFolderListByCriteriaEvent(coaFileFolderCriteria:CoaFileFolderCriteria):void
		{
			super();
			_coaFileFolderCriteria = coaFileFolderCriteria;
		}
		
		public function get coaFileFolderCriteria():CoaFileFolderCriteria
		{
			return _coaFileFolderCriteria;
		}
		
	}
}