package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSchedulePopupEvent extends AbstractTideEvent 
	{	

		private var _event:Event;
		
		private var _titleState:String;
		
		private var _scheduleStatus:ScheduleStatus;
		
		public function ShowSchedulePopupEvent(titleState:String, scheduleStatus:ScheduleStatus, event:Event=null):void
		{
			super();
			_event = event;	
			_titleState = titleState;
			_scheduleStatus = scheduleStatus;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get titleState():String {
			return _titleState;
		}
		
		public function get scheduleStatus():ScheduleStatus {
			return _scheduleStatus;
		}
	}
}