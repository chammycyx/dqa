package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteDraftDelayDeliveryRptFormEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _delayDeliveryRptFormData:DelayDeliveryRptFormData;
		private var _reason:String;
		
		public function DeleteDraftDelayDeliveryRptFormEvent(delayDeliveryRptFormData:DelayDeliveryRptFormData, 
															 reason:String=null,
															 callBackFunction:Function=null):void {
			super();
			_callBackFunction= callBackFunction;
			_delayDeliveryRptFormData = delayDeliveryRptFormData;
			_reason= reason;
		}
		
		public function get reason():String {
			return _reason;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData
		{
			return _delayDeliveryRptFormData;
		}
		
	}
}