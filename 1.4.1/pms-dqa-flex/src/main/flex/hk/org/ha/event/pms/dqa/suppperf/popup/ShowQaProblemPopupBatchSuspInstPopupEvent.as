package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
	
	public class ShowQaProblemPopupBatchSuspInstPopupEvent extends AbstractTideEvent 
	{	
		private var _qaProblemInstitutionListTemp:ArrayCollection;
		private var _qaProblemInstitutionAllInstitutionFlag:YesNoFlag;
		
		public function ShowQaProblemPopupBatchSuspInstPopupEvent(qaProblemInstitutionListTemp:ArrayCollection, qaProblemInstitutionAllInstitutionFlag:YesNoFlag):void
		{
			super();
			_qaProblemInstitutionListTemp = qaProblemInstitutionListTemp;
			_qaProblemInstitutionAllInstitutionFlag = qaProblemInstitutionAllInstitutionFlag;
		}
		
		public function get qaProblemInstitutionListTemp():ArrayCollection {
			return _qaProblemInstitutionListTemp;
		}
		
		public function get qaProblemInstitutionAllInstitutionFlag():YesNoFlag {
			return _qaProblemInstitutionAllInstitutionFlag;
		}
		
	}
}