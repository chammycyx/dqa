package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUserInfoEvent extends AbstractTideEvent 
	{
		private var _userInfo:UserInfo;
		
		private var _event:Event;
		
		public function RetrieveUserInfoEvent(userInfo:UserInfo, event:Event=null):void {
			super();
			_userInfo = userInfo;
			_event = event;
		}
		
		public function get userInfo():UserInfo 
		{
			return _userInfo;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}