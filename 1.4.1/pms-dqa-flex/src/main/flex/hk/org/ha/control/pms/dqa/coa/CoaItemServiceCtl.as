package hk.org.ha.control.pms.dqa.coa{
	
	import hk.org.ha.event.pms.dqa.coa.AddCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.CreateCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.DeleteCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.RefreshCoaItemMaintViewEvent;
	import hk.org.ha.event.pms.dqa.coa.ResetCoaItemMaintEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForCoaItemMaintEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowCoaItemMaintMessageEvent;
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaItemServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("coaItemServiceCtl", restrict="true")]
	public class CoaItemServiceCtl {
		
		[In]
		public var coaItemService:CoaItemServiceBean;
		
		[In]
		public var coaItem:CoaItem;

		private var callBack:Function;
		
		[Observer]
		public function retrieveCoaItem(evt:RetrieveCoaItemEvent):void {
			coaItemService.retrieveCoaItemByCoaItem(evt.coaItem, retrieveCoaItemResult);
		}
		
		public function retrieveCoaItemResult(evt: TideResultEvent):void {
			if (coaItem !=null){
				dispatchEvent(new RetrieveCoaFileFolderListForCoaItemMaintEvent(coaItem));
			}else {				
				evt.context.dispatchEvent(new RefreshCoaItemMaintViewEvent());
			}
		}
		
		[Observer]
		public function createCoaItem(evt:CreateCoaItemEvent):void{
			callBack = evt.callBackFunc;
			coaItemService.createCoaItem(evt.coaItem, createCoaItemResult);
		}
		
		public function createCoaItemResult(evt:TideResultEvent):void{
			if (callBack != null) {
				callBack(evt.result);
			}
		}
		
		[Observer]
		public function addCoaItem(evt:AddCoaItemEvent):void {
			coaItemService.addCoaItem(addCoaItemResult);	
		}
		
		public function addCoaItemResult(evt: TideResultEvent):void {
		}
		
		[Observer]
		public function updateCoaItem(evt:UpdateCoaItemEvent):void {
			coaItemService.updateCoaItem(updateCoaItemResult);
		}
		
		public function updateCoaItemResult(evt: TideResultEvent):void {
			evt.context.dispatchEvent(new RetrieveCoaFileFolderListForCoaItemMaintEvent(coaItem));
		}
		
		[Observer]
		public function deleteCoaItem(evt:DeleteCoaItemEvent):void {
			coaItemService.deleteCoaItem(evt.coaItem, deleteCoaItemResult);
		}
		
		public function deleteCoaItemResult(evt: TideResultEvent):void {
			evt.context.dispatchEvent(new ResetCoaItemMaintEvent());
		}
	}
}
