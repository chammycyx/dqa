package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSessionTimeoutEvent extends AbstractTideEvent 
	{		
		private var _userId:String
		
		public function RefreshSessionTimeoutEvent(userId:String):void 
		{
			super();
			this._userId = userId;
		}
		
		public function get userId():String
		{
			return _userId;
		}
	}
}