package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	
	public class RetrieveAllSampleItemByItemCodeRecordStatusEvent extends AbstractTideEvent 
	{	
		private var _itemCode:String;
		
		private var _recordStatus:RecordStatus;
		
		private var _screenName:String;
		
		
		public function RetrieveAllSampleItemByItemCodeRecordStatusEvent(itemCode:String, recordStatus:RecordStatus, screenName:String):void{
			super();
			_itemCode = itemCode;
			_recordStatus = recordStatus;
			_screenName = screenName;
			
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get recordStatus():RecordStatus
		{
			return _recordStatus;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}	
	}
}