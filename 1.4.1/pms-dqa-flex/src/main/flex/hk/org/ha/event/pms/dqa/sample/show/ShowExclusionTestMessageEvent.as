package hk.org.ha.event.pms.dqa.sample.show {
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowExclusionTestMessageEvent extends AbstractTideEvent 
	{
		private var _msgCode:String;
		
		
		public function ShowExclusionTestMessageEvent(msgCode:String):void {
			super();
			_msgCode = msgCode;		
		}
		
		public function get msgCode():String 
		{
			return _msgCode;
		}
		
	}
}