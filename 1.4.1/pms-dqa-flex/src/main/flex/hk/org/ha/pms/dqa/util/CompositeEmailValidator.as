package hk.org.ha.pms.dqa.util
{
	import mx.validators.EmailValidator;
	import mx.utils.StringUtil;
	public class CompositeEmailValidator extends EmailValidator
	{
		public function CompositeEmailValidator()
		{
		}
		
		override protected function doValidation( value:Object ):Array
		{
			var result:Array = [];
			
			var emailsString:String = String( value );
			var emails:Array = emailsString.split(";");
			for each (var email:String in emails)
			{
				var emailResult:Array = super.doValidation(StringUtil.trim(email));
				if (emailResult.length > 0)
				{
					result = result.concat( emailResult );
				}
			}
			return result;
		}
	}
	
}

