package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemListLikeCaseNumByItemSMPEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemListByQaProblemCriteriaEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemListLikeCaseNumEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemListByCaseNumInstEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveFaxSummaryRptByQaProblemListEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemListWithLastFiveCaseNumEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RefreshQaProblemListForCaseNumInstEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.QaProblemListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.suppperf.QaProblemListForCaseNumServiceBean;
	import hk.org.ha.model.pms.dqa.biz.suppperf.QaProblemListForLastFiveCaseNumServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("qaProblemListServiceCtl", restrict="true")]
	public class QaProblemListServiceCtl {
		
		[In]       
		public var qaProblemListForCaseNumService:QaProblemListForCaseNumServiceBean;
		
		[In]       
		public var qaProblemListService:QaProblemListServiceBean;
		
		[In]       
		public var qaProblemListForLastFiveCaseNumService:QaProblemListForLastFiveCaseNumServiceBean;
		
		[In]
		public var qaProblemListForCaseNum:ArrayCollection;
		
		private var event:Event;
		private var screenNameIn:String;
		
		
		[Observer]
		public function retrieveQaProblemListLikeCaseNumByItemSMP(evt:RetrieveQaProblemListLikeCaseNumByItemSMPEvent):void{
			event = evt.event;
			qaProblemListForCaseNumService.retrieveQaProblemListLikeCaseNumByItemSMP(evt.caseNum, 
																						evt.itemCode,
																						evt.supplierCode,
																						evt.manufCode,
																						evt.pharmCompanyCode,
																						retrieveQaProblemListLikeCaseNumByItemSMPResult);
		}
		
		public function retrieveQaProblemListLikeCaseNumByItemSMPResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		
		[Observer]
		public function retrieveQaProblemListWithLastFiveCaseNum(evt:RetrieveQaProblemListWithLastFiveCaseNumEvent):void{
			event = evt.event;
			qaProblemListForLastFiveCaseNumService.retrieveQaProblemListWithLastFiveCaseNum(retrieveQaProblemListWithLastFiveCaseNumResult);
		}
		
		public function retrieveQaProblemListWithLastFiveCaseNumResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		
		[Observer]
		public function retrieveQaProblemListByQaProblemCriteria(evt:RetrieveQaProblemListByQaProblemCriteriaEvent):void{
			event = evt.event;
			qaProblemListService.retrieveQaProblemListByQaProblemCriteria(evt.qaProblemCriteria, evt.screenName, retrieveQaProblemListByQaProblemCriteriaResult);
		}
		
		public function retrieveQaProblemListByQaProblemCriteriaResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveQaProblemListLikeCaseNum(evt:RetrieveQaProblemListLikeCaseNumEvent):void{
			event = evt.event;
			qaProblemListForCaseNumService.retrieveQaProblemListLikeCaseNum(evt.caseNum, evt.loginInstCode, evt.pcuIn, evt.institutionIn, retrieveQaProblemListLikeCaseNumResult);
		}
		
		public function retrieveQaProblemListLikeCaseNumResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		
		[Observer]
		public function retrieveQaProblemListByCaseNumInst(evt:RetrieveQaProblemListByCaseNumInstEvent):void{
			event = evt.event;
			screenNameIn = evt.screenNameIn;
			qaProblemListForCaseNumService.retrieveQaProblemListByCaseNumInst(evt.caseNum, evt.loginInstCode, evt.pcuIn, evt.institutionIn, retrieveQaProblemListByCaseNumInstResult);
		}
		
		public function retrieveQaProblemListByCaseNumInstResult(evt:TideResultEvent):void{
			if(screenNameIn=="suppPerfBatchSuspView" || screenNameIn=="suppPerfBatchSuspViewAddPopup")
			{
				evt.context.dispatchEvent(new RefreshQaProblemListForCaseNumInstEvent(screenNameIn, qaProblemListForCaseNum));
			}
			else{
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		[Observer]
		public function retrieveFaxSummaryRptByQaProblemList(evt:RetrieveFaxSummaryRptByQaProblemListEvent):void{
			qaProblemListService.retrieveFaxSummaryRptByQaProblemList(evt.qaProblemList, retrieveFaxSummaryRptByQaProblemListResult);
		}
		
		public function retrieveFaxSummaryRptByQaProblemListResult(evt:TideResultEvent):void{
			if ( event == null){
				var today:Date = new Date();
				var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
				evt.context.dispatchEvent( new GenExcelDocumentEvent("/excelTemplate/faxSummaryRpt.xhtml", "FaxSummaryReport", "FaxSummaryReport_"+fileDate) );
			}else{
				evt.context.dispatchEvent( event );
			}
		}
				
	}
}
