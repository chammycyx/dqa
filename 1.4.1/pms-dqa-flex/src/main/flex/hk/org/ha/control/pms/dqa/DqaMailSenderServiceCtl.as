package hk.org.ha.control.pms.dqa{
			
	import hk.org.ha.event.pms.dqa.SendEmailByLetterTemplateEvent;
	import hk.org.ha.model.pms.dqa.biz.DqaMailSenderServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("dqaMailSenderServiceCtl", restrict="true")]
	public class DqaMailSenderServiceCtl {
		
		
		[In]
		public var dqaMailSenderService:DqaMailSenderServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function sendEmail(evt:SendEmailByLetterTemplateEvent):void 
		{		
			event = evt.event;
			dqaMailSenderService.sendEmail( evt.letterTemplate,sendEmailResult );
		}	
		
		private function sendEmailResult(evt:TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}
		}

	}
}