package hk.org.ha.event.pms.dqa.coa.popup {
	
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFollowUpDatePopupEvent extends AbstractTideEvent 
	{				
		public function ShowFollowUpDatePopupEvent():void 
		{
			super();
		}		
	}
}