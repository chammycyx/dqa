package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _itemCode:String;
		
		private var _recordStatus:RecordStatus;
		
		public function RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent(itemCode:String, recordStatus:RecordStatus, event:Event=null):void {
			super();
			_event = event;
			_itemCode = itemCode;
			_recordStatus = recordStatus;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get recordStatus():RecordStatus
		{
			return _recordStatus;
		}
		
	}
}