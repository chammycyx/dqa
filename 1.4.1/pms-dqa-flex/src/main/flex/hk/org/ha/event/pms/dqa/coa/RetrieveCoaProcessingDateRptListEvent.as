package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptCriteria;
	
	public class RetrieveCoaProcessingDateRptListEvent extends AbstractTideEvent 
	{
		private var _coaProcessingDateRptCriteria:CoaProcessingDateRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveCoaProcessingDateRptListEvent(coaProcessingDateRptCriteria:CoaProcessingDateRptCriteria, event:Event=null):void 
		{
			super();
			_coaProcessingDateRptCriteria =coaProcessingDateRptCriteria;
			_event = event;
		}

		public function get coaProcessingDateRptCriteria():CoaProcessingDateRptCriteria{
			return _coaProcessingDateRptCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}