package hk.org.ha.event.pms.dqa.coa.popup {
	
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEmailPopupEvent extends AbstractTideEvent 
	{		
		private var _discrepancyStatus:DiscrepancyStatus;
		
		public function ShowEmailPopupEvent(discrepancyStatus:DiscrepancyStatus):void 
		{
			super();
			_discrepancyStatus = discrepancyStatus;
		}
		
		public function get discrepancyStatus():DiscrepancyStatus
		{
			return _discrepancyStatus;
		}
	}
}