package hk.org.ha.event.pms.dqa.delaydelivery
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelayDeliveryRptFormConflictOfInterestEvent extends AbstractTideEvent 
	{	
		private var _callBackFunc:Function;
		
		public function RetrieveDelayDeliveryRptFormConflictOfInterestEvent(callBackFuncValue:Function):void
		{
			super();
			_callBackFunc = callBackFuncValue;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}