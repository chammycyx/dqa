package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent extends AbstractTideEvent 
	{
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _cpoOnlyFlag:Boolean;
		private var _event:Event;
		
		public function RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent(sampleTestSchedule:SampleTestSchedule, cpoOnlyFlag:Boolean=false, event:Event=null):void {
			super();
			_sampleTestSchedule = sampleTestSchedule;
			_cpoOnlyFlag= cpoOnlyFlag;
			_event = event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule 
		{
			return _sampleTestSchedule;
		}
		
		public function get cpoOnlyFlag():Boolean 
		{
			return _cpoOnlyFlag;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}