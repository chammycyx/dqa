package hk.org.ha.event.pms.dqa.sample.show {
	
	import org.granite.tide.events.AbstractTideEvent;

	public class ShowRiskLevelMessageEvent extends AbstractTideEvent 
	{
		private var _msgCode:String;
		
		
		public function ShowRiskLevelMessageEvent(msgCode:String):void {
			super();
			_msgCode = msgCode;		
		}
		
		public function get msgCode():String 
		{
			return _msgCode;
		}
		
	}
}