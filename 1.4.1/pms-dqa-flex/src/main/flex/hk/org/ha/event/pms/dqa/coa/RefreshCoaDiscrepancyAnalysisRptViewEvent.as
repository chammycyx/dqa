package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCoaDiscrepancyAnalysisRptViewEvent extends AbstractTideEvent 
	{		
		public function RefreshCoaDiscrepancyAnalysisRptViewEvent():void 
		{
			super();
		}		
	}
}