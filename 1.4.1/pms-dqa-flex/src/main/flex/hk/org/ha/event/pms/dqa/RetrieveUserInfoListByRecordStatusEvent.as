package hk.org.ha.event.pms.dqa{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveUserInfoListByRecordStatusEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _recordStatus:RecordStatus;
		
		public function RetrieveUserInfoListByRecordStatusEvent(recordStatus:RecordStatus, event:Event=null):void {
			super();
			_recordStatus = recordStatus;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get recordStatus():RecordStatus{
			return _recordStatus;
		}
	}
}