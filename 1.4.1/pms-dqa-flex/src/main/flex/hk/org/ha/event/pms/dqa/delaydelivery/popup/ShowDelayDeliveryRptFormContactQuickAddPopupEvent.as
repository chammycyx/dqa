package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDelayDeliveryRptFormContactQuickAddPopupEvent extends AbstractTideEvent 
	{	
		private var _updateDelayDeliveryRptFormContactQuickAdd:Function;
		private var _institution:Institution;
		private var _institutionSameClusterList:ArrayCollection;
		private var _state:String;
		private var _delayDeliveryRptFormContactData: DelayDeliveryRptFormContactData;
		private var _delayDeliveryRptFormContactDataListSelect: ArrayCollection;
		public function ShowDelayDeliveryRptFormContactQuickAddPopupEvent( state:String,
																		   institution:Institution,
																		   institutionSameClusterList:ArrayCollection,
																		   delayDeliveryRptFormContactDataListSelect:ArrayCollection,
																		   delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData=null,
																updateDelayDeliveryRptFormContactQuickAdd:Function=null):void
		{
			super();
			_state=state;
			_institution = institution;
			_delayDeliveryRptFormContactDataListSelect= delayDeliveryRptFormContactDataListSelect;
			_delayDeliveryRptFormContactData= delayDeliveryRptFormContactData;
			
			_institutionSameClusterList=institutionSameClusterList;
			_updateDelayDeliveryRptFormContactQuickAdd = updateDelayDeliveryRptFormContactQuickAdd;
		}
		
		
		public function get delayDeliveryRptFormContactDataListSelect():ArrayCollection {
			return _delayDeliveryRptFormContactDataListSelect;
		}
		
		public function get institution():Institution {
			return _institution;
		}
		
		public function get updateDelayDeliveryRptFormQuickAdd():Function {
			return _updateDelayDeliveryRptFormContactQuickAdd;
		}
		
		public function get state():String {
			return _state;
		}
		public function get delayDeliveryRptFormContactData():DelayDeliveryRptFormContactData {
			return _delayDeliveryRptFormContactData;
		}
		public function get institutionSameClusterList():ArrayCollection {
			return _institutionSameClusterList;
		}
	}
	
}