package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDelayDeliveryRptFormDataPopupFileUploadPopupEvent extends AbstractTideEvent 
	{	
		
		private var _delayDeliveryRptFormData:DelayDeliveryRptFormData;
				
		private var _callBackFunc:Function;
		
		public function ShowDelayDeliveryRptFormDataPopupFileUploadPopupEvent(delayDeliveryRptFormData:DelayDeliveryRptFormData, callBackFuncValue:Function=null):void
		{
			super();
			_delayDeliveryRptFormData = delayDeliveryRptFormData;
			_callBackFunc = callBackFuncValue;
		}
		
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData {
			return _delayDeliveryRptFormData;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
		
	}
}