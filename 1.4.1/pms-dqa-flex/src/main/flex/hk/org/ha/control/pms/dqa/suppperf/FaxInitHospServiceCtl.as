package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInitHospEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInitHospRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxInitHospRptEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.FaxInitHospServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.popup.FaxContactPopupInitHospPopup;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("faxInitHospServiceCtl", restrict="true")]
	public class FaxInitHospServiceCtl {
		
		private var event:Event;
		private var qaProblemIn:QaProblem;
		private var eventLogIn:EventLog;
	
		
		[In]
		public var faxInitHospService:FaxInitHospServiceBean;
		
		public var faxContactPopupInitHospPopupIn:FaxContactPopupInitHospPopup;
		
		[Observer]
		public function createFaxInitHosp(evt:CreateFaxInitHospEvent):void 
		{		
			event = evt.event;
			qaProblemIn = evt.qaProblem;
			eventLogIn = evt.eventLog;
			faxContactPopupInitHospPopupIn = evt.faxContactPopupInitHospPopup;
			In(Object(faxInitHospService).success);
			In(Object(faxInitHospService).errorCode);
			In(Object(faxInitHospService).refNum);
			
			faxInitHospService.createFaxInitHosp(evt.qaProblem, evt.eventLog, evt.faxDetailPharmProblemList, createFaxInitHospResult);		
		}	
		
		private function createFaxInitHospResult(evt:TideResultEvent):void {
			if (Object(faxInitHospService).success ){
				faxContactPopupInitHospPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
				else
				{
					evt.context.dispatchEvent(new CreateFaxInitHospRptEvent(qaProblemIn,
																			eventLogIn,
																			Object(faxInitHospService).refNum,
																			false,
																			new ShowFaxInitHospRptEvent()));
					
				}
			}else{
				if (Object(faxInitHospService).errorCode !=null){
					faxContactPopupInitHospPopupIn.showMessage(Object(faxInitHospService).errorCode );
				}
			}
		}
		
		[Observer]
		public function createFaxInitHospRpt(evt:CreateFaxInitHospRptEvent):void 
		{		
			event = evt.event;
			faxInitHospService.createFaxInitHospRpt(evt.qaProblem, evt.eventLog, evt.refNum, evt.reGenFlag, createFaxInitHospRptResult);		
		}	
		
		private function createFaxInitHospRptResult(evt:TideResultEvent):void {
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}