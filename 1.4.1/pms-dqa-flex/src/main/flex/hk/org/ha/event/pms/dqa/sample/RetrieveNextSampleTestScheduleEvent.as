package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveNextSampleTestScheduleEvent extends AbstractTideEvent 
	{
		private var _sampleTestScheduleFile:SampleTestScheduleFile;
		private var _showNextScheduleEvent:Event;
		private var _confirmEvent:Event;
		
		public function RetrieveNextSampleTestScheduleEvent(sampleTestScheduleFile:SampleTestScheduleFile, confirmEvent:Event=null, showNextScheduleEvent:Event=null):void {
			super();
			_sampleTestScheduleFile = sampleTestScheduleFile;
			_confirmEvent = confirmEvent;
			_showNextScheduleEvent = showNextScheduleEvent;
		}
		
		public function get sampleTestScheduleFile():SampleTestScheduleFile 
		{
			return _sampleTestScheduleFile;
		}
		
		public function get confirmEvent():Event {
			return _confirmEvent;
		}
		
		public function get showNextScheduleEvent():Event {
			return _showNextScheduleEvent;
		}
		
	}
}