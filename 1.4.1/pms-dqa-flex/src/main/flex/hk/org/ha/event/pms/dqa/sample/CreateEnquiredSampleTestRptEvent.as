package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class CreateEnquiredSampleTestRptEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function CreateEnquiredSampleTestRptEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}
