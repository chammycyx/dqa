package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveLabListEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.LabListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("labListServiceCtl", restrict="true")]
	public class LabListServiceCtl {
		
		
		[In]
		public var labListService:LabListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveLabList(evt:RetrieveLabListEvent):void {
			event = evt.event;
			labListService.retrieveLabList(retrieveLabListResult);
		}
		
		public function retrieveLabListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
		
		
	}
}
