package hk.org.ha.control.pms.dqa.sample{
	

	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleForPaymentByScheduleIdEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForPaymentEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestSchedulePaymentServiceCtl", restrict="true")]
	public class SampleTestSchedulePaymentServiceCtl {
		[In]
		public var sampleTestScheduleService:SampleTestScheduleServiceBean;
		

		private var event:Event;
		
		
		[Observer]
		public function retrieveSampleTestScheduleForPaymentByScheduleId(evt:RetrieveSampleTestScheduleForPaymentByScheduleIdEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.retrieveSampleTestScheduleForPaymentByScheduleId(evt.scheduleId, retrieveSampleTestScheduleForPaymentByScheduleIdResult);
		}
		
		public function retrieveSampleTestScheduleForPaymentByScheduleIdResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function updateSampleTestScheduleForPayment(evt:UpdateSampleTestScheduleForPaymentEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.updateSampleTestScheduleForPayment(evt.sampleTestSchedule, evt.actionType, updateSampleTestScheduleForPaymentResult);
		}
		
		public function updateSampleTestScheduleForPaymentResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
	}
}
