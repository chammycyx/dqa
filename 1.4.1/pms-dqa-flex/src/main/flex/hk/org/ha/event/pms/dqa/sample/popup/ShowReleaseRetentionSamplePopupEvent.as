package hk.org.ha.event.pms.dqa.sample.popup
{
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReleaseRetentionSamplePopupEvent extends AbstractTideEvent 
	{	
		private var _defaultCpoContact:UserInfo;
		private var _cpoSignature:UserInfo;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _continueConfirmation:Function;
		
		public function ShowReleaseRetentionSamplePopupEvent(defaultCpoContact:UserInfo, cpoSignature:UserInfo, schedule:SampleTestSchedule, continueConfirmation:Function):void
		{
			super();
			_defaultCpoContact = defaultCpoContact;
			_cpoSignature = cpoSignature;
			_sampleTestSchedule = schedule;
			_continueConfirmation = continueConfirmation;
		}
		
		public function get defaultCpoContact():UserInfo
		{
			return _defaultCpoContact;
		}
		
		public function get cpoSignature():UserInfo
		{
			return _cpoSignature;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule
		{
			return _sampleTestSchedule;
		}
		
		public function get continueConfirmation():Function {
			return _continueConfirmation;
		}
	}
}