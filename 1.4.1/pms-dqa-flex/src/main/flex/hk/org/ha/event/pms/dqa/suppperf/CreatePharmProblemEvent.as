package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.view.pms.dqa.suppperf.popup.PharmProblemPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreatePharmProblemEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _pharmProblem:PharmProblem; 
		private var _pharmBatchNumList:ArrayCollection; 
		private var _countryList:ArrayCollection;
		private var _pharmProblemNatureList:ArrayCollection;
		private var _pharmProblemPopup:PharmProblemPopup;
		private var _pharmProblemFileUploadDataList:ArrayCollection;
		private var _sendEmailBoolean:Boolean;
		
		
		public function CreatePharmProblemEvent(pharmProblem:PharmProblem, 
												  pharmBatchNumList:ArrayCollection, 
												  countryList:ArrayCollection, 
												  pharmProblemNatureList:ArrayCollection, 
												  pharmProblemPopup:PharmProblemPopup,
												  pharmProblemFileUploadDataList:ArrayCollection,
												  sendEmailBoolean:Boolean,
												  event:Event=null):void {
			super();
			_event = event;
			_pharmProblem = pharmProblem; 
			_pharmBatchNumList = pharmBatchNumList; 
			_countryList = countryList;
			_pharmProblemNatureList = pharmProblemNatureList;		
			_pharmProblemPopup = pharmProblemPopup;
			_pharmProblemFileUploadDataList = pharmProblemFileUploadDataList;
			_sendEmailBoolean = sendEmailBoolean;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get pharmBatchNumList():ArrayCollection {
			return _pharmBatchNumList;
		}
		
		public function get countryList():ArrayCollection {
			return _countryList;
		}
		
		public function get pharmProblemNatureList():ArrayCollection {
			return _pharmProblemNatureList;
		}
		
		public function get pharmProblemPopup():PharmProblemPopup {
			return _pharmProblemPopup;
		}
		
		public function get pharmProblemFileUploadDataList():ArrayCollection {
			return _pharmProblemFileUploadDataList;
		}
		
		public function get sendEmailBoolean():Boolean {
			return _sendEmailBoolean;
		}
	}
}