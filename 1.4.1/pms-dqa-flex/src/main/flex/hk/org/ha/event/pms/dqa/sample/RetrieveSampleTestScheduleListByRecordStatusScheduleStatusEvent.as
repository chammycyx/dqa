package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListByRecordStatusScheduleStatusEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _recordStatus:RecordStatus;
		
		private var _scheduleStatus:ScheduleStatus;
		
		public function RetrieveSampleTestScheduleListByRecordStatusScheduleStatusEvent(recordStatus:RecordStatus, scheduleStatus:ScheduleStatus, event:Event=null):void {
			super();
			_recordStatus = recordStatus;
			_scheduleStatus = scheduleStatus;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get recordStatus():RecordStatus{
			return _recordStatus;
		}
		
		public function get scheduleStatus():ScheduleStatus{
			return _scheduleStatus;
		}
	}
}