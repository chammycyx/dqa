package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchVerListByCoaBatchIdEvent extends AbstractTideEvent 
	{
		private var _coaBatchId:Number;
		
		private var _event:Event;
		
		public function RetrieveCoaBatchVerListByCoaBatchIdEvent(coaBatchId:Number, event:Event=null):void 
		{
			super();
			_coaBatchId = coaBatchId;
			_event = event;
		}
		
		public function get coaBatchId():Number
		{
			return _coaBatchId;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}