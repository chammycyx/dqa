package hk.org.ha.control.pms.dqa
{					
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
	import hk.org.ha.model.pms.dqa.biz.ReportServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.ExtendedExternalInterface;
	
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("reportServiceCtl", restrict="false")]
	public class ReportServiceCtl
	{		 
		[In]
		public var reportService:ReportServiceBean;
		
		[In]
		public var reportWindowsProperties:String;
		
		private var event:Event;	
		
		private var winName:String;


		[Observer]
		public function genExcelDocument(evt:GenExcelDocumentEvent):void
		{
			reportService.setReport(evt.reportPath, evt.reportKey, evt.reportFileName, genExcelDocumentResult);
		}
		
		public function genExcelDocumentResult(evt:TideResultEvent):void
		{
			
			var url:String = "genRpt.seam";
			
			var winName:String ="excel_win";
			
			var argStr:String = reportWindowsProperties;
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}							
			ExtendedExternalInterface.call("window.open", url, winName, argStr );  
		}	
	}
}