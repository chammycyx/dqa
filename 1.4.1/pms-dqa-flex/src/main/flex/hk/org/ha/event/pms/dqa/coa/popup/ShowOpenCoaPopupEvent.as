package hk.org.ha.event.pms.dqa.coa.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowOpenCoaPopupEvent extends AbstractTideEvent 
	{	
		public function ShowOpenCoaPopupEvent():void 
		{
			super();
		}		
	}
}