package hk.org.ha.event.pms.dqa.coa {
	
	import flash.utils.*;
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
	import hk.org.ha.view.pms.dqa.coa.popup.FileItemUploadPopup;
	
	public class  UploadCoaFileFolderFilesEvent extends AbstractTideEvent 
	{
		
		private var _data:ByteArray;
		private var _refUploadFileName:String;
		private var _coaFileFolder:CoaFileFolder;
		
		
		public function UploadCoaFileFolderFilesEvent(data:ByteArray, refUploadFileName:String,coaFileFolder:CoaFileFolder):void {
			super();
			_data = data;
			_refUploadFileName = refUploadFileName;
			_coaFileFolder = coaFileFolder;			
		}
		
		public function get data():ByteArray 
		{
			return _data;
		}
		
		public function get refUploadFileName():String 
		{
			return _refUploadFileName;
		}
		
		public function get coaFileFolder():CoaFileFolder
		{
			return _coaFileFolder;
		}
		
	}
}