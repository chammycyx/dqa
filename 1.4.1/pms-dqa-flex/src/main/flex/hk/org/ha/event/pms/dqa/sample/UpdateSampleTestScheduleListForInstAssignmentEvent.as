package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import mx.collections.ListCollectionView
		
		import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSampleTestScheduleListForInstAssignmentEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestScheduleList:ListCollectionView;
		
		public function UpdateSampleTestScheduleListForInstAssignmentEvent(sampleTestScheduleList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_sampleTestScheduleList = sampleTestScheduleList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleList():ListCollectionView {
			return _sampleTestScheduleList;
		}
	}
}