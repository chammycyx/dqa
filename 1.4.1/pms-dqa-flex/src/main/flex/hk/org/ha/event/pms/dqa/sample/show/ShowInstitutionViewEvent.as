package hk.org.ha.event.pms.dqa.sample.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInstitutionViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowInstitutionViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
	
	
}