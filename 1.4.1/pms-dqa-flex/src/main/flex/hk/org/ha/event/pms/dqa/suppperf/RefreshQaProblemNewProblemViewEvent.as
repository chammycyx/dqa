package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQaProblemNewProblemViewEvent extends AbstractTideEvent
	{
		private var _event:Event;
		
		public function RefreshQaProblemNewProblemViewEvent(event:Event=null):void
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
	
}