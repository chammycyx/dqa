package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInstitutionAssignmentEditPopupEvent extends AbstractTideEvent 
	{	
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function ShowInstitutionAssignmentEditPopupEvent(sampleTestSchedule:SampleTestSchedule):void
		{
			super();
			_sampleTestSchedule = sampleTestSchedule;	
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
	}
}