package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrieveManufacturerListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function RetrieveManufacturerListEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}	
	}
}