package hk.org.ha.control.pms.dqa.suppperf{
	
	import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveBatchSuspListByBatchSuspCriteriaEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveBatchSuspQtyRptByBatchSuspListEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.BatchSuspListServiceBean;
	
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("batchSuspListServiceCtl", restrict="true")]
	public class BatchSuspListServiceCtl {
		
		[In]
		public var batchSuspListService:BatchSuspListServiceBean;
		
		private var event:Event;
		
		
		[Observer]
		public function retrieveBatchSuspListByBatchSuspCriteria(evt:RetrieveBatchSuspListByBatchSuspCriteriaEvent):void{
			event = evt.event;
			batchSuspListService.retrieveBatchSuspListByBatchSuspCriteria(evt.batchSuspCriteria, retrieveBatchSuspListByBatchSuspCriteriaResult);
		}
		
		public function retrieveBatchSuspListByBatchSuspCriteriaResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveBatchSuspQtyRptByBatchSuspList(evt:RetrieveBatchSuspQtyRptByBatchSuspListEvent):void{
			batchSuspListService.retrieveBatchSuspQtyRptByBatchSuspList(evt.batchSuspList, retrieveBatchSuspQtyRptByBatchSuspListResult);
		}
		
		public function retrieveBatchSuspQtyRptByBatchSuspListResult(evt:TideResultEvent):void{
			if ( event == null){
				var today:Date = new Date();
				var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
				evt.context.dispatchEvent( new GenExcelDocumentEvent("/excelTemplate/batchSuspQtyRpt.xhtml", "BatchSuspQtyReport", "BatchSuspQtyReport_"+fileDate) );
			}else{
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}