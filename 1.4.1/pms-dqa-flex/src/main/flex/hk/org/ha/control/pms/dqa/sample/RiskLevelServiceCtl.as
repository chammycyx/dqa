package hk.org.ha.control.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.AddRiskLevelEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateRiskLevelEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteRiskLevelEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveRiskLevelByRiskLevelCodeEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateRiskLevelEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowRiskLevelMessageEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.RiskLevelServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
	import hk.org.ha.view.pms.dqa.sample.popup.RiskLevelPopup;
	import hk.org.ha.view.pms.dqa.sample.RiskLevelMaintView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("riskLevelServiceCtl", restrict="true")]
	public class RiskLevelServiceCtl {
		
		[In]       
		public var riskLevelService:RiskLevelServiceBean;
		
		[In]
		public var riskLevel:RiskLevel;
		
		[In]
		public var riskLevelPopup:RiskLevelPopup;
		
		[In]
		public var riskLevelMaintView:RiskLevelMaintView;
		
		private var event:Event;
		
		[Observer]
		public function retrieveRiskLevelByRiskLevelCode(evt:RetrieveRiskLevelByRiskLevelCodeEvent):void {
			event = evt.event;
			riskLevelService.retrieveRiskLevelByRiskLevelCode(evt.riskLevelCode, retrieveRiskLevelResult);
		}
		
		public function retrieveRiskLevelResult(evt: TideResultEvent):void {
			if ( event != null && riskLevel !=null){
				evt.context.dispatchEvent( event );
			}		
		}
		
		[Observer]
		public function addRiskLevel(evt:AddRiskLevelEvent):void{
			event = evt.event;
			riskLevelService.addRiskLevel(addRiskLevelResult);
		}
		
		public function addRiskLevelResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function createRiskLevel(evt:CreateRiskLevelEvent):void{
			event = evt.event;
			In(Object(riskLevelService).success);
			In(Object(riskLevelService).errorCode);
			riskLevelService.createRiskLevel( createRiskLevelResult );
		}
		
		public function createRiskLevelResult(evt:TideResultEvent):void{
			if (Object(riskLevelService).success ){
				riskLevelPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(riskLevelService).errorCode !=null){
					riskLevelPopup.showMessage(Object(riskLevelService).errorCode );
				}
			}
		}
	
		[Observer]
		public function updateRiskLevel(evt:UpdateRiskLevelEvent):void{
			event = evt.event;
			In(Object(riskLevelService).success);
			riskLevelService.updateRiskLevel(updateRiskLevelResult);
		}
		
		public function updateRiskLevelResult(evt:TideResultEvent):void{
			if (Object(riskLevelService).success ){
				riskLevelPopup.closeWin();
				
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		[Observer]
		public function deleteRiskLevel(evt:DeleteRiskLevelEvent):void{
			event = evt.event;
			In(Object(riskLevelService).success);
			In(Object(riskLevelService).errorCode);
			riskLevelService.deleteRiskLevel(evt.riskLevel, deleteRiskLevelResult);
		}
	
		public function deleteRiskLevelResult(evt:TideResultEvent):void{

			if (!Object(riskLevelService).success ){
				if (Object(riskLevelService).errorCode !=null){
					riskLevelMaintView.showMessage(Object(riskLevelService).errorCode );
					//evt.context.dispatchEvent(new ShowRiskLevelMessageEvent("0025"));
				}
			}else if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}	
	}
}
