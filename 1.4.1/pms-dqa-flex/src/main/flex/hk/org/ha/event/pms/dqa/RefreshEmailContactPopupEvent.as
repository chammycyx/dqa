package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshEmailContactPopupEvent extends AbstractTideEvent 
	{	
		
		private var _event:Event;
		
		public function RefreshEmailContactPopupEvent(event:Event=null):void
		{
			super();
			_event = event;
		}
	
		public function get event():Event
		{
			return _event;
		}
	}
}
