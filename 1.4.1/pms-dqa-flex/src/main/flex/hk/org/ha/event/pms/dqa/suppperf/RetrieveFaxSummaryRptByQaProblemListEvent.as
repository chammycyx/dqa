package hk.org.ha.event.pms.dqa.suppperf {
	
	import mx.collections.ArrayCollection;
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFaxSummaryRptByQaProblemListEvent extends AbstractTideEvent 
	{
		private var _qaProblemList:ArrayCollection;
		
		public function RetrieveFaxSummaryRptByQaProblemListEvent(qaProblemList:ArrayCollection):void 
		{
			super();
			_qaProblemList = qaProblemList;
		}
		
		public function get qaProblemList():ArrayCollection {
			return _qaProblemList;
		}
	}
}