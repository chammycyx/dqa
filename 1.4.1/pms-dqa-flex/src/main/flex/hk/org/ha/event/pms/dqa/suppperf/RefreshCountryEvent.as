package hk.org.ha.event.pms.dqa.suppperf {
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCountryEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		private var _country:Country;
		
		
		
		public function RefreshCountryEvent(country:Country, screenName:String):void {
			super();
			_screenName = screenName;
			_country = country;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get country():Country
		{
			return _country;
		}
		
	
	}
}