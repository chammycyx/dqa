package hk.org.ha.event.pms.dqa.coa {
	
	import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyCriteria;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaDiscrepancyListForReportDiscrepancyEvent extends AbstractTideEvent 
	{		
		
		private var _coaDiscrepancyCriteria:CoaDiscrepancyCriteria;
		
		public function RetrieveCoaDiscrepancyListForReportDiscrepancyEvent( coaDiscrepancyCriteria:CoaDiscrepancyCriteria ):void 
		{
			super();
			_coaDiscrepancyCriteria = coaDiscrepancyCriteria;
		}				
		
		public function get coaDiscrepancyCriteria():CoaDiscrepancyCriteria
		{
			return _coaDiscrepancyCriteria;
		}
	}
}