package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
	
	import flash.events.Event;
	
	public class UpdateSampleMovementEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleMovement:SampleMovement;
		
		public function UpdateSampleMovementEvent(sampleMovement:SampleMovement, event:Event=null):void{
			super();
			
			_event = event;
			_sampleMovement = sampleMovement;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleMovement():SampleMovement {
			return _sampleMovement;
		}
	}
}