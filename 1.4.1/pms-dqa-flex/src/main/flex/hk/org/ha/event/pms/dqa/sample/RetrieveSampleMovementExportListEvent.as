package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;

	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleMovementExportListEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleMovementKeyExportList:ListCollectionView;
		
		public function RetrieveSampleMovementExportListEvent(sampleMovementKeyExportList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_sampleMovementKeyExportList = sampleMovementKeyExportList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleMovementKeyExportList():ListCollectionView{
			return _sampleMovementKeyExportList;
		}
	}
}
