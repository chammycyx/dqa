package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInstitutionAssignmentPopupEvent extends AbstractTideEvent 
	{	

		private var _event:Event;
		
		public function ShowInstitutionAssignmentPopupEvent(event:Event=null):void
		{
			super();
			_event = event;	
		}
		
		public function get event():Event {
			return _event;
		}
	}
}