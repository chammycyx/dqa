package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
	
	public class RetrieveQaProblemListLikeCaseNumEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _caseNum:String;
		private var _loginInstCode:String;
		private var _pcuIn:Institution;
		private var _institutionIn:Institution;
		
	
		public function RetrieveQaProblemListLikeCaseNumEvent(caseNum:String,
																loginInstCode:String,
																pcuIn:Institution,
																institutionIn:Institution,
																event:Event=null):void {
			super();
			_event = event;
			_caseNum = caseNum;
			_loginInstCode = loginInstCode;
			_pcuIn = pcuIn;
			_institutionIn = institutionIn;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
		
		public function get loginInstCode():String
		{
			return _loginInstCode;
		}
		
		public function get pcuIn():Institution
		{
			return _pcuIn;
		}
		
		public function get institutionIn():Institution
		{
			return _institutionIn;
		}
	}
}