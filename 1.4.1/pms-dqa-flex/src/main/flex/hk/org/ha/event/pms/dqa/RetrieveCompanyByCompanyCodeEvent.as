package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
		
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RetrieveCompanyByCompanyCodeEvent extends AbstractTideEvent
	{
		private var _companyCode:String;
					
		public function RetrieveCompanyByCompanyCodeEvent(companyCode:String):void
		{
			super();
			_companyCode = companyCode;
		}
				
		public function get companyCode():String 
		{
			return _companyCode;
		}
	}
}