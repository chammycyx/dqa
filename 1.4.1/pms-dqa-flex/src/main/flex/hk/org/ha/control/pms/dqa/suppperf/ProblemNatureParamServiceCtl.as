package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureParamEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateProblemNatureParamEvent;
	import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureParamEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureParamByProblemNatureParamIdEvent;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateProblemNatureParamEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureParamServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
	import hk.org.ha.view.pms.dqa.suppperf.popup.NatureOfProblemParameterPopup;
	import hk.org.ha.view.pms.dqa.suppperf.NatureOfProblemMaintView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureParamServiceCtl", restrict="true")]
	public class ProblemNatureParamServiceCtl {
		
		[In]       
		public var problemNatureParamService:ProblemNatureParamServiceBean;
		
		[In]
		public var problemNatureParam:ProblemNatureParam;
		
		[In]
		public var natureOfProblemParameterPopup:NatureOfProblemParameterPopup;
		
		[In]
		public var natureOfProblemMaintView:NatureOfProblemMaintView;
		
		private var event:Event;
		
		
		
		[Observer]
		public function addProblemNatureParam(evt:AddProblemNatureParamEvent):void{
			event = evt.event;
			problemNatureParamService.addProblemNatureParam(addProblemNatureParamResult);
		}
		
		public function addProblemNatureParamResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		
		[Observer]
		public function createProblemNatureParam(evt:CreateProblemNatureParamEvent):void{
			event = evt.event;
			In(Object(problemNatureParamService).success);
			In(Object(problemNatureParamService).errorCode);
			problemNatureParamService.createProblemNatureParam(evt.problemNatureParam, createProblemNatureParamResult);
		}
		
		public function createProblemNatureParamResult(evt:TideResultEvent):void{
			if (Object(problemNatureParamService).success ){
				natureOfProblemParameterPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureParamService).errorCode !=null){
					natureOfProblemParameterPopup.showMessage(Object(problemNatureParamService).errorCode );
				}
			}
		}
		
		[Observer]
		public function updateProblemNatureParam(evt:UpdateProblemNatureParamEvent):void{
			event = evt.event;
			In(Object(problemNatureParamService).success);
			In(Object(problemNatureParamService).errorCode);
			problemNatureParamService.updateProblemNatureParam(evt.problemNatureParam, updateProblemNatureParamResult);
		}
		
		public function updateProblemNatureParamResult(evt:TideResultEvent):void{
			if (Object(problemNatureParamService).success ){
				natureOfProblemParameterPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureParamService).errorCode !=null){
					natureOfProblemParameterPopup.showMessage(Object(problemNatureParamService).errorCode );
				}
			}
		}
		
		[Observer]
		public function deleteProblemNatureParam(evt:DeleteProblemNatureParamEvent):void{
			event = evt.event;
			problemNatureParamService.deleteProblemNatureParam(evt.problemNatureParam, deleteProblemNatureParamResult);
		}
		
		public function deleteProblemNatureParamResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveProblemNatureParamByProblemNatureParamId(evt:RetrieveProblemNatureParamByProblemNatureParamIdEvent):void{
			event = evt.event;
			problemNatureParamService.retrieveProblemNatureParamByProblemNatureParamId(evt.problemNatureParam, retrieveProblemNatureParamByProblemNatureParamIdResult);
		}
		
		public function retrieveProblemNatureParamByProblemNatureParamIdResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
