package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.Contract;
	
	public class RetrieveMspContactListByContractEvent extends AbstractTideEvent 
	{
		private var _contract:Contract;
		
		private var _event:Event;
		
		public function RetrieveMspContactListByContractEvent(contract:Contract, event:Event=null):void {
			super();
			_contract = contract;
			_event = event;
		}
		
		public function get contract():Contract 
		{
			return _contract;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}