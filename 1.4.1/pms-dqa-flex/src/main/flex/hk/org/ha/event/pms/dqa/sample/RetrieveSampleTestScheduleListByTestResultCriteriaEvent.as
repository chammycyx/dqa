package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleTestResultCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListByTestResultCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestResultCriteria:SampleTestResultCriteria;
		
		public function RetrieveSampleTestScheduleListByTestResultCriteriaEvent(sampleTestResultCriteria:SampleTestResultCriteria, event:Event=null):void {
			super();
			_sampleTestResultCriteria = sampleTestResultCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestResultCriteria():SampleTestResultCriteria{
			return _sampleTestResultCriteria;
		}
	}
}