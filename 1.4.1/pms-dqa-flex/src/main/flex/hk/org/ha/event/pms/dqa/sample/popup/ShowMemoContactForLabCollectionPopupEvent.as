package hk.org.ha.event.pms.dqa.sample.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowMemoContactForLabCollectionPopupEvent extends AbstractTideEvent 
	{	
		private var _goToReleaseRetentionSample:Function;
		
		public function ShowMemoContactForLabCollectionPopupEvent(goToReleaseRetentionSample:Function):void
		{
			super();
			_goToReleaseRetentionSample = goToReleaseRetentionSample;
		}
		
		public function get goToReleaseRetentionSample():Function {
			return _goToReleaseRetentionSample;
		}
	}
}