package hk.org.ha.event.pms.dqa.sample {
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveSampleItemByItemCodeRecordStatusEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _recordStatus:RecordStatus;
		
		public function RetrieveSampleItemByItemCodeRecordStatusEvent(itemCode:String, recordStatus:RecordStatus):void {
			super();
			_itemCode = itemCode;
			_recordStatus = recordStatus;
		}
	
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get recordStatus():RecordStatus
		{
			return _recordStatus;
		}
		
	}
}