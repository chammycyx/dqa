package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.view.pms.dqa.suppperf.popup.PharmProblemPopup;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateAndSubmitPharmProblemEvent extends AbstractTideEvent 
	{
		private var _callBack:Function;
		private var _pharmProblem:PharmProblem; 
		private var _pharmBatchNumList:ListCollectionView; 
		private var _countryList:ListCollectionView;
		private var _pharmProblemNatureList:ListCollectionView;
		private var _pharmProblemPopup:PharmProblemPopup;
		private var _pharmProblemFileUploadDataList:ListCollectionView;
		private var _sendEmailBoolean:Boolean;
		
		
		public function CreateAndSubmitPharmProblemEvent(pharmProblem:PharmProblem, 
														 pharmBatchNumList:ListCollectionView, 
														 countryList:ListCollectionView, 
														 pharmProblemNatureList:ListCollectionView, 
														 pharmProblemFileUploadDataList:ListCollectionView,
														 sendEmailBoolean:Boolean,
														 callBack:Function):void {
			super();
			_callBack = callBack;
			_pharmProblem = pharmProblem; 
			_pharmBatchNumList = pharmBatchNumList; 
			_countryList = countryList;
			_pharmProblemNatureList = pharmProblemNatureList;		
			_pharmProblemFileUploadDataList = pharmProblemFileUploadDataList;
			_sendEmailBoolean = sendEmailBoolean;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get pharmBatchNumList():ListCollectionView {
			return _pharmBatchNumList;
		}
		
		public function get countryList():ListCollectionView {
			return _countryList;
		}
		
		public function get pharmProblemNatureList():ListCollectionView {
			return _pharmProblemNatureList;
		}
		
		public function get pharmProblemFileUploadDataList():ListCollectionView {
			return _pharmProblemFileUploadDataList;
		}
		
		public function get sendEmailBoolean():Boolean {
			return _sendEmailBoolean;
		}
	}
}