package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	import mx.collections.ListCollectionView;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEnquiryDetailExportPopupEvent extends AbstractTideEvent 
	{	
		private var _sampleTestScheduleKeyExportList:ListCollectionView;
		private var _event:Event;
		
		public function ShowEnquiryDetailExportPopupEvent(sampleTestScheduleKeyExportList:ListCollectionView, event:Event=null):void
		{
			super();
			_sampleTestScheduleKeyExportList = sampleTestScheduleKeyExportList;
			_event = event;	
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleKeyExportList():ListCollectionView {
			return _sampleTestScheduleKeyExportList;
		}
		
	}
}