package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearSampleItemListEvent extends AbstractTideEvent 
	{
		public function ClearSampleItemListEvent():void {
			super();
		}
	}
}