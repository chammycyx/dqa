package hk.org.ha.event.pms.dqa.coa {		
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateCoaBatchStatusEvent extends AbstractTideEvent 
	{		
		private var _coaBatch:CoaBatch;
		
		private var _coaStatus:CoaStatus;
		
		private var _discrepancyStatus:DiscrepancyStatus;
		
		private var _event:Event;
		
		private var _callBackFunc:Function;
		
		public function UpdateCoaBatchStatusEvent(coaBatch:CoaBatch, coaStatus:CoaStatus, discrepancyStatus:DiscrepancyStatus, event:Event = null, func:Function = null):void 
		{
			super();
			_coaBatch = coaBatch;
			_coaStatus = coaStatus;
			_discrepancyStatus = discrepancyStatus;
			_event = event;
			_callBackFunc = func;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
		
		public function get coaStatus():CoaStatus {
			return _coaStatus;
		}
		
		public function get discrepancyStatus():DiscrepancyStatus {
			return _discrepancyStatus;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
	}
}