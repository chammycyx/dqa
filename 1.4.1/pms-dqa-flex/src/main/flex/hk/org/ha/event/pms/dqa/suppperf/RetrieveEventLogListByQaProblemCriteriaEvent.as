package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;
	
	public class RetrieveEventLogListByQaProblemCriteriaEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblemCriteria:QaProblemCriteria;
		
		public function RetrieveEventLogListByQaProblemCriteriaEvent(qaProblemCriteria:QaProblemCriteria, event:Event=null):void {
			super();
			_event = event;
			_qaProblemCriteria = qaProblemCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblemCriteria():QaProblemCriteria
		{
			return _qaProblemCriteria;
		}
	}
}