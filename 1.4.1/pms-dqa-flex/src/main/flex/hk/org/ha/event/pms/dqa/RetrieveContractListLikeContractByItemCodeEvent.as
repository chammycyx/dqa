package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveContractListLikeContractByItemCodeEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _contractSuffix:String;
		private var _itemCode:String;
		private var _screenName:String;
		private var _event:Event;
		
		public function RetrieveContractListLikeContractByItemCodeEvent(contractNum:String, contractSuffix:String, itemCode:String, screenName:String, event:Event=null):void {
			super();
			_contractNum = contractNum;
			_contractSuffix = contractSuffix;
			_itemCode = itemCode;
			_screenName = screenName;
			_event = event;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get contractSuffix():String 
		{
			return _contractSuffix;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}