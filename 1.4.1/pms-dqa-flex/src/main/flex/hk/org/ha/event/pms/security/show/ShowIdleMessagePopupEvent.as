package hk.org.ha.event.pms.security.show {
	
	import mx.managers.ISystemManager;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowIdleMessagePopupEvent extends AbstractTideEvent 
	{		
		private var _okHandler:Function;
		
		public function ShowIdleMessagePopupEvent(okHandler:Function):void
		{
			super();
			_okHandler = okHandler;
		}
		
		public function get okHandler():Function
		{
			return _okHandler;
		}
	}
}