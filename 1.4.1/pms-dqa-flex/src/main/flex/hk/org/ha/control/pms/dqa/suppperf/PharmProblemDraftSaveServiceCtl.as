package hk.org.ha.control.pms.dqa.suppperf{
	
	
	import hk.org.ha.event.pms.dqa.suppperf.AddPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateAndSubmitPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateUpdateDraftPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrievePharmProblemFormDataByPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowPharmProblemFormDataPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemDraftSaveServiceBean;
	import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFormData;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pharmProblemDraftSaveServiceCtl", restrict="true")]
	public class PharmProblemDraftSaveServiceCtl {
		[In]
		public var pharmProblemDraftSaveService:PharmProblemDraftSaveServiceBean;
		
		[Observer]
		public function retrievePharmProblemFormData(evt:RetrievePharmProblemFormDataByPharmProblemEvent):void {
			pharmProblemDraftSaveService.retrievePharmProblemFormData(evt.pharmProblem, 
				function(tideResultEvent:TideResultEvent):void {
					if (evt.callBackEvent is ShowPharmProblemFormDataPopupEvent) {
						ShowPharmProblemFormDataPopupEvent(evt.callBackEvent).pharmProblemFormData = tideResultEvent.result as PharmProblemFormData;
						tideResultEvent.context.dispatchEvent(evt.callBackEvent);
					}
				}
			);
		}
		
		[Observer]
		public function addPharmProblem(evt:AddPharmProblemEvent):void {			
			pharmProblemDraftSaveService.createPharmProblemFormData(function(tideResultEvent:TideResultEvent):void {
				if (evt.callBackEvent != null && evt.callBackEvent is ShowPharmProblemFormDataPopupEvent) {
					var showPharmProblemFormDataPopupEvent:ShowPharmProblemFormDataPopupEvent = evt.callBackEvent as ShowPharmProblemFormDataPopupEvent;
					showPharmProblemFormDataPopupEvent.pharmProblemFormData = tideResultEvent.result as PharmProblemFormData;
					tideResultEvent.context.dispatchEvent(evt.callBackEvent);
				}
			});
		}
		
		[Observer]
		public function createUpdateDraftPharmProblem(evt:CreateUpdateDraftPharmProblemEvent):void{
			pharmProblemDraftSaveService.createUpdateDraftPharmProblem(evt.pharmProblem, 
				evt.pharmBatchNumList, 
				evt.countryList, 
				evt.pharmProblemNatureList,
				evt.pharmProblemFileUploadDataList,
				function(tideResultEvent:TideResultEvent):void {
					if (tideResultEvent.result == true) {
						evt.callBack();
					}
				}
			);
		}
		
		public function createUpdateDraftPharmProblemResult(evt:TideResultEvent):void{
		}
		
		[Observer]
		public function createAndSubmitPharmProblem(evt:CreateAndSubmitPharmProblemEvent):void {
			pharmProblemDraftSaveService.createPharmProblem(
				evt.pharmProblem, 
				evt.pharmBatchNumList, 
				evt.countryList, 
				evt.pharmProblemNatureList,
				evt.pharmProblemFileUploadDataList,
				evt.sendEmailBoolean,
				function(tideResultEvent:TideResultEvent):void {
					if (tideResultEvent.result == true) {
						evt.callBack();
					}
				}
			);
		}
	}
}
