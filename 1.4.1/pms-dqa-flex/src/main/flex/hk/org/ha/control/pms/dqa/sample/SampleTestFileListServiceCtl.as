package hk.org.ha.control.pms.dqa.sample{
	import hk.org.ha.event.pms.dqa.sample.ClearSampleTestFileListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListBySampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListByCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListBySampleTestFileCatEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListBySampleTestFileCatContractEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListForSchedConfirmEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestFileListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("sampleTestFileListServiceCtl", restrict="true")]
	public class SampleTestFileListServiceCtl {
		
		[In]
		public var sampleTestFileListService:SampleTestFileListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retreiveSampleTestFileList(evt:RetrieveSampleTestFileListByCriteriaEvent):void{
			event = evt.event;
			sampleTestFileListService.retrieveSampleTestFileList(evt.sampleTestFileCriteria, retreiveSampleTestFileListResult);
		}
		
		public function retreiveSampleTestFileListResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveSampleTestFileListForSchedConfirm(evt:RetrieveSampleTestFileListForSchedConfirmEvent):void{
			event = evt.event;
			sampleTestFileListService.retrieveSampleTestFileListForSchedConfirm(evt.itemCode, evt.supplierCode, evt.manufCode, evt.pharmCode, 
				retrieveSampleTestFileListForSchedConfirmResult);
		}
		
		public function retrieveSampleTestFileListForSchedConfirmResult(evt:TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveSampleTestFileListBySampleTestFileCat(evt:RetrieveSampleTestFileListBySampleTestFileCatEvent):void{
			event = evt.event;
			sampleTestFileListService.retrieveSampleTestFileListBySampleTestFileCat(evt.sampleTestFileCat, retrieveSampleTestFileListBySampleTestFileCatResult);
		}
		
		public function retrieveSampleTestFileListBySampleTestFileCatResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveSampleTestFileListBySampleTestFileCatContract(evt:RetrieveSampleTestFileListBySampleTestFileCatContractEvent):void{
			event = evt.event;
			sampleTestFileListService.retrieveSampleTestFileListBySampleTestFileCatContract(evt.sampleTestFileCat, evt.contract, retrieveSampleTestFileListBySampleTestFileCatContractResult);
		}
		
		public function retrieveSampleTestFileListBySampleTestFileCatContractResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveSampleTestFileListBySampleTestSchedule(evt:RetrieveSampleTestFileListBySampleTestScheduleEvent):void{
			event = evt.event;
			sampleTestFileListService.retrieveSampleTestFileListBySampleTestSchedule(evt.sampleTestSchedule, retrieveSampleTestFileListBySampleTestScheduleResult); 
		}
		
		public function retrieveSampleTestFileListBySampleTestScheduleResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
	}
}