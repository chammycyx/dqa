package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.MissingBatchCoaDateType;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveMissingBatchCoaRptListEvent extends AbstractTideEvent 
	{
		private var _fromDate:Date;
		private var _toDate:Date;
		private var _dateType:MissingBatchCoaDateType;
		private var _event:Event;
		
		public function RetrieveMissingBatchCoaRptListEvent(dateType:MissingBatchCoaDateType, fromDate:Date=null, toDate:Date=null, event:Event=null):void 
		{
			super();
			_dateType = dateType;
			_fromDate = fromDate;
			_toDate = toDate;
			_event = event;
		}

		public function get dateType():MissingBatchCoaDateType {
			return _dateType;
		}
		
		public function get fromDate():Date {
			return _fromDate;
		}
		
		public function get toDate():Date {
			return _toDate;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}