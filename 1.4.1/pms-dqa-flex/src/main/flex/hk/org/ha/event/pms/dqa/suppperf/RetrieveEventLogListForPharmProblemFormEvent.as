package hk.org.ha.event.pms.dqa.suppperf {
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveEventLogListForPharmProblemFormEvent extends AbstractTideEvent 
	{	
		private var _callBackFunc:Function;
		
		private var _qaProblemCriteria:QaProblemCriteria;
		
		public function RetrieveEventLogListForPharmProblemFormEvent(qaProblemCriteria:QaProblemCriteria, func:Function=null):void {
			super();
			_callBackFunc = func;
			_qaProblemCriteria = qaProblemCriteria;
		}
		
		public function get qaProblemCriteria():QaProblemCriteria {
			return _qaProblemCriteria;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}