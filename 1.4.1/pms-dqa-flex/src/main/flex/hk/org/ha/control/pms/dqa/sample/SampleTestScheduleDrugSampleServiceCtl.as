package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForDrugSampleByInstitutionItemEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForDrugSampleEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListDrugSampleDetailServiceBean;
	import hk.org.ha.view.pms.dqa.sample.DrugSampleDetailView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleDrugSampleCtl", restrict="true")]
	public class SampleTestScheduleDrugSampleServiceCtl {
		[In]
		public var sampleTestScheduleListDrugSampleDetailService:SampleTestScheduleListDrugSampleDetailServiceBean;
		
		[In]
		public var drugSampleDetailView:DrugSampleDetailView;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleTestScheduleListForDrugSampleByInstitutionItem(evt:RetrieveSampleTestScheduleListForDrugSampleByInstitutionItemEvent):void{
			
			event = evt.event;
			In(Object(sampleTestScheduleListDrugSampleDetailService).errorCode);
			sampleTestScheduleListDrugSampleDetailService.retrieveSampleTestScheduleListForDrugSampleByInstitutionItem(evt.institutionCode, evt.itemCode, retrieveSampleTestScheduleListForDrugSampleByInstitutionItemResult);
		}
		
		public function retrieveSampleTestScheduleListForDrugSampleByInstitutionItemResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				if (Object(sampleTestScheduleListDrugSampleDetailService).errorCode != null){
					drugSampleDetailView.showMessage(Object(sampleTestScheduleListDrugSampleDetailService).errorCode);
				}
				else{
					evt.context.dispatchEvent(event);
				}
			}
		}
		
		[Observer]
		public function updateSampleTestScheduleForDrugSample(evt:UpdateSampleTestScheduleForDrugSampleEvent):void{
			
			event = evt.event;
			sampleTestScheduleListDrugSampleDetailService.updateSampleTestScheduleForDrugSample(evt.sampleTestScheduleList, updateSampleTestScheduleForDrugSampleResult);
		}
		
		public function updateSampleTestScheduleForDrugSampleResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
	}
}
