package hk.org.ha.event.pms.dqa.delaydelivery {
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDelayDeliveryRptFormPoDataPopupEvent extends AbstractTideEvent 
	{
		private var _delayDeliveryRptFormPoData:DelayDeliveryRptFormPoData;
		
		public function RefreshDelayDeliveryRptFormPoDataPopupEvent(delayDeliveryRptFormPoData:DelayDeliveryRptFormPoData):void {
			super();
			_delayDeliveryRptFormPoData = delayDeliveryRptFormPoData;			
		}
		
		public function get delayDeliveryRptFormPoData():DelayDeliveryRptFormPoData 
		{
			return _delayDeliveryRptFormPoData;
		}
	}
}