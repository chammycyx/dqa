package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.DeleteQaProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.PrintCaseFinalRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RefreshQaProblemForCaseNumEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemByCaseNumEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemByPharmProblemQaProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveQaProblemByQaProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateQaProblemWithQaProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowCaseFinalRptContactPopupEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxSummaryPopupEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowPharmProblemPopupCopyBatchNumPopupEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowQaProblemPopupEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowSuppPerfFileEnquiryViewPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.QaProblemServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.ExtendedExternalInterface;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.view.pms.dqa.suppperf.popup.PharmProblemPopup;
	import hk.org.ha.view.pms.dqa.suppperf.popup.QaProblemPopup;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("qaProblemServiceCtl", restrict="true")]
	public class QaProblemServiceCtl {
		
		[In]       
		public var qaProblemService:QaProblemServiceBean;
		
		[In]       
		public var qaProblem:QaProblem;
		
		[In]
		public var normalWindowsProperties:String;
		
		private var event:Event;
		private var pharmProblemIn:PharmProblem;
		private var qaProblemIn:QaProblem;
		private var pharmProblemPopupIn:PharmProblemPopup;
		private var qaProblemPopupIn:QaProblemPopup;
		private var screenNameIn:String;
		
		
		[Observer]
		public function retrieveQaProblemByPharmProblemQaProblem(evt:RetrieveQaProblemByPharmProblemQaProblemEvent):void{
			event = evt.event;
			pharmProblemIn = evt.pharmProblem;
			qaProblemIn = evt.qaProblem;
			pharmProblemPopupIn = evt.pharmProblemPopup;
			screenNameIn = evt.screenName;
			In(Object(qaProblemService).success);
			
			qaProblemService.retrieveQaProblemByCaseNum(evt.qaProblem.caseNum, 
															retrieveQaProblemByPharmProblemQaProblemResult);
		}
		
		public function retrieveQaProblemByPharmProblemQaProblemResult(evt:TideResultEvent):void{
			if (Object(qaProblemService).success ){
				evt.context.dispatchEvent( new ShowPharmProblemPopupCopyBatchNumPopupEvent(pharmProblemIn, qaProblem, pharmProblemPopupIn, screenNameIn) );
			}else{
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		
		[Observer]
		public function retrieveQaProblemByQaProblem(evt:RetrieveQaProblemByQaProblemEvent):void{
			event = evt.event;
			screenNameIn = evt.screenName;
			
			qaProblemService.retrieveQaProblemByQaProblem(evt.qaProblem, 
				retrieveQaProblemByQaProblemResult);
		}
		
		public function retrieveQaProblemByQaProblemResult(evt:TideResultEvent):void{
			if(screenNameIn == "qaProblemCaseEnquiryView")
			{
				evt.context.dispatchEvent(new ShowQaProblemPopupEvent("Edit",qaProblem,screenNameIn) );
			}
			else if(screenNameIn == "suppPerfFileEnquiryView")
			{
				evt.context.dispatchEvent(new ShowSuppPerfFileEnquiryViewPopupEvent(qaProblem) );
			}
			else if(screenNameIn == "suppPerfFaxDetailEnquiryView" || screenNameIn == "suppPerfFaxSummaryEnquiryView")
			{
				evt.context.dispatchEvent(new ShowQaProblemPopupEvent("View",qaProblem,screenNameIn) );
			}
			else if(screenNameIn == "qaProblemCaseEnquiryViewForFax")
			{
				evt.context.dispatchEvent(new ShowFaxSummaryPopupEvent(qaProblem) );
			}
			else if(screenNameIn == "qaProblemGenCaseFinalRpt")
			{
				//evt.context.dispatchEvent(new PrintCaseFinalRptEvent(qaProblem) );
				evt.context.dispatchEvent(new ShowCaseFinalRptContactPopupEvent(qaProblem) );
			}
			else
			{
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		[Observer]
		public function deleteQaProblem(evt:DeleteQaProblemEvent):void{
			event = evt.event;
			
			qaProblemService.deleteQaProblem(evt.qaProblem, evt.deleteReason, deleteQaProblemResult);
		}
		
		public function deleteQaProblemResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateQaProblemWithQaProblem(evt:UpdateQaProblemWithQaProblemEvent):void{
			event = evt.event;
			qaProblemPopupIn = evt.qaProblemPopup;
			In(Object(qaProblemService).success);
			In(Object(qaProblemService).errorCode);
			
			qaProblemService.updateQaProblemWithQaProblem(evt.qaProblem, evt.qaProblemFileUploadDataList, 
				evt.qaProblemInstitutionListTemp, evt.qaProblemNatureSubCatList,
				evt.qaProblemInstitutionAllInstitutionFlag, updateQaProblemWithQaProblemResult);
		}
		
		public function updateQaProblemWithQaProblemResult(evt:TideResultEvent):void{
			if (Object(qaProblemService).success ){
				qaProblemPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(qaProblemService).errorCode !=null){
					qaProblemPopupIn.showMessage(Object(qaProblemService).errorCode );
				}
			}
		}
		
		[Observer]
		public function retrieveQaProblemByCaseNum(evt:RetrieveQaProblemByCaseNumEvent):void{
			event = evt.event;
			screenNameIn = evt.screenName;
			qaProblemService.retrieveQaProblemByCaseNum(evt.caseNum, retrieveQaProblemByCaseNumResult);
		}
		
		public function retrieveQaProblemByCaseNumResult(evt:TideResultEvent):void{
			if(screenNameIn=="suppPerfBatchSuspView" || screenNameIn=="suppPerfBatchSuspViewAddPopup")
			{
				evt.context.dispatchEvent(new RefreshQaProblemForCaseNumEvent(screenNameIn, qaProblem));
			}
			else{
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		[Observer]
		public function printCaseFinalRpt(evt:PrintCaseFinalRptEvent):void{
			qaProblemService.createCaseFinalRpt(evt.qaProblem, evt.supplierContact, evt.companyContact, printCaseFinalRptResult);
		}
		
		public function printCaseFinalRptResult(evt:TideResultEvent):void{
			var urlReq:String = "caseFinalRpt.seam?actionMethod=caseFinalRpt.xhtml%3AqaProblemService.generateCaseFinalRpt";
			
			var winName:String ="pdf_case_final_report_win";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
			}
			
			ExtendedExternalInterface.call("window.open", urlReq, winName, normalWindowsProperties);
		}
		
	}
}
