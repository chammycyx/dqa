package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRiskLevelByRiskLevelCodeEvent extends AbstractTideEvent 
	{
		private var _riskLevelCode:String;
		
		private var _event:Event;
		
		public function RetrieveRiskLevelByRiskLevelCodeEvent(riskLevelCode:String, event:Event=null):void {
			super();
			_riskLevelCode = riskLevelCode;
			_event = event;
		}
		
		public function get riskLevelCode():String 
		{
			return _riskLevelCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}