package hk.org.ha.control.pms.dqa.coa
{					
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RetrieveLetterTemplateByDiscrepancyStatusReminderNumEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaLetterTemplateServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaLetterTemplateServiceCtl", restrict="false")]
	public class CoaLetterTemplateServiceCtl
	{		 
		[In]
		public var coaLetterTemplateService:CoaLetterTemplateServiceBean;
		
		[In]
		public var letterTemplate:LetterTemplate;
		
		private var event:Event;
				
		[Observer]
		public function retrieveLetterTemplateByDiscrepancyStatusReminderNum(evt:RetrieveLetterTemplateByDiscrepancyStatusReminderNumEvent):void 
		{		
			event = evt.event;				
			coaLetterTemplateService.retrieveLetterTemplateByDiscrepancyStatusReminderNum(
								evt.discrepancyStatus, 
								evt.reminderNum,
								retrieveLetterTemplateByDiscrepancyStatusReminderNumResult );	
			
		}
		
		private function retrieveLetterTemplateByDiscrepancyStatusReminderNumResult(evt:TideResultEvent):void 
		{		
			if(event != null) {
				evt.context.dispatchEvent( event );
				event = null;
			}
		}			
	}
}