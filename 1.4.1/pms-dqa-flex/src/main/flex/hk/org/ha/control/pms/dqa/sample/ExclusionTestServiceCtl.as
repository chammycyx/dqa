package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.popup.ShowExclusionTestPopupMessageEvent;
	import hk.org.ha.event.pms.dqa.sample.ValidateExclusionTestEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.ExclusionTestServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
	import hk.org.ha.view.pms.dqa.sample.popup.ExclusionTestPopup;
	
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("exclusionTestServiceCtl", restrict="true")]
	public class ExclusionTestServiceCtl {
		
		[In]
		public var exclusionTestService:ExclusionTestServiceBean;

		[In]
		public var exclusionTest:ExclusionTest;
		
		[In]
		public var exclusionTestPopup:ExclusionTestPopup;
		
		private var actionType:String;

		[Observer]
		public function validateExclusionTestEvent(evt:ValidateExclusionTestEvent):void{
			In(Object(exclusionTestService).success);
			In(Object(exclusionTestService).errorCode);
			actionType = evt.actionType;
			
			if (evt.exclusionTest!=null){
				exclusionTestService.checkDuplicatedExclusionTest(evt.exclusionTest,evt.exclusionTestList, validateNewExclusionTestResult );
			}
		}
		
		public function validateNewExclusionTestResult(evt:TideResultEvent):void{
		
			if (Object(exclusionTestService).success ){
				if (actionType == "Add") {
					exclusionTestPopup.confirmAdd();
				}else {
					exclusionTestPopup.closeWin();
				}
			}
			else if (Object(exclusionTestService).errorCode != null){
				evt.context.dispatchEvent(new ShowExclusionTestPopupMessageEvent(Object(exclusionTestService).errorCode) );
			}	
		}
	}
}
