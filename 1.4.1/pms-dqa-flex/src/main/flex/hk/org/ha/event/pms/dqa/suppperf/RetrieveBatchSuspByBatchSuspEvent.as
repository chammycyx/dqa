package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
	
	public class RetrieveBatchSuspByBatchSuspEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _batchSusp:BatchSusp;
		private var _screenName:String;
		
		public function RetrieveBatchSuspByBatchSuspEvent(batchSusp:BatchSusp, screenName:String, event:Event=null):void {
			super();
			_event = event;
			_batchSusp = batchSusp;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get batchSusp():BatchSusp
		{
			return _batchSusp;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}