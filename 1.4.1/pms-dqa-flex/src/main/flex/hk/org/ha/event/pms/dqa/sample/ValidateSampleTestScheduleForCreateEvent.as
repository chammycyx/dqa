package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;

	public class ValidateSampleTestScheduleForCreateEvent extends AbstractTideEvent
	{
		
		private var _sampleTestSchedule:SampleTestSchedule;
		
		private var _event:Event;
		
		public function ValidateSampleTestScheduleForCreateEvent(sampleTestSchedule:SampleTestSchedule, event:Event=null ):void{
			super();
			_event = event;	
			_sampleTestSchedule = sampleTestSchedule;
		}
	
		public function get sampleTestSchedule():SampleTestSchedule
		{
			return _sampleTestSchedule;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}