package hk.org.ha.control.pms.dqa{
	
	import hk.org.ha.event.pms.dqa.RefreshContractEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractForCoaLastestLiveContractEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractForSampleTestEvent;
	import hk.org.ha.model.pms.dqa.biz.ContractServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.Contract;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("contractServiceCtl", restrict="true")]
	public class ContractServiceCtl {
		
		[In]
		public var contractService:ContractServiceBean;
		
		[In]
		public var contract:Contract;
		
		private var event:Event;
		
		[Observer]
		public function retrieveContractForCoaLastestLiveContract(evt:RetrieveContractForCoaLastestLiveContractEvent):void{
			contractService.retrieveContractForCoaLastestLiveContract(evt.contractNum, evt.itemCode);
		}		
		
		[Observer]
		public function retrieveContractForSampleTest(evt:RetrieveContractForSampleTestEvent):void{
			event = evt.event;
			In(Object(contractService).success);	
			contractService.retrieveContractForSampleTest(evt.contractNum, evt.contractSuffix, evt.orderType, evt.itemCode, retrieveContractForSampleTestResult);
		}
		
		public function retrieveContractForSampleTestResult(evt: TideResultEvent):void{
			if(Object(contractService).success && event != null){
					evt.context.dispatchEvent( event );
			}
		}
		
	}
}