package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
	
	public class ShowNatureOfProblemSubCategoryPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		private var _problemNatureCat:ProblemNatureCat;
		
		public function ShowNatureOfProblemSubCategoryPopupEvent(actionType:String, problemNatureCat:ProblemNatureCat):void
		{
			super();
			_actionType = actionType;
			_problemNatureCat = problemNatureCat;
		}
		
		public function get action():String {
			return _actionType;
		}
		
		public function get problemNatureCat():ProblemNatureCat {
			return _problemNatureCat;
		}
	}
}