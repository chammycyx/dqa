package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaInEmailListForInEmailEnqEvent extends AbstractTideEvent 
	{	
		
		private var _fromReceiveDate:Date;
		
		private var _toReceiveDate:Date;
		
		private var _resultFlag:ValidateResultFlag;
		
		private var _event:Event;
		
		public function RetrieveCoaInEmailListForInEmailEnqEvent(fromReceiveDate:Date, toReceiveDate:Date, resultFlag:ValidateResultFlag, event:Event=null):void 
		{
			super();
			_fromReceiveDate = fromReceiveDate;
			_toReceiveDate = toReceiveDate;
			_resultFlag = resultFlag;
			_event = event;
		}
		
		public function get fromReceiveDate():Date
		{
			return _fromReceiveDate;
		}
		
		public function get toReceiveDate():Date
		{
			return _toReceiveDate;
		}
		
		public function get resultFlag():ValidateResultFlag
		{
			return _resultFlag;
		}
		
		public function get event():Event
		{
			return _event;
		}
		
	}
}