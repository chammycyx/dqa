package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveContractListByItemCodeOrderTypeWithCallBackEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _contractSuffix:String;
		private var _itemCode:String;
		private var _orderType:OrderTypeAll;
		private var _callBack:Function;
		
		public function RetrieveContractListByItemCodeOrderTypeWithCallBackEvent(contractNum:String, contractSuffix:String, itemCode:String, orderType:OrderTypeAll, callBack:Function):void {
			super();
			_contractNum = contractNum;
			_contractSuffix = contractSuffix;
			_itemCode = itemCode;
			_orderType = orderType;
			_callBack = callBack;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get contractSuffix():String 
		{
			return _contractSuffix;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get orderType():OrderTypeAll
		{
			return _orderType;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}