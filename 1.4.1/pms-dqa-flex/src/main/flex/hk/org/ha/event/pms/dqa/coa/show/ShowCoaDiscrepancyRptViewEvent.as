package hk.org.ha.event.pms.dqa.coa.show
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCoaDiscrepancyRptViewEvent extends AbstractTideEvent 
	{	
		private var _clearMessages:Boolean;
		
		public function ShowCoaDiscrepancyRptViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}