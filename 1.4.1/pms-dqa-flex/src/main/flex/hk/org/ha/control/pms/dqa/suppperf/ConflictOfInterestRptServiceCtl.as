package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.delaydelivery.GenerateDelayDeliveryRptFormConflictOfInterestRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.GenerateConflictOfInterestRptEvent;

	import hk.org.ha.model.pms.dqa.biz.suppperf.ConflictOfInterestRptServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.ExtendedExternalInterface;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("conflictOfInterestRptServiceCtl", restrict="true")]
	public class ConflictOfInterestRptServiceCtl {				
		
		[In]
		public var conflictOfInterestRptService:ConflictOfInterestRptServiceBean;
		
		[In]
		public var normalWindowsProperties:String;
		
		[Observer]
		public function genConflictOfInterestRpt(evt:GenerateConflictOfInterestRptEvent):void{
			conflictOfInterestRptService.createConflictOfInterestRpt(evt.conflictOfInterestRpt, genConflictOfInterestRptResult);
		}
		
		
		public function genConflictOfInterestRptResult(evt:TideResultEvent):void{
			var urlReq:String = "conflictOfInterestRpt.seam?actionMethod=conflictOfInterestRpt.xhtml%3AconflictOfInterestRptService.generateConflictOfInterestRpt";
			
			var winName:String ="pdf_conflict_of_interest_report_win";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
			}						
			
			ExtendedExternalInterface.call("window.open", urlReq, winName, normalWindowsProperties);
		}
		
		[Observer]
		public function genDelayDeliveryRptFormConflictOfInterestRpt(evt:GenerateDelayDeliveryRptFormConflictOfInterestRptEvent):void{
			conflictOfInterestRptService.createConflictOfInterestRpt(evt.conflictOfInterestRpt, genDelayDeliveryRptFormConflictOfInterestRptResult);
		}
		
		public function genDelayDeliveryRptFormConflictOfInterestRptResult(evt:TideResultEvent):void{
			var urlReq:String = "conflictOfInterestRpt.seam?actionMethod=conflictOfInterestRpt.xhtml%3AconflictOfInterestRptService.generateConflictOfInterestRpt";
			
			var winName:String ="pdf_conflict_of_interest_report_win";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
			}						
			
			ExtendedExternalInterface.call("window.open", urlReq, winName, normalWindowsProperties);
		}
	}
}
