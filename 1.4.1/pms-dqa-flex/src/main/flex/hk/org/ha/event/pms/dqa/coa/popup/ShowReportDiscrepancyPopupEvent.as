package hk.org.ha.event.pms.dqa.coa.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowReportDiscrepancyPopupEvent extends AbstractTideEvent 
	{		
		public function ShowReportDiscrepancyPopupEvent():void 
		{
			super();
		}
	}
}