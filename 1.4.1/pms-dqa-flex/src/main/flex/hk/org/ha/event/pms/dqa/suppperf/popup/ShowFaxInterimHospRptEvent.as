package hk.org.ha.event.pms.dqa.suppperf.popup {
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFaxInterimHospRptEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function ShowFaxInterimHospRptEvent(event:Event=null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}