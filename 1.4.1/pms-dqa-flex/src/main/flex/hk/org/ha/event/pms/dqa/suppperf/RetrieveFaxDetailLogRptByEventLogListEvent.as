package hk.org.ha.event.pms.dqa.suppperf {
	
	import mx.collections.ArrayCollection;
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFaxDetailLogRptByEventLogListEvent extends AbstractTideEvent 
	{
		private var _eventLogList:ArrayCollection;
		
		public function RetrieveFaxDetailLogRptByEventLogListEvent(eventLogList:ArrayCollection):void 
		{
			super();
			_eventLogList = eventLogList;
		}
		
		public function get eventLogList():ArrayCollection {
			return _eventLogList;
		}
	}
}