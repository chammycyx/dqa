package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
				 
	public class RetrieveSampleTestScheduleListForCopyEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _event2:Event;
		private var _scheduleId:Number;
		private var _itemCode:String;
		
		public function RetrieveSampleTestScheduleListForCopyEvent( scheduleId:Number, itemCode:String, event:Event=null, event2:Event=null):void {
			super();
			_event = event;
			_event2 = event2;
			_scheduleId = scheduleId;
			_itemCode = itemCode;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get event2():Event{
			return _event2;
		}
		
		public function get scheduleId():Number {
			return _scheduleId;
		}
		
		public function get itemCode():String {
			return _itemCode;
		}
	}
}