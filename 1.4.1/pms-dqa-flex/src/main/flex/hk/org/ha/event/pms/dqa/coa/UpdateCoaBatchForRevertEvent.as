package hk.org.ha.event.pms.dqa.coa {		
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateCoaBatchForRevertEvent extends AbstractTideEvent 
	{				
		private var _coaBatchVer:CoaBatchVer;				
		
		private var _event:Event;
		
		public function UpdateCoaBatchForRevertEvent(coaBatchVer:CoaBatchVer, event:Event = null):void 
		{
			super();		
			_coaBatchVer = coaBatchVer;			
			_event = event;
		}
		
		public function get coaBatchVer():CoaBatchVer {
			return _coaBatchVer;
		}

		public function get event():Event {
			return _event;
		}
	}
}