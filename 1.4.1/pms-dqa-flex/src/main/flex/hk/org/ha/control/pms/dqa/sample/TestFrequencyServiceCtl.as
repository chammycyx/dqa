package hk.org.ha.control.pms.dqa.sample{
	import hk.org.ha.event.pms.dqa.sample.AddTestFrequencyEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateTestFrequencyEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteTestFrequencyEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveTestFrequencyEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateTestFrequencyEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowTestFrequencyMaintMessageEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.TestFrequencyServiceBean;
	import hk.org.ha.view.pms.dqa.sample.popup.TestFrequencyPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("testFrequencyServiceCtl", restrict="true")]
	public class TestFrequencyServiceCtl {
		
		[In]       
		public var testFrequencyService:TestFrequencyServiceBean;
		
		[In]
		public var testFrequencyPopup:TestFrequencyPopup;
		
		private var event:Event;
		
		[Observer]
		public function retrieveTestFrequency(evt:RetrieveTestFrequencyEvent):void {
			event = evt.event;
			testFrequencyService.retrieveTestFrequencyByTestFrequency(evt.testFrequency, retrieveTestFrequencyResult);
		}
		
		public function retrieveTestFrequencyResult(evt:TideResultEvent):void {
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}		
		}
		
		[Observer]
		public function createTestFrequency(evt:CreateTestFrequencyEvent):void{
			event = evt.event;
			In(Object(testFrequencyService).success);
			In(Object(testFrequencyService).errorCode);
			testFrequencyService.createTestFrequency(createTestFrequencyResult);
		}
		
		public function createTestFrequencyResult(evt:TideResultEvent):void{
			if (Object(testFrequencyService).success ){
				testFrequencyPopup.closeWin();
				
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else if (Object(testFrequencyService).errorCode != null){
				evt.context.dispatchEvent(new ShowTestFrequencyMaintMessageEvent(Object(testFrequencyService).errorCode) );
			}	
		}
		
		[Observer]
		public function addTestFrequency(evt:AddTestFrequencyEvent):void{
			event = evt.event;
			testFrequencyService.addTestFrequency(addTestFrequencyResult);			
		}
		
		public function addTestFrequencyResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateTestFrequency(evt:UpdateTestFrequencyEvent):void{
			event = evt.event;
			In(Object(testFrequencyService).success);
			In(Object(testFrequencyService).errorCode);
			testFrequencyService.updateTestFrequency(updateTestFrequencyResult);	
		}
		
		public function updateTestFrequencyResult(evt:TideResultEvent):void{
			if (Object(testFrequencyService).success ){
				testFrequencyPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else if (Object(testFrequencyService).errorCode != null){
				evt.context.dispatchEvent(new ShowTestFrequencyMaintMessageEvent(Object(testFrequencyService).errorCode) );
			}		
		}
		
		[Observer]
		public function deleteTestFrequency(evt:DeleteTestFrequencyEvent):void{
			In(Object(testFrequencyService).success);
			In(Object(testFrequencyService).errorCode);
			event = evt.event;
			testFrequencyService.deleteTestFrequency(evt.testFrequency, deleteTestFrequencyResult);
		}
		
		public function deleteTestFrequencyResult(evt:TideResultEvent):void{
			if (Object(testFrequencyService).success ){
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else if (Object(testFrequencyService).errorCode != null){
				evt.context.dispatchEvent(new ShowTestFrequencyMaintMessageEvent(Object(testFrequencyService).errorCode) );
			}	
		}
	}
}
