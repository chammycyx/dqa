package hk.org.ha.control.pms.dqa{
	import hk.org.ha.event.pms.dqa.RefreshDmDrugListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveDmDrugListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveDmDrugListWithCallBackEvent;
	import hk.org.ha.model.pms.dqa.biz.DmDrugListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
		
	[Bindable]
	[Name("dmDrugListServiceCtl", restrict="true")]
	public class DmDrugListServiceCtl {
		
		[In]
		public var dmDrugListService:DmDrugListServiceBean;
		
		[In]
		public var drugItemList:ArrayCollection = new ArrayCollection();
		
		private var event:Event;
		
		[Observer]
		public function retrieveDmDrugList(evt:RetrieveDmDrugListEvent):void {
			event = evt.event;
			dmDrugListService.retrieveDmDrugListLikeItemCode(evt.itemCode, retrieveDmDrugListResult);
		}
		
		public function retrieveDmDrugListResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveDmDrugListWithCallBack(evt:RetrieveDmDrugListWithCallBackEvent):void {
			dmDrugListService.retrieveDmDrugListLikeItemCode(evt.itemCode, function(tideResultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack();
				}
			});
		}
		
	}
}