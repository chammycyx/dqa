package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	
	public class ShowFaxContactPopupFinalSuppPopupEvent extends AbstractTideEvent 
	{	
		
		private var _qaProblem:QaProblem;
		private var _suppManuf:Object;
		private var _userInfo:UserInfo;
		
		public function ShowFaxContactPopupFinalSuppPopupEvent(qaProblem:QaProblem, suppManuf:Object, userInfo:UserInfo):void
		{
			super();
			_qaProblem = qaProblem;
			_suppManuf = suppManuf;
			_userInfo = userInfo;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get suppManuf():Object {
			return _suppManuf;
		}
		
		public function get userInfo():UserInfo {
			return _userInfo;
		}
		
	}
}