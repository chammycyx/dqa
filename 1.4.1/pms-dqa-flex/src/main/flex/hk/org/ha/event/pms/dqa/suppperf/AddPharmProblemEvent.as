package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddPharmProblemEvent extends AbstractTideEvent 
	{
		private var _callBackEvent:Event;
		
		public function AddPharmProblemEvent(eventValue:Event=null):void { 
			super();
			_callBackEvent = eventValue;
		}
		
		public function get callBackEvent():Event {
			return _callBackEvent;
		}
		
	}
}