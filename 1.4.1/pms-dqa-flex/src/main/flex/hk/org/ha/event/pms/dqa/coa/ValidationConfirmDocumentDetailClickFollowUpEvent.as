package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class ValidationConfirmDocumentDetailClickFollowUpEvent extends AbstractTideEvent 
	{
		
		private var _event:Event;
		private var _actionType:String;
				
		public function ValidationConfirmDocumentDetailClickFollowUpEvent(evt:Event, actionType:String):void {
			super();
			this._event = evt;
			this._actionType = actionType;			
		}

		public function get event():Event
		{
			return _event;
		}
		
		public function get actionType():String
		{
			return _actionType;
		}		
	}
}