package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.coa.CoaOutstandBatchCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaOutstandBatchListForOutstandBatchCoaEvent extends AbstractTideEvent 
	{	
		
		private var _criteria:CoaOutstandBatchCriteria;
		
		private var _event:Event;
		
		public function RetrieveCoaOutstandBatchListForOutstandBatchCoaEvent(criteria:CoaOutstandBatchCriteria, event:Event=null):void 
		{
			super();
			_event = event;
			_criteria = criteria;
		}
		
		public function get criteria():CoaOutstandBatchCriteria
		{
			return _criteria;
		}
		public function get event():Event
		{
			return _event;
		}
		
	}
}