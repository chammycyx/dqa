package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveProblemNatureSubCatListFullEvent extends AbstractTideEvent 
	{
		
		private var _callBack:Function;
		
		public function RetrieveProblemNatureSubCatListFullEvent(callBack:Function):void {
			super();
			_callBack = callBack;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
	
	
}