/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.dqa.udt.sample {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.sample.DocStatus")]
    public class DocStatus extends Enum {

        public static const Blank:DocStatus = new DocStatus("Blank", _, " ", "(Blank)");
        public static const Available:DocStatus = new DocStatus("Available", _, "A", "Available");
        public static const NotAvailable:DocStatus = new DocStatus("NotAvailable", _, "NA", "Not Available");
        public static const AvailablePending:DocStatus = new DocStatus("AvailablePending", _, "AP", "Available Pending");
        public static const NotApplicable:DocStatus = new DocStatus("NotApplicable", _, "NAPP", "Not applicable");

        function DocStatus(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Blank.name), restrictor, (dataValue || Blank.dataValue), (displayValue || Blank.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Blank, Available, NotAvailable, AvailablePending, NotApplicable];
        }

        public static function valueOf(name:String):DocStatus {
            return DocStatus(Blank.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):DocStatus {
            return DocStatus(Blank.dataConstantOf(dataValue));
        }
    }
}