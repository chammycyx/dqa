/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (SampleTestScheduleCriteria.as).
 */

package hk.org.ha.model.pms.dqa.vo.sample {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.dqa.udt.OrderType;
    import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
    import org.granite.util.Enum;

    [Bindable]
    public class SampleTestScheduleCriteriaBase implements IExternalizable {

        private var _itemCode:String;
        private var _manufCode:String;
        private var _orderType:OrderType;
        private var _pharmCode:String;
        private var _riskLevelCode:String;
        private var _scheduleMonth:Date;
        private var _scheduleStatus:ScheduleStatus;
        private var _supplierCode:String;
        private var _testCode:String;

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set manufCode(value:String):void {
            _manufCode = value;
        }
        public function get manufCode():String {
            return _manufCode;
        }

        public function set orderType(value:OrderType):void {
            _orderType = value;
        }
        public function get orderType():OrderType {
            return _orderType;
        }

        public function set pharmCode(value:String):void {
            _pharmCode = value;
        }
        public function get pharmCode():String {
            return _pharmCode;
        }

        public function set riskLevelCode(value:String):void {
            _riskLevelCode = value;
        }
        public function get riskLevelCode():String {
            return _riskLevelCode;
        }

        public function set scheduleMonth(value:Date):void {
            _scheduleMonth = value;
        }
        public function get scheduleMonth():Date {
            return _scheduleMonth;
        }

        public function set scheduleStatus(value:ScheduleStatus):void {
            _scheduleStatus = value;
        }
        public function get scheduleStatus():ScheduleStatus {
            return _scheduleStatus;
        }

        public function set supplierCode(value:String):void {
            _supplierCode = value;
        }
        public function get supplierCode():String {
            return _supplierCode;
        }

        public function set testCode(value:String):void {
            _testCode = value;
        }
        public function get testCode():String {
            return _testCode;
        }

        public function readExternal(input:IDataInput):void {
            _itemCode = input.readObject() as String;
            _manufCode = input.readObject() as String;
            _orderType = Enum.readEnum(input) as OrderType;
            _pharmCode = input.readObject() as String;
            _riskLevelCode = input.readObject() as String;
            _scheduleMonth = input.readObject() as Date;
            _scheduleStatus = Enum.readEnum(input) as ScheduleStatus;
            _supplierCode = input.readObject() as String;
            _testCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_itemCode);
            output.writeObject(_manufCode);
            output.writeObject(_orderType);
            output.writeObject(_pharmCode);
            output.writeObject(_riskLevelCode);
            output.writeObject(_scheduleMonth);
            output.writeObject(_scheduleStatus);
            output.writeObject(_supplierCode);
            output.writeObject(_testCode);
        }
    }
}