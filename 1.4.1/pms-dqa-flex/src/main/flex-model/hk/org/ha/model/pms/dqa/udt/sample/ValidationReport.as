/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.dqa.udt.sample {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.sample.ValidationReport")]
    public class ValidationReport extends Enum {

        public static const AC:ValidationReport = new ValidationReport("AC", _, "AC", "Available (CPO)");
        public static const AS:ValidationReport = new ValidationReport("AS", _, "AS", "Available (Supplier)");
        public static const NA:ValidationReport = new ValidationReport("NA", _, "NA", "Not Available");
        public static const AP:ValidationReport = new ValidationReport("AP", _, "AP", "Available Pending");
        public static const NAPP:ValidationReport = new ValidationReport("NAPP", _, "NAPP", "Not Applicable");

        function ValidationReport(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || AC.name), restrictor, (dataValue || AC.dataValue), (displayValue || AC.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [AC, AS, NA, AP, NAPP];
        }

        public static function valueOf(name:String):ValidationReport {
            return ValidationReport(AC.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):ValidationReport {
            return ValidationReport(AC.dataConstantOf(dataValue));
        }
    }
}