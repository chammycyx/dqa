/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (Contracts.as).
 */

package hk.org.ha.model.pms.dqa.vo {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import mx.collections.ListCollectionView;

    [Bindable]
    public class ContractsBase implements IExternalizable {

        private var _contractList:ListCollectionView;

        public function set contractList(value:ListCollectionView):void {
            _contractList = value;
        }
        public function get contractList():ListCollectionView {
            return _contractList;
        }

        public function readExternal(input:IDataInput):void {
            _contractList = input.readObject() as ListCollectionView;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_contractList);
        }
    }
}