/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CoaProcessingDateRptCriteria.as).
 */

package hk.org.ha.model.pms.dqa.vo.coa {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class CoaProcessingDateRptCriteriaBase implements IExternalizable {

        private var _contractNum:String;
        private var _endDate:Date;
        private var _itemCode:String;
        private var _startDate:Date;
        private var _supplierCode:String;

        public function set contractNum(value:String):void {
            _contractNum = value;
        }
        public function get contractNum():String {
            return _contractNum;
        }

        public function set endDate(value:Date):void {
            _endDate = value;
        }
        public function get endDate():Date {
            return _endDate;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set startDate(value:Date):void {
            _startDate = value;
        }
        public function get startDate():Date {
            return _startDate;
        }

        public function set supplierCode(value:String):void {
            _supplierCode = value;
        }
        public function get supplierCode():String {
            return _supplierCode;
        }

        public function readExternal(input:IDataInput):void {
            _contractNum = input.readObject() as String;
            _endDate = input.readObject() as Date;
            _itemCode = input.readObject() as String;
            _startDate = input.readObject() as Date;
            _supplierCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_contractNum);
            output.writeObject(_endDate);
            output.writeObject(_itemCode);
            output.writeObject(_startDate);
            output.writeObject(_supplierCode);
        }
    }
}