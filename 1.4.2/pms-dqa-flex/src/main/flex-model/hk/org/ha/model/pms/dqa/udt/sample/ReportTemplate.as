/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.dqa.udt.sample {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.sample.ReportTemplate")]
    public class ReportTemplate extends Enum {

        public static const ALL:ReportTemplate = new ReportTemplate("ALL", _, "ALL", "All");
        public static const TR:ReportTemplate = new ReportTemplate("TR", _, "TR", "Test Result");

        function ReportTemplate(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || ALL.name), restrictor, (dataValue || ALL.dataValue), (displayValue || ALL.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [ALL, TR];
        }

        public static function valueOf(name:String):ReportTemplate {
            return ReportTemplate(ALL.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):ReportTemplate {
            return ReportTemplate(ALL.dataConstantOf(dataValue));
        }
    }
}