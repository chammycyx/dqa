/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ScreenIdleServiceBean.as).
 */

package hk.org.ha.model.pms.biz.security {

    import flash.utils.flash_proxy;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class ScreenIdleServiceBeanBase extends Component {

        public function syncLockScreenSessionTimeout(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("syncLockScreenSessionTimeout", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("syncLockScreenSessionTimeout", resultHandler);
            else if (resultHandler == null)
                callProperty("syncLockScreenSessionTimeout");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function refreshSessionTimeOut(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("refreshSessionTimeOut", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("refreshSessionTimeOut", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("refreshSessionTimeOut", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function setSessionTimeout(arg0:int, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("setSessionTimeout", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("setSessionTimeout", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("setSessionTimeout", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getSessionTimeout(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getSessionTimeout", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getSessionTimeout", resultHandler);
            else if (resultHandler == null)
                callProperty("getSessionTimeout");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
