/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (SampleTestFileServiceBean.as).
 */

package hk.org.ha.model.pms.dqa.biz.sample {

    import flash.utils.ByteArray;
    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class SampleTestFileServiceBeanBase extends Component {

        public function uploadFile(arg0:ByteArray, arg1:String, arg2:SampleTestFile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("uploadFile", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("uploadFile", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("uploadFile", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveSampleTestFileBySampleTestFileId(arg0:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveSampleTestFileBySampleTestFileId", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveSampleTestFileBySampleTestFileId", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveSampleTestFileBySampleTestFileId", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveSampleTestFileForValidation(arg0:String, arg1:String, arg2:String, arg3:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveSampleTestFileForValidation", arg0, arg1, arg2, arg3, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveSampleTestFileForValidation", arg0, arg1, arg2, arg3, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveSampleTestFileForValidation", arg0, arg1, arg2, arg3);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function validateSampleTestFile(arg0:SampleTestFile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("validateSampleTestFile", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("validateSampleTestFile", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("validateSampleTestFile", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function addSampleTestFile(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("addSampleTestFile", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("addSampleTestFile", resultHandler);
            else if (resultHandler == null)
                callProperty("addSampleTestFile");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function addSampleTestFileForTestResult(arg0:SampleTestFile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("addSampleTestFileForTestResult", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("addSampleTestFileForTestResult", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("addSampleTestFileForTestResult", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createSampleTestFile(arg0:SampleTestFile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createSampleTestFile", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createSampleTestFile", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("createSampleTestFile", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updateSampleTestFile(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateSampleTestFile", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateSampleTestFile", resultHandler);
            else if (resultHandler == null)
                callProperty("updateSampleTestFile");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function deleteSampleTestFile(arg0:SampleTestFile, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("deleteSampleTestFile", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("deleteSampleTestFile", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("deleteSampleTestFile", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isSuccess(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isSuccess", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isSuccess", resultHandler);
            else if (resultHandler == null)
                callProperty("isSuccess");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getErrorCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getErrorCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getErrorCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getErrorCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
