/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CoaProcessRptData.as).
 */

package hk.org.ha.model.pms.dqa.vo.coa {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class CoaProcessRptDataBase implements IExternalizable {

        private var _discrepancy:int;
        private var _fail:int;
        private var _itemCode:String;
        private var _pass:int;
        private var _supplierCode:String;
        private var _total:int;
        private var _verificationInProgress:int;

        public function set discrepancy(value:int):void {
            _discrepancy = value;
        }
        public function get discrepancy():int {
            return _discrepancy;
        }

        public function set fail(value:int):void {
            _fail = value;
        }
        public function get fail():int {
            return _fail;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set pass(value:int):void {
            _pass = value;
        }
        public function get pass():int {
            return _pass;
        }

        public function set supplierCode(value:String):void {
            _supplierCode = value;
        }
        public function get supplierCode():String {
            return _supplierCode;
        }

        public function set total(value:int):void {
            _total = value;
        }
        public function get total():int {
            return _total;
        }

        public function set verificationInProgress(value:int):void {
            _verificationInProgress = value;
        }
        public function get verificationInProgress():int {
            return _verificationInProgress;
        }

        public function readExternal(input:IDataInput):void {
            _discrepancy = input.readObject() as int;
            _fail = input.readObject() as int;
            _itemCode = input.readObject() as String;
            _pass = input.readObject() as int;
            _supplierCode = input.readObject() as String;
            _total = input.readObject() as int;
            _verificationInProgress = input.readObject() as int;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_discrepancy);
            output.writeObject(_fail);
            output.writeObject(_itemCode);
            output.writeObject(_pass);
            output.writeObject(_supplierCode);
            output.writeObject(_total);
            output.writeObject(_verificationInProgress);
        }
    }
}