package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveBatchSuspByBatchSuspEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.BatchSuspServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateBatchSuspEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateBatchSuspEvent;
	import hk.org.ha.event.pms.dqa.suppperf.DeleteBatchSuspEvent;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("batchSuspServiceCtl", restrict="true")]
	public class BatchSuspServiceCtl {
		
		private var countryCode:String;
		
		private var event:Event;
		
		[In]
		public var batchSuspService:BatchSuspServiceBean;
		
		[In]       
		public var batchSusp:BatchSusp;
		
		private var batchSuspIn:BatchSusp;
		private var screenNameIn:String;
		
		[Observer]
		public function retrieveBatchSuspByBatchSusp(evt:RetrieveBatchSuspByBatchSuspEvent):void{
			event = evt.event;
			screenNameIn = evt.screenName;
			
			batchSuspService.retrieveBatchSuspByBatchSusp(evt.batchSusp, retrieveBatchSuspByBatchSuspResult);
		}
		
		public function retrieveBatchSuspByBatchSuspResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function createBatchSusp(evt:CreateBatchSuspEvent):void{
			event = evt.event;

			batchSuspService.createBatchSusp(evt.batchSuspList, createBatchSuspResult);
		}
		
		public function createBatchSuspResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateBatchSusp(evt:UpdateBatchSuspEvent):void{
			event = evt.event;
			
			batchSuspService.updateBatchSusp(evt.batchSusp, updateBatchSuspResult);
		}
		
		public function updateBatchSuspResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function deleteBatchSusp(evt:DeleteBatchSuspEvent):void{
			event = evt.event;
			
			batchSuspService.deleteBatchSusp(evt.batchSusp, deleteBatchSuspResult);
		}
		
		public function deleteBatchSuspResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}