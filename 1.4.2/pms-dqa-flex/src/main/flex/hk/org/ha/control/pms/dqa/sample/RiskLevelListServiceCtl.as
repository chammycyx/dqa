package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveRiskLevelListEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.RiskLevelListServiceBean;
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("riskLevelListServiceCtl", restrict="true")]
	public class RiskLevelListServiceCtl {
		
	
		[In]
		public var riskLevelListService:RiskLevelListServiceBean;
		
		private var event:Event;
	
		[Observer]
		public function retrieveRiskLevelList(evt:RetrieveRiskLevelListEvent):void {
			event = evt.event;
			riskLevelListService.retrieveRiskLevelListByRecordStatus(RecordStatus.Active, retrieveRiskLevelListResult);
		}
		
		public function retrieveRiskLevelListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
		
	
	}
}
