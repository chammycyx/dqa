package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateEmailLogByCoaBatchLetterTemplateEvent extends AbstractTideEvent 
	{
		private var _coaBatch:CoaBatch;
		
		private var _letterTemplate:LetterTemplate;
		
		private var _event:Event;
		
		public function CreateEmailLogByCoaBatchLetterTemplateEvent(coaBatch:CoaBatch, letterTemplate:LetterTemplate, event:Event=null):void {
			super();
			_coaBatch = coaBatch;
			_letterTemplate = letterTemplate;
			_event = event;
		}
		
		public function get coaBatch():CoaBatch 
		{
			return _coaBatch;
		}
		
		public function get letterTemplate():LetterTemplate 
		{
			return _letterTemplate;
		}
		
		public function get event():Event 
		{
			return _event;
		}
		
	}
}