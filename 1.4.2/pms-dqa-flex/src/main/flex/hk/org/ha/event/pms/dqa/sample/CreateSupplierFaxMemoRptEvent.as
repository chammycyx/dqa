package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SupplierMemoContentVo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateSupplierFaxMemoRptEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _supplierMemoContentVo:SupplierMemoContentVo;
		
		
		public function CreateSupplierFaxMemoRptEvent(supplierMemoContentVo:SupplierMemoContentVo, event:Event):void {
			super();
			_event = event;
			_supplierMemoContentVo = supplierMemoContentVo;			
		}
		
		public function get supplierMemoContentVo():SupplierMemoContentVo 
		{
			return _supplierMemoContentVo;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}