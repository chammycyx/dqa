package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	
	public class DeleteCoaBatchEvent extends AbstractTideEvent 
	{
		private var _coaBatch:CoaBatch;
		
		
		public function DeleteCoaBatchEvent(coaBatch:CoaBatch):void {
			super();
			_coaBatch = coaBatch;			
		}
		
		public function get coaBatch():CoaBatch 
		{
			return _coaBatch;
		}
		
	}
}