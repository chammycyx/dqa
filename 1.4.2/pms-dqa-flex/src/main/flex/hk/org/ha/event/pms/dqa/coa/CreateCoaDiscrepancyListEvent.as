package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateCoaDiscrepancyListEvent extends AbstractTideEvent 
	{		
		private var _coaDiscrepancyKeyList:ListCollectionView;
		private var _coaBatch:CoaBatch;
		private var _event:Event;
		private var _callBackFunc:Function;
		
		public function CreateCoaDiscrepancyListEvent(coaBatch:CoaBatch, coaDiscrepancyKeyList:ListCollectionView, event:Event=null, func:Function=null):void 
		{
			super();
			_coaBatch = coaBatch;
			_coaDiscrepancyKeyList = coaDiscrepancyKeyList;
			_event = event;
			_callBackFunc = func;
		}
		
		public function get coaDiscrepancyKeyList():ListCollectionView {
			return _coaDiscrepancyKeyList;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
	}
}