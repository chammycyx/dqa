package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.ReleaseRetentionSampleMemo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateReleaseRetentionSampleMemoEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _releaseRetentionSampleMemo:ReleaseRetentionSampleMemo;
		
		
		public function CreateReleaseRetentionSampleMemoEvent(releaseRetentionSampleMemo:ReleaseRetentionSampleMemo, event:Event=null):void {
			super();
			_event = event;
			_releaseRetentionSampleMemo = releaseRetentionSampleMemo;			
		}
		
		public function get releaseRetentionSampleMemo():ReleaseRetentionSampleMemo 
		{
			return _releaseRetentionSampleMemo;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}