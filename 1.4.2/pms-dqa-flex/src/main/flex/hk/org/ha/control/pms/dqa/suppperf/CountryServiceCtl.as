package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveCountryByCountryCodeEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RefreshCountryEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.CountryServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("countryServiceCtl", restrict="true")]
	public class CountryServiceCtl {
		
		private var countryCode:String;
		
		private var event:Event;
		
		[In]
		public var country:Country;
		
		[In]
		public var countryService:CountryServiceBean;
		
		private var screenName:String;
		
		[Observer]
		public function retrieveCountryByCountryCode(evt:RetrieveCountryByCountryCodeEvent):void 
		{		
			this.screenName = evt.screenName;
			this.countryCode = evt.countryCode;
			event = evt.event;
			countryService.retrieveCountryByCountryCode(this.countryCode, retrieveCountryByCountryCodeResult);		
		}	
		
		private function retrieveCountryByCountryCodeResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshCountryEvent(country, screenName));
		}
	}
}