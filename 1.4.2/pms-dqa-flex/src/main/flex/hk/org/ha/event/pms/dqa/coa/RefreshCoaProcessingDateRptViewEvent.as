package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCoaProcessingDateRptViewEvent extends AbstractTideEvent 
	{		
		public function RefreshCoaProcessingDateRptViewEvent():void 
		{
			super();
		}		
	}
}