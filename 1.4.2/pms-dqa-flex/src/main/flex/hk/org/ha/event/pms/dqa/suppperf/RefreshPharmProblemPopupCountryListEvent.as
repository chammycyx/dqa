package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RefreshPharmProblemPopupCountryListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _countryCode:String;
		
		
		public function RefreshPharmProblemPopupCountryListEvent(countryCode:String, event:Event=null):void {
			super();
			_event = event;
			_countryCode = countryCode;			
		}
		
		public function get countryCode():String 
		{
			return _countryCode;
		}
	}
}