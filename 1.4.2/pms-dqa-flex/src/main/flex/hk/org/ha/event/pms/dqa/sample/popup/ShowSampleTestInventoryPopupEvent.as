package hk.org.ha.event.pms.dqa.sample.popup
{
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSampleTestInventoryPopupEvent extends AbstractTideEvent 
	{	
		
		private var _action:String;
		private var _sampleMovement :SampleMovement;
		
		public function ShowSampleTestInventoryPopupEvent(action:String, sampleMovement:SampleMovement = null):void
		{
			super();
			_action = action;	
			_sampleMovement = sampleMovement;
		}
		
		public function get action():String {
			return _action;
		}
		public function get sampleMovement():SampleMovement {
			return _sampleMovement;
		}
		
	}
}