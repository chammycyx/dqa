package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import flash.events.Event;
	
	public class UpdateSampleTestScheduleForPaymentEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _actionType:String;
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function UpdateSampleTestScheduleForPaymentEvent(sampleTestSchedule:SampleTestSchedule, actionType:String, event:Event=null):void{
			super();
			
			_event = event;
			_actionType = actionType;
			_sampleTestSchedule = sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
		
		public function get actionType():String {
			return _actionType;
		}
	}
}