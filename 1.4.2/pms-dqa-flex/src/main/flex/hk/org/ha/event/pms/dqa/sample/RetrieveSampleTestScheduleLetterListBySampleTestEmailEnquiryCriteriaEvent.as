package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestEmailEnquiryCriteria:SampleTestEmailEnquiryCriteria;
		
		public function RetrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaEvent(sampleTestEmailEnquiryCriteria:SampleTestEmailEnquiryCriteria, event:Event=null):void {
			super();
			_sampleTestEmailEnquiryCriteria = sampleTestEmailEnquiryCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestEmailEnquiryCriteria():SampleTestEmailEnquiryCriteria{
			return _sampleTestEmailEnquiryCriteria;
		}
	}
}