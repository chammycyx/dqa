package hk.org.ha.control.pms.dqa.coa
{		
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyRptListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaDiscrepancyRptServiceBean;
	
	import flash.events.Event;
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("coaDiscrepancyRptServiceCtl", restrict="false")]
	public class CoaDiscrepancyRptServiceCtl
	{
		[In]
		public var coaDiscrepancyRptService:CoaDiscrepancyRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaDiscrepancyRptList(evt:RetrieveCoaDiscrepancyRptListEvent):void
		{
			event = evt.event;
			coaDiscrepancyRptService.retrieveCoaDiscrepancyList(evt.coaDiscrepancyRptCriteria, retrieveCoaDiscrepancyRptListResult);
		}
		
		public function retrieveCoaDiscrepancyRptListResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
	}	
}