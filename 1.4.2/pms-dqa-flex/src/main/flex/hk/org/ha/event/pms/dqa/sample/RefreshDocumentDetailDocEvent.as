package hk.org.ha.event.pms.dqa.sample
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;

	import flash.events.Event;
	
	public class RefreshDocumentDetailDocEvent extends AbstractTideEvent 
	{
		private var _sampleTestFile:SampleTestFile;
		
		public function RefreshDocumentDetailDocEvent(sampleTestFile:SampleTestFile, event:Event=null):void{
			super();
			_sampleTestFile = sampleTestFile;
		}
		
		public function get sampleTestFile():SampleTestFile {
			return _sampleTestFile;
		}
	}
}