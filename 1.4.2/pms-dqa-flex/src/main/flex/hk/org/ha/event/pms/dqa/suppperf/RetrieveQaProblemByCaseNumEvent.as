package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveQaProblemByCaseNumEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _caseNum:String;
		private var _screenName:String;
		
		public function RetrieveQaProblemByCaseNumEvent(caseNum:String, screenName:String, event:Event=null):void {
			super();
			_event = event;
			_caseNum = caseNum;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}