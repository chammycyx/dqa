package hk.org.ha.event.pms.dqa.popup
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEmailContactPopupEvent extends AbstractTideEvent 
	{		
		private var _event:Event;
		
		public function ShowEmailContactPopupEvent(event:Event=null):void
		{
			super();
			_event = event;
		}		
		
		public function get event():Event {
			return _event;
		}
	}
}