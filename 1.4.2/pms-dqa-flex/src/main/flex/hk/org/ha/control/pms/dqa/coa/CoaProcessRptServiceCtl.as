package hk.org.ha.control.pms.dqa.coa
{		
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaProcessRptListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaProcessRptServiceBean;
	
	import flash.events.Event;
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("coaProcessRptServiceCtl", restrict="false")]
	public class CoaProcessRptServiceCtl
	{
		
		[In]
		public var coaProcessRptService:CoaProcessRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaProcessRptList(evt:RetrieveCoaProcessRptListEvent):void
		{
			event = evt.event;
			coaProcessRptService.retrieveCoaProcessList(evt.coaProcessRptCriteria, retrieveCoaProcessRptListResult);
		
		}
		
		public function retrieveCoaProcessRptListResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
	}	
}