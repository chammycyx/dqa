package hk.org.ha.event.pms.dqa.sample {
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
	
	public class RefreshSampleItemEvent extends AbstractTideEvent
	{
		private var _screenName:String;
		
		private var _sampleItem:SampleItem;
		
		public function RefreshSampleItemEvent(screenName:String , sampleItem:SampleItem):void{
			super();
			_screenName = screenName;
			_sampleItem = sampleItem;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get sampleItem():SampleItem
		{
			return _sampleItem;
		}
	}		
}