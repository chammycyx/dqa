package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.view.pms.dqa.suppperf.popup.QaProblemPopup;
	import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
	
	public class UpdateQaProblemWithQaProblemEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblem:QaProblem;
		private var _qaProblemPopup:QaProblemPopup;
		private var _qaProblemFileUploadDataList:ArrayCollection;
		private var _qaProblemInstitutionListTemp:ArrayCollection;
		private var _qaProblemNatureSubCatList:ArrayCollection;
		private var _qaProblemInstitutionAllInstitutionFlag:YesNoFlag;
		
		public function UpdateQaProblemWithQaProblemEvent(qaProblem:QaProblem, qaProblemPopup:QaProblemPopup, 
														  qaProblemFileUploadDataList:ArrayCollection , 
														  qaProblemInstitutionListTemp:ArrayCollection, 
														  qaProblemNatureSubCatList:ArrayCollection, 
														  qaProblemInstitutionAllInstitutionFlag:YesNoFlag, event:Event=null):void {
			super();
			_event = event;
			_qaProblem = qaProblem;
			_qaProblemPopup = qaProblemPopup;
			_qaProblemFileUploadDataList = qaProblemFileUploadDataList;
			_qaProblemInstitutionListTemp = qaProblemInstitutionListTemp;
			_qaProblemNatureSubCatList = qaProblemNatureSubCatList;
			_qaProblemInstitutionAllInstitutionFlag = qaProblemInstitutionAllInstitutionFlag;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblem():QaProblem
		{
			return _qaProblem;
		}
		
		public function get qaProblemPopup():QaProblemPopup
		{
			return _qaProblemPopup;
		}
		
		public function get qaProblemFileUploadDataList():ArrayCollection
		{
			return _qaProblemFileUploadDataList;
		}
		
		public function get qaProblemInstitutionListTemp():ArrayCollection {
			return _qaProblemInstitutionListTemp;
		}
		
		public function get qaProblemNatureSubCatList():ArrayCollection {
			return _qaProblemNatureSubCatList;
		}
		
		public function get qaProblemInstitutionAllInstitutionFlag():YesNoFlag {
			return _qaProblemInstitutionAllInstitutionFlag;
		}
	}
}