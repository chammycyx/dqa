package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	public class ShowQaProblemPopupFileViewPopupEvent extends AbstractTideEvent 
	{	
		
		private var _qaProblem:QaProblem;
		private var _action:String
		private var _qaProblemFileUploadDataList:ArrayCollection;
		
		public function ShowQaProblemPopupFileViewPopupEvent(qaProblem:QaProblem, action:String, qaProblemFileUploadDataList:ArrayCollection):void
		{
			super();
			_qaProblem = qaProblem;
			_action = action;
			_qaProblemFileUploadDataList = qaProblemFileUploadDataList;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get qaProblemFileUploadDataList():ArrayCollection {
			return _qaProblemFileUploadDataList;
		}
		
	}
}