package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQaProblemPopupNatureOfPopupEvent extends AbstractTideEvent 
	{	
		private var _qaProblem:QaProblem;
		private var _problemNatureSubCatList:ArrayCollection;
		private var _problemNatureSubCatListSelect:ArrayCollection;
		private var _updateQaProblemNatureSubCat:Function;
		
		
		public function ShowQaProblemPopupNatureOfPopupEvent( qaProblem:QaProblem, 
															  problemNatureSubCatList:ArrayCollection, 
															  problemNatureSubCatListSelect:ArrayCollection,
															  updateQaProblemNatureSubCat:Function):void{
			super();
			_qaProblem = qaProblem;
			_problemNatureSubCatList = problemNatureSubCatList;
			_problemNatureSubCatListSelect = problemNatureSubCatListSelect;
			_updateQaProblemNatureSubCat = updateQaProblemNatureSubCat;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		public function get problemNatureSubCatList():ArrayCollection {
			return _problemNatureSubCatList;
		}
		public function get problemNatureSubCatListSelect():ArrayCollection {
			return _problemNatureSubCatListSelect;
		}
		public function get updateQaProblemNatureSubCat():Function {
			return _updateQaProblemNatureSubCat;
		}
	}
}