package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class RetrieveProblemNatureCatByProblemNatureCatIdEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _problemNatureCat:ProblemNatureCat;
		
		
		public function RetrieveProblemNatureCatByProblemNatureCatIdEvent(problemNatureCat:ProblemNatureCat, event:Event=null):void {
			super();
			_event = event;
			_problemNatureCat = problemNatureCat;			
		}
		
		public function get problemNatureCat():ProblemNatureCat 
		{
			return _problemNatureCat;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}