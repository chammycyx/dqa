package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDmDrugByItemCodeEvent extends AbstractTideEvent 
	{
		private var _itemCode:String;
		
		private var _screenName:String;
		
		public function RetrieveDmDrugByItemCodeEvent(itemCode:String, screenName:String):void {
			super();
			_itemCode = itemCode;
			_screenName = screenName;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}	
		
	}
}