package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCoaDiscrepancyRptViewEvent extends AbstractTideEvent 
	{		
		public function RefreshCoaDiscrepancyRptViewEvent():void 
		{
			super();
		}		
	}
}