package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	public class ShowQaProblemPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		private var _qaProblem:QaProblem;
		private var _screenName:String;
		
		public function ShowQaProblemPopupEvent(actionType:String, qaProblem:QaProblem, screenName:String):void
		{
			super();
			_actionType = actionType;
			_qaProblem = qaProblem;
			_screenName = screenName;
		}
		
		public function get action():String {
			return _actionType;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get screenName():String {
			return _screenName;
		}
		
	}
}