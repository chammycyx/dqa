package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;
	import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaProcessRptListEvent extends AbstractTideEvent 
	{
		private var _coaProcessRptCriteria:CoaProcessRptCriteria;
		
		private var _event:Event;
		
		public function RetrieveCoaProcessRptListEvent(coaProcessRptCriteria:CoaProcessRptCriteria,  event:Event=null):void 
		{
			super();
			_coaProcessRptCriteria = coaProcessRptCriteria;
			_event = event;
		}
		
		public function get coaProcessRptCriteria():CoaProcessRptCriteria {
			return _coaProcessRptCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}