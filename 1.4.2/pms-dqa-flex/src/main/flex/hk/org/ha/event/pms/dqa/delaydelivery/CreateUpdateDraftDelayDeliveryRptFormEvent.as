package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateUpdateDraftDelayDeliveryRptFormEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _delayDeliveryRptFormData:DelayDeliveryRptFormData;
		private var _delayDeliveryRptFormContactDataList:ArrayCollection;
		private var _delayDeliveryRptFormPoDataList:ArrayCollection;
		private var _emailFlag:Boolean;
		
		public function CreateUpdateDraftDelayDeliveryRptFormEvent(delayDeliveryRptFormData:DelayDeliveryRptFormData, 
																   delayDeliveryRptFormContactDataList:ArrayCollection,
																   delayDeliveryRptFormPoDataList:ArrayCollection,
																   emailFlag:Boolean=false,
																   callBackFunction:Function=null):void {
			super();
			_callBackFunction= callBackFunction;
			_delayDeliveryRptFormData = delayDeliveryRptFormData;
			_delayDeliveryRptFormContactDataList = delayDeliveryRptFormContactDataList;
			_delayDeliveryRptFormPoDataList = delayDeliveryRptFormPoDataList;
			_emailFlag= emailFlag;
		}
		
		public function get emailFlag():Boolean {
			return _emailFlag;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData
		{
			return _delayDeliveryRptFormData;
		}
		
		public function get delayDeliveryRptFormContactDataList():ArrayCollection
		{
			return _delayDeliveryRptFormContactDataList;
		}
		
		public function get delayDeliveryRptFormPoDataList():ArrayCollection
		{
			return _delayDeliveryRptFormPoDataList;
		}
	}
}