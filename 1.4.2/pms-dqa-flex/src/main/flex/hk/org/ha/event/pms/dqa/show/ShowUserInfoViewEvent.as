package hk.org.ha.event.pms.dqa.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowUserInfoViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowUserInfoViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
	
	
}