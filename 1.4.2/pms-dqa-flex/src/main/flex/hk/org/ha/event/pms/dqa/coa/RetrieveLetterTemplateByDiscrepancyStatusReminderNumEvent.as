package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveLetterTemplateByDiscrepancyStatusReminderNumEvent extends AbstractTideEvent 
	{
		private var _discrepancyStatus:DiscrepancyStatus;
		private var _reminderNum:Number;
		private var _event:Event;
		
		public function RetrieveLetterTemplateByDiscrepancyStatusReminderNumEvent(discrepancyStatus:DiscrepancyStatus, reminderNum:Number, event:Event=null):void 
		{
			super();
			_discrepancyStatus = discrepancyStatus;
			_reminderNum = reminderNum;
			_event = event;
		}
		
		public function get discrepancyStatus():DiscrepancyStatus {
			return _discrepancyStatus;
		}
		
		public function get reminderNum():Number {
			return _reminderNum;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}