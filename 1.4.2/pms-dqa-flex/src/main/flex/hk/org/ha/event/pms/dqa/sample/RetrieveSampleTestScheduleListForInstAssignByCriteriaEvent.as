package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _instAssignmentCriteria:InstAssignmentCriteria;
		
		public function RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent(instAssignmentCriteria:InstAssignmentCriteria, event:Event=null):void {
			super();
			_instAssignmentCriteria = instAssignmentCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get instAssignmentCriteria():InstAssignmentCriteria{
			return _instAssignmentCriteria;
		}
	}
}