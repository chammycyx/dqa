package hk.org.ha.control.pms.dqa{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.ChangeSupplierViewStateEvent;
	import hk.org.ha.event.pms.dqa.RetrieveSupplierBySupplierCodeEvent;
	import hk.org.ha.model.pms.dqa.biz.SupplierServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("supplierServiceCtl", restrict="true")]
	public class SupplierServiceCtl {

		private var supplierCode:String;
		
		private var event:Event;
		
		[In]
		public var supplierService:SupplierServiceBean;		
		
		[Observer]
		public function retrieveSupplierBySupplierCode(evt:RetrieveSupplierBySupplierCodeEvent):void 
		{		
			this.supplierCode = evt.supplierCode;
			event = evt.event;
			supplierService.retrieveSupplierBySupplierCode(this.supplierCode, retrieveSupplierBySupplierCodeResult);		
		}	
		
		private function retrieveSupplierBySupplierCodeResult(evt:TideResultEvent):void {
			if(event != null) {
				evt.context.dispatchEvent(event);				
			} else {
				evt.context.dispatchEvent(new ChangeSupplierViewStateEvent());
			}
		}
	}
}