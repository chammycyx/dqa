package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshInstitutionListByPcuForPopupEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		public function RefreshInstitutionListByPcuForPopupEvent(screenName:String):void {
			super();
			_screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}