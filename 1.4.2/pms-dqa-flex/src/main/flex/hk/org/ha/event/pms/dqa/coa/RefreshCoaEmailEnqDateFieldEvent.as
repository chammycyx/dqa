package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.fmk.pms.flex.events.AbstractTideEvent;
	
	public class RefreshCoaEmailEnqDateFieldEvent extends AbstractTideEvent 
	{		
		private var _event:Event;
		
		public function RefreshCoaEmailEnqDateFieldEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}