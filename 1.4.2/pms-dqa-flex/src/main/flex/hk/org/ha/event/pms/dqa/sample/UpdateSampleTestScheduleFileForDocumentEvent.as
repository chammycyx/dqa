package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSampleTestScheduleFileForDocumentEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestFile:SampleTestFile;
		private var _orgSampleTestSchedFile:SampleTestScheduleFile;
		
		public function UpdateSampleTestScheduleFileForDocumentEvent(sampleTestFile:SampleTestFile , orgSampleTestSchedFile:SampleTestScheduleFile, event:Event=null):void {
			super();
			_event = event;
			_sampleTestFile = sampleTestFile;
			_orgSampleTestSchedFile = orgSampleTestSchedFile;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestFile():SampleTestFile {
			return _sampleTestFile;
		}
		
		public function get orgSampleTestSchedFile():SampleTestScheduleFile {
			return _orgSampleTestSchedFile;
		}
		
	}
}