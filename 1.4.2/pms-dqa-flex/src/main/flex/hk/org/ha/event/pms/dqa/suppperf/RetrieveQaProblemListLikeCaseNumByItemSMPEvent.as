package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveQaProblemListLikeCaseNumByItemSMPEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		
		private var _caseNum:String;
		private var _itemCode:String;
		private var _supplierCode:String;
		private var _manufCode:String;
		private var _pharmCompanyCode:String;
		
		public function RetrieveQaProblemListLikeCaseNumByItemSMPEvent(caseNum:String, 
																		 itemCode:String,
																		 supplierCode:String,
																		 manufCode:String,
																		 pharmCompanyCode:String,
																		 event:Event=null):void {
			super();
			_event = event;
			_caseNum = caseNum;
			_itemCode = itemCode;
			_supplierCode = supplierCode;
			_manufCode = manufCode;
			_pharmCompanyCode = pharmCompanyCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get caseNum():String
		{
			return _caseNum;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get supplierCode():String
		{
			return _supplierCode;
		}
		
		public function get manufCode():String
		{
			return _manufCode;
		}
		
		public function get pharmCompanyCode():String
		{
			return _pharmCompanyCode;
		}
	}
}