package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaEmailEnqEvent extends AbstractTideEvent 
	{
		
		private var _event:Event;		
		
		public function RetrieveCoaEmailEnqEvent( event:Event=null ):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}