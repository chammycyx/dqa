package hk.org.ha.event.pms.dqa {
	
	import hk.org.ha.model.pms.dms.persistence.DmDrug;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDmDrugEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		private var _dmDrug:DmDrug;
		
		private var _success:Boolean;
		
		public function RefreshDmDrugEvent(screenName:String, dmDrug:DmDrug, success:Boolean):void {
			super();
			_screenName = screenName;
			_dmDrug = dmDrug;
			_success = success;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get dmDrug():DmDrug
		{
			return _dmDrug;
		}
		
		public function get success():Boolean
		{
			return _success;
		}
	}
}