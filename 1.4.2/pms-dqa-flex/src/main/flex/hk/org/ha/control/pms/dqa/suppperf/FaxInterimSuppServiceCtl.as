package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInterimSuppEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInterimSuppRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxInterimSuppRptEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.FaxInterimSuppServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.popup.FaxContactPopupInterimSuppPopup;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("faxInterimSuppServiceCtl", restrict="true")]
	public class FaxInterimSuppServiceCtl {
		
		private var event:Event;
		private var qaProblemIn:QaProblem;
		private var eventLogIn:EventLog;
		
		[In]
		public var faxInterimSuppService:FaxInterimSuppServiceBean;
		
		public var faxContactPopupInterimSuppPopupIn:FaxContactPopupInterimSuppPopup;
		
		[Observer]
		public function createFaxInterimSupp(evt:CreateFaxInterimSuppEvent):void 
		{		
			event = evt.event;
			qaProblemIn = evt.qaProblem;
			eventLogIn = evt.eventLog;
			faxContactPopupInterimSuppPopupIn = evt.faxContactPopupInterimSuppPopup;
			In(Object(faxInterimSuppService).success);
			In(Object(faxInterimSuppService).errorCode);
			In(Object(faxInterimSuppService).refNum);
			
			faxInterimSuppService.createFaxInterimSupp(evt.qaProblem, evt.eventLog, evt.faxDetailPharmProblemList, createFaxInterimSuppResult);		
		}	
		
		private function createFaxInterimSuppResult(evt:TideResultEvent):void {
			if (Object(faxInterimSuppService).success ){
				faxContactPopupInterimSuppPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
				else
				{
					evt.context.dispatchEvent(new CreateFaxInterimSuppRptEvent(qaProblemIn,
												eventLogIn,
												Object(faxInterimSuppService).refNum,
												false,
												new ShowFaxInterimSuppRptEvent()));
					
				}
			}else{
				if (Object(faxInterimSuppService).errorCode !=null){
					faxContactPopupInterimSuppPopupIn.showMessage(Object(faxInterimSuppService).errorCode );
				}
			}
		}
		
		[Observer]
		public function createFaxInterimSuppRpt(evt:CreateFaxInterimSuppRptEvent):void 
		{		
			event = evt.event;
			faxInterimSuppService.createFaxInterimSuppRpt(evt.qaProblem, evt.eventLog, evt.refNum, evt.reGenFlag, createFaxInterimSuppRptResult);		
		}	
		
		private function createFaxInterimSuppRptResult(evt:TideResultEvent):void {
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}