package hk.org.ha.event.pms.dqa{
	
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrieveSupplierCodeListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function RetrieveSupplierCodeListEvent(event:Event=null):void {
			super();
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
	}
}