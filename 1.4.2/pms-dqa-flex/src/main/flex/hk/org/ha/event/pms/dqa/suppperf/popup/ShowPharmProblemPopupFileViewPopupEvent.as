package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	public class ShowPharmProblemPopupFileViewPopupEvent extends AbstractTideEvent 
	{	
		
		private var _pharmProblem:PharmProblem;
		private var _action:String
		private var _pharmProblemFileUploadDataList:ArrayCollection;
		
		public function ShowPharmProblemPopupFileViewPopupEvent(pharmProblem:PharmProblem, action:String, pharmProblemFileUploadDataList:ArrayCollection):void
		{
			super();
			_pharmProblem = pharmProblem;
			_action = action;
			_pharmProblemFileUploadDataList = pharmProblemFileUploadDataList;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get pharmProblemFileUploadDataList():ArrayCollection {
			return _pharmProblemFileUploadDataList;
		}
		
	}
}