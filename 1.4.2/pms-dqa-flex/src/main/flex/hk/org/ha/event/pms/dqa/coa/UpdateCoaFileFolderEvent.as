package hk.org.ha.event.pms.dqa.coa {
	
	import flash.utils.*;
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;

	public class UpdateCoaFileFolderEvent extends AbstractTideEvent 
	{
		

		private var _coaFileFolder:CoaFileFolder;
		
		
		public function UpdateCoaFileFolderEvent(coaFileFolder:CoaFileFolder):void {
			super();
		
			_coaFileFolder = coaFileFolder;			
		}

		
		public function get coaFileFolder():CoaFileFolder
		{
			return _coaFileFolder;
		}
		
	}
}