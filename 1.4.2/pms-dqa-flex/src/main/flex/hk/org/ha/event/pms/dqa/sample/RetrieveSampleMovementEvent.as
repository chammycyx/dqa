package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleMovementEvent extends AbstractTideEvent 
	{
		private var _sampleMovement:SampleMovement;
		
		private var _action:String;
		public function RetrieveSampleMovementEvent(sampleMovement:SampleMovement, action:String = null):void {
			super();
			_sampleMovement = sampleMovement;
			_action = action;
		}
		
		public function get sampleMovement():SampleMovement 
		{
			return _sampleMovement;
		}
		
		public function get action():String {
			return _action;
		}
		
	}
}