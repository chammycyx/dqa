package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RefreshInEmailEnqPropEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaInEmailListForInEmailEnqEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaInEmailListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaInEmailListServiceCtl", restrict="false")]
	public class CoaInEmailListServiceCtl
	{
	
		[In]
		public var coaInEmailListService:CoaInEmailListServiceBean;
		
		[In]
		public var maxReceiveDateMonthRangeForInEmailEnq:Number;
		
		[In]
		public var defaultReceiveDateMonthRangeForInEmailEnq:Number;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaInEmailListForInEmailEnq(evt:RetrieveCoaInEmailListForInEmailEnqEvent):void 
		{						
			event = evt.event;			
			coaInEmailListService.retrieveCoaInEmailListForInEmailEnq( evt.fromReceiveDate, evt.toReceiveDate, evt.resultFlag, retrieveCoaOutstandBatchListForOutstandBatchCoaResult );
		}
		
		private function retrieveCoaOutstandBatchListForOutstandBatchCoaResult(evt:TideResultEvent):void 
		{				
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function refreshPropValueForInEmailEnq(evt:RefreshInEmailEnqPropEvent):void
		{
			event = evt.event;
			coaInEmailListService.getDefaultReceiveDateMonthRangeForInEmailEnq();
			coaInEmailListService.getMaxReceiveDateMonthRangeForInEmailEnq( refreshPropValueForInEmailEnqResult );
		}
		
		private function refreshPropValueForInEmailEnqResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
	
	}
}