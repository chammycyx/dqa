package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmProblemPopupDescriptionPopupEvent extends AbstractTideEvent 
	{	
		
		public var _description:String;
		public var _currentState:String;
		
		public function ShowPharmProblemPopupDescriptionPopupEvent(description:String,currentState:String):void
		{
			_description = description;
			_currentState= currentState;
			super();
		}
		
		public function get description():String {
			return _description;
		}
		
		public function get currentState():String {
			return _currentState;
		}
	}
}