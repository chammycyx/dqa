package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;

	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaProcessingDateRptListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaProcessingDateRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("coaProcessingDateRptServiceCtl", restrict="false")]
	public class CoaProcessingDateRptServiceCtl
	{
		
		[In]
		public var coaProcessingDateRptService:CoaProcessingDateRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaProcessingDateRptList(evt:RetrieveCoaProcessingDateRptListEvent):void
		{
			event = evt.event;
			coaProcessingDateRptService.retrieveCoaProcessingDateRptList(evt.coaProcessingDateRptCriteria, retrieveCoaProcessingDateRptListResult);
		
		}
		
		public function retrieveCoaProcessingDateRptListResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
	}	
}