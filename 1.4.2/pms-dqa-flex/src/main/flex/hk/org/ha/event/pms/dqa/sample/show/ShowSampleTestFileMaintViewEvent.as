package hk.org.ha.event.pms.dqa.sample.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSampleTestFileMaintViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowSampleTestFileMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}