package hk.org.ha.event.pms.dqa.suppperf.popup {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQaProblemPopupEventLogPopupEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _eventLog:EventLog; 
		private var _eventLogList:ListCollectionView; 
		private var _action:String;
		private var _selectedIndexIn:int;
		
		public function ShowQaProblemPopupEventLogPopupEvent(eventLog:EventLog, 
															   eventLogList:ListCollectionView,
															   action:String,
															   selectedIndexIn:int,
															   event:Event=null):void {
			super();
			_event = event;
			_eventLog= eventLog; 
			_eventLogList = eventLogList; 
			_action = action;
			_selectedIndexIn = selectedIndexIn;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get eventLog():EventLog {
			return _eventLog;
		}
		
		public function get eventLogList():ListCollectionView {
			return _eventLogList;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get selectedIndexIn():int {
			return _selectedIndexIn;
		}
		
	}
}