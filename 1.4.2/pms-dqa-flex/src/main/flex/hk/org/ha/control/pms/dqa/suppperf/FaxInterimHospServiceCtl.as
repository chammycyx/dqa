package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInterimHospEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInterimHospRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxInterimHospRptEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.FaxInterimHospServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.popup.FaxContactPopupInterimHospPopup;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("faxInterimHospServiceCtl", restrict="true")]
	public class FaxInterimHospServiceCtl {
		
		private var event:Event;
		private var qaProblemIn:QaProblem;
		private var eventLogIn:EventLog;
		
		[In]
		public var faxInterimHospService:FaxInterimHospServiceBean;
		
		public var faxContactPopupInterimHospPopupIn:FaxContactPopupInterimHospPopup;
		
		[Observer]
		public function createFaxInterimHosp(evt:CreateFaxInterimHospEvent):void 
		{		
			event = evt.event;
			qaProblemIn = evt.qaProblem;
			eventLogIn = evt.eventLog;
			faxContactPopupInterimHospPopupIn = evt.faxContactPopupInterimHospPopup;
			In(Object(faxInterimHospService).success);
			In(Object(faxInterimHospService).errorCode);
			In(Object(faxInterimHospService).refNum);
			
			faxInterimHospService.createFaxInterimHosp(evt.qaProblem, evt.eventLog, evt.faxDetailPharmProblemList, createFaxInterimHospResult);		
		}	
		
		private function createFaxInterimHospResult(evt:TideResultEvent):void {
			if (Object(faxInterimHospService).success ){
				faxContactPopupInterimHospPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
				else
				{
					evt.context.dispatchEvent(new CreateFaxInterimHospRptEvent(qaProblemIn,
																				eventLogIn,
																				Object(faxInterimHospService).refNum,
																				false,
																				new ShowFaxInterimHospRptEvent()));
					
				}
			}else{
				if (Object(faxInterimHospService).errorCode !=null){
					faxContactPopupInterimHospPopupIn.showMessage(Object(faxInterimHospService).errorCode );
				}
			}
		}
		
		[Observer]
		public function createFaxInterimHospRpt(evt:CreateFaxInterimHospRptEvent):void 
		{		
			event = evt.event;
			faxInterimHospService.createFaxInterimHospRpt(evt.qaProblem, evt.eventLog, evt.refNum, evt.reGenFlag, createFaxInterimHospRptResult);		
		}	
		
		private function createFaxInterimHospRptResult(evt:TideResultEvent):void {
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}