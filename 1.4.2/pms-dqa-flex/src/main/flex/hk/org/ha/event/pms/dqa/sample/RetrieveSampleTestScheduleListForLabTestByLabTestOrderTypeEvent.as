package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.udt.OrderType;
	
	public class RetrieveSampleTestScheduleListForLabTestByLabTestOrderTypeEvent extends AbstractTideEvent 
	{
		private var _labCode:String;
		private var _testCode:String;
		private var _orderType:OrderType;
		
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleListForLabTestByLabTestOrderTypeEvent(labCode:String, testCode:String, orderType:OrderType, event:Event=null):void {
			super();
			_labCode = labCode;
			_testCode = testCode;
			_orderType = orderType;
			_event = event;
		}
		
		public function get labCode():String 
		{
			return _labCode;
		}
		
		public function get testCode():String 
		{
			return _testCode;
		}
		
		public function get orderType():OrderType 
		{
			return _orderType;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}