package hk.org.ha.event.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestFileListBySampleTestFileCatEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestFileCat:SampleTestFileCat;
		
		public function RetrieveSampleTestFileListBySampleTestFileCatEvent(sampleTestFileCat:SampleTestFileCat, event:Event=null):void {
			super();
			_event = event;
			_sampleTestFileCat = sampleTestFileCat;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestFileCat():SampleTestFileCat
		{
			return _sampleTestFileCat;
		}
	}
}