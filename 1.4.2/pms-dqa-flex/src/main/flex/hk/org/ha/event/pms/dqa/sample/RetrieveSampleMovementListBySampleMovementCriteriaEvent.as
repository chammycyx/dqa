package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleMovementCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleMovementListBySampleMovementCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleMovementCriteria:SampleMovementCriteria;
		
		public function RetrieveSampleMovementListBySampleMovementCriteriaEvent(sampleMovementCriteria:SampleMovementCriteria, event:Event=null):void {
			super();
			_sampleMovementCriteria = sampleMovementCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleMovementCriteria():SampleMovementCriteria{
			return _sampleMovementCriteria;
		}
	}
}