package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;
	
	public class RetrieveQaProblemListByQaProblemCriteriaEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblemCriteria:QaProblemCriteria;
		private var _screenName:String;
		
		public function RetrieveQaProblemListByQaProblemCriteriaEvent(qaProblemCriteria:QaProblemCriteria, screenName:String, event:Event=null):void {
			super();
			_event = event;
			_qaProblemCriteria = qaProblemCriteria;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblemCriteria():QaProblemCriteria
		{
			return _qaProblemCriteria;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}