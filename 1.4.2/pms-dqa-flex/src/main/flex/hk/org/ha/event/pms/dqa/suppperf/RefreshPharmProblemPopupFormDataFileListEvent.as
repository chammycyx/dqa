package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	
	public class RefreshPharmProblemPopupFormDataFileListEvent extends AbstractTideEvent 
	{
		private var _pharmProblemFileUploadDataList:ArrayCollection;
		
		
		public function RefreshPharmProblemPopupFormDataFileListEvent(pharmProblemFileUploadDataList:ArrayCollection):void {
			super();
			_pharmProblemFileUploadDataList = pharmProblemFileUploadDataList;			
		}
		
		public function get pharmProblemFileUploadDataList():ArrayCollection 
		{
			return _pharmProblemFileUploadDataList;
		}
	}
}