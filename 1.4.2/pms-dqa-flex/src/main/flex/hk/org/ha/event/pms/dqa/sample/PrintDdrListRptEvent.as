package hk.org.ha.event.pms.dqa.sample {
	
	//import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;
	import hk.org.ha.model.pms.dqa.vo.sample.DdrRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintDdrListRptEvent extends AbstractTideEvent 
	{
		private var _ddrRptCriteria:DdrRptCriteria;
		
		public function PrintDdrListRptEvent(ddrRptCriteria:DdrRptCriteria):void 
		{
			super();
			_ddrRptCriteria = ddrRptCriteria;
		}
		
		public function get ddrRptCriteria():DdrRptCriteria {
			return _ddrRptCriteria;
		}
	}
}
