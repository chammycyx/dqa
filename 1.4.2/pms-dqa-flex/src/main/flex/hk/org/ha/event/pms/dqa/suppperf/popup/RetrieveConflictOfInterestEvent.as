package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveConflictOfInterestEvent extends AbstractTideEvent 
	{	
		private var _callBackFunc:Function;
		
		public function RetrieveConflictOfInterestEvent(callBackFuncValue:Function):void
		{
			super();
			_callBackFunc = callBackFuncValue;
		}
		
		public function get callBackFunc():Function
		{
			return _callBackFunc;
		}
	}
}