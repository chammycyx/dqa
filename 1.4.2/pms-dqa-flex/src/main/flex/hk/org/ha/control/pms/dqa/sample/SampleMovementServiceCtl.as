package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.CreateSampleMovementEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleMovementEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleMovementEvent;
	import hk.org.ha.event.pms.dqa.sample.popup.ShowSampleTestInventoryPopupEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowSampleTestInventoryViewEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleMovementServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
	import hk.org.ha.view.pms.dqa.sample.popup.SampleTestInventoryPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleMovementServiceCtl", restrict="true")]
	public class SampleMovementServiceCtl {
		
		[In]
		public var sampleMovementService:SampleMovementServiceBean;
		
		[In]
		public var sampleTestInventoryPopup:SampleTestInventoryPopup;
		
		private var action:String;
		
		[Observer]
		public function retrieveSampleMovement(evt:RetrieveSampleMovementEvent):void {
			action = evt.action;
			sampleMovementService.retrieveSampleMovement(evt.sampleMovement, retrieveSampleMovementResult);
		}
		
		public function retrieveSampleMovementResult(evt: TideResultEvent):void {
			if( evt.result != null){
				var event:Event = new ShowSampleTestInventoryPopupEvent(action,evt.result as SampleMovement); 
				evt.context.dispatchEvent( event );
			}
				
		}
		
		[Observer]
		public function createSampleMovement(evt:CreateSampleMovementEvent):void{
			In(Object(sampleMovementService).success);
			sampleMovementService.createSampleMovement(evt.sampleMovement, createSampleMovementResult);
		}
		
		public function createSampleMovementResult(evt:TideResultEvent):void{
			if (Object(sampleMovementService).success ){
				sampleTestInventoryPopup.closeWin();
				var event:Event = new ShowSampleTestInventoryViewEvent();
				evt.context.dispatchEvent( event );
			}	
		}
		
		
		[Observer]
		public function updateSampleMovement(evt:UpdateSampleMovementEvent):void{
			In(Object(sampleMovementService).success);
			sampleMovementService.updateSampleMovement(evt.sampleMovement, updateSampleMovementResult);	
		}
		
		public function updateSampleMovementResult(evt:TideResultEvent):void{
			if (Object(sampleMovementService).success ){
				sampleTestInventoryPopup.closeWin();
				var event:Event = new ShowSampleTestInventoryViewEvent();
				evt.context.dispatchEvent( event );
			}
		}
		
		
	}
}
