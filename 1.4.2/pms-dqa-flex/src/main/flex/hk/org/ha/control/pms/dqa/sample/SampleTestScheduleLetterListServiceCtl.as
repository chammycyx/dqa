package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateSampleTestScheduleLetterEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleLetterListServiceBean;
		
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleLetterListServiceCtl", restrict="true")]
	public class SampleTestScheduleLetterListServiceCtl {
		[In]
		public var sampleTestScheduleLetterListService:SampleTestScheduleLetterListServiceBean;
		
		
		private var event:Event;
		
		
		[Observer]
		public function retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(evt:RetrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaEvent):void{
			event = evt.event;
			sampleTestScheduleLetterListService.retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(evt.sampleTestEmailEnquiryCriteria, retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaResult);
			
		}
		
		public function retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteriaResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function createSampleTestScheduleLetter(evt:CreateSampleTestScheduleLetterEvent):void{
			event = evt.event;
			sampleTestScheduleLetterListService.createSampleTestScheduleLetter(evt.letterTemplate,	evt.refNum, evt.sampleTestScheduleList, evt.sampleTestSchedule, createSampleTestScheduleLetterResult);
		}
		
		public function createSampleTestScheduleLetterResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
	}
}
