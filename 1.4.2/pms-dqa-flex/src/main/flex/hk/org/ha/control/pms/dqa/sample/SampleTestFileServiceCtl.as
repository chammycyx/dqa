package hk.org.ha.control.pms.dqa.sample{
	import hk.org.ha.event.pms.dqa.sample.AddSampleTestFileEvent;
	import hk.org.ha.event.pms.dqa.sample.AddSampleTestFileForTestResultEvent;
	import hk.org.ha.event.pms.dqa.sample.AddSampleTestScheduleFileForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.ClearSampleTestFileListEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateSampleTestFileEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteSampleTestFileEvent;
	import hk.org.ha.event.pms.dqa.sample.RefreshDocumentDetailDocEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileBySampleTestFileIdEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestFileListEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestFileEvent;
	import hk.org.ha.event.pms.dqa.sample.UploadSampleTestFileEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowSampleTestFileMaintViewEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestFileServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	import hk.org.ha.view.pms.dqa.sample.SampleTestFileMaintView;
	import hk.org.ha.view.pms.dqa.sample.popup.DocumentFileUploadPopup;
	import hk.org.ha.view.pms.dqa.sample.popup.SampleTestFileMaintPopup;
	import hk.org.ha.view.pms.dqa.sample.popup.TestResultFileUploadPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;

	[Bindable]
	[Name("sampleTestFileServiceCtl", restrict="true")]
	public class SampleTestFileServiceCtl {
		
		[In]
		public var sampleTestFileService:SampleTestFileServiceBean;
		
		[In]
		public var sampleTestFileMaintPopup:SampleTestFileMaintPopup;
		
		[In]
		public var documentFileUploadPopup:DocumentFileUploadPopup;

		[In]
		public var sampleTestFileMaintView:SampleTestFileMaintView;
		
		[In]
		public var testResultFileUploadPopup:TestResultFileUploadPopup;

		[In]
		public var sampleTestFile:SampleTestFile;
		
		private var event:Event;
		
		private var screenName:String;
		
		[Observer]
		public function retrieveSampleTestFileBySampleTestFileId(evt:RetrieveSampleTestFileBySampleTestFileIdEvent):void{
			event = evt.event;
			sampleTestFileService.retrieveSampleTestFileBySampleTestFileId(evt.sampleTestFileId, retrieveSampleTestFileBySampleTestFileIdResult);
		}
		
		public function retrieveSampleTestFileBySampleTestFileIdResult(evt:TideResultEvent):void{
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function uploadSampleTestFile(evt:UploadSampleTestFileEvent):void{
			In(Object(sampleTestFileService).success);
			In(Object(sampleTestFileService).errorCode);
			screenName = evt.screenName;
			sampleTestFileService.uploadFile(evt.data, evt.fileName, evt.sampleTestFile, uploadSampleTestFileResult);
		}
		
		public function uploadSampleTestFileResult(evt:TideResultEvent):void{

			if (Object(sampleTestFileService).success){
				trace("sampleTestFile created id : "+sampleTestFile.sampleTestFileId);
				if (screenName == "sampleTestFileMaintPopup"){
					sampleTestFileMaintPopup.closeWin();
					evt.context.dispatchEvent(new RetrieveSampleTestFileListEvent());
				}else if (screenName == "documentFileUploadPopup"){
					documentFileUploadPopup.closeWin();
					evt.context.dispatchEvent(new AddSampleTestScheduleFileForDocumentEvent(sampleTestFile));
				}else if (screenName == "testResultFileUploadPopup"){
					testResultFileUploadPopup.closeWin();
					evt.context.dispatchEvent(new AddSampleTestFileForTestResultEvent(sampleTestFile));
				}
			}else if (Object(sampleTestFileService).errorCode !=null){
				if (screenName == "sampleTestFileMaintPopup"){
					sampleTestFileMaintPopup.showMessage(Object(sampleTestFileService).errorCode);
				} else if (screenName == "testResultFileUploadPopup") {
					testResultFileUploadPopup.closeWin();
				}
			}
		}
		
		
		[Observer]
		public function addSampleTestFileForTestResult(evt:AddSampleTestFileForTestResultEvent):void{
			event = evt.event;
			sampleTestFileService.addSampleTestFileForTestResult(evt.sampleTestFile, addSampleTestFileForTestResultResult);
		}
		
		public function addSampleTestFileForTestResultResult(evt:TideResultEvent):void {		
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		

		[Observer]
		public function addSampleTestFile(evt:AddSampleTestFileEvent):void{
			event = evt.event;
			In(Object(sampleTestFileService).success);
			sampleTestFileService.addSampleTestFile( addSampleTestFileResult);
		}
		
		public function addSampleTestFileResult(evt:TideResultEvent):void {		
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateSampleTestFile(evt:UpdateSampleTestFileEvent):void{
			event = evt.event;
			In(Object(sampleTestFileService).success);
			sampleTestFileService.updateSampleTestFile(updateSampleTestFileResult);
		
		}
		
		public function updateSampleTestFileResult(evt:TideResultEvent):void {
			if (Object(sampleTestFileService).success){
				sampleTestFileMaintPopup.closeWin();
				
				if ( event != null){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
		[Observer]
		public function deleteSampleTestFile(evt:DeleteSampleTestFileEvent):void{
			event = evt.event;
			In(Object(sampleTestFileService).success);
			In(Object(sampleTestFileService).errorCode);
			sampleTestFileService.deleteSampleTestFile(evt.sampleTestFile, deleteSampleTestFileResult);
		}
		
		public function deleteSampleTestFileResult(evt:TideResultEvent):void{
			if (Object(sampleTestFileService).success){
				if ( event != null){
					evt.context.dispatchEvent( event );
				}
			}else {
				if (Object(sampleTestFileService).errorCode != null){
					sampleTestFileMaintView.showMessage(Object(sampleTestFileService).errorCode);
				}
			}
		}
	}
}