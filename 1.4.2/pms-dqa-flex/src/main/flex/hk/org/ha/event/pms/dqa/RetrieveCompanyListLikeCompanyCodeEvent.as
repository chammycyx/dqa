package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;
	import flash.events.Event;
	
	public class RetrieveCompanyListLikeCompanyCodeEvent extends AbstractTideEvent 
	{
		private var _pharmCompanyManufCode:String;
		
		private var _event:Event;
		
		public function RetrieveCompanyListLikeCompanyCodeEvent(pharmCompanyManufCode:String, event:Event=null):void {
			super();
			_pharmCompanyManufCode = pharmCompanyManufCode;
			_event = event;
		}
		
		public function get pharmCompanyManufCode():String
		{
			return _pharmCompanyManufCode;
		}
	
		public function get event():Event {
			return _event;
		}	
	}
}