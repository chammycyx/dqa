package hk.org.ha.event.pms.dqa.suppperf.popup {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.view.pms.dqa.suppperf.popup.PharmProblemPopup;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class ShowPharmProblemPopupCopyBatchNumPopupEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _pharmProblem:PharmProblem; 
		private var _qaProblem:QaProblem; 
		private var _pharmProblemPopup:PharmProblemPopup;
		private var _screenName:String;
		
		public function ShowPharmProblemPopupCopyBatchNumPopupEvent(pharmProblem:PharmProblem, 
																			qaProblem:QaProblem,
																			pharmProblemPopup:PharmProblemPopup,
																			screenName:String,
																			event:Event=null):void {
			super();
			_event = event;
			_pharmProblem = pharmProblem; 
			_qaProblem = qaProblem; 
			_pharmProblemPopup = pharmProblemPopup;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get pharmProblemPopup():PharmProblemPopup {
			return _pharmProblemPopup;
		}
		
		public function get screenName():String {
			return _screenName;
		}
		
	}
}