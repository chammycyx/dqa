package hk.org.ha.event.pms.dqa.sample.popup
{	
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;
	
	public class ShowEmailMemoPopupEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestScheduleList:ListCollectionView;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _userInfo:UserInfo;
		private var _supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact;
		
		public function ShowEmailMemoPopupEvent(sampleTestScheduleList:ListCollectionView, sampleTestSchedule:SampleTestSchedule, userInfo:UserInfo, supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact, event:Event=null):void
		{
			super();
			_event = event;
			_sampleTestScheduleList = sampleTestScheduleList;
			_sampleTestSchedule = sampleTestSchedule;
			_userInfo = userInfo;
			_supplierPharmCompanyManufacturerContact = supplierPharmCompanyManufacturerContact;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleList():ListCollectionView {
			return _sampleTestScheduleList;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
		
		public function get userInfo():UserInfo {
			return _userInfo;
		}
		
		public function get supplierPharmCompanyManufacturerContact():SupplierPharmCompanyManufacturerContact {
			return _supplierPharmCompanyManufacturerContact;
		}
	}
}