package hk.org.ha.event.pms.dqa.suppperf {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import flash.events.Event;
	
	public class AddProblemNatureSubCatEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function AddProblemNatureSubCatEvent(event:Event=null):void { 
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}