package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class DeleteSampleTestScheduleEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _cancelSchedReason:CancelSchedReason;
		
		public function DeleteSampleTestScheduleEvent(sampleTestSchedule:SampleTestSchedule, cancelSchedReason:CancelSchedReason, event:Event=null):void {
			super();
			_sampleTestSchedule = sampleTestSchedule;
			_event = event;
			_cancelSchedReason = cancelSchedReason;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule 
		{
			return _sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get cancelSchedReason():CancelSchedReason {
			return _cancelSchedReason;
		}
	}
}