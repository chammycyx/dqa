package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.ModuleType;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveContractListForSampleTestEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _contractSuffix:String;
		private var _orderType:OrderType;
		private var _itemCode:String;
		private var _screenName:String;
		private var _moduleType:ModuleType;
		private var _event:Event;
		
		public function RetrieveContractListForSampleTestEvent(contractNum:String, contractSuffix:String, itemCode:String, orderType:OrderType, moduleType:ModuleType, screenName:String, event:Event=null):void {
			super();
			_contractNum = contractNum;
			_contractSuffix = contractSuffix;
			_orderType = orderType;
			_itemCode = itemCode;
			_screenName = screenName;
			_moduleType = moduleType;
			_event = event;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		
		public function get contractSuffix():String 
		{
			return _contractSuffix;
		}
		
		public function get orderType():OrderType
		{
			return _orderType;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get moduleType():ModuleType
		{
			return _moduleType;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}