package hk.org.ha.event.pms.dqa.sample.show {
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowLabTestDetailEditModeEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function ShowLabTestDetailEditModeEvent(event:Event=null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}

