package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelayDeliveryRptFormContactQuickAddEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _institution:Institution;
		
		public function RetrieveDelayDeliveryRptFormContactQuickAddEvent(institution:Institution, callBackFunction:Function):void {
			super();
			_callBackFunction= callBackFunction;
			_institution = institution;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get institution():Institution
		{
			return _institution;
		}
	}
}