package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveFullInstitutionListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveInstitutionListByPcuEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveInstitutionListByPcuFlagEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveInstitutionListByPcuFlagRecordStatusEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveInstitutionListByPcuForPopupEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrievePcuListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrievePcuListForPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.FullInstitutionListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.InstitutionListByPcuForPopupServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.InstitutionListByPcuServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.InstitutionListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.PcuListForPopupServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.PcuListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	
	[Bindable]
	[Name("institutionListServiceCtl", restrict="true")]
	public class InstitutionListServiceCtl {
		
	
		[In]
		public var institutionListService:InstitutionListServiceBean;
		
		[In]
		public var fullInstitutionListService:FullInstitutionListServiceBean;
		
		[In]
		public var pcuListService:PcuListServiceBean;
		
		[In]
		public var institutionListByPcuService:InstitutionListByPcuServiceBean;
		
		[In]
		public var pcuListForPopupService:PcuListForPopupServiceBean;
		
		[In]
		public var institutionListByPcuForPopupService:InstitutionListByPcuForPopupServiceBean;
		
		[In]
		public var ctx:Context;
		
		private var event:Event;
	
		[Observer]
		public function retrieveInstitutionListByPcuFlag(evt:RetrieveInstitutionListByPcuFlagEvent):void {
			event = evt.event;
			institutionListService.retreiveInstitutionListByPcuFlag(evt.pcuFlag);
		}
		
		public function retrieveInstitutionListByPcuFlagResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveInstitutionList(evt:RetrieveInstitutionListByPcuFlagRecordStatusEvent):void {
			event = evt.event;
			institutionListService.retrieveInstitutionListByPcuFlagRecordStatus(evt.pcuFlag, evt.recordStatus);
		}
		
		public function retrieveInstitutionListResultByPcuFlagRecordStatus(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveFullInstitutionList(evt:RetrieveFullInstitutionListEvent):void {
			ctx.fullInstitutionList = null;
			event = evt.event;
			fullInstitutionListService.retrieveFullInstitutionList(retrieveFullInstitutionListResult);
		}
		
		public function retrieveFullInstitutionListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrievePcuList(evt:RetrievePcuListEvent):void {
			event = evt.event;
			pcuListService.retrievePcuList(retrievePcuListResult);
		}
		
		public function retrievePcuListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveInstitutionListByPcu(evt:RetrieveInstitutionListByPcuEvent):void {
			event = evt.event;
			institutionListByPcuService.retrieveInstitutionListByPcu(evt.pcuCode, retrieveInstitutionListByPcuResult);
		}
		
		public function retrieveInstitutionListByPcuResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrievePcuListForPopup(evt:RetrievePcuListForPopupEvent):void {
			event = evt.event;
			pcuListForPopupService.retrievePcuListForPopup(retrievePcuListForPopupResult);
		}
		
		public function retrievePcuListForPopupResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveInstitutionListByPcuForPopup(evt:RetrieveInstitutionListByPcuForPopupEvent):void {
			event = evt.event;
			institutionListByPcuForPopupService.retrieveInstitutionListByPcuForPopup(evt.pcuCode, retrieveInstitutionListByPcuForPopupResult);
		}
		
		public function retrieveInstitutionListByPcuForPopupResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
