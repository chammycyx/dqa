package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSupplierListEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		public function RefreshSupplierListEvent(screenName:String):void {
			super();
			_screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}