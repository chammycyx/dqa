package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureSubCatListEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureSubCatListFullEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowPharmProblemPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureSubCatListServiceBean;
	
	import hk.org.ha.view.pms.dqa.suppperf.NatureOfProblemMaintView;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureSubCatListServiceCtl", restrict="true")]
	public class ProblemNatureSubCatListServiceCtl {
		
		[In]       
		public var problemNatureSubCatListService:ProblemNatureSubCatListServiceBean;
		
		[In]
		public var natureOfProblemMaintView:NatureOfProblemMaintView;
		
		[In]       
		public var problemNatureSubCatList:ArrayCollection;
		
		[In]       
		public var qaProblemNatureSubCatList:ArrayCollection;
		
		
		private var event:Event;
		private var screenName:String;
		private var problemNatureCatIn:ProblemNatureCat;
		
		
		[Observer]
		public function retrieveProblemNatureSubCatList(evt:RetrieveProblemNatureSubCatListEvent):void{
			event = evt.event;
			screenName = evt.screenName;
			problemNatureCatIn = evt.problemNatureCat;
			problemNatureSubCatListService.retrieveProblemNatureSubCatList(evt.problemNatureCat, retrieveProblemNatureSubCatListResult);
		}
		
		public function retrieveProblemNatureSubCatListResult(evt:TideResultEvent):void{
			if(screenName == "natureOfProblemMaintView")
			{
				natureOfProblemMaintView.txtSubCatParamDisplayOrder.text =  String(problemNatureCatIn.problemNatureParam.displayOrder);
				natureOfProblemMaintView.txtSubCatParamDesc.text = problemNatureCatIn.problemNatureParam.paramDesc;
				natureOfProblemMaintView.txtSubCatCatDisplayOrder.text =  String(problemNatureCatIn.displayOrder);
				natureOfProblemMaintView.txtSubCatCatDesc.text = problemNatureCatIn.catDesc;
				natureOfProblemMaintView.problemNatureParamSelect = problemNatureCatIn.problemNatureParam;
				natureOfProblemMaintView.problemNatureCatSelect = problemNatureCatIn;
				
			}
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
			
		}
		
		[Observer]
		public function retrieveProblemNatureSubCatListFull(evt:RetrieveProblemNatureSubCatListFullEvent):void{
			problemNatureSubCatListService.retrieveProblemNatureSubCatListFull(function(tideResultEvent:TideResultEvent):void {
				if (evt.callBack != null) {
					evt.callBack(problemNatureSubCatList);
				}
			});
		}
		
		
	}
}
