package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSupplierListLikeEvent extends AbstractTideEvent 
	{	
		
		private var _supplierCode:String;
		
		private var _event:Event;
		
		public function RetrieveSupplierListLikeEvent(supplierCode:String, event:Event=null):void
		{
			super();
			_supplierCode = supplierCode;
			_event = event;
		}
		
		public function get supplierCode():String 
		{
			return _supplierCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}
