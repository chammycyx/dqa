package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePharmCompanyListLikeEvent extends AbstractTideEvent 
	{	
		
		private var _pharmCompanyCode:String;
		
		private var _event:Event;
		
		public function RetrievePharmCompanyListLikeEvent(pharmCompanyCode:String, event:Event=null):void
		{
			super();
			_pharmCompanyCode = pharmCompanyCode;
			_event = event;
		}
		
		public function get pharmCompanyCode():String 
		{
			return _pharmCompanyCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}
