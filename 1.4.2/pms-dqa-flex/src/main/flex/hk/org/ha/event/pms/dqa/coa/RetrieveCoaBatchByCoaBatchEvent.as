package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchByCoaBatchEvent extends AbstractTideEvent 
	{
		
		private var _coaBatch:CoaBatch;
		
		private var _event:Event;
		
		private var _refreshEvent:Event;
		
		private var _status:CoaStatus;		
		
		public function RetrieveCoaBatchByCoaBatchEvent(coaBatch:CoaBatch, status:CoaStatus=null ,event:Event=null, _refreshEvent:Event=null):void 
		{
			super();
			_coaBatch = coaBatch;
			_event = event;
			_status= status;
			_refreshEvent = refreshEvent;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get refreshEvent():Event {
			return _refreshEvent;
		}
		
		public function get status():CoaStatus {
			return _status;
		}
	}
}