package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RefreshCoaDiscrepancyViewEvent;
	import hk.org.ha.event.pms.dqa.coa.RefreshCoaVerificationViewEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListByCoaStatusDiscrepancyStatusEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListByCoaStatusEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListForGroupDiscrepancyEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaChecklistKeyListEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyKeyListEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaVerificationDataListEvent;
	import hk.org.ha.event.pms.dqa.coa.show.ShowDiscrepancyViewEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	import hk.org.ha.view.pms.dqa.coa.CoaVerificationView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaBatchListServiceCtl", restrict="false")]
	public class CoaBatchListServiceCtl
	{
	
		[In]
		public var coaBatchListService:CoaBatchListServiceBean;
		
		[In]
		public var coaBatchList:ArrayCollection;
	
		private var coaBatch:CoaBatch;
		
		private var callBackFunc:Function;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaVerificationDataList(evt:RetrieveCoaVerificationDataListEvent):void {
			coaBatchListService.retrieveCoaVerificationDataList(
				function(tideResultEvent:TideResultEvent):void 
				{
					evt.callBackFunc(tideResultEvent.result);
					tideResultEvent.context.dispatchEvent( new RetrieveCoaChecklistKeyListEvent() );
					tideResultEvent.context.dispatchEvent( new RetrieveCoaDiscrepancyKeyListEvent() );	
				}
			);
		}
		
		[Observer]
		public function retrieveCoaBatchListByCoaStatus(evt:RetrieveCoaBatchListByCoaStatusEvent):void 
		{						
			callBackFunc = evt.callBackFunc;
			coaBatchListService.retrieveCoaBatchListByCoaStatus(evt.coaStatus, retrieveCoaBatchListByCoaStatusResult);					
		}
		
		private function retrieveCoaBatchListByCoaStatusResult(evt:TideResultEvent):void 
		{				
			if( callBackFunc != null ) {
				callBackFunc(evt.result);
			}
			evt.context.dispatchEvent( new RetrieveCoaChecklistKeyListEvent() );
			evt.context.dispatchEvent( new RetrieveCoaDiscrepancyKeyListEvent() );								
		}
		
		[Observer]
		public function retrieveCoaBatchListForGroupDiscrepancy(evt:RetrieveCoaBatchListForGroupDiscrepancyEvent):void
		{			
			coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus(
				CoaStatus.DiscrepancyClarificationInProgress, 
				DiscrepancyStatus.DiscrepancyUnderGrouping, 
				retrieveCoaBatchListForGroupDiscrepancyResult);
		}
		
		private function retrieveCoaBatchListForGroupDiscrepancyResult(evt:TideResultEvent):void
		{			
			evt.context.dispatchEvent( new ShowDiscrepancyViewEvent() );
		}
		
		[Observer]
		public function retrieveCoaBatchListByCoaStatusDiscrepancyStatus(evt:RetrieveCoaBatchListByCoaStatusDiscrepancyStatusEvent):void
		{			
			event = evt.event;
			coaBatchListService.retrieveCoaBatchListByCoaStatusDiscrepancyStatus(evt.coaStatus, evt.discrepancyStatus, retrieveCoaBatchListByCoaStatusDiscrepancyStatusResult);
		}
		
		private function retrieveCoaBatchListByCoaStatusDiscrepancyStatusResult(evt:TideResultEvent):void
		{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}
		}
	}
}