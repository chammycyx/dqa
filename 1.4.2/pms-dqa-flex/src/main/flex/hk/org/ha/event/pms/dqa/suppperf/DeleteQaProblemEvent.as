package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class DeleteQaProblemEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _qaProblem:QaProblem; 
		private var _deleteReason:String; 
		
		
		
		public function DeleteQaProblemEvent(qaProblem:QaProblem, 
												  deleteReason:String, 
												  event:Event=null):void {
			super();
			_event = event;
			_qaProblem = qaProblem; 
			_deleteReason = deleteReason; 
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get deleteReason():String {
			return _deleteReason;
		}
		
		
	}
}