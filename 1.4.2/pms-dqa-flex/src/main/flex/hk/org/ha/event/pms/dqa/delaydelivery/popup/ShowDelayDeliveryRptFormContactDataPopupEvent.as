package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDelayDeliveryRptFormContactDataPopupEvent extends AbstractTideEvent 
	{	
		private var _updateDelayDeliveryRptFormContactData:Function;
		private var _state:String;
		private var _delayDeliveryRptFormContactData: DelayDeliveryRptFormContactData;
		private var _delayDeliveryRptFormContactDataListSelect: ArrayCollection;
		
		public function ShowDelayDeliveryRptFormContactDataPopupEvent( state:String, delayDeliveryRptFormContactDataListSelect:ArrayCollection,
																delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData=null,
																updateDelayDeliveryRptFormContactData:Function=null):void
		{
			super();
			_state=state;
			_delayDeliveryRptFormContactData= delayDeliveryRptFormContactData;
			_delayDeliveryRptFormContactDataListSelect= delayDeliveryRptFormContactDataListSelect;
			_updateDelayDeliveryRptFormContactData = updateDelayDeliveryRptFormContactData;
		}
		
		public function get delayDeliveryRptFormContactDataListSelect():ArrayCollection {
			return _delayDeliveryRptFormContactDataListSelect;
		}
		
		public function get updateDelayDeliveryRptFormData():Function {
			return _updateDelayDeliveryRptFormContactData;
		}
		
		public function get state():String {
			return _state;
		}
		public function get delayDeliveryRptFormContactData():DelayDeliveryRptFormContactData {
			return _delayDeliveryRptFormContactData;
		}
	}
	
}