package hk.org.ha.control.pms.dqa.delaydelivery

{
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.dqa.delaydelivery.CreateUpdateDraftDelayDeliveryRptFormEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.DeleteDraftDelayDeliveryRptFormEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RefreshDelayDeliveryRptFormDataPopupFileViewPopupEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormContactEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormContactQuickAddEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormSelectedEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDraftDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.UpdateDelayDeliveryRptFormStatusEvent;
	import hk.org.ha.model.pms.dqa.biz.delaydelivery.DelayDeliveryRptFormListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.delaydelivery.DelayDeliveryRptFormServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
	import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormContact;
	import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormPo;
	import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormVer;
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.TideResultEvent;


	
	[Bindable]
	[Name("delayDeliveryRptFormListServiceCtl", restrict="true")]
	public class DelayDeliveryRptFormListServiceCtl {
		
		[In]       
		public var delayDeliveryRptFormListService:DelayDeliveryRptFormListServiceBean;
		
		
		[In]       
		public var delayDeliveryRptFormService:DelayDeliveryRptFormServiceBean;
		
		
		
		public var dummyForExteralizedDelayDeliveryRptForm : DelayDeliveryRptForm;
		public var dummyForExteralizedDelayDeliveryRptFormVer :DelayDeliveryRptFormVer;
		public var dummyForExteralizedDelayDeliveryRptFormContact : DelayDeliveryRptFormContact;
		public var dummyForExteralizedDelayDeliveryRptFormPo :DelayDeliveryRptFormPo;

		
		private var event:Event;
		
		private var retrieveDraftDelayDeliveryListEvent:RetrieveDraftDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent;
		private var retrievDelayDeliveryListEvent:RetrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent;
		private var retrieveDelayDeliveryRptFormSelectedEvent:RetrieveDelayDeliveryRptFormSelectedEvent;
		private var createUpdateDraftDelayDeliveryRptFormEvent:CreateUpdateDraftDelayDeliveryRptFormEvent;
		private var updateDelayDeliveryRptFormStatusEvent:UpdateDelayDeliveryRptFormStatusEvent;
		private var deleteDraftDelayDeliveryRptFormEvent:DeleteDraftDelayDeliveryRptFormEvent;
		private var retrieveDelayDeliveryRptFormContactQuickAddEvent:RetrieveDelayDeliveryRptFormContactQuickAddEvent;
		private var retrieveDelayDeliveryRptFormContactEvent:RetrieveDelayDeliveryRptFormContactEvent;
		private var refreshDelayDeliveryRptFormDataPopupFileViewPopupEvent:RefreshDelayDeliveryRptFormDataPopupFileViewPopupEvent;
		
		
		
		[Observer]
		public function retrieveDraftDelayDeliveryList(evt:RetrieveDraftDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent):void{
			retrieveDraftDelayDeliveryListEvent= evt;
			delayDeliveryRptFormListService.retrieveDraftDelayDeliveryRptFormList(evt.delayDeliveryRptFormCriteria, retrieveDraftDelayDeliveryListResult);
		}
		
		public function retrieveDraftDelayDeliveryListResult(evt:TideResultEvent):void{
			if (retrieveDraftDelayDeliveryListEvent.callBack != null) {
				retrieveDraftDelayDeliveryListEvent.callBack(evt.result as ArrayCollection);
			}
			
		}
		
		[Observer]
		public function retrieveDelayDeliveryList(evt:RetrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent):void{
			retrievDelayDeliveryListEvent= evt;
			delayDeliveryRptFormListService.retrieveDelayDeliveryRptFormList(evt.delayDeliveryRptFormCriteria, retrieveDelayDeliveryListResult);
		}
		
		public function retrieveDelayDeliveryListResult(evt:TideResultEvent):void{
			if (retrievDelayDeliveryListEvent.callBack != null) {
				retrievDelayDeliveryListEvent.callBack(evt.result as ArrayCollection);
			}
			
		}
		
		
		
		[Observer]
		public function retrieveDelayDeliveryRptFormSelected(evt:RetrieveDelayDeliveryRptFormSelectedEvent):void{
			retrieveDelayDeliveryRptFormSelectedEvent= evt;
			delayDeliveryRptFormListService.retrieveDelayDeliveryRptFormData(evt.delayDeliveryRptForm, retrieveDelayDeliveryRptFormSelectedResult);
		}
		
		public function retrieveDelayDeliveryRptFormSelectedResult(evt:TideResultEvent):void{
			if (retrieveDelayDeliveryRptFormSelectedEvent.callBackFunction != null) {
				retrieveDelayDeliveryRptFormSelectedEvent.callBackFunction(evt.result as DelayDeliveryRptFormData);
			}
		
			
		}
		
		
		
		[Observer]
		public function retrieveDelayDeliveryRptFormContact(evt:RetrieveDelayDeliveryRptFormContactEvent):void{
			retrieveDelayDeliveryRptFormContactEvent= evt;
			delayDeliveryRptFormListService.retrieveDelayDeliveryRptFormContact(evt.delayDeliveryRptFormContactData, retrieveDelayDeliveryRptFormContactEventResult);
		}
		
		public function retrieveDelayDeliveryRptFormContactEventResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
			if (retrieveDelayDeliveryRptFormContactEvent.callBackFunction != null) {
				retrieveDelayDeliveryRptFormContactEvent.callBackFunction(evt.result as DelayDeliveryRptFormContactData);
			}
			
			
		}
		
		
		[Observer]
		public function retrieveDelayDeliveryRptFormContactQuickAdd(evt:RetrieveDelayDeliveryRptFormContactQuickAddEvent):void{
			retrieveDelayDeliveryRptFormContactQuickAddEvent= evt;
			delayDeliveryRptFormListService.retrieveDelayDeliveryRptFormContactQuickAddList(evt.institution, retrieveDelayDeliveryRptFormContactQuickAddEventResult);
		}
		
		public function retrieveDelayDeliveryRptFormContactQuickAddEventResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
			if (retrieveDelayDeliveryRptFormContactQuickAddEvent.callBackFunction != null) {
				retrieveDelayDeliveryRptFormContactQuickAddEvent.callBackFunction(evt.result as ArrayCollection);
			}
			
			
		}
		
		
		[Observer]
		public function createUpdateDraftDelayDeliveryRptForm(evt:CreateUpdateDraftDelayDeliveryRptFormEvent):void{
			createUpdateDraftDelayDeliveryRptFormEvent= evt;
			delayDeliveryRptFormService.createUpdateDelayDeliveryRptForm(evt.delayDeliveryRptFormData, 
				evt.delayDeliveryRptFormContactDataList, 
				evt.delayDeliveryRptFormPoDataList, evt.emailFlag,createUpdateDraftDelayDeliveryRptFormResult);
		}
		
		public function createUpdateDraftDelayDeliveryRptFormResult(evt:TideResultEvent):void{
			if (createUpdateDraftDelayDeliveryRptFormEvent.callBackFunction != null) {
				createUpdateDraftDelayDeliveryRptFormEvent.callBackFunction(evt.result as String);
			}
		}
		
		[Observer]
		public function updateDelayDeliveryRptFormStatus(evt:UpdateDelayDeliveryRptFormStatusEvent):void{
			updateDelayDeliveryRptFormStatusEvent= evt;
			delayDeliveryRptFormService.updateDelayDeliveryRptFormStatus(evt.delayDeliveryRptFormData,updateDelayDeliveryRptFormStatusResult); 
		}
		
		public function updateDelayDeliveryRptFormStatusResult(evt:TideResultEvent):void{
			if (updateDelayDeliveryRptFormStatusEvent.callBackFunction != null) {
				updateDelayDeliveryRptFormStatusEvent.callBackFunction(evt.result as DelayDeliveryRptFormData );
			}
		}
		
		[Observer]
		public function deleteDraftDelayDeliveryRptForm(evt:DeleteDraftDelayDeliveryRptFormEvent):void{
			deleteDraftDelayDeliveryRptFormEvent= evt;
			delayDeliveryRptFormService.deleteDelayDeliveryRptForm(evt.delayDeliveryRptFormData, 
			 evt.reason,deleteDraftDelayDeliveryRptFormResult);
		}
		
		public function deleteDraftDelayDeliveryRptFormResult(evt:TideResultEvent):void{
			if (deleteDraftDelayDeliveryRptFormEvent.callBackFunction != null) {
				deleteDraftDelayDeliveryRptFormEvent.callBackFunction();
			}
		}
		
//		
		
	}
}
