package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureCatListEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureCatListServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.NatureOfProblemMaintView;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureCatListServiceCtl", restrict="true")]
	public class ProblemNatureCatListServiceCtl {
		
		[In]       
		public var problemNatureCatListService:ProblemNatureCatListServiceBean;
		
		[In]
		public var natureOfProblemMaintView:NatureOfProblemMaintView;
		
		private var event:Event;
		private var screenName:String;
		private var problemNatureParamIn:ProblemNatureParam;
		
		
		[Observer]
		public function retrieveProblemNatureCatList(evt:RetrieveProblemNatureCatListEvent):void{
			event = evt.event;
			screenName = evt.screenName;
			problemNatureParamIn = evt.problemNatureParam;
			problemNatureCatListService.retrieveProblemNatureCatList(evt.problemNatureParam, retrieveProblemNatureCatListResult);
		}
		
		public function retrieveProblemNatureCatListResult(evt:TideResultEvent):void{
			if(screenName == "natureOfProblemMaintView")
			{
				natureOfProblemMaintView.txtCatParamDisplayOrder.text =  String(problemNatureParamIn.displayOrder);
				natureOfProblemMaintView.txtCatParamDesc.text = problemNatureParamIn.paramDesc;
				natureOfProblemMaintView.problemNatureParamSelect = problemNatureParamIn;
			}
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
			
		}
		
	}
}
