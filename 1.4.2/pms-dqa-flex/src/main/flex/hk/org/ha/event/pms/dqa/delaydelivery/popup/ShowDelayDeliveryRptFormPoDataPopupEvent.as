package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDelayDeliveryRptFormPoDataPopupEvent extends AbstractTideEvent 
	{	
		private var _updateDelayDeliveryRptFormPoData:Function;
		private var _state:String;
		
		private var _delayDeliveryRptFormData :DelayDeliveryRptFormData;
		private var _delayDeliveryRptFormPoData: DelayDeliveryRptFormPoData;
		private var _delayDeliveryRptFormPoDataListSelected: ArrayCollection;
		public function ShowDelayDeliveryRptFormPoDataPopupEvent( state:String,
																  delayDeliveryRptFormData:DelayDeliveryRptFormData,
																  delayDeliveryRptFormPoDataListSelected:ArrayCollection,
																delayDeliveryRptFormPoData:DelayDeliveryRptFormPoData=null,
																updateDelayDeliveryRptFormPoData:Function=null):void
		{
			super();
			_state=state;
			_delayDeliveryRptFormPoDataListSelected= delayDeliveryRptFormPoDataListSelected;
			_delayDeliveryRptFormData = delayDeliveryRptFormData;
			_delayDeliveryRptFormPoData= delayDeliveryRptFormPoData;
			_updateDelayDeliveryRptFormPoData = updateDelayDeliveryRptFormPoData;
		}
		
		
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData {
			return _delayDeliveryRptFormData;
		}
		
		public function get delayDeliveryRptFormPoDataListSelected():ArrayCollection {
			return _delayDeliveryRptFormPoDataListSelected;
		}
		
		public function get updateDelayDeliveryRptFormPoData():Function {
			return _updateDelayDeliveryRptFormPoData;
		}
		
		public function get state():String {
			return _state;
		}
		public function get delayDeliveryRptFormPoData():DelayDeliveryRptFormPoData {
			return _delayDeliveryRptFormPoData;
		}
	}
	
}