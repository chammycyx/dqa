package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSessionTimeoutEvent extends AbstractTideEvent 
	{		
		private var _sessionTime:int;
		
		public function UpdateSessionTimeoutEvent(sessionTime:int):void 
		{
			super();
			this._sessionTime = sessionTime;
		}
		
		public function get sessionTime():int
		{
			return _sessionTime;
		}
	}
}