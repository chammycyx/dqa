package hk.org.ha.event.pms.dqa.sample.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	public class ShowLabTestLabCollectQtyPopupEvent extends AbstractTideEvent 
	{	
		
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function ShowLabTestLabCollectQtyPopupEvent(sampleTestSchedule:SampleTestSchedule):void
		{
			super();
			_sampleTestSchedule = sampleTestSchedule;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
	}
}