package hk.org.ha.event.pms.dqa.sample.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowExclusionTestPopupMessageEvent extends AbstractTideEvent 
	{
		private var _msgCode:String;
		
		
		public function ShowExclusionTestPopupMessageEvent(msgCode:String):void {
			super();
			_msgCode = msgCode;			
		}
		
		public function get msgCode():String 
		{
			return _msgCode;
		}
		
	}


}