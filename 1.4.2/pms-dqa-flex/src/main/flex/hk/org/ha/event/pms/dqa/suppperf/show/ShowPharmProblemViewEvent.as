package hk.org.ha.event.pms.dqa.suppperf.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmProblemViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowPharmProblemViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}