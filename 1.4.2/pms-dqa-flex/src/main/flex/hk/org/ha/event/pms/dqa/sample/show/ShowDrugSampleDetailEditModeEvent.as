package hk.org.ha.event.pms.dqa.sample.show {
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDrugSampleDetailEditModeEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function ShowDrugSampleDetailEditModeEvent(event:Event=null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}

