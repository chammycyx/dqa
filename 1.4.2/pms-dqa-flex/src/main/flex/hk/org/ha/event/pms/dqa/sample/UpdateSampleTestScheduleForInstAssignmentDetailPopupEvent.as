package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	import mx.collections.ListCollectionView
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSampleTestScheduleForInstAssignmentDetailPopupEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _actionType:String;
		private var _sampleTestScheduleList:ListCollectionView;
		
		public function UpdateSampleTestScheduleForInstAssignmentDetailPopupEvent(sampleTestSchedule:SampleTestSchedule, actionType:String, sampleTestScheduleList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_sampleTestSchedule = sampleTestSchedule;
			_actionType = actionType;
			_sampleTestScheduleList = sampleTestScheduleList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule{
			return _sampleTestSchedule;
		}
		
		public function get actionType():String{
			return _actionType;
		}
		
		public function get sampleTestScheduleList():ListCollectionView {
			return _sampleTestScheduleList;
		}
	}
}