package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEnquiryDetailExportCriteriaPopupEvent extends AbstractTideEvent 
	{	
		
		private var _event:Event;
		
		public function ShowEnquiryDetailExportCriteriaPopupEvent(event:Event=null):void
		{
			super();
			_event = event;	
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}