package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class AddSampleTestFileForTestResultEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestFile:SampleTestFile;
		
		public function AddSampleTestFileForTestResultEvent(sampleTestFile:SampleTestFile , event:Event=null):void {
			super();
			_event = event;
			_sampleTestFile = sampleTestFile;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestFile():SampleTestFile {
			return _sampleTestFile;
		}
		
	}
}