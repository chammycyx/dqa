package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveManufacturerListLikeEvent extends AbstractTideEvent 
	{	
		
		private var _manufacturerCode:String;
		
		private var _event:Event;
		
		public function RetrieveManufacturerListLikeEvent(manufacturerCode:String, event:Event=null):void
		{
			super();
			_manufacturerCode = manufacturerCode;
			_event = event;
		}
		
		public function get manufacturerCode():String 
		{
			return _manufacturerCode;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}
