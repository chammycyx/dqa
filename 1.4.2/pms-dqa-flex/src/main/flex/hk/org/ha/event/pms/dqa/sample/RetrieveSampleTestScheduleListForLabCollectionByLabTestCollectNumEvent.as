package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumEvent extends AbstractTideEvent 
	{
		private var _labCode:String;
		private var _testCode:String;
		private var _labCollectNum:String;
		
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleListForLabCollectionByLabTestCollectNumEvent(labCode:String, testCode:String, labCollectNum:String, event:Event=null):void {
			super();
			_labCode = labCode;
			_testCode = testCode;
			_labCollectNum = labCollectNum;
			_event = event;
		}
		
		public function get labCode():String 
		{
			return _labCode;
		}
		
		public function get testCode():String 
		{
			return _testCode;
		}
		
		public function get labCollectNum():String 
		{
			return _labCollectNum;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}