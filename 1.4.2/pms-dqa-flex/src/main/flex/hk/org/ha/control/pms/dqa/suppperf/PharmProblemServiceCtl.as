package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
//	import hk.org.ha.event.pms.dqa.suppperf.AddPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreatePharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateUpdateDraftPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.DeletePharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.GenPharmProblemRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrievePharmProblemByPharmProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateNewPharmProblemWithQaProblemEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowPharmProblemPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemNewServiceBean;
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemServiceBean;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureSubCatListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.ExtendedExternalInterface;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.view.pms.dqa.suppperf.PharmProblemView;
	import hk.org.ha.view.pms.dqa.suppperf.popup.PharmProblemPopup;
	import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.config.ServerConfig;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pharmProblemServiceCtl", restrict="true")]
	public class PharmProblemServiceCtl {
		
		[In]       
		public var pharmProblemService:PharmProblemServiceBean;
		
		[In]       
		public var pharmProblemNewService:PharmProblemNewServiceBean
		
		[In]       
		public var problemNatureSubCatListService:ProblemNatureSubCatListServiceBean;
		
		[In]
		public var pharmProblem:PharmProblem;
		
		[In]
		public var pharmProblemView:PharmProblemView;
		
		[In]
		public var normalWindowsProperties:String;
		
		[In]       
		public var problemNatureSubCatList:ArrayCollection;
		
		private var event:Event;
		private var screenName:String;
		private var pharmProblemPopup:PharmProblemPopup;
		private var pharmProblemPopupIn:PharmProblemPopup;
		
		
		
//		[Observer]
//		public function addPharmProblem(evt:AddPharmProblemEvent):void{
//			event = evt.event;
//			pharmProblemService.addPharmProblem(addPharmProblemResult);
//		}
//		
//		public function addPharmProblemResult(evt:TideResultEvent):void{
//			if ( event != null ){
//				evt.context.dispatchEvent( event );
//			}
//		}
		
		
		[Observer]
		public function createPharmProblem(evt:CreatePharmProblemEvent):void{
			event = evt.event;
			pharmProblemPopup = evt.pharmProblemPopup;
			In(Object(pharmProblemService).success);
			In(Object(pharmProblemService).errorCode);
			pharmProblemService.createPharmProblem(evt.pharmProblem, 
														evt.pharmBatchNumList, 
														evt.countryList, 
														evt.pharmProblemNatureList,
														evt.pharmProblemFileUploadDataList,
														evt.sendEmailBoolean,
														createPharmProblemResult);
		}
		
		public function createPharmProblemResult(evt:TideResultEvent):void{
			if (Object(pharmProblemService).success ){
				pharmProblemPopup.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(pharmProblemService).errorCode !=null){
					pharmProblemPopup.showMessage(Object(pharmProblemService).errorCode );
				}
			}
		}
		
		
		
//		[Observer]
//		public function createUpdateDraftPharmProblem(evt:CreateUpdateDraftPharmProblemEvent):void{
//			event = evt.event;
//			pharmProblemPopup = evt.pharmProblemPopup;
//			In(Object(pharmProblemService).success);
//			In(Object(pharmProblemService).errorCode);
//			pharmProblemService.createUpdateDraftPharmProblem(evt.pharmProblem, 
//				evt.pharmBatchNumList, 
//				evt.countryList, 
//				evt.pharmProblemNatureList,
//				evt.pharmProblemFileUploadDataList,
//				createUpdateDraftPharmProblemResult);
//		}
//		
//		public function createUpdateDraftPharmProblemResult(evt:TideResultEvent):void{
//			if (Object(pharmProblemService).success ){
//				pharmProblemPopup.btnCancel_click();
//				if ( event != null ){
//					evt.context.dispatchEvent( event );
//				}
//			}else{
//				if (Object(pharmProblemService).errorCode !=null){
//					pharmProblemPopup.showMessage(Object(pharmProblemService).errorCode );
//				}
//			}
//		}
		
		
		
		
		
		[Observer]
		public function retrievePharmProblemByPharmProblem(evt:RetrievePharmProblemByPharmProblemEvent):void{
			event = evt.event;
			screenName = evt.screenName;
			
			pharmProblemService.retrievePharmProblemByPharmProblem(evt.pharmProblem, 
																			retrievePharmProblemByPharmProblemResult);
		}
		
		public function retrievePharmProblemByPharmProblemResult(evt:TideResultEvent):void{
			if(screenName == "pharmProblemView" && pharmProblem.problemStatus == ProblemStatus.Drafted)
			{
//				problemNatureSubCatListService.retrieveProblemNatureSubCatListFull(retrieveProblemNatureSubCatListFullResultForDraftPharmProblem);
			}
			else if(screenName == "pharmProblemView" || screenName == "qaProblemPopup")
			{
				evt.context.dispatchEvent(new ShowPharmProblemPopupEvent("View",null,pharmProblem,screenName) );
			}
			else if(screenName == "qaProblemNewProblemView" || screenName == "qaProblemPharmProblemEnquiryView")
			{
				evt.context.dispatchEvent(new ShowPharmProblemPopupEvent("QaView",null,pharmProblem,screenName) );
			}
			else if(screenName == "genPharmProblemRpt")
			{
				evt.context.dispatchEvent(new GenPharmProblemRptEvent(pharmProblem) );
			}	
			else
			{
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}
		}
		
//		public function retrieveProblemNatureSubCatListFullResultForDraftPharmProblem(evt:TideResultEvent):void{
//		
//			evt.context.dispatchEvent(new ShowPharmProblemPopupEvent("Add",problemNatureSubCatList,pharmProblem,screenName) );
//		}
		
		
		
		[Observer]
		public function deletePharmProblem(evt:DeletePharmProblemEvent):void{
			event = evt.event;
			
			pharmProblemService.deletePharmProblem(evt.pharmProblem, evt.deleteReason, deletePharmProblemResult);
		}
		
		public function deletePharmProblemResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateNewPharmProblemWithQaProblem(evt:UpdateNewPharmProblemWithQaProblemEvent):void{
			event = evt.event;
			In(Object(pharmProblemNewService).success);
			In(Object(pharmProblemNewService).errorCode);
			pharmProblemPopupIn = evt.pharmProblemPopup;
			pharmProblemNewService.updateNewPharmProblemWithQaProblem(evt.pharmProblem, evt.qaProblem, evt.caseType, evt.qaBatchNumList, updateNewPharmProblemWithQaProblemResult);
		}
		
		public function updateNewPharmProblemWithQaProblemResult(evt:TideResultEvent):void{
			if (Object(pharmProblemNewService).success ){
				pharmProblemPopupIn.btnCancel_click();
			
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(pharmProblemNewService).errorCode !=null){
					pharmProblemPopupIn.showMessage(Object(pharmProblemNewService).errorCode );
				}
			}
			
		}
		
		[Observer]
		public function genPharmProblemRpt(evt:GenPharmProblemRptEvent):void{
			pharmProblemService.createPharmProblemRpt(evt.pharmProblem, genPharmProblemRptResult);
		}
		
		public function genPharmProblemRptResult(evt:TideResultEvent):void{
			var urlReq:String = "pharmProblemRpt.seam?actionMethod=pharmProblemRpt.xhtml%3ApharmProblemService.generatePharmProblemRpt";
			
			var winName:String ="pdf_pharm_problem_report_win";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				urlReq = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + urlReq;
			}
						
			ExtendedExternalInterface.call("window.open", urlReq, winName, normalWindowsProperties);
		}
		
	}
}
