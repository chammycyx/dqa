package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateDelayDeliveryRptFormStatusEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _delayDeliveryRptFormData:DelayDeliveryRptFormData;
		
		public function UpdateDelayDeliveryRptFormStatusEvent(delayDeliveryRptFormData:DelayDeliveryRptFormData, 
																   callBackFunction:Function=null):void {
			super();
			_callBackFunction= callBackFunction;
			_delayDeliveryRptFormData = delayDeliveryRptFormData;
		}
		
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData
		{
			return _delayDeliveryRptFormData;
		}
		
	}
}