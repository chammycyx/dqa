package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
	
	
	public class DeleteSampleItemEvent extends AbstractTideEvent 
	{
		private var _sampleItem:SampleItem;
		
		
		public function DeleteSampleItemEvent(sampleItem:SampleItem):void {
			super();
			_sampleItem = sampleItem;			
		}
		
		public function get sampleItem():SampleItem 
		{
			return _sampleItem;
		}
		
	}
}