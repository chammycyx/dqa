package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	
	public class RefreshQaProblemPopupFileListEvent extends AbstractTideEvent 
	{
		private var _qaProblemFileUploadDataList:ArrayCollection;
		
		
		public function RefreshQaProblemPopupFileListEvent(qaProblemFileUploadDataList:ArrayCollection):void {
			super();
			_qaProblemFileUploadDataList = qaProblemFileUploadDataList;			
		}
		
		public function get qaProblemFileUploadDataList():ArrayCollection 
		{
			return _qaProblemFileUploadDataList;
		}
	}
}