package hk.org.ha.event.pms.dqa.sample{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleListByCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestScheduleCriteria:SampleTestScheduleCriteria;
		
		public function RetrieveSampleTestScheduleListByCriteriaEvent(sampleTestScheduleCriteria:SampleTestScheduleCriteria, event:Event=null):void {
			super();
			_sampleTestScheduleCriteria = sampleTestScheduleCriteria;
			_event = event;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleCriteria():SampleTestScheduleCriteria{
			return _sampleTestScheduleCriteria;
		}
	}
}