package hk.org.ha.control.pms.dqa{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.RetrieveLetterTemplateListByModuleTypeRecordStatusEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.RetrieveConflictOfInterestEvent;
	import hk.org.ha.event.pms.dqa.delaydelivery.RetrieveDelayDeliveryRptFormConflictOfInterestEvent;
	import hk.org.ha.model.pms.dqa.biz.LetterContentServiceBean;
	import hk.org.ha.model.pms.dqa.biz.LetterTemplateListServiceBean;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("letterContentServiceCtl", restrict="true")]
	public class LetterContentServiceCtl {
		
		[In]
		public var letterContentService:LetterContentServiceBean;
		
		[Observer]
		public function retrieveConflictOfInterest(evt:RetrieveConflictOfInterestEvent):void {
			letterContentService.retrieveConflictOfInterest(
				function(tideResultEvent:TideResultEvent):void{
					evt.callBackFunc(tideResultEvent.result);
				}
			);
		}
		
		[Observer]
		public function retrieveDelayDeliveryConflictOfInterest(evt:RetrieveDelayDeliveryRptFormConflictOfInterestEvent):void {
			letterContentService.retrieveDelayDeliveryRptFormConflictOfInterest(
				function(tideResultEvent:TideResultEvent):void{
					evt.callBackFunc(tideResultEvent.result);
				}
			);
		}
		
	}
}