package hk.org.ha.event.pms.dqa.suppperf {
	
	import mx.collections.ArrayCollection;
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveBatchSuspQtyRptByBatchSuspListEvent extends AbstractTideEvent 
	{
		private var _batchSuspList:ArrayCollection;
		
		public function RetrieveBatchSuspQtyRptByBatchSuspListEvent(batchSuspList:ArrayCollection):void 
		{
			super();
			_batchSuspList = batchSuspList;
		}
		
		public function get batchSuspList():ArrayCollection {
			return _batchSuspList;
		}
	}
}