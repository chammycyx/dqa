package hk.org.ha.event.pms.dqa.sample.popup {
	
	import flash.events.Event;
	import mx.collections.ListCollectionView;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDefaultContactForLabTestPopupEvent extends AbstractTideEvent
	{
		private var _event:Event;
		
		private var _sampleTestScheduleList:ListCollectionView;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _labCode:String;
		
		public function ShowDefaultContactForLabTestPopupEvent(sampleTestScheduleList:ListCollectionView, sampleTestSchedule:SampleTestSchedule, labCode:String,  event:Event=null):void{
			super();
			_event = event;
			_sampleTestScheduleList = sampleTestScheduleList;
			_sampleTestSchedule = sampleTestSchedule;
			_labCode = labCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleList():ListCollectionView{
			return _sampleTestScheduleList;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule{
			return _sampleTestSchedule;
		}
		
		public function get labCode():String {
			return _labCode;
		}
		
	}	
	
}