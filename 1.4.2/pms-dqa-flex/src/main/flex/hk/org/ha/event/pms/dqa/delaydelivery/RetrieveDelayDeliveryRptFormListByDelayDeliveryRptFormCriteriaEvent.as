package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent extends AbstractTideEvent 
	{	
		private var _callBack:Function;
		private var _delayDeliveryRptFormCriteria:DelayDeliveryRptFormCriteria;
		
		public function RetrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteriaEvent(delayDeliveryRptFormCriteria:DelayDeliveryRptFormCriteria, callBack:Function=null):void {
			super();
			_callBack= callBack;
			_delayDeliveryRptFormCriteria = delayDeliveryRptFormCriteria;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
		
		public function get delayDeliveryRptFormCriteria():DelayDeliveryRptFormCriteria
		{
			return _delayDeliveryRptFormCriteria;
		}
	}
}