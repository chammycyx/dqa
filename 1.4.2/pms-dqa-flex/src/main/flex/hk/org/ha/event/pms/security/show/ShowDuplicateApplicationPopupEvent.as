package hk.org.ha.event.pms.security.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDuplicateApplicationPopupEvent extends AbstractTideEvent 
	{
		
		public function ShowDuplicateApplicationPopupEvent():void 
		{
			super();
		}
	}
}