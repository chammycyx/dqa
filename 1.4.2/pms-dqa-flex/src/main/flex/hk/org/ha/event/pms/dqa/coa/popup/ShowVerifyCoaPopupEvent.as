package hk.org.ha.event.pms.dqa.coa.popup {
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowVerifyCoaPopupEvent extends AbstractTideEvent 
	{			
		public function ShowVerifyCoaPopupEvent():void 
		{
			super();			
		}			
	}
}