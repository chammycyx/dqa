package hk.org.ha.control.pms.dqa.coa
{		
			
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RefreshCoaItemMaintViewEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListByCriteriaEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForCoaItemMaintEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForEmailEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForEmailToSuppEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForFileItemMaintEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListWithoutBatchOthersEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowOpenCoaPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderInventoryListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderListForCoaItemMaintServiceBean;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderListServiceBean;
	import hk.org.ha.model.pms.dqa.vo.coa.CoaFileFolderCriteria;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaFileFolderListServiceCtl", restrict="false")]
	public class CoaFileFolderListServiceCtl
	{
		private var coaFileFolderCriteria:CoaFileFolderCriteria;
		
		private var event:Event;
		
		[In]
		public var coaFileFolderListService:CoaFileFolderListServiceBean;
		
		[In]
		public var coaFileFolderListForCoaItemMaintService:CoaFileFolderListForCoaItemMaintServiceBean;
		
		[In]
		public var coaFileFolderInventoryListService:CoaFileFolderInventoryListServiceBean;
		
		[In]
		public var coaFileFolderInventoryList:ArrayCollection = new ArrayCollection();
		
		[In]
		public var coaFileFolderForCoaItemMaintList:ArrayCollection = new ArrayCollection();
		
		[Observer]
		public function retrieveCoaFileFolderListForEmailToSupp(evt:RetrieveCoaFileFolderListForEmailToSuppEvent):void
		{
			coaFileFolderListService.retrieveCoaFileFolderListForEmailToSupp(
				evt.coaBatch,
				function(event:TideResultEvent):void {
					event.context.dispatchEvent(evt.event);
				}
			);
		}
		
		[Observer]
		public function retrieveCoaFileFolderListForEmail(evt:RetrieveCoaFileFolderListForEmailEvent):void 
		{					
			var func:Function = function(tideResultEvent:TideResultEvent):void {
				tideResultEvent.context.dispatchEvent( evt.event );
			};
			
			if (evt.emailToSupplier) {
				coaFileFolderListService.retrieveCoaFileFolderListForEmailToSupp(
					evt.coaBatch, 
					func
				);
			} else {
				coaFileFolderListService.retrieveCoaFileFolderListForReminderToSupp(
					evt.coaBatch, 
					func
				);
			}
		}
		
		[Observer]
		public function retrieveCoaFileFolderListWithoutBatchOthersByCoaBatch(evt:RetrieveCoaFileFolderListWithoutBatchOthersEvent):void
		{						
			coaFileFolderListService.retrieveCoaFileFolderListWithoutBatchOthers(evt.coaBatchId, function(tideResultEvent:TideResultEvent):void {
				tideResultEvent.context.dispatchEvent(evt.event);
			});
		}
		
		[Observer]
		public function retrieveCoaFileFolderListByCriteria(evt:RetrieveCoaFileFolderListByCriteriaEvent):void 
		{	
			
			this.coaFileFolderCriteria = evt.coaFileFolderCriteria;			
			coaFileFolderInventoryListService.retrieveCoaFileFolderListByCriteria(this.coaFileFolderCriteria);
		}
		
		[Observer]
		public function retrieveCoaFileFolderListForCoaItemMaint(evt:RetrieveCoaFileFolderListForCoaItemMaintEvent):void
		{
			coaFileFolderListForCoaItemMaintService.retrieveCoaFileFolderListForCoaItemMaint(evt.coaItem, retrieveCoaFileFolderListForCoaItemMaintResult);
		}	
		
		public function retrieveCoaFileFolderListForCoaItemMaintResult(evt: TideResultEvent):void {
			evt.context.dispatchEvent(new RefreshCoaItemMaintViewEvent());
		}
		

	}
}
