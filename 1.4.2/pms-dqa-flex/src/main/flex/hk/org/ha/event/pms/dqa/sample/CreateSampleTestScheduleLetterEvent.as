package hk.org.ha.event.pms.dqa.sample
{
	import flash.events.Event;
	import mx.collections.ListCollectionView;
	
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateSampleTestScheduleLetterEvent extends AbstractTideEvent 
	{
	
		private var _letterTemplate:LetterTemplate;
		private var _refNum:String;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _sampleTestScheduleList:ListCollectionView;
		private var _event:Event;
		
		public function CreateSampleTestScheduleLetterEvent(letterTemplate:LetterTemplate, refNum:String, sampleTestScheduleList:ListCollectionView, sampleTestSchedule:SampleTestSchedule,  event:Event=null):void {
			super();
			_letterTemplate = letterTemplate;
			_refNum = refNum;
			_sampleTestScheduleList = sampleTestScheduleList;
			_sampleTestSchedule = sampleTestSchedule;
			_event = event;
		}
		
		public function get letterTemplate():LetterTemplate
		{
			return _letterTemplate;
		}
		
		public function get refNum():String
		{
			return _refNum;
		}
		
		public function get sampleTestScheduleList():ListCollectionView
		{
			return _sampleTestScheduleList;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule
		{
			return _sampleTestSchedule;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}