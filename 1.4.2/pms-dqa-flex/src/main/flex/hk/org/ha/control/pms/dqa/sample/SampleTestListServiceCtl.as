package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestListEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("sampleTestListServiceCtl", restrict="true")]
	public class SampleTestListServiceCtl {
		
	
		[In]
		public var sampleTestListService:SampleTestListServiceBean;
		
		private var event:Event;
	
		[Observer]
		public function retrieveSampleTestList(evt:RetrieveSampleTestListEvent):void {
			event = evt.event;
			sampleTestListService.retrieveSampleTestList(retrieveSampleTestListResult);
		
		}
		
		public function retrieveSampleTestListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
	}
}
