package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import mx.collections.ArrayCollection;
	
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDelayDeliveryRptFormStatusListPopupEvent extends AbstractTideEvent 
	{	
		
		private var _delayDeliveryRptFormStatusListSelect:ArrayCollection;
		private var _updateDelayDeliveryRptFormStatusList:Function;
		
		public function ShowDelayDeliveryRptFormStatusListPopupEvent( 
																	   delayDeliveryRptFormStatusListSelect:ArrayCollection,
																	   updateDelayDeliveryRptFormStatusList:Function=null):void
		{
			super();
			_delayDeliveryRptFormStatusListSelect = delayDeliveryRptFormStatusListSelect;
			_updateDelayDeliveryRptFormStatusList = updateDelayDeliveryRptFormStatusList;
		}
		public function get delayDeliveryRptFormStatusListSelect():ArrayCollection {
			return _delayDeliveryRptFormStatusListSelect;
		}
		public function get updateDelayDeliveryRptFormStatusList():Function {
			return _updateDelayDeliveryRptFormStatusList;
		}
		
	}
	
}