package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateProblemNatureCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureCatByProblemNatureCatIdEvent;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateProblemNatureCatEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureCatServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
	import hk.org.ha.view.pms.dqa.suppperf.popup.NatureOfProblemCategoryPopup;
	import hk.org.ha.view.pms.dqa.suppperf.NatureOfProblemMaintView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureCatServiceCtl", restrict="true")]
	public class ProblemNatureCatServiceCtl {
		
		[In]       
		public var problemNatureCatService:ProblemNatureCatServiceBean;
		
		[In]
		public var problemNatureCat:ProblemNatureCat;
		
		[In]
		public var natureOfProblemCategoryPopup:NatureOfProblemCategoryPopup;
		
		[In]
		public var natureOfProblemMaintView:NatureOfProblemMaintView;
		
		private var event:Event;
		
		
		[Observer]
		public function addProblemNatureCat(evt:AddProblemNatureCatEvent):void{
			event = evt.event;
			problemNatureCatService.addProblemNatureCat(addProblemNatureCatResult);
		}
		
		public function addProblemNatureCatResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function createProblemNatureCat(evt:CreateProblemNatureCatEvent):void{
			event = evt.event;
			In(Object(problemNatureCatService).success);
			In(Object(problemNatureCatService).errorCode);
			problemNatureCatService.createProblemNatureCat(evt.problemNatureCat, createProblemNatureCatResult);
		}
		
		public function createProblemNatureCatResult(evt:TideResultEvent):void{
			if (Object(problemNatureCatService).success ){
				natureOfProblemCategoryPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureCatService).errorCode !=null){
					natureOfProblemCategoryPopup.showMessage(Object(problemNatureCatService).errorCode );
				}
			}
		}
		
		[Observer]
		public function updateProblemNatureCat(evt:UpdateProblemNatureCatEvent):void{
			event = evt.event;
			In(Object(problemNatureCatService).success);
			In(Object(problemNatureCatService).errorCode);
			problemNatureCatService.updateProblemNatureCat(evt.problemNatureCat, updateProblemNatureCatResult);
		}
		
		public function updateProblemNatureCatResult(evt:TideResultEvent):void{
			if (Object(problemNatureCatService).success ){
				natureOfProblemCategoryPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureCatService).errorCode !=null){
					natureOfProblemCategoryPopup.showMessage(Object(problemNatureCatService).errorCode );
				}
			}
		}
		
		[Observer]
		public function deleteProblemNatureCat(evt:DeleteProblemNatureCatEvent):void{
			event = evt.event;
			problemNatureCatService.deleteProblemNatureCat(evt.problemNatureCat, deleteProblemNatureCatResult);
		}
		
		public function deleteProblemNatureCatResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveProblemNatureCatByProblemNatureCatId(evt:RetrieveProblemNatureCatByProblemNatureCatIdEvent):void{
			event = evt.event;
			problemNatureCatService.retrieveProblemNatureCatByProblemNatureCatId(evt.problemNatureCat, retrieveProblemNatureCatByProblemNatureCatIdResult);
		}
		
		public function retrieveProblemNatureCatByProblemNatureCatIdResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
