package hk.org.ha.event.pms.dqa.sample.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowEnquiyDetailExportEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowEnquiyDetailExportEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}