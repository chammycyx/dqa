package hk.org.ha.event.pms.dqa.sample{
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
	
	import flash.events.Event;
	
	public class CreateInstitutionEvent extends AbstractTideEvent 
	{
		private var _institution:Institution;
		private var _event:Event;
		
		public function CreateInstitutionEvent(institution:Institution, event:Event=null):void {
			super();
			_institution = institution;	
			_event = event;
		}
		
		public function get institution():Institution 
		{
			return _institution;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}