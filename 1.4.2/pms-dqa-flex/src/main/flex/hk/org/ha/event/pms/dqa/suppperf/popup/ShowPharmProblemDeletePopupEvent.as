package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmProblemDeletePopupEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _pharmProblem:PharmProblem;
		private var _screenName:String;
		
		public function ShowPharmProblemDeletePopupEvent(pharmProblem:PharmProblem, screenName:String,event:Event=null):void
		{
			super();
			_event = event;
			_pharmProblem = pharmProblem;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get pharmProblem():PharmProblem{
			return _pharmProblem;
		}
		
		public function get screenName():String{
			return _screenName;
		}
	}
}