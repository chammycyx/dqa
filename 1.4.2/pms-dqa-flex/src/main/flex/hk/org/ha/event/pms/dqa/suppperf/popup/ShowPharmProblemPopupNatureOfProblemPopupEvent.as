package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmProblemPopupNatureOfProblemPopupEvent extends AbstractTideEvent 
	{	
		private var _problemNatureSubCatList:ArrayCollection;
		private var _problemNatureSubCatListSelect:ArrayCollection;
		private var _updatePharmProblemNatureSubCat:Function;
		
		public function ShowPharmProblemPopupNatureOfProblemPopupEvent(problemNatureSubCatList:ArrayCollection, 
																	   problemNatureSubCatListSelect:ArrayCollection,
																	   updatePharmProblemNatureSubCat:Function=null):void
		{
			super();
			_problemNatureSubCatList = problemNatureSubCatList;
			_problemNatureSubCatListSelect = problemNatureSubCatListSelect;
			_updatePharmProblemNatureSubCat = updatePharmProblemNatureSubCat;
		}
		
		public function get problemNatureSubCatList():ArrayCollection {
			return _problemNatureSubCatList;
		}
		public function get problemNatureSubCatListSelect():ArrayCollection {
			return _problemNatureSubCatListSelect;
		}
		public function get updatePharmProblemNatureSubCat():Function {
			return _updatePharmProblemNatureSubCat;
		}
	}
}