package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSuppPerfBatchSuspViewEvent extends AbstractTideEvent
	{
		private var _event:Event;
		private var _actionFlag:String;
		
		public function RefreshSuppPerfBatchSuspViewEvent(actionFlag:String, event:Event=null):void
		{
			super();
			_event = event;
			_actionFlag = actionFlag;
		}
		
		public function get event():Event
		{
			return _event;
		}
		
		public function get actionFlag():String
		{
			return _actionFlag;
		}
	}
	
}