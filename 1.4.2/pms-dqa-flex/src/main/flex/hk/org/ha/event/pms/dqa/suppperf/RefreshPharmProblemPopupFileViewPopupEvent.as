package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
	
	public class RefreshPharmProblemPopupFileViewPopupEvent extends AbstractTideEvent 
	{
		private var _pharmProblemFileUploadData:PharmProblemFileUploadData;
		
		
		public function RefreshPharmProblemPopupFileViewPopupEvent(pharmProblemFileUploadData:PharmProblemFileUploadData):void {
			super();
			_pharmProblemFileUploadData = pharmProblemFileUploadData;			
		}
		
		public function get pharmProblemFileUploadData():PharmProblemFileUploadData 
		{
			return _pharmProblemFileUploadData;
		}
	}
}