package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaDiscrepancyListByCoaBatchEvent extends AbstractTideEvent 
	{		
		
		private var _coaBatch:CoaBatch;
		
		private var _event:Event;
		
		public function RetrieveCoaDiscrepancyListByCoaBatchEvent( coaBatch:CoaBatch, event:Event=null ):void 
		{
			super();
			_coaBatch = coaBatch;
			_event = event;
		}				
		
		public function get coaBatch():CoaBatch
		{
			return _coaBatch;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}