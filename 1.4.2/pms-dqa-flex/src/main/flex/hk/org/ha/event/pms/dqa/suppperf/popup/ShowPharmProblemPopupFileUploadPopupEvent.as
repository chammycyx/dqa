package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPharmProblemPopupFileUploadPopupEvent extends AbstractTideEvent 
	{	
		
		private var _pharmProblem:PharmProblem;
				
		private var _callBackFunc:Function;
		
		public function ShowPharmProblemPopupFileUploadPopupEvent(pharmProblem:PharmProblem, callBackFuncValue:Function=null):void
		{
			super();
			_pharmProblem = pharmProblem;
			_callBackFunc = callBackFuncValue;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
		
	}
}