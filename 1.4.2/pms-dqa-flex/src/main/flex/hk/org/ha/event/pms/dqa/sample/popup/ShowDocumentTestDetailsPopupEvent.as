package hk.org.ha.event.pms.dqa.sample.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowDocumentTestDetailsPopupEvent extends AbstractTideEvent 
	{	

		private var _testCode:String;
		
		public function ShowDocumentTestDetailsPopupEvent(testCode:String):void
		{
			super();
			_testCode = testCode;
		}
	
		public function get testCode():String {
			return _testCode;
		}
	}
}