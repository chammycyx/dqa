package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaVerificationDataListEvent extends AbstractTideEvent 
	{
		private var _callBackFunc:Function;
		
		public function RetrieveCoaVerificationDataListEvent(callBackFunc:Function):void 
		{
			super();
			_callBackFunc = callBackFunc;
		}
		
		public function get callBackFunc():Function {
			return _callBackFunc;
		}
	}
}