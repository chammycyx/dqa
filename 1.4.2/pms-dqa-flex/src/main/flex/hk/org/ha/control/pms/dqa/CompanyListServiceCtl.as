package hk.org.ha.control.pms.dqa{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.RetrieveCompanyListLikeCompanyCodeEvent;
	import hk.org.ha.event.pms.dqa.RetrieveManufacturerListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveManufacturerListLikeEvent;
	import hk.org.ha.model.pms.dqa.biz.CompanyListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("companyListServiceCtl", restrict="true")]
	public class CompanyListServiceCtl {
		
		[In]
		public var companyListService:CompanyListServiceBean;
		
		[In]
		public var companyList:ArrayCollection = new ArrayCollection();
		
		private var event:Event;
		
		[Observer]
		public function retrieveManufacturerList(evt:RetrieveManufacturerListEvent):void{
			event = evt.event;
			companyListService.retrieveManufacturerList(retrieveManufacturerListResult);			
		}
		
		public function retrieveManufacturerListResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveManufacturerListLike(evt:RetrieveManufacturerListLikeEvent):void{
			event = evt.event;
			companyListService.retrieveManufacturerListLike(evt.manufacturerCode, retrieveManufacturerListLikeResult);			
		}
		
		public function retrieveManufacturerListLikeResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveCompanyListLikeCompanyCode(evt:RetrieveCompanyListLikeCompanyCodeEvent):void{
			event = evt.event;
			companyListService.retrieveCompanyListLikeCompanyCode(evt.pharmCompanyManufCode, retrievePharmCompanyListResult);			
		}
				
		public function retrievePharmCompanyListResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
	}
}