package hk.org.ha.control.pms.dqa.sample{
	import hk.org.ha.event.pms.dqa.sample.AddInstitutionEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateInstitutionEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteInstitutionEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveInstitutionEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateInstitutionEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.InstitutionServiceBean;
	import hk.org.ha.view.pms.dqa.sample.popup.InstitutionPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("institutionServiceCtl", restrict="true")]
	public class InstitutionServiceCtl {
		
		[In]       
		public var institutionService:InstitutionServiceBean;
		
		[In]
		public var institutionPopup:InstitutionPopup;
		
		private var event:Event;
		
		[Observer]
		public function retrieveInstitution(evt:RetrieveInstitutionEvent):void {
			event = evt.event;
			institutionService.retrieveInstitutionByInstitution(evt.institution, retrieveInstitutionResult);
		}
		
		public function retrieveInstitutionResult(evt:TideResultEvent):void {
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}		
		}
		
		[Observer]
		public function createInstitution(evt:CreateInstitutionEvent):void{
			event = evt.event;
			In(Object(institutionService).success);
			In(Object(institutionService).errorCode);
			institutionService.createInstitution(evt.institution, createInstitutionResult);
		}
		
		public function createInstitutionResult(evt:TideResultEvent):void{
			if (Object(institutionService).success ){
				institutionPopup.closeWin();
				
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else if (Object(institutionService).errorCode != null){
				institutionPopup.showMessage(Object(institutionService).errorCode);
			}	
		}
		
		[Observer]
		public function addInstitution(evt:AddInstitutionEvent):void{
			event = evt.event;
			institutionService.addInstitution(addInstitutionResult);
		}
		
		public function addInstitutionResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function updateInstitution(evt:UpdateInstitutionEvent):void{
			event = evt.event;
			In(Object(institutionService).success);
			In(Object(institutionService).errorCode);
			institutionService.updateInstitution(evt.institution, updateInstitutionResult);	
		}
		
		public function updateInstitutionResult(evt:TideResultEvent):void{
			if (Object(institutionService).success ){
				institutionPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else if (Object(institutionService).errorCode != null){
				institutionPopup.showMessage(Object(institutionService).errorCode);
			}		
		}
		
		[Observer]
		public function deleteInstitution(evt:DeleteInstitutionEvent):void{
			event = evt.event;
			institutionService.deleteInstitution(evt.institution, deleteInstitutionResult);
		}
		
		public function deleteInstitutionResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}
