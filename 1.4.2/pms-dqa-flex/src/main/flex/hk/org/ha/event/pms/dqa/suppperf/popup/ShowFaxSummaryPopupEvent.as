package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	public class ShowFaxSummaryPopupEvent extends AbstractTideEvent 
	{	
		
		private var _qaProblem:QaProblem;
		
		public function ShowFaxSummaryPopupEvent(qaProblem:QaProblem):void
		{
			super();
			_qaProblem = qaProblem;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
	}
}