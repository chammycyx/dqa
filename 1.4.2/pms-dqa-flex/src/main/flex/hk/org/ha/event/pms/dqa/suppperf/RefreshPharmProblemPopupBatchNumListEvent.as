package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPharmProblemPopupBatchNumListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _batchNum:String;
		
		
		public function RefreshPharmProblemPopupBatchNumListEvent(batchNum:String, event:Event=null):void {
			super();
			_event = event;
			_batchNum = batchNum;			
		}
		
		public function get batchNum():String 
		{
			return _batchNum;
		}
	}
}