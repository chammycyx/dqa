package hk.org.ha.event.pms.dqa.coa.popup {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowBatchCoaDetailsPopupEvent extends AbstractTideEvent 
	{				
		
		private var _event:Event;
		
		public function ShowBatchCoaDetailsPopupEvent(event:Event = null):void 
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}		
	}	
}