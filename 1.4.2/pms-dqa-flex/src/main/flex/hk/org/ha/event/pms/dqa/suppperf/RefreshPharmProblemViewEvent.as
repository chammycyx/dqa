package hk.org.ha.event.pms.dqa.suppperf {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPharmProblemViewEvent extends AbstractTideEvent 
	{
	
		public function RefreshPharmProblemViewEvent():void {
			super();
		}
	}
}