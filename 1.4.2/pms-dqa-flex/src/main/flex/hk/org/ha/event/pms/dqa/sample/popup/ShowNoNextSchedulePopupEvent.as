package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	
	public class ShowNoNextSchedulePopupEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestScheduleFile:SampleTestScheduleFile;
		private var _sampleTestFile:SampleTestFile;
		
		public function ShowNoNextSchedulePopupEvent(sampleTestScheduleFile:SampleTestScheduleFile, sampleTestFile:SampleTestFile, event:Event=null):void
		{
			super();
			_event = event;
			_sampleTestScheduleFile = sampleTestScheduleFile;
			_sampleTestFile = sampleTestFile;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleFile():SampleTestScheduleFile {
			return _sampleTestScheduleFile;
		}
		
		public function get sampleTestFile():SampleTestFile {
			return _sampleTestFile;
		}
	}
}