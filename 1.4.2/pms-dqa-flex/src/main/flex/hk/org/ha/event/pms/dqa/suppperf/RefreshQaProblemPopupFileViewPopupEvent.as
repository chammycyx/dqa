package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;
	
	public class RefreshQaProblemPopupFileViewPopupEvent extends AbstractTideEvent 
	{
		private var _qaProblemFileUploadData:QaProblemFileUploadData;
		
		
		public function RefreshQaProblemPopupFileViewPopupEvent(qaProblemFileUploadData:QaProblemFileUploadData):void {
			super();
			_qaProblemFileUploadData = qaProblemFileUploadData;			
		}
		
		public function get qaProblemFileUploadData():QaProblemFileUploadData 
		{
			return _qaProblemFileUploadData;
		}
	}
}