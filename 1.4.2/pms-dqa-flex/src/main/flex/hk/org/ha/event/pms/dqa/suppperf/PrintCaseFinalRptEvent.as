package hk.org.ha.event.pms.dqa.suppperf {
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
	import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class PrintCaseFinalRptEvent extends AbstractTideEvent 
	{
		private var _qaProblem:QaProblem;
		private var _supplierContact:SupplierContact;
		private var _companyContact:CompanyContact;
		
		public function PrintCaseFinalRptEvent(qaProblem:QaProblem, supplierContact:SupplierContact, companyContact:CompanyContact):void 
		{
			super();
			_qaProblem = qaProblem;
			_supplierContact = supplierContact;
			_companyContact = companyContact;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get supplierContact():SupplierContact {
			return _supplierContact;
		}
		
		public function get companyContact():CompanyContact {
			return _companyContact;
		}
		
	}
}