package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQaProblemPopupEventLogListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _eventLogList:ListCollectionView; 
		
		public function RefreshQaProblemPopupEventLogListEvent(eventLogList:ListCollectionView,
																 event:Event=null):void {
			super();
			_event = event;
			_eventLogList = eventLogList; 
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get eventLogList():ListCollectionView {
			return _eventLogList;
		}
	}
}