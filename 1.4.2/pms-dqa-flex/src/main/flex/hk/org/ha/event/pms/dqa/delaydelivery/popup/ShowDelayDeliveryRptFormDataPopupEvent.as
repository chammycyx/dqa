package hk.org.ha.event.pms.dqa.delaydelivery.popup
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
	
	public class ShowDelayDeliveryRptFormDataPopupEvent extends AbstractTideEvent 
	{	
		private var _updateDelayDeliveryRptFormData:Function;
		private var _institutionSameClusterList:ArrayCollection;
		private var _state:String;
		private var _delayDeliveryRptFormData: DelayDeliveryRptFormData;
		
		public function ShowDelayDeliveryRptFormDataPopupEvent( state:String,
																institutionSameClusterList:ArrayCollection,
																delayDeliveryRptFormData:DelayDeliveryRptFormData=null,
																updateDelayDeliveryRptFormData:Function=null):void
		{
			super();
			_state=state;
			_institutionSameClusterList= institutionSameClusterList
			_delayDeliveryRptFormData= delayDeliveryRptFormData;
			_updateDelayDeliveryRptFormData = updateDelayDeliveryRptFormData;
		}
		public function get updateDelayDeliveryRptFormData():Function {
			return _updateDelayDeliveryRptFormData;
		}
		
		public function get institutionSameClusterList():ArrayCollection {
			return _institutionSameClusterList;
		}
		public function get state():String {
			return _state;
		}
		public function get delayDeliveryRptFormData():DelayDeliveryRptFormData {
			return _delayDeliveryRptFormData;
		}
	}
	
}