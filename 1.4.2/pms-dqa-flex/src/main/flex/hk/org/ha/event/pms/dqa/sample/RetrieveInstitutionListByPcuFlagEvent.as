package hk.org.ha.event.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveInstitutionListByPcuFlagEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _pcuFlag:YesNoFlag;
		
		public function RetrieveInstitutionListByPcuFlagEvent(pcuFlag:YesNoFlag, event:Event=null):void {
			super();
			_event = event;
			_pcuFlag = pcuFlag;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get pcuFlag():YesNoFlag {
			return _pcuFlag;
		}
	}
}