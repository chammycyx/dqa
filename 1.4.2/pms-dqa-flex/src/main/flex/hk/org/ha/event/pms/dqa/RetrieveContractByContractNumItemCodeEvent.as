package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveContractByContractNumItemCodeEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _itemCode:String;
		private var _supplierCode:String;
		
		
		public function RetrieveContractByContractNumItemCodeEvent(contractNum:String, itemCode:String, supplierCode:String):void {
			super();
			_contractNum = contractNum;
			_itemCode = itemCode;
			_supplierCode = supplierCode;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get supplierCode():String 
		{
			return _supplierCode;
		}
		
	}
}