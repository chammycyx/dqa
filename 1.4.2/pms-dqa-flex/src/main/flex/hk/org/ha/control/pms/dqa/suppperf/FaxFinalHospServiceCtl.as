package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxFinalHospEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxFinalHospRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxFinalHospRptEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.FaxFinalHospServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.popup.FaxContactPopupFinalHospPopup;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("faxFinalHospServiceCtl", restrict="true")]
	public class FaxFinalHospServiceCtl {
		
		private var event:Event;
		private var qaProblemIn:QaProblem;
		private var eventLogIn:EventLog;
		
		[In]
		public var faxFinalHospService:FaxFinalHospServiceBean;
		
		public var faxContactPopupFinalHospPopupIn:FaxContactPopupFinalHospPopup;
		
		[Observer]
		public function createFaxFinalHosp(evt:CreateFaxFinalHospEvent):void 
		{		
			event = evt.event;
			qaProblemIn = evt.qaProblem;
			eventLogIn = evt.eventLog;
			faxContactPopupFinalHospPopupIn = evt.faxContactPopupFinalHospPopup;
			In(Object(faxFinalHospService).success);
			In(Object(faxFinalHospService).errorCode);
			In(Object(faxFinalHospService).refNum);
			
			faxFinalHospService.createFaxFinalHosp(evt.qaProblem, evt.eventLog, evt.faxDetailPharmProblemList, createFaxFinalHospResult);		
		}	
		
		private function createFaxFinalHospResult(evt:TideResultEvent):void {
			if (Object(faxFinalHospService).success ){
				faxContactPopupFinalHospPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
				else
				{
					evt.context.dispatchEvent(new CreateFaxFinalHospRptEvent(qaProblemIn,
																				eventLogIn,
																				Object(faxFinalHospService).refNum,
																				false,
																				new ShowFaxFinalHospRptEvent()));
					
				}
			}else{
				if (Object(faxFinalHospService).errorCode !=null){
					faxContactPopupFinalHospPopupIn.showMessage(Object(faxFinalHospService).errorCode );
				}
			}
		}
		
		[Observer]
		public function createFaxFinalHospRpt(evt:CreateFaxFinalHospRptEvent):void 
		{		
			event = evt.event;
			faxFinalHospService.createFaxFinalHospRpt(evt.qaProblem, evt.eventLog, evt.refNum, evt.reGenFlag, createFaxFinalHospRptResult);		
		}	
		
		private function createFaxFinalHospRptResult(evt:TideResultEvent):void {
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}