package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowNatureOfProblemParameterPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		
		public function ShowNatureOfProblemParameterPopupEvent(actionType:String):void
		{
			super();
			_actionType = actionType;
		}
		
		public function get action():String {
			return _actionType;
		}
	}
}