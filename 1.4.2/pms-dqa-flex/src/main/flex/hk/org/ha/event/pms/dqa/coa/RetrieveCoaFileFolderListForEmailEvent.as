package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaFileFolderListForEmailEvent extends AbstractTideEvent 
	{		
		private var _coaBatch:CoaBatch;		
		
		private var _emailToSupplier:Boolean;
		
		private var _event:Event;
		
		public function RetrieveCoaFileFolderListForEmailEvent(coaBatch:CoaBatch, emailToSupplier:Boolean, event:Event):void 
		{
			super();
			_coaBatch = coaBatch;
			_emailToSupplier = emailToSupplier;
			_event = event;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
		
		public function get emailToSupplier():Boolean {
			return _emailToSupplier;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}