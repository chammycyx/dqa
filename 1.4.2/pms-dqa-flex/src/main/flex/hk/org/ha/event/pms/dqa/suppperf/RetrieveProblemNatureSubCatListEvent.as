package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
	
	public class RetrieveProblemNatureSubCatListEvent extends AbstractTideEvent 
	{
		
		private var _screenName:String;
		private var _problemNatureCat:ProblemNatureCat;
		private var _event:Event;
		
		public function RetrieveProblemNatureSubCatListEvent(screenName:String, problemNatureCat:ProblemNatureCat, event:Event=null):void {
			super();
			_screenName = screenName;
			_problemNatureCat = problemNatureCat;
			_event = event;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get problemNatureCat():ProblemNatureCat 
		{
			return _problemNatureCat;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}