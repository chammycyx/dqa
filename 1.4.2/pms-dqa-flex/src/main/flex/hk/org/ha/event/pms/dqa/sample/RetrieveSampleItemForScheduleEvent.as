package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleItemForScheduleEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _itemCode:String;
		
		public function RetrieveSampleItemForScheduleEvent(itemCode:String, event:Event=null):void {
			super();
			_event = event;
			_itemCode = itemCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}	
	}
}