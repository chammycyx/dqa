package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchByCoaBatchIdEvent extends AbstractTideEvent 
	{
		private var _coaBatchId:Number;		
		private var _event:Event;
		private var _callBack:Function;
		private var _status:CoaStatus;		
		
		public function RetrieveCoaBatchByCoaBatchIdEvent(coaBatchId:Number, status:CoaStatus=null ,event:Event=null, callBack:Function=null):void 
		{
			super();
			_coaBatchId = coaBatchId;
			_status= status;
			_event = event;
			_callBack = callBack;
		}
		
		public function get coaBatchId():Number {
			return _coaBatchId;
		}
		public function get status():CoaStatus {
			return _status;
		}
		public function get event():Event {
			return _event;
		}
		
		public function get callBack():Function {
			return _callBack;
		}
	}
}