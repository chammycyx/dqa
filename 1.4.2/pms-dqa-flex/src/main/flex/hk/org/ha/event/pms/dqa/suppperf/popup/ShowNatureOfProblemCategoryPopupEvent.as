package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
	
	public class ShowNatureOfProblemCategoryPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		private var _problemNatureParam:ProblemNatureParam;
		
		public function ShowNatureOfProblemCategoryPopupEvent(actionType:String, problemNatureParam:ProblemNatureParam):void
		{
			super();
			_actionType = actionType;
			_problemNatureParam = problemNatureParam;
		}
		
		public function get action():String {
			return _actionType;
		}
		
		public function get problemNatureParam():ProblemNatureParam {
			return _problemNatureParam;
		}
	}
}