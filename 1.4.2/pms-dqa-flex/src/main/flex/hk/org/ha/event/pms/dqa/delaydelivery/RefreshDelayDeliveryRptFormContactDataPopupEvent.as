package hk.org.ha.event.pms.dqa.delaydelivery {
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDelayDeliveryRptFormContactDataPopupEvent extends AbstractTideEvent 
	{
		private var _delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData;
		
		public function RefreshDelayDeliveryRptFormContactDataPopupEvent(delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData):void {
			super();
			_delayDeliveryRptFormContactData = delayDeliveryRptFormContactData;			
		}
		
		public function get delayDeliveryRptFormContactData():DelayDeliveryRptFormContactData 
		{
			return _delayDeliveryRptFormContactData;
		}
	}
}