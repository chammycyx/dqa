package hk.org.ha.event.pms.dqa.sample {
	
	import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class ValidateExclusionTestEvent extends AbstractTideEvent 
	{
		private var _exclusionTest:ExclusionTest;
		
		private var _exclusionTestList:ListCollectionView;
		
		private var _actionType:String;
		
		public function ValidateExclusionTestEvent(exclusionTest:ExclusionTest , exclusionList:ListCollectionView, actionType:String):void {
			super();
			_exclusionTest = exclusionTest;
			_exclusionTestList = exclusionList;
			_actionType = actionType;
		}
		
		public function get exclusionTest():ExclusionTest 
		{
			return _exclusionTest;
		}
		
		public function get actionType():String
		{
			return _actionType;
		}
		
		public function get exclusionTestList():ListCollectionView
		{
			return _exclusionTestList;
		}
	}
}