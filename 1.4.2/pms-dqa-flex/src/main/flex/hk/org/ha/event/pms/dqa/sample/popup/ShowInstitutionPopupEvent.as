package hk.org.ha.event.pms.dqa.sample.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowInstitutionPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		
		public function ShowInstitutionPopupEvent(actionType:String):void
		{
			super();
			_actionType = actionType;
		}
		
		public function get action():String {
			return _actionType;
		}
	}
}