package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchListByCoaStatusDiscrepancyStatusEvent extends AbstractTideEvent 
	{
		private var _coaStatus:CoaStatus;
		private var _discrepancyStatus:DiscrepancyStatus;
		private var _event:Event;
		
		public function RetrieveCoaBatchListByCoaStatusDiscrepancyStatusEvent(coaStatus:CoaStatus, discrepancyStatus:DiscrepancyStatus, event:Event=null):void 
		{
			super();
			_coaStatus = coaStatus;
			_discrepancyStatus = discrepancyStatus;
			_event = event;
		}
		
		public function get coaStatus():CoaStatus {
			return _coaStatus;
		}
		
		public function get discrepancyStatus():DiscrepancyStatus {
			return _discrepancyStatus;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}