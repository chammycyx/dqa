package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	public class RetrievePharmProblemByPharmProblemEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _pharmProblem:PharmProblem;
		private var _screenName:String;
		
		public function RetrievePharmProblemByPharmProblemEvent(pharmProblem:PharmProblem, screenName:String):void {
			super();
			_event = event;
			_pharmProblem = pharmProblem;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get pharmProblem():PharmProblem
		{
			return _pharmProblem;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}