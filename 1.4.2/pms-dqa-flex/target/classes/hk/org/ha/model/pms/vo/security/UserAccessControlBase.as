/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (UserAccessControl.as).
 */

package hk.org.ha.model.pms.vo.security {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class UserAccessControlBase implements IExternalizable {

        private var _systemMenu:SystemMenu;
        private var _userAccessInfo:UserAccessInfo;

        public function set systemMenu(value:SystemMenu):void {
            _systemMenu = value;
        }
        public function get systemMenu():SystemMenu {
            return _systemMenu;
        }

        public function set userAccessInfo(value:UserAccessInfo):void {
            _userAccessInfo = value;
        }
        public function get userAccessInfo():UserAccessInfo {
            return _userAccessInfo;
        }

        public function readExternal(input:IDataInput):void {
            _systemMenu = input.readObject() as SystemMenu;
            _userAccessInfo = input.readObject() as UserAccessInfo;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_systemMenu);
            output.writeObject(_userAccessInfo);
        }
    }
}