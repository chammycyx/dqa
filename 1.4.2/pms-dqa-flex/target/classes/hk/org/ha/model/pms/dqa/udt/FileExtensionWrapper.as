package hk.org.ha.model.pms.dqa.udt {
	
	import flash.net.FileFilter;
	
	import hk.org.ha.model.pms.dqa.udt.FileExtension;
	
	import org.granite.util.Enum;
	
	[Bindable]
	[RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.FileExtensionWrapper")]
	public class FileExtensionWrapper extends FileExtension {
		function FileExtensionWrapper(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
			super((value || JPG.name), restrictor, (dataValue || JPG.dataValue), (displayValue || JPG.displayValue));
		}
		
		public static function get supportedFile():Array {
			return [JPG,JPEG,PNG,PDF];
		}
		
		public static function getFileFilter():Array {
			var fileExtArr:Array = new Array;
			for each (var item:FileExtension in supportedFile) {
				fileExtArr.push("*"+item.dataValue.toUpperCase());
			}
			return [new FileFilter(fileExtArr.join(";"),fileExtArr.join(";"))];
		}
		
		public static function isSupportedFile(fileExt:String):Boolean {
			for each (var fileExtension:FileExtension in supportedFile) {
				if (fileExtension.dataValue.toLowerCase() == fileExt.toLowerCase()) {
					return true;
				}
			}
			return false;
		}
	}
}