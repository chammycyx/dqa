/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (Country.as).
 */

package hk.org.ha.model.pms.dqa.persistence.suppperf {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.udt.RecordStatus;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class CountryBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _countryCode:String;
        private var _countryName:String;
        private var _pharmProblemCountryList:ListCollectionView;
        private var _recordStatus:RecordStatus;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is Country) || (property as Country).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set countryCode(value:String):void {
            _countryCode = value;
        }
        [Id]
        public function get countryCode():String {
            return _countryCode;
        }

        public function set countryName(value:String):void {
            _countryName = value;
        }
        public function get countryName():String {
            return _countryName;
        }

        public function set pharmProblemCountryList(value:ListCollectionView):void {
            _pharmProblemCountryList = value;
        }
        public function get pharmProblemCountryList():ListCollectionView {
            return _pharmProblemCountryList;
        }

        public function set recordStatus(value:RecordStatus):void {
            _recordStatus = value;
        }
        public function get recordStatus():RecordStatus {
            return _recordStatus;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (!_countryCode)
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_countryCode) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:CountryBase = CountryBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._countryCode, _countryCode, null, this, 'countryCode', function setter(o:*):void{_countryCode = o as String}, false);
               em.meta_mergeExternal(src._countryName, _countryName, null, this, 'countryName', function setter(o:*):void{_countryName = o as String}, false);
               em.meta_mergeExternal(src._pharmProblemCountryList, _pharmProblemCountryList, null, this, 'pharmProblemCountryList', function setter(o:*):void{_pharmProblemCountryList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._recordStatus, _recordStatus, null, this, 'recordStatus', function setter(o:*):void{_recordStatus = o as RecordStatus}, false);
            }
            else {
               em.meta_mergeExternal(src._countryCode, _countryCode, null, this, 'countryCode', function setter(o:*):void{_countryCode = o as String});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _countryCode = input.readObject() as String;
                _countryName = input.readObject() as String;
                _pharmProblemCountryList = input.readObject() as ListCollectionView;
                _recordStatus = Enum.readEnum(input) as RecordStatus;
            }
            else {
                _countryCode = input.readObject() as String;
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_countryCode is IPropertyHolder) ? IPropertyHolder(_countryCode).object : _countryCode);
                output.writeObject((_countryName is IPropertyHolder) ? IPropertyHolder(_countryName).object : _countryName);
                output.writeObject((_pharmProblemCountryList is IPropertyHolder) ? IPropertyHolder(_pharmProblemCountryList).object : _pharmProblemCountryList);
                output.writeObject((_recordStatus is IPropertyHolder) ? IPropertyHolder(_recordStatus).object : _recordStatus);
            }
            else {
                output.writeObject(_countryCode);
            }
        }
    }
}
