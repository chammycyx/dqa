/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (ContractMsp.as).
 */

package hk.org.ha.model.pms.dqa.persistence {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.udt.RecordStatus;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class ContractMspBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _contract:Contract;
        private var _contractMspId:Number;
        private var _countryOfOrigin:String;
        private var _endDate:Date;
        private var _erpContractMspId:Number;
        private var _manufacturer:Company;
        private var _pharmCompany:Company;
        private var _recordStatus:RecordStatus;
        private var _startDate:Date;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is ContractMsp) || (property as ContractMsp).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set contract(value:Contract):void {
            _contract = value;
        }
        public function get contract():Contract {
            return _contract;
        }

        public function set contractMspId(value:Number):void {
            _contractMspId = value;
        }
        [Id]
        public function get contractMspId():Number {
            return _contractMspId;
        }

        public function set countryOfOrigin(value:String):void {
            _countryOfOrigin = value;
        }
        public function get countryOfOrigin():String {
            return _countryOfOrigin;
        }

        public function set endDate(value:Date):void {
            _endDate = value;
        }
        public function get endDate():Date {
            return _endDate;
        }

        public function set erpContractMspId(value:Number):void {
            _erpContractMspId = value;
        }
        public function get erpContractMspId():Number {
            return _erpContractMspId;
        }

        public function set manufacturer(value:Company):void {
            _manufacturer = value;
        }
        public function get manufacturer():Company {
            return _manufacturer;
        }

        public function set pharmCompany(value:Company):void {
            _pharmCompany = value;
        }
        public function get pharmCompany():Company {
            return _pharmCompany;
        }

        public function set recordStatus(value:RecordStatus):void {
            _recordStatus = value;
        }
        public function get recordStatus():RecordStatus {
            return _recordStatus;
        }

        public function set startDate(value:Date):void {
            _startDate = value;
        }
        public function get startDate():Date {
            return _startDate;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_contractMspId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_contractMspId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:ContractMspBase = ContractMspBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._contract, _contract, null, this, 'contract', function setter(o:*):void{_contract = o as Contract}, false);
               em.meta_mergeExternal(src._contractMspId, _contractMspId, null, this, 'contractMspId', function setter(o:*):void{_contractMspId = o as Number}, false);
               em.meta_mergeExternal(src._countryOfOrigin, _countryOfOrigin, null, this, 'countryOfOrigin', function setter(o:*):void{_countryOfOrigin = o as String}, false);
               em.meta_mergeExternal(src._endDate, _endDate, null, this, 'endDate', function setter(o:*):void{_endDate = o as Date}, false);
               em.meta_mergeExternal(src._erpContractMspId, _erpContractMspId, null, this, 'erpContractMspId', function setter(o:*):void{_erpContractMspId = o as Number}, false);
               em.meta_mergeExternal(src._manufacturer, _manufacturer, null, this, 'manufacturer', function setter(o:*):void{_manufacturer = o as Company}, false);
               em.meta_mergeExternal(src._pharmCompany, _pharmCompany, null, this, 'pharmCompany', function setter(o:*):void{_pharmCompany = o as Company}, false);
               em.meta_mergeExternal(src._recordStatus, _recordStatus, null, this, 'recordStatus', function setter(o:*):void{_recordStatus = o as RecordStatus}, false);
               em.meta_mergeExternal(src._startDate, _startDate, null, this, 'startDate', function setter(o:*):void{_startDate = o as Date}, false);
            }
            else {
               em.meta_mergeExternal(src._contractMspId, _contractMspId, null, this, 'contractMspId', function setter(o:*):void{_contractMspId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _contract = input.readObject() as Contract;
                _contractMspId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _countryOfOrigin = input.readObject() as String;
                _endDate = input.readObject() as Date;
                _erpContractMspId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _manufacturer = input.readObject() as Company;
                _pharmCompany = input.readObject() as Company;
                _recordStatus = Enum.readEnum(input) as RecordStatus;
                _startDate = input.readObject() as Date;
            }
            else {
                _contractMspId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_contract is IPropertyHolder) ? IPropertyHolder(_contract).object : _contract);
                output.writeObject((_contractMspId is IPropertyHolder) ? IPropertyHolder(_contractMspId).object : _contractMspId);
                output.writeObject((_countryOfOrigin is IPropertyHolder) ? IPropertyHolder(_countryOfOrigin).object : _countryOfOrigin);
                output.writeObject((_endDate is IPropertyHolder) ? IPropertyHolder(_endDate).object : _endDate);
                output.writeObject((_erpContractMspId is IPropertyHolder) ? IPropertyHolder(_erpContractMspId).object : _erpContractMspId);
                output.writeObject((_manufacturer is IPropertyHolder) ? IPropertyHolder(_manufacturer).object : _manufacturer);
                output.writeObject((_pharmCompany is IPropertyHolder) ? IPropertyHolder(_pharmCompany).object : _pharmCompany);
                output.writeObject((_recordStatus is IPropertyHolder) ? IPropertyHolder(_recordStatus).object : _recordStatus);
                output.writeObject((_startDate is IPropertyHolder) ? IPropertyHolder(_startDate).object : _startDate);
            }
            else {
                output.writeObject(_contractMspId);
            }
        }
    }
}
