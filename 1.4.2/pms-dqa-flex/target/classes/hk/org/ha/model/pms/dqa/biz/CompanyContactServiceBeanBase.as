/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (CompanyContactServiceBean.as).
 */

package hk.org.ha.model.pms.dqa.biz {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
    import hk.org.ha.model.pms.dqa.persistence.Contact;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class CompanyContactServiceBeanBase extends Component {

        public function isUpdateSucceed(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isUpdateSucceed", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isUpdateSucceed", resultHandler);
            else if (resultHandler == null)
                callProperty("isUpdateSucceed");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveContactIdListByFirstNameLastNameEmail(arg0:Contact, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveContactIdListByFirstNameLastNameEmail", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveContactIdListByFirstNameLastNameEmail", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveContactIdListByFirstNameLastNameEmail", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveCompanyContactIdListByFirstNameLastNameEmail(arg0:CompanyContact, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveCompanyContactIdListByFirstNameLastNameEmail", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveCompanyContactIdListByFirstNameLastNameEmail", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveCompanyContactIdListByFirstNameLastNameEmail", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function addCompanyContact(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("addCompanyContact", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("addCompanyContact", resultHandler);
            else if (resultHandler == null)
                callProperty("addCompanyContact");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createCompanyContact(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createCompanyContact", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createCompanyContact", resultHandler);
            else if (resultHandler == null)
                callProperty("createCompanyContact");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrieveCompanyContactByCompanyCodeContactId(arg0:String, arg1:Number, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrieveCompanyContactByCompanyCodeContactId", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrieveCompanyContactByCompanyCodeContactId", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("retrieveCompanyContactByCompanyCodeContactId", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function updateCompanyContact(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("updateCompanyContact", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("updateCompanyContact", resultHandler);
            else if (resultHandler == null)
                callProperty("updateCompanyContact");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function deleteCompanyContact(arg0:CompanyContact, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("deleteCompanyContact", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("deleteCompanyContact", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("deleteCompanyContact", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
