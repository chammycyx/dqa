package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.EmailLogManagerLocal;
import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchManagerBean.CoaBatchResult;
import hk.org.ha.model.pms.dqa.exception.ConcurrentUpdateException;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.ConfirmCheckListFlag;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaBatchService")
@RemoteDestination
@MeasureCalls
public class CoaBatchServiceBean implements CoaBatchServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	@Out(required = false)
	private CoaBatch coaBatch;

	@In(create = true)
	private CoaFileFolderManagerLocal coaFileFolderManager;

	@In(create = true)
	private EmailLogManagerLocal emailLogManager;
	
	@In(create = true)
	private CoaBatchManagerLocal coaBatchManager;
	
	private boolean duplicated;
	
	private String errorCode;

	public boolean retrieveCoaBatchByCoaBatch(CoaBatch coaBatchIn, CoaStatus status){
		logger.debug("retrieveCoaBatchByCoaBatch #0", coaBatchIn.getCoaBatchId());
		
		coaBatch = (CoaBatch)em.createNamedQuery("CoaBatch.findByCoaBatchId")
							   .setHint(QueryHints.FETCH, "o.coaItem")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract.supplier")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract.manufacturer")
							   .setHint(QueryHints.LEFT_FETCH, "o.coaDiscrepancyList")
							   .setHint(QueryHints.LEFT_FETCH, "o.coaDiscrepancyList.coaDiscrepancyKey")
							   .setParameter("coaBatchId", coaBatchIn.getCoaBatchId())
							   .getSingleResult();

		if( !coaBatch.getVersion().equals(coaBatchIn.getVersion()) ) {
			return true;
		}
		
		if(status != null){
			if (!coaBatch.getCoaBatchVer().getCoaStatus().equals( status )) {
				return true;
			}
		}
		
		if( coaBatch.getCoaDiscrepancyList().size() > 0 ) {
			List<CoaDiscrepancy> cDList = coaBatch.getCoaDiscrepancyList();
			Collections.sort( cDList,
					new CoaDiscrepancyComparator()
			);
			StringBuffer combinedDiscrepancy = new StringBuffer();
			for(CoaDiscrepancy coaDiscrepancy:coaBatch.getCoaDiscrepancyList()) {
				if(coaDiscrepancy.getPassFlag() == PassFlag.Passed) {continue;}
				combinedDiscrepancy.append(coaDiscrepancy.getCoaDiscrepancyKey().getDiscrepancyDesc())
				.append("<br>");
			}
			coaBatch.setCombinedDiscrepancy(combinedDiscrepancy.toString());
			em.flush();
		}
		
		coaBatch.setFormattedCreateDate(String.format("%1$td-%1$tb-%1$tY", coaBatch.getCoaBatchVer().getCreateDate()));
		coaBatch.setFormattedFollowUpDate(String.format("%1$td-%1$tb-%1$tY", coaBatch.getFollowUpDate()));
		
		return false;
	}
	
	public boolean retrieveCoaBatchByCoaBatchId(Long coaBatchId, CoaStatus status) {
		logger.debug("retrieveCoaBatchByCoaBatchId #0", coaBatchId);
		coaBatch = (CoaBatch)em.createNamedQuery("CoaBatch.findByCoaBatchId")
							   .setHint(QueryHints.FETCH, "o.coaItem")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract.supplier")
							   .setHint(QueryHints.FETCH, "o.coaItem.contract.manufacturer")
				               .setParameter("coaBatchId", coaBatchId)
				               .getSingleResult();		
		
		if(status != null){
			if (!coaBatch.getCoaBatchVer().getCoaStatus().equals( status )) {
				return true;
			}
		}
		
		return false;
	}
	
	
	public void updateCoaBatch() {
		logger.debug("updateCoaBatch #0", coaBatch.getCoaBatchId());
		coaBatch=em.merge(coaBatch);
		em.flush();
	}

	public void updateCoaBatchStatus(CoaBatch coaBatchIn, CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus) {		
		updateCoaBatchVerStatus(coaBatchIn, coaStatus, discrepancyStatus);
		em.flush();
	}
	
	private CoaBatch getLockedCoaBatch(CoaBatch coaBatchIn) {
		return (CoaBatch) em.createNamedQuery("CoaBatch.findByCoaBatchId")
		.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
		.setParameter("coaBatchId", coaBatchIn.getCoaBatchId())
		.getSingleResult();	
	}
	
	public void updateCoaBatchVerStatus(CoaBatch coaBatchIn, CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus) {
		
		logger.debug("updateCoaBatchVerStatus #0 #1 #2", 
				coaBatchIn.getCoaBatchId(),
				coaBatchIn.getCoaBatchVer().getCoaStatus(), 
				coaBatchIn.getCoaBatchVer().getDiscrepancyStatus());				

		CoaBatch coaBatchInDB = getLockedCoaBatch(coaBatchIn);
		
		if (!coaBatchInDB.getVersion().equals(coaBatchIn.getVersion())) {			
			throw new ConcurrentUpdateException("Can't update coa batch status");
		}
		
		CoaBatchVer newCoaBatchVer = new CoaBatchVer();
		CoaBatchVer orgCoaBatchVer = coaBatchInDB.getCoaBatchVer();

		newCoaBatchVer.setCoaBatch(coaBatchInDB);
		newCoaBatchVer.setCoaStatus(coaStatus);
		newCoaBatchVer.setDiscrepancyStatus(discrepancyStatus);		
		newCoaBatchVer.setRecordStatus(RecordStatus.Active);
		newCoaBatchVer.setEmailLog(orgCoaBatchVer.getEmailLog());
		
		if( newCoaBatchVer.getDiscrepancyStatus() == DiscrepancyStatus.ReminderToSupplier ) {
			newCoaBatchVer.setReminderNum( 
					( ( orgCoaBatchVer.getReminderNum() == null )?
							0:orgCoaBatchVer.getReminderNum()) + 1 );
		} else {
			newCoaBatchVer.setReminderNum( null );
		}

		em.persist(newCoaBatchVer);	
		coaBatchInDB.setCoaBatchVerCount(coaBatchInDB.getCoaBatchVerCount()+1);
		coaBatchInDB.setCfmChecklistFlag(coaBatchIn.getCfmChecklistFlag());
		coaBatchInDB.setCoaBatchVer(newCoaBatchVer);						
		em.merge(coaBatchInDB);	
		
		em.flush();
		
		coaBatch = null;
	}
	
	public void createCoaBatch(CoaBatch coaBatchIn)
	{
		CoaBatchResult coaBatchResult = coaBatchManager.createCoaBatch(coaBatchIn);
		if (coaBatchResult.getCoaBatch() != null)
		{
			coaBatch = coaBatchResult.getCoaBatch();
		}
		
		duplicated = coaBatchResult.isDuplicate();
	}

	public void updateCoaBatchForRevert(CoaBatchVer coaBatchVer) {
		logger.debug("updateCoaBatchForRevert #0", coaBatchVer.getCoaBatchVerId());
		HashSet<CoaBatch> coaBatchHS = new HashSet<CoaBatch>();
		CoaBatch parentCoaBatch = coaBatchVer.getCoaBatch();

		parentCoaBatch = em.find(CoaBatch.class, parentCoaBatch.getCoaBatchId());		

		if(coaBatchVer.getCoaStatus() == CoaStatus.VerificationInProgress) {// if CoaStatus change from Discrepancy to Verification

			parentCoaBatch.getCoaDiscrepancyList().size();// get parentCoaDiscrepancyList

			for(CoaDiscrepancy parentCoaDiscrepancy:parentCoaBatch.getCoaDiscrepancyList()) {			

				parentCoaDiscrepancy.getChildCoaDiscrepancyList().size();// get childCoaDiscrepancyList 								

				for(CoaDiscrepancy childCoaDiscrepancy:parentCoaDiscrepancy.getChildCoaDiscrepancyList()) {									
					coaBatchHS.add( childCoaDiscrepancy.getCoaBatch() );
					childCoaDiscrepancy.setParentCoaDiscrepancy(null);// set child's parentCoaDiscrepancy as null				
					em.merge( childCoaDiscrepancy );

				}
				em.remove( em.merge( parentCoaDiscrepancy ) ); // remove parentCoaDiscrepancy
			}

			coaBatchVer.getCoaBatch().setDiscrepancyRemark(null);// set null to parent CoaBatch's discrepancyRemark
			coaBatchVer.getCoaBatch().setCfmChecklistFlag(ConfirmCheckListFlag.NullValue); // restore checklist flag as new add item
		}			

		parentCoaBatch.getCoaBatchVerList().size();// get coaBatchVerList		

		for(CoaBatchVer cbv:parentCoaBatch.getCoaBatchVerList()) {
			//logical delete all CoaBatchVer those create_date is after the reverted CoaBatchVer			
			if( cbv.getRecordStatus() == RecordStatus.Active && 
					cbv.getCreateDate().compareTo( coaBatchVer.getCreateDate() ) >= 0 ) {

				cbv.setRecordStatus(RecordStatus.Deleted);
				em.merge( cbv );
			}			
		}
		
		Integer orgReminderNum = coaBatchVer.getReminderNum();
		
		if( orgReminderNum != null ) {
			coaBatchVer.getCoaBatch().getCoaBatchVer().setReminderNum( orgReminderNum.intValue() - 1 );
		} else {
			coaBatchVer.getCoaBatch().getCoaBatchVer().setReminderNum( null );
		}
		
		updateCoaBatchVerStatus(coaBatchVer.getCoaBatch(), coaBatchVer.getCoaStatus(), coaBatchVer.getDiscrepancyStatus());

		changeCoaBatchDiscrepancyStatusFromGtoP( coaBatchHS );

		em.flush();
	}	

	public void updateCoaBatchForFailCoa(CoaBatch coaBatchIn){
		logger.debug("updateCoaBatchForFailCoa #0", coaBatchIn.getCoaBatchId());

		HashSet<CoaBatch> coaBatchHS = new HashSet<CoaBatch>();		

		em.merge(coaBatchIn);
		CoaBatch coaBatchFromDb = em.find(CoaBatch.class, coaBatchIn.getCoaBatchId());
		coaBatchFromDb.getCoaDiscrepancyList().size();				

		for( CoaDiscrepancy cd:coaBatchFromDb.getCoaDiscrepancyList() ) {

			if(cd.getParentCoaDiscrepancy() != null) {

				cd.setParentCoaDiscrepancy(null);
				em.merge( cd );

			} else {

				cd.getChildCoaDiscrepancyList().size();

				for( CoaDiscrepancy childCoaDiscrepancy:cd.getChildCoaDiscrepancyList() ) {
					childCoaDiscrepancy.setParentCoaDiscrepancy(null);
					coaBatchHS.add( childCoaDiscrepancy.getCoaBatch() );
					em.merge( childCoaDiscrepancy );
				}				
			}			
		}		

		changeCoaBatchDiscrepancyStatusFromGtoP( coaBatchHS );
		updateCoaBatchVerStatus( coaBatchFromDb, CoaStatus.Fail, coaBatchIn.getCoaBatchVer().getDiscrepancyStatus() );
		em.flush();

	}
	
	public void updateCoaBatchForErpInterface(CoaBatch coaBatchIn) {
		logger.debug("updateCoaBatchForErpInterface #0", coaBatchIn.getBatchNum());
		coaBatchIn.setInterfaceFlag( InterfaceFlag.Yes );
		coaBatchIn.setInterfaceDate( new Date() );
		em.merge(coaBatchIn);					
		em.flush();		
	}
	
	private void changeCoaBatchDiscrepancyStatusFromGtoP( Set<CoaBatch> coaBatchHS ){
		//Update Child CoaBatch
		Iterator<CoaBatch> it = coaBatchHS.iterator();		

		while( it.hasNext() ) {			
			CoaBatch childCoaBatch = it.next();
			if( childCoaBatch.getCoaBatchVer().getCoaStatus() != CoaStatus.DiscrepancyClarificationInProgress ) { continue; }//ignore passed CoaBatch
			//Change discrepancyStatus to "P" when child CoaBatch's discrepancyStatus = "G" 
			if( childCoaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.DiscrepancyUnderGrouping ) {				
				updateCoaBatchVerStatus( childCoaBatch, childCoaBatch.getCoaBatchVer().getCoaStatus(), DiscrepancyStatus.PharmacistFollowUp );				
			}																		
		}	
	}
	
	public void createEmailLogForCoa( CoaBatch coaBatchIn, final LetterTemplate letterTemplate, EmailType emailType) {
		EmailLog emailLog = emailLogManager.createEmailLogByLetterTemplate(letterTemplate, emailType);
		coaBatchIn.getCoaBatchVer().setEmailLog(emailLog);
		em.merge( coaBatchIn.getCoaBatchVer() );
		em.flush();
	}
	
	public void deleteCoaBatch(CoaBatch coaBatchIn)
	{
		logger.debug("deleteCoaBatch : #0", coaBatchIn.getCoaBatchId());
		CoaBatch cb = em.find(CoaBatch.class, coaBatchIn.getCoaBatchId());
		
		errorCode =null;
		if (!cb.getCoaFileFolderList().isEmpty()){
			errorCode = "0020";
		} else {
			cb.getCoaDiscrepancyList().size();		
			cb.getCoaBatchVerList().size();
			
			for (CoaDiscrepancy tmpDisc :cb.getCoaDiscrepancyList() ){

				for (CoaDiscrepancy childDisc :tmpDisc.getChildCoaDiscrepancyList()){
					childDisc.setParentCoaDiscrepancy(null);					
					em.merge(childDisc);
				}				
				tmpDisc.setCoaBatch(null);
				em.merge(tmpDisc);
			}

			for(CoaBatchVer cbv:cb.getCoaBatchVerList()) {
				cbv.setCoaBatch(null);
			}

			cb = em.merge(cb);

			for (CoaFileFolder cff : cb.getCoaFileFolderList()){
				coaFileFolderManager.deletePhysicalFile(cff.getFileItem().getFilePath());
			}
			em.remove(cb);		
			em.flush();
		}
	}
	
	
	public boolean isDuplicated(){
		return duplicated;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	@Remove
	public void destroy(){
		if(coaBatch != null){
			coaBatch = null;
		}
	}

	public static class CoaDiscrepancyComparator implements Comparator<CoaDiscrepancy>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		public int compare(CoaDiscrepancy c1, CoaDiscrepancy c2){
			return c1.getCoaDiscrepancyKey()
			.getOrderSeq()
			.compareTo( 
					c2.getCoaDiscrepancyKey()
					.getOrderSeq() 
			);
		}						
	}

}
