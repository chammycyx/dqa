package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.phs.PhsException;
import hk.org.ha.model.pms.asa.vo.dqa.DqaCompany;
import hk.org.ha.model.pms.asa.vo.dqa.DqaInstitution;
import hk.org.ha.model.pms.asa.vo.dqa.DqaStockMonthlyExp;
import hk.org.ha.model.pms.asa.vo.dqa.DqaSupplier;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.FuncSeqNum;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.StockMonthlyExp;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.service.pms.asa.interfaces.phs.PhsServiceJmsRemote;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("phsImportService")
@RemoteDestination
@MeasureCalls
public class PhsImportServiceBean implements PhsImportServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;	
	
	@In
	private PhsServiceJmsRemote phsServiceProxy;
	
	public Boolean importSupplier()
	{
		Map<String, DqaSupplier> phsSupplierListMap = new HashMap<String, DqaSupplier>();
		Calendar supplierUpdateDate = Calendar.getInstance();
		Calendar compareDate = null;
		Supplier supplierFind;
		Supplier supplierInsert;
		Contact contactInsert;
		try {
			List<DqaSupplier> supplierListFind = phsServiceProxy.retrieveSupplierForDqa();
			int countUpdate = 0;
			
			for(DqaSupplier ds:supplierListFind)
			{
				if (!phsSupplierListMap.containsKey(ds.getSupplierCode())) {
					phsSupplierListMap.put(ds.getSupplierCode(), ds);
				}
				
				compareDate = Calendar.getInstance();
				supplierUpdateDate.setTime(ds.getUploadDate());
				compareDate.add(Calendar.DATE, -3);

				if(supplierUpdateDate.compareTo(compareDate)>=0)
				{
					supplierFind = em.find(Supplier.class, ds.getSupplierCode());

					if(supplierFind==null)
					{
						supplierInsert = new Supplier();
						supplierInsert.setCreateDate(ds.getUploadDate());
						supplierInsert.setModifyDate(ds.getUploadDate());
						supplierInsert.setSupplierCode(ds.getSupplierCode());
						supplierInsert.setSupplierName(ds.getSupplierName());
						if(("Y").equals(ds.getSuspendFlag()))
						{
							supplierInsert.setSuspendFlag(SuspendFlag.Suspended);
						}
						else
						{
							supplierInsert.setSuspendFlag(SuspendFlag.Unsuspended);
						}
						supplierInsert.setUploadDate(new Date());
						
						contactInsert = new Contact();
						contactInsert.setAddress1(ds.getAddress1());
						contactInsert.setAddress2(ds.getAddress2());
						contactInsert.setAddress3(ds.getAddress3());
						contactInsert.setCreateDate(ds.getUploadDate());
						contactInsert.setEmail(ds.getEmail());
						contactInsert.setFax(ds.getFax());
						contactInsert.setModifyDate(ds.getUploadDate());
						contactInsert.setOfficePhone(ds.getOfficePhone());
						contactInsert.setFirstName(ds.getSupplierName());
						contactInsert.setTitle(TitleType.NIL);
						em.persist(contactInsert);
						em.flush();
						
						supplierInsert.setContact(contactInsert);
						em.persist(supplierInsert);
						em.flush();
						
						countUpdate++;
					}
					else if(supplierFind.getModifyDate().compareTo(ds.getUploadDate())!=0)
					{
						supplierFind.setModifyDate(ds.getUploadDate());
						supplierFind.setSupplierName(ds.getSupplierName());
						if(("Y").equals(ds.getSuspendFlag()))
						{
							supplierFind.setSuspendFlag(SuspendFlag.Suspended);
						}
						else
						{
							supplierFind.setSuspendFlag(SuspendFlag.Unsuspended);
						}
						supplierFind.setUploadDate(new Date());
						
						if(supplierFind.getContact()!=null)
						{
							supplierFind.getContact().setAddress1(ds.getAddress1());
							supplierFind.getContact().setAddress2(ds.getAddress2());
							supplierFind.getContact().setAddress3(ds.getAddress3());
							supplierFind.getContact().setEmail(ds.getEmail());
							supplierFind.getContact().setFax(ds.getFax());
							supplierFind.getContact().setOfficePhone(ds.getOfficePhone());
							supplierFind.getContact().setModifyDate(ds.getUploadDate());
						
							em.merge(supplierFind.getContact());
							em.flush();
						}
						else
						{
							contactInsert = new Contact();
							contactInsert.setAddress1(ds.getAddress1());
							contactInsert.setAddress2(ds.getAddress2());
							contactInsert.setAddress3(ds.getAddress3());
							contactInsert.setCreateDate(ds.getUploadDate());
							contactInsert.setEmail(ds.getEmail());
							contactInsert.setFax(ds.getFax());
							contactInsert.setModifyDate(ds.getUploadDate());
							contactInsert.setOfficePhone(ds.getOfficePhone());
							contactInsert.setFirstName(ds.getSupplierName());
							contactInsert.setTitle(TitleType.NIL);
							em.persist(contactInsert);
							em.flush();
							
							supplierFind.setContact(contactInsert);
						}
						
						em.merge(supplierFind);
						em.flush();
						countUpdate++;
					}
				}
				
			}
			
			int countSuspend = 0;
			List<Supplier> supplierListAll = em.createNamedQuery("Supplier.findAll").getResultList();
			if(supplierListAll!=null && supplierListAll.size()>0)
			{
				for (Supplier s:supplierListAll) {
					if (!phsSupplierListMap.containsKey(s.getSupplierCode()) && s.getSuspendFlag().equals(SuspendFlag.Unsuspended)) {
						s.setSuspendFlag(SuspendFlag.Suspended);
						s.setModifyDate(new Date());
						em.merge(s);
						em.flush();
						countSuspend++;
					}
				}
			}
			logger.info("Import PHS Supplier complete. #0 record(s) updated. #1 record(s) status updated to suspended. ", countUpdate, countSuspend);
			
		} catch (PhsException e) {
			logger.error("Fail to import supplier.", e);
			return false;
		}

		return true;
	}
	
	public Boolean importCompany()
	{
		Map<String, DqaCompany> phsCompanyListMap = new HashMap<String, DqaCompany>();
		Calendar companyUpdateDate = Calendar.getInstance();
		Calendar compareDate = null;
		Company companyFind;
		Company companyInsert;
		Contact contactInsert;
		try {
			List<DqaCompany> companyListFind = phsServiceProxy.retrieveCompanyForDqa();

			int countUpdate = 0;
			for(DqaCompany dc:companyListFind)
			{
				if (!phsCompanyListMap.containsKey(dc.getCompanyCode())) {
					phsCompanyListMap.put(dc.getCompanyCode(), dc);
				}
				
				compareDate = Calendar.getInstance();
				companyUpdateDate.setTime(dc.getUploadDate());
				compareDate.add(Calendar.DATE, -3);
				if(companyUpdateDate.compareTo(compareDate)>=0)
				{
					companyFind = em.find(Company.class, dc.getCompanyCode());
					if(companyFind==null)
					{
						companyInsert = new Company();
						companyInsert.setCompanyCode(dc.getCompanyCode());
						companyInsert.setCompanyName(dc.getCompanyName());
						companyInsert.setCreateDate(dc.getUploadDate());
						if(("Y").equals(dc.getManufacturerFlag()))
						{
							companyInsert.setManufFlag(YesNoFlag.Yes);
						}
						else
						{
							companyInsert.setManufFlag(YesNoFlag.No);
						}
						companyInsert.setModifyDate(dc.getUploadDate());
						if(("Y").equals(dc.getPharmCompanyFlag()))
						{
							companyInsert.setPharmCompanyFlag(YesNoFlag.Yes);
						}
						else
						{
							companyInsert.setPharmCompanyFlag(YesNoFlag.No);
						}
						
						if(("Y").equals(dc.getSuspendFlag()))
						{
							companyInsert.setSuspendFlag(SuspendFlag.Suspended);
						}
						else
						{
							companyInsert.setSuspendFlag(SuspendFlag.Unsuspended);
						}
						companyInsert.setUploadDate(new Date());
						
						contactInsert = constructCompanyContactInsert(dc);
						em.persist(contactInsert);
						em.flush();
						
						companyInsert.setContact(contactInsert);
						em.persist(companyInsert);
						em.flush();
						countUpdate++;
					}
					else if(companyFind.getModifyDate().compareTo(dc.getUploadDate())!=0)
					{
						companyFind.setCompanyName(dc.getCompanyName());
						if(("Y").equals(dc.getManufacturerFlag()))
						{
							companyFind.setManufFlag(YesNoFlag.Yes);
						}
						else
						{
							companyFind.setManufFlag(YesNoFlag.No);
						}
						companyFind.setModifyDate(dc.getUploadDate());
						if(("Y").equals(dc.getPharmCompanyFlag()))
						{
							companyFind.setPharmCompanyFlag(YesNoFlag.Yes);
						}
						else
						{
							companyFind.setPharmCompanyFlag(YesNoFlag.No);
						}
						
						if(("Y").equals(dc.getSuspendFlag()))
						{
							companyFind.setSuspendFlag(SuspendFlag.Suspended);
						}
						else
						{
							companyFind.setSuspendFlag(SuspendFlag.Unsuspended);
						}
						companyFind.setUploadDate(new Date());
						
						if(companyFind.getContact()!=null)
						{
							companyFind.getContact().setAddress1(dc.getAddress1());
							companyFind.getContact().setAddress2(dc.getAddress2());
							companyFind.getContact().setAddress3(dc.getAddress3());
							companyFind.getContact().setEmail(dc.getEmail());
							companyFind.getContact().setFax(dc.getFax());
							companyFind.getContact().setModifyDate(dc.getUploadDate());
							companyFind.getContact().setOfficePhone(dc.getOfficePhone());
							em.merge(companyFind.getContact());
							em.flush();
						}
						else
						{
							contactInsert = new Contact();
							contactInsert.setAddress1(dc.getAddress1());
							contactInsert.setAddress2(dc.getAddress2());
							contactInsert.setAddress3(dc.getAddress3());
							contactInsert.setCreateDate(dc.getUploadDate());
							contactInsert.setEmail(dc.getEmail());
							contactInsert.setFax(dc.getFax());
							contactInsert.setModifyDate(dc.getUploadDate());
							contactInsert.setOfficePhone(dc.getOfficePhone());
							contactInsert.setFirstName(dc.getCompanyName());
							contactInsert.setTitle(TitleType.NIL);
							em.persist(contactInsert);
							em.flush();
							
							companyFind.setContact(contactInsert);
						}
						
						em.merge(companyFind);
						em.flush();
						countUpdate++;
					}
				}
			}
			
			int countSuspend = 0;
			List<Company> companyListAll = em.createNamedQuery("Company.findAll").getResultList();
			if(companyListAll!=null && companyListAll.size()>0)
			{
				for (Company c:companyListAll) {
					if (!phsCompanyListMap.containsKey(c.getCompanyCode()) && c.getSuspendFlag().equals(SuspendFlag.Unsuspended)) {
						c.setSuspendFlag(SuspendFlag.Suspended);
						c.setModifyDate(new Date());
						em.merge(c);
						em.flush();
						countSuspend++;
					}
				}
			}
			logger.info("Import PHS company complete. #0 record(s) updated. #1 record(s) status updated to suspended. ", countUpdate, countSuspend);
			
		} catch (PhsException e) {
			logger.error("Fail to import company.", e);
			return false;
		}
		return true;
	}
	
	private Contact constructCompanyContactInsert(DqaCompany dc)
	{
		Contact contactInsert = new Contact();
		contactInsert.setAddress1(dc.getAddress1());
		contactInsert.setAddress2(dc.getAddress2());
		contactInsert.setAddress3(dc.getAddress3());
		contactInsert.setCreateDate(dc.getUploadDate());
		contactInsert.setEmail(dc.getEmail());
		contactInsert.setFax(dc.getFax());
		contactInsert.setModifyDate(dc.getUploadDate());
		contactInsert.setOfficePhone(dc.getOfficePhone());
		contactInsert.setFirstName(dc.getCompanyName());
		contactInsert.setTitle(TitleType.NIL);
		return contactInsert;
	}
	
				
	public Boolean importInstitution()
	{
		Calendar institutionUpdateDate = Calendar.getInstance();
		Calendar compareDate = null;
		Institution institutionFind;
		Institution institutionInsert;
		Contact contactInsert;
		try {
			List<DqaInstitution> institutionListFind = phsServiceProxy.retrieveInstitutionForDqa();
			for(DqaInstitution di:institutionListFind)
			{
				compareDate = Calendar.getInstance();
				institutionUpdateDate.setTime(di.getUploadDate());
				compareDate.add(Calendar.DATE, -3);
				if(institutionUpdateDate.compareTo(compareDate)>=0)
				{
					institutionFind = em.find(Institution.class, di.getInstitutionCode());
					if(institutionFind==null)
					{
						institutionInsert = new Institution();
						institutionInsert.setCreateDate(di.getUploadDate());
						institutionInsert.setGopcFlag(di.getGopcFlag());
						institutionInsert.setInstitutionCluster(di.getCluster());
						institutionInsert.setInstitutionCode(di.getInstitutionCode());
						institutionInsert.setInstitutionName(di.getInstitutionName());
						institutionInsert.setModifyDate(di.getUploadDate());
						institutionInsert.setPcuCode(di.getPcuCode());
						if(("Y").equals(di.getPcuFlag()))
						{
							institutionInsert.setPcuFlag(YesNoFlag.Yes);
						}
						else
						{
							institutionInsert.setPcuFlag(YesNoFlag.No);
						}
						institutionInsert.setRecordStatus(di.getRecordStatus());
						
						
						contactInsert = new Contact();
						contactInsert.setAddress1(di.getAddress1());
						contactInsert.setAddress2(di.getAddress2());
						contactInsert.setAddress3(di.getAddress3());
						contactInsert.setTitle(TitleType.NIL);
						contactInsert.setCreateDate(di.getUploadDate());
						contactInsert.setModifyDate(di.getUploadDate());
						
						em.persist(contactInsert);
						em.flush();
						
						institutionInsert.setContact(contactInsert);
						em.persist(institutionInsert);
						em.flush();
					}
					else if(institutionFind.getModifyDate().compareTo(di.getUploadDate())!=0)
					{
						institutionFind.setGopcFlag(di.getGopcFlag());
						institutionFind.setInstitutionCluster(di.getCluster());
						institutionFind.setModifyDate(di.getUploadDate());
						institutionFind.setPcuCode(di.getPcuCode());
						if(("Y").equals(di.getPcuFlag()))
						{
							institutionFind.setPcuFlag(YesNoFlag.Yes);
						}
						else
						{
							institutionFind.setPcuFlag(YesNoFlag.No);
						}
						institutionFind.setRecordStatus(di.getRecordStatus());
						
						if(institutionFind.getContact()!=null)
						{
							institutionFind.getContact().setAddress1(di.getAddress1());
							institutionFind.getContact().setAddress2(di.getAddress2());
							institutionFind.getContact().setAddress3(di.getAddress3());
							
							em.merge(institutionFind.getContact());
							em.flush();
						}
						else
						{
							contactInsert = new Contact();
							contactInsert.setAddress1(di.getAddress1());
							contactInsert.setAddress2(di.getAddress2());
							contactInsert.setAddress3(di.getAddress3());
							contactInsert.setTitle(TitleType.NIL);
							contactInsert.setCreateDate(di.getUploadDate());
							contactInsert.setModifyDate(di.getUploadDate());
							
							em.persist(contactInsert);
							em.flush();
						
							institutionFind.setContact(contactInsert);
						}
						
						em.merge(institutionFind);
						em.flush();
					}
				}
				
			}
		} catch (PhsException e) {
			logger.error("Fail to import institution.", e);
			return false;
		}

		return true;
	}
	
	public Boolean importStockMonthlyExp()
	{
		Calendar todayDate = Calendar.getInstance();
		int year = todayDate.get(Calendar.YEAR);
		int month = todayDate.get(Calendar.MONTH);

		List<StockMonthlyExp> stockMonthlyExpList = em.createNamedQuery("StockMonthlyExp.findByYearMonth")
													.setParameter("year", year)
													.setParameter("month", month)
													.getResultList();
		
		int countInsert = 0;
		if(stockMonthlyExpList!=null && stockMonthlyExpList.size()>0)
		{
			//record already exist
			logger.error("Fail to import stock Monthly exp. Records already exist for year: #0, month: #1", year, month);
			return false;
		}
		else
		{
			List<Institution> institutionListFind = em.createNamedQuery("Institution.findByPcuFlag")
														.setParameter("pcuFlag", YesNoFlag.Yes)
														.getResultList();
			
			Map<String, Institution> institutionMap = new HashMap<String, Institution>();
			for(Institution inst: institutionListFind)
			{
				 if (!institutionMap.containsKey(inst.getInstitutionCode())){
					 institutionMap.put(inst.getInstitutionCode(), inst);
				 }                 
			}

			List<DqaStockMonthlyExp> stockMonthlyExpListFind = phsServiceProxy.retrieveStockMonthlyExpForDqa(year, month);
			for(DqaStockMonthlyExp sme:stockMonthlyExpListFind)
			{
				for(String dqaInst: sme.getInstitutionCodeList())
				{
					Institution instFind = institutionMap.get(dqaInst);
					if(instFind!=null)
					{
						StockMonthlyExp stockMonthlyExpInsert = new StockMonthlyExp();
						stockMonthlyExpInsert.setItemCode(sme.getItemCode());
						stockMonthlyExpInsert.setYear(sme.getYear());
						stockMonthlyExpInsert.setMonth(sme.getMonth());
						stockMonthlyExpInsert.setInstitution(instFind);
						em.persist(stockMonthlyExpInsert);
						em.flush();
						countInsert++;
					}
				}
			}	
		}
		
		logger.info("Import PHS StockMonthlyExp complete. #0 record(s) inserted.", countInsert);
		
		return true;
	}
	
	public Boolean updateFuncSeqNumYear()
	{
		
		List<String> funcCodeList = new ArrayList<String>();
		funcCodeList.add("FRN");
		funcCodeList.add("DDN");
		
		DateFormat df = new SimpleDateFormat("yyyy");

		for(String funcCode : funcCodeList){
			
			FuncSeqNum funcSeqNum = em.find(FuncSeqNum.class, funcCode);
			Integer updateYear = Integer.parseInt(df.format(new Date()));
			
			if(!updateYear.equals(funcSeqNum.getYear())){
				funcSeqNum.setLastNum(0);
				funcSeqNum.setModifyDate(new Date());
				funcSeqNum.setYear(Integer.parseInt(df.format(new Date())));
				em.merge(funcSeqNum);
				em.flush();
			}else{
				logger.info( funcCode +" funcSeqNumYear is current year, update aborted");
			}
		}
		
		return true;
	}
	
}