package hk.org.ha.model.pms.dqa.persistence.coa;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.coa.ConfirmCheckListFlag;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "COA_BATCH")
@NamedQueries( {
	@NamedQuery(name = "CoaBatch.findByCoaStatus", query = "select o from CoaBatch o where o.coaBatchVer.coaStatus = :coaStatus and o.batchNum is not null order by o.coaBatchId"),	
	@NamedQuery(name = "CoaBatch.findByCoaBatchId", query = "select o from CoaBatch o where o.coaBatchId = :coaBatchId"),
	@NamedQuery(name = "CoaBatch.findByBatchNumCoaItemId", query = "select o from CoaBatch o where o.batchNum=:batchNum and o.coaItem.coaItemId=:coaItemId"),
	@NamedQuery(name = "CoaBatch.findByCoaStatusDiscrepancyStatus", query = "select o from CoaBatch o where o.coaBatchVer.coaStatus = :coaStatus and o.coaBatchVer.discrepancyStatus = :discrepancyStatus and o.batchNum is not null order by o.coaBatchId"),
	@NamedQuery(name = "CoaBatch.findByCoaItemIdBatchNum", query = "select o from CoaBatch o where o.coaItem.coaItemId =:coaItemId and (o.batchNum =:batchNum or (:batchNum is null and o.batchNum is null))"),
	@NamedQuery(name = "CoaBatch.findByInterfaceFlagCoaStatus", query = "select o from CoaBatch o where (o.interfaceFlag is null or o.interfaceFlag = :interfaceFlag ) and o.coaBatchVer.coaStatus = :coaStatus and o.coaBatchVer.recordStatus = hk.org.ha.model.pms.dqa.udt.RecordStatus.Active")
})
@Customizer(AuditCustomizer.class)
public class CoaBatch extends VersionEntity {

	private static final long serialVersionUID = 638226485769791911L;

	@Id
	@Column(name = "COA_BATCH_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaBatchSeq")
	@SequenceGenerator(name = "coaBatchSeq", sequenceName = "SEQ_COA_BATCH", initialValue = 10000)
	private Long coaBatchId;

	@Column(name = "BATCH_NUM", length = 20, nullable = true)
	private String batchNum;

	@Column(name = "DISCREPANCY_REMARK", length = 300, nullable = true)
	private String discrepancyRemark;
	
	@Column(name = "FAIL_REMARK", length = 300, nullable = true)
	private String failRemark;

	@Column(name = "FOLLOW_UP_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date followUpDate;

	@Column(name = "COA_BATCH_VER_COUNT")
	private Integer coaBatchVerCount;

	@Column(name = "DISCREPANCY_ACTION_DATE", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date discrepancyActionDate;
	
	@Converter(name="CoaBatch.passFlag", converterClass=InterfaceFlag.Converter.class)
	@Convert("CoaBatch.passFlag")	
	@Column(name = "INTERFACE_FLAG", length=1, nullable=false)
	private InterfaceFlag interfaceFlag;
	
	@Column(name = "INTERFACE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date interfaceDate;
	
	@Converter(name = "CoaBatch.cfmChecklistFlag", converterClass = ConfirmCheckListFlag.Converter.class)
	@Convert("CoaBatch.cfmChecklistFlag")
	@Column(name="CFM_CHECKLIST_FLAG", length=1, nullable=false)
	private ConfirmCheckListFlag cfmChecklistFlag;
	
	@Transient
	private String combinedDiscrepancy;
	
	@Transient
	private String formattedCreateDate;
	
	@Transient
	private String formattedFollowUpDate;
	
	@ManyToOne
	@JoinColumn(name = "COA_ITEM_ID")
	private CoaItem coaItem;

	@OneToMany(mappedBy = "coaBatch", cascade=CascadeType.REMOVE )
	private List<CoaDiscrepancy> coaDiscrepancyList;

	@OneToMany(mappedBy = "coaBatch" , cascade=CascadeType.REMOVE)
	private List<CoaFileFolder> coaFileFolderList;

	@OneToOne(cascade=CascadeType.PERSIST)	
	@JoinColumn(name = "COA_BATCH_VER_ID")
	private CoaBatchVer coaBatchVer;			

	@OneToMany(mappedBy = "coaBatch", cascade=CascadeType.REMOVE)
    private List<CoaBatchVer> coaBatchVerList;
		
	public CoaBatch() {
		coaBatchVerCount = Integer.valueOf(1);
		cfmChecklistFlag = ConfirmCheckListFlag.NullValue;
	}
	
	public Long getCoaBatchId() {
		return coaBatchId;
	}

	public void setCoaBatchId(Long coaBatchId) {
		this.coaBatchId = coaBatchId;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public void setCoaItem(CoaItem coaItem) {
		this.coaItem = coaItem;
	}

	public CoaItem getCoaItem() {
		return coaItem;
	}

	public void setCoaFileFolderList(List<CoaFileFolder> coaFileFolderList) {
		this.coaFileFolderList = coaFileFolderList;
	}

	public List<CoaFileFolder> getCoaFileFolderList() {
		return coaFileFolderList;
	}

	public void setCoaDiscrepancyList(List<CoaDiscrepancy> coaDiscrepancyList) {
		this.coaDiscrepancyList = coaDiscrepancyList;
	}

	public List<CoaDiscrepancy> getCoaDiscrepancyList() {
		return coaDiscrepancyList;
	}

	public void setCoaBatchVer(CoaBatchVer coaBatchVer) {
		this.coaBatchVer = coaBatchVer;
	}

	public CoaBatchVer getCoaBatchVer() {
		return coaBatchVer;
	}

	public void setCoaBatchVerCount(Integer coaBatchVerCount) {
		this.coaBatchVerCount = coaBatchVerCount;
	}

	public Integer getCoaBatchVerCount() {
		return coaBatchVerCount;
	}
	
	public List<CoaBatchVer> getCoaBatchVerList() {
		return coaBatchVerList;
	}

	public void setFailRemark(String failRemark) {
		this.failRemark = failRemark;
	}

	public String getFailRemark() {
		return failRemark;
	}

	public void setDiscrepancyRemark(String discrepancyRemark) {
		this.discrepancyRemark = discrepancyRemark;
	}

	public String getDiscrepancyRemark() {
		return discrepancyRemark;
	}

	public void setCoaBatchVerList(List<CoaBatchVer> coaBatchVerList) {
		this.coaBatchVerList = coaBatchVerList;
	}

	public void setFollowUpDate(Date followUpDate) {
		if (followUpDate != null) {
			this.followUpDate = new Date(followUpDate.getTime());
		} else {
			this.followUpDate = null;
		}
	}

	public Date getFollowUpDate() {		
		return (followUpDate != null) ? new Date(followUpDate.getTime()) : null;
	}

	public void setCombinedDiscrepancy(String combinedDiscrepancy) {
		this.combinedDiscrepancy = combinedDiscrepancy;
	}

	public String getCombinedDiscrepancy() {
		return combinedDiscrepancy;
	}

	public void setFormattedCreateDate(String formattedCreateDate) {
		this.formattedCreateDate = formattedCreateDate;
	}

	public String getFormattedCreateDate() {
		return formattedCreateDate;
	}

	public void setFormattedFollowUpDate(String formattedFollowUpDate) {
		this.formattedFollowUpDate = formattedFollowUpDate;
	}

	public String getFormattedFollowUpDate() {
		return formattedFollowUpDate;
	}

	public void setDiscrepancyActionDate(Date discrepancyActionDate) {
		if (discrepancyActionDate != null) {
			this.discrepancyActionDate = new Date(discrepancyActionDate.getTime());
		} else {
			this.discrepancyActionDate = null;
		}
	}

	public Date getDiscrepancyActionDate() {
		return (discrepancyActionDate != null) ? new Date(discrepancyActionDate.getTime()) : null;
	}

	public void setInterfaceFlag(InterfaceFlag interfaceFlag) {
		this.interfaceFlag = interfaceFlag;
	}

	public InterfaceFlag getInterfaceFlag() {
		return interfaceFlag;
	}

	public void setInterfaceDate(Date interfaceDate) {
		if( interfaceDate != null ) {
			this.interfaceDate = new Date( interfaceDate.getTime() );
		} else {
			this.interfaceDate = null;
		}
	}

	public Date getInterfaceDate() {		
		return ( interfaceDate != null )? new Date( interfaceDate.getTime() ) :null;
	}

	public void setCfmChecklistFlag(ConfirmCheckListFlag cfmChecklistFlag) {
		this.cfmChecklistFlag = cfmChecklistFlag;
	}

	public ConfirmCheckListFlag getCfmChecklistFlag() {
		return cfmChecklistFlag;
	}
}