package hk.org.ha.model.pms.dqa.batch.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;


public class BatchClient {

	public static final String PROGRAM_NAME = "pms-dqa-ejb-client.jar";
	public static final DateFormat df = new SimpleDateFormat("yyyyMMdd.HHmmss");
	private static String url = null;
	private static String username = null;
	private static String password = null;
	private static String bean = null;

	public static void main(String[] args) throws IOException {

		String jobId = null;
		String params = null;
		String method = "getCoaResult";

		initConnections();

		for (int i = 0; i < args.length; i++)
		{
			if( args[i].equals("-m") ) {        		        		
				method = args[++i];
			} else {
				if (!args[i].startsWith("-")) {
					jobId = args[i];

					if (i+1 < args.length) {
						params = args[i+1];
					}
					break;
				}
			}
		}

		if (jobId == null || jobId.trim().length() == 0) {
			System.out.println("Usage: java -jar " + PROGRAM_NAME + " [-url <provider_url>] [-u <username>] [-p <password>] [-b bean] [-m <method>] JobId [Parameters]");
			System.exit(-1);                
		}

		Boolean isSuccess = Boolean.FALSE;

		try {
			printLog(jobId, "start");

			printLog(jobId, "Provider URL: " + url);
			printLog(jobId, "Session Bean: " + bean);
			printLog(jobId, "Method: " + method);
			printLog(jobId, "Parameters: " + params);

			Hashtable<Object, String> env = new Hashtable<Object, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, 
					"weblogic.jndi.WLInitialContextFactory");                
			env.put(Context.PROVIDER_URL, url);

			if (!isEmpty(username) && !isEmpty(password)) {
				env.put(Context.SECURITY_PRINCIPAL, username);
				env.put(Context.SECURITY_CREDENTIALS, password);
			}
			
			Context context = new InitialContext(env);

			Object o = context.lookup(bean); 

			if (params == null) {
				Method m = o.getClass().getDeclaredMethod(method, String.class);
				isSuccess = (Boolean) m.invoke(o, jobId);
			} else {
				Method m = o.getClass().getDeclaredMethod(method, String.class, String.class);
				isSuccess = (Boolean) m.invoke(o, jobId, params);
			}
		} catch (Exception e) {
			isSuccess = false;			
			e.printStackTrace(System.err);
		} finally {
			printLog(jobId, "end: " + (isSuccess ? "success" : "failure"));

			if (isSuccess) {
				System.exit(0);
			} else { 
				System.exit(1);
			}
		}

	}

	public static void initConnections() {
		Properties sysProps = System.getProperties();
		String dirConnectionPropsFile = sysProps.getProperty("config");
		FileInputStream fis=null;
		Properties profile = new Properties();
		try {
			fis = new FileInputStream(dirConnectionPropsFile);
			profile.load( fis );
			url = profile.getProperty( "dest.url" );
			username = profile.getProperty( "dest.username" );
			password = profile.getProperty( "dest.password" );
			bean = profile.getProperty( "dest.bean" );
		} catch (IOException e) {
			e.printStackTrace(System.err);
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	public static void printLog(String jobId, String message) {
		System.out.println(df.format(new Date()) + ": " + PROGRAM_NAME + ": " + jobId + ": " + message);
	}

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
