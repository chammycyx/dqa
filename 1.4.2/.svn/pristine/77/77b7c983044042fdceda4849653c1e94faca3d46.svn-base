<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:mdcs="com.iwobanas.controls.*"
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("natureOfProblemMaintView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.fmk.pms.flex.components.message.*;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			
			import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureParamEvent;
			import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureCatEvent;	
			import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureSubCatEvent;
			import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureParamEvent;
			import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureCatEvent;
			import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureSubCatEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RefreshNatureOfProblemParameterViewEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RefreshNatureOfProblemCategoryViewEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RefreshNatureOfProblemSubCategoryViewEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureParamByProblemNatureParamIdEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureCatByProblemNatureCatIdEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureParamListEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureCatListEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureSubCatListEvent;
			import hk.org.ha.event.pms.dqa.suppperf.popup.ShowNatureOfProblemParameterPopupEvent;
			import hk.org.ha.event.pms.dqa.suppperf.popup.ShowNatureOfProblemCategoryPopupEvent;
			import hk.org.ha.event.pms.dqa.suppperf.popup.ShowNatureOfProblemSubCategoryPopupEvent;
			
			import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
			import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
			import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
			
			import mx.collections.ArrayCollection;
			import mx.collections.ICollectionView;
			import mx.collections.IViewCursor;
			import mx.collections.ListCollectionView;
			import mx.controls.Alert;
			import mx.controls.ToolTip;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.FlexEvent;
			import mx.events.ItemClickEvent;
			import mx.events.ListEvent;
			import mx.events.CalendarLayoutChangeEvent;
			import mx.events.ValidationResultEvent;
			import mx.managers.PopUpManager;
			
			[In][Bindable]
			public var problemNatureParamList:ArrayCollection;
			
			[In][Bindable]
			public var problemNatureCatList:ArrayCollection;
			
			[In][Bindable]
			public var problemNatureSubCatList:ArrayCollection;
			
			[Bindable]
			protected var pParameterContextMenu:ContextMenu = new ContextMenu();
			
			[Bindable]
			protected var pCategoryContextMenu:ContextMenu = new ContextMenu();
			
			[Bindable]
			protected var pSubCategoryContextMenu:ContextMenu = new ContextMenu();
			
			private var msgProp:SystemMessagePopupProp;
			private var screenName:String="natureOfProblemMaintView";
			public var problemNatureParamSelect:ProblemNatureParam;
			public var problemNatureCatSelect:ProblemNatureCat;
			
			
			
			public override function onShow():void{
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.clearFunc  = clear;
					pmsToolbar.init(); //create the toolbar
				}
				refreshParameterPanel();
			}
			
			protected function addPParameterContextMenu():void{
				var resetParameterMenuItem:ContextMenuItem   = new ContextMenuItem("Reset Filter");
				var addParameterMenuItem:ContextMenuItem   = new ContextMenuItem("Add Parameter");
				var editParameterMenuItem:ContextMenuItem   = new ContextMenuItem("Edit Parameter");
				var deleteParameterMenuItem:ContextMenuItem   = new ContextMenuItem("Delete Parameter");
				
				resetParameterMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    resetParameterMenuClick);
				addParameterMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    addParameterMenuClick);
				editParameterMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    editParameterMenuClick);
				deleteParameterMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    deleteParameterMenuClick);
				
				pParameterContextMenu.hideBuiltInItems();
				pParameterContextMenu.customItems = [resetParameterMenuItem, addParameterMenuItem, editParameterMenuItem, deleteParameterMenuItem];
			}
			
			protected function addPCategoryContextMenu():void{
				var resetCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Reset Filter");
				var addCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Add Category");
				var editCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Edit Category");
				var deleteCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Delete Category");
				
				resetCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    resetCategoryMenuClick);
				addCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    addCategoryMenuClick);
				editCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    editCategoryMenuClick);
				deleteCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    deleteCategoryMenuClick);
				
				pCategoryContextMenu.hideBuiltInItems();
				pCategoryContextMenu.customItems = [resetCategoryMenuItem, addCategoryMenuItem, editCategoryMenuItem, deleteCategoryMenuItem];
			}
			
			protected function addPSubCategoryContextMenu():void{
				var resetSubCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Reset Filter");
				var addSubCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Add Sub-Category");
				var editSubCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Edit Sub-Category");
				var deleteSubCategoryMenuItem:ContextMenuItem   = new ContextMenuItem("Delete Sub-Category");
				
				resetSubCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    resetSubCategoryMenuClick);
				addSubCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    addSubCategoryMenuClick);
				editSubCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    editSubCategoryMenuClick);
				deleteSubCategoryMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,    deleteSubCategoryMenuClick);
				
				pSubCategoryContextMenu.hideBuiltInItems();
				pSubCategoryContextMenu.customItems = [resetSubCategoryMenuItem, addSubCategoryMenuItem, editSubCategoryMenuItem, deleteSubCategoryMenuItem];
			}
			
			protected function resetParameterMenuClick(evt:ContextMenuEvent):void{
				resetGpParameterFilter();
			}
			protected function addParameterMenuClick(evt:ContextMenuEvent):void{
				var event:Event = new ShowNatureOfProblemParameterPopupEvent("Add");
				dispatchEvent(new AddProblemNatureParamEvent(event));
			}
			protected function editParameterMenuClick(evt:ContextMenuEvent):void{
				if (gpParameter.selectedItem != null){
					var event:Event = new ShowNatureOfProblemParameterPopupEvent("Edit");
					dispatchEvent(new RetrieveProblemNatureParamByProblemNatureParamIdEvent(gpParameter.selectedItem as ProblemNatureParam, event));
				}
				else
				{
					showMessage("0003");
				}
			}
			protected function deleteParameterMenuClick(evt:ContextMenuEvent):void{
				if (gpParameter.selectedItem != null){
					showMessage("0084" , confirmDeleteParameter);
				}else {
					showMessage("0003");
				}		
			}
			
			protected function confirmDeleteParameter(evt:Event):void{
				var event:Event = new RefreshNatureOfProblemParameterViewEvent();
				dispatchEvent (new DeleteProblemNatureParamEvent(gpParameter.selectedItem as ProblemNatureParam , event  ));
				
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);					
			}
			
			protected function resetCategoryMenuClick(evt:ContextMenuEvent):void{
				resetGpCategoryFilter();
			}
			protected function addCategoryMenuClick(evt:ContextMenuEvent):void{
				if(txtCatParamDisplayOrder.text!="" && txtCatParamDesc.text!="" && problemNatureParamSelect!=null)
				{
					var event:Event = new ShowNatureOfProblemCategoryPopupEvent("Add", problemNatureParamSelect);
					dispatchEvent(new AddProblemNatureCatEvent(event));
				}
			}
			protected function editCategoryMenuClick(evt:ContextMenuEvent):void{
				if (gpCategory.selectedItem != null){
					var event:Event = new ShowNatureOfProblemCategoryPopupEvent("Edit", problemNatureParamSelect);
					dispatchEvent(new RetrieveProblemNatureCatByProblemNatureCatIdEvent(gpCategory.selectedItem as ProblemNatureCat, event));
				}
				else
				{
					showMessage("0003");
				}
			}
			protected function deleteCategoryMenuClick(evt:ContextMenuEvent):void{
				if (gpCategory.selectedItem != null){
					showMessage("0085" , confirmDeleteCategory);
				}else {
					showMessage("0003");
				}
			}
			
			protected function confirmDeleteCategory(evt:Event):void{
				var event:Event = new RefreshNatureOfProblemCategoryViewEvent();
				dispatchEvent (new DeleteProblemNatureCatEvent(gpCategory.selectedItem as ProblemNatureCat , event  ));
				
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);					
			}
			
			protected function resetSubCategoryMenuClick(evt:ContextMenuEvent):void{
				resetGpSubCategoryFilter();
			}
			protected function addSubCategoryMenuClick(evt:ContextMenuEvent):void{
				if(txtSubCatParamDisplayOrder.text!=""  && txtSubCatParamDesc.text!=""  &&
					txtSubCatCatDisplayOrder.text!=""  && txtSubCatCatDesc.text!="")
				{
					var event:Event = new ShowNatureOfProblemSubCategoryPopupEvent("Add", problemNatureCatSelect);
					dispatchEvent(new AddProblemNatureSubCatEvent(event));
				}
			}
			protected function editSubCategoryMenuClick(evt:ContextMenuEvent):void{
				if (gpSubCategory.selectedItem != null){
					var event:Event = new ShowNatureOfProblemSubCategoryPopupEvent("Edit", problemNatureCatSelect);
					dispatchEvent(new RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent(gpSubCategory.selectedItem as ProblemNatureSubCat, event));
				}
				else
				{
					showMessage("0003");
				}
			}
			protected function deleteSubCategoryMenuClick(evt:ContextMenuEvent):void{
				if (gpSubCategory.selectedItem != null){
					showMessage("0006" , confirmDeleteSubCategory);
				}else {
					showMessage("0003");
				}
			}
			
			protected function confirmDeleteSubCategory(evt:Event):void{
				var event:Event = new RefreshNatureOfProblemSubCategoryViewEvent();
				dispatchEvent (new DeleteProblemNatureSubCatEvent(gpSubCategory.selectedItem as ProblemNatureSubCat , event  ));
				
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);					
			}
			
			private function clear():void{	
				refreshParameterPanel();
			}
			
			public function refreshParameterPanel():void{
				dispatchEvent( new RetrieveProblemNatureParamListEvent());
				resetGpParameterFilter();
				resetGpCategoryFilter();
				resetGpSubCategoryFilter();
				resetForm(pCategory);
				resetForm(pSubCategory);
				resetGpCategory();
				resetGpSubCategory();
				problemNatureParamSelect=null;
				problemNatureCatSelect=null;
			}
			
			public function refreshCategoryPanel():void{
				if (problemNatureParamSelect!=null)
				{
					dispatchEvent( new RetrieveProblemNatureCatListEvent("natureOfProblemMaintView", problemNatureParamSelect));	
				}
				resetGpCategoryFilter();
				resetGpSubCategoryFilter();
				resetForm(pSubCategory);
				resetGpSubCategory();
				problemNatureCatSelect=null;
			}
			
			public function refreshSubCategoryPanel():void{
				if (problemNatureCatSelect!=null)
				{
					dispatchEvent( new RetrieveProblemNatureSubCatListEvent("natureOfProblemMaintView", problemNatureCatSelect));	
				}
				resetGpSubCategoryFilter();
			}
			
			public function resetGpParameterFilter():void{
				gpParameter.columns.every(function(object:Object, index:int, array:Array):Boolean {
					if (object is MDataGridColumn) {
						(object as MDataGridColumn).filter = null;
					}
					return true;
				});
			}
			
			public function resetGpCategoryFilter():void{
				gpCategory.columns.every(function(object:Object, index:int, array:Array):Boolean {
					if (object is MDataGridColumn) {
						(object as MDataGridColumn).filter = null;
					}
					return true;
				});
			}
			
			public function resetGpSubCategoryFilter():void{
				gpSubCategory.columns.every(function(object:Object, index:int, array:Array):Boolean {
					if (object is MDataGridColumn) {
						(object as MDataGridColumn).filter = null;
					}
					return true;
				});
			}
			
			public function resetGpCategory():void{
				ctx.problemNatureCatList = null;
				gpCategory.dataProvider=problemNatureCatList;
				(gpCategory.dataProvider as ArrayCollection).sort = null;
				(gpCategory.dataProvider as ArrayCollection).refresh();
			}
			
			public function resetGpSubCategory():void{
				ctx.problemNatureSubCatList = null;
				gpSubCategory.dataProvider=problemNatureSubCatList;
				(gpSubCategory.dataProvider as ArrayCollection).sort = null;
				(gpSubCategory.dataProvider as ArrayCollection).refresh();
			}
			
			protected function gpParameterDoubleClickHandler(evt:ListEvent):void
			{
				if(gpParameter.selectedItem !=null){
					var eventRefreshProblemNatureCat:Event = new RefreshNatureOfProblemCategoryViewEvent();
					dispatchEvent( new RetrieveProblemNatureCatListEvent( screenName, gpParameter.selectedItem as ProblemNatureParam, eventRefreshProblemNatureCat));
				}
				else {
					showMessage("0008");
				}
			}
			
			protected function gpCategoryDoubleClickHandler(evt:ListEvent):void
			{
				if(gpCategory.selectedItem !=null){
					var eventRefreshProblemNatureSubCat:Event = new RefreshNatureOfProblemSubCategoryViewEvent();
					dispatchEvent( new RetrieveProblemNatureSubCatListEvent( screenName, gpCategory.selectedItem as ProblemNatureCat, eventRefreshProblemNatureSubCat));
				}
				else {
					showMessage("0008");
				}
			}
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/> 
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>
	
	<s:VGroup width="100%" height="100%" gap="0">
		<s:HGroup verticalAlign="middle" width="100%" height="20%">
			<s:Panel id="pParameter" title="Parameter" width="100%" height="100%" contextMenu="{pParameterContextMenu}"  creationComplete="addPParameterContextMenu()" mouseEnabled="true">
				<s:HGroup verticalAlign="middle" width="100%" height="100%" mouseEnabled="true" >
					<mdcs:MDataGrid id="gpParameter" width="100%" height="100%" copyDataProvider="false" verticalScrollPolicy="on" mouseEnabled="true"
									dataProvider="{problemNatureParamList}" doubleClickEnabled="true" itemDoubleClick="gpParameterDoubleClickHandler(event)">
						<mdcs:columns>
							<mx:DataGridColumn headerText="Display Order" minWidth="100" width="0.2" dataField="displayOrder" draggable="false" sortable="false"/>
							<mdgc:MDataGridColumn headerText="Parameter" minWidth="200" width="0.8" dataField="paramDesc"  draggable="false" sortable="false"/>
						</mdcs:columns>
					</mdcs:MDataGrid>
				</s:HGroup>
			</s:Panel>
		</s:HGroup>
		<s:HGroup verticalAlign="middle" width="100%" height="40%">
			<s:Panel id="pCategory" title="Category" width="100%" height="100%" contextMenu="{pCategoryContextMenu}"  creationComplete="addPCategoryContextMenu()" mouseEnabled="true">
				<s:VGroup width="100%" height="100%" gap="0">
					<s:HGroup verticalAlign="middle" width="100%" height="15%">
						<mx:Spacer width="5"/>
						<s:Label text="Parameter" width="70"/> 
						<s:TextInput id="txtCatParamDisplayOrder" width="30"  enabled="false"/>
						<s:TextInput id="txtCatParamDesc" width="340" enabled="false"/>
					</s:HGroup>
					<s:HGroup verticalAlign="middle" width="100%" height="85%" mouseEnabled="true" >
						<mdcs:MDataGrid id="gpCategory" width="100%" height="100%" copyDataProvider="false" verticalScrollPolicy="on" mouseEnabled="true"
										dataProvider="{problemNatureCatList}" doubleClickEnabled="true" itemDoubleClick="gpCategoryDoubleClickHandler(event)">
							<mdcs:columns>
								<mx:DataGridColumn headerText="Display Order" minWidth="100" width="0.2" dataField="displayOrder" draggable="false" sortable="false"/>
								<mdgc:MDataGridColumn headerText="Category" minWidth="200" width="0.8" dataField="catDesc" draggable="false" sortable="false"/>
							</mdcs:columns>
						</mdcs:MDataGrid>
					</s:HGroup>
				</s:VGroup>
			</s:Panel>
		</s:HGroup>
		<s:HGroup verticalAlign="middle" width="100%" height="40%">
			<s:Panel id="pSubCategory" title="Sub - Category" width="100%" height="100%" contextMenu="{pSubCategoryContextMenu}"  creationComplete="addPSubCategoryContextMenu()" mouseEnabled="true">
				<s:VGroup width="100%" height="100%" gap="0">
					<s:HGroup verticalAlign="middle" width="100%" height="15%">
						<mx:Spacer width="5"/>
						<s:Label text="Parameter" width="70"/> 
						<s:TextInput id="txtSubCatParamDisplayOrder" width="30"  enabled="false"/>
						<s:TextInput id="txtSubCatParamDesc" width="340" enabled="false"/>
						<mx:Spacer width="50"/>
						<s:Label text="Category" width="70"/> 
						<s:TextInput id="txtSubCatCatDisplayOrder" width="30"  enabled="false"/>
						<s:TextInput id="txtSubCatCatDesc" width="340" enabled="false"/>
					</s:HGroup>
					<s:HGroup verticalAlign="middle" width="100%" height="85%" mouseEnabled="true" >
						<mdcs:MDataGrid id="gpSubCategory" width="100%" height="100%" copyDataProvider="false" verticalScrollPolicy="on" mouseEnabled="true"
										dataProvider="{problemNatureSubCatList}" doubleClickEnabled="true" >
							<mdcs:columns>
								<mx:DataGridColumn headerText="Display Order" minWidth="100" width="0.2" dataField="displayOrder" draggable="false" sortable="false"/>
								<mdgc:MDataGridColumn headerText="Sub-Category" minWidth="200" width="0.8" dataField="subCatDesc" draggable="false" sortable="false"/>
							</mdcs:columns>
						</mdcs:MDataGrid>
					</s:HGroup>
				</s:VGroup>
			</s:Panel>
		</s:HGroup>
	</s:VGroup>
</fc:ExtendedNavigatorContent>
