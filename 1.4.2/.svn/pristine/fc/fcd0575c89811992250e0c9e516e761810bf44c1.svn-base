package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SAMPLE_TEST_SCHEDULE_FILE")
@NamedQueries({
	@NamedQuery(name = "SampleTestScheduleFile.findBySampleTestFileId", query ="select o from SampleTestScheduleFile o where " +
			"o.sampleTestFile.sampleTestFileId = :sampleTestFileId"),
	@NamedQuery(name = "SampleTestScheduleFile.findByScheduleId", query ="select o from SampleTestScheduleFile o where " +
			"o.sampleTestSchedule.scheduleId = :scheduleId"),
	@NamedQuery(name = "SampleTestScheduleFile.findByScheduleIdFileCat", query ="select o from SampleTestScheduleFile o where " +
			"o.sampleTestSchedule.scheduleId = :scheduleId and o.sampleTestFile.fileCat = :fileCat"),
	@NamedQuery(name = "SampleTestScheduleFile.findByRecordStatusScheduleStatusFileCat", query ="select o from SampleTestScheduleFile o where " +
			"o.sampleTestSchedule.recordStatus = :recordStatus and o.sampleTestSchedule.scheduleStatus = :scheduleStatus and o.sampleTestFile.fileCat = :fileCat"),
	@NamedQuery(name = "SampleTestScheduleFile.findByScheduleIdFileCatMoaFpsVr", query ="select o from SampleTestScheduleFile o where " +
			"o.sampleTestSchedule.scheduleId = :scheduleId and o.sampleTestFile.fileCat in :fileCatList")
	})
	
@Customizer(AuditCustomizer.class)	
public class SampleTestScheduleFile extends VersionEntity{

	private static final long serialVersionUID = -7795494071250495522L;

	
	@Id
	@Column(name="SAMPLE_TEST_SCHEDULE_FILE_ID",nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleTestScheduleFileSeq")
	@SequenceGenerator(name="sampleTestScheduleFileSeq", sequenceName="SEQ_SAMPLE_TEST_SCHED_FILE", initialValue=10000)	
	private Long sampleTestScheduleFileId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "SCHEDULE_ID", nullable = true)
	private SampleTestSchedule sampleTestSchedule;
	
	@ManyToOne
	@JoinColumn(name="SAMPLE_TEST_FILE_ID", nullable = true)
	private SampleTestFile sampleTestFile;
	
	public Long getSampleTestScheduleFileId() {
		return sampleTestScheduleFileId;
	}

	public void setSampleTestScheduleFileId(Long sampleTestScheduleFileId) {
		this.sampleTestScheduleFileId = sampleTestScheduleFileId;
	}

	public SampleTestSchedule getSampleTestSchedule() {
		return sampleTestSchedule;
	}

	public void setSampleTestSchedule(SampleTestSchedule sampleTestSchedule) {
		this.sampleTestSchedule = sampleTestSchedule;
	}

	public void setSampleTestFile(SampleTestFile sampleTestFile) {
		this.sampleTestFile = sampleTestFile;
	}

	public SampleTestFile getSampleTestFile() {
		return sampleTestFile;
	}
	
}
