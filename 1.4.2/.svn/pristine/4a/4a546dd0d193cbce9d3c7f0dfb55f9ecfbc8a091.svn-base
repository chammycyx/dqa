package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Locale;


import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.vo.sample.DdrRpt;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import hk.org.ha.model.pms.dqa.vo.sample.DdrRptCriteria;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.NatureOfTrx;


@Stateful
@Scope(ScopeType.SESSION)
@Name("ddrRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DdrRptServiceBean implements DdrRptServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String UNCHECKED = "unchecked";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy";
	
	private List<DdrRpt> ddrRptList;
	
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
    
	private JRDataSource dataSource;
	 
	public void retrieveDdrList(DdrRptCriteria ddrRptCriteria) {
		
		ddrRptList = new ArrayList<DdrRpt>();
		
		List<SampleMovement> sampleMovementList =  getDdrListByCriteria(ddrRptCriteria);
		
    	for (SampleMovement sampleMovement : sampleMovementList ) {
    		// check dangerous drug
    		//if ( sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getDangerousDrug().equalsIgnoreCase("Y") ) {
    		if(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null)
    		{
    			logger.debug("Item Code " + sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode() + " not found in DMS" );
    		}
    		else if ( ddrRptCriteria.getDangerousDrug().equalsIgnoreCase("N") ||
    				( ddrRptCriteria.getDangerousDrug().equalsIgnoreCase("Y") &&
    					sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getDangerousDrug().equalsIgnoreCase("Y") )
    			) {

	    		DdrRpt tmpDdrRpt = constructDdrRptData(sampleMovement);
				
	    		ddrRptList.add(tmpDdrRpt);
    		}
    	}
    	
    	List<DdrRpt> list = addRowPaddingforEachGroup(ddrRptList);
    	
    	dataSource = new JRBeanCollectionDataSource(list);
	}
	
	public String getLocationDesc(String locationCode) {
		String locationDesc = null;
		
		if (locationCode != null) {
			List<Lab> labResultList = em.createNamedQuery("Lab.findByLabCode")
									.setParameter("labCode", locationCode)
									.getResultList();
			
			if ( labResultList!=null && labResultList.size() > 0 ) {
				locationDesc = labResultList.get(0).getLabName();
			}
			else
			{
				List<Institution> institutionResultList = em.createNamedQuery("Institution.findByInstitutionCode")
											.setParameter("institutionCode", locationCode)
											.getResultList();
			
				if ( institutionResultList!=null && institutionResultList.size() > 0 ) {
					locationDesc = institutionResultList.get(0).getInstitutionName();
				}
			}
		}
		
		return locationDesc;
	}
	
	
	public String getManufDesc(String manufCode){
		String manufDesc = null;
		if(!StringUtils.isEmpty(manufCode)){
			List<Company> companyList = em.createNamedQuery("Company.findLikeManufacturerCode")
					.setParameter("manufacturerCode", manufCode)
					.setParameter("manufFlag",YesNoFlag.Yes)
					.getResultList();
			
			if(companyList !=null && companyList.size() > 0){
				manufDesc = companyList.get(0).getCompanyName();
			}
		}
		
		return manufDesc;
	}

	
	private DdrRpt constructDdrRptData(SampleMovement sampleMovement) {
		SimpleDateFormat sdf;
		String modifyDate;
		String natureOfTrx = null;
		DdrRpt ddrRpt = new DdrRpt();
		StringBuffer sbNameOfPoisions;
		String supplierName;
		String manufName;
		
		
		
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.ENGLISH);
		modifyDate = sdf.format( sampleMovement.getModifyDate() );
		
		if ( sampleMovement.getAdjustFlag() == null ) {
			natureOfTrx = sampleMovement.getMovementQty()>= 0 ? 
					NatureOfTrx.H.getDisplayValue() : NatureOfTrx.L.getDisplayValue();
		} else if ( "Y".equals(sampleMovement.getAdjustFlag()) ) {
			natureOfTrx = sampleMovement.getAdjustReason();
		}

		sbNameOfPoisions = new StringBuffer();
		sbNameOfPoisions = sbNameOfPoisions.append(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getFullDrugDesc())
							.append(" (")
							.append(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode())
							.append(")");
		supplierName = getLocationDesc(sampleMovement.getLocationCode());
		manufName = getManufDesc(sampleMovement.getManufCode());
		
		
		ddrRpt.setFullDrugDesc(sbNameOfPoisions.toString());
		ddrRpt.setBaseUnit(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getBaseUnit());
		ddrRpt.setManufName(manufName);
		
		ddrRpt.setSupplierName(supplierName);
		ddrRpt.setInvoiceNo(sampleMovement.getSampleTestSchedule().getScheduleNum());
		ddrRpt.setMovementQty(sampleMovement.getMovementQty());
		ddrRpt.setOnHandQty(sampleMovement.getOnHandQty());
		ddrRpt.setModifyDate( modifyDate );
		ddrRpt.setNatureOfTrx( natureOfTrx );
		ddrRpt.setBatchNo(sampleMovement.getSampleTestSchedule().getBatchNum());
		ddrRpt.setPackSize(sampleMovement.getSampleTestSchedule().getContract().getPackSize());
		
		return ddrRpt;
	}
	
	private List<DdrRpt> addRowPaddingforEachGroup(List<DdrRpt> ddrRptList) {
		
		int maxRowSize = 6;
		int numGrpRecord = 0;
		int paddingSize = 0;
		
		List<DdrRpt> ddrRptListwithPadding = new ArrayList<DdrRpt>();
		
		for (int i=0; i < ddrRptList.size(); i++) {
			numGrpRecord++;
			
			ddrRptListwithPadding.add(ddrRptList.get(i));
			
			if ( i + 1 == ddrRptList.size() && (numGrpRecord % maxRowSize == 0) ) {
				break;
			}
			
			if ( i + 1 == ddrRptList.size() || 
					(!ddrRptList.get(i).getFullDrugDesc().equals(ddrRptList.get(i+1).getFullDrugDesc()) && 
							numGrpRecord % maxRowSize != 0)
			) 
			{
				paddingSize = maxRowSize - ((numGrpRecord) % maxRowSize);
				
				for (int j=1; j<=paddingSize; j++) {
					DdrRpt emptyDdrRpt = new DdrRpt();
					
					emptyDdrRpt.setFullDrugDesc(ddrRptList.get(i).getFullDrugDesc());
					emptyDdrRpt.setBaseUnit(ddrRptList.get(i).getBaseUnit());
					
					ddrRptListwithPadding.add(emptyDdrRpt);
				}
				
				// Reset
				numGrpRecord = 0;
			}
		}
		
		if ( ddrRptList.size() == 0 ) {
			for (int j=1; j<=maxRowSize; j++) {
				DdrRpt emptyDdrRpt = new DdrRpt();
				
				ddrRptListwithPadding.add(emptyDdrRpt);
			}
		}
		
		return ddrRptListwithPadding;
	}
	 
	@SuppressWarnings(UNCHECKED)
	public List<SampleMovement> getDdrListByCriteria(DdrRptCriteria ddrRptCriteria) {
		return em.createNamedQuery("SampleMovement.findByModifyDate")
				.setParameter("startDate", ddrRptCriteria.getStartDate())
				.setParameter("endDate", ddrRptCriteria.getEndDate())
				.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
				.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		.getResultList();      		
	}  
	
	public void generateDdrRpt() {
		//passing relevant information to the jasper report	
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/DdrRpt.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
    
    @Remove
	public void destroy(){
    	ddrRptList = null;
	}

}
