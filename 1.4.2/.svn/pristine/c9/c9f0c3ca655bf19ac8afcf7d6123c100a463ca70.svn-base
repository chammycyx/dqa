package hk.org.ha.model.pms.dqa.udt.suppperf;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum FaxType implements StringValuedEnum {
	
	IH("IH", "Initial Correspondence to Hospital"),
	MH("MH", "Interim Correspondence to Hospital"),
	FH("FH", "Final Correspondence to Hospital"),
	IS("IS", "Initial Correspondence to Supplier"),
	MS("MS", "Interim Correspondence to Supplier"),
	FS("FS", "Final Correspondence to Supplier");	
	
    private final String dataValue;
    private final String displayValue;
        
    FaxType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<FaxType> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<FaxType> getEnumClass() {
    		return FaxType.class;
    	}
    }
}
