<?xml version="1.0" encoding="utf-8"?>

<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009"
	xmlns:s="library://ns.adobe.com/flex/spark"
	xmlns:mx="library://ns.adobe.com/flex/mx" 
	xmlns:mdcs="com.iwobanas.controls.*"
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("coaVerificationView")]
	</fx:Metadata>
	
	<fx:Script>
		<![CDATA[
			import com.iwobanas.controls.dataGridClasses.filterEditors.DropDownFilterEditor;
			
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchByCoaBatchEvent;
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchByCoaBatchIdEvent;
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListByCoaStatusEvent;
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListWithoutBatchOthersEvent;
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaVerificationDataListEvent;
			import hk.org.ha.event.pms.dqa.coa.popup.ShowOpenCoaPopupEvent;
			import hk.org.ha.event.pms.dqa.coa.popup.ShowVerifyCoaPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dms.persistence.DmDrug;
			import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
			import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
			import hk.org.ha.model.pms.dqa.udt.OrderType;
			import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
			import hk.org.ha.model.pms.dqa.vo.coa.CoaVerificationData;
			
			import mx.collections.ArrayCollection;
			import mx.collections.ListCollectionView;
			import mx.events.FlexEvent;
			import mx.events.ItemClickEvent;
			import mx.events.ListEvent;						
			
			private var coaBatch:CoaBatch;
			
			public static const REFRESH:String = "Refresh";
			
			[Bindable]
			private var coaVerificationDataList:ListCollectionView;
			
			[Bindable]
			protected var gridContextMenu:ContextMenu = new ContextMenu();
			
			private var coaVerificationData:CoaVerificationData;
			
			private var selectedCoaVerificationData:*;
			
			public function removeCoaVerificationData(coaBatchId:Number):void {
				if (coaVerificationDataList == null) {
					return;
				}
				var removeIndex:int = 0;
				for each (var verificationData:CoaVerificationData in coaVerificationDataList) {
					if (verificationData.coaBatchId == coaBatchId) {
						coaVerificationDataList.removeItemAt(removeIndex);
						return;
					} 
					++removeIndex;
				}
				coaVerificationDataList.refresh();
			}
			
			protected function addGridContextMenu():void
			{
				var openCoaMenuItem:ContextMenuItem = new ContextMenuItem("Open COA");
				var verifyMenuItem:ContextMenuItem = new ContextMenuItem("Verify");
				var pageRefreshMenuItem:ContextMenuItem = new ContextMenuItem(REFRESH,true);
				
				pageRefreshMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, pageRefreshClick);
				openCoaMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, openCoaClick);
				verifyMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, verifyClick);								
				
				gridContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, updateContextMenu);
				gridContextMenu.hideBuiltInItems();
				gridContextMenu.customItems = [openCoaMenuItem, verifyMenuItem, pageRefreshMenuItem];
			}

			protected function pageRefreshClick(evt:ContextMenuEvent):void
			{						
				retrieve();
			}
			
			protected function openCoaClick(evt:ContextMenuEvent):void
			{						
				var verificationData:CoaVerificationData = selectedCoaVerificationData;
				dispatchEvent(
					new RetrieveCoaFileFolderListWithoutBatchOthersEvent( 
						verificationData.coaBatchId,
						new RetrieveCoaBatchByCoaBatchIdEvent(
							verificationData.coaBatchId, 
							CoaStatus.VerificationInProgress,
							null, 
							function():void { 
								dispatchEvent(new ShowOpenCoaPopupEvent());
							} 
						)
					)
				);				
			}
			
			protected function verifyClick(evt:ContextMenuEvent):void
			{
				var verificationData:CoaVerificationData = selectedCoaVerificationData;
				dispatchEvent(new RetrieveCoaBatchByCoaBatchIdEvent(verificationData.coaBatchId,CoaStatus.VerificationInProgress, null, function():void {
					dispatchEvent(new ShowVerifyCoaPopupEvent());
				}));
			}
			
			protected function updateContextMenu(evt:ContextMenuEvent):void
			{
				var cm:ContextMenu = evt.target as ContextMenu;	
				var isSelected:Boolean = selectedCoaVerificationData != null;
				
				for(var i:int = 0; i<cm.customItems.length; i++) {
					var cmi:ContextMenuItem = cm.customItems[i] as ContextMenuItem;
					if(cmi.caption == REFRESH) { continue; }
					cmi.enabled = isSelected;					
				}				
			}

			public override function onShow():void 
			{				
				retrieve();
			}
			
			public override function onHide():void
			{	
				coaVerificationGrid.dataProvider = null;
			}	
			
			public function retrieve():void
			{
				verificationGroup.enabled = false;
//				dispatchEvent( new RetrieveCoaBatchListByCoaStatusEvent(CoaStatus.VerificationInProgress, afterRetrieve) );
				dispatchEvent(new RetrieveCoaVerificationDataListEvent(afterRetrieve));
			}
			
			private function afterRetrieve(list:ListCollectionView):void {
				coaVerificationDataList = list;
				refresh();
			}
			
			public function refresh():void
			{						
				verificationGroup.enabled = true;
				
				coaVerificationGrid.columns.every(function(object:Object, index:int, array:Array):Boolean {
					if (object is MDataGridColumn) {
						(object as MDataGridColumn).filter = null;
					}
					return true;
				});	
				(coaVerificationGrid.dataProvider as ArrayCollection).sort =null;
				(coaVerificationGrid.dataProvider as ArrayCollection).refresh();		
			}
						
			protected function initGrid():void
			{							
				addGridContextMenu();
			}
			
			private function followUpDateLabelFunc(item:Object, column:DataGridColumn):String {
				return dateFormatter.format(item.createDate);
			}
			

			protected function itemClickHandler(event:ItemClickEvent):void
			{				
				onShow();
			}
			
			private function contractNumLabelFunc(item:Object, column:DataGridColumn):String {
				var finalContractNum:String = "";
				if ( item.contractNum != null ) {
					finalContractNum=item.contractNum;
				}
				
				if ( item.contractSuffix == null ) {
					return finalContractNum;	
				} else {
					return finalContractNum+"/"+item.contractSuffix;
				}
			}
			
			private function batchNumLabelFunc(item:Object, column:DataGridColumn):String {
				if (item.batchNum ==null){
					return "";
				}else{
					return item.batchNum;
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:states>
		<s:State name="default" />			
		<s:State name="noRecord" basedOn="default"/>		
	</fc:states>	
	
	<s:VGroup id="verificationGroup" width="100%" height="100%" top="10" left="10" right="10" bottom="10">			
		<mdcs:MDataGrid id="coaVerificationGrid" width="100%" height="100%" 
						doubleClickEnabled="true" contextMenu="{gridContextMenu}"
					    selectedItem="@{selectedCoaVerificationData}" copyDataProvider="false"
						dataProvider="{coaVerificationDataList}" creationComplete="initGrid()"
						verticalScrollPolicy="on">
			<mdcs:columns >
				<mdgc:MDataGridColumn width="0.125" dataField="itemCode" headerText="Item Code" draggable="false" dataTipField="fullDrugDesc" showDataTips="true"/>
				<mdgc:MDataGridColumn width="0.125" dataField="supplierCode" headerText="Supplier Code" draggable="false" dataTipField="supplierName" showDataTips="true"/>
				<mdgc:MDataGridColumn width="0.091" dataField="orderType.dataValue" headerText="Order Type" draggable="false"/>
				<mdgc:MDataGridColumn width="0.170" dataField="contractNum" labelFunction="contractNumLabelFunc" headerText="Contract No." draggable="false"/>
				<mx:DataGridColumn width="0.381" dataField="batchNum" labelFunction="batchNumLabelFunc" headerText="Batch No." draggable="false"/>
				<mx:DataGridColumn width="0.108" dataField="createDate" headerText="Create Date" labelFunction="followUpDateLabelFunc" draggable="false" />		
			</mdcs:columns>				
		</mdcs:MDataGrid>
	</s:VGroup>
</fc:ExtendedNavigatorContent>
