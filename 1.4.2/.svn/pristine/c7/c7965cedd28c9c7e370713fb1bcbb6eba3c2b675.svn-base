package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;


import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleItemListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class SampleItemListServiceBean implements SampleItemListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private SampleTestListServiceLocal sampleTestListService;
	
	@Out(required = false)
	private List<SampleItem> sampleItemList;	
	
	private static final String RECORD_STATUS = "recordStatus";
	private static final String ITEM_CODE = "itemCode";
	private static final String RISK_LEVEL_CODE = "riskLevelCode";

	private List<SampleItem> sampleItemExportList;

	public void retrieveSampleItemListByCriteria(SampleItemCriteria sampleItemCriteria){
		logger.debug("retrieveSampleItemListByCriteria");
	
		if (sampleItemCriteria.getExclusionCode() == null && sampleItemCriteria.getTestCode() == null){
			
			logger.debug(".....no exclusion ");	
				sampleItemList = em.createNamedQuery("SampleItem.findForSampleItemMaint")
				.setParameter(RECORD_STATUS, sampleItemCriteria.getRecordStatus())
				.setParameter(ITEM_CODE, sampleItemCriteria.getItemCode())
				.setParameter(RISK_LEVEL_CODE, sampleItemCriteria.getRiskLevelCode())
				.getResultList();
			
		}else {
			logger.debug(".....have exclusions ");
			sampleItemList = em.createNamedQuery("SampleItem.findByCriteria")
			.setParameter(RECORD_STATUS, sampleItemCriteria.getRecordStatus())
			.setParameter(ITEM_CODE, sampleItemCriteria.getItemCode())
			.setParameter(RISK_LEVEL_CODE, sampleItemCriteria.getRiskLevelCode())
			.setParameter("exclusionCode", sampleItemCriteria.getExclusionCode())
			.setParameter("testCode", sampleItemCriteria.getTestCode())
			.getResultList();
			
		}
		if (sampleItemList != null && sampleItemCriteria.getOrderType()!=null){
			// filter by orderType
			for (Iterator it = sampleItemList.iterator();it.hasNext();){
				SampleItem si = (SampleItem)it.next();
				DmDrug dmDrug = si.getDmDrug(); 
				if (dmDrug!= null){
					if (dmDrug.getDmProcureInfo() != null && dmDrug.getDmProcureInfo().getOrderType() != null){
						if (!dmDrug.getDmProcureInfo().getOrderType().equals(sampleItemCriteria.getOrderType().getDataValue())
								  ){
							it.remove();
						}
					}
				}
			}
		}
	}
	
	public void retrieveSampleItemExportList(Collection<Long> sampleItemKeyList){
		logger.debug("retrieveSampleItemExportList");
		
		sampleItemExportList = em.createNamedQuery("SampleItem.findBySampleItemId")
		.setParameter("keyList",sampleItemKeyList )
		.getResultList();
	}
	
	public  List<SampleItem> retrieveSampleItemListByRecordStatusRiskLevelCodeForValidation(RecordStatus recordStatus, String riskLevelCode){
		logger.debug("retrieveSampleItemListByRecordStatusRiskLevelCodeForValidation");
		
		List<SampleItem> resultList = em.createNamedQuery("SampleItem.findByRecordStatusRiskLevelCode")
			.setParameter(RECORD_STATUS, recordStatus)
			.setParameter(RISK_LEVEL_CODE, riskLevelCode)
			.setHint(QueryHints.FETCH, "o.riskLevel")
			.getResultList();
		
		return resultList;
	}
	
	public void retrieveSampleItemListForSampleTest(RecordStatus recordStatus, String itemCode){
		logger.debug("retrieveSampleItemListForSampleTest");
		List <SampleItem> outList = new ArrayList<SampleItem>();
		
		List <SampleTest> sampleTestList = sampleTestListService.retrieveSampleTestListForReport();
		int sampleTestListCnt = sampleTestList.size();

		List <SampleItem> tmpList = em.createNamedQuery("SampleItem.findForSampleTest")
			.setParameter(ITEM_CODE, itemCode+"%")
			.setParameter(RECORD_STATUS, recordStatus)
			.setParameter("proceedSampleTestFlag", YesNoFlag.Yes )
			.setHint(QueryHints.FETCH, "o.riskLevel")
			.setHint(QueryHints.BATCH, "o.exclusionTestList")
			.setHint(QueryHints.BATCH, "o.exclusionTestList.sampleTest")
			.getResultList();
		
		
		
		for (SampleItem sampleItem :tmpList ){
			
			
			sampleItem.getExclusionTestList().size();	
			
			if(sampleItem.getDmDrug()!=null){
				
				
				if( RegStatus.PandPRegistrationNotRequired.getDataValue().equals(sampleItem.getDmDrug().getDqaDrugSupplier().getRegStatus()) ||   
					RegStatus.PandPRegistrationDrug.getDataValue().equals(sampleItem.getDmDrug().getDqaDrugSupplier().getRegStatus()) 
					
				  )
				{
					if (sampleItem.getExclusionTestList().size() <sampleTestListCnt){
						outList.add(sampleItem);
					}
				}
			}
		}
		sampleItemList = outList;
	}
	
	public void retrieveAllSampleItemListLikeItemCodeRecordStatus(RecordStatus recordStatus, String itemCode){
		logger.debug("retrieveAllSampleItemListLikeItemCodeRecordStatus");
		
		sampleItemList = em.createNamedQuery("SampleItem.findLikeItemCodeRecordStatus")
		.setParameter(ITEM_CODE, itemCode+"%")
		.setParameter(RECORD_STATUS, recordStatus)
		.getResultList();
	}
	
	public List<SampleItem> getSampleItemExportList() {
		return sampleItemExportList;
	}
	
	public List<SampleItem> retrieveSampleItemListByTestFrequency(TestFrequency testFrequencyIn, OrderType orderTypeIn)
	{
		logger.debug("retrieveSampleItemListByTestFrequency");
		String riskLevelCode = null;
		List<SampleItem> sampleItemListReturn = null;
		Boolean nullRiskLevelFound = false;
		String testCodeIn = testFrequencyIn.getSampleTest().getTestCode();
		
		if(testFrequencyIn.getRiskLevel()==null || testFrequencyIn.getRiskLevel().getRiskLevelCode()==null)
		{
			nullRiskLevelFound=true;
		}
		else
		{
			riskLevelCode = testFrequencyIn.getRiskLevel().getRiskLevelCode();
		}
		
		
		if(nullRiskLevelFound)
		{
			sampleItemListReturn = em.createNamedQuery("SampleItem.findByProceedSampleTestFlagRecordStatus")
									.setParameter("proceedSampleTestFlag", YesNoFlag.Yes)
									.setParameter(RECORD_STATUS, RecordStatus.Active)
									.getResultList();
		}
		else
		{
			sampleItemListReturn = em.createNamedQuery("SampleItem.findByRiskLevelCodeProceedSampleTestFlagRecordStatus")
									.setParameter("proceedSampleTestFlag", YesNoFlag.Yes)
									.setParameter(RECORD_STATUS, RecordStatus.Active)
									.setParameter(RISK_LEVEL_CODE, riskLevelCode)
									.getResultList();
		}
		
		for (Iterator it = sampleItemListReturn.iterator();it.hasNext();){
			SampleItem si= (SampleItem)it.next(); 
			DmDrug dmDrug = si.getDmDrug(); 
			List<ExclusionTest> exclusionTestListIn = si.getExclusionTestList();
			if (dmDrug!= null)
			{
				if(!(orderTypeIn.getDataValue().equals(dmDrug.getDmProcureInfo().getOrderType())))
				{
					it.remove();
					continue;
				}
				else if (!("N").equals(dmDrug.getDqaDrugSupplier().getRegStatus()) && !("R").equals(dmDrug.getDqaDrugSupplier().getRegStatus()))
				{
					it.remove();
					continue;
				}
				
			}
			
			if(exclusionTestListIn!=null && exclusionTestListIn.size()>0)
			{
				for(ExclusionTest et : exclusionTestListIn)
				{
					ExclusionTest exclusionTestIn = et;
					if(exclusionTestIn.getSampleTest().getTestCode().equals(testCodeIn))
					{
						it.remove();
						break;
					}
				}
			}
		}
		
		return sampleItemListReturn;
	}
	
	@Remove
	public void destroy(){
		if (sampleItemList != null){
			sampleItemList = null;
		}	
		
		if (sampleItemExportList != null){
			sampleItemExportList = null;
		}
	}

}
