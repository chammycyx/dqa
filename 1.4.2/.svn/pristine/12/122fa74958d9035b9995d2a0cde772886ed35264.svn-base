package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaOutstandBatchManager")
@MeasureCalls
public class CoaOutstandBatchManagerBean implements CoaOutstandBatchManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	public CoaOutstandBatch isOutstandBatchExist( Contract contract, String batchNum ) {
		logger.debug("isOutstandBatchExist #0 #1", contract.getContractId(), batchNum);
		return retrieveCoaOutstandBatchForOutstand(contract, batchNum);
	}
	
	@SuppressWarnings("unchecked")
	private CoaOutstandBatch retrieveCoaOutstandBatchForOutstand( Contract contract, String batchNum){
		logger.debug("CoaOutstandBatch.findForOutstand #0 \"#1\"", contract.getContractId(), batchNum);
		List<CoaOutstandBatch> osBatchList = em.createNamedQuery("CoaOutstandBatch.findForOutstand")
											   .setHint(QueryHints.FETCH, "o.contract")											   
											   .setHint(QueryHints.FETCH, "o.coaInEmailAtt")
											   .setHint(QueryHints.FETCH, "o.coaInEmailAtt.coaInEmail")
											   .setHint(QueryHints.FETCH, "o.coaInEmailAtt.coaInEmail.emailLog")
											   .setParameter("contract", contract)
											   .setParameter("batchNum", batchNum)
											   .getResultList();
		if( osBatchList.size() > 0 ) {
			
			return osBatchList.get(0);
		} else {
			return null;
		}		
	}
	
	public void createCoaOutstandBatch(Contract c, CoaInEmailAtt att, String batchNum) {
		logger.debug("createCoaOutstandBatch #0 #1 #2", c.getContractNum(), att.getFileName(), batchNum);
		CoaOutstandBatch osBatch = new CoaOutstandBatch();
		osBatch.setBatchNum(batchNum);
		osBatch.setCoaInEmailAtt(att);		
		osBatch.setContract(c);
		
		em.persist( osBatch );
		em.flush();
	}
	
	public void updateCoaOutstandBatch( CoaOutstandBatch coaOutstandBatch, CoaFileFolder folder, Date receiveDate ) {
		coaOutstandBatch.setReceiveDate( receiveDate );
		coaOutstandBatch.setCoaFileFolder( folder );
		em.merge( coaOutstandBatch );
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public CoaOutstandBatch retrieveCoaOutStandBatchForCoaFileFolder(CoaFileFolder coaFileFolder){
		logger.debug("retrieveCoaOutStandBatchForCoaFileFolder #0", coaFileFolder.getCoaFileFolderId());
		
		List<CoaOutstandBatch> osBatchList = em.createNamedQuery("CoaOutstandBatch.findForCoaFileFolder")
			.setHint(QueryHints.FETCH, "o.contract")											   
			.setHint(QueryHints.FETCH, "o.coaInEmailAtt")
			.setHint(QueryHints.FETCH, "o.coaInEmailAtt.coaInEmail")
			.setHint(QueryHints.FETCH, "o.coaInEmailAtt.coaInEmail.emailLog")
			.setParameter("coaFileFolderId", coaFileFolder.getCoaFileFolderId())
			.getResultList();
		
		if (!osBatchList.isEmpty()){
			return osBatchList.get(0);
		}else {
			return null;
		}
	}
}
