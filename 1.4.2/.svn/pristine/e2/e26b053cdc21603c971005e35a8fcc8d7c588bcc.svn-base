<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	xmlns:flexiframe="com.google.code.flexiframe.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("suppPerfAuditRptView")]
	</fx:Metadata>
	
	<fx:Declarations>
		<mx:DateFormatter id="fileDateFormatter" formatString="YYYYMMDD"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import com.google.code.flexiframe.IFrame;
			
			import flash.events.Event;
			
			import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
			import hk.org.ha.event.pms.dqa.suppperf.RetrieveAuditRptExcelEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRpt;
			import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRptCriteria;
			import hk.org.ha.event.pms.dqa.sample.RefreshFullInstitutionListEvent;
			import hk.org.ha.event.pms.dqa.sample.RetrieveFullInstitutionListEvent;
			import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
			import hk.org.ha.fmk.pms.security.UamInfo;
			import hk.org.ha.model.pms.dqa.udt.YesNoFlag;


			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.ItemClickEvent;
			import mx.managers.PopUpManager;
			import mx.messaging.config.ServerConfig;
			import spark.components.DropDownList;
			import spark.events.DropDownEvent;
			import spark.events.TextOperationEvent;
			import mx.collections.ListCollectionView;

			[In] [Bindable]
			public var uamInfo:UamInfo;
			
			[Bindable]
			private var auditRptCriteria:AuditRptCriteria;
			
			[Bindable]
			private var buttonBarName:ArrayCollection = new ArrayCollection(['Retrieve', 'Clear']);
			
			[In][Bindable]
			public var fullInstitutionList:ArrayCollection;
			
			private var todayDate:Date;
			
			private var msgProp:SystemMessagePopupProp;
			
			private var auditRpt:AuditRpt;
			
			private var screenName:String="suppPerfAuditRptView";
			
			public override function onShow():void{
				auditRptCriteria = new AuditRptCriteria();
				
				if (!pmsToolbar.hasInited()) {
					pmsToolbar.clearFunc = clear;
					pmsToolbar.exportFunc = export;
					pmsToolbar.init();
				}
				
				clear();
			}
			
			
			public override function onShowLater():void{
				var event:Event = new RefreshFullInstitutionListEvent(screenName);
				dispatchEvent(new RetrieveFullInstitutionListEvent(event));
			//	dispatchEvent(new RetrieveUserInfoListEvent());	
				
			}
			
			public function resetDateAndInstitution():void{
				todayDate = new Date();
				
				endDate.selectedDate = todayDate;
				startDate.selectedDate = todayDate;
				startDate.selectedDate.fullYear -=1;
				ddlInstitution.enabled = true;
				startDate.enabled = true;
				endDate.enabled= true;
				setInstitutionContent(ddlInstitution, fullInstitutionList);
			}
			
			protected function clear():void{
				pmsToolbar.clearButton.enabled = true;
				pmsToolbar.exportButton.enabled = true;
				resetDateAndInstitution();
				ddlInstitution.selectedIndex = 0;
			}
			
			protected function export():void{
				if(auditRptCriteria.startDate != null && auditRptCriteria.endDate != null)
				{
					pmsToolbar.clearButton.enabled = false;
					pmsToolbar.exportButton.enabled = false;
					startDate.enabled = false;
					endDate.enabled= false;
					auditRptCriteria.endDate["date"] += 1;
					ddlInstitution.enabled = false;
					var institution: Institution = ddlInstitution.selectedItem;
					
					auditRptCriteria.institutionCode =institution.institutionCode;
					dispatchEvent(new RetrieveAuditRptExcelEvent(auditRptCriteria, generateReport));
				}
			}
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function generateReport():void 
			{
				var today:Date = new Date();
				
				var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
				dispatchEvent(new GenExcelDocumentEvent("/excelTemplate/auditRpt.xhtml", "AuditReport", "AuditReport_"+fileDate));		
				
				
				pmsToolbar.clearButton.enabled = true;
			}
			
			
			private function cbxInstitutionEmuLabelFunc(data: Object):String{
				if (data.institutionCode == "*") {
					return "All";
				} else {
					return data.institutionCode + " - " + data.institutionName;
				}
			}
			
			private function cbxInstitutionEmuExtLabelFunc(data: Object):String{
				if (data.institutionCode == "*") {
					return "All";
				} else {
					return data.institutionCode;
				}
			}
			
			private function cbxOpen(evt:DropDownEvent, obj:DropDownList, widthIn:int):void {
				obj.dropDown.width = widthIn;
			}
			
			public function setInstitutionContent(institutionDropDownList:ExtendedDropDownList, inList:ArrayCollection):void{
				
				
				var selectedInst:Institution = null;
				
				if(fullInstitutionList!=null)
				{
					selectedInst = findInstitution(uamInfo.hospital, fullInstitutionList);
					var instAll:Institution = new Institution;
					instAll.institutionCode = "*";
					instAll.institutionName = "ALL";
					instAll.pcuCode = selectedInst.institutionCode;
					
					inList = fullInstitutionList;
					var firstInstitution:Institution = inList.getItemAt(0) as Institution;
					
					if(firstInstitution.institutionName != "ALL"){
						inList.addItemAt(instAll,0);
					}
					
					institutionDropDownList.dataProvider= inList;
					institutionDropDownList.selectedIndex = 0;
					callLater(function():void {
						
						institutionDropDownList.selectedIndex = inList.getItemIndex(selectedInst);
					}
					);
					
						
				}
				
				
			}
			
			private function findInstitution(instCode:String, srcList:ListCollectionView):Institution {
				for each (var institution:Institution in srcList) {
					if (institution.institutionCode == instCode) {
						return institution;
					}
				}
				return null;
			}
		]]>
	</fx:Script>	
	
	<fc:layout>
		<s:VerticalLayout gap="0"/> 
	</fc:layout>
	<fc:Toolbar id="pmsToolbar" width="100%"/>
	
	<s:VGroup width="100%" gap="10" paddingBottom="10" paddingTop="10" paddingLeft="10" paddingRight="10">
		<s:Panel id="pSuppPerformanceRptRpt"  width="100%" height="108" title="Report Criteria">
			<s:layout>
				<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
			</s:layout>
			<s:HGroup verticalAlign="middle">
				<s:Label text="Institution Code" width="150"/>
				<fc:ExtendedDropDownList id="ddlInstitution"  width="90" dataProvider="{fullInstitutionList}" 
										 creationComplete="setInstitutionContent(ddlInstitution,fullInstitutionList)"
										 labelField="institutionCode" 
										 labelFunction="cbxInstitutionEmuLabelFunc"
										 extLabelFunction="cbxInstitutionEmuExtLabelFunc"
										 open="cbxOpen(event, ddlInstitution, 350)"/>
			</s:HGroup>	
			<s:HGroup verticalAlign="middle">
				<s:Label text="Problem Date" width="150"/>
				<calendar:AbbDateField width="126" id="startDate" editable="true"  selectedDate="@{auditRptCriteria.startDate}"/>
				<s:HGroup height="100%" verticalAlign="middle">
					<s:Label top="10" text=" to "/>
				</s:HGroup>
				<calendar:AbbDateField width="126" id="endDate" editable="true"  selectedDate="@{auditRptCriteria.endDate}"/>
			</s:HGroup>
		</s:Panel>
	</s:VGroup>
	
</fc:ExtendedNavigatorContent>
