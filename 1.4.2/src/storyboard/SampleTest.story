<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels>
    <screen href="maintenance/sampleTest/Sample%20Item%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/sampleTest/Risk%20Level%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/sampleTest/Test%20Frequency%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/sampleTest/Sample%20Testing%20File%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/general/User%20Contact%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/general/Institution%20Contact%20Maintenance.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%201%20-%20Schedule%20Generation%20(SG).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%202%20-%20Schedule%20(S).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%203%20-%20Document%20(D).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%204%20-%20Institution%20Assignment%20(IA).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%205%20-%20Drug%20Sample%20(DS).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%206%20-%20Lab%20Test%20(LT).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%207%20-%20Lab%20Collection%20(LC).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%208%20-%20Test%20Result%20(TR).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%209%20-%20Payment%20(P).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Step%2010%20-%20Schedule%20Enquiry.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Schedule%20Detail%20Disable.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Test%20Result%20Enquiry.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Sample%20Testing%20Inventory.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Email%20Enquiry.screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Email%20Template%20(To%20Supplier)%20-%20Schedule%20(S).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Email%20Template%20(To%20Institution)%20-%20Institution%20Assignment%20(IA).screen#/"/>
  </panels>
  <panels>
    <screen href="sampleTest/Email%20Template%20(To%20Lab)%20-%20Lab%20Test%20(LT).screen#/"/>
  </panels>
</story:Storyboard>
