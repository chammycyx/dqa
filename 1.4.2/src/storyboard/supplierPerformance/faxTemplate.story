<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels x="675" y="75">
    <screen href="Fax%20Template%20-%20Initial%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="75" y="375">
    <screen href="Fax%20Template%20-%20Interim%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="75" y="75">
    <screen href="Fax%20Template%20-%20Final%20Correspondence%20to%20Hospital.screen#/"/>
  </panels>
  <panels x="975" y="75">
    <screen href="Fax%20Template%20-%20Initial%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="375" y="375">
    <screen href="Fax%20Template%20-%20Interim%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
  <panels x="375" y="75">
    <screen href="Fax%20Template%20-%20Final%20Correspondence%20to%20Supplier.screen#/"/>
  </panels>
</story:Storyboard>
