alter table coa_discrepancy add constraint fk_coa_discrepancy_01 foreign key (discrepancy_id) references coa_discrepancy_key (discrepancy_id);
alter table coa_discrepancy add constraint fk_coa_discrepancy_02 foreign key (parent_coa_discrepancy) references coa_discrepancy (coa_discrepancy_id);
alter table coa_discrepancy add constraint fk_coa_discrepancy_03 foreign key (coa_batch_id) references coa_batch (coa_batch_id);

--//@UNDO
alter table coa_discrepancy drop constraint fk_coa_discrepancy_01;
alter table coa_discrepancy drop constraint fk_coa_discrepancy_02;
alter table coa_discrepancy drop constraint fk_coa_discrepancy_03;
--//