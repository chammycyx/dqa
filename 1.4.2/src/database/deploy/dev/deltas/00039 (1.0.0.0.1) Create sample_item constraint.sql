alter table sample_item add constraint fk_sample_item_01 foreign key (item_code) references drug_item (item_code);

--//@UNDO
alter table sample_item drop constraint fk_sample_item_01;
--//
