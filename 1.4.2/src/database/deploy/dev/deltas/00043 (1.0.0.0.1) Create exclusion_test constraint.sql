alter table exclusion_test add constraint fk_exclusion_test_01 foreign key (sample_item_id) references sample_item (sample_item_id);

--//@UNDO
alter table exclusion_test drop constraint fk_exclusion_test_01;
--//