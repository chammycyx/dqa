insert into FUNC_SEQ_NUM (FUNC_CODE,FUNC_DESC,START_NUM,END_NUM,FUNC_PREFIX,LAST_NUM,MODIFY_USER,MODIFY_DATE,CREATE_USER,CREATE_DATE,VERSION) values 
('SN','Schedule Number',1,99999999,'SN',0,'dqaadmin',sysdate,'dqaadmin',sysdate,0);

insert into FUNC_SEQ_NUM (FUNC_CODE,FUNC_DESC,START_NUM,END_NUM,FUNC_PREFIX,LAST_NUM,MODIFY_USER,MODIFY_DATE,CREATE_USER,CREATE_DATE,VERSION) values 
('CN','Collection Number',1,99999999,'CN',0,'dqaadmin',sysdate,'dqaadmin',sysdate,0);

insert into FUNC_SEQ_NUM (FUNC_CODE,FUNC_DESC,START_NUM,END_NUM,FUNC_PREFIX,LAST_NUM,MODIFY_USER,MODIFY_DATE,CREATE_USER,CREATE_DATE,VERSION) values 
('IA','Memo Ref Number',1,99999999,'IA',0,'dqaadmin',sysdate,'dqaadmin',sysdate,0);

insert into FUNC_SEQ_NUM (FUNC_CODE,FUNC_DESC,START_NUM,END_NUM,FUNC_PREFIX,LAST_NUM,MODIFY_USER,MODIFY_DATE,CREATE_USER,CREATE_DATE,VERSION) values 
('SS','Supplier Ref Number',1,99999999,'SS',0,'dqaadmin',sysdate,'dqaadmin',sysdate,0);


insert into SAMPLE_TEST (TEST_CODE, TEST_NAME, MODIFY_USER, MODIFY_DATE, CREATE_USER, CREATE_DATE, VERSION) values
('CHEM','Chemical Analysis','dqaadmin',sysdate,'dqaadmin',sysdate,0);

insert into SAMPLE_TEST (TEST_CODE, TEST_NAME, MODIFY_USER, MODIFY_DATE, CREATE_USER, CREATE_DATE, VERSION) values
('MICRO','Microbiology Test','dqaadmin',sysdate,'dqaadmin',sysdate,0);

insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0030', 'en_US', 'Invalid item', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0031', 'en_US', 'Invalid contract', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0032', 'en_US', 'Sample Test Schedule exist', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0033', 'en_US', 'Please select the file for upload', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0034', 'en_US', 'Only adjusted record can be edited', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 1000, null, 'Q', '0035', 'en_US', 'Are you sure to confirm the record?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0036', 'en_US', 'Are you sure to reverse the schedule?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0037', 'en_US', 'Please save the record in edit mode', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 1000, null, 'Q', '0038', 'en_US', 'Do you want to save the record?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0039', 'en_US', 'Are you sure to reverse the schedule to previous status?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0040', 'en_US', 'Reference Drug Cost > HKD$5000. No need to input Retention Quantity', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0041', 'en_US', 'Required Quantity should be equal to Actual Required Quantity', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0042', 'en_US', 'Retention Quantity should be equal to Actual Retention Quantity', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0043', 'en_US', 'Please select a record before upload', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0044', 'en_US', 'Please input Test Report Date first', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0045', 'en_US', 'The file is referenced by Schedules', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0046', 'en_US', 'Please retrieve a record before save', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0047', 'en_US', 'Please select record to confirm', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0048', 'en_US', 'Please input both Received Quantity, Batch Number and Expiry Date for the record to be confirmed', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0049', 'en_US', 'Please input valid Received Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0050', 'en_US', 'Invalid Expiry Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0051', 'en_US', 'Received Quantity not equal to Required Quantity plus Retention Quantity. Do you want to continue?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0052', 'en_US', 'Please input valid Collection Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0053', 'en_US', 'Please input valid Test Report Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0054', 'en_US', 'Please input valid PR Issued Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0055', 'en_US', 'Please input valid Cheque Received Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0056', 'en_US', 'Please input valid Cheque Sent Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0057', 'en_US', 'Please input valid Invoice Received Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0058', 'en_US', 'Please input valid Invoice Sent Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0059', 'en_US', 'Please input valid Date', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0060', 'en_US', 'Please input valid lab collect quantity', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0061', 'en_US', 'Lab collect quantity is greater than received quantity', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0062', 'en_US', 'Please input at least 2 parameters', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0063', 'en_US', 'Please input lab code', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0064', 'en_US', 'Are you sure to send email?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0065', 'en_US', 'Are you sure to export email?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'I', '0066', 'en_US', 'Completed.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0067', 'en_US', 'Please input at least 2 parameters including Test Report Date.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0068', 'en_US', 'Please input at least 2 parameters including Inventory Date.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0069', 'en_US', 'Please input at least 2 parameters including Send Date.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0070', 'en_US', 'Please input at least 2 parameters including Schedule Month.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0071', 'en_US', 'Please either input ALL for Order Type or Risk Level.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0072', 'en_US', 'Please input at least 3 parameters including Report Template and Schedule Month.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0073', 'en_US', 'Test Frequency Not Found.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0074', 'en_US', 'Please input TO field.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0075', 'en_US', 'Proceed Sample Test of Risk Level is NO.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0076', 'en_US', 'Please input all the fields, uploaded document and the fields inside Test Details.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0077', 'en_US', 'Please input Test Report Date and Test Code first.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0078', 'en_US', 'Quantity should be greater than 0.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0079', 'en_US', 'Stock on hand quantity will be negative.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'W', '0080', 'en_US', 'Please input at least 1 parameter.', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0081', 'en_US', 'Email to Supplier has not been sent. Do you want to continue?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0082', 'en_US', 'Email to Institution has not been sent. Do you want to continue?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);
insert into system_message(application_id, function_id, operation_id, severity_code, message_code, locale, main_msg, suppl_msg, detail, btn_yes_caption, btn_yes_size, btn_yes_shortcut, btn_no_caption, btn_no_size, btn_no_shortcut, btn_cancel_caption, btn_cancel_size, btn_cancel_shortcut, effective, expiry, create_user, create_date, modify_user, modify_date, version) values 
( 1006, 3000, null, 'Q', '0083', 'en_US', 'Email to Lab has not been sent. Do you want to continue?', '' ,'', '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, 'itd', sysdate, 'itd', sysdate, 1);



--//@UNDO
delete FUNC_SEQ_NUM;
delete SAMPLE_TEST;
delete system_message where message_code = '0030';
delete system_message where message_code = '0031';
delete system_message where message_code = '0032';
delete system_message where message_code = '0033';
delete system_message where message_code = '0034';
delete system_message where message_code = '0035';
delete system_message where message_code = '0036';
delete system_message where message_code = '0037';
delete system_message where message_code = '0038';
delete system_message where message_code = '0039';
delete system_message where message_code = '0040';
delete system_message where message_code = '0041';
delete system_message where message_code = '0042';
delete system_message where message_code = '0043';
delete system_message where message_code = '0044';
delete system_message where message_code = '0045';
delete system_message where message_code = '0046';
delete system_message where message_code = '0047';
delete system_message where message_code = '0048';
delete system_message where message_code = '0049';
delete system_message where message_code = '0050';
delete system_message where message_code = '0051';
delete system_message where message_code = '0052';
delete system_message where message_code = '0053';
delete system_message where message_code = '0054';
delete system_message where message_code = '0055';
delete system_message where message_code = '0056';
delete system_message where message_code = '0057';
delete system_message where message_code = '0058';
delete system_message where message_code = '0059';
delete system_message where message_code = '0060';
delete system_message where message_code = '0061';
delete system_message where message_code = '0062';
delete system_message where message_code = '0063';
delete system_message where message_code = '0064';
delete system_message where message_code = '0065';
delete system_message where message_code = '0066';
delete system_message where message_code = '0067';
delete system_message where message_code = '0068';
delete system_message where message_code = '0069';
delete system_message where message_code = '0070';
delete system_message where message_code = '0071';
delete system_message where message_code = '0072';
delete system_message where message_code = '0073';
delete system_message where message_code = '0074';
delete system_message where message_code = '0075';
delete system_message where message_code = '0076';
delete system_message where message_code = '0077';
delete system_message where message_code = '0078';
delete system_message where message_code = '0079';
delete system_message where message_code = '0080';
delete system_message where message_code = '0081';
delete system_message where message_code = '0082';
delete system_message where message_code = '0083';
--//