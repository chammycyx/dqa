alter table SAMPLE_MOVEMENT add MANUF_ON_HAND_QTY number(10,0);

--//@UNDO
alter table SAMPLE_MOVEMENT set unused column MANUF_ON_HAND_QTY ;
--//
