create table coa_discrepancy (
	coa_discrepancy_id number(19) not null, 
	coa_batch_id number(19) null, 
	discrepancy_id number(19) null, 
	overwrite_remark varchar2(300) null, 
	pass_flag varchar2(1) not null, 
	parent_coa_discrepancy number(19) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 		
	version number(19) not null, 
	constraint pk_coa_discrepancy primary key (coa_discrepancy_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_discrepancy;
--//
