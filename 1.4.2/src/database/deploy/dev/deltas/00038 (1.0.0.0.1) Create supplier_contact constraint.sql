alter table supplier_contact add constraint fk_supplier_contact_01 foreign key (contact_id) references contact (contact_id);
alter table supplier_contact add constraint fk_supplier_contact_02 foreign key (supplier_code) references supplier (supplier_code);

--//@UNDO
alter table supplier_contact drop constraint fk_supplier_contact_01;
alter table supplier_contact drop constraint fk_supplier_contact_02;
--//