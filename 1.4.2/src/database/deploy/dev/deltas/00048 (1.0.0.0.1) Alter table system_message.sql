alter table system_message rename column supp_msg to suppl_msg;
alter table system_message rename column details to detail;

--//@UNDO
alter table system_message rename column suppl_msg to supp_msg;
alter table system_message rename column detail to details;
--//
