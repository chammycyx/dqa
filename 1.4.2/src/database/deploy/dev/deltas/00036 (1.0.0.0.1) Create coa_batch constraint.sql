alter table coa_batch add constraint fk_coa_batch_01 foreign key (coa_item_id) references coa_item (coa_item_id);
alter table coa_batch add constraint fk_coa_batch_02 foreign key (coa_batch_ver_id) references coa_batch_ver (coa_batch_ver_id);

--//@UNDO
alter table coa_batch drop constraint fk_coa_batch_01;
alter table coa_batch drop constraint fk_coa_batch_02;
--//
