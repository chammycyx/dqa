ALTER TABLE COA_BATCH ADD (
	INTERFACE_DATE TIMESTAMP NULL,
	INTERFACE_FLAG VARCHAR2(1) DEFAULT 'N' NOT NULL
);

--//@UNDO
ALTER TABLE COA_BATCH DROP COLUMN INTERFACE_DATE;
ALTER TABLE COA_BATCH DROP COLUMN INTERFACE_FLAG;
--//