alter table QA_PROBLEM add INVEST_RPT_DATE timestamp;
alter table QA_PROBLEM add CLOSE_DATE timestamp;
--//@UNDO
alter table QA_PROBLEM set unused column INVEST_RPT_DATE ;
alter table QA_PROBLEM set unused column CLOSE_DATE ;
--//
