update FUNC_SEQ_NUM set YEAR=to_number(to_char(sysdate, 'yyyy')) where FUNC_PREFIX ='DDN';
update FUNC_SEQ_NUM set LAST_NUM=nvl((select substr(max(REF_NUM),5,9) from DLD_RPT_FORM where substr(REF_NUM,0,4) = to_number(to_char(sysdate, 'yyyy'))),0) where FUNC_PREFIX ='DDN';


--//@UNDO
update FUNC_SEQ_NUM set YEAR=nvl((select substr(max(REF_NUM),0,4) from DLD_RPT_FORM),to_number(to_char(sysdate, 'yyyy'))) where FUNC_PREFIX ='DDN';
update FUNC_SEQ_NUM set LAST_NUM=nvl((select substr(max(REF_NUM),5,9) from DLD_RPT_FORM where substr(REF_NUM,0,4) = (select substr(max(REF_NUM),0,4) from DLD_RPT_FORM)),0) where FUNC_PREFIX ='DDN';
--//