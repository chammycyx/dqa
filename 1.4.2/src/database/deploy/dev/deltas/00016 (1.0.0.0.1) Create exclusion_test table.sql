create table exclusion_test (
	exclusion_test_id number(19) not null, 
	exclusion_code varchar2(3) null, 
	exclusion_reason varchar2(80) null,
	sample_item_id number(19) null, 
	test_code varchar2(5) not null, 
	create_user varchar2(15) not null, 	 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 	
	modify_date timestamp not null, 	
	version number(19) not null, 
	constraint pk_exclusion_test primary key (exclusion_test_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table exclusion_test;
--//