update SYSTEM_MESSAGE set MAIN_MSG ='Duplicate record found' where MESSAGE_CODE = '0007';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input PO no.' where MESSAGE_CODE = '0151';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid PO no.' where MESSAGE_CODE = '0152';
update SYSTEM_MESSAGE set MAIN_MSG ='Duplicate PO no.' where MESSAGE_CODE = '0154';
update SYSTEM_MESSAGE set MAIN_MSG ='Duplicate contact person' where MESSAGE_CODE = '0161';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid telephone no.' where MESSAGE_CODE = '0167';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input stock on-hand quantity' where MESSAGE_CODE = '0171';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input average monthly consumption quantity' where MESSAGE_CODE = '0172';
update SYSTEM_MESSAGE set MAIN_MSG ='The delayed delivery report form [#0] status has been updated to [#1]' where MESSAGE_CODE = '0175';
update SYSTEM_MESSAGE set MAIN_MSG ='Order quantity should be equal to or greater than outstanding quantity' where MESSAGE_CODE = '0178';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid BPA no.' where MESSAGE_CODE = '0179';





--//@UNDO
update SYSTEM_MESSAGE set MAIN_MSG ='Duplicated record found' where MESSAGE_CODE = '0007';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input PO No.' where MESSAGE_CODE = '0151';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid PO No.' where MESSAGE_CODE = '0152';
update SYSTEM_MESSAGE set MAIN_MSG ='Duplicated PO No.' where MESSAGE_CODE = '0154';
update SYSTEM_MESSAGE set MAIN_MSG ='Duplicated contact person' where MESSAGE_CODE = '0161';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid tel. no' where MESSAGE_CODE = '0167';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input stock on hand quantity' where MESSAGE_CODE = '0171';
update SYSTEM_MESSAGE set MAIN_MSG ='Please input avg monthly consumption quantity' where MESSAGE_CODE = '0172';
update SYSTEM_MESSAGE set MAIN_MSG ='The delayed delivery report form [#0] status has been updated [#1]' where MESSAGE_CODE = '0175';
update SYSTEM_MESSAGE set MAIN_MSG ='Order qty shall be equal to or greater than outstanding qty' where MESSAGE_CODE = '0178';
update SYSTEM_MESSAGE set MAIN_MSG ='Invalid BPA No.' where MESSAGE_CODE = '0179';


--//