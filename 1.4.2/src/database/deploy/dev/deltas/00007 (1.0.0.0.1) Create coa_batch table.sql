create table coa_batch (
	coa_batch_id number(19) not null, 
	coa_item_id number(19) null, 
	coa_batch_ver_id number(19) null, 
	batch_num varchar2(20) null, 
	follow_up_date timestamp null, 
	discrepancy_remark varchar2(300) null, 
	fail_remark varchar2(300) null,
	coa_batch_ver_count number(10) null, 
	discrepancy_action_date timestamp null, 		
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null,  
	constraint pk_coa_batch primary key (coa_batch_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_batch;
--//
