alter table USER_INFO modify USER_CODE varchar2(12);
alter table USER_INFO add CPO_FLAG varchar2(1) null;

--//@UNDO
alter table USER_INFO modify USER_CODE varchar2(6);
alter table USER_INFO set unused column CPO_FLAG;
--//
