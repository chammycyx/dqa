create table contract (
	contract_id number(19) not null, 
	contract_num varchar2(20) null,
	contract_suffix varchar2(3) null, 
	item_code varchar2(6) null, 
	contract_type varchar2(1) null, 	
	start_date timestamp null, 
	end_date timestamp null, 
	pack_size number(10) null, 
	pack_price number(19,4) null, 
	currency_code varchar2(3) null, 
	supplier_code varchar2(5) null, 
	manu_code varchar2(5) null, 
	pharm_code varchar2(5) null,
	suspend_flag varchar2(1) null,  
	module_type varchar2(1) null, 
	upload_date timestamp null, 			
	create_user varchar2(15) not null, 			
	create_date timestamp not null, 
	modify_date timestamp not null, 
	modify_user varchar2(15) not null, 
	version number(19) not null, 		
	constraint pk_contract primary key (contract_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table contract;
--//
