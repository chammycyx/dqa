alter table coa_file_folder add constraint fk_coa_file_folder_01 foreign key (coa_batch_id) references coa_batch (coa_batch_id);
alter table coa_file_folder add constraint fk_coa_file_folder_02 foreign key (file_id) references file_item (file_id);

--//@UNDO
alter table coa_file_folder drop constraint fk_coa_file_folder_01;
alter table coa_file_folder drop constraint fk_coa_file_folder_02;
--//
