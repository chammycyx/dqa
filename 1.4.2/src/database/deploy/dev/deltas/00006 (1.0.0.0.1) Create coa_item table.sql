create table coa_item (
	coa_item_id number(19) not null, 
	coa_batch_id number(19) null,
	contract_id number(19) null, 
	order_type varchar2(1) not null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 	
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 	
	version number(19) not null, 
	constraint pk_coa_item primary key (coa_item_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_item;
--//
