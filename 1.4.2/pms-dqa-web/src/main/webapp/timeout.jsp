<%@ page contentType="text/html;charset=Big5" isErrorPage="true"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; Big5" />
<title>Error</title>
<link rel="stylesheet" type="text/css" href="./css/application_framework.css">
</head>
<body>
<table width="100%">
	<tr>
		<td width="150"><img src="./image/ha_logo.gif"/></td>
	</tr>
	<tr>
		<td style="font-family:Arial; color:red; font-weight:bold; font-size:18;">Your session is expired</td>
	</tr>
	<tr>
		<%String logonUrl = "${web.uam-server-url}" + "/logoff.jsp?appCode=DQA";%>
		
		<td><a href=<%=logonUrl %>>Please logon again</a></td>
	</tr>
</table>
</body>
</html>