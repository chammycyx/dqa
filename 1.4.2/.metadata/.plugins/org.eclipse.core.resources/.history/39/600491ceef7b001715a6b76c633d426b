package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;
import hk.org.ha.fmk.pms.web.MeasureCalls;


import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleMovementService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleMovementServiceBean implements SampleMovementServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private SampleMovement sampleMovement;
	
	private boolean success;

			
	@SuppressWarnings("unchecked")
	public SampleMovement retrieveSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("retrieveSampleMovement" );

		List<SampleMovement> sampleMovementList = em.createNamedQuery("SampleMovement.findBySampleLogId") 
		.setParameter("sampleLogId", sampleMovementIn.getSampleLogId())
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
        .getResultList();
		
		if (sampleMovementList==null || sampleMovementList.size()==0)
		{
			sampleMovement = null;
			return null;
		}
		else
		{
			sampleMovement = sampleMovementList.get(0);
			SampleMovement smNewest = new SampleMovement();
			SampleMovement smItemCode = retrieveSampleMovementNewestByItemCode(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode());
			SampleMovement smItemCodeManuf = retrieveSampleMovementNewestByItemCodeManuf(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode() , sampleMovement.getManufCode() );
			
			if(!smItemCode.getManufCode().equals(smItemCodeManuf.getManufCode()) ){
				if(smItemCode.getCreateDate().before(smItemCodeManuf.getCreateDate())){
					smNewest.setOnHandQty(smItemCodeManuf.getOnHandQty());
					smNewest.setManufOnHandQty(smItemCode.getManufOnHandQty());
				}else{
					smNewest.setOnHandQty(smItemCode.getOnHandQty());
					smNewest.setManufOnHandQty(smItemCodeManuf.getManufOnHandQty());
				}
			}else{
				smNewest.setOnHandQty(smItemCode.getOnHandQty());
				smNewest.setManufOnHandQty(smItemCode.getManufOnHandQty());
				
			}
			
			return smNewest;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public SampleMovement retrieveSampleMovementNewestByItemCode(String itemCode){
		logger.debug("retrieveSampleMovementNewestByItemCode" );
		List<SampleMovement> sampleMovementNewestList = em.createNamedQuery("SampleMovement.findNewestByItemCode") 
		.setParameter("itemCode", itemCode)
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
        .getResultList();
		
		if (sampleMovementNewestList==null || sampleMovementNewestList.size()==0){
			return null;
		}else{
			return sampleMovementNewestList.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public SampleMovement retrieveSampleMovementNewestByItemCodeManuf(String itemCode, String manufCode){
		logger.debug("retrieveSampleMovementNewestByItemCodeManuf" );
		List<SampleMovement> sampleMovementNewestItemCodeManufList = em.createNamedQuery("SampleMovement.findNewestByItemCodeManuf") 
		.setParameter("itemCode", itemCode)
		.setParameter("manufCode", manufCode)
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
	    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
        .getResultList();
		
		if (sampleMovementNewestItemCodeManufList==null || sampleMovementNewestItemCodeManufList.size()==0){
			return null;
		}else{
			return sampleMovementNewestItemCodeManufList.get(0);
		}
	}
	
	public void createSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("createSampleMovement");
		success = false;
		
		em.persist(sampleMovementIn);		

		em.flush();
		success = true;	
	}
	
	public void updateSampleMovement(SampleMovement sampleMovementIn){
		logger.debug("updateSampleMovement");
		success = false;
		
		em.merge(sampleMovementIn);		

		em.flush();
		success = true;	
	}
	
	@SuppressWarnings("unchecked")
	public void createSampleMovementForReverseSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn){       
		SampleMovement sampleMovementSave = new SampleMovement();
		SampleMovement sampleMovementNewestIn = retrieveSampleMovementNewestByItemCode(sampleTestScheduleIn.getSampleItem().getItemCode());
		SampleMovement sampleMovementNewestManufIn = retrieveSampleMovementNewestByItemCodeManuf(sampleTestScheduleIn.getSampleItem().getItemCode(), sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
		
		if(scheduleStatusIn==ScheduleStatus.LabTest){
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getInstitution().getInstitutionCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getRecQty()*-1);
			
			if(sampleMovementNewestIn==null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty((sampleTestScheduleIn.getRecQty()*-1));
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getRecQty()*-1);	
			}else if(sampleMovementNewestIn != null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getRecQty()*-1));
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getRecQty()*-1);	
			}else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getRecQty()*-1));
				sampleMovementSave.setManufOnHandQty(sampleMovementNewestManufIn.getManufOnHandQty() + (sampleTestScheduleIn.getRecQty()*-1));
			}
			
			sampleMovementSave.setAdjustFlag("Y");
			sampleMovementSave.setAdjustReason("Reverse from LT to DS");
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		else if (scheduleStatusIn==ScheduleStatus.TestResult)
		{
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getLab().getLabCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getLabCollectQty());
			
			if(sampleMovementNewestIn==null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getLabCollectQty());
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getLabCollectQty());
			}else if(sampleMovementNewestIn != null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getLabCollectQty());
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getLabCollectQty());
			}else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getLabCollectQty());
				sampleMovementSave.setManufOnHandQty(sampleMovementNewestManufIn.getManufOnHandQty() + sampleTestScheduleIn.getLabCollectQty());
			}
			
			sampleMovementSave.setAdjustFlag("Y");
			sampleMovementSave.setAdjustReason("Reverse from TR to LC");
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
	
		em.persist(sampleMovementSave);
	}
	
	@SuppressWarnings("unchecked")
	public void createSampleMovementForNormalSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn){
		SampleMovement sampleMovementSave = new SampleMovement();
		SampleMovement sampleMovementNewestIn = retrieveSampleMovementNewestByItemCode(sampleTestScheduleIn.getSampleItem().getItemCode());
		SampleMovement sampleMovementNewestManufIn = retrieveSampleMovementNewestByItemCodeManuf(sampleTestScheduleIn.getSampleItem().getItemCode(), sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
		
		if (scheduleStatusIn==ScheduleStatus.DrugSample)
		{ 
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getInstitution().getInstitutionCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getRecQty());

			if(sampleMovementNewestIn ==null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getRecQty());
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getRecQty());
			}else if(sampleMovementNewestIn != null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getRecQty());
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getRecQty());
			}else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + sampleTestScheduleIn.getRecQty());
				sampleMovementSave.setManufOnHandQty(sampleMovementNewestManufIn.getManufOnHandQty() + sampleTestScheduleIn.getRecQty());
			}
			
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		else if (scheduleStatusIn==ScheduleStatus.LabCollection)
		{ 
			sampleMovementSave.setLocationCode(sampleTestScheduleIn.getLab().getLabCode());
			sampleMovementSave.setManufCode(sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode());
			sampleMovementSave.setMovementQty(sampleTestScheduleIn.getLabCollectQty() * -1);
			
			if(sampleMovementNewestIn==null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleTestScheduleIn.getLabCollectQty() * -1);
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getLabCollectQty() *-1);

			}else if(sampleMovementNewestIn != null && sampleMovementNewestManufIn == null){
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getLabCollectQty() * -1));
				sampleMovementSave.setManufOnHandQty(sampleTestScheduleIn.getLabCollectQty() *-1);
			}else{
				sampleMovementSave.setOnHandQty(sampleMovementNewestIn.getOnHandQty() + (sampleTestScheduleIn.getLabCollectQty() * -1));
				sampleMovementSave.setManufOnHandQty(sampleMovementNewestManufIn.getManufOnHandQty() + (sampleTestScheduleIn.getLabCollectQty() * -1));
			}
			
			sampleMovementSave.setSampleTestSchedule(sampleTestScheduleIn);
		}
		
		em.persist(sampleMovementSave);
	}
	

	public boolean isSuccess(){
		return success;
	}
	
	@Remove
	public void destroy() {
		if (sampleMovement !=null){
			sampleMovement = null;
		}
	}
}
