package hk.org.ha.model.pms.dqa.sample.test;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.biz.sample.SampleTestLetterTemplateServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleLetter;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class LetterTemplateTest extends SeamTest {
	@Test
	public void letterTemplateComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //retrieve
	            LetterTemplate letterTemplateFind = (LetterTemplate)invokeMethod("#{letterTemplateManager.retrieveLetterTemplateByCode('LT_CHEM')}");
	            assert (letterTemplateFind!=null);
	            
	            //retrieve List
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);	
	            	
	            invokeMethod("#{letterTemplateListService.retrieveLetterTemplateListByModuleTypeRecordStatus('S',recordStatusIn)}");
		        List<LetterTemplate> letterTemplateListFind = (List<LetterTemplate>)getValue("#{letterTemplateList}");
		        assert letterTemplateListFind.size() >0;
		        
		        //retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria
		        
		        Calendar fromDateCal = Calendar.getInstance();
		        Calendar toDateCal = Calendar.getInstance();
		        
		        fromDateCal.add(Calendar.MONTH, -3);
		        
		        
		        SampleTestEmailEnquiryCriteria sampleTestEmailEnquiryCriteriaIn = new SampleTestEmailEnquiryCriteria();
		        sampleTestEmailEnquiryCriteriaIn.setScheduleStatus(ScheduleStatus.Complete);
		        sampleTestEmailEnquiryCriteriaIn.setScheduleNum("SN99999999");
		        sampleTestEmailEnquiryCriteriaIn.setItemCode("PARA01");
		        sampleTestEmailEnquiryCriteriaIn.setSendAtScheduleStatus(ScheduleStatus.Document);
		        sampleTestEmailEnquiryCriteriaIn.setFromSendDate(fromDateCal.getTime());
		        sampleTestEmailEnquiryCriteriaIn.setToSendDate(toDateCal.getTime());
		        sampleTestEmailEnquiryCriteriaIn.setSupplierCode("3M");
		        sampleTestEmailEnquiryCriteriaIn.setManufCode("3M");
		        sampleTestEmailEnquiryCriteriaIn.setPharmCompanyCode("3M");
		        sampleTestEmailEnquiryCriteriaIn.setInstitutionCode("PYN");
		        sampleTestEmailEnquiryCriteriaIn.setLabCode("CU");
		        
		        Contexts.getSessionContext().set("sampleTestEmailEnquiryCriteriaIn", sampleTestEmailEnquiryCriteriaIn);
		        
		        invokeMethod("#{sampleTestScheduleLetterListService.retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(sampleTestEmailEnquiryCriteriaIn)}");
		        List<SampleTestScheduleLetter> sampleTestScheduleLetterListFind = (List<SampleTestScheduleLetter>)getValue("#{sampleTestScheduleLetterList}");
		        assert sampleTestScheduleLetterListFind ==null;
		        
		        
		        //retrieveLetterTemplateByCode
		        LetterTemplate ltFind = (LetterTemplate)invokeMethod("#{sampleTestLetterTemplateService.retrieveLetterTemplateByCode('LT_MIC')}");
		        assert ltFind!=null;
		        
		        
		        //retrieveLetterTemplateForSchedule
		        Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.LabCollection);
	            
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByRecordStatusScheduleStatus(recordStatusIn,scheduleStatusIn)}");
	            List<SampleTestSchedule> sTSFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId(2)}");
	            SampleTestSchedule sTSFindDType = (SampleTestSchedule) getValue("#{sampleTestSchedule}");
	            
	            invokeMethod("#{userInfoListService.retrieveUserInfoList()}");
	            List<UserInfo> userInfoListFind = (List<UserInfo>)getValue("#{userInfoList}");
	            
	            SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn=new  SupplierPharmCompanyManufacturerContact();
	            supplierPharmCompanyManufacturerContactIn.setCode("3M");
	            supplierPharmCompanyManufacturerContactIn.setName("3M Desc");
	            supplierPharmCompanyManufacturerContactIn.setContactType("Supplier");
	            supplierPharmCompanyManufacturerContactIn.setContact(userInfoListFind.get(1).getContact());
	            
	            Contexts.getSessionContext().set("stsIn", sTSFind.get(0));
	            Contexts.getSessionContext().set("stsDTypeIn", sTSFindDType);
	            Contexts.getSessionContext().set("userInfoIn", userInfoListFind.get(0));
	            Contexts.getSessionContext().set("spcmfcIn", supplierPharmCompanyManufacturerContactIn);
	            
	            SampleTestLetterTemplateServiceLocal sampleTestLetterTemplateService = (SampleTestLetterTemplateServiceLocal)getInstance("sampleTestLetterTemplateService");
	            
	            sampleTestLetterTemplateService.retrieveLetterTemplateForSchedule(null, sTSFind.get(0), userInfoListFind.get(0), supplierPharmCompanyManufacturerContactIn);

	            sampleTestLetterTemplateService.retrieveLetterTemplateForInstitutionAssignment(sTSFind.get(0), supplierPharmCompanyManufacturerContactIn, userInfoListFind.get(0), userInfoListFind.get(0));
	            
	            sampleTestLetterTemplateService.retrieveLetterTemplateForInstitutionAssignment(sTSFindDType, supplierPharmCompanyManufacturerContactIn, userInfoListFind.get(0), userInfoListFind.get(0));
		        
	            List<SampleTestSchedule> stsListFind = new ArrayList<SampleTestSchedule>();
	            stsListFind.add(sTSFind.get(0));
	            Contexts.getSessionContext().set("stsListFindIn", stsListFind);
	            invokeMethod("#{sampleTestLetterTemplateService.retrieveLetterTemplateForLabTest(stsListFindIn, stsIn, userInfoIn, spcmfcIn )}");

	            //createSampleTestScheduleLetter
	            String toMail="totest@test.com";
	            String ccMail="cctest@test.com";
	            String[] toMailList = {"totest@test.com"};
	            String[] ccMailList = {"cctest@test.com"};
	            
	            LetterTemplate letterTemplateIn = new LetterTemplate();
	            letterTemplateIn.setCcList(ccMailList);
	            letterTemplateIn.setCode("Test Code");
	            letterTemplateIn.setContent("Test Content");
	            letterTemplateIn.setModuleType("S");
	            letterTemplateIn.setName("Test Name");
	            letterTemplateIn.setRecordStatus(RecordStatus.Active);
	            letterTemplateIn.setSubject("Test Subject");
	            letterTemplateIn.setToList(toMailList);
	            
	            Contexts.getSessionContext().set("letterTemplateIn", letterTemplateIn);
	            Contexts.getSessionContext().set("stsIn", sTSFind.get(0));
	            Contexts.getSessionContext().set("stsListIn", sTSFind);
	            	            
	            invokeMethod("#{sampleTestScheduleLetterListService.createSampleTestScheduleLetter(letterTemplateIn, null, null, stsIn )}");
	            
	            invokeMethod("#{sampleTestScheduleLetterListService.createSampleTestScheduleLetter(letterTemplateIn, null, stsListIn, null )}");
	            
	            ////////// retrieveLetterTemplateForLabTest
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.LabTest);
	            
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByRecordStatusScheduleStatus(recordStatusIn,scheduleStatusIn)}");
	            sTSFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            
	          //retrieveLetterTemplateForLabTest
	            stsListFind = new ArrayList<SampleTestSchedule>();
	            stsListFind.add(sTSFind.get(0));
	            Contexts.getSessionContext().set("stsListFindIn", stsListFind);
	            invokeMethod("#{sampleTestLetterTemplateService.retrieveLetterTemplateForLabTest(stsListFindIn, stsIn, userInfoIn, spcmfcIn )}");
	            
	       }
		}.run();
	}

}

