package hk.org.ha.model.pms.dqa.suppperf.test;

import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemHeader;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.persistence.suppperf.SendCollectSample;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemBy;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemRpt;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class PharmProblemTest extends SeamTest {
	@Test
	public void PharmProblemComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//Login
	            invokeMethod("#{identity.login}");
			
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("31/12/2012");
	            Date problemDate = dfm.parse("1/12/2011");
	            
	            
	            //add PharmProblem
	            invokeMethod("#{pharmProblemService.addPharmProblem()}");
	            
	            //create PharmProblem
	            ProblemHeader problemHeaderIn= new ProblemHeader();
				Contract contractIn= new Contract();
				Supplier supplierIn= new Supplier();
				Company manufacturerIn= new Company();
				Company pharmCompanyIn= new Company();
				List<PharmProblemNature> pharmProblemNatureListIn = new ArrayList<PharmProblemNature>();
				List<String> pharmBatchNumListIn = new ArrayList<String>();
				List<String> countryListIn = new ArrayList<String>();
				PharmProblemNature pharmProblemNatureIn= new PharmProblemNature();
				SendCollectSample sendCollectSampleIn= new SendCollectSample();
								PharmProblem pharmProblemIn= new PharmProblem();
				Institution institutionIn = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('PYN')}");
				
				supplierIn.setSupplierCode("ZUEL");
				manufacturerIn.setCompanyCode("3M");
				pharmCompanyIn.setCompanyCode("3M");
				
				contractIn.setContractNum("HOC100-36-1");
				contractIn.setContractSuffix("");
				contractIn.setContractType("C");
				contractIn.setModuleType(ModuleType.SuppPerf);
				contractIn.setItemCode("PARA01");
				contractIn.setSupplier(supplierIn);
				contractIn.setManufacturer(manufacturerIn);
				contractIn.setPharmCompany(pharmCompanyIn);
				
				problemHeaderIn.setItemCode("PARA01");
				problemHeaderIn.setOrderType(OrderTypeAll.Contract);
				
				sendCollectSampleIn=null;
				
				pharmProblemIn.setAffectQty(100);
				pharmProblemIn.setRecordStatus(RecordStatus.Active);
				pharmProblemIn.setProblemByName("NAME1");
				pharmProblemIn.setProblemByRank("RANK1");
				pharmProblemIn.setProblemByType(ProblemBy.Pharmacy);
				pharmProblemIn.setProblemByTypeDesc(ProblemBy.Pharmacy.getDisplayValue());
				pharmProblemIn.setProblemDate(problemDate);
				pharmProblemIn.setCoordinatorName("NAEM1");
				pharmProblemIn.setCoordinatorRank(Rank.P);
				pharmProblemIn.setCoordinatorPhone("12345678");
				pharmProblemIn.setProblemDetail("Test Detail");
				pharmProblemIn.setInstitution(institutionIn);
				pharmProblemIn.setSendCollectSample(sendCollectSampleIn);
				pharmProblemIn.setProblemHeader(problemHeaderIn);
				pharmProblemIn.setContract(contractIn);
				
				//select ProblemNatureSubCat
				ProblemNatureSubCat problemNatureSubCatIn = new ProblemNatureSubCat();
				problemNatureSubCatIn.setProblemNatureSubCatId(Long.valueOf(1));
				Contexts.getSessionContext().set("problemNatureSubCatIn", problemNatureSubCatIn);
	            invokeMethod("#{problemNatureSubCatService.retrieveProblemNatureSubCatByProblemNatureSubCatId(problemNatureSubCatIn)}");
	            problemNatureSubCatIn = (ProblemNatureSubCat)getValue("#{problemNatureSubCat}");
	            
				pharmProblemNatureIn = new PharmProblemNature();
				pharmProblemNatureIn.setProblemNatureSubCat(problemNatureSubCatIn);
				pharmProblemNatureIn.setPharmProblem(pharmProblemIn);
				pharmProblemNatureIn.setRecordStatus(RecordStatus.Active);
				pharmProblemNatureListIn.add(pharmProblemNatureIn);
				
				pharmBatchNumListIn.add("BATCH-TEST-1");
				countryListIn.add("HK"); 
	            
	            
				Contexts.getSessionContext().set("pharmProblemIn", pharmProblemIn);
				Contexts.getSessionContext().set("pharmBatchNumList", pharmBatchNumListIn);
				Contexts.getSessionContext().set("countryList", countryListIn);
				Contexts.getSessionContext().set("pharmProblemNatureList", pharmProblemNatureListIn);
				Contexts.getSessionContext().set("pharmProblemFileUploadDataList", null);
				Contexts.getSessionContext().set("sendEmailBoolean", false);
		        invokeMethod("#{pharmProblemService.createPharmProblem(pharmProblemIn, pharmBatchNumList, countryList, pharmProblemNatureList, pharmProblemFileUploadDataList, sendEmailBoolean)}");
		            
				
		        ///////////////////////
		        //////retrievePharmProblemListWithNullProblemStatus
		        //////////////////////////////
		        invokeMethod("#{pharmProblemNewListService.retrievePharmProblemListWithNullProblemStatus()}");
	            assert getValue("#{pharmProblemNewList!=null}").equals(true);

	            
	            // retrieve by criteria
	            PharmProblemCriteria pharmProblemCriteriaIn = new PharmProblemCriteria();
	            pharmProblemCriteriaIn.setFromProblemDate(fromDate);
	            pharmProblemCriteriaIn.setToProblemDate(toDate);
	            pharmProblemCriteriaIn.setInstitution(institutionIn);
	            pharmProblemCriteriaIn.setItemCode("PARA01");
	            pharmProblemCriteriaIn.setOrderType(OrderTypeAll.Contract);
	            pharmProblemCriteriaIn.setSupplierCode("ZUEL");
	            pharmProblemCriteriaIn.setManufCode("3M");
	            pharmProblemCriteriaIn.setPharmCompanyCode("3M");
	            
	            Contexts.getSessionContext().set("pharmProblemCriteriaIn", pharmProblemCriteriaIn);
	            invokeMethod("#{pharmProblemListService.retrievePharmProblemListByPharmProblemCriteria(pharmProblemCriteriaIn)}");
	            		
	            assert getValue("#{pharmProblemList!=null}").equals(true);
	            
	            List<PharmProblem> pharmProblemListFind = (List<PharmProblem>)getValue("#{pharmProblemList}");
	            PharmProblem pharmProblemFind;
	            if(pharmProblemListFind!=null)
	            {
	            	pharmProblemFind = pharmProblemListFind.get(0);
	            	
	            	//retrieve by pharmProblem
	            	Contexts.getSessionContext().set("pharmProblemFind", pharmProblemFind);
	 	            invokeMethod("#{pharmProblemService.retrievePharmProblemByPharmProblem(pharmProblemFind)}");
	            	
	 	            assert getValue("#{pharmProblem!=null}").equals(true);
	 	            
	 	            
	 	            //delete
	 	           Contexts.getSessionContext().set("pharmProblemFind", pharmProblemFind);
	 	           Contexts.getSessionContext().set("deleteReason", "Test Delete");
	 	           invokeMethod("#{pharmProblemService.deletePharmProblem(pharmProblemFind, deleteReason)}");
	            }
	            
	            //////////////////////////////////////////////////////////
	            ///////////////////////////////////////////////////////////
	            PharmProblemRpt pharmProblemRptInit = new PharmProblemRpt();
	            pharmProblemRptInit.setAffectQty("");
	            pharmProblemRptInit.setBatchNum("");
	            pharmProblemRptInit.setCaseNum("");
	            pharmProblemRptInit.setProblemByName("");
	            pharmProblemRptInit.setProblemByRank("");
	            pharmProblemRptInit.setProblemByTypeDesc("");
	            pharmProblemRptInit.setProblemDate("");
	            pharmProblemRptInit.setProblemDetail("");
	            pharmProblemRptInit.setProblemNum("");
	            pharmProblemRptInit.setProblemStatus("");
	            pharmProblemRptInit.setContractNum("");
	            pharmProblemRptInit.setCoordinatorName("");
	            pharmProblemRptInit.setCoordinatorPhone("");
	            pharmProblemRptInit.setCoordinatorRank("");
	            pharmProblemRptInit.setCountry("");
	            pharmProblemRptInit.setCreateDate("");
	            pharmProblemRptInit.setInstitutionCode("");
	            pharmProblemRptInit.setItemCode("");
	            pharmProblemRptInit.setManufCode("");
	            pharmProblemRptInit.setNatureOfProblem("");
	            pharmProblemRptInit.setOrderType("");
	            pharmProblemRptInit.setPharmCompanyCode("");
	            pharmProblemRptInit.setPharmSendMethod("");
	            pharmProblemRptInit.setQaCollectDate("");
	            pharmProblemRptInit.setSampleQty("");
	            pharmProblemRptInit.setSendToQaCode("");
	            pharmProblemRptInit.setSendToSuppCode("");
	            pharmProblemRptInit.setSendToType("");
	            pharmProblemRptInit.setSuppCollectDate("");
	            pharmProblemRptInit.setSupplierCode("");
	            
	            pharmProblemRptInit.getAffectQty();
	            pharmProblemRptInit.getBatchNum();
	            pharmProblemRptInit.getCaseNum();
	            pharmProblemRptInit.getProblemByName();
	            pharmProblemRptInit.getProblemByRank();
	            pharmProblemRptInit.getProblemByTypeDesc();
	            pharmProblemRptInit.getProblemDate();
	            pharmProblemRptInit.getProblemDetail();
	            pharmProblemRptInit.getProblemNum();
	            pharmProblemRptInit.getProblemStatus();
	            pharmProblemRptInit.getContractNum();
	            pharmProblemRptInit.getCoordinatorName();
	            pharmProblemRptInit.getCoordinatorPhone();
	            pharmProblemRptInit.getCoordinatorRank();
	            pharmProblemRptInit.getCountry();
	            pharmProblemRptInit.getCreateDate();
	            pharmProblemRptInit.getInstitutionCode();
	            pharmProblemRptInit.getItemCode();
	            pharmProblemRptInit.getManufCode();
	            pharmProblemRptInit.getNatureOfProblem();
	            pharmProblemRptInit.getOrderType();
	            pharmProblemRptInit.getPharmCompanyCode();
	            pharmProblemRptInit.getPharmSendMethod();
	            pharmProblemRptInit.getQaCollectDate();
	            pharmProblemRptInit.getSampleQty();
	            pharmProblemRptInit.getSendToQaCode();
	            pharmProblemRptInit.getSendToSuppCode();
	            pharmProblemRptInit.getSendToType();
	            pharmProblemRptInit.getSuppCollectDate();
	            pharmProblemRptInit.getSupplierCode();
	            
	       }
		}.run();
	}

}
