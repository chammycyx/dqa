package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class RiskLevelMaintTest extends SeamTest {
	@Test
	public void riskLevelMaintComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve 
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            
	            invokeMethod("#{riskLevelListService.retrieveRiskLevelListByRecordStatus(recordStatusIn)}");
	            
	            List<RiskLevel> riskLevelListFind = (List<RiskLevel>)getValue("#{riskLevelList}");
	            assert riskLevelListFind.size() >0;
	            
	            //Add	
	            
	            invokeMethod("#{riskLevelService.addRiskLevel()}");
	            
	            RiskLevel riskLevelAdd = new RiskLevel();
	            riskLevelAdd.setRiskLevelCode("XXX");
	            riskLevelAdd.setRiskLevelDesc("XXX DESC");
	            riskLevelAdd.setProceedSampleTestFlag(YesNoFlag.Yes);
	            riskLevelAdd.setRecordStatus(RecordStatus.Active);

	            Contexts.getSessionContext().set("riskLevel", riskLevelAdd);
	            invokeMethod("#{riskLevelService.createRiskLevel()}");
	            
	            invokeMethod("#{riskLevelService.retrieveRiskLevelByRiskLevelCode('XXX')}");
	            
	            assert getValue("#{riskLevel.riskLevelCode}").equals("XXX");
	            
	            //Update
	           
	            RiskLevel riskLevelUpdate = (RiskLevel)getValue("#{riskLevel}");
	            riskLevelUpdate.setRiskLevelDesc("YYY DESC");
	            Contexts.getSessionContext().set("riskLevelIn", riskLevelUpdate);
	            
	            invokeMethod("#{riskLevelService.updateRiskLevel(riskLevelIn)}");
	            
	            invokeMethod("#{riskLevelService.retrieveRiskLevelByRiskLevelCode('XXX')}");
	            assert getValue("#{riskLevel.riskLevelDesc}").equals("YYY DESC");
	            
	            
	            //Delete
	            
	            RiskLevel riskLevelDelete = (RiskLevel)getValue("#{riskLevel}");
	            Contexts.getSessionContext().set("riskLevelIn", riskLevelDelete);
	            
	            invokeMethod("#{riskLevelService.deleteRiskLevel(riskLevelIn)}");
	            
	            invokeMethod("#{riskLevelService.retrieveRiskLevelByRiskLevelCode('XXX')}");
	            assert getValue("#{riskLevel.recordStatus}").equals(RecordStatus.Deleted);
	         
			}
		}.run();
	}

}
