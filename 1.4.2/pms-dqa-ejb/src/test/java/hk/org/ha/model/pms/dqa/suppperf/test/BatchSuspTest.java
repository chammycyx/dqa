package hk.org.ha.model.pms.dqa.suppperf.test;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class BatchSuspTest extends SeamTest {
	@Test
	public void BatchSuspComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//Login
	            invokeMethod("#{identity.login}");
			
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("31/12/2012");
	            
	            Institution institutionIn = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('PHQ')}");
	            
	            BatchSuspCriteria batchSuspCriteriaIn = new BatchSuspCriteria();
	            batchSuspCriteriaIn.setCaseNum("CASE1");
	            batchSuspCriteriaIn.setPcu(institutionIn);
	            batchSuspCriteriaIn.setInstitution(institutionIn);
	            
	            // retrieve by criteria
	            Contexts.getSessionContext().set("batchSuspCriteriaIn", batchSuspCriteriaIn);
	            invokeMethod("#{batchSuspListService.retrieveBatchSuspListByBatchSuspCriteria(batchSuspCriteriaIn)}");
	            		
	            assert getValue("#{batchSuspList==null}").equals(true);
	            
	            // retrieve by batchSusp
	            BatchSusp batchSuspIn = new BatchSusp();
	            batchSuspIn.setBatchSuspId(Long.valueOf(1));
	            Contexts.getSessionContext().set("batchSuspIn", batchSuspIn);
	            invokeMethod("#{batchSuspService.retrieveBatchSuspByBatchSusp(batchSuspIn)}");
	            
	            assert getValue("#{batchSusp==null}").equals(true);
	            
	            
	            
	       }
		}.run();
	}

}
