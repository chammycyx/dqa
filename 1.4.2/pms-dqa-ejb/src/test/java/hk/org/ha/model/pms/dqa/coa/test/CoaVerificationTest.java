package hk.org.ha.model.pms.dqa.coa.test;

import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchListServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaChecklistKey;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyCriteria;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaVerificationTest extends SeamTest {
	@Test
	public void testCoaVerificationComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception
			{
				// Login
	            invokeMethod("#{identity.login}");
				
	            // Retrieve 
	            CoaBatchListServiceLocal coaBatchListService = (CoaBatchListServiceLocal)getInstance("coaBatchListService");
	            List<CoaBatch> coaBatchListForCoaVerification = coaBatchListService.retrieveCoaBatchListByCoaStatus(CoaStatus.VerificationInProgress);
	            assert coaBatchListForCoaVerification.size() > 0;
	            
	            invokeMethod("#{coaChecklistKeyListService.retrieveCoaChecklistKeyList}");
	            List<CoaChecklistKey> coaChecklistKeyList = (List<CoaChecklistKey>) getValue("#{coaChecklistKeyList}");
	            assert coaChecklistKeyList.size() > 0;
	            
	            invokeMethod("#{coaDiscrepancyKeyListService.retrieveCoaDiscrepancyKeyList}");
	            List<CoaDiscrepancyKey> coaDiscrepancyKeyList = (List<CoaDiscrepancyKey>) getValue("#{coaDiscrepancyKeyList}");
	            assert ((List<CoaDiscrepancyKey>)getValue("#{coaDiscrepancyKeyList}")).size() > 0;	            
	            	            	            
	            //==============Verify COA===============
	            Long coaBatchIdForVerify = 5L;         
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForVerify + "')}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForVerify);	            
	            
	            //View COA
	            invokeMethod("#{coaFileFolderListService.retrieveCoaFileFolderListByCoaBatch(coaBatch)}");
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderOrgCoaList}")).size() >= 0;
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderBatchCoaList}")).size() >= 0;
	            
	            //Pass
	            invokeMethod("#{coaBatchService.updateCoaBatchStatus(coaBatch, '" + CoaStatus.Pass + "', '" + DiscrepancyStatus.NullValue + "')}");
	            
	            //===============Report Discrepancy===============	            
	            Long coaBatchIdForDiscrepancy = 7L;
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            
	            //View COA
	            invokeMethod("#{coaFileFolderListService.retrieveCoaFileFolderListByCoaBatch(coaBatch)}");
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderOrgCoaList}")).size() > 0;
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderBatchCoaList}")).size() > 0;	            	            	            
	            
	            //Group Child discrepancy to parent discrepancy
	            CoaDiscrepancyCriteria coaDiscrepancyCriteria = new CoaDiscrepancyCriteria();
	            
	            coaDiscrepancyCriteria.setCoaStatus( CoaStatus.DiscrepancyClarificationInProgress );
	            coaDiscrepancyCriteria.setContractNum( coaBatch.getCoaItem().getContract().getContractNum() );
	            coaDiscrepancyCriteria.setItemCode( coaBatch.getCoaItem().getContract().getItemCode() );
	            coaDiscrepancyCriteria.setOrderType( coaBatch.getCoaItem().getOrderType() );
	            coaDiscrepancyCriteria.setSupplierCode( coaBatch.getCoaItem().getContract().getSupplier().getSupplierCode() );
	            Contexts.getSessionContext().set("coaDiscrepancyCriteria", coaDiscrepancyCriteria);
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListForReportDiscrepancy(coaDiscrepancyCriteria)}");
	            List<CoaDiscrepancy> coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() >= 0;
	            
	            //Insert COA Discrepancy
	            for(int i=0; i<coaDiscrepancyKeyList.size(); i++) {
	            	((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setEnable(true);        	
	            }
	            String discrepancyRemark = coaBatch.getBatchNum()+":Test Remark";
	            coaBatch.setDiscrepancyRemark(discrepancyRemark);	            
	            invokeMethod("#{coaDiscrepancyListService.createCoaDiscrepancyList(coaBatch, coaDiscrepancyKeyList)}");
	            
	            //retrieve parent CoaBatch
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            CoaBatch parentCoaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);	            
	            
	            coaBatchIdForDiscrepancy = 8L;
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            	            	            
	            //View COA
	            invokeMethod("#{coaFileFolderListService.retrieveCoaFileFolderListByCoaBatch(coaBatch)}");
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderOrgCoaList}")).size() > 0;
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderBatchCoaList}")).size() > 0;
	            
	            //Group Child discrepancy to parent discrepancy
	            coaDiscrepancyCriteria = new CoaDiscrepancyCriteria();
	            coaDiscrepancyCriteria.setCoaStatus( CoaStatus.DiscrepancyClarificationInProgress );
	            coaDiscrepancyCriteria.setContractNum( coaBatch.getCoaItem().getContract().getContractNum() );
	            coaDiscrepancyCriteria.setItemCode( coaBatch.getCoaItem().getContract().getItemCode() );
	            coaDiscrepancyCriteria.setOrderType( coaBatch.getCoaItem().getOrderType() );
	            coaDiscrepancyCriteria.setSupplierCode( coaBatch.getCoaItem().getContract().getSupplier().getSupplierCode() );	 
	            Contexts.getSessionContext().set("coaDiscrepancyCriteria", coaDiscrepancyCriteria);
	            
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListForReportDiscrepancy(coaDiscrepancyCriteria)}");
	            coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() >= 0;
	            
	            boolean hasPassed = false;
	            
	            for( CoaDiscrepancy cd:coaDiscrepancyList ) {
	            	if(cd.getPassFlag() == PassFlag.Passed) {
	            		hasPassed = true;
	            		break;
	            	}
	            }
	            
	            assert hasPassed == false;
	            
	            //Insert COA Discrepancy
	            List<CoaDiscrepancy> parentCoaDiscrepancyList = new ArrayList<CoaDiscrepancy>();
	            for( CoaDiscrepancy parentCoaDiscrepancy : coaDiscrepancyList ) {
	            	if( parentCoaDiscrepancy.getCoaBatch().getCoaBatchId().equals( parentCoaBatch.getCoaBatchId() ) ) {
	            		parentCoaDiscrepancyList.add(parentCoaDiscrepancy);
	            	}
	            }
	            
	            for(int i=0; i<coaDiscrepancyKeyList.size(); i++) {
	            	if(i < 2){
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setEnable(true);
	            		for( CoaDiscrepancy parentCoaDiscrepancy : parentCoaDiscrepancyList ) {
	            			if( ((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).getDiscrepancyId().equals( parentCoaDiscrepancy.getCoaDiscrepancyKey().getDiscrepancyId() ) ) {	            				
	            				((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setParentCoaDiscrepancy( parentCoaDiscrepancy );
	            			}	            			
	            		}	            		
	            	} else {
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setEnable(false);
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setParentCoaDiscrepancy(null);
	            	}
	            }	            	            	            
	            
	            discrepancyRemark = coaBatch.getBatchNum()+":Test Remark";
	            coaBatch.setDiscrepancyRemark(discrepancyRemark);	            
	            invokeMethod("#{coaDiscrepancyListService.createCoaDiscrepancyList(coaBatch, coaDiscrepancyKeyList)}");
	            
	            coaBatchIdForDiscrepancy = 9L;
	            invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId('" + coaBatchIdForDiscrepancy + "')}");
	            coaBatch = (CoaBatch) getValue("#{coaBatch}");
	            assert getValue("#{coaBatch.coaBatchId}").equals(coaBatchIdForDiscrepancy);
	            
	            //View COA
	            invokeMethod("#{coaFileFolderListService.retrieveCoaFileFolderListByCoaBatch(coaBatch)}");
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderOrgCoaList}")).size() > 0;
	            assert ((List<CoaFileFolder>)getValue("#{coaFileFolderBatchCoaList}")).size() >= 0;
	            
	            //Group Child discrepancy to parent discrepancy
	            coaDiscrepancyCriteria = new CoaDiscrepancyCriteria();
	            coaDiscrepancyCriteria.setCoaStatus( CoaStatus.DiscrepancyClarificationInProgress );
	            coaDiscrepancyCriteria.setContractNum( coaBatch.getCoaItem().getContract().getContractNum() );
	            coaDiscrepancyCriteria.setItemCode( coaBatch.getCoaItem().getContract().getItemCode() );
	            coaDiscrepancyCriteria.setOrderType( coaBatch.getCoaItem().getOrderType() );
	            coaDiscrepancyCriteria.setSupplierCode( coaBatch.getCoaItem().getContract().getSupplier().getSupplierCode() );	           
	            Contexts.getSessionContext().set("coaDiscrepancyCriteria", coaDiscrepancyCriteria);
	            invokeMethod("#{coaDiscrepancyListService.retrieveCoaDiscrepancyListForReportDiscrepancy(coaDiscrepancyCriteria)}");
	            coaDiscrepancyList = (List<CoaDiscrepancy>) getValue("#{coaDiscrepancyList}");
	            assert coaDiscrepancyList.size() > 0;	            
	            
	            //Insert COA Discrepancy
	            parentCoaDiscrepancyList = new ArrayList<CoaDiscrepancy>();
	            for( CoaDiscrepancy parentCoaDiscrepancy : coaDiscrepancyList ) {
	            	if( parentCoaDiscrepancy.getCoaBatch().getCoaBatchId().equals( parentCoaBatch.getCoaBatchId() ) ) {
	            		parentCoaDiscrepancyList.add(parentCoaDiscrepancy);
	            	}
	            }
	            
	            for(int i=0; i<coaDiscrepancyKeyList.size(); i++) {
	            	if(i == 0 || i == 2){
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setEnable(true);
	            		for( CoaDiscrepancy parentCoaDiscrepancy : parentCoaDiscrepancyList ) {
	            			if( ((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).getDiscrepancyId().equals( parentCoaDiscrepancy.getCoaDiscrepancyKey().getDiscrepancyId() ) ) {	            				
	            				((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setParentCoaDiscrepancy( parentCoaDiscrepancy );
	            			}	            			
	            		}
	            	} else {
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setEnable(false);
	            		((CoaDiscrepancyKey)coaDiscrepancyKeyList.get(i)).setParentCoaDiscrepancy(null);
	            	}
	            }	 

	            discrepancyRemark = coaBatch.getBatchNum()+":Test Remark";
	            coaBatch.setDiscrepancyRemark(discrepancyRemark);	            
	            invokeMethod("#{coaDiscrepancyListService.createCoaDiscrepancyList(coaBatch, coaDiscrepancyKeyList)}");
	            
	          //==============Verify COA By Incoming Email===============
	            Long coaItemIdForVerify = 4L;
	            String coaBatchNumForVerify = "837893";
	            CoaBatch result = (CoaBatch) invokeMethod("#{coaBatchManager.retrieveCoaBatchByBatchNumCoaItemId('" + coaBatchNumForVerify + "', '" + coaItemIdForVerify + "')}");
	            assert result.getBatchNum().equals(coaBatchNumForVerify);	     
	            //Pass
	            invokeMethod("#{coaBatchService.updateCoaBatchStatus(coaBatch, '" + CoaStatus.Pass + "', '" + DiscrepancyStatus.NullValue + "')}");
	            
			}
		}.run();		
	}
}
