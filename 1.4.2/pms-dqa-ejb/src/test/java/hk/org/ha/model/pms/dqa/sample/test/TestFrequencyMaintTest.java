package hk.org.ha.model.pms.dqa.sample.test;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.Frequency;
import hk.org.ha.model.pms.dqa.vo.coa.TestFrequencyCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class TestFrequencyMaintTest extends SeamTest {
	@Test
	public void testFrequencyMaintComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
	            
	            TestFrequency tf = new TestFrequency();
	            invokeMethod("#{riskLevelService.retrieveRiskLevelByRiskLevelCode('BIO')}");
	            RiskLevel rk = (RiskLevel)getValue("#{riskLevel}");
	            
	            SampleTest st = new SampleTest();
	            st.setTestCode("MICRO");
	            // case 1
	            tf.setOrderType(OrderType.Contract.getDataValue());
	            tf.setSampleTest(st);
	            tf.setRiskLevel(rk);
	            tf.setRecordStatus(RecordStatus.Active);
	            tf.setFrequencyPerYear(Frequency.Twelve);
	            tf.setPercentage(10);
	            
	            Contexts.getSessionContext().set("testFrequency", tf);
	            invokeMethod("#{testFrequencyService.addTestFrequency}");
	            
	            TestFrequency tfIn = new TestFrequency();
	            tfIn.setFrequencyId(1L);
	            Contexts.getSessionContext().set("testFrequencyIn", tfIn);
	            invokeMethod("#{testFrequencyService.retrieveTestFrequencyByTestFrequency(testFrequencyIn)}");
	            
	            TestFrequency tfFind = (TestFrequency)getValue("#{testFrequency}");
	            
	            tfFind.setFrequencyPerYear(Frequency.Six);
	            tfFind.setPercentage(50);
	            
	            invokeMethod("#{testFrequencyService.updateTestFrequency()}");
	            
	            
	            Contexts.getSessionContext().set("recordStatus", RecordStatus.Active);
	            invokeMethod("#{testFrequencyListService.retrieveTestFrequencyListByRecordStatus(recordStatus)}");
	            
	            // case 2
	            tf.setOrderType(OrderType.Contract.getDataValue());
	            tf.setSampleTest(st);
	            tf.setRiskLevel(null);
	            tf.setRecordStatus(RecordStatus.Active);
	            tf.setFrequencyPerYear(Frequency.Twelve);
	            tf.setPercentage(10);

	            st.setTestCode("CHEM");
	            tf.setSampleTest(st);
//	            tf.setOrderType(OrderType.DirectPurchase.getDataValue());
	            tf.setOrderType(null);
	            tf.setRiskLevel(rk);
	            tf.setFrequencyPerYear(Frequency.One);
	            tf.setPercentage(100);

System.out.println("add test freq");	            
	            Contexts.getSessionContext().set("testFrequency", tf);
	            invokeMethod("#{testFrequencyService.createTestFrequency()}");
	            assert getValue("#{testFrequencyService.success}").equals(true);
	            
System.out.println("add test freq 2");	            
	            st.setTestCode("MICRO");
	            tf.setSampleTest(st);
	            tf.setOrderType(OrderType.DirectPurchase.getDataValue());
	            tf.setRiskLevel(null);
	            tf.setFrequencyPerYear(Frequency.One);
	            tf.setPercentage(100);
	            
	            Contexts.getSessionContext().set("testFrequency", tf);
	            invokeMethod("#{testFrequencyService.createTestFrequency()}");
	            assert getValue("#{testFrequencyService.success}").equals(false);
	            
	            // retrieve test frequency with 100%
	            Contexts.getSessionContext().set("orderTypeIn", OrderType.Contract);
	            List<TestFrequency> testFrequencyListFind = (List<TestFrequency>)invokeMethod("#{testFrequencyListService.retrieveTestFrequencyListByTestCodeOrderTypeFullPercentage('MICRO', orderTypeIn, true)}");
	            assert testFrequencyListFind.size()>0;
	            
	            //deleteTestFrequency
	            //invokeMethod("#{testFrequencyService.deleteTestFrequency(testFrequency)}");

	            TestFrequencyCriteria testFrequencyCriteriaInit = new TestFrequencyCriteria();
	            testFrequencyCriteriaInit.setOrderType("");
	            testFrequencyCriteriaInit.setRiskLevelCode("");
	            testFrequencyCriteriaInit.setTestCode("");
	            testFrequencyCriteriaInit.setRecordStatus(RecordStatus.Active);
	            
	            testFrequencyCriteriaInit.getOrderType();
	            testFrequencyCriteriaInit.getRiskLevelCode();
	            testFrequencyCriteriaInit.getTestCode();
	            testFrequencyCriteriaInit.getRecordStatus();
	            
	            	
	 
			}
		}.run();
	}
}

