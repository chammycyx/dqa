package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.IAFaxMemoRpt;
import hk.org.ha.model.pms.dqa.vo.sample.IAMemoContentVo;
import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class InstitutionAssignmentDetailTest extends SeamTest {
	@Test
	public void institutionAssignmentDetailComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve All Schedule Record with schedule status = "IA"
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.InstitutionAssignment);
	            
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByRecordStatusScheduleStatus(recordStatusIn,scheduleStatusIn)}");
	            
	            List<SampleTestSchedule> sTSFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            assert sTSFind.size() >0;
	            

	            SampleTestSchedule sampleTestScheduleUpdate = sTSFind.get(0);
	            SampleTestSchedule sampleTestScheduleConfirm = sTSFind.get(1);
	            SampleTestSchedule sampleTestScheduleDelete = sTSFind.get(2);
	            SampleTestSchedule sampleTestScheduleReverse = sTSFind.get(3);
	            
	            //Update
	        	
	            Institution institutionFind = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('PYN')}");
	            
	            sampleTestScheduleUpdate.setInstitution(institutionFind);
	           
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleUpdate);
	            
	            invokeMethod("#{sampleTestScheduleService.updateSampleTestScheduleForInstAssignment(sampleTestScheduleIn,'Save')}");

	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleUpdate.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.institution.institutionCode}").equals("PYN");
	            
	            
	            //Confirm
	
	           Institution institutionFind2 = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('TMH')}");
	            
	            sampleTestScheduleConfirm.setInstitution(institutionFind2);
	            
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleConfirm);
	            
	            invokeMethod("#{sampleTestScheduleService.updateSampleTestScheduleForInstAssignment(sampleTestScheduleIn,'Confirm')}");

	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleConfirm.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.institution.institutionCode}").equals("TMH");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.DrugSample);
	            
	            //Delete
	            Contexts.getSessionContext().set("deleteReasonIn", CancelSchedReason.ReferenceStandardNotAvailable);
	            Contexts.getSessionContext().set("deleteSampleTestScheduleIn", sampleTestScheduleDelete);
	            
	            invokeMethod("#{sampleTestScheduleService.deleteSampleTestSchedule(deleteSampleTestScheduleIn,deleteReasonIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleDelete.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.recordStatus}").equals(RecordStatus.Deleted);
	            
	            //Reverse
	            
	            Contexts.getSessionContext().set("reverseSampleTestScheduleIn", sampleTestScheduleReverse);
	            
	            invokeMethod("#{sampleTestScheduleService.reverseSampleTestScheduleStatus(reverseSampleTestScheduleIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleReverse.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.Document);
	            
	            //institution assignment
	            InstAssignmentCriteria instAssignmentCriteriaFind = new InstAssignmentCriteria();
	            instAssignmentCriteriaFind.setOrderType(OrderType.Contract);
	            instAssignmentCriteriaFind.setTestCode("MICRO");
	            
	            Contexts.getSessionContext().set("instAssignmentCriteriaIn", instAssignmentCriteriaFind);
	            
	            invokeMethod("#{sampleTestScheduleListInstAssignCriteriaService.retrieveSampleTestScheduleListForInstAssignByCriteria(instAssignmentCriteriaIn)}");
	            
	            List<SampleTestSchedule> sTSIAFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleInstAssignCriteriaList}");
	            
	            assert sTSIAFind.size() >0;
	            
	            //institution assignment popup update
	            SampleTestSchedule sampleTestSchedulePopupUpdate = sTSIAFind.get(0);
	            
	            Institution institutionPopupFind = (Institution)invokeMethod("#{institutionService.retrieveInstitutionForPhs('TMH')}");
	            
	            sampleTestSchedulePopupUpdate.setInstitution(institutionPopupFind);
	           
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestSchedulePopupUpdate);
	            Contexts.getSessionContext().set("sampleTestScheduleListIn", sTSIAFind);
	            
	            invokeMethod("#{sampleTestScheduleListInstAssignCriteriaService.updateSampleTestScheduleForInstAssignmentDetailPopup(sampleTestScheduleIn,'Save',sampleTestScheduleListIn)}");

	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestSchedulePopupUpdate.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.institution.institutionCode}").equals("TMH");
	            
	            //institution assignment popup update List
	            sTSIAFind.remove(0);
	            for(SampleTestSchedule sts: sTSIAFind)
	            {
	            	sts.setInstitution(institutionPopupFind);
	            }
	            
	            Contexts.getSessionContext().set("sampleTestScheduleListIn", sTSIAFind);
	            
	            invokeMethod("#{sampleTestScheduleListService.updateSampleTestScheduleListForInstAssignment(sampleTestScheduleListIn)}");
	            
	            ///////////////////////////////////
	            IAMemoContentVo iAMemoContentVoIn = new IAMemoContentVo();
	            iAMemoContentVoIn.setRefNum("");
	            iAMemoContentVoIn.setFaxMemoParagraph1("");
	            iAMemoContentVoIn.setFaxMemoParagraph2("");
	            iAMemoContentVoIn.setFaxMemoParagraph3("");
	            iAMemoContentVoIn.setFaxMemoParagraph4("");
	            iAMemoContentVoIn.setFaxMemoParagraphOther("");
	            
	            iAMemoContentVoIn.getRefNum();
	            iAMemoContentVoIn.getFaxMemoParagraph1();
	            iAMemoContentVoIn.getFaxMemoParagraph2();
	            iAMemoContentVoIn.getFaxMemoParagraph3();
	            iAMemoContentVoIn.getFaxMemoParagraph4();
	            iAMemoContentVoIn.getFaxMemoParagraphOther();
	            
	            ///////////////////////////////////
	            IAFaxMemoRpt iAFaxMemoRptIn = new IAFaxMemoRpt();
	            iAFaxMemoRptIn.setRef("");
	            iAFaxMemoRptIn.setTel("");
	            iAFaxMemoRptIn.setFax("");
	            iAFaxMemoRptIn.setDate("");
	            iAFaxMemoRptIn.setTo("");
	            iAFaxMemoRptIn.setAttn("");
	            iAFaxMemoRptIn.setItemDesc("");
	            iAFaxMemoRptIn.setManuf("");
	            iAFaxMemoRptIn.setSupplier("");
	            iAFaxMemoRptIn.setParagraph1("");
	            iAFaxMemoRptIn.setParagraph2("");
	            iAFaxMemoRptIn.setParagraph3("");
	            iAFaxMemoRptIn.setParagraph4("");
	            iAFaxMemoRptIn.setParagraphOther("");
	            iAFaxMemoRptIn.setSender("");
	            iAFaxMemoRptIn.setP2No("");
	            iAFaxMemoRptIn.setP3No("");
	            iAFaxMemoRptIn.setP4No("");
	            iAFaxMemoRptIn.setpOthNo("");
	            
	            iAFaxMemoRptIn.getRef();
	            iAFaxMemoRptIn.getTel();
	            iAFaxMemoRptIn.getFax();
	            iAFaxMemoRptIn.getDate();
	            iAFaxMemoRptIn.getTo();
	            iAFaxMemoRptIn.getAttn();
	            iAFaxMemoRptIn.getItemDesc();
	            iAFaxMemoRptIn.getManuf();
	            iAFaxMemoRptIn.getSupplier();
	            iAFaxMemoRptIn.getParagraph1();
	            iAFaxMemoRptIn.getParagraph2();
	            iAFaxMemoRptIn.getParagraph3();
	            iAFaxMemoRptIn.getParagraph4();
	            iAFaxMemoRptIn.getParagraphOther();
	            iAFaxMemoRptIn.getSender();
	            iAFaxMemoRptIn.getP2No();
	            iAFaxMemoRptIn.getP3No();
	            iAFaxMemoRptIn.getP4No();
	            iAFaxMemoRptIn.getpOthNo();
	            
			}
		}.run();
	}

}
