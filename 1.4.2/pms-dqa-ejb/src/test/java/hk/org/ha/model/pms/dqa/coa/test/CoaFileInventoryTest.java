package hk.org.ha.model.pms.dqa.coa.test;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.vo.coa.CoaFileFolderCriteria;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaFileInventoryTest extends SeamTest  {
	@Test 
	public void testCoaFileFolderComponent() throws Exception
	{
		new ComponentTest() {
			@SuppressWarnings("unchecked")	
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
	            
	           
	            DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date a = dfm.parse("29/07/2009");
	            Date b = dfm.parse("30/07/2011");
	               
	            CoaFileFolderCriteria coaFileFolderCriteria = new CoaFileFolderCriteria();
	            coaFileFolderCriteria.setItemCode("PARA01");
	            coaFileFolderCriteria.setSupplierCode("All");
	            coaFileFolderCriteria.setFileCat(FileCategory.ALL);
	            coaFileFolderCriteria.setOrderType(OrderType.Contract);
	            coaFileFolderCriteria.setContractNum("");
	            coaFileFolderCriteria.setBatchNum("");
	            coaFileFolderCriteria.setStartDate(a);
	            coaFileFolderCriteria.setEndDate(b);
	            
	            Contexts.getSessionContext().set("coaFileFolderCriteria", coaFileFolderCriteria);
	            
	            invokeMethod("#{coaFileFolderInventoryListService.retrieveCoaFileFolderListByCriteria(coaFileFolderCriteria)}");
	            List<CoaFileFolder> coaFileFolderInventoryList = (List<CoaFileFolder>) getValue("#{coaFileFolderInventoryList}");
	            
	            assert coaFileFolderInventoryList.size() >0;	
			}
		}.run();
		
	}
}
