package hk.org.ha.model.pms.dqa.suppperf.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EventLogTest extends SeamTest {
	@Test
	public void EventLogComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				//Login
	            invokeMethod("#{identity.login}");
			
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("31/12/2012");
				
	            // retrieve by criteria
	            QaProblemCriteria qaProblemCriteriaIn = new QaProblemCriteria();
	            qaProblemCriteriaIn.setFromEventLogDate(fromDate);
	            qaProblemCriteriaIn.setToEventLogDate(toDate);
	            qaProblemCriteriaIn.setItemCode("PARA01");
	            qaProblemCriteriaIn.setSupplierCode("ZUEL");
	            qaProblemCriteriaIn.setManufCode("3M");
	            qaProblemCriteriaIn.setPharmCompanyCode("3M");
	            qaProblemCriteriaIn.setFromCreateDate(fromDate);
	            qaProblemCriteriaIn.setToCreateDate(toDate);
	            
	            Contexts.getSessionContext().set("qaProblemCriteriaIn", qaProblemCriteriaIn);
	            invokeMethod("#{eventLogListService.retrieveEventLogListByQaProblemCriteria(qaProblemCriteriaIn)}");
	            		
	            assert getValue("#{eventLogList==null}").equals(true);
	            
	           
	            
	       }
		}.run();
	}

}
