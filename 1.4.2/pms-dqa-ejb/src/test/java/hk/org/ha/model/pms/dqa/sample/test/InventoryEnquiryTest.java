package hk.org.ha.model.pms.dqa.sample.test;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.Payment;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleLetter;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleMovementCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventory;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class InventoryEnquiryTest extends SeamTest {
	@Test
	public void inventoryEnquiryComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("01/01/2012");
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve 
	            SampleMovementCriteria sampleMovementCriteria = new SampleMovementCriteria();
	            
	            sampleMovementCriteria.setItemCode("PARA01");
	            sampleMovementCriteria.setFromInventoryDate(fromDate);
	            sampleMovementCriteria.setToInventoryDate(toDate);
	            sampleMovementCriteria.setManufCode("3M");
	            sampleMovementCriteria.setInventorySelectionType("");
	            
	            Contexts.getSessionContext().set("sampleMovementCriteriaIn", sampleMovementCriteria);
	            invokeMethod("#{sampleMovementListService.retrieveSampleMovementListBySampleMovementCriteria(sampleMovementCriteriaIn)}");
	            
	            List<SampleMovement> sMLFind = (List<SampleMovement>)getValue("#{sampleMovementList}");
	            assert sMLFind.size() >0;
	            
	            //Add
	            SampleMovement sampleMovementAdd = sMLFind.get(0);
	            SampleMovement sampleMovementIn = new SampleMovement();
	            SampleTestSchedule sampleTestScheduleIn = new SampleTestSchedule();
	            
	            sampleMovementIn.setLocationCode(sampleMovementAdd.getLocationCode());
	            sampleMovementIn.setManufCode(sampleMovementAdd.getManufCode());
				sampleMovementIn.setMovementQty(10);
				sampleMovementIn.setOnHandQty(sampleMovementAdd.getOnHandQty() + 10);
				sampleMovementIn.setAdjustFlag("Y");
				sampleMovementIn.setAdjustReason("Test Case");
				
				sampleMovementIn.setSampleTestSchedule(sampleMovementAdd.getSampleTestSchedule());
				
				Contexts.getSessionContext().set("sampleMovementIn", sampleMovementIn);
				invokeMethod("#{sampleMovementService.createSampleMovement(sampleMovementIn)}");

	            invokeMethod("#{sampleMovementService.retrieveSampleMovement(sampleMovementIn)}");
	            assert getValue("#{sampleMovement!=null}").equals(true);
	            assert getValue("#{sampleMovement.adjustReason=='Test Case'}").equals(true);
	            

	            //Edit
	            
	            sampleMovementIn.setAdjustReason("Test Case 2nd time");
				
				Contexts.getSessionContext().set("sampleMovementIn", sampleMovementIn);
				invokeMethod("#{sampleMovementService.updateSampleMovement(sampleMovementIn)}");

	            invokeMethod("#{sampleMovementService.retrieveSampleMovement(sampleMovementIn)}");
	            assert getValue("#{sampleMovement!=null}").equals(true);
	            assert getValue("#{sampleMovement.adjustReason=='Test Case 2nd time'}").equals(true);
	            
	            invokeMethod("#{sampleMovementService.retrieveSampleMovementNewestByItemCode('AAAA01')}");
	            
	            
	            ///////////////////////////////////
	            SampleTestInventory sampleTestInventoryIn = new SampleTestInventory();
	            sampleTestInventoryIn.setInvDate(new Date());
	            sampleTestInventoryIn.setItemCode("");
	            sampleTestInventoryIn.setItemDesc("");
	            sampleTestInventoryIn.setTest("");
	            sampleTestInventoryIn.setBaseUnit("");
	            sampleTestInventoryIn.setMovementQty(0);
	            sampleTestInventoryIn.setCdp(1.0);
	            sampleTestInventoryIn.setLocation("");
	            sampleTestInventoryIn.setBatchNum("");
	            sampleTestInventoryIn.setExpiryDate("");
	            sampleTestInventoryIn.setOrderType("");
	            sampleTestInventoryIn.setSupplier("");
	            sampleTestInventoryIn.setManufacturer("");
	            sampleTestInventoryIn.setPharmCompany("");
	            sampleTestInventoryIn.setScheduleNum("");
	            sampleTestInventoryIn.setAdjustFlag("");
	            sampleTestInventoryIn.setAdjustReason("");
	            
	            sampleTestInventoryIn.getInvDate();
	            sampleTestInventoryIn.getItemCode();
	            sampleTestInventoryIn.getItemDesc();
	            sampleTestInventoryIn.getTest();
	            sampleTestInventoryIn.getBaseUnit();
	            sampleTestInventoryIn.getMovementQty();
	            sampleTestInventoryIn.getCdp();
	            sampleTestInventoryIn.getLocation();
	            sampleTestInventoryIn.getBatchNum();
	            sampleTestInventoryIn.getExpiryDate();
	            sampleTestInventoryIn.getOrderType();
	            sampleTestInventoryIn.getSupplier();
	            sampleTestInventoryIn.getManufacturer();
	            sampleTestInventoryIn.getPharmCompany();
	            sampleTestInventoryIn.getScheduleNum();
	            sampleTestInventoryIn.getAdjustFlag();
	            sampleTestInventoryIn.getAdjustReason();
	        }
		}.run();
	}

}
