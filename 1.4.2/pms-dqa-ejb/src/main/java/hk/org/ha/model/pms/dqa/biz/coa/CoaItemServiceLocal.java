package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;

import javax.ejb.Local;


@Local
public interface CoaItemServiceLocal {
		
	void retrieveCoaItemByCoaItem(CoaItem coaItemIn);
	
	void addCoaItem();
	
	String createCoaItem(CoaItem coaItemIn);
	
	void updateCoaItem();
	
	void deleteCoaItem(CoaItem coaItemIn);
	
    void destroy();
}
