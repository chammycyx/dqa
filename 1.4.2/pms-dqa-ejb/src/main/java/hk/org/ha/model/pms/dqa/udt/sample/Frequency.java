package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Frequency implements StringValuedEnum {
	
	Half("0.5", "0.5x/yr"),
	One("1", "1x/yr"),
	Two("2", "2x/yr"),
	Three("3", "3x/yr"),
	Four("4", "4x/yr"),
	Six("6", "6x/yr"),
	Twelve("12", "12x/yr");	
	
    private final String dataValue;
    private final String displayValue;
        
    Frequency(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Frequency> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<Frequency> getEnumClass() {
    		return Frequency.class;
    	}
    }
}
