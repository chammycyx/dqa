package hk.org.ha.model.pms.dqa.biz.sample;

import javax.ejb.Local;

@Local
public interface InstitutionListByPcuForPopupServiceLocal {
	
	void retrieveInstitutionListByPcuForPopup(String pcuCode);
	
	void destroy();
}
