package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.vo.coa.CoaVerificationData;

import javax.ejb.Local;

@Local
public interface CoaBatchListServiceLocal {

	List<CoaBatch> retrieveCoaBatchListByCoaStatus(CoaStatus coaStatus);
	
	List<CoaVerificationData> retrieveCoaVerificationDataList();
	
	void retrieveCoaBatchListByCoaStatusDiscrepancyStatus( CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus );		
	
	List<CoaBatch> retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus();
	
	void destroy();
}
