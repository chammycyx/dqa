package hk.org.ha.model.pms.dqa.vo.suppperf;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FaxFinalSuppRpt {
	private String faxCreateDate;
	private String contractNo;
	private String refNum;
	private String itemCode;
	private String fullDrugDesc;
	private String manuf;
	private String problemDetail;
	private String batchNum;
	private String caseNum;
	private String orderType;
	private String simlarProblem;
	private String faxFrom;
	private String attn;
	private String faxTo;
	private String qaFax;
	private String qaOfficePhone;
	private String supplierFax;
	
	public String getFaxCreateDate() {
		return faxCreateDate;
	}
	public void setFaxCreateDate(String faxCreateDate) {
		this.faxCreateDate = faxCreateDate;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	public String getManuf() {
		return manuf;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getFaxFrom() {
		return faxFrom;
	}
	public void setFaxFrom(String faxFrom) {
		this.faxFrom = faxFrom;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getSimlarProblem() {
		return simlarProblem;
	}
	public void setSimlarProblem(String simlarProblem) {
		this.simlarProblem = simlarProblem;
	}
	public String getAttn() {
		return attn;
	}
	public void setAttn(String attn) {
		this.attn = attn;
	}
	public String getFaxTo() {
		return faxTo;
	}
	public void setFaxTo(String faxTo) {
		this.faxTo = faxTo;
	}
	public String getQaFax() {
		return qaFax;
	}
	public void setQaFax(String qaFax) {
		this.qaFax = qaFax;
	}
	public String getQaOfficePhone() {
		return qaOfficePhone;
	}
	public void setQaOfficePhone(String qaOfficePhone) {
		this.qaOfficePhone = qaOfficePhone;
	}
	public String getSupplierFax() {
		return supplierFax;
	}
	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}
}