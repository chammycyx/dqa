package hk.org.ha.model.pms.dqa.vo.sample;

import hk.org.ha.model.pms.dqa.persistence.Contact;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SupplierPharmCompanyManufacturerContact {
	
	private Boolean enable;
	private String contactType;
	private String code;
	private String name;
	private Contact contact;	
	
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	public Boolean getEnable() {
		return enable;
	}
	
	public String getContactType() {
		return contactType;
	}
	
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Contact getContact() {
		return contact;
	}
	
	public void setContact(Contact contact) {
		this.contact = contact;
	}
}
