package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.sample.ReleaseRetentionSampleMemo;

import javax.ejb.Local;

@Local
public interface ReleaseRetentionSampleRptServiceLocal {
	ReleaseRetentionSampleMemo retrieveReleaseRetentionSampleMemo(UserInfo defaultCpoContact, UserInfo cpoSignature, SampleTestSchedule sampleTestScheduleIn);
	void createReleaseRetentionSampleMemo(ReleaseRetentionSampleMemo releaseRetentionSampleMemo);
	void generateReleaseRetentionSampleMemo();
	void destroy();
}
