package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.util.QueryUtils;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.coa.MissingBatchCoaDateType;
import hk.org.ha.model.pms.dqa.vo.coa.MissingBatchCoaRptData;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.NumberUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;

@Stateful
@Scope(ScopeType.SESSION)
@Name("missingBatchCoaRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class MissingBatchCoaRptServiceBean implements MissingBatchCoaRptServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String REPORT_NAME = "Missing Batch COA Report";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	private static final String PARAM_DATE_TIME_FORMAT = "dd-MMM-yyyy"; 
	
	private SimpleDateFormat paramSdf = new SimpleDateFormat(PARAM_DATE_TIME_FORMAT);
	
	private String reportParameters = "";
	private String retrieveDate;
	private SimpleDateFormat sdf;
	
	private List<MissingBatchCoaRptData> missingBatchCoaRptList;
	
	public void retrieveMissingBatchCoaRptList(MissingBatchCoaDateType dateType, Date fromDate, Date toDate){
		Date coaBatchStartDate = null;
		Date coaBatchEndDate = null;
		DateMidnight todayMidnight = new DateMidnight();
		
		Date todayDate = todayMidnight.toDate();
		try {
			todayDate = paramSdf.parse(paramSdf.format(todayDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Date sqlEndDate = todayMidnight.plusDays(1).toDate();
				
		if (NumberUtils.isNumber(dateType.getDataValue())) {
			coaBatchStartDate = todayMidnight.minusMonths(Integer.parseInt(dateType.getDataValue())).toDate();
		} else if (dateType == MissingBatchCoaDateType.Others) {
			coaBatchStartDate = fromDate;
		} else {
			coaBatchStartDate = null;
		}
		
		if (dateType == MissingBatchCoaDateType.All) {
			reportParameters = dateType.getDisplayValue();
		} else {
			reportParameters = dateType.getDisplayValue() + " (" + paramSdf.format(fromDate) + " to " + paramSdf.format(toDate) + ")";	
		}
				
		/* batch coa report */
		String sqlStr = "select o.contract.contractNum, o.orderType, o.contract.itemCode," +
				"o.contract.supplier.supplierCode, o.contract.manufacturer.companyCode, o.createDate, " +
				"o.contract.pharmCompany.companyCode, o.contract.startDate, o.contract.endDate, o.coaItemId " +
				"from CoaItem o where o.coaItemId not in (" +
				"                  select cb.coaItem.coaItemId " +
				"				   from CoaBatch cb " +
				"                  where cb.batchNum is not null" +
				"                  and ( (:coaBatchStartDate is null or cb.createDate >= :coaBatchStartDate) and (:coaBatchEndDate is null or cb.createDate < :coaBatchEndDate) )" +
				") " +
				"and o.contract.suspendFlag =:suspendFlag and o.contract.startDate < :endDate and o.contract.endDate >= :today";				
		
		List<?> reportList = em.createQuery(sqlStr)
				.setParameter("suspendFlag", SuspendFlag.Unsuspended)
				.setParameter("today", todayDate, TemporalType.DATE)
				.setParameter("endDate", sqlEndDate, TemporalType.DATE)
				.setParameter("coaBatchStartDate", coaBatchStartDate, TemporalType.DATE)
				.setParameter("coaBatchEndDate", coaBatchEndDate, TemporalType.DATE)
				.getResultList();
		
		MissingBatchCoaRptData obcData;
		missingBatchCoaRptList = new ArrayList<MissingBatchCoaRptData>();
		
		List<Long> coaItemIdList = new ArrayList<Long>();
		
		for (Object o : reportList){
			
			if (o.getClass().isArray()){
				obcData = new MissingBatchCoaRptData();
				obcData.setContractNum((String)Array.get(o, 0));
				obcData.setOrderType(((OrderType)Array.get(o, 1)).getDataValue());
				obcData.setItemCode((String)Array.get(o, 2));
				obcData.setItemDesc(DmDrugCacher.instance().getDrugByItemCode(obcData.getItemCode()).getFullDrugDesc());
				obcData.setSupplierCode((String)Array.get(o, 3));
				obcData.setManufacturerCode((String)Array.get(o, 4));
				obcData.setCreateDate((Date)Array.get(o, 5) );
				obcData.setPharmCompanyCode((String)Array.get(o, 6));
				obcData.setStartDate((Date)Array.get(o, 7) );
				obcData.setEndDate((Date)Array.get(o, 8) );
				obcData.setCoaItemId((Long)Array.get(o, 9));								
				
				missingBatchCoaRptList.add(obcData);
				
				coaItemIdList.add(obcData.getCoaItemId());
			}
		}
		
		if (!missingBatchCoaRptList.isEmpty()) {
			Map<Long, Date> receiveDateMap = getLatestCoaBatch(coaItemIdList);
			for (MissingBatchCoaRptData rptData:missingBatchCoaRptList) {
				rptData.setLatestBatchCoaReceiveDate(receiveDateMap.get(rptData.getCoaItemId()));
			}
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("retrieveMissingBatchCoaRptList - todayMidnight:#0, todayDate:#1, sqlEndDate:#2, coaBatchStartDate:#3, coaBatchEndDate:#4\n" +
					"missingBatchCoaRptList report list size: #5", todayMidnight, todayDate, sqlEndDate, coaBatchStartDate, coaBatchEndDate, missingBatchCoaRptList.size());
		}
	}
		
	private Map<Long,Date> getLatestCoaBatch(List<Long> coaItemIdList) {
		Map<Long, Date> resultMap = new HashMap<Long, Date>();
		List<?> resultList = QueryUtils.splitExecute(em.createQuery("select o.coaItem.coaItemId, max(o.createDate) from CoaBatch o " +
				" where o.coaItem.coaItemId in :coaItemIdList" +
				" and o.coaBatchVer.recordStatus = :recordStatus" +
				" group by o.coaItem.coaItemId")
				.setParameter("recordStatus", RecordStatus.Active),"coaItemIdList", coaItemIdList);
		
		Long id;
		Date createDate;
		
		for (Object obj:resultList) {
			if (obj.getClass().isArray()){
				id = (Long)Array.get(obj, 0);
				createDate = (Date)Array.get(obj, 1);
				resultMap.put(id, createDate);
			}
		}
		
		return resultMap;
	}
	
	public String getReportParameters() {
		return reportParameters;
	}


	public String getRetrieveDate() {
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}

	public String getReportName() {
		return REPORT_NAME;
	}
	
	public List<MissingBatchCoaRptData> getMissingBatchCoaRptList() {
		return missingBatchCoaRptList;
	}
	
	
	@Remove
	public void destroy(){
		
	}

}
