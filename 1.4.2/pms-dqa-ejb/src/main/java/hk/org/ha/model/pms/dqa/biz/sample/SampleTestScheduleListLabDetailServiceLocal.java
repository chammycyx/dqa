package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Collection;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import javax.ejb.Local;

@Local
public interface SampleTestScheduleListLabDetailServiceLocal {
	
	void retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNum(String labCode, String testCode, String labCollectNum);
	
	void retrieveSampleTestScheduleListForLabTestByLabTestOrderType(String labCode, String testCode, OrderType orderType);

	void updateSampleTestScheduleForLabCollection(List<SampleTestSchedule> sampleTestScheduleListIn);
	
	void updateSampleTestScheduleForLabTest(List<SampleTestSchedule> sampleTestScheduleListIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy(); 
}
