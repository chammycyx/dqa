package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ClosureStatus implements StringValuedEnum {
	
	OPEN("OPEN", "Open"),
	CLOSED("CLOSED", "Closed");
	
	
    private final String dataValue;
    private final String displayValue;
        
    ClosureStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ClosureStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<ClosureStatus> getEnumClass() {
    		return ClosureStatus.class;
    	}
    }
}
