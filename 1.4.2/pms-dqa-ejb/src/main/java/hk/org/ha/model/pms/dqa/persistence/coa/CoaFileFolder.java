package hk.org.ha.model.pms.dqa.persistence.coa;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "COA_FILE_FOLDER")
@NamedQueries({
	@NamedQuery(name = "CoaFileFolder.findOrgCoaByCoaItem", query = "select o from CoaFileFolder o where o.coaBatch.coaItem = :coaItem and o.coaBatch.batchNum is null order by o.coaFileFolderId"),
	@NamedQuery(name = "CoaFileFolder.findBatchCoaByCoaBatch", query = "select o from CoaFileFolder o where o.coaBatch = :coaBatch and o.coaBatch.batchNum is not null order by o.coaFileFolderId"),
	@NamedQuery(name = "CoaFileFolder.findBatchCoaWithoutOthersByCoaBatch", query = "select o from CoaFileFolder o where o.coaBatch = :coaBatch and o.coaBatch.batchNum is not null and o.fileCat = :fileCat order by o.coaFileFolderId"),
	@NamedQuery(name = "CoaFileFolder.findCoaFileFolderListByCriteria",
			query = "select o from CoaFileFolder o where "+
			"(o.coaBatch.coaItem.contract.itemCode = :itemCode) " +
			"and (o.coaBatch.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' =:supplierCode ) " +
			"and (o.fileCat = :category or '-' = :categoryStr) "  +
			"and o.coaBatch.coaItem.orderType = :orderType " +
			"and (o.coaBatch.coaItem.contract.contractNum LIKE :contractNum or :contractNum is null )" +
			"and (o.coaBatch.batchNum LIKE :batchNum or :batchNum is null)" +
			"and o.fileItem.createDate >=:startDate and  o.fileItem.createDate <= :endDate " +
			"order by o.fileItem.createDate, o.fileItem.modifyDate "),
	@NamedQuery(name = "CoaFileFolder.findCoaFileFolderListForCoaItemMaint", 
			query = "select o from CoaFileFolder o where o.coaBatch.coaItem = :coaItem " +
					"order by o.fileItem.createDate, o.fileItem.modifyDate"),
	@NamedQuery(name = "CoaFileFolder.findCoaFileFolderForFileItemMaint", 
			query = "select o from CoaFileFolder o where o.coaBatch.coaItem.contract.itemCode =:itemCode " +
					"and o.coaBatch.coaItem.coaItemId = :coaItemId " +
					"and o.coaBatch.coaItem.contract.supplier.supplierCode =:supplierCode "+
					"and o.coaBatch.coaItem.orderType = :orderType " +
					"and o.fileCat = :fileCategory " +
					"and (o.effectiveDate = :effectiveDate or :effectiveDate is null)" +
					"and (o.coaBatch.batchNum = :batchNum or :batchNum is null)"),
	@NamedQuery(name = "CoaFileFolder.findByCoaFileFolderId", query = "select o from CoaFileFolder o where o.coaFileFolderId = :coaFileFolderId")
	}    
)
@Customizer(AuditCustomizer.class)	
public class CoaFileFolder extends VersionEntity {

	private static final long serialVersionUID = 2805049643638450385L;

	@Id
	@Column(name="COA_FILE_FOLDER_ID",nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaFileFolderSeq")
	@SequenceGenerator(name="coaFileFolderSeq", sequenceName="SEQ_COA_FILE_FOLDER", initialValue=10000)		
	private Long coaFileFolderId;

	@Converter(name = "CoaFileFolder.fileCat", converterClass = FileCategory.Converter.class )
	@Convert("CoaFileFolder.fileCat")
	@Column(name="FILE_CAT", length = 1, nullable = false)
	private FileCategory fileCat;

	@Column(name="EFFECTIVE_DATE", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date effectiveDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "COA_BATCH_ID", nullable = true)
	private CoaBatch coaBatch;

	@OneToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="FILE_ID")
	private FileItem fileItem;    

	@Transient
	private boolean enable;
	
	public CoaBatch getCoaBatch() {
		return coaBatch;
	}

	public void setCoaBatch(CoaBatch coaBatch) {
		this.coaBatch = coaBatch;
	}

	public void setCoaFileFolderId(Long coaFileFolderId) {
		this.coaFileFolderId = coaFileFolderId;
	}

	public Long getCoaFileFolderId() {
		return coaFileFolderId;
	}

	public void setFileCat(FileCategory fileCat) {
		this.fileCat = fileCat;
	}

	public FileCategory getFileCat() {
		return fileCat;
	}

	public void setEffectiveDate(Date effectiveDate) {
		if (effectiveDate != null) {
			this.effectiveDate = new Date(effectiveDate.getTime());
		} else {
			this.effectiveDate = null;
		}
	}

	public Date getEffectiveDate() {
		return (effectiveDate != null) ? new Date(effectiveDate.getTime()) : null;
	}
	
	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}

	public FileItem getFileItem() {
		return fileItem;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}

}

