package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemInstitution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;

import javax.ejb.Local;

@Local
public interface QaProblemServiceLocal {
	
	void retrieveQaProblemByCaseNum(String caseNumIn);
	
	void retrieveQaProblemByQaProblem(QaProblem qaProblemIn);
	
	QaProblem getQaProblemByCaseNum(String caseNumIn);
	
	void saveQaProblem(QaProblem qaProblemIn);
	
	void deleteQaProblem(QaProblem qaProblemIn, String deleteReason);
	
	void updateQaProblemWithQaProblem(QaProblem qaProblemIn, List<QaProblemFileUploadData> qaProblemFileUploadDataListIn, List<QaProblemInstitution> qaProblemInstitutionListIn, List<QaProblemNatureSubCat> qaProblemNatureSubCatListIn, YesNoFlag qaProblemInstitutionAllInstitutionFlagIn);
	
	void createCaseFinalRpt(QaProblem qaProblemIn, SupplierContact supplierContactIn, CompanyContact companyContactIn);
	
	void generateCaseFinalRpt();
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
