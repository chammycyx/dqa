package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SAMPLE_TEST_SCHEDULE_LETTER")
@Customizer(AuditCustomizer.class)
public class SampleTestScheduleLetter extends VersionEntity {

	private static final long serialVersionUID = 4854124755362977341L;
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleTestScheduleLetterSeq")
	@SequenceGenerator(name = "sampleTestScheduleLetterSeq", sequenceName = "SEQ_SAMPLE_TEST_SCHED_LETTER", initialValue=10000)
	@Id
	@Column(name="SAMPLE_TEST_SCHEDULE_LETTER_ID", nullable=false)
	private Long sampleTestScheduleLetterId;

	@ManyToOne
	@JoinColumn(name="LETTER_ID")
	private Letter letter;
	
	@ManyToOne
	@JoinColumn(name="SCHEDULE_ID")
	private SampleTestSchedule sampleTestSchedule;
			
	public void setSampleTestScheduleLetterId(Long sampleTestScheduleLetterId) {
		this.sampleTestScheduleLetterId = sampleTestScheduleLetterId;
	}

	public Long getSampleTestScheduleLetterId() {
		return sampleTestScheduleLetterId;
	}

	public void setLetter(Letter letter) {
		this.letter = letter;
	}

	public Letter getLetter() {
		return letter;
	}

	public void setSampleTestSchedule(SampleTestSchedule sampleTestSchedule) {
		this.sampleTestSchedule = sampleTestSchedule;
	}

	public SampleTestSchedule getSampleTestSchedule() {
		return sampleTestSchedule;
	}
}
