package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.ManufacturerListManagerLocal;
import hk.org.ha.model.pms.dqa.biz.PharmCompanyListManagerLocal;
import hk.org.ha.model.pms.dqa.biz.SupplierListManagerLocal;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.persistence.sys.SystemMessage;
import hk.org.ha.model.pms.vo.sys.SysMsgMap;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

/**
 * Session Bean implementation class PostLogonServiceBean
 */
@Stateful
@Name("postLogonService")
@Scope(ScopeType.SESSION)
@RemoteDestination
@MeasureCalls
public class PostLogonServiceBean implements PostLogonServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unused")
	@Out(required = false)
	private int idleTimeout;
	
	@Out
	private SysProfile sysProfile;
	
	@Out
	private SysMsgMap sysMsgMap = new SysMsgMap();

	@In 
	private AccessControlLocal accessControl;
	
	@In
	private DmDrugCacher dmDrugCacher;
	
	@In
	private SupplierListManagerLocal supplierListManager;
	
	@In
	private PharmCompanyListManagerLocal pharmCompanyListManager;
	
	@In
	private ManufacturerListManagerLocal manufacturerListManager;
	
	private boolean enablePreLoadCacher = false;
	
	private boolean isEnablePreLoadCacher() {
		return enablePreLoadCacher;
	}

	public void setEnablePreLoadCacher(boolean enablePreLoadCacher) {
		this.enablePreLoadCacher = enablePreLoadCacher;
	}

	private void loadSessionData() {
		supplierListManager.retrieveSupplierList();
		pharmCompanyListManager.retrievePharmCompanyList();
		manufacturerListManager.retrieveManufacturerList();
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveSystemMessage() {

		List<SystemMessage> systemMessageList = em.createQuery(
				"select o from SystemMessage o")
				.getResultList();
		
		Map<String, SystemMessage> properties = new HashMap<String, SystemMessage>();
		for (SystemMessage systemMessage : systemMessageList) {
			properties.put(systemMessage.getMessageCode(), systemMessage);
		}
		
		sysMsgMap.setMsg(properties);
		Contexts.getSessionContext().set("sysMsgMap", sysMsgMap);
	}
	
	public void postLogon() throws IOException {	
		sysProfile = (SysProfile) Component.getInstance(SysProfile.class, true);
		
		accessControl.retrieveUserAccessControl();
		
		if (isEnablePreLoadCacher()) {
			dmDrugCacher.load();
		}
		
		loadSessionData();
		retrieveSystemMessage();
	}
	
	@Remove
	public void destroy() {
		if (sysProfile != null) {
			sysProfile = null;
		}
	}
}
