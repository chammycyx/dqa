package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum DiscrepancyStatus implements StringValuedEnum {
			
	NullValue("N", "Null value"),
	DiscrepancyUnderGrouping("G", "Discrepancy under grouping"),
	PharmacistFollowUp("P", "Pharmacist follow up"),
	EmailToSupplier("E", "Email to supplier"),
	ReminderToSupplier("R", "Reminder to supplier");
	
	
    private final String dataValue;
    private final String displayValue;
        
    DiscrepancyStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<DiscrepancyStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<DiscrepancyStatus> getEnumClass() {
    		return DiscrepancyStatus.class;
    	}
    }
}
