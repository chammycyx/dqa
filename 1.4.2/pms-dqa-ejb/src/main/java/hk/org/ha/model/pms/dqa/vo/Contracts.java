package hk.org.ha.model.pms.dqa.vo;

import hk.org.ha.model.pms.dqa.udt.ActionType;
import hk.org.ha.model.pms.dqa.udt.CancelFlag;
import hk.org.ha.model.pms.dqa.udt.ClosureStatus;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name="Contracts")
@XmlType(propOrder={"contractList"})
public class Contracts {

	private List<Contract> contractList = new ArrayList<Contract>();
	
	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}
	
	@XmlElement(name="Contract", type=Contract.class)
	public List<Contract> getContractList() {
		return contractList;
	}

	@Override
	public String toString() {
		return "Contracts [contractList=" + contractList + "]";
	}
	
	public static void main(String[] args) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(Contracts.class, Contract.class, ContractLine.class, ContractMsp.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
				
		Contracts cts = new Contracts();
		ContractMsp cm = new ContractMsp();
		
		Calendar cal = Calendar.getInstance();
		
		cm.setCountryOfOrigin("US");
		cal.set(2011, Calendar.JANUARY, 1);
		cm.setEffectiveStartDate( cal.getTime() );
		cal.set(2011, Calendar.DECEMBER, 31);
		cm.setEffectiveEndDate( cal.getTime() );
		cm.setManufCode("VPL");
		cm.setManufName("VALEANT PHARMACEUTICALS INTERNATIONAL");
		cm.setMspRecordId(1L);
		cm.setPharmCompanyCode("INOV");
		cm.setPharmCompanyName("INOVA PHARMACEUTICALS (AUSTRALIA)PTY LTD");
		
		List<ContractMsp> cmList = new ArrayList<ContractMsp>();
		cmList.add( cm );
		
		ContractLine cl = constructContractLine(cal, cmList);
		
		List<ContractLine> clList = new ArrayList<ContractLine>();
		clList.add( cl );
		
		Contract c = constructContract(cal, clList);
		
		List<Contract> chList = new ArrayList<Contract>();
		chList.add( c );
		
		cts.setContractList( chList );
		
//		marshaller.marshal(cts, System.out);		
		StringWriter writer = new StringWriter();		
		marshaller.marshal(cts, writer);	
	}
	
	private static ContractLine constructContractLine(Calendar calIn, List<ContractMsp> cmList){
		ContractLine cl = new ContractLine();
		Calendar cal = calIn;
		cl.setContractMsps( cmList );
		cl.setBaseUnit("TAB");
		cl.setBuyQty(1);
		cl.setCancelFlag( CancelFlag.N );
		cl.setClosureStatus( ClosureStatus.OPEN );
		cl.setContractLineId(1L);
		cl.setContractLineNum("1");
		cal.set(2011, Calendar.DECEMBER, 29);
		cl.setEffectiveEndDate( cal.getTime() );
		cl.setGetFreeQty(1);
		cl.setItemCode( "PARA01" );
		cl.setItemDesc( "PARACETAMOL TABLET 500MG 1G " );
		cl.setPackSize( 12 );
		cl.setUnitOfMeasure( "Pack" );
		cl.setUnitOfMeasureName( "pack of 3" );
		cl.setUnitPrice( 0.9134 );
		
		return cl;
	}
	
	private static Contract constructContract(Calendar calIn, List<ContractLine> clList ){
		Contract c = new Contract();
		Calendar cal = calIn;
		c.setActionType( ActionType.C );
		c.setCancelFlag( CancelFlag.N );
		c.setCity( "GuangZhou" );
		c.setClosureStatus( ClosureStatus.OPEN );
		c.setContractHeaderId( 1L );
		c.setContractLines( clList );
		c.setContractNum( "9100000001" );
		c.setPhsContractNum( "HOC2323AB" );
		c.setContractStatus( "Approved" );
		c.setContractType( "C" );
		c.setCurrency( "HKD" );
		cal.set(2012, Calendar.JANUARY, 1);
		c.setEffectiveEndDate( cal.getTime() );
		cal.set(2010, Calendar.DECEMBER, 23);
		c.setEffectiveStartDate( cal.getTime() );
		c.setPaymentTerms( "TEST" );
		c.setPhsContractNum( "HOC111111A" );
		c.setRevisionNum( "123" );
		c.setSupplierAddress1( "22/F, HAB" );
		c.setSupplierAddress2( "Agyle Street, Mong Kok" );
		c.setSupplierAddress3( "HONG KONG" );
		c.setSupplierCode( "ZUEL" );
		c.setSupplierFaxNum( "25785144556" );
		c.setSupplierName( "ZUEL Company Ltd." );	
		return c;
	}
	
}
