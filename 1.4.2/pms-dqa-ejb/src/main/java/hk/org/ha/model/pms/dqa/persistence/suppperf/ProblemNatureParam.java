package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PROBLEM_NATURE_PARAM")
@NamedQueries({
	@NamedQuery(name = "ProblemNatureParam.findByProblemNatureParamId", query = "select o from ProblemNatureParam o where o.problemNatureParamId = :problemNatureParamId and o.recordStatus = :recordStatus order by o.displayOrder, o.paramDesc  "),
	@NamedQuery(name = "ProblemNatureParam.findByParamDesc", query = "select o from ProblemNatureParam o where o.paramDesc = :paramDesc and o.recordStatus = :recordStatus order by o.displayOrder, o.paramDesc "),
	@NamedQuery(name = "ProblemNatureParam.findByRecordStatus", query = "select o from ProblemNatureParam o where o.recordStatus = :recordStatus order by o.displayOrder, o.paramDesc  "),
	@NamedQuery(name = "ProblemNatureParam.deleteByProblemNatureParamId", query = "update ProblemNatureParam o set o.recordStatus = :recordStatus where o.problemNatureParamId = :problemNatureParamId ")
})
@Customizer(AuditCustomizer.class)
public class ProblemNatureParam extends VersionEntity {

	
	private static final long serialVersionUID = 6932550485618390193L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "problemNatureParamSeq")
	@SequenceGenerator(name = "problemNatureParamSeq", sequenceName = "SEQ_PROBLEM_NATURE_PARAM", initialValue=10000)
	@Id
	@Column(name="PROBLEM_NATURE_PARAM_ID", nullable=false)
	private Long problemNatureParamId;

	@Column(name="PARAM_DESC", length=50, nullable=false)
	private String paramDesc;
	
	@Column(name="DISPLAY_ORDER", nullable=false)
    private Integer displayOrder;
	
	@Converter(name = "ProblemNatureParam.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("ProblemNatureParam.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;

	public Long getProblemNatureParamId() {
		return problemNatureParamId;
	}

	public void setProblemNatureParamId(Long problemNatureParamId) {
		this.problemNatureParamId = problemNatureParamId;
	}

	public String getParamDesc() {
		return paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
}
