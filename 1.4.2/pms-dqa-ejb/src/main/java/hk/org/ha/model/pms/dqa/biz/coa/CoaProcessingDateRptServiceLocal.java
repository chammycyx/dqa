package hk.org.ha.model.pms.dqa.biz.coa;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptData;

import javax.ejb.Local;

@Local
public interface CoaProcessingDateRptServiceLocal {
	
	void retrieveCoaProcessingDateRptList(CoaProcessingDateRptCriteria coaProcessingDateRptCriteria);
	
	List<CoaProcessingDateRptData> getCoaProcessingDateRptList();
	
	String getReportName();
	
	String getReportParameters();
	
	Date getRetrieveDate();
	
	void destroy();

}
