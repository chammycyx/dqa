package hk.org.ha.model.pms.dqa.biz.coa;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaDiscrepancyAnalysisRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaDiscrepancyAnalysisRptServiceBean implements CoaDiscrepancyAnalysisRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	private static final String REPORT_NAME = "COA Discrepancy Analysis Report";
	private static final String DATE_FORMAT = "dd-MMM-yyyy";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";

	private String reportParameters;
	private String retrieveDate;

	private SimpleDateFormat sdf;

	private List<CoaDiscrepancyAnalysisRptData> coaDiscrepancyAnalysisRptList;

	private List<String> headerList;


	@In(create=true)
	private CoaDiscrepancyKeyListServiceLocal coaDiscrepancyKeyListService;

	@In(required = false)
	private List<CoaDiscrepancyKey> coaDiscrepancyKeyList;

	private void setReportParameters(CoaDiscrepancyAnalysisRptCriteria coaDiscrepancyAnalysisRptCriteria){

		String supplierCodeIn = coaDiscrepancyAnalysisRptCriteria.getSupplierCode();
		String itemCodeIn = coaDiscrepancyAnalysisRptCriteria.getItemCode();
		Date startDate = coaDiscrepancyAnalysisRptCriteria.getStartDate();
		Date endDate = coaDiscrepancyAnalysisRptCriteria.getEndDate();
		
		itemCodeIn = (itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn);

		sdf = new SimpleDateFormat(DATE_FORMAT);
		String startDateStr = (sdf.format(startDate));
		String endDateStr = (sdf.format(endDate));

		reportParameters = "Supplier Code="+ supplierCodeIn + ", Item Code=" +  itemCodeIn
							+ ", Create Date=" + startDateStr+ " to " +endDateStr ;
	}


	@SuppressWarnings("unchecked")
	private List retrieveCoaDiscrepancyListData(CoaDiscrepancyAnalysisRptCriteria coaDiscrepancyAnalysisRptCriteria){
		logger.debug("retrieveCoaDiscrepancyListData");

		setReportParameters(coaDiscrepancyAnalysisRptCriteria);

		String supplierCodeIn = coaDiscrepancyAnalysisRptCriteria.getSupplierCode();
		String itemCodeIn = coaDiscrepancyAnalysisRptCriteria.getItemCode();
		Date startDate = coaDiscrepancyAnalysisRptCriteria.getStartDate();

		Calendar c = Calendar.getInstance(); 
		c.setTime(coaDiscrepancyAnalysisRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
		

		String sqlStr ="select o.coaBatch.coaItem.contract.supplier.supplierCode, o.coaBatch.coaItem.contract.itemCode,  o.coaDiscrepancyKey.discrepancyDesc, " +
						"count(o) " +
						"from CoaDiscrepancy o where o.coaBatch.createDate >=:startDate and o.coaBatch.createDate < :endDate " +
						"and (o.coaBatch.coaItem.contract.itemCode = :itemCode or :itemCode is null) " +
						"and (o.coaBatch.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' = :supplierCode ) " +
						"group by o.coaBatch.coaItem.contract.supplier.supplierCode, o.coaBatch.coaItem.contract.itemCode,  o.coaDiscrepancyKey.discrepancyDesc";

		List reportList = em.createQuery(sqlStr)
		.setParameter("startDate", startDate)
		.setParameter("endDate", sqlEndDate)
		.setParameter("itemCode", itemCodeIn)
		.setParameter("supplierCode", supplierCodeIn)
		.getResultList();

		return reportList;
	}


	public void retrieveHeaderList(){
		coaDiscrepancyKeyList = coaDiscrepancyKeyListService.retrieveCoaDiscrepancyKeyListForReport();

		headerList = new ArrayList<String>();
		for(CoaDiscrepancyKey cdKey:coaDiscrepancyKeyList ){

			headerList.add(cdKey.getDiscrepancyDesc());
		}
	}

	public List<String> getHeaderList() {

		return headerList;
	}

	@SuppressWarnings("unchecked")
	public void retrieveCoaDiscrepancyAnalysisList(CoaDiscrepancyAnalysisRptCriteria coaDiscrepancyAnalysisRptCriteria){
		logger.debug("retrieveCoaDiscrepancyAnalysisList");

		coaDiscrepancyAnalysisRptList = new ArrayList<CoaDiscrepancyAnalysisRptData>();

		List dataList = retrieveCoaDiscrepancyListData(coaDiscrepancyAnalysisRptCriteria);

		if (dataList != null){
			retrieveHeaderList();

			HashMap<String, CoaDiscrepancyAnalysisRptData> coaDiscAnalysisHM = new HashMap<String, CoaDiscrepancyAnalysisRptData>();
			String reportDataKey;
			CoaDiscrepancyAnalysisRptData caData;

			HashMap<String, Integer> dataHM = new HashMap<String, Integer>();
			String discHMKey;

			Integer discCnt;

			String strSupplierCode;
			String strItemCode;
			String strDiscrepancy;
			for(Object cd: dataList){
				strSupplierCode = (String)Array.get(cd, 0); 
				strItemCode = (String)Array.get(cd, 1);
				strDiscrepancy = (String)Array.get(cd, 2);

				reportDataKey = strSupplierCode +":" + strItemCode;
				discHMKey =  strSupplierCode +":" + strItemCode  +":"+ strDiscrepancy;

				if (!coaDiscAnalysisHM.containsKey(reportDataKey)){
					caData = new CoaDiscrepancyAnalysisRptData();
					caData.setSupplierCode(strSupplierCode);
					caData.setItemCode(strItemCode);

					coaDiscAnalysisHM.put(reportDataKey, caData);
					coaDiscrepancyAnalysisRptList.add(caData);
				}

				if (!dataHM.containsKey(discHMKey)){
					discCnt = ((Long)Array.get(cd, 3)).intValue();
					dataHM.put(discHMKey, discCnt);
				}	
			}

			Integer totalCnt = 0;
			
			if (coaDiscrepancyAnalysisRptList.size() >0){
				List<String> headerDataList = getHeaderList();

				for (CoaDiscrepancyAnalysisRptData ca : coaDiscrepancyAnalysisRptList )
				{
					totalCnt = 0;
					for (String headerStr : headerDataList){

						String tmpKey = ca.getSupplierCode() + ":" + ca.getItemCode() + ":" +headerStr;

						if (dataHM.containsKey(tmpKey))
						{				
							ca.getDiscDataList().add(dataHM.get(tmpKey));
							totalCnt += dataHM.get(tmpKey);
						}else {
							ca.getDiscDataList().add(Integer.valueOf(0));
							totalCnt += 0;
						}
					}
					
					ca.setTotal(totalCnt);
				}
			}
		}
		logger.debug("coaDiscrepancyAnalysisRptList report list size: #0", coaDiscrepancyAnalysisRptList.size());
	}	

	public List<CoaDiscrepancyAnalysisRptData> getCoaDiscrepancyAnalysisRptList() {
		return coaDiscrepancyAnalysisRptList;
	}

	public String getReportName() {
		return REPORT_NAME;
	}

	public String getReportParameters() {
		return reportParameters;
	}

	public String getRetrieveDate() {
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}

	@Remove
	public void destroy(){
		headerList = null;
		coaDiscrepancyAnalysisRptList = null;
	}


}
