package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.SystemManualFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "EVENT_LOG")
@Customizer(AuditCustomizer.class)
public class EventLog extends VersionEntity {

	private static final long serialVersionUID = 2553965126485647715L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventLogSeq")
	@SequenceGenerator(name = "eventLogSeq", sequenceName = "SEQ_EVENT_LOG", initialValue=10000)
	@Id
	@Column(name="EVENT_LOG_ID", nullable=false)
	private Long eventLogId;
	
	@Column(name="EVENT_LOG_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date eventLogDate;
		
	@Column(name="EVENT_DESC", length=500)
	private String eventDesc;
	
	@Converter(name = "EventLog.eventType", converterClass = SystemManualFlag.Converter.class )
	@Convert("EventLog.eventType")
	@Column(name="EVENT_TYPE", length=1)
	private SystemManualFlag eventType;
	
	@Converter(name = "EventLog.faxLogFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("EventLog.faxLogFlag")
	@Column(name="FAX_LOG_FLAG", length=1)
	private YesNoFlag faxLogFlag;
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_DETAIL_ID")
	private FaxDetail faxDetail;
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	
	@Transient
	private String actionType;
	
	
	
	public Long getEventLogId() {
		return eventLogId;
	}

	public void setEventLogId(Long eventLogId) {
		this.eventLogId = eventLogId;
	}

	public Date getEventLogDate() {
		return (eventLogDate!=null)?new Date(eventLogDate.getTime()):null;
	}

	public void setEventLogDate(Date eventLogDate) {
		if (eventLogDate!=null) {
			this.eventLogDate = new Date(eventLogDate.getTime());
		} else {
			this.eventLogDate = null;
		}
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public SystemManualFlag getEventType() {
		return eventType;
	}

	public void setEventType(SystemManualFlag eventType) {
		this.eventType = eventType;
	}

	public YesNoFlag getFaxLogFlag() {
		return faxLogFlag;
	}

	public void setFaxLogFlag(YesNoFlag faxLogFlag) {
		this.faxLogFlag = faxLogFlag;
	}
	
	public FaxDetail getFaxDetail() {
		return faxDetail;
	}

	public void setFaxDetail(FaxDetail faxDetail) {
		this.faxDetail = faxDetail;
	}
	
	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}
	
	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}


}
