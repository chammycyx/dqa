package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.Date;

public class MissingBatchCoaRptData {

	private String contractNum="";
	
	private String orderType ="";
	
	private String itemCode="";
	
	private String itemDesc="";
	
	private String supplierCode="";
	
	private String manufacturerCode="";
	
	private String pharmCompanyCode = "";
	
	private Date createDate = null;
	
	private Date startDate = null;

	private Date endDate = null;	
	
	private Date latestBatchCoaReceiveDate = null;
	
	private Long coaItemId = null;
	
	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public String getManufacturerCode() {
		return manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}
	
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public Date getCreateDate() {
		return (createDate != null )? new Date(createDate.getTime()) : null;
	}

	public void setCreateDate(Date createDate) {
		if (createDate != null){
			this.createDate = new Date(createDate.getTime());
		}
	}
	
	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()): null;
	}

	public void setStartDate(Date startDate) {
		if (startDate != null){
			this.startDate = new Date(startDate.getTime());
		}
	}

	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}

	public void setEndDate(Date endDate) {
		if (endDate != null){
			this.endDate = new Date(endDate.getTime());
		}
	}

	public void setLatestBatchCoaReceiveDate(Date latestBatchCoaReceiveDate) {
		this.latestBatchCoaReceiveDate = latestBatchCoaReceiveDate;
	}

	public Date getLatestBatchCoaReceiveDate() {
		return latestBatchCoaReceiveDate;
	}

	public void setCoaItemId(Long coaItemId) {
		this.coaItemId = coaItemId;
	}

	public Long getCoaItemId() {
		return coaItemId;
	}
}
