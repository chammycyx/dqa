package hk.org.ha.model.pms.dqa.biz.delaydelivery;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.biz.DqaMailSenderServiceLocal;
import hk.org.ha.model.pms.dqa.biz.FileItemManagerLocal;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.FuncSeqNum;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormContact;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormPo;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormVer;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.delaydelivery.DelayDeliveryRptFormStatus;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.internal.jpa.EntityManagerImpl;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
@Stateful
@Scope(ScopeType.SESSION)
@Name("delayDeliveryRptFormService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DelayDeliveryRptFormServiceBean implements DelayDeliveryRptFormServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	public DelayDeliveryRptFormData delayDeliveryRptFormData;
	
	@In(create=true)
	private FileItemManagerLocal fileItemManager;
	

	@In(create =true)
	private DqaMailSenderServiceLocal dqaMailSenderService;
	
	public String delayDeliveryToEmail;
	
	private String uploadPath;
	private static final String UNCHECKED = "unchecked";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy";
	public static final DateFormat df = new SimpleDateFormat("yyyy");
	public static final DateFormat dmf = new SimpleDateFormat("MM");
	private static final String FILE_PATH_PREFIX ="DELAYDELIVERY";
	private static final String PATH_DELIMITER ="/";
	private static final String CONFLICT_MSG = "The reporting staff has declared <font color='red'>conflict of interest</font> with "; 
	private static final String CONFLICT_MSG_END = " under this delayed delivery report"; 
	private static final String NO_CONFLICT_MSG = "The reporting staff has declared <font color='blue'>no conflict of interest</font> with the company/manufacturer/supplier under this delayed delivery report";
	private static final DateFormat dateformatter = new SimpleDateFormat(DATE_TIME_FORMAT);
	
	@Remove
	public void destroy(){
	    	
	}
	
	@Override
	public String createUpdateDelayDeliveryRptForm(
			DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			Boolean sendEmailBoolean){
		
		if(delayDeliveryRptFormDataIn.getRefNum() == null){
			return createDelayDeliveryRptForm(delayDeliveryRptFormDataIn,delayDeliveryRptFormContactDataList,delayDeliveryRptFormPoDataList,sendEmailBoolean);
		}else{
			return updateDelayDeliveryRptForm(delayDeliveryRptFormDataIn,delayDeliveryRptFormContactDataList,delayDeliveryRptFormPoDataList,sendEmailBoolean);
		}
			
		
	}
	
	
	
	@Override
	public String createDelayDeliveryRptForm(
			DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			Boolean sendEmailBoolean) {

		
			DelayDeliveryRptForm d = new DelayDeliveryRptForm();
		
			d = constructDelayDeliveryRptForm( d, delayDeliveryRptFormDataIn);
		
			em.persist(d);
			em.flush();
			
			delayDeliveryRptFormDataIn.setRefNum(d.getRefNum());
			
			List<DelayDeliveryRptFormContact> dcList = null;
			List<DelayDeliveryRptFormPo> dpList  = null;
			if(delayDeliveryRptFormContactDataList != null){
				dcList  = constructDelayDeliveryRptFormContactList(d, delayDeliveryRptFormContactDataList);
			}
			if(delayDeliveryRptFormPoDataList != null){
				dpList = constructDelayDeliveryRptFormPoList(d, delayDeliveryRptFormPoDataList , new ArrayList<String>());
			}

			DelayDeliveryRptFormVer dVer = new DelayDeliveryRptFormVer();
			dVer.setDelayDeliveryRptForm(d);
			dVer.setStatus(delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus());
			
			if(delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus().equals(DelayDeliveryRptFormStatus.Submitted)){
				d.setReportDate(new Date());
			}
			
			em.persist(dVer);
			em.flush();
			
			
			d.setDelayDeliveryRptFormContactList(dcList);
			d.setDelayDeliveryRptFormPoList(dpList);
			d.setDelayDeliveryRptFormVer(dVer);
			
			if(sendEmailBoolean){
				sendEmail(delayDeliveryRptFormDataIn,
						 delayDeliveryRptFormContactDataList,
						 delayDeliveryRptFormPoDataList);
			}
			
			return "R"+d.getRefNum();
		
		
	}
	
	
	private boolean sendEmail(DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList){
		
			LetterTemplate letterTemplate = new LetterTemplate();

			String[] tos = delayDeliveryToEmail.split(";");
			
			letterTemplate.setToList(tos);
			
			
			letterTemplate.setCcList(new String[0]);
			letterTemplate.setSampleTestFileList(null);
			letterTemplate.setCoaFileFolderList(null);
			String emailNumber = retrieveEmailNumber(delayDeliveryRptFormDataIn);
			letterTemplate.setContent(
					constructEmailContent( delayDeliveryRptFormDataIn,
							delayDeliveryRptFormContactDataList,
							delayDeliveryRptFormPoDataList, emailNumber)
			);
			
			letterTemplate.setCode(null);
			letterTemplate.setName(null);
			letterTemplate.setSubject(emailNumber+" Delayed Delivery Report on " + delayDeliveryRptFormDataIn.getItemCode() + " from " + delayDeliveryRptFormDataIn.getInstitution().getInstitutionCode() + " with "+ delayDeliveryRptFormDataIn.getOnHandLastFor().toString() + " month(s) SOH" );
			
			dqaMailSenderService.sendEmail(letterTemplate);
			
			return true;
				
				
		
	}
	
	private boolean checkContactDelete(DelayDeliveryRptFormContact c){
		StringBuffer sqlQueryBuffer = new StringBuffer();
		
		sqlQueryBuffer.append("select o from DelayDeliveryRptFormContact o "); 
		sqlQueryBuffer.append("where o.contact.contactId = :contactId ");
		
		Query q = em.createQuery(sqlQueryBuffer.toString())
		.setParameter("contactId", c.getContact().getContactId());
		
		q=q.setHint(QueryHints.FETCH, "o.contact");
		
		List<DelayDeliveryRptForm> resultList = q.getResultList();
		
		if(resultList.size() > 1)
		{
			return false;
		}
		else
		{
			return true;
		}
		
		
	}
	
	
	@Override
	public String updateDelayDeliveryRptForm(
			DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			Boolean sendEmailBoolean) {
		
		
			DelayDeliveryRptForm d = em.find(DelayDeliveryRptForm.class, delayDeliveryRptFormDataIn.getRefNum());
			
			

			if( !delayDeliveryRptFormDataIn.getVersion().equals(d.getVersion()) ) {
				throw new OptimisticLockException();
			}
			
			d = constructDelayDeliveryRptForm( d, delayDeliveryRptFormDataIn);
			
			List<String> fileNoChangeList = checkFileNoChange(d.getDelayDeliveryRptFormPoList(), delayDeliveryRptFormPoDataList);
			
			for(DelayDeliveryRptFormPo p: d.getDelayDeliveryRptFormPoList()){
				if(p.getFileId() != null){
					
					//not physical remove file
					if(!fileNoChangeList.contains(p.getFileId().getFileName()+p.getFileId().getCreateDate())){
						deletedFile(p.getFileId().getFilePath());
					}
					
					em.remove(p.getFileId());
					em.flush();
				}
				em.remove(p);
				em.flush();
			}
			for(DelayDeliveryRptFormContact c: d.getDelayDeliveryRptFormContactList()){
				if(checkContactDelete(c)){
					em.remove(c.getContact());
					em.flush();
				}
				em.remove(c);
				em.flush();
			}
			
			List<DelayDeliveryRptFormContact> dcList = null;
			List<DelayDeliveryRptFormPo> dpList  = null;
			if(delayDeliveryRptFormContactDataList != null){
				dcList  = constructDelayDeliveryRptFormContactList(d, delayDeliveryRptFormContactDataList);
			}
			if(delayDeliveryRptFormPoDataList != null){
				dpList = constructDelayDeliveryRptFormPoList(d, delayDeliveryRptFormPoDataList, fileNoChangeList);
			}
			
			d.setDelayDeliveryRptFormContactList(dcList);
			d.setDelayDeliveryRptFormPoList(dpList);
			DelayDeliveryRptFormVer dVer= new DelayDeliveryRptFormVer();
			dVer.setDelayDeliveryRptForm(d);
			dVer.setStatus(delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus());
			
			if(delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus().equals(DelayDeliveryRptFormStatus.Submitted)){
				d.setReportDate(new Date());
			}
			
			em.persist(dVer);
			em.flush();
			d.setDelayDeliveryRptFormVer(dVer);
			
			//Submitted 
			if(sendEmailBoolean){
				sendEmail(delayDeliveryRptFormDataIn,
						 delayDeliveryRptFormContactDataList,
						 delayDeliveryRptFormPoDataList);
			}
			
			return "R"+d.getRefNum();
		
	}

	
	

	private List<String> checkFileNoChange(
			List<DelayDeliveryRptFormPo> delayDeliveryRptFormPoList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList) {
		
		List<String> resultList = new ArrayList<String>();
		
		for(DelayDeliveryRptFormPo p : delayDeliveryRptFormPoList){
			for(DelayDeliveryRptFormPoData pData : delayDeliveryRptFormPoDataList){
				
				FileItem pFile = p.getFileId();
				FileItem pDataFile = pData.getFileItem();
				
				if(pFile != null && pDataFile != null){
					
					if(StringUtils.equals(pFile.getFileName(),pDataFile.getFileName()) ){
						Date pDate = p.getFileId().getCreateDate();
						Date pDataDate = pData.getFileItem().getCreateDate();
						
						if(pDate != null && pDate.equals(pDataDate)){
							
							resultList.add(p.getFileId().getFileName()+p.getFileId().getCreateDate());
							
						}
						
					}
				}
				
			}
		}
		return resultList;
	}

	@Override
	public DelayDeliveryRptFormData updateDelayDeliveryRptFormStatus(DelayDeliveryRptFormData delayDeliveryRptFormDataIn) {
		
		DelayDeliveryRptForm d = em.find(DelayDeliveryRptForm.class, delayDeliveryRptFormDataIn.getRefNum());
	
		if( !delayDeliveryRptFormDataIn.getVersion().equals(d.getVersion()) ) {
			throw new OptimisticLockException();
		}
		
		DelayDeliveryRptFormVer dVer= new DelayDeliveryRptFormVer();
		dVer.setDelayDeliveryRptForm(d);
		dVer.setStatus(delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus());
		
		em.persist(dVer);
		em.flush();
		d.setDelayDeliveryRptFormVer(dVer);
		String[] stringArr = {"R"+d.getRefNum(), delayDeliveryRptFormDataIn.getDelayDeliveryRptFormStatus().getDisplayValue()};
		return delayDeliveryRptFormDataIn;
		
	}

	
	
	
	
	
	@Override
	public boolean deleteDelayDeliveryRptForm(
			DelayDeliveryRptFormData delayDeliveryRptFormDataIn, String deleteReason) {

		try{
			DelayDeliveryRptForm d = em.find(DelayDeliveryRptForm.class, delayDeliveryRptFormDataIn.getRefNum());
			DelayDeliveryRptFormVer dVer= new DelayDeliveryRptFormVer();
			
			dVer.setDelayDeliveryRptForm(d);
			dVer.setStatus(DelayDeliveryRptFormStatus.Deleted);
			dVer.setReason(deleteReason);
			em.persist(dVer);
			em.flush();
			d.setDelayDeliveryRptFormVer(dVer);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			
			return false;
		}
		
		
	}
	
	
	
	private List<DelayDeliveryRptFormPo> constructDelayDeliveryRptFormPoList(
			DelayDeliveryRptForm delayDeliveryRptForm,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			List<String> fileNoChangeList) {
		
		DelayDeliveryRptFormPo p ;
		ArrayList<DelayDeliveryRptFormPo> list = new ArrayList<DelayDeliveryRptFormPo>();
		for (DelayDeliveryRptFormPoData d : delayDeliveryRptFormPoDataList) {
			
			if(d.getDelayDeliveryRptFormPoId() == null){
				p = new DelayDeliveryRptFormPo();
				p.setApproveDate(d.getApproveDate());
				p.setDelayDeliveryRptForm(delayDeliveryRptForm);
				
				if(d.getFileItem() != null){
					
					if(fileNoChangeList.contains(d.getFileItem().getFileName()+d.getFileItem().getCreateDate())){
						p.setFileId(d.getFileItem());
					}else{
						String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + df.format(new Date()) + PATH_DELIMITER + dmf.format(new Date()) +PATH_DELIMITER + "R"+delayDeliveryRptForm.getRefNum() ;
						uploadFile(d.getFileItem(),
								d.getFileData(),
								dbFilePath,
								null, p);
					}
				}
				
				p.setOrderQty(d.getOrderQty());
				p.setOutstandQty(d.getOutstandQty());
				p.setPoNum(d.getPoNum());
				p.setPackSize(d.getPackSize());
				p.setQuotationLeadTime(d.getQuotationLeadTime());
				p.setSupplierReplyDate(d.getSupplierReplyDate());
				em.persist(p);
				em.flush();
				
			}else{
				p = em.find(DelayDeliveryRptFormPo.class, d.getDelayDeliveryRptFormPoId());
				if(p == null){
					p = new DelayDeliveryRptFormPo();
					p.setApproveDate(d.getApproveDate());
					p.setDelayDeliveryRptForm(delayDeliveryRptForm);
					
					if(d.getFileItem() != null){
						
						if(fileNoChangeList.contains(d.getFileItem().getFileName()+d.getFileItem().getCreateDate())){
							p.setFileId(d.getFileItem());
						}else{
						
						String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + df.format(new Date()) + PATH_DELIMITER + dmf.format(new Date()) +PATH_DELIMITER + "R"+delayDeliveryRptForm.getRefNum() ;
						uploadFile(d.getFileItem(),
								d.getFileData(),
								dbFilePath,
								null, p);
						
						}
					}
					
					
					p.setOrderQty(d.getOrderQty());
					p.setOutstandQty(d.getOutstandQty());
					p.setPoNum(d.getPoNum());
					p.setPackSize(d.getPackSize());
					p.setQuotationLeadTime(d.getQuotationLeadTime());
					p.setSupplierReplyDate(d.getSupplierReplyDate());
					em.persist(p);
					em.flush();
				}else{
					p.setApproveDate(d.getApproveDate());
					p.setDelayDeliveryRptForm(delayDeliveryRptForm);
					p.setFileId(d.getFileItem());
					p.setOrderQty(d.getOrderQty());
					p.setOutstandQty(d.getOutstandQty());
					p.setPoNum(d.getPoNum());
					p.setPackSize(d.getPackSize());
					p.setQuotationLeadTime(d.getQuotationLeadTime());
					p.setSupplierReplyDate(d.getSupplierReplyDate());
				}
			}
			
			list.add(p);
		}
		
		return list;
	}
	
	private List<DelayDeliveryRptFormContact> constructDelayDeliveryRptFormContactList(
			DelayDeliveryRptForm delayDeliveryRptForm,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList) {
		
		ArrayList<DelayDeliveryRptFormContact> list = new ArrayList<DelayDeliveryRptFormContact>();
		for (DelayDeliveryRptFormContactData d : delayDeliveryRptFormContactDataList) {
			
			Contact contact;
			if(d.getContactId() == null){
				contact= new Contact();
				contact.setEmail(d.getEmail());
				contact.setFirstName(d.getFirstName());
				contact.setOfficePhone(d.getOfficePhone());
				contact.setPosition(d.getPosition().getDataValue());
				contact.setTitle(d.getTitle());
				contact.setLastName(d.getLastName());
				em.persist(contact);
				em.flush();
			}else{
				contact = em.find(Contact.class, d.getContactId());
				if(contact != null){
					contact.setEmail(d.getEmail());
					contact.setFirstName(d.getFirstName());
					contact.setOfficePhone(d.getOfficePhone());
					contact.setPosition(d.getPosition().getDataValue());
					contact.setTitle(d.getTitle());
					contact.setLastName(d.getLastName());
				}else{
					contact= new Contact();
					contact.setEmail(d.getEmail());
					contact.setFirstName(d.getFirstName());
					contact.setOfficePhone(d.getOfficePhone());
					contact.setPosition(d.getPosition().getDataValue());
					contact.setTitle(d.getTitle());
					contact.setLastName(d.getLastName());
					em.persist(contact);
					em.flush();
				}
				
				
			}
			DelayDeliveryRptFormContact c = new DelayDeliveryRptFormContact();
			c.setContact(contact);
			c.setDelayDeliveryRptForm(delayDeliveryRptForm);
			c.setPriority(d.getPriority());
			em.persist(c);
			em.flush();
			list.add(c);
		}
		
		
		return list;
	}
	
	
	private DelayDeliveryRptForm constructDelayDeliveryRptForm(
			DelayDeliveryRptForm d,
			DelayDeliveryRptFormData delayDeliveryRptFormData) {
		
		if(delayDeliveryRptFormData.getRefNum() != null){
			
			d.setRefNum(delayDeliveryRptFormData.getRefNum());
		}else{
			
			Long refNum = constructSeq("DDN");
			d.setRefNum(refNum);
			delayDeliveryRptFormData.setRefNum(refNum);
		}
		
		d.setConflictByName(delayDeliveryRptFormData.getConflictByName());
		d.setConflictByRank(delayDeliveryRptFormData.getConflictByRank());
		d.setAvgMthConsumptQty(delayDeliveryRptFormData.getAvgMthConsumptQty());
		d.setConflictFile(delayDeliveryRptFormData.getFileItem());
		if(delayDeliveryRptFormData.getFileItem() != null){
			String dbFilePath= formatFilePath(d) ;
			uploadFile(delayDeliveryRptFormData.getFileItem(),
					delayDeliveryRptFormData.getFileData(),
					dbFilePath,
					d, null);
		}
		
		d.setConflictFlag(delayDeliveryRptFormData.getConflictFlag());
		d.setConflictMspName(delayDeliveryRptFormData.getConflictMspName());
		d.setConflictRelationship(delayDeliveryRptFormData.getConflictRelationship());
		d.setConflictDate(new Date());
		
		if(d.getContract() != null){
			Contract contract = d.getContract();
			
			contract.setItemCode(delayDeliveryRptFormData.getItemCode());
			contract.setContractNum(delayDeliveryRptFormData.getContractNum());
			contract.setContractSuffix(delayDeliveryRptFormData.getContractSuffix());
			contract.setContractType(delayDeliveryRptFormData.getOrderType().getDataValue());
			contract.setBaseUnit(delayDeliveryRptFormData.getBaseUnit());
			
			Supplier supplier = em.find(Supplier.class, delayDeliveryRptFormData.getSupplierCode());
			contract.setSupplier(supplier);
			
			Company manu = em.find(Company.class, delayDeliveryRptFormData.getManufCode());
			contract.setManufacturer(manu);
			
			Company pharmCompany = em.find(Company.class, delayDeliveryRptFormData.getPharmCompanyCode());
			contract.setPharmCompany(pharmCompany);
			
			
			contract.setModuleType(ModuleType.DelayDelivery);
			d.setContract(contract);
		}else{
			
			Contract contract = new Contract();
			
			contract.setItemCode(delayDeliveryRptFormData.getItemCode());
			contract.setContractNum(delayDeliveryRptFormData.getContractNum());
			contract.setContractSuffix(delayDeliveryRptFormData.getContractSuffix());
			contract.setContractType(delayDeliveryRptFormData.getOrderType().getDataValue());
			contract.setBaseUnit(delayDeliveryRptFormData.getBaseUnit());
			
			
			Supplier supplier = em.find(Supplier.class, delayDeliveryRptFormData.getSupplierCode());
			contract.setSupplier(supplier);
			
			Company manu = em.find(Company.class, delayDeliveryRptFormData.getManufCode());
			contract.setManufacturer(manu);
			
			Company pharmCompany = em.find(Company.class, delayDeliveryRptFormData.getPharmCompanyCode());
			contract.setPharmCompany(pharmCompany);
			
			contract.setModuleType(ModuleType.DelayDelivery);
			
			em.persist(contract);
			em.flush();
			d.setContract(contract);
		}
		
		if(delayDeliveryRptFormData.getInstitution() != null){
			Institution inst = em.find(Institution.class, delayDeliveryRptFormData.getInstitution().getInstitutionCode());
			d.setInstitution(inst);
		}
		
		d.setOnHandLastFor(delayDeliveryRptFormData.getOnHandLastFor());
		d.setOnHandQty(delayDeliveryRptFormData.getOnHandQty());
		
		d.setReportDate(new Date());
		
		return d;
	}
	
	private EntityManagerImpl getEmi(EntityManager em) {
		return (EntityManagerImpl) em.getDelegate();
	}
	
	private FuncSeqNum retrieveNextFuncSeqNum(String funcCode){
		logger.debug("retrieveNextFuncSeqNum : #0", funcCode);
		FuncSeqNum funcSeqNum = null;
		try{
			funcSeqNum = this.getEmi(em).find(FuncSeqNum.class, funcCode, LockModeType.WRITE);
			funcSeqNum.setLastNum(funcSeqNum.getLastNum()+1);
		}catch (NoResultException e){
			return null;
		}
		return funcSeqNum;
	}
	
	private Long constructSeq(String funcCode){
		FuncSeqNum funcSeqNum = retrieveNextFuncSeqNum(funcCode);
		
		String seq = funcSeqNum.getLastNum().toString();
		if (seq.length() == 1){
			seq = "0000" +  seq;
		}else if (seq.length() == 2){
			seq = "000" +  seq;
		}else if (seq.length() == 3){
			seq = "00" +  seq;
		}else if (seq.length() == 4){
			seq = "0" +  seq;
		}
		
		Long refNum = Long.parseLong(funcSeqNum.getYear().toString() + seq);
		return refNum;
	}
	
	
	private String constructEmailContent(DelayDeliveryRptFormData delayDeliveryRptFormDataIn,
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList,
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList,
			String emailNumber
			){

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DmDrug dmDrugFind = DmDrugCacher.instance().getDrugByItemCode(delayDeliveryRptFormDataIn.getItemCode());


		StringBuffer emailContent = new StringBuffer();

		emailContent.append("<table>");
		emailContent.append("<tr><td>Dear CPO Colleague, </td></tr>");
		emailContent.append("<tr><td>&nbsp;</td></tr>");
		emailContent.append("<tr><td>Appended below please find the Delayed Delivery Report for your next actions. Thank you.</td></tr>");
		emailContent.append("<tr><td>&nbsp;</td></tr>");
		emailContent.append("</table>");
		
		emailContent.append("<table>");
		emailContent.append("<tr><td>&nbsp;</td></tr>");
		emailContent.append("<tr><td><b><u>"+emailNumber+" Delayed Delivery Report on " + delayDeliveryRptFormDataIn.getItemCode() + " from " + delayDeliveryRptFormDataIn.getInstitution().getInstitutionCode() + " with "+ delayDeliveryRptFormDataIn.getOnHandLastFor().toString() + " month(s) SOH</u></b></td></tr>");
		emailContent.append("<tr><td>&nbsp;</td></tr>");
		emailContent.append("</table>");
		
		emailContent.append("<table>");
		emailContent.append("<tr><td>");
		if (delayDeliveryRptFormDataIn.getConflictFlag()) {
			emailContent.append(CONFLICT_MSG + delayDeliveryRptFormDataIn.getConflictMspName() + CONFLICT_MSG_END);
		} else {
			emailContent.append(NO_CONFLICT_MSG);
		}
		emailContent.append(".</td></tr>");
		emailContent.append("<tr><td>&nbsp;</td></tr>");
		emailContent.append("</table>");

		emailContent.append("<table>");
		emailContent.append("<tr><td>Reference No.</td><td>:</td><td>"+"R"+delayDeliveryRptFormDataIn.getRefNum()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Report Date</td><td>:</td><td>"+dateformatter.format(delayDeliveryRptFormDataIn.getReportDate())+"</td></tr>");
		emailContent.append("<tr><td>Institution</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getInstitution().getInstitutionCode()+"</td></tr>");
		emailContent.append("<tr><td>Item Code</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getItemCode()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Item Description</td><td>:</td><td>"+dmDrugFind.getFullDrugDesc()+"</td></tr>");
		emailContent.append("<tr><td>Order Type</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getOrderType().getDisplayValue()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");

		if("C".equals(delayDeliveryRptFormDataIn.getOrderType().getDataValue()) || 
				"Q".equals(delayDeliveryRptFormDataIn.getOrderType().getDataValue())
		)
		{
			emailContent.append("<td>BPA No.</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getContractNum()+(delayDeliveryRptFormDataIn.getContractSuffix()==null||"".equals(delayDeliveryRptFormDataIn.getContractSuffix().trim())?"":("-"+delayDeliveryRptFormDataIn.getContractSuffix()))+"</td>");
		}
		else
		{
			emailContent.append("<td>BPA No.</td><td>:</td><td> </td>");
		}
		emailContent.append("</tr>");

		
		emailContent.append("<tr><td>Supplier</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getSupplierCode()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Stock On-hand Qty</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getOnHandQty()+" "+dmDrugFind.getBaseUnit()+"</td></tr>");
		emailContent.append("<tr><td>Manufacturer</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getManufCode()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Avg. Monthly Consumption Qty</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getAvgMthConsumptQty()+" "+dmDrugFind.getBaseUnit()+"</td></tr>");
		emailContent.append("<tr><td>Pharmaceutical Company</td><td>:</td><td>"+delayDeliveryRptFormDataIn.getPharmCompanyCode()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><b>Stock On-hand last for</b></td><td>:</td><td>"+delayDeliveryRptFormDataIn.getOnHandLastFor()+ " Month(s)</td></tr>");
		
		emailContent.append("<tr><td></td><td></td><td></td></tr>");
		emailContent.append("<tr><td></td><td></td><td></td></tr>");
	

	
		emailContent.append("<tr><td><u>Purchase Order</u></td><td>:</td><td></td></tr>");
		
		if(!delayDeliveryRptFormPoDataList.isEmpty()){
			for(DelayDeliveryRptFormPoData delayDeliveryRptFormPoData:delayDeliveryRptFormPoDataList)
			{
				
					String supplierDateStr = delayDeliveryRptFormPoData.getSupplierReplyDate() == null ? " Not Specified": dateformatter.format(delayDeliveryRptFormPoData.getSupplierReplyDate());
					String leadTimeStr = (delayDeliveryRptFormPoData.getQuotationLeadTime() == null || delayDeliveryRptFormPoData.getQuotationLeadTime() == 0) ? " Not Specified" : ( delayDeliveryRptFormPoData.getQuotationLeadTime().toString() +" Day(s)");
					String fileExist = delayDeliveryRptFormPoData.getFileItem() == null ? " No":" Yes";
					emailContent.append("<tr><td>PO No.</td><td>:</td><td>"+delayDeliveryRptFormPoData.getPoNum()+"</td></tr>");
					emailContent.append("<tr><td>Approval Date</td><td>:</td><td>"+dateformatter.format(delayDeliveryRptFormPoData.getApproveDate())+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Delivery Lead Time on Quotation</td><td>:</td><td>"+leadTimeStr+"</td></tr>");
					emailContent.append("<tr><td>Order Qty</td><td>:</td><td>"+delayDeliveryRptFormPoData.getOrderQty()+ " " + delayDeliveryRptFormDataIn.getBaseUnit()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Supplier's Reply on Delivery Date</td><td>:</td><td>"+ supplierDateStr +"</td></tr>");
					emailContent.append("<tr><td>Qutstanding Qty</td><td>:</td><td>"+delayDeliveryRptFormPoData.getOutstandQty()+ " " + delayDeliveryRptFormDataIn.getBaseUnit()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Quotation Attached</td><td>:</td><td>"+ fileExist +"</td></tr>");
					emailContent.append("<tr><td>Pack Size(s) Affected</td><td>:</td><td>"+delayDeliveryRptFormPoData.getPackSize()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>");
					emailContent.append("<tr><td></td><td></td><td></td></tr>");
					emailContent.append("<tr><td></td><td></td><td></td></tr>");
			}
		}
			
		emailContent.append("<tr><td></td><td></td><td></td></tr>");
		emailContent.append("<tr><td></td><td></td><td></td></tr>");
			
		emailContent.append("<tr><td><u>Contact Person</u></td><td>:</td><td></td></tr>");
		if(!delayDeliveryRptFormContactDataList.isEmpty()){
			for(DelayDeliveryRptFormContactData delayDeliveryRptFormContactData:delayDeliveryRptFormContactDataList)
			{	
				
					emailContent.append("<tr><td>Priority</td><td>:</td><td>"+delayDeliveryRptFormContactData.getPriority()+"</td></tr>");
					emailContent.append("<tr><td>Title</td><td>:</td><td>"+delayDeliveryRptFormContactData.getTitle()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Rank</td><td>:</td><td>"+delayDeliveryRptFormContactData.getPosition().getDisplayValue()+"</td></tr>");
					emailContent.append("<tr><td>First Name</td><td>:</td><td>"+delayDeliveryRptFormContactData.getFirstName()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Email</td><td>:</td><td>"+delayDeliveryRptFormContactData.getEmail()+"</td></tr>");
					emailContent.append("<tr><td>Last Name</td><td>:</td><td>"+delayDeliveryRptFormContactData.getLastName()+"</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Tel. No.</td><td>:</td><td>"+delayDeliveryRptFormContactData.getOfficePhone()+"</td></tr>");
					emailContent.append("<tr><td></td><td></td><td></td></tr>");
					emailContent.append("<tr><td></td><td></td><td></td></tr>");
			
			}
			
		}

		emailContent.append("<tr><td></td><td></td><td></td></tr>");
		emailContent.append("<tr><td></td><td></td><td></td></tr>");
		emailContent.append("</table>");
		
		
		
		return emailContent.toString();
	}
	
	
	private boolean deletedFile(String dbFilePath){
		
		String diskFilePath = uploadPath + PATH_DELIMITER + dbFilePath ;
	
		File f = new File(diskFilePath);
		if (f.exists()){
			fileItemManager.deleteFile(f);
			logger.info("Process succeed , File deleted : #0", f.getPath());
		}else{
			
			logger.info("Process failure , File cannot deleted : #0", f.getPath());
		}
		
		return true;
	}
	
	private boolean uploadFile(FileItem fileItemIn, byte[] bytes, String dbFilePath, DelayDeliveryRptForm delayDeliveryRptForm , DelayDeliveryRptFormPo delayDeliveryRptFormPo){
		logger.debug("uploadFile");
		boolean success;
		String errorCode;

				
		String fileStrIn = fileItemIn.getFileName().trim();
		logger.debug("uploadFile : #0 ", fileStrIn);
		success = false;
		errorCode ="0019";
		
		
		if (fileStrIn!=null && bytes !=null){
			String ext = fileItemIn.getFileType();

			String diskFilePath = uploadPath + PATH_DELIMITER + dbFilePath ;

			File fDir = new File(diskFilePath);

			if (!fDir.exists()){
				boolean dirCreated = fDir.mkdirs();
				logger.debug("new folder : #0 , #1",dirCreated, fDir.getPath());
			}

			File f = new File(diskFilePath, fileStrIn+"."+ext);
			
			logger.debug("file path : "+ f.getPath());
			int fileNum = 0;
			String newFileName ="";
			if (fDir.exists()){
				while(f.exists()){
					fileNum++;
					newFileName = fileStrIn+"("+ fileNum +")."+ ext;

					f = new File(diskFilePath, newFileName);
					
				}
				
				if (fileItemManager.uploadFile(f, bytes)){
					logger.info("File uploaded : #0", f.getPath());
					
					if(!"".equals(newFileName)){
						fileItemIn.setFileName( fileStrIn+"("+ fileNum +")");
					}else{
						
						fileItemIn.setFileName(fileStrIn);
					}
					
					
					fileItemIn.setFilePath(dbFilePath + PATH_DELIMITER+f.getName());
					
					try {
						em.persist(fileItemIn);
						em.flush();
						if(delayDeliveryRptForm != null){
							delayDeliveryRptForm.setConflictFile(fileItemIn);
						}else{
							delayDeliveryRptFormPo.setFileId(fileItemIn);
						}
						
						success = true;
						errorCode = null;

					}catch(Exception e){
						e.printStackTrace();
						
					}finally{
						if (!success){
							if (f.exists()){
								fileItemManager.deleteFile(f);
								logger.info("Process failure , File deleted : #0", f.getPath());
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	
	
	
	private String formatFilePath(DelayDeliveryRptForm delayDeliveryRptFormDataIn){
		
		String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + df.format(new Date()) + PATH_DELIMITER + dmf.format(new Date()) +PATH_DELIMITER + "R"+ delayDeliveryRptFormDataIn.getRefNum() ;

		return dbFilePath;
	}
	
	
	
	private String retrieveEmailNumber(DelayDeliveryRptFormData delayDeliveryRptFormData) {
		
		StringBuffer sqlString = new StringBuffer();
		
		sqlString.append("select count(o) from DelayDeliveryRptForm o "); 
		sqlString.append("where o.contract.itemCode = :itemCode ");
		sqlString.append("and o.contract.supplier.supplierCode = :supplierCode ");
		sqlString.append("and o.contract.manufacturer.companyCode = :manuCode ");
		sqlString.append("and o.contract.pharmCompany.companyCode = :pharmCompanyCode ");
		sqlString.append("and o.contract.contractType = :orderType ");
		sqlString.append("and o.delayDeliveryRptFormVer.status in :statusList ");
		
		
		List<DelayDeliveryRptFormStatus> statusList = new ArrayList<DelayDeliveryRptFormStatus>();
		
		statusList.add(DelayDeliveryRptFormStatus.Submitted);
		statusList.add(DelayDeliveryRptFormStatus.CpoProcessing);
		
		Query q = em.createQuery(sqlString.toString())
		.setParameter("itemCode", delayDeliveryRptFormData.getItemCode())
		.setParameter("supplierCode", delayDeliveryRptFormData.getSupplierCode())
		.setParameter("manuCode", delayDeliveryRptFormData.getManufCode())
		.setParameter("pharmCompanyCode", delayDeliveryRptFormData.getPharmCompanyCode())
		.setParameter("orderType", delayDeliveryRptFormData.getOrderType().getDataValue())
		.setParameter("statusList", statusList);
		
		Number result = (Number) q.getResultList().get(0);
		int count = result.intValue();

		if(count > 0)
		{
			String resultString ="";
			
			if(count == 1){
				resultString = "1st";
			}else if(count == 2){
				resultString = "2nd";
			}else if(count == 3){
				resultString = "3rd";
			}else{
				resultString = (count ) +"th";
			}
			
			return resultString;
			
		}
		else
		{
			return "";
		}
		
	}
	
	
	
}
