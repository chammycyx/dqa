package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum IAMemoContent implements StringValuedEnum {
	
	IA1("IA1", "I would be grateful if you could provide samples of the above item in original packaging to this office for Continuous Quality Assurance (CQA) testing. The quantity required for testing is "),
	IA2C("IA2C", "The testing samples provided should be taken from the same batch of stock and if possible, with at least 9 months before the expiry date. Fresh stock delivered to your pharmacy within the past few months is preferred."),
	IA2DQ("IA2DQ", "The testing samples provided should be taken from the same batch of stock and if possible, with at least 9 months before the expiry date. Fresh stock delivered to your pharmacy within the past few months is preferred."),
	IA3C("IA3C", "Replacement stock covering the quantity used in the test will be delivered to your pharmacy by the drug supplier. You may arrange stock transfer to QA(CPO) after receiving the replacement stock so that your inventory balance will not be affected."),
	IA3DQ("IA3DQ", "A GF277 voucher covering the quantity used in the test is attached with this memorandum for the transfer of testing sample to QA(CPO)."),
	IA4C("IA4C", "You are required to suspend {0} from the same batch of stock as retention sample and enter the transaction of the stock transfer in ERP system accordingly. The suspended retention sample should be kept at a distinct location under proper storage conditions and be available upon request from CPO if a re-test is required."),
	IA4DQ("IA4DQ", "You are required to suspend {0} from the same batch of stock as retention sample and enter the transaction of the stock transfer in ERP system accordingly. The suspended retention sample should be kept at a distinct location under proper storage conditions and be available upon request from CPO if a re-test is required."),
	IA5C("IA5C", "If the replacement stock is not delivered to you within 2 weeks, please contact "),
	IA5DQ("IA5DQ", "Your follow-up actions within 7 days would be much appreciated. Should you have any enquiries, please contact "),
	IS1("IS1", "Please be informed that sample testing will be conducted for the captioned item under our Continuous Quality Assurance Program. As stipulated in the contract document, the contractor is required to reimburse the costs of the laboratory tests to Hospital Authority and replenish the goods drawn for the said analysis to the selected hospital."),
	IS2("IS2", "In this connection, please make a cheque for HK${0} payable to the 'Hospital Authority'. The cheque should be delivered to my attention at the address printed below. The following goods in original packing are requested to be sent to {1} within seven days:"),
	IS3("IS3", "Contract Number: {0} \nDescription: {1} \nQuantity Required: {2}"),
	IS4("IS4", "For further clarification, please contact "),
	IS5("IS5", "Thank you for your attention.");
	
    private final String dataValue;
    private final String displayValue;
        
    IAMemoContent(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<IAMemoContent> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<IAMemoContent> getEnumClass() {
    		return IAMemoContent.class;
    	}
    }
}
