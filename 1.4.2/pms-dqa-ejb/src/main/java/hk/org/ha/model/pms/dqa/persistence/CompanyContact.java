package hk.org.ha.model.pms.dqa.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@NamedQueries({
		@NamedQuery(name = "CompanyContact.findByCompanyCodeContactId", query = "select o from CompanyContact o  where o.company.companyCode = :companyCode and o.contact.contactId = :contactId  "),
		@NamedQuery(name = "CompanyContact.findByCompanyCode", query = "select o from CompanyContact o  where (o.company.companyCode = :manufCode or o.company.companyCode = :pharmCompanyCode) order by o.company.companyCode, o.contact.firstName "),
		@NamedQuery(name = "CompanyContact.findByFirstNameLastNameEmail", query = "select o.contact.contactId from CompanyContact o where upper(o.contact.firstName) =:firstName " +
		"and upper(o.contact.lastName) =:lastName and upper(o.contact.email) =:email and o.company.companyCode = :companyCode ")
	})
@Table(name = "COMPANY_CONTACT")
@Customizer(AuditCustomizer.class)
public class CompanyContact extends VersionEntity{

	private static final long serialVersionUID = -3804653001020655930L;

	@Id
	@Column(name = "COMPANY_CONTACT_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CompanyContactSeq")
	@SequenceGenerator(name = "CompanyContactSeq", sequenceName = "SEQ_COMPANY_CONTACT", initialValue=10000)
	private Long companyContactId;
	
	@ManyToOne
	@JoinColumn(name="COMPANY_CODE")
	private Company company;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setCompanyContactId(Long companyContactId) {
		this.companyContactId = companyContactId;
	}

	public Long getCompanyContactId() {
		return companyContactId;
	}

}
