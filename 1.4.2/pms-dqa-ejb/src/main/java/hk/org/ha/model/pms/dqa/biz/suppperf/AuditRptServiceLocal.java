package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRptCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AuditRptServiceLocal {

	public List<AuditRpt> retrieveAuditRpt(AuditRptCriteria auditRptCriteria);
	
	void destroy();

	public List<AuditRpt> getAuditRptList();
}
