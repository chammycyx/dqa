package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;

import javax.ejb.Local;

@Local
public interface ProblemNatureCatServiceLocal {
	
	void addProblemNatureCat();
	
	void createProblemNatureCat(ProblemNatureCat problemNatureCatIn);
	
	void deleteProblemNatureCat(ProblemNatureCat problemNatureCatIn);
	
	void updateProblemNatureCat(ProblemNatureCat problemNatureCatIn);
	
	void retrieveProblemNatureCatByProblemNatureCatId(ProblemNatureCat problemNatureCatIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
