package hk.org.ha.model.pms.dqa.persistence.coa;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import java.util.List;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.JoinFetch;

@Entity
@Table(name = "COA_ITEM")
@NamedQueries({
	@NamedQuery(name = "CoaItem.findCoaItemForCoaItemMaint", query = 
		"select o from CoaItem o where " +
		"o.orderType in(:orderType1 , :orderType2) " +
		"and o.contract.itemCode = :itemCode " +
		"and o.contract.contractNum = :contractNum " +
		"and o.contract.moduleType = :moduleType"),
	@NamedQuery(name = "CoaItem.findCoaItemForCoaItemMaintDType", query = 
		"select o from CoaItem o where o.contract.supplier.supplierCode = :supplierCode and " +
		"o.orderType = :orderType " +
		"and o.contract.itemCode = :itemCode " +
		"and o.contract.contractNum is null " +
		"and o.contract.moduleType = :moduleType"
		),
	@NamedQuery(name = "CoaItem.findByContract", query = "select o from CoaItem o where o.contract = :contract ")
})
@Customizer(AuditCustomizer.class)
public class CoaItem extends VersionEntity {

	private static final long serialVersionUID = 5499828606392435963L;

	@Id
	@Column(name="COA_ITEM_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaItemSeq")
	@SequenceGenerator(name="coaItemSeq", sequenceName="SEQ_COA_ITEM", initialValue=10000)
	private Long coaItemId;


	@Converter(name = "CoaItem.orderType", converterClass = OrderType.Converter.class )
	@Convert("CoaItem.orderType")
	@Column(name="ORDER_TYPE", length = 1, nullable = false)
	private OrderType orderType;

	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	@JoinFetch
	@JoinColumn(name="COA_BATCH_ID")
	private CoaBatch coaBatch;

	@OneToMany(mappedBy="coaItem", cascade=CascadeType.REMOVE)
	private List<CoaBatch> coaBatchList;   

	@OneToOne
	@JoinColumn(name = "CONTRACT_ID")
	private Contract contract;
	
	public CoaItem() {
	}

	public Long getCoaItemId() {
		return coaItemId;
	}

	public void setCoaItemId(Long coaItemId) {
		this.coaItemId = coaItemId;
	}

	public void setCoaBatchList(List<CoaBatch> coaBatchList) {
		this.coaBatchList = coaBatchList;
	}

	public List<CoaBatch> getCoaBatchList() {
		return coaBatchList;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Contract getContract() {
		return contract;
	}

	public void setCoaBatch(CoaBatch coaBatch) {
		this.coaBatch = coaBatch;
	}

	public CoaBatch getCoaBatch() {
		return coaBatch;
	}

}

