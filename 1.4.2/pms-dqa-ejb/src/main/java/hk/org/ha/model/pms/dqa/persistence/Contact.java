package hk.org.ha.model.pms.dqa.persistence;

import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.hibernate.validator.Email;
import org.hibernate.validator.Length;
import org.hibernate.validator.Pattern;

@Entity
@NamedQueries({
	@NamedQuery(name = "Contact.findAll", query = "select o from Contact o order by o.contactId"),
	@NamedQuery(name = "Contact.findByContactId", query = "select o from Contact o where o.contactId = :contactId"),
	@NamedQuery(name = "Contact.findByFirstNameLastNameEmail", query = "select o.contactId from Contact o where upper(o.firstName) =:firstName " +
			"and upper(o.lastName) =:lastName and upper(o.email) =:email")
})
@Table(name = "CONTACT")
public class Contact extends VersionEntity {

	private static final long serialVersionUID = 7616154416167075815L;

	@Id
    @Column(name="CONTACT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ContactSeq")
	@SequenceGenerator(name = "ContactSeq", sequenceName = "SEQ_CONTACT", initialValue=10000)
    private Long contactId;
	
	@Column(name="FIRST_NAME", length = 100)
	@Length(min = 2, max = 100)
	@Pattern(regex="^[a-zA-Z ()]{2,100}$", message="Invalid username.")
    private String firstName;
	
	@Column(name="LAST_NAME", length = 100)
	@Length(min = 2, max = 100)
	@Pattern(regex="^[a-zA-Z ()]{2,100}$", message="Invalid username.")
    private String lastName;
	
	@Converter(name = "Contact.title", converterClass = TitleType.Converter.class )
	@Convert("Contact.title")
	@Column(name="TITLE", length = 5)
    private TitleType title;
    
	@Column(name="POSITION", length = 50)
	@Length(max = 50)
    private String position;
	
    @Column(name="OFFICE_PHONE", length = 20)
    @Length(min = 8, max = 20)
    private String officePhone;
    
    @Column(name="MOBILE_PHONE", length = 20)
    @Length(min = 8, max =20)
    private String mobilePhone;
    
    @Column(name="FAX", length = 20)
    private String fax;
    
    @Column(name="EMAIL", length = 256)
    @Email
    private String email;
    
    @Column(name="ADDRESS1", length = 100)
    private String address1;
    
    @Column(name="ADDRESS2", length = 100)
    private String address2;
    
    @Column(name="ADDRESS3", length = 100)
    private String address3;
    
    @Column(name="OPT_CONTACT_POINT", length = 1)
    private String optContactPoint;
     
    @Column(name="REMARK", length = 300)
    private String remark;
    
    @Transient
	private boolean enable;
    
	public Contact(){
    }

	public String constructSupplierInfo() {
		return "(Attn: " + (getTitle() == null?"":getTitle()) + (StringUtils.isBlank(getFirstName())?"":" " + getFirstName()) + 
		(StringUtils.isBlank(getLastName())?"":" " + getLastName()) + (StringUtils.isBlank(getPosition())?"":" " + getPosition()) + ")";
	}
	
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getContactId() {
		return contactId;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getOptContactPoint() {
		return optContactPoint;
	}

	public void setOptContactPoint(String optContactPoint) {
		this.optContactPoint = optContactPoint;
	}

	public void setTitle(TitleType title) {
		this.title = title;
	}

	public TitleType getTitle() {
		return title;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isEnable() {
		return enable;
	}
   
}
