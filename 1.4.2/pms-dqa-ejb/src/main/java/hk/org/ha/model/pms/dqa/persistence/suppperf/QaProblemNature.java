package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_PROBLEM_NATURE")
/*@NamedQueries({
	@NamedQuery(name = "QaProblemNature.findByQaProblemNatureId", query = "select o from QaProblemNature o where o.qaProblemNatureId = :qaProblemNatureId ")
})*/
@Customizer(AuditCustomizer.class)
public class QaProblemNature extends VersionEntity {

	private static final long serialVersionUID = -8839873406948823013L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaProblemNatureSeq")
	@SequenceGenerator(name = "qaProblemNatureSeq", sequenceName = "SEQ_QA_PROBLEM_NATURE", initialValue=10000)
	@Id
	@Column(name="QA_PROBLEM_NATURE_ID", nullable=false)
	private Long qaProblemNatureId;


	@Converter(name = "QaProblemNature.physicalDefectFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemNature.physicalDefectFlag")
	@Column(name="PHYSICAL_DEFECT_FLAG", length=1)
	private YesNoFlag physicalDefectFlag;
	
	@Converter(name = "QaProblemNature.packagingDefectFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemNature.packagingDefectFlag")
	@Column(name="PACKAGING_DEFECT_FLAG", length=1)
	private YesNoFlag packagingDefectFlag;
	
	@Converter(name = "QaProblemNature.appearDiscFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemNature.appearDiscFlag")
	@Column(name="APPEAR_DISC_FLAG", length=1)
	private YesNoFlag appearDiscFlag;
	
	@Converter(name = "QaProblemNature.foreignMatterFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemNature.foreignMatterFlag")
	@Column(name="FOREIGN_MATTER_FLAG", length=1)
	private YesNoFlag foreignMatterFlag;
	
	@Converter(name = "QaProblemNature.otherFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemNature.otherFlag")
	@Column(name="OTHER_FLAG", length=1)
	private YesNoFlag otherFlag;
	
	@Column(name="PROBLEM_DETAIL", length=500)
	private String problemDetail;

	public Long getQaProblemNatureId() {
		return qaProblemNatureId;
	}

	public void setQaProblemNatureId(Long qaProblemNatureId) {
		this.qaProblemNatureId = qaProblemNatureId;
	}

	public YesNoFlag getPhysicalDefectFlag() {
		return physicalDefectFlag;
	}

	public void setPhysicalDefectFlag(YesNoFlag physicalDefectFlag) {
		this.physicalDefectFlag = physicalDefectFlag;
	}

	public YesNoFlag getPackagingDefectFlag() {
		return packagingDefectFlag;
	}

	public void setPackagingDefectFlag(YesNoFlag packagingDefectFlag) {
		this.packagingDefectFlag = packagingDefectFlag;
	}

	public YesNoFlag getAppearDiscFlag() {
		return appearDiscFlag;
	}

	public void setAppearDiscFlag(YesNoFlag appearDiscFlag) {
		this.appearDiscFlag = appearDiscFlag;
	}

	public YesNoFlag getForeignMatterFlag() {
		return foreignMatterFlag;
	}

	public void setForeignMatterFlag(YesNoFlag foreignMatterFlag) {
		this.foreignMatterFlag = foreignMatterFlag;
	}

	public YesNoFlag getOtherFlag() {
		return otherFlag;
	}

	public void setOtherFlag(YesNoFlag otherFlag) {
		this.otherFlag = otherFlag;
	}

	public String getProblemDetail() {
		return problemDetail;
	}

	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	
}
