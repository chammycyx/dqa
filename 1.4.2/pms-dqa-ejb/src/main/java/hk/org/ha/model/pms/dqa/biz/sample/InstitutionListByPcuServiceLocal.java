package hk.org.ha.model.pms.dqa.biz.sample;

import javax.ejb.Local;

@Local
public interface InstitutionListByPcuServiceLocal {
	
	void retrieveInstitutionListByPcu(String pcuCode);
	
	void destroy();
}
