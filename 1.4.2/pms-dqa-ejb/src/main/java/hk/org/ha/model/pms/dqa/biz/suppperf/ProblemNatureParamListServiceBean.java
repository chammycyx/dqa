package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureParamListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ProblemNatureParamListServiceBean implements ProblemNatureParamListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<ProblemNatureParam> problemNatureParamList;
	
	@SuppressWarnings("unchecked")
	public void retrieveProblemNatureParamList(){
		logger.debug("retrieveProblemNatureParamList");
		
		problemNatureParamList = em.createNamedQuery("ProblemNatureParam.findByRecordStatus")
		.setParameter("recordStatus", RecordStatus.Active)
		.getResultList();

	}
	
	@Remove
	public void destroy(){
		if(problemNatureParamList != null) {
			problemNatureParamList = null;  
		}
	}

}
