package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ReleaseIndicator implements StringValuedEnum {
	Yes("Y","Yes"),
	No("N", "No"),
	X("X", "Not Applicable");

	ReleaseIndicator(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	private final String dataValue;
	private final String displayValue;

	@Override
	public String getDataValue() {
		return this.dataValue;
	}

	@Override
	public String getDisplayValue() {
		return this.displayValue;
	}  

	public static class Converter extends StringValuedEnumConverter<ReleaseIndicator> {

		private static final long serialVersionUID = -6078866455215937858L;

		@Override
    	public Class<ReleaseIndicator> getEnumClass() {
    		return ReleaseIndicator.class;
    	}
    }
}
