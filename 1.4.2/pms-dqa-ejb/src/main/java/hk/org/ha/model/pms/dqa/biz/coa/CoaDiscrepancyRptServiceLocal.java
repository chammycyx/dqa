package hk.org.ha.model.pms.dqa.biz.coa;
import java.util.List;

import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptData;

import javax.ejb.Local;

@Local
public interface CoaDiscrepancyRptServiceLocal {
	
	void retrieveCoaDiscrepancyList(CoaDiscrepancyRptCriteria coaDiscrepancyRptCriteria);
	
	List<CoaDiscrepancyRptData> getCoaDiscrepancyRptList();
	
	String getReportName();

	String getReportParameters();

	String getRetrieveDate();
	
	void destroy();
	
}
