package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("batchSuspService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class BatchSuspServiceBean implements BatchSuspServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private BatchSusp batchSusp;
	
	public static final EventLogComparator EVENT_LOG_COMPARATOR = new EventLogComparator();
	
	public void retrieveBatchSuspByBatchSusp(BatchSusp batchSuspIn){	
		logger.debug("retrieveBatchSuspByBatchSusp");
								
		List<BatchSusp> batchSusps = em.createNamedQuery("BatchSusp.findByBatchSuspId")
									   	.setParameter("batchSuspId", batchSuspIn.getBatchSuspId())
									   	.setParameter("recordStatus", RecordStatus.Active)
									   	.setHint(QueryHints.FETCH, "o.institution")
										.setHint(QueryHints.FETCH, "o.qaBatchNum")
										.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem")
										.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.problemHeader")
										.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.supplier")
										.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.manufacturer")
										.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.pharmCompany")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.pharmProblemList")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaBatchNumList")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaProblemNature")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.classification")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaProblemFileList")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.eventLogList")
										.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.faxSummary")
										.getResultList();

		if(batchSusps==null || batchSusps.size()==0)
		{
			batchSusp = null;
		}
		else
		{
			batchSusp = batchSusps.get(0);
			getLazyQaProblem(batchSusp.getQaBatchNum().getQaProblem());
		}
			
	}
	
	public void createBatchSusp(List<BatchSusp> batchSuspListIn){
		logger.debug("createBatchSusp");
		
		List<BatchSusp> batchSusps;
		
		for(BatchSusp bs:batchSuspListIn)
		{
			batchSusps = em.createNamedQuery("BatchSusp.findByInstitutionCodeCaseNumBatchNum")
							.setParameter("institutionCode", bs.getInstitution().getInstitutionCode())
						   	.setParameter("caseNum", bs.getQaBatchNum().getQaProblem().getCaseNum())
						   	.setParameter("batchNum", bs.getQaBatchNum().getBatchNum())
						   	.setParameter("recordStatus", RecordStatus.Active)
						   	.setHint(QueryHints.FETCH, "o.institution")
							.setHint(QueryHints.FETCH, "o.qaBatchNum")
							.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem")
							.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.problemHeader")
							.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.supplier")
							.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.manufacturer")
							.setHint(QueryHints.FETCH, "o.qaBatchNum.qaProblem.pharmCompany")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.pharmProblemList")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaBatchNumList")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaProblemNature")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.classification")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.qaProblemFileList")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.eventLogList")
							.setHint(QueryHints.BATCH, "o.qaBatchNum.qaProblem.faxSummary")
							.getResultList();
			
			if(batchSusps==null || batchSusps.size()==0)
			{
				em.persist(bs);
				em.flush();
			}
			else
			{
				BatchSusp bsMerge = batchSusps.get(0);
				bsMerge.setBatchSuspQtyOnHand(bs.getBatchSuspQtyOnHand());
				em.merge(bsMerge);
				em.flush();
			}
		}
	}
	
	public void updateBatchSusp(BatchSusp batchSuspIn){
		logger.debug("updateBatchSusp");
		
		em.merge(batchSuspIn);
		em.flush();
	}
	
	public void deleteBatchSusp(BatchSusp batchSuspIn){
		logger.debug("deleteBatchSusp");
		
		batchSuspIn.setInstitution(null);
		batchSuspIn.setQaBatchNum(null);
		BatchSusp batchSuspTemp = em.merge(batchSuspIn);
		em.remove(batchSuspTemp);
	}	
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		Collections.sort(qaProblemLazy.getEventLogList(), EVENT_LOG_COMPARATOR);
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
			pc.getPharmBatchNumList();
			pc.getPharmBatchNumList().size();
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
		}
		
		return qaProblemLazy;
	}
	
	
	private static class EventLogComparator implements Comparator<EventLog>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private Date getEventLogDate(EventLog el) {
        	return el.getEventLogDate();
        }
        
        public int compare(EventLog el1, EventLog el2) {

            Date date1 = getEventLogDate(el1);
            Date date2 = getEventLogDate(el2);

            if (!date1.equals(date2)) {
            	return date1.compareTo(date2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}
	
	@Remove
	public void destroy(){
		if(batchSusp!=null) {
			batchSusp = null;
		}
	}
}
