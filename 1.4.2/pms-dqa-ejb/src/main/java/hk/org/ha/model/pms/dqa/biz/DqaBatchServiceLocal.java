package hk.org.ha.model.pms.dqa.biz;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.dqa.interfaces.DqaBatchServiceJmsRemote;

@Local
public interface DqaBatchServiceLocal extends DqaBatchServiceJmsRemote {

}
