package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("erpContractMspListService")
@RemoteDestination
@MeasureCalls
public class ErpContractMspListServiceBean implements ErpContractMspListServiceLocal {
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;	
	
	@SuppressWarnings("unchecked")
	public List<ContractMsp> retrieveContractMspListByContractId( Long contractId ){
		logger.debug("retrieveContractMspListByContractId #0", contractId);
		return (List<ContractMsp>)em.createNamedQuery("ContractMsp.findByContractId")
				 .setParameter("contractId", contractId)
				 .getResultList();
	}
	
	public void updateContractMspListForLogicallyDeleted( Long contractId ) {
		logger.debug("updateContractMspListForLogicallyDeleted #0", contractId);
		List<ContractMsp> contractMspList = retrieveContractMspListByContractId( contractId );
		for( ContractMsp cMsp : contractMspList ) {
			cMsp.setRecordStatus( RecordStatus.Deleted );
			em.merge( cMsp );
		}
		em.flush();
	}
	
}
