package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.vo.suppperf.ConflictOfInterestRpt;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("conflictOfInterestRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ConflictOfInterestRptServiceBean implements ConflictOfInterestRptServiceLocal {

	@Logger
	private Log logger;
	
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	private JRDataSource dataSource;
	
	public void createConflictOfInterestRpt(ConflictOfInterestRpt conflictOfInterestRptIn) {
		logger.debug("createConflictOfInterestRpt");				
		
		List<ConflictOfInterestRpt> conflictOfInterestRptList = new ArrayList<ConflictOfInterestRpt>();		
		conflictOfInterestRptList.add(conflictOfInterestRptIn);		
		dataSource = new JRBeanCollectionDataSource(conflictOfInterestRptList);		
	}
	
	public void generateConflictOfInterestRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/conflictOfInterestRpt.jasper", parameters, dataSource);
		
		reportProvider.redirectReport(contentId);
	}
	
	@Remove
	public void destroy(){			
	}
}
