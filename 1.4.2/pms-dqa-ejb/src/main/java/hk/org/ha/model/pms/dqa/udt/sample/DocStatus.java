package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum DocStatus implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	Available("A", "Available"),
	NotAvailable("NA", "Not Available"),
	AvailablePending("AP", "Available Pending"),
	NotApplicable("NAPP", "Not applicable");
	
    private final String dataValue;
    private final String displayValue;
        
    DocStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<DocStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<DocStatus> getEnumClass() {
    		return DocStatus.class;
    	}
    }
}
