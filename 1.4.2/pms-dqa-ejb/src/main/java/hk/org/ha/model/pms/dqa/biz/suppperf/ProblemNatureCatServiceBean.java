package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureCatService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class ProblemNatureCatServiceBean implements ProblemNatureCatServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private ProblemNatureCat problemNatureCat;
	
	private boolean success;
	
	private String errorCode;

		
	public void addProblemNatureCat(){
		logger.debug("addProblemNatureCat");
		
		problemNatureCat = new ProblemNatureCat();
	}
	
	public void createProblemNatureCat(ProblemNatureCat problemNatureCatIn){
		logger.debug("createProblemNatureCat");
		success = false;
		
		List<ProblemNatureCat> problemNatureCats = em.createNamedQuery("ProblemNatureCat.findByCatDescParamId")
												.setParameter("catDesc", problemNatureCatIn.getCatDesc())
												.setParameter("problemNatureParamId", problemNatureCatIn.getProblemNatureParam().getProblemNatureParamId())
												.setParameter("recordStatus", RecordStatus.Active)
												.getResultList();
		
		if(problemNatureCats==null || problemNatureCats.size()==0)
		{
			em.persist(problemNatureCatIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
		
	}
	
	public void updateProblemNatureCat(ProblemNatureCat problemNatureCatIn){
		logger.debug("updateProblemNatureCat");
		success = false;
		
		List<ProblemNatureCat> problemNatureCats = em.createNamedQuery("ProblemNatureCat.findByCatDescParamId")
												.setParameter("catDesc", problemNatureCatIn.getCatDesc())
												.setParameter("problemNatureParamId", problemNatureCatIn.getProblemNatureParam().getProblemNatureParamId())
												.setParameter("recordStatus", RecordStatus.Active)
												.getResultList();
		
		if(problemNatureCats==null || problemNatureCats.size()==0)
		{
			em.merge(problemNatureCatIn);
			em.flush();
			
			success = true;
		}
		else if(problemNatureCats.get(0).getProblemNatureCatId().equals(problemNatureCatIn.getProblemNatureCatId()))
		{
			em.merge(problemNatureCatIn);
			em.flush();
			
			success = true;
		}
		else
		{
			success = false;
			errorCode = "0007";
		}
	}
	
	public void deleteProblemNatureCat(ProblemNatureCat problemNatureCatIn){
		logger.debug("deleteProblemNatureCat");
		
		em.createNamedQuery("ProblemNatureCat.deleteByProblemNatureCatId")
										.setParameter("problemNatureCatId", problemNatureCatIn.getProblemNatureCatId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
		
		em.createNamedQuery("ProblemNatureSubCat.deleteByProblemNatureCatId")
										.setParameter("problemNatureCatId", problemNatureCatIn.getProblemNatureCatId())
										.setParameter("recordStatus", RecordStatus.Deleted)
										.executeUpdate();
	}
	
	public void retrieveProblemNatureCatByProblemNatureCatId(ProblemNatureCat problemNatureCatIn){
		logger.debug("retrieveProblemNatureCatByProblemNatureCatId");
		
		List<ProblemNatureCat> problemNatureCats = em.createNamedQuery("ProblemNatureCat.findByProblemNatureCatId")
											.setParameter("problemNatureCatId", problemNatureCatIn.getProblemNatureCatId())
											.setParameter("recordStatus", RecordStatus.Active)
											.getResultList();

		if(problemNatureCats==null || problemNatureCats.size()==0)
		{
			problemNatureCat = null;
		}
		else
		{
			problemNatureCat = problemNatureCats.get(0);
		}
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
		
	@Remove
	public void destroy(){
		if (problemNatureCat != null){
			problemNatureCat = null;
		}
	}
}
