package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PHARM_PROBLEM_NATURE")
@Customizer(AuditCustomizer.class)
public class PharmProblemNature extends VersionEntity {

	private static final long serialVersionUID = 908329040076159469L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmProblemNatureSeq")
	@SequenceGenerator(name = "pharmProblemNatureSeq", sequenceName = "SEQ_PHARM_PROBLEM_NATURE", initialValue=10000)
	@Id
	@Column(name="PHARM_PROBLEM_NATURE_ID", nullable=false)
	private Long pharmProblemNatureId;

	@Converter(name = "PharmProblemNature.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("PharmProblemNature.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@ManyToOne
	@JoinColumn(name="PROBLEM_NATURE_SUB_CAT_ID")
	private ProblemNatureSubCat problemNatureSubCat;
	
	@ManyToOne
	@JoinColumn(name="PHARM_PROBLEM_ID")
	private PharmProblem pharmProblem;
	

	
	public Long getPharmProblemNatureId() {
		return pharmProblemNatureId;
	}

	public void setPharmProblemNatureId(Long pharmProblemNatureId) {
		this.pharmProblemNatureId = pharmProblemNatureId;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public ProblemNatureSubCat getProblemNatureSubCat() {
		return problemNatureSubCat;
	}

	public void setProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCat) {
		this.problemNatureSubCat = problemNatureSubCat;
	}

	public PharmProblem getPharmProblem() {
		return pharmProblem;
	}

	public void setPharmProblem(PharmProblem pharmProblem) {
		this.pharmProblem = pharmProblem;
	}


}
