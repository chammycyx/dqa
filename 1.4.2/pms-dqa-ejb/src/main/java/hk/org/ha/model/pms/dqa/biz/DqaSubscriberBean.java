package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.remote.JmsServiceProxy;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;

import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("dqaSubscriber")
@MeasureCalls
public class DqaSubscriberBean implements DqaSubscriberLocal {
	
	@Logger
	private Log logger;
	
	@In
	private DmDrugCacher dmDrugCacher;
	
	private Random random = new Random();
	
	private int randomSleepTime = 3*60*1000;

	public int getRandomSleepTime() {
		return randomSleepTime;
	}

	public void setRandomSleepTime(int randomSleepTime) {
		this.randomSleepTime = randomSleepTime;
	}
	
	public void initCache() 
	{
		initCacheWithDmDrugList(null);
	}
	
	public void initCacheWithDmDrugList(List<DmDrug> dmDrugList) 
	{
		if (dmDrugList == null) {

			dmDrugCacher.clear();
			
			int sleepTime = random.nextInt(randomSleepTime);		
			logger.info("initCache : start, random sleep for #0", sleepTime);		
	
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
			
			long startTime = System.currentTimeMillis();
			dmDrugCacher.load();
			logger.info("initCache : time used #0", System.currentTimeMillis() - startTime);		
			
		} else {
			
			logger.info("initCache : received dmDrugList.size:" + dmDrugList.size());
			dmDrugCacher.initDrugCache(dmDrugList);
			logger.info("initCache : done!");
		
		}
	}

	public void initJmsServiceProxy() {
		JmsServiceProxy.REINITING.get().set(true);
		try {
			Events.instance().raiseEvent(JmsServiceProxy.EVENT_REINIT_PROXY);
		} finally {
			JmsServiceProxy.REINITING.get().set(false);
		}
	}	
}
