package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_BATCH_NUM")
@Customizer(AuditCustomizer.class)
public class QaBatchNum extends VersionEntity {

	private static final long serialVersionUID = -535037484067018062L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaBatchNumSeq")
	@SequenceGenerator(name = "qaBatchNumSeq", sequenceName = "SEQ_QA_BATCH_NUM", initialValue=10000)
	@Id
	@Column(name="QA_BATCH_NUM_ID", nullable=false)
	private Long qaBatchNumId;

	@Column(name="BATCH_NUM", length=20, nullable=false)
	private String batchNum;
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	
	@OneToMany(mappedBy="qaBatchNum")
    private List<BatchSusp> batchSuspList;

	@Transient
	private String actionType;
	
	

	public Long getQaBatchNumId() {
		return qaBatchNumId;
	}

	public void setQaBatchNumId(Long qaBatchNumId) {
		this.qaBatchNumId = qaBatchNumId;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	
	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}
	
	public List<BatchSusp> getBatchSuspList() {
		return batchSuspList;
	}

	public void setBatchSuspList(List<BatchSusp> batchSuspList) {
		this.batchSuspList = batchSuspList;
	}
	
	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

}
