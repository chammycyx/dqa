package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("coaDiscrepancyListManager")
public class CoaDiscrepancyListManagerBean implements CoaDiscrepancyListManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<CoaDiscrepancy> getAllRelatedCoaDiscrepancyListByCoaBatchPassFlag(CoaItem coaItem, PassFlag passFlag) {
		return em.createQuery("select o from CoaDiscrepancy o " +
					"where o.coaBatch.coaItem = :coaItem " +					
					"and o.passFlag = :passFlag " +
					"and o.coaDiscrepancyKey.recordStatus = :recordStatus " +
					"order by o.coaDiscrepancyKey.orderSeq asc, o.coaBatch.batchNum asc")
					.setHint(QueryHints.FETCH, "o.coaBatch")
					.setHint(QueryHints.FETCH, "o.coaDiscrepancyKey")
					.setHint(QueryHints.LEFT_FETCH, "o.parentCoaDiscrepancy")
					.setHint(QueryHints.LEFT_FETCH, "o.parentCoaDiscrepancy.coaBatch")
	                .setParameter("coaItem", coaItem)
	                .setParameter("passFlag", passFlag)
	                .setParameter("recordStatus", RecordStatus.Active)
	                .getResultList();
	}
	
}
