package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.SystemManualFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.suppperf.FaxType;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxDetailLogRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("eventLogListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class EventLogListServiceBean implements EventLogListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<EventLog> eventLogList;
	
	private static final String REPORT_NAME = "Fax Detail Log Report";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	
	private String reportParameters = "";
	private String retrieveDate;
	private SimpleDateFormat sdf;
	
	private List<FaxDetailLogRpt> faxDetailLogRptList;
	
	private boolean success;
	
	private String errorCode;
	
	public static final EventLogComparator EVENT_LOG_COMPARATOR = new EventLogComparator();
	
	public void retrieveEventLogListByQaProblemCriteria(QaProblemCriteria qaProblemCriteriaIn){
		logger.debug("retrieveEventLogListByQaProblemCriteria");
		
		StringBuffer eventLogListSql = new StringBuffer();
		eventLogListSql.append("select o from EventLog o "); 
		eventLogListSql.append("where (o.eventLogDate >= :fromEventLogDate or :fromEventLogDate is null) ");
		eventLogListSql.append("and (o.eventLogDate < :toEventLogDate or :toEventLogDate is null) ");
		eventLogListSql.append("and (o.qaProblem.recordStatus =:recordStatus ) ");
		eventLogListSql.append("and (o.eventType =:eventType ) ");
		eventLogListSql.append("and (o.faxLogFlag =:faxLogFlag ) ");
		
		if(qaProblemCriteriaIn.getItemCode()!=null){
			eventLogListSql.append("and (o.qaProblem.problemHeader.itemCode = :itemCode or :itemCode is null) ");}
		if(qaProblemCriteriaIn.getSupplierCode()!=null){
			eventLogListSql.append("and (o.qaProblem.supplier.supplierCode = :supplierCode or :supplierCode is null) ");}
		if(qaProblemCriteriaIn.getManufCode()!=null){
			eventLogListSql.append("and (o.qaProblem.manufacturer.companyCode = :manufCode or :manufCode is null) ");}
		if(qaProblemCriteriaIn.getPharmCompanyCode()!=null){
			eventLogListSql.append("and (o.qaProblem.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");}
		if(qaProblemCriteriaIn.getCaseNum()!=null){
			eventLogListSql.append("and (o.qaProblem.caseNum = :caseNum or :caseNum is null) ");}
		if(qaProblemCriteriaIn.getCaseStatus()!=null){
			eventLogListSql.append("and (o.qaProblem.caseStatus = :caseStatus or :caseStatusDataValue is null) ");}
		if(qaProblemCriteriaIn.getFaxType()!=null){
			eventLogListSql.append("and (o.faxDetail.faxType = :faxType or :faxTypeDataValue is null) ");}
		if(qaProblemCriteriaIn.getFromCreateDate()!=null){
			eventLogListSql.append("and (o.qaProblem.createDate >= :fromCaseCreateDate or :fromCaseCreateDate is null) ");}
		if(qaProblemCriteriaIn.getToCreateDate()!=null){
			eventLogListSql.append("and (o.qaProblem.createDate < :toCaseCreateDate or :toCaseCreateDate is null) ");}
	
		eventLogListSql.append("order by o.eventLogDate desc ");
		
		Query q = em.createQuery(eventLogListSql.toString())
		.setParameter("fromEventLogDate", qaProblemCriteriaIn.getFromEventLogDate(), TemporalType.DATE)
		.setParameter("toEventLogDate", qaProblemCriteriaIn.getToEventLogDate(), TemporalType.DATE)
		.setParameter("recordStatus", RecordStatus.Active)
		.setParameter("eventType", SystemManualFlag.System)
		.setParameter("faxLogFlag", YesNoFlag.Yes);

		if(qaProblemCriteriaIn.getItemCode()!=null){
			q=q.setParameter("itemCode", qaProblemCriteriaIn.getItemCode());}			
		if(qaProblemCriteriaIn.getSupplierCode()!=null){
			q=q.setParameter("supplierCode", qaProblemCriteriaIn.getSupplierCode());}
		if(qaProblemCriteriaIn.getManufCode()!=null){
			q=q.setParameter("manufCode", qaProblemCriteriaIn.getManufCode());}
		if(qaProblemCriteriaIn.getPharmCompanyCode()!=null){
			q=q.setParameter("pharmCompanyCode", qaProblemCriteriaIn.getPharmCompanyCode());}
		if(qaProblemCriteriaIn.getCaseNum()!=null){
			q=q.setParameter("caseNum", qaProblemCriteriaIn.getCaseNum());}
		if(qaProblemCriteriaIn.getCaseStatus()!=null){
			q=q.setParameter("caseStatus", qaProblemCriteriaIn.getCaseStatus());
			q=q.setParameter("caseStatusDataValue", qaProblemCriteriaIn.getCaseStatus().getDataValue());}
		if(qaProblemCriteriaIn.getFaxType()!=null){
			q=q.setParameter("faxType", qaProblemCriteriaIn.getFaxType());
			q=q.setParameter("faxTypeDataValue", qaProblemCriteriaIn.getFaxType().getDataValue());}
		if(qaProblemCriteriaIn.getFromCreateDate()!=null){
			q=q.setParameter("fromCaseCreateDate", qaProblemCriteriaIn.getFromCreateDate());}
		if(qaProblemCriteriaIn.getToCreateDate()!=null){
			q=q.setParameter("toCaseCreateDate", qaProblemCriteriaIn.getToCreateDate());}
		
		q=q.setHint(QueryHints.FETCH, "o.qaProblem");
		q=q.setHint(QueryHints.FETCH, "o.qaProblem.problemHeader");
		q=q.setHint(QueryHints.FETCH, "o.qaProblem.supplier");
		q=q.setHint(QueryHints.FETCH, "o.qaProblem.manufacturer");
		q=q.setHint(QueryHints.FETCH, "o.qaProblem.pharmCompany");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.pharmProblemList");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.qaBatchNumList");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.qaProblemNature");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.classification");
		q=q.setHint(QueryHints.BATCH, "o.qaProblem.qaProblemFileList");
		q=q.setHint(QueryHints.BATCH, "o.faxDetail");
		
		
		eventLogList = q.getResultList();
		
		if(eventLogList!=null && eventLogList.size()>0)
		{
			for(EventLog el:eventLogList)
			{
				el.getFaxDetail();
				if(el.getFaxDetail()!=null)
				{
					el.getFaxDetail().getFaxInitHosp();
					el.getFaxDetail().getFaxInterimHosp();
					el.getFaxDetail().getFaxFinalHosp();
					el.getFaxDetail().getFaxInitSupp();
					el.getFaxDetail().getFaxInterimSupp();
					el.getFaxDetail().getFaxSender();
				}
				getLazyQaProblem(el.getQaProblem());
			}
		}
		else
		{
			eventLogList=null;
		}
	}
	
	@SuppressWarnings("static-access")
	private boolean isCorrespondenceToHospital(FaxType faxType) {
		return faxType == FaxType.IH || faxType == FaxType.MH || faxType == faxType.FH;
	}
	
	public List<EventLog> retrieveEventLogListForPharmProblemForm(QaProblemCriteria qaProblemCriteriaIn) {
		List<EventLog> resultEventLogList = new ArrayList<EventLog>();
		retrieveEventLogListByQaProblemCriteria(qaProblemCriteriaIn);
		Map<String,EventLog> latestEventLogMap = new HashMap<String,EventLog>();
		EventLog tmpEventLog;
		String faxTypeStr;
		if (eventLogList != null) {
			for (EventLog eventLog : eventLogList) {
				if (isCorrespondenceToHospital(eventLog.getFaxDetail().getFaxType())) {
					faxTypeStr = eventLog.getFaxDetail().getFaxType().getDataValue();
					tmpEventLog = latestEventLogMap.get(faxTypeStr);
					if (tmpEventLog == null) {
						latestEventLogMap.put(faxTypeStr, eventLog);
					} else if (eventLog.getCreateDate().after(tmpEventLog.getCreateDate())) {
						latestEventLogMap.put(faxTypeStr, eventLog);
					}
				}
			}
			eventLogList = null;
			resultEventLogList.addAll(latestEventLogMap.values());
		}
		return resultEventLogList;
	}
	
	public void retrieveFaxDetailLogByEventLogList(List<EventLog> eventLogList)
	{
		faxDetailLogRptList = new ArrayList<FaxDetailLogRpt>();
		for (EventLog eventLog:eventLogList)
		{
			QaProblem qaCom = eventLog.getQaProblem();
			FaxDetail faxDetail = eventLog.getFaxDetail();
			
			FaxDetailLogRpt rpt = new FaxDetailLogRpt();
			rpt.setCaseStatus(qaCom.getCaseStatus().getDataValue());
			rpt.setCaseNum(qaCom.getCaseNum());
			rpt.setRefNum(faxDetail.getRefNum());
			rpt.setClassification(qaCom.getClassification()==null?"":(qaCom.getClassification().getClassificationCode()==null?"":(qaCom.getClassification().getClassificationCode().getDataValue())));
			rpt.setItemCode(qaCom.getProblemHeader().getItemCode());
			rpt.setItemDesc(qaCom.getProblemHeader().getDmDrug().getFullDrugDesc());
			rpt.setOrderType(qaCom.getProblemHeader().getOrderType().getDataValue());
			rpt.setSupplier(qaCom.getSupplier().getSupplierName());
			rpt.setManuf(qaCom.getManufacturer().getCompanyName());
			rpt.setPharmCom(qaCom.getPharmCompany().getCompanyName());
			rpt.setFaxType(faxDetail.getFaxType().getDataValue());
			rpt.setFrom(faxDetail.getFaxSender().getContact().getFirstName() + " " + (faxDetail.getFaxSender().getContact().getLastName()==null?"":faxDetail.getFaxSender().getContact().getLastName()));
			rpt.setTo(faxDetail.getFaxTo());
			rpt.setProblemDetail(qaCom.getQaProblemNature().getProblemDetail());
			rpt.setCaseCreateDate(qaCom.getCreateDate());
			
			Date faxGenDate = null;
			if (faxDetail.getFaxType().equals(FaxType.IH)){
				faxGenDate = faxDetail.getFaxInitHosp().getCreateDate();}
			else if (faxDetail.getFaxType().equals(FaxType.MH)){
				faxGenDate = faxDetail.getFaxInterimHosp().getCreateDate();}
			else if (faxDetail.getFaxType().equals(FaxType.FH)){
				faxGenDate = faxDetail.getFaxFinalHosp().getCreateDate();}
			else if (faxDetail.getFaxType().equals(FaxType.IS)){
				faxGenDate = faxDetail.getFaxInitSupp().getCreateDate();}
			else if (faxDetail.getFaxType().equals(FaxType.MS)){
				faxGenDate = faxDetail.getFaxInterimSupp().getCreateDate();}
			else if (faxDetail.getFaxType().equals(FaxType.FS)){
				faxGenDate = faxDetail.getCreateDate();}
			
			rpt.setFaxGenerationDate(faxGenDate);
			
			faxDetailLogRptList.add(rpt);
		}
	}
	
	public List<FaxDetailLogRpt> getFaxDetailLogRptList()
	{
		return faxDetailLogRptList;
	}
	
	public String getReportName() 
	{
		return REPORT_NAME;
	}

	public String getReportParameters() 
	{
		return reportParameters;
	}

	public String getRetrieveDate() 
	{
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		Collections.sort(qaProblemLazy.getEventLogList(), EVENT_LOG_COMPARATOR);
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		
		
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
			pc.getPharmBatchNumList();
			pc.getPharmBatchNumList().size();
			pc.getContract();
			pc.getContract().getSupplier();
			if(pc.getContract().getSupplier()!=null)
			{
				pc.getContract().getSupplier().getContact();
			}
			pc.getContract().getManufacturer();
			if(pc.getContract().getManufacturer()!=null)
			{
				pc.getContract().getManufacturer().getContact();
			}
			pc.getContract().getPharmCompany();
			if(pc.getContract().getPharmCompany()!=null)
			{
				pc.getContract().getPharmCompany().getContact();
			}
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
		}
		
		return qaProblemLazy;
	}	
	
	private static class EventLogComparator implements Comparator<EventLog>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private Date getEventLogDate(EventLog el) {
        	return el.getEventLogDate();
        }
        
        public int compare(EventLog el1, EventLog el2) {

            Date date1 = getEventLogDate(el1);
            Date date2 = getEventLogDate(el2);

            if (!date1.equals(date2)) {
            	return date1.compareTo(date2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
		if (eventLogList != null){
			eventLogList = null;
		}
	}
}
