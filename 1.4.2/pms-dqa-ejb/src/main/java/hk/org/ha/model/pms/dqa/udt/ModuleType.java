package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ModuleType implements StringValuedEnum {
	
	All("A", "All"),
	SampleTest("S", "Sample Test"),
	COA("C", "COA"),
	SuppPerf("P", "Suppplier Performance"),
	DelayDelivery("D", "Delay Delivery");
	
    private final String dataValue;
    private final String displayValue;
        
    ModuleType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ModuleType> {

		private static final long serialVersionUID = 5676934850732831535L;

		@Override
    	public Class<ModuleType> getEnumClass() {
    		return ModuleType.class;
    	}
    }
}
