package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;

import java.util.List;

import javax.ejb.Local;


@Local
public interface FaxInterimHospServiceLocal {
	
	void createFaxInterimHosp(QaProblem qaProblemIn, EventLog eventLogIn, List<FaxDetailPharmProblem> faxDetailPharmProblemListIn);
	
	void createFaxInterimHospRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag);
	
	void generateFaxInterimHospRpt();
	
	boolean isSuccess();
	
	String getErrorCode();
	
	String getRefNum();
	
    void destroy();
}
