package hk.org.ha.model.pms.dqa.persistence;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;


@Entity
@Table(name = "CONTRACT")
@NamedQueries({
	@NamedQuery(name = "Contract.findForCoaItemMaint", 
			query = "select o from Contract o where " +
			"(o.contractNum =:contractNum or (:contractNum is null and o.contractNum is null) ) " +
			"and o.supplier.supplierCode = :supplierCode and o.manufacturer.companyCode = :companyCode and o.itemCode =:itemCode and o.moduleType = :moduleType"),
	@NamedQuery(name = "Contract.findForCoaLiveContractList", 
			query = "select o from Contract o where o.contractNum like :contractNum and o.itemCode = :itemCode " +
					"and o.moduleType in :moduleList and o.endDate >=(select max(c1.endDate) from Contract c1 where c1.contractNum = o.contractNum " +
					"and c1.itemCode = o.itemCode and c1.moduleType = o.moduleType) order by o.contractNum"
	),	
	@NamedQuery(name = "Contract.findForCoaLastestLiveContract", 
			query = "select o from Contract o where o.contractNum =:contractNum and o.itemCode = :itemCode " +
					"and o.moduleType in :moduleList " +
					"and o.endDate >=(select max(c1.endDate) from Contract c1 where c1.contractNum = o.contractNum " +
					"and c1.itemCode = o.itemCode and c1.moduleType = o.moduleType)"
	),
	@NamedQuery(name = "Contract.findByItemCodeSupplierCodeModuleType", 
			query = "select o from Contract o where o.itemCode = :itemCode and o.supplier.supplierCode = :supplierCode and o.moduleType = :moduleType "
	),
	@NamedQuery(name = "Contract.findByErpContractLineId",
			query = "select o from Contract o where o.erpContractLineId = :erpContractLineId"
	),
	@NamedQuery(name = "Contract.findByErpContractLineIdModuleType",
			query = "select o from Contract o where o.erpContractLineId = :erpContractLineId and o.moduleType = :moduleType"
	),
	@NamedQuery(name = "Contract.findByContractNumModuleType",
			query = "select o from Contract o where o.contractNum = :contractNum and o.moduleType = :moduleType"
	),
	@NamedQuery(name = "Contract.findByContractNumItemCodeModuleType", 
			query = "select o from Contract o where o.contractNum =:contractNum and o.itemCode = :itemCode " +
					"and o.moduleType = :moduleType"			
	),
	@NamedQuery(name = "Contract.findContractListForSampleTest", 
			query = "select o from Contract o where o.contractNum like :contractNum " +
					"and (o.contractSuffix like :contractSuffixStr or :contractSuffix is null ) " +
					"and o.contractType = :contractType and o.itemCode = :itemCode " +
					"and o.moduleType = :moduleType and o.endDate >= :currentDate and o.startDate <= :currentDate " +
					"order by o.packSize, o.endDate"			
	),
	@NamedQuery(name = "Contract.findForSampleTestWithNoSuffix", 
			query = "select o from Contract o where o.contractNum = :contractNum and (o.contractSuffix is null)" +
					"and o.contractType = :contractType " +
					"and o.itemCode = :itemCode " +
					"and o.moduleType = :moduleType and o.endDate >= :currentDate and o.startDate <= :currentDate " +
					"order by o.packSize, o.endDate"			
	),
	@NamedQuery(name = "Contract.findForSampleTestWithSuffix", 
			query = "select o from Contract o where o.contractNum = :contractNum and (o.contractSuffix = :contractSuffix)" +
					"and o.contractType = :contractType " +
					"and o.itemCode = :itemCode " +
					"and o.moduleType = :moduleType and o.endDate >= :currentDate and o.startDate <= :currentDate " +
					"order by o.packSize, o.endDate"			
	),
	@NamedQuery(name = "Contract.findContractListLikeContractByItemCode", 
			query = "select o from Contract o where o.contractNum like :contractNum " +
					"and (o.contractSuffix like :contractSuffixStr or :contractSuffix is null ) " +
					"and o.itemCode = :itemCode " +
					"and o.moduleType = :moduleType " +
					"order by o.packSize, o.endDate"			
	),
	@NamedQuery(name = "Contract.findNextLiveContractByItemCodeOrderTypeScheduleMonthModuleType", 
			query = "select o from Contract o where " +
					"o.itemCode = :itemCode " +
					"and o.contractType = :orderType " +
					"and o.startDate<= :scheduleMonth " +
					"and o.endDate >= :scheduleMonth " +
					"and o.moduleType = :moduleType " +
					"and o.suspendFlag = :suspendFlag " +
					"order by o.packSize, o.endDate desc"			
	),
	@NamedQuery(name = "Contract.findContractListLikeContractByItemCodeOrderType", 
			query = "select o from Contract o where o.contractNum like :contractNum " +
					"and (o.contractSuffix like :contractSuffixStr or :contractSuffix is null ) " +
					"and o.itemCode = :itemCode " +
					"and o.contractType = :contractType " +
					"and o.moduleType = :moduleType " +
					"order by o.contractNum, o.contractSuffix"			
	),
	@NamedQuery(name = "Contract.findByItemContractNumContractSuffixContractTypeModuleType", 
			query = "select o from Contract o where o.contractNum = :contractNum " +
					"and (o.contractSuffix = :contractSuffixStr or :contractSuffix is null ) " +
					"and o.itemCode = :itemCode " +
					"and o.contractType = :contractType " +
					"and o.moduleType = :moduleType " +
					"order by o.contractNum, o.contractSuffix"			
	)
	
	
})
@Customizer(AuditCustomizer.class)
public class Contract extends VersionEntity {

	private static final long serialVersionUID = -2268751762290652482L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contractSeq")
	@SequenceGenerator(name="contractSeq", sequenceName="SEQ_CONTRACT", initialValue=10000)
	@Column(name="CONTRACT_ID", nullable = false)
	private Long contractId;

	@Column(name="CONTRACT_NUM", length=20)
    private String contractNum;

	@Column(name="CONTRACT_TYPE", length=1)
    private String contractType;

	@Column(name="CONTRACT_SUFFIX", length=3)
	private String contractSuffix;

	@Column(name="START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
	
	@Column(name="END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

	@Column(name="PACK_SIZE")
    private Integer packSize;
	
	@Column(name="PACK_PRICE")
    private Double packPrice;
	
	@Column(name="CURRENCY_CODE", length=3)
    private String currencyCode;
	
	@Column(name="UOM_CODE", length=25)
	private String uomCode;
	
	@Column(name="BASE_UNIT", length=4)
	private String baseUnit;
	
	@Column(name="BUY_QTY")
	private Integer buyQty;
	
	@Column(name="GET_FREE_QTY")
	private Integer getFreeQty;
	
	@Column(name="PAYMENT_TERM", length=50)
	private String paymentTerm;
	
	@Column(name="ERP_CONTRACT_LINE_ID")
	private Long erpContractLineId;
	
	@Column(name="CONTRACT_STATUS", length=30)
	private String contractStatus;
	
	@Converter(name = "Contract.suspendFlag", converterClass = SuspendFlag.Converter.class )
	@Convert("Contract.suspendFlag")
	@Column(name="SUSPEND_FLAG", length=1)
    private SuspendFlag suspendFlag;
	
	@Column(name="UPLOAD_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;

	@Column(name="PHS_CONTRACT_NUM", length=20)
	private String phsContractNum;
	
	@Converter(name = "Contract.moduleType", converterClass = ModuleType.Converter.class )
	@Convert("Contract.moduleType")
	@Column(name="MODULE_TYPE", length=1)
    private ModuleType moduleType;
	
	@Transient
	private boolean toCopy;
	
	@Column(name="ITEM_CODE")
	private String itemCode;
	
	@Transient
	private DmDrug dmDrug = null;
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null) {
			dmDrug = DmDrugCacher.instance().getDrugByItemCode(itemCode);
		}
	}

	@ManyToOne
	@JoinColumn(name="SUPPLIER_CODE")
	private Supplier supplier;
	
	@ManyToOne	
	@JoinColumn(name="MANUF_CODE", referencedColumnName="COMPANY_CODE")
	private Company manufacturer;
	
	@ManyToOne
	@JoinColumn(name="PHARM_COMPANY_CODE", referencedColumnName="COMPANY_CODE")
	private Company pharmCompany;
	
	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}

	public String getContractSuffix() {
		return contractSuffix;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()) : null;
	}

	public void setStartDate(Date startDate) {
		if (startDate != null) {
			this.startDate = new Date(startDate.getTime());
		}
		else{
			this.startDate = null;
		}
		
	}

	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}

	public void setEndDate(Date endDate) {
		if (endDate != null) {
			this.endDate = new Date(endDate.getTime()); 
		}
		else{
			this.endDate = null;
		}
	}

	public Integer getPackSize() {
		return packSize;
	}
	
	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}

	public Double getPackPrice() {
		return packPrice;
	}

	public void setPackPrice(Double packPrice) {
		this.packPrice = packPrice;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setUomCode(String uomCode) {
		this.uomCode = uomCode;
	}

	public String getUomCode() {
		return uomCode;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}

	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBuyQty(Integer buyQty) {
		this.buyQty = buyQty;
	}

	public Integer getBuyQty() {
		return buyQty;
	}

	public void setGetFreeQty(Integer getFreeQty) {
		this.getFreeQty = getFreeQty;
	}

	public Integer getGetFreeQty() {
		return getFreeQty;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setErpContractLineId(Long erpContractLineId) {
		this.erpContractLineId = erpContractLineId;
	}

	public Long getErpContractLineId() {
		return erpContractLineId;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Date getUploadDate() {
		return (uploadDate != null) ? new Date(uploadDate.getTime()) : null;
	}

	public void setPhsContractNum(String phsContractNum) {
		this.phsContractNum = phsContractNum;
	}

	public String getPhsContractNum() {
		return phsContractNum;
	}

	public void setUploadDate(Date uploadDate) {
		if (uploadDate != null) {
			this.uploadDate = new Date(uploadDate.getTime());
		} else {
			this.uploadDate = null;
		}
	}

	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setManufacturer(Company manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Company getManufacturer() {
		return manufacturer;
	}

	public void setPharmCompany(Company pharmCompany) {
		this.pharmCompany = pharmCompany;
	}

	public Company getPharmCompany() {
		return pharmCompany;
	}

	public void setModuleType(ModuleType moduleType) {
		this.moduleType = moduleType;
	}

	public ModuleType getModuleType() {
		return moduleType;
	}

	public boolean isToCopy() {
		return toCopy;
	}

	public void setToCopy(boolean toCopy) {
		this.toCopy = toCopy;
	}

	public void setSuspendFlag(SuspendFlag suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public SuspendFlag getSuspendFlag() {
		return suspendFlag;
	}

	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}
	
	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}
}
