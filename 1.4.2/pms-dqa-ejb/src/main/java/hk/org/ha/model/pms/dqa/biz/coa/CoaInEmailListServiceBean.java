package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaReplyEmail;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("coaInEmailListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaInEmailListServiceBean implements CoaInEmailListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaInEmail> coaInEmailList;
	
	@Out(required = false)
	private Integer maxReceiveDateMonthRangeForInEmailEnq;

	@Out(required = false)
	private Integer defaultReceiveDateMonthRangeForInEmailEnq;

	public void setMaxReceiveDateMonthRangeForInEmailEnq(
			Integer maxReceiveDateMonthRangeForInEmailEnq) {
		this.maxReceiveDateMonthRangeForInEmailEnq = maxReceiveDateMonthRangeForInEmailEnq;
	}
	
	public Integer getMaxReceiveDateMonthRangeForInEmailEnq() {
		return maxReceiveDateMonthRangeForInEmailEnq;
	}

	public void setDefaultReceiveDateMonthRangeForInEmailEnq(
			Integer defaultReceiveDateMonthRangeForInEmailEnq) {
		this.defaultReceiveDateMonthRangeForInEmailEnq = defaultReceiveDateMonthRangeForInEmailEnq;
	}
	
	public Integer getDefaultReceiveDateMonthRangeForInEmailEnq() {
		return defaultReceiveDateMonthRangeForInEmailEnq;
	}	

	@SuppressWarnings("unchecked")
	public void retrieveCoaInEmailListForInEmailEnq(Date fromReceiveDate, Date toReceiveDate, ValidateResultFlag resultFlag) {
		logger.debug("retrieveCoaInEmailListForInEmailEnq #0 #1 #2", fromReceiveDate, toReceiveDate, resultFlag);
		coaInEmailList = (List<CoaInEmail>)em.createNamedQuery("CoaInEmail.findForInEmailEnq")
											 .setHint(QueryHints.FETCH, "o.emailLog")
											 .setHint(QueryHints.BATCH, "o.coaInEmailAttList")
											 .setHint(QueryHints.BATCH, "o.coaInEmailAttList.coaFileFolder")
											 .setHint(QueryHints.BATCH, "o.coaInEmailAttList.coaFileFolder.fileItem")
											 .setHint(QueryHints.BATCH, "o.coaReplyEmailList")
										     .setHint(QueryHints.BATCH, "o.coaReplyEmailList.emailLog")
		                                     .setParameter("fromReceiveDate", fromReceiveDate)
		                                     .setParameter("toReceiveDate", toReceiveDate)
		                                     .setParameter("resultFlag", resultFlag)
		                                     .setParameter("resultFlagValue", resultFlag.getDataValue())
		                                     .getResultList();
		
		for( CoaInEmail inEmail:coaInEmailList ) {
			inEmail.getCoaInEmailAttList().size();
			inEmail.getCoaReplyEmailList().size();
			for( CoaReplyEmail replyEmail:inEmail.getCoaReplyEmailList() ) {
				replyEmail.getEmailLog();
			}
		}
		logger.info("coaInEmailList size #0", coaInEmailList.size());
	}
	
	//For Seam TestNG test case only
	public void testRetrieveCoaInEmailListForInEmailEnq() {
		Calendar cal = Calendar.getInstance();
		cal.set( Calendar.HOUR, 0);
		cal.set( Calendar.MINUTE, 0);
		cal.set( Calendar.SECOND, 0);
		cal.set( Calendar.MILLISECOND, 0);
		cal.add( Calendar.DATE, 1);
		Date fromReceiveDate = cal.getTime();
		cal.add( Calendar.MONTH, -1);
		Date toReceiveDate = cal.getTime();
		retrieveCoaInEmailListForInEmailEnq( fromReceiveDate, toReceiveDate, ValidateResultFlag.All );		
	}
}
