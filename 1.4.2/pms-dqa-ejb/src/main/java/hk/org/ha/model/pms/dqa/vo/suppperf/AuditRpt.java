package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class AuditRpt {
	
	private String institutionName;
	private String institutionCode;
	private String caseNum;
	private String department;
	private String reportedBy;
	private Date reportDate;
	private String itemCode;
	private String itemDescription;
	private Integer quantityWithProblem;
	private String supplierCode;
	private String supplierName;
	private String manufacturerCode;
	private String manufacturerName;
	private String pharmCompanyCode;
	private String pharmCompanyName;
	private String batchNum;
	private String category;
	private String natureOfProblem;
	private String problemDetail;
	private String howDiscovered;
	private String assessmentOfProblem;
	private String immediateActionTaken;
	private String riskIndex;
	private String followUpAction;
	private String investigationReport;
	private String NoFurtherActionRequiredFlag;
	private String BatchCanBeReleasedFlag;
	private String StockReplacementFlag;
	private String others;
	private String BatchSuspensionFlag;
	private String ProductRecallFlag;
	private Date caseCreateDate;
	private Date investigationReportDate;
	private Date caseCloseDate;
	private String caseStatus;
	private String countryCode;
	private String countryName;
	
	
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getReportedBy() {
		return reportedBy;
	}
	public void setReportedBy(String reportedBy) {
		this.reportedBy = reportedBy;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public Integer getQuantityWithProblem() {
		return quantityWithProblem;
	}
	public void setQuantityWithProblem(Integer quantityWithProblem) {
		this.quantityWithProblem = quantityWithProblem;
	}
	public String getManufacturerCode() {
		return manufacturerCode;
	}
	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getNatureOfProblem() {
		return natureOfProblem;
	}
	public void setNatureOfProblem(String natureOfProblem) {
		this.natureOfProblem = natureOfProblem;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public String getHowDiscovered() {
		return howDiscovered;
	}
	public void setHowDiscovered(String howDiscovered) {
		this.howDiscovered = howDiscovered;
	}
	public String getAssessmentOfProblem() {
		return assessmentOfProblem;
	}
	public void setAssessmentOfProblem(String assessmentOfProblem) {
		this.assessmentOfProblem = assessmentOfProblem;
	}
	public String getImmediateActionTaken() {
		return immediateActionTaken;
	}
	public void setImmediateActionTaken(String immediateActionTaken) {
		this.immediateActionTaken = immediateActionTaken;
	}
	public String getRiskIndex() {
		return riskIndex;
	}
	public void setRiskIndex(String riskIndex) {
		this.riskIndex = riskIndex;
	}
	public String getFollowUpAction() {
		return followUpAction;
	}
	public void setFollowUpAction(String followUpAction) {
		this.followUpAction = followUpAction;
	}
	public String getInvestigationReport() {
		return investigationReport;
	}
	public void setInvestigationReport(String investigationReport) {
		this.investigationReport = investigationReport;
	}
	
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	
	public String getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getNoFurtherActionRequiredFlag() {
		return NoFurtherActionRequiredFlag;
	}
	public void setNoFurtherActionRequiredFlag(String noFurtherActionRequiredFlag) {
		NoFurtherActionRequiredFlag = noFurtherActionRequiredFlag;
	}
	public String getBatchCanBeReleasedFlag() {
		return BatchCanBeReleasedFlag;
	}
	public void setBatchCanBeReleasedFlag(String batchCanBeReleasedFlag) {
		BatchCanBeReleasedFlag = batchCanBeReleasedFlag;
	}
	public String getStockReplacementFlag() {
		return StockReplacementFlag;
	}
	public void setStockReplacementFlag(String stockReplacementFlag) {
		StockReplacementFlag = stockReplacementFlag;
	}
	public String getBatchSuspensionFlag() {
		return BatchSuspensionFlag;
	}
	public void setBatchSuspensionFlag(String batchSuspensionFlag) {
		BatchSuspensionFlag = batchSuspensionFlag;
	}
	public String getProductRecallFlag() {
		return ProductRecallFlag;
	}
	public void setProductRecallFlag(String productRecallFlag) {
		ProductRecallFlag = productRecallFlag;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public String getPharmCompanyName() {
		return pharmCompanyName;
	}
	public void setPharmCompanyName(String pharmCompanyName) {
		this.pharmCompanyName = pharmCompanyName;
	}
	public Date getCaseCreateDate() {
		return caseCreateDate;
	}
	public void setCaseCreateDate(Date caseCreateDate) {
		this.caseCreateDate = caseCreateDate;
	}
	public Date getInvestigationReportDate() {
		return investigationReportDate;
	}
	public void setInvestigationReportDate(Date investigationReportDate) {
		this.investigationReportDate = investigationReportDate;
	}
	public Date getCaseCloseDate() {
		return caseCloseDate;
	}
	public void setCaseCloseDate(Date caseCloseDate) {
		this.caseCloseDate = caseCloseDate;
	}
	
	
	
}
