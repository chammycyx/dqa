package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;

import javax.ejb.Local;

@Local
public interface ProblemNatureSubCatServiceLocal {
	
	void addProblemNatureSubCat();
	
	void createProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn);
	
	void deleteProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn);
	
	void updateProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCatIn);
	
	void retrieveProblemNatureSubCatByProblemNatureSubCatId(ProblemNatureSubCat problemNatureSubCatIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
