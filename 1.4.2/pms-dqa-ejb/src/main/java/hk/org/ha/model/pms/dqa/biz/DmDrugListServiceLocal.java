package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface DmDrugListServiceLocal {
	
	void retrieveDmDrugListLikeItemCode(String prefixItemCode);
	
	void destroy();

}
