package hk.org.ha.model.pms.dqa.biz.cddb;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.vo.cddb.ContractInfo;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("dqaCddbService")
@MeasureCalls
public class DqaCddbServiceBean implements DqaCddbServiceLocal {

	private static final String CONTRACT_INFO_CLASS = ContractInfo.class.getName();
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<ContractInfo> retrieveActiveContract() {
		
		return em.createQuery(
				"select new " + CONTRACT_INFO_CLASS + "(o.itemCode, o.manufacturer.companyCode, o.contractType) " +
				"from Contract o " +
				"where o.startDate <= :today " +
				"and o.endDate >= :today " +
				"and o.suspendFlag = :suspendFlag " +
				"and o.moduleType = :moduleType "
			)
			.setParameter("today", Calendar.getInstance().getTime())
			.setParameter("suspendFlag", SuspendFlag.Unsuspended)
			.setParameter("moduleType", ModuleType.All)
			.getResultList();
	
	}
}
