package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CoaDiscrepancyAnalysisRptServiceLocal {
	
	void retrieveCoaDiscrepancyAnalysisList(CoaDiscrepancyAnalysisRptCriteria coaDiscrepancyAnalysisRptCriteria);
	
	List<CoaDiscrepancyAnalysisRptData> getCoaDiscrepancyAnalysisRptList();
	
	List<String> getHeaderList(); 
	
	String getReportName();

	String getReportParameters();

	String getRetrieveDate();

	void destroy();

}
