package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PHARM_BATCH_NUM")
@Customizer(AuditCustomizer.class)
public class PharmBatchNum extends VersionEntity {

	private static final long serialVersionUID = 3403829186073691605L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmBatchNumSeq")
	@SequenceGenerator(name = "pharmBatchNumSeq", sequenceName = "SEQ_PHARM_BATCH_NUM", initialValue=10000)
	@Id
	@Column(name="PHARM_BATCH_NUM_ID", nullable=false)
	private Long pharmBatchNumId;

	@Column(name="BATCH_NUM", length=20, nullable=false)
	private String batchNum;
	
	@ManyToOne
	@JoinColumn(name="PHARM_PROBLEM_ID")
	private PharmProblem pharmProblem;
	
	@Transient
	private boolean selected;

	public Long getPharmBatchNumId() {
		return pharmBatchNumId;
	}

	public void setPharmBatchNumId(Long pharmBatchNumId) {
		this.pharmBatchNumId = pharmBatchNumId;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	
	public PharmProblem getPharmProblem() {
		return pharmProblem;
	}

	public void setPharmProblem(PharmProblem pharmProblem) {
		this.pharmProblem = pharmProblem;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}

}
