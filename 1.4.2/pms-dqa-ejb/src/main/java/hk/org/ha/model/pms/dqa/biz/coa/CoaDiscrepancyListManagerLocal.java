package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CoaDiscrepancyListManagerLocal {
	List<CoaDiscrepancy> getAllRelatedCoaDiscrepancyListByCoaBatchPassFlag(CoaItem coaItem, PassFlag passFlag);
}
