package hk.org.ha.model.pms.dqa.vo.cddb;

import java.io.Serializable;

public class ContractInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String itemCode;
	
	private String manufacturerCode;
	
	private String contractType;

	public ContractInfo() {
	}
	
	public ContractInfo(String itemCode, String manufacturerCode, String contractType) {
		this.itemCode = itemCode;
		this.manufacturerCode = manufacturerCode;
		this.contractType = contractType;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getManufacturerCode() {
		return manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
}
