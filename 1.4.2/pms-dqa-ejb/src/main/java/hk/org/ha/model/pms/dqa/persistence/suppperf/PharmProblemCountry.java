package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PHARM_PROBLEM_COUNTRY")
@Customizer(AuditCustomizer.class)
public class PharmProblemCountry extends VersionEntity {

	private static final long serialVersionUID = 5835728159608530765L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmProblemCountrySeq")
	@SequenceGenerator(name = "pharmProblemCountrySeq", sequenceName = "SEQ_PHARM_PROBLEM_COUNTRY", initialValue=10000)
	@Id
	@Column(name="PHARM_PROBLEM_COUNTRY_ID", nullable=false)
	private Long pharmProblemCountryId;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_CODE")
	private Country country;
	
	@ManyToOne
	@JoinColumn(name="PHARM_PROBLEM_ID")
	private PharmProblem pharmProblem;

	
	public Long getPharmProblemCountryId() {
		return pharmProblemCountryId;
	}

	public void setPharmProblemCountryId(Long pharmProblemCountryId) {
		this.pharmProblemCountryId = pharmProblemCountryId;
	}
	
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	public PharmProblem getPharmProblem() {
		return pharmProblem;
	}

	public void setPharmProblem(PharmProblem pharmProblem) {
		this.pharmProblem = pharmProblem;
	}

}
