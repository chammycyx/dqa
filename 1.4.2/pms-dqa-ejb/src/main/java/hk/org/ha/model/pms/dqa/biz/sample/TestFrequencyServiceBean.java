package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("testFrequencyService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class TestFrequencyServiceBean implements TestFrequencyServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private TestFrequency testFrequency;
	
	@In(create=true)
	private SampleTestScheduleListServiceLocal sampleTestScheduleListService;
	
	private static final String RECORD_STATUS = "recordStatus";
	private static final String TEST_CODE = "testCode";
	private static final String ORDER_TYPE = "orderType";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String RISK_LEVEL_CODE = "riskLevelCode";
	private static final String O_RISK_LEVEL = "o.riskLevel";
	
	private boolean success;

	private String errorCode;
	
	public void retrieveTestFrequencyByTestFrequency(TestFrequency testFrequencyIn){
		logger.debug("retrieveTestFrequencyByTestFrequency");
		testFrequency = retrieveTestFrequencyByTestFrequencyForValidation(testFrequencyIn.getFrequencyId());
	}
	
	private TestFrequency retrieveTestFrequencyByTestFrequencyForValidation(Long frequencyId){
		logger.debug("retrieveTestFrequencyByTestFrequencyForValidation");		
		return em.find(TestFrequency.class, frequencyId);
	}
	
	public void addTestFrequency(){
		logger.debug("addTestFrequency");
		
		testFrequency = new TestFrequency();
		testFrequency.setPercentage(1);
	}

	private TestFrequency reActiveDeletedTestFrequency(TestFrequency testFrequencyIn, TestFrequency testFrequencyOut){
		testFrequencyOut.setFrequencyPerYear(testFrequencyIn.getFrequencyPerYear());
		testFrequencyOut.setPercentage(testFrequencyIn.getPercentage());
		
		testFrequencyOut.setRecordStatus(RecordStatus.Active);
		
		return testFrequencyOut;
	}	
	
	@SuppressWarnings("unchecked")
	private List<TestFrequency> retrieveNormalTestFrequency(){
		logger.debug("retrieveNormalTestFrequency");
		List<TestFrequency> resultExisting;
		
		List<TestFrequency> outList = null;
		
		String riskLevelIn = (testFrequency.getRiskLevel()==null ? null :testFrequency.getRiskLevel().getRiskLevelCode() );	
		String orderTypeIn = testFrequency.getOrderType();
		String testCodeIn = (testFrequency.getSampleTest()==null ? null : testFrequency.getSampleTest().getTestCode());
	
		resultExisting = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeOrderTypeNullRiskLevel") 	// check 'all' riskLevel item
		.setParameter(RECORD_STATUS, RecordStatus.Active)
		.setParameter(TEST_CODE, testCodeIn)
		.setParameter(ORDER_TYPE, orderTypeIn)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.getResultList();

		logger.debug("...... findByRecordStatusTestCodeOrderTypeNullRiskLevel : #0", resultExisting.size());		
		if (resultExisting.size() == 0){  
			resultExisting = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeRiskLevelCodeNullOrderType") // check 'all' orderType item
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(TEST_CODE, testCodeIn)
			.setParameter(RISK_LEVEL_CODE, riskLevelIn)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.BATCH, O_RISK_LEVEL)
			.getResultList();
			
			logger.debug("...... findByRecordStatusTestCodeRiskLevelCodeNullOrderType : #0", resultExisting.size());			
			if (resultExisting.size() ==0){  
				resultExisting = em.createNamedQuery("TestFrequency.findByOrderTypeRiskLevelCodeTestCode") // check normal item
				.setParameter(ORDER_TYPE, orderTypeIn)
				.setParameter(RISK_LEVEL_CODE, riskLevelIn)
				.setParameter(TEST_CODE, testCodeIn)
				.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
				.setHint(QueryHints.FETCH, O_RISK_LEVEL)
				.getResultList();
				
				logger.debug("..... findByOrderTypeRiskLevelCodeTestCode : #0", resultExisting.size());
				if (resultExisting.size() <=1){ // size - 0: new item, 1: existing item
					outList = resultExisting;
				}
			}
		}
		return outList;
	}
	
	@SuppressWarnings("unchecked")
	private List<TestFrequency> retrieveIsAllRiskLevelTestFrequency(){
		logger.debug("retrieveAllRiskLevelFrequency");
		List<TestFrequency> resultExisting;
		List<TestFrequency> outList= null;
			
		String orderTypeIn = testFrequency.getOrderType();
		String testCodeIn = (testFrequency.getSampleTest()==null ? null : testFrequency.getSampleTest().getTestCode());
		
		resultExisting = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeOrderType")
		.setParameter(RECORD_STATUS, RecordStatus.Active)
		.setParameter(TEST_CODE, testCodeIn)
		.setParameter(ORDER_TYPE, orderTypeIn)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
//		.setHint(QueryHints.LEFT_FETCH, O_RISK_LEVEL)
		.setHint(QueryHints.FETCH, O_RISK_LEVEL)
		.getResultList();

		logger.debug("..... findByRecordStatusTestCodeOrderType : #0", resultExisting.size());		
		if (resultExisting.size() == 0){
		
				resultExisting = em.createNamedQuery("TestFrequency.findByTestCodeOrderTypeNullRiskLevel")
				.setParameter(TEST_CODE, testCodeIn)
				.setParameter(ORDER_TYPE, orderTypeIn)
				.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
				.getResultList();
				
				logger.debug("..... findByTestCodeOrderTypeNullRiskLevel : #0", resultExisting.size());				
				if (resultExisting.size() <=1){ // 0 : New , 1: deleted exist
					outList = resultExisting;
				}	
		}
		
		return outList;
	}
	
	@SuppressWarnings("unchecked")
	private List<TestFrequency> retrieveIsAllOrderTypeTestFrequency(){
		logger.debug("retrieveAllOrderTypeTestFrequency");

		List<TestFrequency> outList = null;
		String riskLevelIn = (testFrequency.getRiskLevel()==null ? null :testFrequency.getRiskLevel().getRiskLevelCode() );	
		String testCodeIn = (testFrequency.getSampleTest()==null ? null : testFrequency.getSampleTest().getTestCode());
		
		List<TestFrequency> resultExisting;
		
		resultExisting = em.createNamedQuery("TestFrequency.findByRecordStatusTestCodeRiskLevel")
		.setParameter(RECORD_STATUS, RecordStatus.Active)
		.setParameter(TEST_CODE, testCodeIn)
		.setParameter(RISK_LEVEL_CODE, riskLevelIn)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.setHint(QueryHints.BATCH, O_RISK_LEVEL)
		.getResultList();
		
		logger.debug("...... findByRecordStatusTestCodeRiskLevel : #0", resultExisting.size());		
		if (resultExisting.size() == 0){
			resultExisting = em.createNamedQuery("TestFrequency.findByTestCodeRiskLevelNullOrderType")
			.setParameter(TEST_CODE, testCodeIn)
			.setParameter(RISK_LEVEL_CODE, riskLevelIn)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.getResultList();

			logger.debug("...... findByTestCodeRiskLevelNullOrderType : #0", resultExisting.size());
			if (resultExisting.size() <= 1){ // 0 : New , 1: deleted exist 
				outList = resultExisting;
			}
		}
		
		return outList;
	}

	public void createTestFrequency(){
		logger.debug("createTestFrequency");
		success = false;
		errorCode = null;
		String riskLevelIn2 = (testFrequency.getRiskLevel()==null ? null :testFrequency.getRiskLevel().getRiskLevelCode() );	
		String orderTypeIn = testFrequency.getOrderType();

		boolean isNew = false;
		TestFrequency testFrequencyFind = null;
		List<TestFrequency> testList = null;
 		
		if (riskLevelIn2 != null && orderTypeIn != null){		
			testList = retrieveNormalTestFrequency();
			logger.debug("normal testFrequency");
		}else {
			if (riskLevelIn2 == null && orderTypeIn !=null){ //All riskLevel input
				testList = retrieveIsAllRiskLevelTestFrequency();
				logger.debug("All riskLevel type testFrequency");
			}else if(orderTypeIn == null && riskLevelIn2 !=null){ //All orderType input
				testList = retrieveIsAllOrderTypeTestFrequency();
				logger.debug("All order type testFrequency");
			}else
			{
				errorCode = "0071";
			}
		}
		
		if(errorCode == null)
		{
			if (testList != null){		
				if (testList.size() ==0){
					isNew = true;						
				}else if (testList.size() == 1 && testList.get(0).getRecordStatus() == RecordStatus.Deleted){
					testFrequencyFind = reActiveDeletedTestFrequency(testFrequency, testList.get(0));
					logger.debug("Add -> update");				
				}
			}
			
			if (isNew){
				testFrequency.setRecordStatus(RecordStatus.Active);	
				em.persist(testFrequency);
				em.flush();
				success = true;
			}else if (testFrequencyFind != null){
				em.merge(testFrequencyFind);
				em.flush();	
				success = true;
			}else {
				errorCode = "0007";
			}
		}
	}

	public void updateTestFrequency(){
		logger.debug("updateTestFreqency");
		success = false;
		errorCode = null;

		em.merge(testFrequency);		
		em.flush();
		success = true;		
	}
	
	public void deleteTestFrequency(TestFrequency testFrequencyIn){
		logger.debug("deleteTestFrequency");
		success = false;
		errorCode = null;
		List<SampleTestSchedule> scheduleIdList = sampleTestScheduleListService.retrieveSampleTestScheduleIdListForTestFreqMaint(RecordStatus.Active, ScheduleStatus.Complete, testFrequencyIn.getFrequencyId());
		
		if (scheduleIdList.isEmpty()){
			TestFrequency testFrequencyFind = retrieveTestFrequencyByTestFrequencyForValidation(testFrequencyIn.getFrequencyId());
			
			if (testFrequencyFind != null && testFrequencyFind.getRecordStatus() == RecordStatus.Active){
				testFrequencyFind.setRecordStatus(RecordStatus.Deleted);
				
				em.merge(testFrequencyFind);
				em.flush();
				success = true;		
			}
			else
			{
				errorCode = "0008"; 
			}
		}else {
			errorCode = "0032"; 
		}
	}
	
	public boolean isSuccess(){
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	@Remove
	public void destroy(){
		if (testFrequency != null){
			testFrequency = null;
		}
	}
}
