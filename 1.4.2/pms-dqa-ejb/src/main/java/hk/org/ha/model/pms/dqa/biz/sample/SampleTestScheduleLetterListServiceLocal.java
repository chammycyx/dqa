
package hk.org.ha.model.pms.dqa.biz.sample;


import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;
import javax.ejb.Local;

@Local
public interface SampleTestScheduleLetterListServiceLocal {

	void retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(SampleTestEmailEnquiryCriteria sampleTestEmailEnquiryCriteria);
	
	void createSampleTestScheduleLetter(LetterTemplate letterTemplateIn, String refNumIn, List<SampleTestSchedule> sampleTestScheduleListIn, SampleTestSchedule sampleTestScheduleIn);
	
	void destroy(); 
}
