package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListServiceLocal;
import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleNextServiceLocal;
import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.CancelFlag;
import hk.org.ha.model.pms.dqa.udt.ClosureStatus;
import hk.org.ha.model.pms.dqa.udt.ContractStatus;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.util.FTPHelper;
import hk.org.ha.model.pms.dqa.vo.ContractLine;
import hk.org.ha.model.pms.dqa.vo.Contracts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.ChannelSftp.LsEntry;

@Stateless
@Scope(ScopeType.SESSION)
@Name("erpContractImportService")
@RemoteDestination
@MeasureCalls
public class ErpContractImportServiceBean implements ErpContractImportServiceLocal {

	@Logger
	private Log logger;
	
	@In(create = true)
	private ErpContractServiceLocal erpContractService;
	
	@In(create = true)
	private ErpContractListServiceLocal erpContractListService;	
	
	@In(create = true)
	private ErpContractMspServiceLocal erpContractMspService;
	
	@In(create = true)
	private ErpContractMspListServiceLocal erpContractMspListService;
	
	@In(create = true)
	private SampleTestScheduleListServiceLocal sampleTestScheduleListService;
	
	@In(create = true)
	private SampleTestScheduleNextServiceLocal sampleTestScheduleNextService;
	
	@In(create = true)
	private SampleTestScheduleServiceLocal sampleTestScheduleService;
	
	public static final String CONF_DIR_SUBFOLDER = "/pms-dqa";
	public static final String CONF_DIR = "conf.dir";
	public static final String ERP_DIR_PROPERTIES = "/erp-dir.properties";
	public static final String ERROR_PROMPT = "Error";
	
	private static final String ERP_CONTRACT = "erpContract"; 
	
	private String ftpBackupDir;
	private String workDir;	
	
	//sftp setup
	private String sftpWorkDir;
	private String fileTransferMode;
	private ChannelSftp dqaErpContractChannelSftp;
	private static final String SFTP = "sftp";
	private static final String FTP = "ftp";
	private static final String FILE_NAME_PATTERN = "DRUG_CONTRACT_";
	private static final String FILE_EXTENSION = ".xml";
	
	private String configDirSubfolder;
	
	private Map<String, List> erpContractForSampleTestScheduleCHEMMap; 
	private Map<String, List> erpContractForSampleTestScheduleMICROMap;
	private SampleTestSchedule sampleTestScheduleNew;
	
	public void setConfigDirSubfolder(String configDirSubfolder){
		this.configDirSubfolder = configDirSubfolder;
	}
	
	private void initDir(String connectType) {
		Properties profile = new Properties(); 
		FileInputStream fis = null;
		String configDir = System.getProperty(CONF_DIR);
        try {        	
        	fis = new FileInputStream( configDir + CONF_DIR_SUBFOLDER + ERP_DIR_PROPERTIES );
			profile.load(fis);			
			logger.debug("configDir + CONF_DIR_SUBFOLDER + ERP_DIR_PROPERTIES : " + configDir + CONF_DIR_SUBFOLDER + ERP_DIR_PROPERTIES);
			
			ftpBackupDir = profile.getProperty(connectType+".ftpBackupDir");
			workDir = profile.getProperty(connectType+".workDir");
		} catch (IOException e) {
			logger.error("Fail to read properties",e);
		}finally{
			try {
				fis.close();
			}catch(IOException e){
				logger.error("Fail to close file",e);
        	}
		}
	}
	
	@SuppressWarnings("unchecked")
	private File getRemoteFile( String propName ) throws IOException {		
		FTPHelper ftpHelper = new FTPHelper( propName, configDirSubfolder );
		HashMap<String, Map> hashMap = (HashMap<String, Map>)ftpHelper.dirDetails( "./" );
		Set<Map.Entry<String, Map>> set = hashMap.entrySet();
		String fileName=null;
		File f = null;
		for( Map.Entry<String, Map> m : set ) {
			fileName = m.getKey();
			break;
		}
		if( fileName!=null && ftpHelper.fileExists( fileName ) ) {
			ftpHelper.download( workDir + "/" + fileName, fileName );
		}
		ftpHelper.quit();		
		f = new File( workDir + "/" + fileName );
		return f;
	}
	
	private void ftpMoveFile( String propName, String path, String fileName ) throws IOException {
		FTPHelper ftpHelper = new FTPHelper( propName, configDirSubfolder );
		if( ftpHelper.fileExists(fileName) ) {						
			ftpHelper.rename(fileName, path + "/" + fileName);
			ftpHelper.quit();
		}
	}
	
	private void updateErpContractInfo( hk.org.ha.model.pms.dqa.vo.Contract ct) {
		
		Calendar cal = Calendar.getInstance();
		cal.set(1900, Calendar.JANUARY, 1);
		Map<String, Date> endDateMap = new HashMap<String, Date>();
		
		if( CancelFlag.Y == ct.getCancelFlag()  
				|| ClosureStatus.CLOSED == ct.getClosureStatus() ) {
			//Suspend the whole contract with all contract lines
			erpContractListService.updateContractListForSuspension( ct, ModuleType.All );
			erpContractListService.updateContractListForSuspension( ct, ModuleType.SampleTest );
			
		} else {
					
			for( ContractLine cl : ct.getContractLines() ) {												
				if (cl.getUnitPrice() == 0) {
					logger.debug("Skip contract #0 line #1 because unit price is 0. #2", ct.getContractNum(), cl.getContractLineNum(), cl.getContractLineId());
					continue;
				}
				
				Contract erpContract = erpContractService.retrieveContractByErpContractLineId( cl.getContractLineId() );
								
				if( erpContract == null ) { 											
					// find out the late effective end date of each item
					endDateMap = processEndDateMap(cl.getItemCode(), ct.getEffectiveEndDate(), cl.getEffectiveEndDate(), endDateMap);
					
					erpContractService.createContract(ct, cl);
				} else {
					erpContract.setContractStatus( ct.getContractStatus() );	
					
					if( CancelFlag.Y == cl.getCancelFlag() 
							|| ClosureStatus.CLOSED == cl.getClosureStatus() ) {
						//Suspend contract line
						erpContractService.updateContractForSuspension( erpContract );
					} else {
						logger.debug("cl.getEffectiveEndDate() "+ cl.getEffectiveEndDate());										
						
						// find out the late effective end date of each item
						endDateMap = processEndDateMap(cl.getItemCode(), ct.getEffectiveEndDate(), cl.getEffectiveEndDate(), endDateMap);
												
						//Update contract line
						erpContractService.updateContract( erpContract, ct, cl );												
						
						erpContractMspListService.updateContractMspListForLogicallyDeleted( erpContract.getContractId() );
						
						for( hk.org.ha.model.pms.dqa.vo.ContractMsp cm:cl.getContractMsps() ) {
						
							ContractMsp contractMsp = erpContractMspService.retrieveContractMspByContractIdErpContractMspId( erpContract.getContractId(), cm.getMspRecordId() );
							
							if( contractMsp == null ) {
								erpContractMspService.createContractMsp( erpContract, cl, cm );
							} else {
								erpContractMspService.updateContractMsp( contractMsp, cm );
							}
						}
					}
				}
				
				///////////////////////////////////////////////////
				/////Check to insert for Sample Test Schedule
				//////////////////////////////////////////////////
				sampleTestScheduleNew = null;
				if( ContractStatus.Approved.getDataValue().equals( ct.getContractStatus() ) )
				{
					if( !(CancelFlag.Y == cl.getCancelFlag() || ClosureStatus.CLOSED == cl.getClosureStatus()) ) 
					{
						//For Chem
						sampleTestScheduleNew = sampleTestScheduleNextService.checkForInsertNextSampleTestScheduleByErpContractTestCode(ct, cl, "CHEM");
						if(sampleTestScheduleNew!=null)
						{
							erpContractForSampleTestScheduleCHEMMap = constructErpContractForSampleTestScheduleMap(erpContractForSampleTestScheduleCHEMMap, 
										   																			sampleTestScheduleNew, ct, cl);
						}
						
						//For Micro
						sampleTestScheduleNew = sampleTestScheduleNextService.checkForInsertNextSampleTestScheduleByErpContractTestCode(ct, cl, "MICRO");
						if(sampleTestScheduleNew!=null)
						{
							erpContractForSampleTestScheduleMICROMap = constructErpContractForSampleTestScheduleMap(erpContractForSampleTestScheduleMICROMap, 
										sampleTestScheduleNew, ct, cl);
						}
					}
				}
			}
			
			erpContractListService.updateContractListForSampleTest( ct );
			
			//Update contract to the latest contract end date mentioned in contract line
			//per contract number and item code
			erpContractService.updateContractLatestEndDate(ct, endDateMap);
			
		}

	}
	
	private Map<String, Date> processEndDateMap(String itemCode, Date chEndDate, Date clEndDate, Map<String, Date> endDateMap) {
		logger.debug("processEndDateMap #0 #1 #2 ", itemCode, chEndDate, clEndDate);
		
		if (!endDateMap.containsKey(itemCode)) {
			if (clEndDate == null) {
				endDateMap.put(itemCode, chEndDate);
			} else {
				endDateMap.put(itemCode, clEndDate);
			}
		} else {
			if (clEndDate == null &&
					chEndDate.after(endDateMap.get(itemCode))) {
				
				endDateMap.put(itemCode, chEndDate);
			} else if (clEndDate != null &&
					clEndDate.after(endDateMap.get(itemCode))) {
				
				endDateMap.put(itemCode, clEndDate);
			}
		}
		
		return endDateMap;
	}
	
	//For testing only
	public Boolean importErpContractTest( String fileName, String jobId ) {
		logger.info("importErpContractTest #0 #1",fileName,jobId);
		JAXBContext jaxbContext;
		File inboundFile = new File("src/test/resources/testdata/"+fileName);		
		
		erpContractForSampleTestScheduleCHEMMap = new HashMap<String, List>();
		erpContractForSampleTestScheduleMICROMap = new HashMap<String, List>();

		if (inboundFile == null || !inboundFile.exists()){
			logger.debug("File #0 not exist", inboundFile.getName());
			return true;
		}

		// for empty file
		if (inboundFile.length() ==0 ){
			logger.debug("File #0 is empty.", inboundFile.getName());
			return true;
		}

		try {
			jaxbContext = JAXBContext.newInstance("hk.org.ha.model.pms.dqa.vo");									
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Contracts cts = (Contracts)unmarshaller.unmarshal( inboundFile ); 			
			List<hk.org.ha.model.pms.dqa.vo.Contract> ctList = cts.getContractList();									

			for(hk.org.ha.model.pms.dqa.vo.Contract ct:ctList) {															

				if( ContractStatus.Approved.getDataValue().equals( ct.getContractStatus() ) ) {	
					logger.debug("ContractHeaderId #0", ct.getContractHeaderId());
					updateErpContractInfo( ct);		
				} else {
					erpContractService.updateContractStatus( ct );
				}						
			}			
			return true;

		} catch (JAXBException e) {		
			logger.error("JAXB error", e);			
		} catch (Exception e1) {
			logger.error(ERROR_PROMPT, e1);
		}

		return false;
	}
	
	public Boolean importErpContract() {
		JAXBContext jaxbContext;
		File inboundFile = null;		
		erpContractForSampleTestScheduleCHEMMap = new HashMap<String, List>();
		erpContractForSampleTestScheduleMICROMap = new HashMap<String, List>();
		
		logger.info("File transfer mode is #0", fileTransferMode);
		
		if (fileTransferMode.equals(FTP))
		{
			initDir( ERP_CONTRACT );
			try {
				inboundFile = getRemoteFile( ERP_CONTRACT );
				if (inboundFile == null || !inboundFile.exists()){
					return true;
				}
				
				// for empty file
				if (inboundFile.length() ==0 ){
					logger.info("File #0 is empty.", inboundFile.getName());
				} else {
					logger.info("Import ERP contract file #0", inboundFile.getName());
					
					jaxbContext = JAXBContext.newInstance("hk.org.ha.model.pms.dqa.vo");									
					Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();			
					Contracts cts = (Contracts)unmarshaller.unmarshal( inboundFile ); 			
					List<hk.org.ha.model.pms.dqa.vo.Contract> ctList = cts.getContractList();									
					
					for(hk.org.ha.model.pms.dqa.vo.Contract ct:ctList) {															
						
						if( ContractStatus.Approved.getDataValue().equals( ct.getContractStatus() ) ) {	
							logger.debug("ContractHeaderId #0", ct.getContractHeaderId());
							updateErpContractInfo( ct);
						} else {
							erpContractService.updateContractStatus( ct );
						}
		
					}
					
					createNextSampleTestSchedule(erpContractForSampleTestScheduleCHEMMap);
					createNextSampleTestSchedule(erpContractForSampleTestScheduleMICROMap);
		
					Calendar modifyDateIn = Calendar.getInstance();
					modifyDateIn.add(Calendar.DATE, -1);
					sampleTestScheduleListService.resetSampleTestScheduleActionFlag( modifyDateIn );
				}
				
				ftpMoveFile(ERP_CONTRACT, ftpBackupDir, inboundFile.getName());					
				return true;
	
			} catch (JAXBException e) {		
				if( inboundFile.exists() ) {
					try {
						ftpMoveFile(ERP_CONTRACT, ftpBackupDir, inboundFile.getName());
					} catch (IOException e1) {
						logger.error("File error", e1);
					}
				}
				logger.error(ERROR_PROMPT, e);
			} catch (Exception e1) {
				if( inboundFile.exists() ) {
					try {
						ftpMoveFile(ERP_CONTRACT, ftpBackupDir, inboundFile.getName());
					} catch (IOException e) {
						logger.error("File error", e);
					}
				}
				logger.error(ERROR_PROMPT, e1);
			}
		}
		else if (fileTransferMode.equals(SFTP))
		{
			dqaErpContractChannelSftp = (ChannelSftp) Component.getInstance("dqaErpContractChannelSftp");
			try {
				List fileVector = dqaErpContractChannelSftp.ls(".");
				if (fileVector.size() != 0) 
				{
					for (int i = 0; i < fileVector.size(); i++) 
					{
						LsEntry lsEntry = (LsEntry) fileVector.get(i);

						if (!lsEntry.getFilename().equals(".") && !lsEntry.getFilename().equals("..")) 
						{
							if (lsEntry.getFilename().startsWith(FILE_NAME_PATTERN) && 
									lsEntry.getFilename().endsWith(FILE_EXTENSION))
							{
								inboundFile = new File(sftpWorkDir, lsEntry.getFilename());
								InputStream inputStream = dqaErpContractChannelSftp.get(lsEntry.getFilename());
								OutputStream outStream= new FileOutputStream(inboundFile);
								byte buf[] = new byte[1024];
								int len;
								while((len=inputStream.read(buf))>0)
								{
									outStream.write(buf ,0, len);
								}
								
								outStream.close();
								inputStream.close();
								break;
							}
						}
					}
				}
				
				if (inboundFile == null || !inboundFile.exists()){
					logger.info("Contract interface file does not exist");
					return true;
				}
				
				// for empty file
				if (inboundFile.length() ==0 ){
					logger.info("File #0 is empty.", inboundFile.getName());
				} else {
					logger.info("Import ERP contract file #0", inboundFile.getName());
					
					jaxbContext = JAXBContext.newInstance("hk.org.ha.model.pms.dqa.vo");									
					Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();			
					Contracts cts = (Contracts)unmarshaller.unmarshal( inboundFile ); 			
					List<hk.org.ha.model.pms.dqa.vo.Contract> ctList = cts.getContractList();									
					
					for(hk.org.ha.model.pms.dqa.vo.Contract ct:ctList) {															
						
						if( ContractStatus.Approved.getDataValue().equals( ct.getContractStatus() ) ) {	
							logger.debug("ContractHeaderId #0", ct.getContractHeaderId());
							updateErpContractInfo( ct);
						} else {
							erpContractService.updateContractStatus( ct );
						}
		
					}
					
					createNextSampleTestSchedule(erpContractForSampleTestScheduleCHEMMap);
					createNextSampleTestSchedule(erpContractForSampleTestScheduleMICROMap);
		
					Calendar modifyDateIn = Calendar.getInstance();
					modifyDateIn.add(Calendar.DATE, -1);
					sampleTestScheduleListService.resetSampleTestScheduleActionFlag( modifyDateIn );
				}
				
				dqaErpContractChannelSftp.rm(inboundFile.getName());
				return true;
				
			} catch (SftpException e) {
				logger.error("Fail to delete file.", e);
			} catch (FileNotFoundException e) {
				logger.error("File error", e);
			} catch (IOException e) {
				logger.error("File IO error", e);
			} catch (JAXBException e) {
				if( inboundFile != null && inboundFile.exists() ) {
					inboundFile.delete();
				}
				logger.error(ERROR_PROMPT, e);
			}
		}
		else
		{
			logger.error("File transfer mode not correct. Mode can only be " + SFTP + " or " + FTP);
		}
		
		return false;
	}

	
	private Map<String, List> constructErpContractForSampleTestScheduleMap(Map<String, List> erpContractForSampleTestScheduleMap, 
																		   SampleTestSchedule sampleTestScheduleNew, 
																		   hk.org.ha.model.pms.dqa.vo.Contract ct, 
																		   ContractLine cl){
		
		if (erpContractForSampleTestScheduleMap.containsKey(cl.getItemCode()))
		{
			List objectList = erpContractForSampleTestScheduleMap.get(cl.getItemCode());
			SampleTestSchedule sampleTestScheduleFind = (SampleTestSchedule)objectList.get(0);
			ContractLine contractLineFind = (ContractLine)objectList.get(2);
			
			if((sampleTestScheduleNew.getScheduleMonth()).compareTo(sampleTestScheduleFind.getScheduleMonth())<0)
			{
				objectList.set(0,sampleTestScheduleNew);
				objectList.set(1,ct);
				objectList.set(2,cl);
			}
			else if((sampleTestScheduleNew.getScheduleMonth()).compareTo(sampleTestScheduleFind.getScheduleMonth())==0)
			{
				if(cl.getPackSize() < contractLineFind.getPackSize())
				{
					objectList.set(0,sampleTestScheduleNew);
					objectList.set(1,ct);
					objectList.set(2,cl);
				}
			}
		}
		else
		{
			List objectList = new ArrayList();
			objectList.add(sampleTestScheduleNew);
			objectList.add(ct);
			objectList.add(cl);
			
			erpContractForSampleTestScheduleMap.put(cl.getItemCode(), objectList);
		}
	
		return erpContractForSampleTestScheduleMap;
		
	}
	
	private void createNextSampleTestSchedule(Map<String, List> erpContractForSampleTestScheduleMap)
	{
		if (erpContractForSampleTestScheduleMap!=null)
		{
			 for (Map.Entry<String, List> entry : erpContractForSampleTestScheduleMap.entrySet()) {
				 List objectList = entry.getValue();
				 SampleTestSchedule sampleTestScheduleFind = (SampleTestSchedule)objectList.get(0);
				 hk.org.ha.model.pms.dqa.vo.Contract contractHeaderFind = (hk.org.ha.model.pms.dqa.vo.Contract)objectList.get(1);
				 ContractLine contractLineFind = (ContractLine)objectList.get(2);
				 sampleTestScheduleService.createNextSampleTestScheduleByErpContract(sampleTestScheduleFind, contractHeaderFind, contractLineFind);
			 }
		}
	}
	
}
