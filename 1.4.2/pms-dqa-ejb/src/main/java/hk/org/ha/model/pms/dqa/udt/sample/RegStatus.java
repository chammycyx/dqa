package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum RegStatus implements StringValuedEnum {
	
	PharmManufactoryOrSelfManufacturedItem("M", "Pharmacutical Manufactory/Self Manufactured Item"),
	PandPRegistrationNotRequired("N", "P and P Registration Not Required"),
	PandPRegistrationDrug("R", "P and P Registration Drug"),	
	UnregistrationDrug("U", "Unregistration Drug");
	
    private final String dataValue;
    private final String displayValue;
        
    RegStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<RegStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<RegStatus> getEnumClass() {
    		return RegStatus.class;
    	}
    }
}
