package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("problemNatureSubCatListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ProblemNatureSubCatListServiceBean implements ProblemNatureSubCatListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<ProblemNatureSubCat> problemNatureSubCatList;
	
	@SuppressWarnings("unchecked")
	public void retrieveProblemNatureSubCatList(ProblemNatureCat problemNatureCatIn){
		logger.debug("retrieveProblemNatureSubCatList");
		
		problemNatureSubCatList = em.createNamedQuery("ProblemNatureSubCat.findByCatIdRecordStatus")
		.setParameter("problemNatureCatId", problemNatureCatIn.getProblemNatureCatId())
		.setParameter("recordStatus", RecordStatus.Active)
		.setHint(QueryHints.FETCH, "o.problemNatureCat")
		.setHint(QueryHints.FETCH, "o.problemNatureCat.problemNatureParam")
		.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	public void retrieveProblemNatureSubCatListFull(){
		logger.debug("retrieveProblemNatureSubCatListFull");
		
		problemNatureSubCatList = em.createNamedQuery("ProblemNatureSubCat.findAll")
		.setParameter("recordStatus", RecordStatus.Active)
		.setHint(QueryHints.FETCH, "o.problemNatureCat")
		.setHint(QueryHints.FETCH, "o.problemNatureCat.problemNatureParam")
		.getResultList();
		
	}
	
	@Remove
	public void destroy(){
		if(problemNatureSubCatList != null) {
			problemNatureSubCatList = null;  
		}
	}

}
