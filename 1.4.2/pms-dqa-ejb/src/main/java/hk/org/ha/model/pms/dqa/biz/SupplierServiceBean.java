package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("supplierService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SupplierServiceBean implements SupplierServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private Supplier supplier;
	
	@In(create = true)
	private SupplierManagerLocal supplierManager;
	
	public void retrieveSupplierBySupplierCode(String code){	
		logger.debug("retrieveSupplierBysupplierCode #0", code);					
		supplier = supplierManager.retrieveSupplier( code );
		if (supplier != null)
		{
			for (SupplierContact supplierContact : supplier.getSupplierContactList()) {
				supplierContact.getSupplier();
			}
		}
	}
	
	@Restrict("#{!identity.loggedIn}")	
	public Supplier retrieveSupplierForErp( String supplierCode ) {
		return supplierManager.retrieveSupplier(supplierCode);			
	}
	
	@Restrict("#{!identity.loggedIn}")	
	public Supplier retrieveSupplierForPhs( String supplierCode ) {
		return supplierManager.retrieveSupplier(supplierCode);			
	}

	public Supplier findSupplierBySupplierCode( String supplierCode ) {
		logger.debug("findSupplierBySupplierCode #0", supplierCode);
		
		List<Supplier> suppliers = em.createNamedQuery("Supplier.findBySupplierCode").setParameter("supplierCode", supplierCode).getResultList();
		
		if(suppliers!=null && suppliers.size()>0)
		{
			return suppliers.get(0);
		}
		else
		{
			return null;
		}
	}
	
	@Remove
	public void destroy(){
		if(supplier!=null) {
			supplier = null;
		}
	}
}
