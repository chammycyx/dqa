package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.sample.IAFaxMemoRpt;
import hk.org.ha.model.pms.dqa.vo.sample.IAMemoContentVo;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierMemoContentVo;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestLetterTemplateService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestLetterTemplateServiceBean implements SampleTestLetterTemplateServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Interpolator interpolator;

	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	@In
	private SampleTestEmailFaxManagerLocal sampleTestEmailFaxManager;
	
	private JRDataSource dataSource;
	
	private static final String SPACE = "          ";
		
	public LetterTemplate retrieveLetterTemplateByCode(String code){
		
		logger.debug("retrieveLetterTemplateByCode #0", code);
		LetterTemplate tmpLetterTemplate = em.find(LetterTemplate.class, code);
		
		LetterTemplate letterTemplate = new LetterTemplate();
		letterTemplate.setCode(tmpLetterTemplate.getCode());
		letterTemplate.setName(tmpLetterTemplate.getName());	
		letterTemplate.setSubject(tmpLetterTemplate.getSubject()==null?"":interpolator.interpolate(tmpLetterTemplate.getSubject()));
		letterTemplate.setContent(tmpLetterTemplate.getContent()==null?"":interpolator.interpolate(tmpLetterTemplate.getContent()));
		
		return letterTemplate;
	} 
	
	public void retrieveLetterTemplateForLabTest(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn) {
		sampleTestEmailFaxManager.retrieveLetterTemplateForLabTest(scheduleList, sampleTestScheduleIn, userInfoIn, supplierPharmCompanyManufacturerContactIn);
	}
	
	public void retrieveLetterTemplateForSchedule(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn){
		sampleTestEmailFaxManager.retrieveLetterTemplateForSchedule(scheduleList, sampleTestScheduleIn, userInfoIn, supplierPharmCompanyManufacturerContactIn);
	}
	
	public void retrieveLetterTemplateForInstitutionAssignment(SampleTestSchedule sampleTestScheduleIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, UserInfo defaultCpoContactUserInfo, UserInfo cpoSignatureUserInfo){
		sampleTestEmailFaxManager.retrieveLetterTemplateForInstitutionAssignment(sampleTestScheduleIn, supplierPharmCompanyManufacturerContactIn, defaultCpoContactUserInfo, cpoSignatureUserInfo);
	}
	
	private IAFaxMemoRpt mapIAFaxMemoRpt(IAMemoContentVo in)
	{
		IAFaxMemoRpt rpt = new IAFaxMemoRpt();
		
		rpt.setRef(in.getRefNum()+" in PHS/QA/2/3");
		if (!DqaRptUtil.strIsEmptyOrNull(in.getUserInfoFax().getContact().getOfficePhone())){
			rpt.setTel(in.getUserInfoFax().getContact().getOfficePhone());}
		else if (!DqaRptUtil.strIsEmptyOrNull(in.getUserInfoFax().getContact().getMobilePhone())){
			rpt.setTel(in.getUserInfoFax().getContact().getMobilePhone());}
		rpt.setFax(in.getUserInfoFax().getContact().getFax());
		rpt.setDate(DqaRptUtil.getDateStr("dd MMM yyyy", in.getSendDate()));
		rpt.setTo("DM (Pharm), "+
				in.getSampleTestScheduleFax().getInstitution().getInstitutionCode());
		rpt.setAttn(in.getSampleTestScheduleFax().getInstitution().getContact().constructSupplierInfo());
		rpt.setItemDesc(in.getSampleTestScheduleFax().getSampleItem().getDmDrug().getFullDrugDesc() + " ("+in.getSampleTestScheduleFax().getSampleItem().getDmDrug().getItemCode()+")");
		rpt.setManuf("Manufacturer: "+in.getSampleTestScheduleFax().getContract().getManufacturer().getCompanyName());
		rpt.setSupplier("Supplier: "+in.getSampleTestScheduleFax().getContract().getSupplier().getSupplierName());
		if (!DqaRptUtil.strIsEmptyOrNull(in.getFaxMemoParagraph1()))
		{
			rpt.setParagraph1("                        "+in.getFaxMemoParagraph1());
		}
		int paragraphIdx = 2;
		if (!DqaRptUtil.strIsEmptyOrNull(in.getFaxMemoParagraph2()))
		{
			rpt.setP2No(paragraphIdx+".");
			rpt.setParagraph2(in.getFaxMemoParagraph2());
			paragraphIdx++;
		}
		if (!DqaRptUtil.strIsEmptyOrNull(in.getFaxMemoParagraph3()))
		{
			rpt.setP3No(paragraphIdx+".");
			rpt.setParagraph3(in.getFaxMemoParagraph3());
			paragraphIdx++;
		}
		if (!DqaRptUtil.strIsEmptyOrNull(in.getFaxMemoParagraph4()))
		{
			rpt.setP4No(paragraphIdx+".");
			rpt.setParagraph4(in.getFaxMemoParagraph4());
			paragraphIdx++;
		}
		if (!DqaRptUtil.strIsEmptyOrNull(in.getFaxMemoParagraphOther()))
		{
			rpt.setpOthNo(paragraphIdx+".");
			rpt.setParagraphOther(in.getFaxMemoParagraphOther());
			paragraphIdx++;
		}
		rpt.setSender("("+
				(in.getUserInfoFax().getContact().getTitle().getDisplayValue())+" "+
				(in.getUserInfoFax().getContact()==null?"":(in.getUserInfoFax().getContact().getFirstName()==null?"":in.getUserInfoFax().getContact().getFirstName()))+" "+
				(in.getUserInfoFax().getContact()==null?"":(in.getUserInfoFax().getContact().getLastName()==null?"":in.getUserInfoFax().getContact().getLastName()))
				+")");
		
		return rpt;
	}
	
	public void createIAFaxMemoRpt(IAMemoContentVo iAMemoContentIn)
	{
		logger.debug("createFaxInitHospRpt");
		
		List<IAFaxMemoRpt> iAFaxMemoRptList = new ArrayList<IAFaxMemoRpt>();
		
		iAFaxMemoRptList.add(mapIAFaxMemoRpt(iAMemoContentIn));
		
		dataSource = new JRBeanCollectionDataSource(iAFaxMemoRptList);
	}

	public void generateIAFaxMemoRpt()
	{
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/iAFaxMemo.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	public void createSupplierFaxMemoRpt(SupplierMemoContentVo supplierMemoContentVo)
	{
		logger.debug("createFaxInitHospRpt");
		
		List<SupplierMemoContentVo> supplierMemoContentVoList = new ArrayList<SupplierMemoContentVo>();	
		supplierMemoContentVoList.add(supplierMemoContentVo);
		
		if (supplierMemoContentVo.getSendDate() != null) {
			supplierMemoContentVo.setMemoDate(new SimpleDateFormat("dd MMM yyyy").format(supplierMemoContentVo.getSendDate()));
		}
		
		supplierMemoContentVo.constructParagraph3();
		supplierMemoContentVo.setFaxMemoParagraph1(SPACE.concat(supplierMemoContentVo.getFaxMemoParagraph1()));
		supplierMemoContentVo.setFaxMemoParagraph2(SPACE.concat(supplierMemoContentVo.getFaxMemoParagraph2()));
		supplierMemoContentVo.setFaxMemoParagraph3(SPACE.concat(supplierMemoContentVo.getFaxMemoParagraph3()));
		supplierMemoContentVo.setFaxMemoParagraph4(SPACE.concat(supplierMemoContentVo.getFaxMemoParagraph4()));
		supplierMemoContentVo.setFaxMemoParagraph5(SPACE.concat(supplierMemoContentVo.getFaxMemoParagraph5()));
		dataSource = new JRBeanCollectionDataSource(supplierMemoContentVoList);
	}
	
	public void generateSupplierFaxMemoRpt()
	{
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/supplierFaxMemo.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	@Remove
	public void destroy(){
	}

}
