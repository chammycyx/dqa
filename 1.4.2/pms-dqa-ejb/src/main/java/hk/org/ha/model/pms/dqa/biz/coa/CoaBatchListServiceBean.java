package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;
import hk.org.ha.model.pms.dqa.vo.coa.CoaVerificationData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaBatchListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaBatchListServiceBean implements CoaBatchListServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaBatch> coaBatchList;
	
	private Comparator comparator = new CoaDiscrepancyComparator();
    
	public List<CoaVerificationData> retrieveCoaVerificationDataList() {
		List<CoaVerificationData> coaVerificationDataList = new ArrayList<CoaVerificationData>();
		List<CoaBatch> list = retrieveCoaBatchListByCoaStatus(CoaStatus.VerificationInProgress);
		CoaVerificationData data;
		for (CoaBatch coaBatch:list) {
			data = new CoaVerificationData();
			data.setBatchNum(coaBatch.getBatchNum());
			data.setCoaBatchId(coaBatch.getCoaBatchId());			
			data.setContractNum(coaBatch.getCoaItem().getContract().getContractNum());
			data.setContractSuffix(coaBatch.getCoaItem().getContract().getContractSuffix());
			data.setCreateDate(coaBatch.getCreateDate());
			data.setItemCode(coaBatch.getCoaItem().getContract().getItemCode());
			data.setFullDrugDesc(coaBatch.getCoaItem().getContract().getDmDrug().getFullDrugDesc());
			data.setOrderType(coaBatch.getCoaItem().getOrderType());
			data.setSupplierCode(coaBatch.getCoaItem().getContract().getSupplier().getSupplierCode());
			data.setSupplierName(coaBatch.getCoaItem().getContract().getSupplier().getSupplierName());
			coaVerificationDataList.add(data);
		}
		return coaVerificationDataList;
	}
	
	public List<CoaBatch> retrieveCoaBatchListByCoaStatus(CoaStatus coaStatus) {		
    	logger.debug("retrieveCoaBatchListByCoaStatus #0", coaStatus.getDataValue());
    	List<CoaBatch> resultList = em.createNamedQuery("CoaBatch.findByCoaStatus")
    					 .setParameter("coaStatus", coaStatus)
    					 .setHint(QueryHints.FETCH, "o.coaBatchVer")    					 
    					 .setHint(QueryHints.FETCH, "o.coaItem")
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract")
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract.supplier")
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract.manufacturer")				 
    					 .getResultList();
    	return resultList;
    	
    }	
	
	public void retrieveCoaBatchListByCoaStatusDiscrepancyStatus( CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus ) {
		logger.debug("retrieveCoaBatchListByCoaStatusDiscrepancyStatus #0 #1", coaStatus, discrepancyStatus);
		coaBatchList = em.createNamedQuery("CoaBatch.findByCoaStatusDiscrepancyStatus")
						 .setParameter("coaStatus", coaStatus)
						 .setParameter("discrepancyStatus", discrepancyStatus)
    					 .setHint(QueryHints.FETCH, "o.coaBatchVer")    					 
    					 .setHint(QueryHints.FETCH, "o.coaItem")    					 
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract")
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract.manufacturer")
    					 .setHint(QueryHints.FETCH, "o.coaItem.contract.supplier")    					 
    					 .setHint(QueryHints.BATCH, "o.coaDiscrepancyList")
    					 .setHint(QueryHints.BATCH, "o.coaDiscrepancyList.parentCoaDiscrepancy")
    					 .setHint(QueryHints.BATCH, "o.coaDiscrepancyList.coaDiscrepancyKey")
    					 .setHint(QueryHints.BATCH, "o.coaDiscrepancyList.childCoaDiscrepancyList")
    					 .getResultList();
		
		for(CoaBatch coaBatch : coaBatchList) {			
			// sort the coaDiscrepancyList
			coaBatch.getCoaDiscrepancyList().size();
			for(CoaDiscrepancy parentCoaDiscrepancy: coaBatch.getCoaDiscrepancyList()) {			
				parentCoaDiscrepancy.getChildCoaDiscrepancyList().size();
			}			
			Collections.sort( coaBatch.getCoaDiscrepancyList(), comparator);			
		}
		
		em.flush();		
		
	}
		
	@Restrict("#{!identity.loggedIn}")
	public List<CoaBatch> retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus() {
		logger.debug("retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus #0 #1", InterfaceFlag.No, CoaStatus.Pass);
		return em.createNamedQuery("CoaBatch.findByInterfaceFlagCoaStatus")
		                 .setParameter("interfaceFlag", InterfaceFlag.No)		 
		                 .setParameter("coaStatus", CoaStatus.Pass)		                     					
		                 .getResultList();    	
	}
	
	@Remove
	public void destroy(){
		if(coaBatchList != null) {
			coaBatchList = null;
		}
	}
	
	public static class CoaDiscrepancyComparator implements Comparator<CoaDiscrepancy>,Serializable
	{

		private static final long serialVersionUID = 8032402230292096246L;

		public int compare(CoaDiscrepancy c1, CoaDiscrepancy c2){
			return c1.getCoaDiscrepancyKey()
					 .getOrderSeq()
					 .compareTo( 
								c2.getCoaDiscrepancyKey()
								  .getOrderSeq() 
				      );
		}						
	}
	
	
}
