package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.udt.coa.AttRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("coaInEmailAttManager")
@MeasureCalls
public class CoaInEmailAttManagerBean implements CoaInEmailAttManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	public CoaInEmailAtt createCoaInEmailAtt( CoaInEmail coaInEmail, String fileName ){
		logger.debug("createCoaInEmailAtt #0 #1", coaInEmail.getCoaInEmailId(), fileName);
		
		CoaInEmailAtt att = new CoaInEmailAtt();
		att.setCoaInEmail( coaInEmail );
		att.setFileName( fileName );
		att.setRemarkType( AttRemarkType.NullValue );
		att.setResultFlag( ValidateResultFlag.Pass );		
		em.persist( att );
		em.flush();
		
		return att;
		
	}
	
	public CoaInEmailAtt updateCoaInEmailAttVerifyResult(CoaInEmailAtt emailAtt, AttRemarkType remarkType) {
		logger.debug("updateCoaInEmailAttVerifyResult #0 #1", emailAtt.getCoaInEmailAttId(), remarkType);
		
		CoaInEmailAtt att = emailAtt;
		
		att.setRemarkType( remarkType );
		if( remarkType != AttRemarkType.Pass ) {			
			att.setResultFlag( ValidateResultFlag.Fail );
		} else {
			att.setResultFlag( ValidateResultFlag.Pass );
		}		
		att = em.merge(att);
		em.flush();
		
		return att;
	}
	
	public CoaInEmailAtt updateCoaInEmailAtt(CoaInEmailAtt att, CoaFileFolder folder) {
		CoaInEmailAtt attOut = att;
		attOut.setCoaFileFolder( folder );
		attOut = em.merge( attOut );
		em.flush();
		return attOut;
	}
	
	public CoaInEmailAtt updateCoaInEmailAttResult(CoaInEmailAtt emailAtt, ValidateResultFlag validateFlag,AttRemarkType remarkType) {
		logger.debug("updateCoaInEmailAttResult #0 #1 #2", emailAtt.getCoaInEmailAttId(), validateFlag, remarkType);
		
		CoaInEmailAtt att = emailAtt;
		
		att.setRemarkType( remarkType );	
		att.setResultFlag( validateFlag );
		att = em.merge(att);
		em.flush();
		
		return att;
	}
	
	@SuppressWarnings("unchecked")
	public CoaInEmailAtt retrieveCoaInEmailAttForCoaFileFolder(CoaFileFolder coaFileFolder){
		logger.debug("retrieveCoaInEmailAttForCoaFileFolder #0", coaFileFolder.getCoaFileFolderId());
		
		List<CoaInEmailAtt> emailAttList = em.createNamedQuery("CoaInEmailAtt.findForCoaFileFolder")
			.setHint(QueryHints.FETCH, "o.coaInEmail")
			.setHint(QueryHints.FETCH, "o.coaInEmail.emailLog")
			.setParameter("coaFileFolderId", coaFileFolder.getCoaFileFolderId())
			.getResultList();
		
		if (!emailAttList.isEmpty()){
			return emailAttList.get(0);
		}else {
			return null;
		}
	}
}
