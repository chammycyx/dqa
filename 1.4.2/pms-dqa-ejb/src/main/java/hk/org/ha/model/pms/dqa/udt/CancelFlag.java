package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CancelFlag implements StringValuedEnum {
	
	Y("Y", "Yes"),
	N("N", "No");	
	
    private final String dataValue;
    private final String displayValue;
        
    CancelFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<CancelFlag> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<CancelFlag> getEnumClass() {
    		return CancelFlag.class;
    	}
    }
}
