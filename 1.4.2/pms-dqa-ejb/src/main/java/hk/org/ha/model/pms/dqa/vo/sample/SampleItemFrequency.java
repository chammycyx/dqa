package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleItemFrequency {
	
	private String headerInfo;
	private Date updateDate;
	private String itemCode;
	private String itemDesc;
	private String chemAnalysisFreq;
	private String chemAnaylsisQtyReq;
	private String microBioTestFreq;
	private String microBioTestQtyReq;
	private String validTestQtyReq;
		
	public String getHeaderInfo() {
		return headerInfo;
	}
	
	public void setHeaderInfo(String headerInfo) {
		this.headerInfo = headerInfo;
	}
	
	public Date getUpdateDate() {
		return updateDate!=null?new Date(updateDate.getTime()):null;
	}

	public void setUpdateDate(Date updateDate) {
		if (updateDate != null) {
			this.updateDate = new Date(updateDate.getTime());
		}
	}
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	
	public String getChemAnalysisFreq() {
		return chemAnalysisFreq;
	}
	
	public void setChemAnalysisFreq(String chemAnalysisFreq) {
		this.chemAnalysisFreq = chemAnalysisFreq;
	}
	
	public String getChemAnaylsisQtyReq() {
		return chemAnaylsisQtyReq;
	}
	
	public void setChemAnaylsisQtyReq(String chemAnaylsisQtyReq) {
		this.chemAnaylsisQtyReq = chemAnaylsisQtyReq;
	}
	
	public String getMicroBioTestFreq() {
		return microBioTestFreq;
	}
	
	public void setMicroBioTestFreq(String microBioTestFreq) {
		this.microBioTestFreq = microBioTestFreq;
	}
	
	public String getMicroBioTestQtyReq() {
		return microBioTestQtyReq;
	}
	
	public void setMicroBioTestQtyReq(String microBioTestQtyReq) {
		this.microBioTestQtyReq = microBioTestQtyReq;
	}
	
	public String getValidTestQtyReq() {
		return validTestQtyReq;
	}

	public void setValidTestQtyReq(String validTestQtyReq) {
		this.validTestQtyReq = validTestQtyReq;
	}
}
