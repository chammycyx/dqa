package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReportTemplate;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.ContractLine;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestResultCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleTestResult;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleListService")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleListServiceBean implements SampleTestScheduleListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestSchedule> sampleTestScheduleList;

	@In(required = false)
	private SampleTestSchedule sampleTestSchedule;
	
	private List<SampleTestScheduleTestResult> sampleTestScheduleTestResultList;
	private List<SampleTestSchedule> exportSampleTestScheduleList;
	
	private static final String UNCHECKED = "unchecked";
	private static final String RECORD_STATUS = "recordStatus";
	private static final String SCHEDULE_STATUS = "scheduleStatus";
	private static final String ITEM_CODE = "itemCode";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String O_CONTRACT = "o.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.contract.supplier";
	private static final String O_CONTRACT_MANUFACTURER = "o.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.contract.pharmCompany";
	private static final String O_SAMPLE_ITEM = "o.sampleItem";
	private static final String O_SAMPLE_ITEM_RISK_LEVEL = "o.sampleItem.riskLevel";
	private static final String O_INSTITUTION = "o.institution";
	private static final String O_PAYMENT = "o.payment";
	private static final String TEST_CODE = "testCode";
	private static final String SUPPLIER_CODE = "supplierCode";
	private static final String MANUF_CODE = "manufCode";
	private static final String SCHE_STATUS_LIST = "scheduleStatusList";
	
	private static final String MICROBIOLOGY_TEST = "MICRO";
	private static final String CHEMICAL_ANAYLYSIS = "CHEM";
	
	
	private boolean success;
	
	private String errorCode = null;
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListByCriteria(SampleTestScheduleCriteria sampleTestScheduleCriteria){
		logger.debug("retrieveSampleTestScheduleListByCriteria" );
	
		StringBuffer enquiryDetailSql = new StringBuffer();
		enquiryDetailSql.append("select o from SampleTestSchedule o where o.recordStatus= :recordStatus ");
		enquiryDetailSql.append("and o.scheduleStatus = :scheduleStatus ");
		enquiryDetailSql.append("and (o.sampleItem.itemCode = :itemCode or :itemCode is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth = :scheduleMonth or :scheduleMonth is null ) ");
		enquiryDetailSql.append("and (o.sampleTest.testCode = :testCode or :testCode is null) ");
		enquiryDetailSql.append("and (o.orderType = :orderType or :orderTypeValue is null) ");
		enquiryDetailSql.append("and (o.sampleItem.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) ");
		if(sampleTestScheduleCriteria.getSupplierCode()!=null) {
			enquiryDetailSql.append("and (o.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) ");
		}	
		if(sampleTestScheduleCriteria.getManufCode() != null) {
			enquiryDetailSql.append("and (o.contract.manufacturer.companyCode = :manufCode or :manufCode is null) ");
		}	
		if(sampleTestScheduleCriteria.getPharmCode() !=null) {
			enquiryDetailSql.append("and (o.contract.pharmCompany.companyCode = :pharmCode or :pharmCode is null) " );
		}


		Query q = em.createQuery(enquiryDetailSql.toString())
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHEDULE_STATUS, sampleTestScheduleCriteria.getScheduleStatus())
			.setParameter(ITEM_CODE, sampleTestScheduleCriteria.getItemCode())
			.setParameter("scheduleMonth", sampleTestScheduleCriteria.getScheduleMonth())
			.setParameter(TEST_CODE, sampleTestScheduleCriteria.getTestCode())
			.setParameter("orderType", sampleTestScheduleCriteria.getOrderType())
			.setParameter("orderTypeValue", (sampleTestScheduleCriteria.getOrderType()==null?null:sampleTestScheduleCriteria.getOrderType().getDataValue()))
			.setParameter("riskLevelCode", sampleTestScheduleCriteria.getRiskLevelCode());
		
		if(sampleTestScheduleCriteria.getSupplierCode()!=null) {
			q=q.setParameter(SUPPLIER_CODE, sampleTestScheduleCriteria.getSupplierCode());
		}	
		if(sampleTestScheduleCriteria.getManufCode() != null) {
			q=q.setParameter(MANUF_CODE, sampleTestScheduleCriteria.getManufCode());
		}	
		if(sampleTestScheduleCriteria.getPharmCode() !=null) {
			q=q.setParameter("pharmCode", sampleTestScheduleCriteria.getPharmCode());
		}
		
		
		q=q.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.BATCH, O_INSTITUTION)
			.setHint(QueryHints.BATCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.BATCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.BATCH, O_CONTRACT_PHARMCOMPANY);
		
		
		sampleTestScheduleList = q.getResultList();
		
		if(sampleTestScheduleList!=null && sampleTestScheduleList.size()>0)
		{
			for(SampleTestSchedule sts:sampleTestScheduleList)
			{
				getLazySampleTestSchedule(sts);
			}
		}
			
	}
	
	@SuppressWarnings(UNCHECKED)
	public void  retrieveSampleTestScheduleListByRecordStatusScheduleStatus(RecordStatus recordStatus, ScheduleStatus scheduleStatus){
		logger.debug("retrieveSampleTestScheduleListByRecordStatusScheduleStatus #0", scheduleStatus);
		
		Query queryObj = em.createNamedQuery("SampleTestSchedule.findByRecordStatusScheduleStatus")
			.setParameter(RECORD_STATUS, recordStatus)
			.setParameter(SCHEDULE_STATUS, scheduleStatus)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST);
		
		if (scheduleStatus == ScheduleStatus.ScheduleGen ||
			scheduleStatus == ScheduleStatus.Schedule)
		{
			queryObj.setHint(QueryHints.BATCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.BATCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.BATCH, O_CONTRACT_PHARMCOMPANY);
		}
		else
		{
			queryObj.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY);
		}
		
		
		if (scheduleStatus == ScheduleStatus.Document){
			logger.debug("Document type loading ");
			queryObj.setHint(QueryHints.BATCH, O_INSTITUTION) 			// tuned
					.setHint(QueryHints.BATCH, "o.chemicalAnalysis") 	// tuned
					.setHint(QueryHints.BATCH, "o.microBioTest"); 		// tuned
			
			sampleTestScheduleList = queryObj.getResultList(); 
			for (SampleTestSchedule schedule: sampleTestScheduleList){
				schedule.getInstitution();
				if(schedule.getSampleTest().getTestCode().equals(CHEMICAL_ANAYLYSIS))
				{
					schedule.getChemicalAnalysis();
				}
				if(schedule.getSampleTest().getTestCode().equals(MICROBIOLOGY_TEST))
				{
					schedule.getMicroBioTest();
				}
				
				getLazySampleTestSchedule(schedule);
			}
		}else if (scheduleStatus == ScheduleStatus.InstitutionAssignment){
			logger.debug("Drug sample loading ");
			queryObj.setHint(QueryHints.BATCH, O_INSTITUTION);
			
			sampleTestScheduleList = queryObj.getResultList(); 
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
				sampleTestScheduleFind.getInstitution();
				
				if (sampleTestScheduleFind.getSampleTest().getTestCode().equals(CHEMICAL_ANAYLYSIS)){
					
					if (sampleTestScheduleFind.getChemicalAnalysis() != null){
						int totalQty = sampleTestScheduleFind.getChemicalAnalysis().getRetentionQty().intValue() + sampleTestScheduleFind.getReqQty().intValue();
						sampleTestScheduleFind.setTotalQty(totalQty);
					}
				}
				
				getLazySampleTestSchedule(sampleTestScheduleFind);
			}	
			
		}
		else if (scheduleStatus == ScheduleStatus.LabTest || 
				    scheduleStatus == ScheduleStatus.LabCollection ||
				    scheduleStatus == ScheduleStatus.DrugSample){
						
			sampleTestScheduleList = em.createNamedQuery("SampleTestSchedule.findByRecordStatusScheduleStatus")
				.setParameter(RECORD_STATUS, recordStatus)
				.setParameter(SCHEDULE_STATUS, scheduleStatus)
				.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
				.setHint(QueryHints.FETCH, O_CONTRACT)
				.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
				.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
				.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
				.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
				.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
				.setHint(QueryHints.FETCH, O_INSTITUTION)
				.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
				.getResultList();
			
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
				getLazySampleTestSchedule(sampleTestScheduleFind);
			}	
			
		}
		else if (scheduleStatus == ScheduleStatus.Payment){
			logger.debug("Payment loading ");
			queryObj.setHint(QueryHints.FETCH, O_INSTITUTION)
					.setHint(QueryHints.BATCH, O_PAYMENT)
					.getResultList();
		
			sampleTestScheduleList = queryObj.getResultList();
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
				getLazySampleTestSchedule(sampleTestScheduleFind);
			}	
		}
		else { // Type : SG, S
			logger.debug("type : #0 loading ", scheduleStatus.getDisplayValue());
			queryObj.setHint(QueryHints.BATCH, O_INSTITUTION);
		
			sampleTestScheduleList = queryObj.getResultList();
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
				getLazySampleTestSchedule(sampleTestScheduleFind);
				
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleListByTestResultCriteria(SampleTestResultCriteria sampleTestResultCriteria){
		logger.debug("retrieveSampleTestScheduleListByTestResultCriteria" );

			sampleTestScheduleList= em.createNamedQuery("SampleTestSchedule.findByTestResultCriteria")
				.setParameter(RECORD_STATUS, sampleTestResultCriteria.getRecordStatus())
				.setParameter(ITEM_CODE, sampleTestResultCriteria.getItemCode())
				.setParameter(TEST_CODE, sampleTestResultCriteria.getTestCode())
				.setParameter(SUPPLIER_CODE, sampleTestResultCriteria.getSupplierCode())
				.setParameter(MANUF_CODE, sampleTestResultCriteria.getManufCode())
				.setParameter("pharmCompanyCode", sampleTestResultCriteria.getPharmCompanyCode())
				.setParameter("testResult", sampleTestResultCriteria.getTestResult()==null?null:sampleTestResultCriteria.getTestResult().getDataValue())
				.setParameter("testResultValue", sampleTestResultCriteria.getTestResult()==null?null:sampleTestResultCriteria.getTestResult().getDataValue())
				.setParameter("fromTestReportDate", sampleTestResultCriteria.getFromTestReportDate(), TemporalType.DATE)
				.setParameter("toTestReportDate", sampleTestResultCriteria.getToTestReportDate(), TemporalType.DATE)
				.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
				.setHint(QueryHints.FETCH, O_CONTRACT)
				.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
				.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
				.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
				.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
				.getResultList();
			
			if(sampleTestScheduleList == null || sampleTestScheduleList.size()==0)
			{
				sampleTestScheduleList = null;
			}
			else{
				for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
					sampleTestScheduleFind.getPayment();
					sampleTestScheduleFind.getContract();
					sampleTestScheduleFind.getContract().getSupplier();
					sampleTestScheduleFind.getContract().getSupplier().getContact();
					sampleTestScheduleFind.getContract().getManufacturer();
					sampleTestScheduleFind.getContract().getManufacturer().getContact();
					sampleTestScheduleFind.getContract().getPharmCompany();
					sampleTestScheduleFind.getContract().getPharmCompany().getContact();
					sampleTestScheduleFind.getInstitution();
					sampleTestScheduleFind.getTestFrequency();
					sampleTestScheduleFind.getLab();
					sampleTestScheduleFind.getMicroBioTest();
					sampleTestScheduleFind.getChemicalAnalysis();
				}
			}
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleListByEnquiryDetailCriteria(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria){
		logger.debug("retrieveSampleTestScheduleListByEnquiryDetailCriteria" );
		
			StringBuffer enquiryDetailSql = constructSQLStringForEnquiryDetailCriteria(sampleEnquiryDetailCriteria);
			
			Query q = em.createQuery(enquiryDetailSql.toString())
									.setParameter(RECORD_STATUS, sampleEnquiryDetailCriteria.getRecordStatus())
									.setParameter("scheduleNum", sampleEnquiryDetailCriteria.getScheduleNum())
									.setParameter(ITEM_CODE, sampleEnquiryDetailCriteria.getItemCode())
									.setParameter("orderType", sampleEnquiryDetailCriteria.getOrderType()==null?null:sampleEnquiryDetailCriteria.getOrderType())
									.setParameter("orderTypeValue", sampleEnquiryDetailCriteria.getOrderType()==null?null:sampleEnquiryDetailCriteria.getOrderType().getDataValue())
									.setParameter(TEST_CODE, sampleEnquiryDetailCriteria.getTestCode())
									.setParameter("riskLevelCode", sampleEnquiryDetailCriteria.getRiskLevelCode())
									.setParameter("fromScheduleMonth", sampleEnquiryDetailCriteria.getFromScheduleMonth(), TemporalType.DATE)
   								    .setParameter("toScheduleMonth", sampleEnquiryDetailCriteria.getToScheduleMonth(), TemporalType.DATE)
									.setParameter("fromTestReportDate", sampleEnquiryDetailCriteria.getFromTestReportDate(), TemporalType.DATE)
									.setParameter("toTestReportDate", sampleEnquiryDetailCriteria.getToTestReportDate(), TemporalType.DATE);

			if(sampleEnquiryDetailCriteria.getScheduleStatusList().size() > 0) {
				q=q.setParameter(SCHE_STATUS_LIST, sampleEnquiryDetailCriteria.getScheduleStatusList());
			}
			/*else
			{
				q=q.setParameter("scheduleStatus", sampleEnquiryDetailCriteria.getScheduleStatus()==null?null:sampleEnquiryDetailCriteria.getScheduleStatus());
				q=q.setParameter("scheduleStatusValue", sampleEnquiryDetailCriteria.getScheduleStatus()==null?null:sampleEnquiryDetailCriteria.getScheduleStatus().getDataValue());
				
			}*/
			
			if(sampleEnquiryDetailCriteria.getSupplierCode()!=null) {
				q=q.setParameter(SUPPLIER_CODE, sampleEnquiryDetailCriteria.getSupplierCode());
			}	
			if(sampleEnquiryDetailCriteria.getManufCode() != null) {
				q=q.setParameter(MANUF_CODE, sampleEnquiryDetailCriteria.getManufCode());
			}	
			if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null) {
				q=q.setParameter("pharmCompanyCode", sampleEnquiryDetailCriteria.getPharmCompanyCode());
			}	
			if(sampleEnquiryDetailCriteria.getLabCode() != null) {
				q=q.setParameter("labCode", sampleEnquiryDetailCriteria.getLabCode());
			}	
			if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null) {
				q=q.setParameter("fromPrIssueDate", sampleEnquiryDetailCriteria.getFromPrIssueDate(), TemporalType.DATE);
			}	
			if(sampleEnquiryDetailCriteria.getToPrIssueDate() != null) {
				q=q.setParameter("toPrIssueDate", sampleEnquiryDetailCriteria.getToPrIssueDate(), TemporalType.DATE);
			}
			if(sampleEnquiryDetailCriteria.getPoNum()!= null) {
				q=q.setParameter("poNum", sampleEnquiryDetailCriteria.getPoNum());
			}

									q=q.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
									   .setHint(QueryHints.FETCH, O_CONTRACT)
									   .setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
									   .setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL);
								    if(sampleEnquiryDetailCriteria.getSupplierCode()!=null){
									   q=q.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER);
								    }   
									if(sampleEnquiryDetailCriteria.getManufCode() != null) {
									   q=q.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER);
									}  
									if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null) {
									   q=q.setHint(QueryHints.FETCH, "o.contract.pharmCompany");
									}   
									if(sampleEnquiryDetailCriteria.getLabCode() != null) {
									   q=q.setHint(QueryHints.FETCH, "o.lab");
									}  
									if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null || sampleEnquiryDetailCriteria.getToPrIssueDate() != null || sampleEnquiryDetailCriteria.getPoNum() != null) {
									   q=q.setHint(QueryHints.FETCH, "o.payment");
									}
									
			sampleTestScheduleList = q.getResultList();
					
			
			if(sampleTestScheduleList.size()==0)
			{
				sampleTestScheduleList = null;
			}
			else
			{
				for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
					sampleTestScheduleFind.getPayment();
					sampleTestScheduleFind.getContract();
					sampleTestScheduleFind.getContract().getSupplier();
					sampleTestScheduleFind.getContract().getSupplier().getContact();
					sampleTestScheduleFind.getContract().getManufacturer();
					sampleTestScheduleFind.getContract().getManufacturer().getContact();
					sampleTestScheduleFind.getContract().getPharmCompany();
					sampleTestScheduleFind.getContract().getPharmCompany().getContact();
					sampleTestScheduleFind.getInstitution();
					sampleTestScheduleFind.getTestFrequency();
					sampleTestScheduleFind.getLab();
					sampleTestScheduleFind.getMicroBioTest();
					sampleTestScheduleFind.getChemicalAnalysis();
				}
			}
		
	}
	
	private StringBuffer constructSQLStringForEnquiryDetailCriteria(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria){
		logger.debug("constructSQLStringForEnquiryDetailCriteria" );
		
		StringBuffer enquiryDetailSql = new StringBuffer();
		enquiryDetailSql.append("select o from SampleTestSchedule o "); 
		enquiryDetailSql.append("where o.recordStatus= :recordStatus ");
		if (sampleEnquiryDetailCriteria.getScheduleStatusList().size() > 0)
		{	enquiryDetailSql.append("and (o.scheduleStatus in :scheduleStatusList) ");}
		//else
		//{	enquiryDetailSql.append("and (o.scheduleStatus = :scheduleStatus or :scheduleStatusValue is null) ");}
		enquiryDetailSql.append("and (o.scheduleNum = :scheduleNum or :scheduleNum is null) ");
		enquiryDetailSql.append("and (o.sampleItem.itemCode = :itemCode or :itemCode is null) ");
		enquiryDetailSql.append("and (o.orderType = :orderType or :orderTypeValue is null) ");
		enquiryDetailSql.append("and (o.sampleTest.testCode = :testCode or :testCode is null) ");
		enquiryDetailSql.append("and (o.sampleItem.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth >= :fromScheduleMonth or :fromScheduleMonth is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth < :toScheduleMonth or :toScheduleMonth is null) ");
		enquiryDetailSql.append("and (o.testReportDate >= :fromTestReportDate or :fromTestReportDate is null) ");
		enquiryDetailSql.append("and (o.testReportDate < :toTestReportDate or :toTestReportDate is null) ");
		if(sampleEnquiryDetailCriteria.getSupplierCode()!=null) {
			enquiryDetailSql.append("and (o.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getManufCode() != null) {
			enquiryDetailSql.append("and (o.contract.manufacturer.companyCode = :manufCode or :manufCode is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null) {
			enquiryDetailSql.append("and (o.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getLabCode() != null) {
			enquiryDetailSql.append("and (o.lab.labCode = :labCode or :labCode is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null) {
			enquiryDetailSql.append("and (o.payment.prIssueDate >= :fromPrIssueDate or :fromPrIssueDate is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getToPrIssueDate() != null) {
			enquiryDetailSql.append("and (o.payment.prIssueDate < :toPrIssueDate or :toPrIssueDate is null) ");
		}	
		if(sampleEnquiryDetailCriteria.getPoNum() != null) {
			enquiryDetailSql.append("and (o.payment.poNum = :poNum or :poNum is null) ");
		}
		
		return enquiryDetailSql;
	}

	
	
	/*@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForCopy(Long scheduleId, String itemCode){
		logger.debug("retrieveSampleTestScheduleListForCopy :#0 #1", itemCode, scheduleId );
		success = false;
		
		sampleTestScheduleList= em.createNamedQuery("SampleTestSchedule.findForCopySchedule")
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHE_STATUS_LIST, new ArrayList<ScheduleStatus>(Arrays.asList(ScheduleStatus.Schedule, ScheduleStatus.ScheduleGen)))
			.setParameter("scheduleId", scheduleId)
			.setParameter(ITEM_CODE, itemCode)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.BATCH, O_INSTITUTION)
			.getResultList();
		
		if (!sampleTestScheduleList.isEmpty()){
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleList ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				sampleTestScheduleFind.getContract().getSupplier().getContact();
				sampleTestScheduleFind.getContract().getManufacturer();
				sampleTestScheduleFind.getContract().getManufacturer().getContact();
				sampleTestScheduleFind.getContract().getPharmCompany();
				sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
			success = true;
		}
	}*/
	
	@SuppressWarnings(UNCHECKED)
	public List<SampleTestSchedule> retrieveSampleTestScheduleListForOutStanding(String itemCode, String testCode){
		logger.debug("retrieveSampleTestScheduleListForOutStanding : #0 ", itemCode);
		List<SampleTestSchedule> scheduleList = em.createNamedQuery("SampleTestSchedule.findForOutstanding")
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHEDULE_STATUS, ScheduleStatus.Complete)
			.setParameter(ITEM_CODE, itemCode)
			.setParameter(TEST_CODE, testCode)
			.getResultList();
		
		if(scheduleList!=null && scheduleList.size()>0)
		{
			for(SampleTestSchedule sts:scheduleList)
			{
				getLazySampleTestSchedule(sts);
			}
		}
		
		return scheduleList;
	}
	
	public void copySampleTestSchedule(Collection<SampleTestSchedule> sampleTestScheduleCopyList) {
		logger.debug("copySampleTestSchedule, from schedule id : #0 ", sampleTestSchedule.getScheduleId());
		success = false;

		for(SampleTestSchedule sampleTesScheduleOld:sampleTestScheduleCopyList){
			logger.debug("update , to copy schedule id:#0 ", sampleTesScheduleOld.getScheduleId() );	
			
			sampleTesScheduleOld = copySchedule(sampleTestSchedule, sampleTesScheduleOld );
			
			em.merge(sampleTesScheduleOld.getContract());			
			em.merge(sampleTesScheduleOld);
			success = true;
		}
		
		if (!success){
			errorCode = "0030";
		}else {
			em.flush();
		}
	}
	
	private SampleTestSchedule copySchedule(SampleTestSchedule scheduleIn, SampleTestSchedule scheduleOut){
		
		Contract contractCopy = copyContract(scheduleIn.getContract(), scheduleOut.getContract());
		
		scheduleOut.setContract( contractCopy  );
		scheduleOut.setScheduleMonth(scheduleIn.getScheduleMonth());
		scheduleOut.setInstitution(scheduleIn.getInstitution());
		
		return scheduleOut;
	}
	
	private Contract copyContract(Contract contractIn , Contract contractOut){
		
		contractOut.setContractNum( contractIn.getContractNum());
		contractOut.setContractSuffix(contractIn.getContractSuffix());
		
		contractOut.setStartDate(contractIn.getStartDate());
		contractOut.setEndDate(contractIn.getEndDate());
		
		contractOut.setSupplier(contractIn.getSupplier());
		contractOut.setManufacturer(contractIn.getManufacturer());
		contractOut.setPharmCompany(contractIn.getPharmCompany());
		
		contractOut.setPackPrice(contractIn.getPackPrice());
		contractOut.setPackSize(contractIn.getPackSize());
		
		return contractOut;
	}
	
//	public void updateSampleTestScheduleListForInstAssignment(Collection<SampleTestSchedule> sampleTestScheduleIAList){
//		logger.debug("updateSampleTestScheduleListForInstAssignment");
//		success = false;
//		for(SampleTestSchedule tmpSched:sampleTestScheduleIAList){
//			logger.debug("update schedule :#0 ", tmpSched.getScheduleId() );	
//						
//			em.merge(tmpSched);
//			success = true;
//		}
//		
//		if (success){
//			em.flush();
//		}
//	}
	
	@SuppressWarnings(UNCHECKED)
	public List<SampleTestSchedule> retrieveSampleTestScheduleListForUpdateLastDocDate(String itemCode, String supplierCode, String manufCode, String pharmCode, Long scheduleId){
		logger.debug("retrieveSampleTestScheduleListForUpdateLastDocDate :#0 #1 #2 #3 #4", itemCode, supplierCode, manufCode, pharmCode, scheduleId );
		List<SampleTestSchedule> scheduleList = em.createNamedQuery("SampleTestSchedule.findForUpdateLastDocDate")
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHE_STATUS_LIST, new ArrayList<ScheduleStatus>(Arrays.asList(ScheduleStatus.Schedule, ScheduleStatus.ScheduleGen)))
			.setParameter("scheduleId", scheduleId)
			.setParameter(ITEM_CODE, itemCode)
			.setParameter(SUPPLIER_CODE, supplierCode)
			.setParameter(MANUF_CODE, manufCode)
			.setParameter("pharmCode", pharmCode)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.getResultList();
		
		if(scheduleList!=null && scheduleList.size()>0)
		{
			for(SampleTestSchedule sts:scheduleList)
			{
				getLazySampleTestSchedule(sts);
			}
		}
		
		return scheduleList;
	}
	
	
	@SuppressWarnings(UNCHECKED)
	public List<SampleTestSchedule> retrieveSampleTestScheduleIdListForTestFreqMaint(RecordStatus recordStatus, ScheduleStatus scheduleStatus, Long frequencyId){
		List<SampleTestSchedule> scheduleList = em.createNamedQuery("SampleTestSchedule.findForTestFreqMaint")
			.setParameter(RECORD_STATUS, recordStatus)
			.setParameter(SCHEDULE_STATUS, scheduleStatus)
			.setParameter("frequencyId", frequencyId)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.getResultList();
		
		return scheduleList;
	}
	
	
	@SuppressWarnings(UNCHECKED)
	public List<SampleTestSchedule> retrieveSampleTestScheduleListByContractLine(ContractLine contractLineIn){
		List<SampleTestSchedule> sampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findByContractLineIdModuleTypeRecordStatus")
																	.setParameter("erpContractLineId", contractLineIn.getContractLineId())
																	.setParameter("moduleType", ModuleType.SampleTest)
																	.setParameter(RECORD_STATUS, RecordStatus.Active)
																	.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
																	.setHint(QueryHints.FETCH, O_CONTRACT)
																	.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
																	.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
																	.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
																	.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
																	.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
																	.setHint(QueryHints.BATCH, O_INSTITUTION)
																	.setHint(QueryHints.BATCH, O_PAYMENT)
																	.getResultList();
		
		if(sampleTestScheduleListFind == null || sampleTestScheduleListFind.size()==0)
		{
			sampleTestScheduleListFind = null;
		}
		else
		{
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleListFind ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				if(sampleTestScheduleFind.getContract().getSupplier()!=null) {
					sampleTestScheduleFind.getContract().getSupplier().getContact();
				}	
				sampleTestScheduleFind.getContract().getManufacturer();
				if(sampleTestScheduleFind.getContract().getManufacturer()!=null) {
					sampleTestScheduleFind.getContract().getManufacturer().getContact();
				}	
				sampleTestScheduleFind.getContract().getPharmCompany();
				if(sampleTestScheduleFind.getContract().getPharmCompany()!=null) {
					sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				}
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
		}
		
		return sampleTestScheduleListFind;
	}
	
	public void updateSampleTestScheduleListForInstAssignment(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("updateSampleTestScheduleListForInstAssignment");
		for(SampleTestSchedule sts:sampleTestScheduleListIn)
		{
			sts=getLazySampleTestSchedule(sts);
			em.merge(sts);
			em.flush();
		}
	}
	
	public void resetSampleTestScheduleActionFlag(Calendar modifyDate)
	{
		em.createNamedQuery("SampleTestSchedule.updateSampleTestScheduleResetActionFlagByModifyDate")
		.setParameter("actionFlag", "Y")
		.setParameter("modifyDate", modifyDate.getTime())
		.setParameter(RECORD_STATUS, RecordStatus.Active)
		.setParameter("scheduleStatus", ScheduleStatus.ScheduleGen)
		.executeUpdate();
		
	}
	
	
	private Query constructReportQueryFromCriteria(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria)
	{
		StringBuffer enquiryDetailSql = constructSQLStringForReport(sampleEnquiryDetailCriteria);
		
		Query q = constructQueryFromCriteria(enquiryDetailSql, sampleEnquiryDetailCriteria);
								
		q = q.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL);
		
		if(sampleEnquiryDetailCriteria.getSupplierCode()!=null){
			q = q.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER);}
		if(sampleEnquiryDetailCriteria.getManufCode() != null){
			q = q.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER);}
		if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null){
			q = q.setHint(QueryHints.FETCH, "o.contract.pharmCompany");}
		if(sampleEnquiryDetailCriteria.getLabCode() != null){
			q = q.setHint(QueryHints.FETCH, "o.lab");}
		if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null || sampleEnquiryDetailCriteria.getToPrIssueDate() != null){
			q = q.setHint(QueryHints.FETCH, "o.payment");}
	
		return q;
	}
	
	private Query constructQueryFromCriteria(StringBuffer enquiryDetailSql, SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria)
	{
		Query q = em.createQuery(enquiryDetailSql.toString())
					.setParameter("scheduleNum", sampleEnquiryDetailCriteria.getScheduleNum())
					.setParameter(ITEM_CODE, sampleEnquiryDetailCriteria.getItemCode())
					.setParameter("orderType", sampleEnquiryDetailCriteria.getOrderType()==null?null:sampleEnquiryDetailCriteria.getOrderType())
					.setParameter("orderTypeValue", sampleEnquiryDetailCriteria.getOrderType()==null?null:sampleEnquiryDetailCriteria.getOrderType().getDataValue())
					.setParameter(TEST_CODE, sampleEnquiryDetailCriteria.getTestCode())
					.setParameter("riskLevelCode", sampleEnquiryDetailCriteria.getRiskLevelCode())
					.setParameter("fromScheduleMonth", sampleEnquiryDetailCriteria.getFromScheduleMonth(), TemporalType.DATE)
				    .setParameter("toScheduleMonth", sampleEnquiryDetailCriteria.getToScheduleMonth(), TemporalType.DATE)
					.setParameter("fromTestReportDate", sampleEnquiryDetailCriteria.getFromTestReportDate(), TemporalType.DATE)
					.setParameter("toTestReportDate", sampleEnquiryDetailCriteria.getToTestReportDate(), TemporalType.DATE);
		if(sampleEnquiryDetailCriteria.getRecordStatus() != null){
			q = q.setParameter(RECORD_STATUS, sampleEnquiryDetailCriteria.getRecordStatus());}
		
		q = constructQueryFromCriteriaForScheduleStatus(q, sampleEnquiryDetailCriteria);
	
		if(sampleEnquiryDetailCriteria.getSupplierCode()!=null){
			q = q.setParameter(SUPPLIER_CODE, sampleEnquiryDetailCriteria.getSupplierCode());}
		if(sampleEnquiryDetailCriteria.getManufCode() != null){
			q = q.setParameter(MANUF_CODE, sampleEnquiryDetailCriteria.getManufCode());}
		if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null){
			q = q.setParameter("pharmCompanyCode", sampleEnquiryDetailCriteria.getPharmCompanyCode());}
		if(sampleEnquiryDetailCriteria.getLabCode() != null){
			q = q.setParameter("labCode", sampleEnquiryDetailCriteria.getLabCode());}
		if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null){
			q = q.setParameter("fromPrIssueDate", sampleEnquiryDetailCriteria.getFromPrIssueDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getToPrIssueDate() != null){
			q = q.setParameter("toPrIssueDate", sampleEnquiryDetailCriteria.getToPrIssueDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getInstitutionCode() != null){
			q = q.setParameter("institutionCode", sampleEnquiryDetailCriteria.getInstitutionCode());}
		if(sampleEnquiryDetailCriteria.getContractNum() != null){
			q = q.setParameter("contractNum", sampleEnquiryDetailCriteria.getContractNum());}
		if(sampleEnquiryDetailCriteria.getContractSuffix() != null){
			q = q.setParameter("contractSuffix", sampleEnquiryDetailCriteria.getContractSuffix());}
		if(sampleEnquiryDetailCriteria.getContractStartDate() != null){
			q = q.setParameter("contractStartDate", sampleEnquiryDetailCriteria.getContractStartDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getContractEndDate() != null){
			q = q.setParameter("contractEndDate", sampleEnquiryDetailCriteria.getContractEndDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getBatchNum() != null){
			q = q.setParameter("batchNum", sampleEnquiryDetailCriteria.getBatchNum());}
		if(sampleEnquiryDetailCriteria.getExpiryDate() != null){
			q = q.setParameter("expiryDate", sampleEnquiryDetailCriteria.getExpiryDate());}
		if(sampleEnquiryDetailCriteria.getFromReceiveDate() != null){
			q = q.setParameter("fromReceiveDate", sampleEnquiryDetailCriteria.getFromReceiveDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getToReceiveDate() != null){
			q = q.setParameter("toReceiveDate", sampleEnquiryDetailCriteria.getToReceiveDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getFromLabCollectDate() != null){
			q = q.setParameter("fromLabCollectDate", sampleEnquiryDetailCriteria.getFromLabCollectDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getToLabCollectDate() != null){
			q = q.setParameter("toLabCollectDate", sampleEnquiryDetailCriteria.getToLabCollectDate(), TemporalType.DATE);}
		if(sampleEnquiryDetailCriteria.getCollectNum() != null){
			q = q.setParameter("collectNum", sampleEnquiryDetailCriteria.getCollectNum());}
		if(sampleEnquiryDetailCriteria.getPoNum() != null){
			q = q.setParameter("poNum", sampleEnquiryDetailCriteria.getPoNum());}
		if(sampleEnquiryDetailCriteria.getTestResult() != null){
			q = q.setParameter("testResult", sampleEnquiryDetailCriteria.getTestResult());}
		
		return q;
	}
	
	private Query constructQueryFromCriteriaForScheduleStatus(Query q, SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria)
	{
		if(sampleEnquiryDetailCriteria.getScheduleStatusList() != null && sampleEnquiryDetailCriteria.getScheduleStatusList().size() > 0){
			return q.setParameter(SCHE_STATUS_LIST, sampleEnquiryDetailCriteria.getScheduleStatusList());}
		else{
			return q;}
	}
	
	private StringBuffer constructSQLStringForReport(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria){
		StringBuffer enquiryDetailSql = new StringBuffer();
		enquiryDetailSql.append("select o from SampleTestSchedule o ");
		enquiryDetailSql.append("where (o.scheduleNum = :scheduleNum or :scheduleNum is null) ");
		enquiryDetailSql.append("and (o.sampleItem.itemCode = :itemCode or :itemCode is null) ");
		enquiryDetailSql.append("and (o.orderType = :orderType or :orderTypeValue is null) ");
		enquiryDetailSql.append("and (o.sampleTest.testCode = :testCode or :testCode is null) ");
		enquiryDetailSql.append("and (o.sampleItem.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth >= :fromScheduleMonth or :fromScheduleMonth is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth < :toScheduleMonth or :toScheduleMonth is null) ");
		enquiryDetailSql.append("and (o.testReportDate >= :fromTestReportDate or :fromTestReportDate is null) ");
		enquiryDetailSql.append("and (o.testReportDate < :toTestReportDate or :toTestReportDate is null) ");
		if(sampleEnquiryDetailCriteria.getRecordStatus() != null){
			enquiryDetailSql.append("and (o.recordStatus = :recordStatus) ");}
		if(sampleEnquiryDetailCriteria.getScheduleStatusList() != null && sampleEnquiryDetailCriteria.getScheduleStatusList().size() > 0){
			enquiryDetailSql.append("and (o.scheduleStatus in :scheduleStatusList) ");}
		if(sampleEnquiryDetailCriteria.getSupplierCode()!=null){
			enquiryDetailSql.append("and (o.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) ");}
		if(sampleEnquiryDetailCriteria.getManufCode() != null){
			enquiryDetailSql.append("and (o.contract.manufacturer.companyCode = :manufCode or :manufCode is null) ");}
		if(sampleEnquiryDetailCriteria.getPharmCompanyCode() !=null){
			enquiryDetailSql.append("and (o.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");}
		if(sampleEnquiryDetailCriteria.getLabCode() != null){
			enquiryDetailSql.append("and (o.lab.labCode = :labCode or :labCode is null) ");}
		if(sampleEnquiryDetailCriteria.getFromPrIssueDate() != null){
			enquiryDetailSql.append("and (o.payment.prIssueDate >= :fromPrIssueDate or :fromPrIssueDate is null) ");}
		if(sampleEnquiryDetailCriteria.getToPrIssueDate() != null){
			enquiryDetailSql.append("and (o.payment.prIssueDate < :toPrIssueDate or :toPrIssueDate is null) ");}
		if(sampleEnquiryDetailCriteria.getInstitutionCode() != null){
			enquiryDetailSql.append("and (o.institution.institutionCode = :institutionCode or :institutionCode is null) ");}
		if(sampleEnquiryDetailCriteria.getContractNum() != null){
			enquiryDetailSql.append("and (o.contract.contractNum = :contractNum or :contractNum is null) ");}
		if(sampleEnquiryDetailCriteria.getContractSuffix() != null){
			enquiryDetailSql.append("and (o.contract.contractSuffix = :contractSuffix or :contractSuffix is null) ");}
		if(sampleEnquiryDetailCriteria.getContractStartDate() != null){
			enquiryDetailSql.append("and (o.contract.startDate >= :contractStartDate or :contractStartDate is null) ");}
		if(sampleEnquiryDetailCriteria.getContractEndDate() != null){
			enquiryDetailSql.append("and (o.contract.endDate <= :contractEndDate or :contractEndDate is null) ");}
		if(sampleEnquiryDetailCriteria.getBatchNum() != null){
			enquiryDetailSql.append("and (o.batchNum = :batchNum or :batchNum is null) ");}
		if(sampleEnquiryDetailCriteria.getExpiryDate() != null){
			enquiryDetailSql.append("and (o.expiryDate = :expiryDate or :expiryDate is null) ");}
		if(sampleEnquiryDetailCriteria.getFromReceiveDate() != null){
			enquiryDetailSql.append("and (o.receiveDate >= :fromReceiveDate or :fromReceiveDate is null) ");}
		if(sampleEnquiryDetailCriteria.getToReceiveDate() != null){
			enquiryDetailSql.append("and (o.receiveDate <= :toReceiveDate or :toReceiveDate is null) ");}
		if(sampleEnquiryDetailCriteria.getFromLabCollectDate() != null){
			enquiryDetailSql.append("and (o.labCollectDate >= :fromLabCollectDate or :fromLabCollectDate is null) ");}
		if(sampleEnquiryDetailCriteria.getToLabCollectDate() != null){
			enquiryDetailSql.append("and (o.labCollectDate <= :toLabCollectDate or :toLabCollectDate is null) ");}
		if(sampleEnquiryDetailCriteria.getCollectNum() != null){
			enquiryDetailSql.append("and (o.labCollectNum = :collectNum or :collectNum is null) ");}
		if(sampleEnquiryDetailCriteria.getPoNum() != null){
			enquiryDetailSql.append("and (o.payment.poNum = :poNum or :poNum is null) ");}
		if(sampleEnquiryDetailCriteria.getTestResult() != null){
			enquiryDetailSql.append("and (o.testResult = :testResult or :testResult is null) ");}
		
		return enquiryDetailSql;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleListReport(Collection<Long> sampleTestScheduleKeyList){
		logger.debug("retrieveSampleTestScheduleListReport");
		
		exportSampleTestScheduleList = em.createNamedQuery("SampleTestSchedule.findByScheduleIdList")
		.setParameter("keyList",sampleTestScheduleKeyList )
		.getResultList();
		
		if (sampleTestScheduleTestResultList == null){
			sampleTestScheduleTestResultList = new ArrayList<SampleTestScheduleTestResult>();}
		else{
			sampleTestScheduleTestResultList.clear();}
		
		if(exportSampleTestScheduleList == null || exportSampleTestScheduleList.size() == 0)
		{
			exportSampleTestScheduleList = null;
		}
		else
		{
			SampleTestScheduleTestResult sampleTestScheduleTestResultAdd;
			SampleTestSchedule sampleTestScheduleGetLazyIn;
			for( SampleTestSchedule sampleTestScheduleFind:exportSampleTestScheduleList ) {
				sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleFind);
				
				sampleTestScheduleTestResultAdd = constructSampleTestScheduleTestResultDataByRecord(sampleTestScheduleGetLazyIn);
				sampleTestScheduleTestResultList.add(sampleTestScheduleTestResultAdd);
			}
		}
	}
	
	private SampleTestScheduleTestResult constructSampleTestScheduleTestResultDataByRecord(SampleTestSchedule sampleTestSchedule) {
		SampleTestScheduleTestResult sampleTestScheduleTestResultAdd = new SampleTestScheduleTestResult();
		
		sampleTestScheduleTestResultAdd.setScheduleStatus(sampleTestSchedule.getScheduleStatus().getDisplayValue());
		
		if (sampleTestSchedule.getScheduleMonth() != null){
			sampleTestScheduleTestResultAdd.setScheduleMonth(sampleTestSchedule.getScheduleMonth());}
		
		sampleTestScheduleTestResultAdd.setScheduleNum(sampleTestSchedule.getScheduleNum());
		if (sampleTestSchedule.getTestReportDate() != null){
			sampleTestScheduleTestResultAdd.setTestReportDate(sampleTestSchedule.getTestReportDate());}
		
		if (sampleTestSchedule.getSampleItem() != null) {
			sampleTestScheduleTestResultAdd.setItemCode(sampleTestSchedule.getSampleItem().getItemCode());
			sampleTestScheduleTestResultAdd.setItemDesc(sampleTestSchedule.getSampleItem().getDmDrug()==null?"":sampleTestSchedule.getSampleItem().getDmDrug().getFullDrugDesc());
		}
		
		if (sampleTestSchedule.getSampleTest() != null){
			sampleTestScheduleTestResultAdd.setTestCode(sampleTestSchedule.getSampleTest().getTestCode());}
		
		sampleTestScheduleTestResultAdd.setOrderType(sampleTestSchedule.getOrderType().getDataValue());
		
		if (sampleTestSchedule.getSampleItem() != null && sampleTestSchedule.getSampleItem().getRiskLevel() != null){
			sampleTestScheduleTestResultAdd.setRiskLevelCode(sampleTestSchedule.getSampleItem().getRiskLevel().getRiskLevelCode());}
		
		if (sampleTestSchedule.getLab() != null){
			sampleTestScheduleTestResultAdd.setLabCode(sampleTestSchedule.getLab().getLabCode());}
		
		if (sampleTestSchedule.getTestPrice() != null){
			sampleTestScheduleTestResultAdd.setTestPrice(sampleTestSchedule.getTestPrice());}
		
		if (sampleTestSchedule.getPayment() != null) {
			if (sampleTestSchedule.getPayment().getPrIssueDate() != null){
				sampleTestScheduleTestResultAdd.setPrIssueDate(sampleTestSchedule.getPayment().getPrIssueDate());}
			
			if (sampleTestSchedule.getPayment().getPoNum() != null){
				sampleTestScheduleTestResultAdd.setPoNum(sampleTestSchedule.getPayment().getPoNum());}
		}
		sampleTestScheduleTestResultAdd.setTestResult(sampleTestSchedule.getTestResult());
		
		return sampleTestScheduleTestResultAdd;
	}

	
		
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleListByCriteriaReport(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria){
		logger.debug("retrieveSampleTestScheduleListByCriteriaReport");
			
		Query q = constructReportQueryFromCriteria(sampleEnquiryDetailCriteria);
		exportSampleTestScheduleList = q.getResultList();
		
		boolean testResultReport = false;
		if (sampleEnquiryDetailCriteria.getReportTemplate() == null) {
			return;
		} else if (ReportTemplate.TR.getDisplayValue().equals(sampleEnquiryDetailCriteria.getReportTemplate().getDisplayValue())) {
			// Test Result
			testResultReport = true;
		} else if (ReportTemplate.ALL.getDisplayValue().equals(sampleEnquiryDetailCriteria.getReportTemplate().getDisplayValue())) {
			testResultReport = false;
		}

		
		if (sampleTestScheduleTestResultList == null){
			sampleTestScheduleTestResultList = new ArrayList<SampleTestScheduleTestResult>();}
		else{
			sampleTestScheduleTestResultList.clear();}
	
		if(exportSampleTestScheduleList == null || exportSampleTestScheduleList.size() == 0)
		{
			exportSampleTestScheduleList = null;
		}
		else
		{
			SampleTestScheduleTestResult sampleTestScheduleTestResultAdd;
			SampleTestSchedule sampleTestScheduleGetLazyIn;
			for( SampleTestSchedule sampleTestScheduleFind:exportSampleTestScheduleList ) {
				sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleFind);

				if (testResultReport){
					sampleTestScheduleTestResultAdd = constructSampleTestScheduleTestResultData(sampleTestScheduleGetLazyIn);
				}else{
					sampleTestScheduleTestResultAdd = constructSampleTestScheduleTestResultAllData(sampleTestScheduleGetLazyIn);
				}

				sampleTestScheduleTestResultList.add(sampleTestScheduleTestResultAdd);
			}
		}

	}

	public SampleTestScheduleTestResult constructSampleTestScheduleTestResultData(SampleTestSchedule sampleTestSchedule) {
		logger.debug("constructSampleTestScheduleTestResultData");
		SampleTestScheduleTestResult sampleTestScheduleTestResultAdd = new SampleTestScheduleTestResult();
		
		if (sampleTestSchedule.getTestReportDate() != null){
			sampleTestScheduleTestResultAdd.setTestReportDate(sampleTestSchedule.getTestReportDate());}
		
		if (sampleTestSchedule.getSampleItem() != null) {
			sampleTestScheduleTestResultAdd.setItemCode(sampleTestSchedule.getSampleItem().getItemCode());
			sampleTestScheduleTestResultAdd.setItemDesc(sampleTestSchedule.getSampleItem().getDmDrug()==null?"":sampleTestSchedule.getSampleItem().getDmDrug().getFullDrugDesc());
		}

		if (sampleTestSchedule.getSampleTest() != null) {
			sampleTestScheduleTestResultAdd.setTestCode(sampleTestSchedule.getSampleTest().getTestCode());
		}

		if (sampleTestSchedule.getContract() != null) {
			if (sampleTestSchedule.getContract().getSupplier() != null){
				sampleTestScheduleTestResultAdd.setSupplierCode(sampleTestSchedule.getContract().getSupplier().getSupplierCode());}
			
			if (sampleTestSchedule.getContract().getManufacturer() != null){
				sampleTestScheduleTestResultAdd.setManufCode(sampleTestSchedule.getContract().getManufacturer().getCompanyCode());}
			
			if (sampleTestSchedule.getContract().getPharmCompany() != null){
				sampleTestScheduleTestResultAdd.setPharmCompanyCode(sampleTestSchedule.getContract().getPharmCompany().getCompanyCode());}
		}

		sampleTestScheduleTestResultAdd.setTestResult(sampleTestSchedule.getTestResult());
		
		return sampleTestScheduleTestResultAdd;
	}
	
	private SampleTestScheduleTestResult constructSampleTestScheduleTestResultAllData(SampleTestSchedule sampleTestSchedule) {
		logger.debug("constructSampleTestScheduleTestResultAllData");
		SampleTestScheduleTestResult sampleTestScheduleTestResultAdd = constructSampleTestScheduleTestResultData(sampleTestSchedule);
		
		sampleTestScheduleTestResultAdd.setScheduleStatus(sampleTestSchedule.getScheduleStatus().getDisplayValue());
		sampleTestScheduleTestResultAdd.setScheduleMonth(sampleTestSchedule.getScheduleMonth());
		sampleTestScheduleTestResultAdd.setScheduleNum(sampleTestSchedule.getScheduleNum());
		sampleTestScheduleTestResultAdd.setScheduleConfirmDate(sampleTestSchedule.getScheduleConfirmDate());
		sampleTestScheduleTestResultAdd.setScheduleStartDate(sampleTestSchedule.getScheduleStartDate());
		sampleTestScheduleTestResultAdd.setRecordStatus(sampleTestSchedule.getRecordStatus().getDataValue());
		sampleTestScheduleTestResultAdd.setCancelReason(sampleTestSchedule.getCancelSchedReason().getDataValue());
		
		if (sampleTestSchedule.getSampleItem() != null && sampleTestSchedule.getSampleItem().getDmDrug() != null){
			sampleTestScheduleTestResultAdd.setBaseUnit(sampleTestSchedule.getSampleItem().getDmDrug().getBaseUnit());}
		
		sampleTestScheduleTestResultAdd.setOrderType(sampleTestSchedule.getOrderType().getDataValue());
		
		if (sampleTestSchedule.getContract() != null) {
			sampleTestScheduleTestResultAdd.setContractNum(sampleTestSchedule.getContract().getContractNum());
			if (sampleTestSchedule.getContract().getPackSize() != null){
				sampleTestScheduleTestResultAdd.setPackSize(sampleTestSchedule.getContract().getPackSize());}
			if (sampleTestSchedule.getContract().getPackPrice() != null){
				sampleTestScheduleTestResultAdd.setPackPrice(sampleTestSchedule.getContract().getPackPrice());}
			sampleTestScheduleTestResultAdd.setCurrencyCode(sampleTestSchedule.getContract().getCurrencyCode());
		}
		
		sampleTestScheduleTestResultAdd.setBatchNum(sampleTestSchedule.getBatchNum());
		sampleTestScheduleTestResultAdd.setExpiryDate(sampleTestSchedule.getExpiryDate());
		if (sampleTestSchedule.getReqQty() != null){
			sampleTestScheduleTestResultAdd.setReqQty(sampleTestSchedule.getReqQty());}

		if (sampleTestSchedule.getRecQty() != null){
			sampleTestScheduleTestResultAdd.setRecQty(sampleTestSchedule.getRecQty());}

		sampleTestScheduleTestResultAdd.setLabCollectNum(sampleTestSchedule.getLabCollectNum());
		sampleTestScheduleTestResultAdd.setLabCollectDate(sampleTestSchedule.getLabCollectDate());
		sampleTestScheduleTestResultAdd.setTestResultDesc(sampleTestSchedule.getTestResultDesc());
		sampleTestScheduleTestResultAdd.setRefDrugCost(sampleTestSchedule.getRefDrugCost());
		
		if (sampleTestSchedule.getInstitution() != null){
			sampleTestScheduleTestResultAdd.setInstitutionCode(sampleTestSchedule.getInstitution().getInstitutionCode());}
		
		if (sampleTestSchedule.getLab() != null){
			sampleTestScheduleTestResultAdd.setLabCode(sampleTestSchedule.getLab().getLabCode());}
		
		SampleTestScheduleTestResult sampleTestScheduleTestResultAddPlus = constructSampleTestScheduleTestResultByTest(sampleTestScheduleTestResultAdd, sampleTestSchedule);
		
		if (sampleTestSchedule.getPayment() != null) {
			sampleTestScheduleTestResultAddPlus.setPoNum(sampleTestSchedule.getPayment().getPoNum());
			sampleTestScheduleTestResultAddPlus.setPrIssueDate(sampleTestSchedule.getPayment().getPrIssueDate());
			sampleTestScheduleTestResultAddPlus.setChequeAmount(sampleTestSchedule.getPayment().getChequeAmount()==null?null:sampleTestSchedule.getPayment().getChequeAmount().doubleValue());
			sampleTestScheduleTestResultAddPlus.setChequeNum(sampleTestSchedule.getPayment().getChequeNum());
			sampleTestScheduleTestResultAddPlus.setChequeReceiveDate(sampleTestSchedule.getPayment().getChequeReceiveDate());
			sampleTestScheduleTestResultAddPlus.setChequeSendDate(sampleTestSchedule.getPayment().getChequeSendDate());
			sampleTestScheduleTestResultAddPlus.setInvRefNum(sampleTestSchedule.getPayment().getInvRefNum());
			sampleTestScheduleTestResultAddPlus.setInvReceiveDate(sampleTestSchedule.getPayment().getInvReceiveDate());
			sampleTestScheduleTestResultAddPlus.setInvSendDate(sampleTestSchedule.getPayment().getInvSendDate());
			sampleTestScheduleTestResultAddPlus.setPaymentRemark(sampleTestSchedule.getPayment().getRemark());
		}
		
		if (sampleTestSchedule.getSampleItem() != null && sampleTestSchedule.getSampleItem().getRiskLevel() != null){
			sampleTestScheduleTestResultAddPlus.setRiskLevelCode(sampleTestSchedule.getSampleItem().getRiskLevel().getRiskLevelCode());}
		
		if (sampleTestSchedule.getTestPrice() != null){
			sampleTestScheduleTestResultAddPlus.setTestPrice(sampleTestSchedule.getTestPrice());}

		return sampleTestScheduleTestResultAddPlus;
	}
	
	public SampleTestScheduleTestResult constructSampleTestScheduleTestResultByTest(SampleTestScheduleTestResult sampleTestScheduleTestResultAdd, SampleTestSchedule sampleTestScheduleIn){
		logger.debug("constructSampleTestScheduleTestResult");
		
		if (sampleTestScheduleIn.getMicroBioTest() != null) {
			sampleTestScheduleTestResultAdd.setRawMaterial(sampleTestScheduleIn.getMicroBioTest().getRawMaterial()==null?null:sampleTestScheduleIn.getMicroBioTest().getRawMaterial().getDataValue());
			sampleTestScheduleTestResultAdd.setValidationRpt(sampleTestScheduleIn.getMicroBioTest().getValidationRpt()==null?null:sampleTestScheduleIn.getMicroBioTest().getValidationRpt().getDataValue());
			sampleTestScheduleTestResultAdd.setValidationRptValidityFlag(sampleTestScheduleIn.getMicroBioTest().getValidationRptValidityFlag()==null?null:sampleTestScheduleIn.getMicroBioTest().getValidationRptValidityFlag().getDataValue());
			sampleTestScheduleTestResultAdd.setMicroTestParam(sampleTestScheduleIn.getMicroBioTest().getTestParam()==null?null:sampleTestScheduleIn.getMicroBioTest().getTestParam().getDataValue());
			sampleTestScheduleTestResultAdd.setMicroNationalPharmStd(sampleTestScheduleIn.getMicroBioTest().getNationalPharmStd()==null?null:sampleTestScheduleIn.getMicroBioTest().getNationalPharmStd().getDataValue());
			sampleTestScheduleTestResultAdd.setAcceptCriteria(sampleTestScheduleIn.getMicroBioTest().getAcceptCriteria()==null?null:sampleTestScheduleIn.getMicroBioTest().getAcceptCriteria().getDataValue());
			sampleTestScheduleTestResultAdd.setAcceptCriteriaDesc(sampleTestScheduleIn.getMicroBioTest().getAcceptCriteriaDesc());
		}
		
		if (sampleTestScheduleIn.getChemicalAnalysis() != null) {
			sampleTestScheduleTestResultAdd.setSpecification(sampleTestScheduleIn.getChemicalAnalysis().getSpecification()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getSpecification().getDataValue());
			sampleTestScheduleTestResultAdd.setMoa(sampleTestScheduleIn.getChemicalAnalysis().getMoa()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getMoa().getDataValue());
			sampleTestScheduleTestResultAdd.setFps(sampleTestScheduleIn.getChemicalAnalysis().getFps()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getFps().getDataValue());
			sampleTestScheduleTestResultAdd.setRefStd(sampleTestScheduleIn.getChemicalAnalysis().getRefStd()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getRefStd().getDataValue());
			sampleTestScheduleTestResultAdd.setRefStdReqFlag(sampleTestScheduleIn.getChemicalAnalysis().getRefStdReqFlag()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getRefStdReqFlag().getDataValue());
			sampleTestScheduleTestResultAdd.setRetentionQty(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty());
			sampleTestScheduleTestResultAdd.setChemNationalPharmStd(sampleTestScheduleIn.getChemicalAnalysis().getSpecification()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getSpecification().getDisplayValue());
			sampleTestScheduleTestResultAdd.setChemTest(sampleTestScheduleIn.getChemicalAnalysis().getTest()==null?null:sampleTestScheduleIn.getChemicalAnalysis().getTest().getDataValue());
			sampleTestScheduleTestResultAdd.setChemTestDesc(sampleTestScheduleIn.getChemicalAnalysis().getTestDesc());
		}
		
		return sampleTestScheduleTestResultAdd;
	}


	public List<SampleTestScheduleTestResult> getSampleTestScheduleTestResultList(){
	  	return sampleTestScheduleTestResultList;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	@Remove
	public void destroy() {
		if (sampleTestScheduleList !=null){
			sampleTestScheduleList = null;
		}
		if (exportSampleTestScheduleList != null) {
			exportSampleTestScheduleList = null;
		}
		if (sampleTestScheduleTestResultList != null) {
			sampleTestScheduleTestResultList = null;
		}
	}
}
