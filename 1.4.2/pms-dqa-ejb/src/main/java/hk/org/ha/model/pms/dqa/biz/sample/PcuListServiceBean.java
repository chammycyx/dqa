package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("pcuListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PcuListServiceBean implements PcuListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Institution> pcuList;
	
	@SuppressWarnings("unchecked")
	public void retrievePcuList(){
		logger.debug("retrievePcuList");
		
		pcuList = em.createNamedQuery("Institution.findByPcuFlagRecordStatus")
			.setParameter("pcuFlag", YesNoFlag.Yes)
			.setParameter("recordStatus", "A")
			.setHint(QueryHints.REFRESH, HintValues.TRUE)
			.setHint(QueryHints.FETCH, "o.contact")
			.getResultList();
	}
	
	@Remove
	public void destroy(){
		if (pcuList != null){
			pcuList = null;
		}
	}
	
}
