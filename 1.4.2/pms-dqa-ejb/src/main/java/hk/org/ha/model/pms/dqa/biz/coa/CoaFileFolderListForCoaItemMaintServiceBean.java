package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaFileFolderListForCoaItemMaintService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaFileFolderListForCoaItemMaintServiceBean implements CoaFileFolderListForCoaItemMaintServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaFileFolder> coaFileFolderForCoaItemMaintList;
		
	public void retrieveCoaFileFolderListForCoaItemMaint(CoaItem coaItem)
	  {   
		  logger.info("retrieveCoaFileFolderListForCoaItemMaint");
	
		  coaFileFolderForCoaItemMaintList =  em.createNamedQuery("CoaFileFolder.findCoaFileFolderListForCoaItemMaint")
			  .setParameter("coaItem", coaItem)
			  .setHint(QueryHints.BATCH, "o.coaBatch.coaItem.contract")
			  .setHint(QueryHints.LEFT_FETCH, "o.coaBatch.coaBatchVer")
			  .setHint(QueryHints.FETCH, "o.fileItem")
			  .getResultList(); 

		  if (coaFileFolderForCoaItemMaintList.size() > 0){
			  for (  CoaFileFolder coaFileFolder : coaFileFolderForCoaItemMaintList){
				  coaFileFolder.getCoaBatch().getCoaItem();
				  coaFileFolder.getFileItem();
			  }
		  }else{ 
			  coaFileFolderForCoaItemMaintList = null;
		  }
	  }

	@Remove
	public void destroy() {
		if (coaFileFolderForCoaItemMaintList !=null){
			coaFileFolderForCoaItemMaintList = null;
		}
	}

}
