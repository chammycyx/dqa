package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "STOCK_MONTHLY_EXP")
@NamedQueries({
	@NamedQuery(name = "StockMonthlyExp.findByItemYearMonth", query = "select o from StockMonthlyExp o " +
					"where o.itemCode = :itemCode and o.year = :year and o.month = :month "),
	@NamedQuery(name = "StockMonthlyExp.findByYearMonthItemList", query = "select o from StockMonthlyExp o " +
					"where o.year = :year and o.month = :month and o.itemCode in :itemCodeList "),
	@NamedQuery(name = "StockMonthlyExp.findByYearMonth", query = "select o from StockMonthlyExp o " +
					"where o.year = :year and o.month = :month ")
					
})
@Customizer(AuditCustomizer.class)
public class StockMonthlyExp extends VersionEntity{

	private static final long serialVersionUID = -8655905658326830669L;

	@Id
	@Column(name="ITEM_CODE")
	private String itemCode;
	
	@Id
	@Column(name="YEAR")
	private Integer year;
	
	@Id
	@Column(name="MONTH")
	private Integer month;

	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	
}
