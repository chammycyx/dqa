package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.HasFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_PROBLEM")
@NamedQueries({
	@NamedQuery(name = "QaProblem.findAllWithOrderByCreateDateDesc", query = "select o from QaProblem o order by o.createDate desc "),
	@NamedQuery(name = "QaProblem.findLikeCaseNumByItemSMP", query = "select o from QaProblem o where o.caseNum like :caseNum " +
																	   "and o.problemHeader.itemCode = :itemCode " +
																	   "and o.supplier.supplierCode = :supplierCode " +
																	   "and o.manufacturer.companyCode = :manufCode " +
																	   "and o.pharmCompany.companyCode = :pharmCompanyCode " +
																	   "and o.recordStatus = :recordStatus "),
	@NamedQuery(name = "QaProblem.findByCaseNumRecordStatus", query = "select o from QaProblem o where o.caseNum = :caseNum " +
																	   "and o.recordStatus = :recordStatus "),
    @NamedQuery(name = "QaProblem.findByQaProblemId", query = "select o from QaProblem o where o.qaProblemId = :qaProblemId and o.recordStatus=:recordStatus "),
    @NamedQuery(name = "QaProblem.findLikeCaseNum", query = "select o from QaProblem o where o.caseNum like :caseNum " +
    																	"and o.recordStatus = :recordStatus ")
	
})
@Customizer(AuditCustomizer.class)
public class QaProblem extends VersionEntity {

	private static final long serialVersionUID = -4119632257920847487L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaProblemSeq")
	@SequenceGenerator(name = "qaProblemSeq", sequenceName = "SEQ_QA_PROBLEM", initialValue=10000)
	@Id
	@Column(name="QA_PROBLEM_ID", nullable=false)
	private Long qaProblemId;

	@Column(name="CASE_NUM", length=10, nullable=false)
	private String caseNum;
	
	
	@Column(name="INVEST_RPT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date investigationReportDate;
	
	@Column(name="CLOSE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date caseCloseDate;
	
	
	@Converter(name = "QaProblem.caseStatus", converterClass = CaseStatus.Converter.class )
	@Convert("QaProblem.caseStatus")
	@Column(name="CASE_STATUS", length=1)
	private CaseStatus caseStatus;
	
	@Column(name="CASE_SUMMARY", length=500)
	private String caseSummary;
	
	@Converter(name = "QaProblem.similarProblem", converterClass = HasFlag.Converter.class )
	@Convert("QaProblem.similarProblem")
	@Column(name="SIMILAR_PROBLEM", length=1)
	private HasFlag similarProblem;

	@Column(name="DELETE_REASON", length=255)
	private String deleteReason;
	
	@Converter(name = "QaProblem.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("QaProblem.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@OneToOne
	@JoinColumn(name="PROBLEM_HEADER_ID")
	private ProblemHeader problemHeader;
	
	@OneToOne
	@JoinColumn(name="QA_PROBLEM_NATURE_ID")
	private QaProblemNature qaProblemNature;
	
	@OneToOne
	@JoinColumn(name="CLASSIFICATION_ID")
	private Classification classification;
	
	@OneToOne
	@JoinColumn(name="FAX_SUMMARY_ID")
	private FaxSummary faxSummary;
	
	@ManyToOne
	@JoinColumn(name="SUPPLIER_CODE", referencedColumnName="SUPPLIER_CODE")
	private Supplier supplier;
	
	@ManyToOne	
	@JoinColumn(name="MANUF_CODE", referencedColumnName="COMPANY_CODE")
	private Company manufacturer;
	
	@ManyToOne
	@JoinColumn(name="PHARM_COMPANY_CODE", referencedColumnName="COMPANY_CODE")
	private Company pharmCompany;
	
	@OneToMany(mappedBy="qaProblem")
    private List<QaBatchNum> qaBatchNumList;
	
	@OneToMany(mappedBy="qaProblem")
    private List<PharmProblem> pharmProblemList;
	
	@OneToMany(mappedBy="qaProblem")
    private List<EventLog> eventLogList;
	
	@OneToMany(mappedBy="qaProblem")
    private List<QaProblemFile> qaProblemFileList;
	
	@OneToMany(mappedBy="qaProblem")
    private List<QaProblemInstitution> qaProblemInstitutionList;
	
	@OneToMany(mappedBy="qaProblem")
    private List<QaProblemNatureSubCat> qaProblemNatureSubCatList;
	

	
	public Long getQaProblemId() {
		return qaProblemId;
	}

	public void setQaProblemId(Long qaProblemId) {
		this.qaProblemId = qaProblemId;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public CaseStatus getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(CaseStatus caseStatus) {
		this.caseStatus = caseStatus;
	}

	public String getCaseSummary() {
		return caseSummary;
	}

	public void setCaseSummary(String caseSummary) {
		this.caseSummary = caseSummary;
	}

	public HasFlag getSimilarProblem() {
		return similarProblem;
	}

	public void setSimilarProblem(HasFlag similarProblem) {
		this.similarProblem = similarProblem;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public ProblemHeader getProblemHeader() {
		return problemHeader;
	}

	public void setProblemHeader(ProblemHeader problemHeader) {
		this.problemHeader = problemHeader;
	}

	public Company getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Company manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Company getPharmCompany() {
		return pharmCompany;
	}

	public void setPharmCompany(Company pharmCompany) {
		this.pharmCompany = pharmCompany;
	}
	
	public QaProblemNature getQaProblemNature() {
		return qaProblemNature;
	}

	public void setQaProblemNature(QaProblemNature qaProblemNature) {
		this.qaProblemNature = qaProblemNature;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public FaxSummary getFaxSummary() {
		return faxSummary;
	}

	public void setFaxSummary(FaxSummary faxSummary) {
		this.faxSummary = faxSummary;
	}

	public List<QaBatchNum> getQaBatchNumList() {
		return qaBatchNumList;
	}

	public void setQaBatchNumList(List<QaBatchNum> qaBatchNumList) {
		this.qaBatchNumList = qaBatchNumList;
	}
	
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	public List<PharmProblem> getPharmProblemList() {
		return pharmProblemList;
	}

	public void setPharmProblemList(List<PharmProblem> pharmProblemList) {
		this.pharmProblemList = pharmProblemList;
	}

	public List<EventLog> getEventLogList() {
		return eventLogList;
	}

	public void setEventLogList(List<EventLog> eventLogList) {
		this.eventLogList = eventLogList;
	}

	public List<QaProblemFile> getQaProblemFileList() {
		return qaProblemFileList;
	}

	public void setQaProblemFileList(List<QaProblemFile> qaProblemFileList) {
		this.qaProblemFileList = qaProblemFileList;
	}

	public List<QaProblemInstitution> getQaProblemInstitutionList() {
		return qaProblemInstitutionList;
	}

	public void setQaProblemInstitutionList(
			List<QaProblemInstitution> qaProblemInstitutionList) {
		this.qaProblemInstitutionList = qaProblemInstitutionList;
	}

	public List<QaProblemNatureSubCat> getQaProblemNatureSubCatList() {
		return qaProblemNatureSubCatList;
	}

	public void setQaProblemNatureSubCatList(
			List<QaProblemNatureSubCat> qaProblemNatureSubCatList) {
		this.qaProblemNatureSubCatList = qaProblemNatureSubCatList;
	}

	public Date getInvestigationReportDate() {
		return investigationReportDate;
	}

	public void setInvestigationReportDate(Date investigationReportDate) {
		this.investigationReportDate = investigationReportDate;
	}

	public Date getCaseCloseDate() {
		return caseCloseDate;
	}

	public void setCaseCloseDate(Date caseCloseDate) {
		this.caseCloseDate = caseCloseDate;
	}
	
	
	
	
	
}
