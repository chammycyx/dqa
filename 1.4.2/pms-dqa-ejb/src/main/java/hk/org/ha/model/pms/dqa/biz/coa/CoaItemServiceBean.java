package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.biz.ContractManagerLocal;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaItemService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaItemServiceBean implements CoaItemServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	@Out(required = false)
	private CoaItem coaItem;

	@In(create=true)
	private CoaFileFolderManagerLocal coaFileFolderManager;	

	@In(create=true)
	private ContractManagerLocal contractManager; 

	@In
	private DmDrugCacherInf dmDrugCacher;

	private boolean duplicated;

	private boolean itemNotExist; 

	@SuppressWarnings("unchecked")
	private CoaItem retrieveCoaItem(CoaItem coaItemIn){
		logger.info("retrieveCoaItem");
		CoaItem coaItemFind;

		List<CoaItem> coaItems = null;

		if (coaItemIn.getOrderType() == OrderType.DirectPurchase){
			coaItems = em.createNamedQuery("CoaItem.findCoaItemForCoaItemMaintDType")
			.setParameter("itemCode", coaItemIn.getContract().getItemCode().trim().toUpperCase(Locale.ENGLISH))
			.setParameter("supplierCode", coaItemIn.getContract().getSupplier().getSupplierCode().trim()  )
			.setParameter("orderType", coaItemIn.getOrderType())
			.setParameter("moduleType", ModuleType.COA)
			.setHint(QueryHints.FETCH, "o.contract")
			.setHint(QueryHints.FETCH, "o.contract.supplier")
			.setHint(QueryHints.BATCH, "o.coaBatchList")
			.getResultList();
		}else{
			coaItems = em.createNamedQuery("CoaItem.findCoaItemForCoaItemMaint")
			.setParameter("itemCode", coaItemIn.getContract().getItemCode().trim().toUpperCase(Locale.ENGLISH))
			.setParameter("contractNum", coaItemIn.getOrderType()==OrderType.DirectPurchase?null:coaItemIn.getContract().getContractNum().trim())
			.setParameter("orderType1", OrderType.Contract )
			.setParameter("orderType2", OrderType.StandardQuotation )
			.setParameter("moduleType", ModuleType.COA)
			.setHint(QueryHints.FETCH, "o.contract")
			.setHint(QueryHints.FETCH, "o.coaBatch")
			.setHint(QueryHints.FETCH, "o.contract.supplier")
			.setHint(QueryHints.FETCH, "o.contract.manufacturer")
			.setHint(QueryHints.BATCH, "o.coaBatchList")
			.getResultList();
		}

		if (coaItems!=null && coaItems.size()>0){
			coaItemFind = coaItems.get(0);
			coaItemFind.getCoaBatch();
			coaItemFind.getCoaBatchList().size();
		}else{
			coaItemFind = null;
		}
		return coaItemFind;	
	}

	public void retrieveCoaItemByCoaItem(CoaItem coaItemIn){
		logger.info("retrieveCoaItemByCoaItem");

		coaItem = retrieveCoaItem(coaItemIn);
	}

	public void addCoaItem(){
		logger.debug("createCoaItem");

		coaItem = new CoaItem();
		CoaBatch tempCoaBatch = new CoaBatch();
		Contract tempContract = new Contract();
		Supplier tempSupplier = new Supplier();
		Company tempCompany = new Company();
		Company tempPharmCompany = new Company();
		
		tempSupplier.setContact(null);
		tempContract.setSupplier(tempSupplier);
		tempContract.setManufacturer(tempCompany);
		tempContract.setPharmCompany(tempPharmCompany);
		tempContract.getManufacturer().getContact();
		tempContract.getPharmCompany().getContact();
		
		coaItem.setContract(tempContract);
		coaItem.setCoaBatch(tempCoaBatch);
	}


	public String createCoaItem(CoaItem coaItemIn){
		logger.debug("createCoaItem");
		Contract existingContract;
		Contract newContract;
		Supplier newSupplier;		
		Company newCompany;
		Company newPharmCompany;
		duplicated = true;
		itemNotExist = true;
		CoaItem coaItemFind = new CoaItem();

		DmDrug dmDrugFind = dmDrugCacher.getDrugByItemCode(coaItemIn.getContract().getItemCode());
		if (dmDrugFind != null){	
			itemNotExist = false;
			coaItemFind = retrieveCoaItem(coaItemIn);
			coaItemIn.getContract().getSupplier().getContact();
			if (coaItemFind == null){
				duplicated = false;
				existingContract = (Contract)contractManager.retrieveContractForCoaItemMaint(
						("".equals(coaItemIn.getContract().getContractNum())?null:coaItemIn.getContract().getContractNum()), 
						coaItemIn.getContract().getSupplier().getSupplierCode(), coaItemIn.getContract().getManufacturer().getCompanyCode(), 
						coaItemIn.getContract().getItemCode(), ModuleType.COA);			

				newSupplier = coaItemIn.getContract().getSupplier();
				newCompany = coaItemIn.getContract().getManufacturer();
				newPharmCompany = coaItemIn.getContract().getPharmCompany();

				if (existingContract ==null){					
					newContract = initNewContract(coaItemIn, newSupplier , newCompany, newPharmCompany);					
					coaItemIn.setContract(newContract);
					em.persist(newContract);  
				}else if (coaItemIn.getOrderType() != OrderType.DirectPurchase){											
					newContract = coaItemIn.getContract();
					existingContract.setSupplier(newSupplier);
					existingContract.setManufacturer(newCompany);
					existingContract.setPharmCompany(newPharmCompany);
					existingContract.setStartDate(newContract.getStartDate());
					existingContract.setEndDate(newContract.getEndDate());

					existingContract = em.merge(existingContract);
					coaItemIn.setContract(existingContract);					
				}
				CoaBatch newCoaBatch = new CoaBatch();
				newCoaBatch.setCoaItem(coaItemIn);
				newCoaBatch.setFollowUpDate(Calendar.getInstance().getTime());
				newCoaBatch.setInterfaceFlag(InterfaceFlag.No);

				if (coaItemIn.getCoaBatchList() ==null ) {
					coaItemIn.setCoaBatchList(new ArrayList<CoaBatch>() );
				}
				coaItemIn.getCoaBatchList().add(newCoaBatch);
				coaItemIn.setCoaBatch(newCoaBatch);
				newCoaBatch.setCoaItem(coaItemIn);								

				em.persist(coaItemIn);
				em.flush();
				coaItem = coaItemIn; // for call back event after add/ update coaItem
			}
		}
		
		if(coaItem != null){
			coaItem.getContract();
		}
		
		
		if (itemNotExist){
			logger.debug("item not exist #0",  coaItemIn.getContract().getItemCode() );
			coaItem = coaItemIn;
			return "0139";
		}else if (duplicated){
			logger.debug("duplicated #0 #1",  coaItemIn.getContract().getItemCode(),coaItemIn.getContract().getContractNum() );
			coaItem = coaItemFind;
			return "0143";
		}
		
		return "";
	}

	private Contract initNewContract(CoaItem coaItemIn, Supplier newSupplier, Company newCompany, Company newPharmCompany){// Manufacturer newManufacturer ){
		logger.debug("init New contract");
		Contract newContract = coaItemIn.getContract();
		newContract.setModuleType(ModuleType.COA);
		newContract.setSupplier(newSupplier);
		newContract.setManufacturer(newCompany);
		newContract.setPharmCompany(newPharmCompany);
		newContract.setContractNum(coaItemIn.getContract().getContractNum().trim());
		newContract.setContractType(coaItemIn.getOrderType().getDataValue());
		newContract.setItemCode(coaItemIn.getContract().getItemCode());
		newContract.setStartDate(coaItemIn.getContract().getStartDate());
		newContract.setEndDate(coaItemIn.getContract().getEndDate());
		newContract.setSuspendFlag(SuspendFlag.Unsuspended);
		newContract.setUploadDate(Calendar.getInstance().getTime());

		return newContract;
	}

	public void updateCoaItem()
	{
		logger.info("updateCoaItem");
		DmDrug dmDrugFind = dmDrugCacher.getDrugByItemCode(coaItem.getContract().getItemCode());  

		if (dmDrugFind != null)
		{
			em.merge(coaItem.getContract());
			coaItem = em.merge(coaItem);
			em.flush();
		}
	}

	public void deleteCoaItem(CoaItem coaItemIn)
	{
		logger.info("deleteCoaItem");

		CoaItem coaItemFind = em.find(CoaItem.class, coaItemIn.getCoaItemId());
		if (coaItemFind != null){
			Contract tmpContract = coaItemFind.getContract();			
			coaItemFind.setContract(null);			
			em.remove(tmpContract);

			if (coaItemFind.getCoaBatchList()!=null ){
				coaItemFind.getCoaBatchList().size();

				for (CoaBatch cb:coaItemFind.getCoaBatchList()){

					for (CoaDiscrepancy disc :cb.getCoaDiscrepancyList() ){

						for (CoaDiscrepancy childDisc :disc.getChildCoaDiscrepancyList()){
							childDisc.setParentCoaDiscrepancy(null);					
							em.merge(childDisc);
						}				
						disc.setCoaBatch(null);
						em.merge(disc);
					}

					for(CoaBatchVer cbv:cb.getCoaBatchVerList()) {
						cbv.setCoaBatch(null);
					}

					cb.getCoaItem().setCoaBatch(null);

					em.merge(cb);

					for (CoaFileFolder cff : cb.getCoaFileFolderList())
					{
						coaFileFolderManager.deletePhysicalFile(cff.getFileItem().getFilePath());
						em.remove(cff);
					}
				}
			}

			CoaItem ci = em.merge(coaItemFind);

			em.remove(ci);
			em.flush();
		}
	}

	public boolean isDuplicated() {
		return duplicated;
	}

	public boolean notExistItem() {
		return itemNotExist;
	}

	@Remove
	public void destroy(){
		coaItem = null;
	}


}
