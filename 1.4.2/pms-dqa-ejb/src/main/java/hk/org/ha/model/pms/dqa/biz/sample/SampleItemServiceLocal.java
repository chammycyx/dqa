package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Local;


@Local
public interface SampleItemServiceLocal {
			
	SampleItem retrieveSampleItemByItemCode(String itemCodeIn);
	
	void retrieveSampleItemByItemCodeRecordStatus(String itemCodeIn, RecordStatus recordStatus);
	
	void retrieveSampleItemForSampleTest(String itemCode);
	
	void retrieveAllSampleItemByItemCodeRecordStatus(String itemCode, RecordStatus recordStatus );
	
	void retrieveSampleItemForSchedule(String itemCode);
	
	SampleItem retrieveSampleItemForSampleTestValidation(String itemCodeIn, RecordStatus recordStatus);
	
	void addSampleItem();
	
	void createSampleItem(SampleItem sampleItemIn);
	
	void updateSampleItem(SampleItem sampleItemIn);
	
	void deleteSampleItem(SampleItem sampleItemIn);
	
	boolean isDuplicated();
	
	boolean isDrugItemNotExist();
	
    void destroy();
}
