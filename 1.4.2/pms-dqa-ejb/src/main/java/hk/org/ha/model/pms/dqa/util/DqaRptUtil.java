package hk.org.ha.model.pms.dqa.util;

import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DqaRptUtil {
	private static final String DATE_TIME_FORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";
	private static final String DATE_TIME_FORMAT_DD_MMM_YYYY_HHMMSS = "dd-MMM-yyyy HH:mm:ss";
	
	private DqaRptUtil(){};
	
	public static Boolean getBoolean(YesNoFlag flag)
	{
		if (flag == null){
			return Boolean.FALSE;}
		if (flag.getDataValue().equalsIgnoreCase("Y")){
			return Boolean.TRUE;}
		else{
			return Boolean.FALSE;}
	}
	
	public static String getDateStrDDMMMYYYYHHMMSS(Date dateIn)
	{
		return getDateStr(DATE_TIME_FORMAT_DD_MMM_YYYY_HHMMSS, dateIn);
	}
	
	public static String getDateStrDDMMMYYYY(Date dateIn)
	{
		return getDateStr(DATE_TIME_FORMAT_DD_MMM_YYYY, dateIn);
	}
	
	public static String getDateStr(String dateFormat, Date dateIn)
	{
		if (dateIn == null){
			return null;}
		
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
		
		return sdf.format(dateIn);
	}
	
	public static boolean strIsEmptyOrNull(String in)
	{
		if (in == null){
			return true;
		}
		
		if (in.trim().equals("")){
			return true;
		}
		
		return false;
	}
	
	public static String getString(String in, String defaultStr)
	{
		if (strIsEmptyOrNull(in)){
			return defaultStr;
		}
		
		return in;
	}
	     
}