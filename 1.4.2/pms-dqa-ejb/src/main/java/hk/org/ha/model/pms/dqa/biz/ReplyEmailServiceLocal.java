package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.fmk.pms.jms.MessageCreator;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.jms.JMSException;
import javax.xml.bind.JAXBException;

@Local
public interface ReplyEmailServiceLocal {

	@SuppressWarnings("unchecked")
	List<MessageCreator> replyMessage( final EaiMessage eaiMessage, final Map attHashMap ) throws JMSException, JAXBException;

	Boolean testReplyMessage(String fileName);
}
