package hk.org.ha.model.pms.dqa.biz.sample;

import javax.ejb.Local;

@Local
public interface FullInstitutionListServiceLocal {
	
	void retrieveFullInstitutionList();
	
	void destroy();
}
