package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxSummaryRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qaProblemListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class QaProblemListServiceBean implements QaProblemListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	@Out(required = false)
	private List<QaProblem> qaProblemList;
	
	private static final String REPORT_NAME = "Fax Detail Log Report";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	
		
	private String reportParameters = "";
	private String retrieveDate;
	private SimpleDateFormat sdf;
	
	private List<FaxSummaryRpt> faxSummaryRptList;
	
	public static final EventLogComparator EVENT_LOG_COMPARATOR = new EventLogComparator();
	
	
		
	public void retrieveQaProblemListByQaProblemCriteria(QaProblemCriteria qaProblemCriteriaIn, String screenNameIn){
		logger.debug("retrieveQaProblemListByQaProblemCriteria");
		
		StringBuffer qaProblemListSql = new StringBuffer();
		qaProblemListSql.append("select o from QaProblem o "); 
		qaProblemListSql.append("where (o.createDate >= :fromCreateDate or :fromCreateDate is null) ");
		qaProblemListSql.append("and (o.createDate < :toCreateDate or :toCreateDate is null) ");
		qaProblemListSql.append("and o.recordStatus = :recordStatus ");
		
		if(qaProblemCriteriaIn.getItemCode()!=null){
			qaProblemListSql.append("and (o.problemHeader.itemCode = :itemCode or :itemCode is null) ");}
		if(qaProblemCriteriaIn.getOrderType()!=null){
			qaProblemListSql.append("and (o.problemHeader.orderType = :orderType or :orderTypeDataValue is null) ");}
		if(qaProblemCriteriaIn.getSupplierCode()!=null){
			qaProblemListSql.append("and (o.supplier.supplierCode = :supplierCode or :supplierCode is null) ");}
		if(qaProblemCriteriaIn.getManufCode()!=null){
			qaProblemListSql.append("and (o.manufacturer.companyCode = :manufCode or :manufCode is null) ");}
		if(qaProblemCriteriaIn.getPharmCompanyCode()!=null){
			qaProblemListSql.append("and (o.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");}
		if(qaProblemCriteriaIn.getCaseNum()!=null){
			qaProblemListSql.append("and (o.caseNum = :caseNum or :caseNum is null) ");}
		if(qaProblemCriteriaIn.getCaseClassification()!=null){
			qaProblemListSql.append("and (o.classification.classificationCode = :caseClassification or :caseClassificationDataValue is null) ");}
		if(qaProblemCriteriaIn.getCaseStatus()!=null){
			qaProblemListSql.append("and (o.caseStatus = :caseStatus or :caseStatusDataValue is null) ");}
		
		if(screenNameIn.equals("qaProblemCaseEnquiryView")){
			qaProblemListSql.append("order by o.caseNum desc ");
		}
		else if(screenNameIn.equals("suppPerfFaxSummaryEnquiryView")){
			qaProblemListSql.append("order by o.caseNum asc ");
		}
		else if(screenNameIn.equals("suppPerfFileEnquiryView")){
			qaProblemListSql.append("order by o.createDate desc ");
		}
		
		
		Query q = em.createQuery(qaProblemListSql.toString())
		.setParameter("fromCreateDate", qaProblemCriteriaIn.getFromCreateDate(), TemporalType.DATE)
		.setParameter("toCreateDate", qaProblemCriteriaIn.getToCreateDate(), TemporalType.DATE)
		.setParameter("recordStatus", RecordStatus.Active);

		if(qaProblemCriteriaIn.getItemCode()!=null){
			q=q.setParameter("itemCode", qaProblemCriteriaIn.getItemCode());}			
		if(qaProblemCriteriaIn.getOrderType()!=null){
			q=q.setParameter("orderType", qaProblemCriteriaIn.getOrderType());
			q=q.setParameter("orderTypeDataValue", qaProblemCriteriaIn.getOrderType().getDataValue());}
		if(qaProblemCriteriaIn.getSupplierCode()!=null){
			q=q.setParameter("supplierCode", qaProblemCriteriaIn.getSupplierCode());}
		if(qaProblemCriteriaIn.getManufCode()!=null){
			q=q.setParameter("manufCode", qaProblemCriteriaIn.getManufCode());}
		if(qaProblemCriteriaIn.getPharmCompanyCode()!=null){
			q=q.setParameter("pharmCompanyCode", qaProblemCriteriaIn.getPharmCompanyCode());}
		if(qaProblemCriteriaIn.getCaseNum()!=null){
			q=q.setParameter("caseNum", qaProblemCriteriaIn.getCaseNum());}
		if(qaProblemCriteriaIn.getCaseClassification()!=null){
			q=q.setParameter("caseClassification", qaProblemCriteriaIn.getCaseClassification());
			q=q.setParameter("caseClassificationDataValue", qaProblemCriteriaIn.getCaseClassification().getDataValue());}
		if(qaProblemCriteriaIn.getCaseStatus()!=null){
			q=q.setParameter("caseStatus", qaProblemCriteriaIn.getCaseStatus());
			q=q.setParameter("caseStatusDataValue", qaProblemCriteriaIn.getCaseStatus().getDataValue());}
		
		q=q.setHint(QueryHints.FETCH, "o.problemHeader");
		q=q.setHint(QueryHints.FETCH, "o.supplier");
		q=q.setHint(QueryHints.FETCH, "o.manufacturer");
		q=q.setHint(QueryHints.FETCH, "o.pharmCompany");
		q=q.setHint(QueryHints.BATCH, "o.pharmProblemList");
		q=q.setHint(QueryHints.BATCH, "o.qaBatchNumList");
		q=q.setHint(QueryHints.BATCH, "o.qaProblemNature");
		q=q.setHint(QueryHints.BATCH, "o.classification");
		q=q.setHint(QueryHints.BATCH, "o.qaProblemFileList");
		q=q.setHint(QueryHints.BATCH, "o.eventLogList");
		q=q.setHint(QueryHints.BATCH, "o.faxSummary");
		
		
		qaProblemList = q.getResultList();
		
		if(qaProblemList!=null && qaProblemList.size()>0)
		{
			for(QaProblem qc:qaProblemList)
			{
				getLazyQaProblem(qc);
			}
		}
		else
		{
			qaProblemList=null;
		}
		
	}
	
	public void retrieveFaxSummaryRptByQaProblemList(List<QaProblem> qaProblemList)
	{
		logger.debug("retrieveFaxSummaryRptByQaProblemList");
		
		faxSummaryRptList = new ArrayList<FaxSummaryRpt>();
		for (QaProblem qaCom:qaProblemList)
		{
			FaxSummaryRpt rpt = new FaxSummaryRpt();
			
			rpt.setCaseStatus(qaCom.getCaseStatus().getDataValue());
			rpt.setCaseNum(qaCom.getCaseNum());
			rpt.setClassification(qaCom.getClassification()==null?"":(qaCom.getClassification().getClassificationCode()==null?"":(qaCom.getClassification().getClassificationCode().getDataValue())));
			rpt.setItemCode(qaCom.getProblemHeader().getItemCode());
			rpt.setItemDesc(qaCom.getProblemHeader().getDmDrug().getFullDrugDesc());
			rpt.setOrderType(qaCom.getProblemHeader().getOrderType().getDataValue());
			rpt.setSupplier(qaCom.getSupplier().getSupplierName());
			rpt.setManuf(qaCom.getManufacturer().getCompanyName());
			rpt.setPharmCom(qaCom.getPharmCompany().getCompanyName());
			rpt.setProblemDetail(qaCom.getQaProblemNature()==null?"":(qaCom.getQaProblemNature().getProblemDetail()));
			if(qaCom.getFaxSummary()!=null){
				rpt.setLastInitHospDate(qaCom.getFaxSummary().getInitHUpdateDate());
				rpt.setInitHospCount(qaCom.getFaxSummary().getInitHCount()==null?"0":(qaCom.getFaxSummary().getInitHCount().toString()));
				rpt.setLastInitSuppDate(qaCom.getFaxSummary().getInitSUpdateDate());
				rpt.setInitSuppCount(qaCom.getFaxSummary().getInitSCount()==null?"0":(qaCom.getFaxSummary().getInitSCount().toString()));
				rpt.setLastInterimHospDate(qaCom.getFaxSummary().getInterimHUpdateDate());
				rpt.setInterimHospCount(qaCom.getFaxSummary().getInterimHCount()==null?"0":(qaCom.getFaxSummary().getInterimHCount().toString()));
				rpt.setLastInterimSuppDate(qaCom.getFaxSummary().getInterimSUpdateDate());
				rpt.setInterimSuppCount(qaCom.getFaxSummary().getInterimSCount()==null?"0":(qaCom.getFaxSummary().getInterimSCount().toString()));
				rpt.setLastFinalHospDate(qaCom.getFaxSummary().getFinalHUpdateDate());
				rpt.setFinalHospCount(qaCom.getFaxSummary().getFinalHCount()==null?"0":(qaCom.getFaxSummary().getFinalHCount().toString()));
				rpt.setLastFinalSuppDate(qaCom.getFaxSummary().getFinalSUpdateDate());
				rpt.setFinalSuppCount(qaCom.getFaxSummary().getFinalSCount()==null?"0":(qaCom.getFaxSummary().getFinalSCount().toString()));
			}
			else
			{
				rpt.setInitHospCount("0");
				rpt.setInitSuppCount("0");
				rpt.setInterimHospCount("0");
				rpt.setInterimSuppCount("0");
				rpt.setFinalHospCount("0");
				rpt.setFinalSuppCount("0");
			}
			rpt.setCaseCreateDate(qaCom.getCreateDate());
			
			faxSummaryRptList.add(rpt);
		}
	}
	
	public List<FaxSummaryRpt> getFaxSummaryRptList()
	{
		return faxSummaryRptList;
	}
	
	public String getReportName() 
	{
		return REPORT_NAME;
	}

	public String getReportParameters() 
	{
		return reportParameters;
	}

	public String getRetrieveDate() 
	{
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		Collections.sort(qaProblemLazy.getEventLogList(), EVENT_LOG_COMPARATOR);
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		
				
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
		}
		
		return qaProblemLazy;
	}	

	private static class EventLogComparator implements Comparator<EventLog>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private Date getEventLogDate(EventLog el) {
        	return el.getEventLogDate();
        }
        
        public int compare(EventLog el1, EventLog el2) {

            Date date1 = getEventLogDate(el1);
            Date date2 = getEventLogDate(el2);

            if (!date1.equals(date2)) {
            	return date1.compareTo(date2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}
		
	@Remove
	public void destroy(){
		if (qaProblemList != null){
			qaProblemList = null;
		}
	}
}
