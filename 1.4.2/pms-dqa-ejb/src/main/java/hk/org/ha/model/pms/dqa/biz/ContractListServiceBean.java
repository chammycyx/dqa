package hk.org.ha.model.pms.dqa.biz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemListServiceBean.ProblemNumComparator;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("contractListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ContractListServiceBean implements ContractListServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<Contract> contractList;

	@SuppressWarnings("unchecked")
	public void retrieveContractListForCoaLiveContractList(String contractNum, String itemCode) {
		logger.info("retrieveContractList");
		
		contractList = em.createNamedQuery("Contract.findForCoaLiveContractList")
		.setHint(QueryHints.FETCH, "o.supplier")
		.setParameter("contractNum", contractNum.trim() + "%")
		.setParameter("itemCode", itemCode.trim())
		.setParameter("moduleList", new ArrayList<ModuleType>(Arrays.asList(ModuleType.All, ModuleType.COA)))
		.getResultList();

		for (Contract c:contractList){
			c.getSupplier();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveContractListForSampleTest(String contractNum, String contractSuffix, OrderType orderType, String itemCode){
		logger.debug("retrieveContractListForSampleTestSchedule");
		
		contractList = em.createNamedQuery("Contract.findContractListForSampleTest")
		.setHint(QueryHints.FETCH, "o.supplier")
		.setHint(QueryHints.FETCH, "o.manufacturer")
		.setHint(QueryHints.FETCH, "o.pharmCompany")
		.setParameter("contractNum", contractNum + "%")
		.setParameter("contractSuffixStr", contractSuffix+ "%")
		.setParameter("contractSuffix", (contractSuffix.equals("")?null:contractSuffix.trim()))
		.setParameter("contractType", orderType.getDataValue())
		.setParameter("itemCode", itemCode.trim())
		.setParameter("moduleType", ModuleType.All)
		.setParameter("currentDate", new Date(), TemporalType.DATE)
		.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveContractListLikeContractByItemCode(String contractNum, String contractSuffix, String itemCode){
		logger.debug("retrieveContractListLikeContractByItemCode");
		
		contractList = em.createNamedQuery("Contract.findContractListLikeContractByItemCode")
		.setHint(QueryHints.FETCH, "o.supplier")
		.setHint(QueryHints.FETCH, "o.manufacturer")
		.setHint(QueryHints.FETCH, "o.pharmCompany")
		.setParameter("contractNum", contractNum + "%")
		.setParameter("contractSuffixStr", contractSuffix+ "%")
		.setParameter("contractSuffix", (("").equals(contractSuffix)?null:contractSuffix.trim()))
		.setParameter("itemCode", itemCode.trim())
		.setParameter("moduleType", ModuleType.All)
		.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveContractListLikeContractByItemCodeOrderType(String contractNum, String contractSuffix, String itemCode, OrderTypeAll orderType){
		logger.debug("retrieveContractListLikeContractByItemCodeOrderType");
		
		List<Contract> resultList = em.createNamedQuery("Contract.findContractListLikeContractByItemCodeOrderType")
		.setHint(QueryHints.FETCH, "o.supplier")
		.setHint(QueryHints.FETCH, "o.manufacturer")
		.setHint(QueryHints.FETCH, "o.pharmCompany")
		.setParameter("contractNum", contractNum + "%")
		.setParameter("contractSuffixStr", contractSuffix+ "%")
		.setParameter("contractSuffix", (("").equals(contractSuffix)?null:contractSuffix.trim()))
		.setParameter("itemCode", itemCode.trim())
		.setParameter("contractType", orderType.getDataValue())
		.setParameter("moduleType", ModuleType.All)
		.getResultList();
		
		Collections.sort(resultList, new EndDateComparator());
		contractList = resultList;
	}
	
	
	public static class EndDateComparator implements Comparator<Contract>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		public int compare(Contract c1, Contract c2){
			return c2.getEndDate()
			.compareTo( 
					c1.getEndDate()
			);
		}						
	}
	
	@Remove
	public void destroy(){
		if (contractList != null){
			contractList = null;
		}
	}

}
