package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;

import javax.ejb.Local;

@Local
public interface ProblemNatureSubCatListServiceLocal {
	
	void retrieveProblemNatureSubCatList(ProblemNatureCat problemNatureCatIn);
	
	void retrieveProblemNatureSubCatListFull();
	
	void destroy();
	
}
