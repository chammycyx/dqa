package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.vo.sample.SampleMovementCriteria;


import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleMovementListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleMovementListServiceBean implements SampleMovementListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<SampleMovement> sampleMovementList;
	
	
			
	@SuppressWarnings("unchecked")
	public void retrieveSampleMovementListBySampleMovementCriteria(SampleMovementCriteria sampleMovementCriteria){
		List<String> movLocationList = new ArrayList<String>();
		logger.debug("retrieveSampleMovementListBySampleMovementCriteria" );
		
		StringBuffer enquirySampleMovementSql = constructStringBufferForRetrieveSampleMovementList(sampleMovementCriteria);
		
		Query q = em.createQuery(enquirySampleMovementSql.toString())
								.setParameter("manufCode", sampleMovementCriteria.getManufCode())
								.setParameter("fromInventoryDate", sampleMovementCriteria.getFromInventoryDate(), TemporalType.DATE)
								.setParameter("toInventoryDate", sampleMovementCriteria.getToInventoryDate(), TemporalType.DATE);
		
		if(sampleMovementCriteria.getItemCode()!=null){
			q=q.setParameter("itemCode", sampleMovementCriteria.getItemCode());}
		if(sampleMovementCriteria.getOrderType()!=null)
		{
			q=q.setParameter("orderType", sampleMovementCriteria.getOrderType()==null?null:sampleMovementCriteria.getOrderType())
			   .setParameter("orderTypeValue", sampleMovementCriteria.getOrderType()==null?null:sampleMovementCriteria.getOrderType().getDataValue());
		}
		if(sampleMovementCriteria.getSupplierCode()!=null){
			q=q.setParameter("supplierCode", sampleMovementCriteria.getSupplierCode());}
		if(sampleMovementCriteria.getPharmCompanyCode()!=null){
			q=q.setParameter("pharmCompanyCode", sampleMovementCriteria.getPharmCompanyCode());}
		if(sampleMovementCriteria.getTestCode()!=null){
			q=q.setParameter("testCode", sampleMovementCriteria.getTestCode());}
		if(sampleMovementCriteria.getBatchNum()!=null){
			q=q.setParameter("batchNum", sampleMovementCriteria.getBatchNum());}
		if(sampleMovementCriteria.getExpiryDate()!=null){
			q=q.setParameter("expiryDate", sampleMovementCriteria.getExpiryDate());}
		if(sampleMovementCriteria.getScheduleNum()!=null){
			q=q.setParameter("scheduleNum", sampleMovementCriteria.getScheduleNum());}
		

		if(sampleMovementCriteria.getInstitutionCode()!=null){
			movLocationList.add(sampleMovementCriteria.getInstitutionCode());}
		if(sampleMovementCriteria.getLabCode()!=null){
			movLocationList.add(sampleMovementCriteria.getLabCode());}
		
		if(movLocationList!=null && movLocationList.size()>0){
			q=q.setParameter("movLocationList", movLocationList);}
		
		if(sampleMovementCriteria.getInventorySelectionType().equals("I")){
			List<Institution> institutionListFind = em.createNamedQuery("Institution.findByPcuFlagRecordStatus")
			.setParameter("pcuFlag", YesNoFlag.Yes)
			.setParameter("recordStatus", (RecordStatus.Active).getDataValue())
			.getResultList();
			
			List<String> institutionListString = new ArrayList();
			for(Institution i:institutionListFind)
			{
				institutionListString.add(i.getInstitutionCode());
			}
			
			q=q.setParameter("institutionList", institutionListString);
		}
		else if(sampleMovementCriteria.getInventorySelectionType().equals("L")){
			List<Lab> labListFind = em.createNamedQuery("Lab.findAll").getResultList();
			
			List<String> labListString = new ArrayList();
			for(Lab l:labListFind)
			{
				labListString.add(l.getLabCode());
			}
			
			q=q.setParameter("labList", labListString);
		}
		
								
		q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel");
		
		if(sampleMovementCriteria.getInstitutionCode()!=null){
			q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.institution");}
		if(sampleMovementCriteria.getLabCode()!=null){
			q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.lab");}
											   
		sampleMovementList = q.getResultList();
		
				
		if(sampleMovementList == null || sampleMovementList.size()==0){
			sampleMovementList = null;}
		else{
			for(SampleMovement sml:sampleMovementList)
			{
				getLazySampleTestSchedule(sml.getSampleTestSchedule());
			}
		}
	}
	
	
	private StringBuffer constructStringBufferForRetrieveSampleMovementList(SampleMovementCriteria sampleMovementCriteria){
		List<String> movLocationList = new ArrayList<String>();
		StringBuffer enquirySampleMovementSql = new StringBuffer();
		enquirySampleMovementSql.append("select o from SampleMovement o "); 
		enquirySampleMovementSql.append("where (o.manufCode = :manufCode or :manufCode is null) ");
		enquirySampleMovementSql.append("and (o.createDate >= :fromInventoryDate or :fromInventoryDate is null) ");
		enquirySampleMovementSql.append("and (o.createDate < :toInventoryDate or :toInventoryDate is null) ");
		
		if(sampleMovementCriteria.getItemCode()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.sampleItem.itemCode = :itemCode or :itemCode is null) ");}
		if(sampleMovementCriteria.getOrderType()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.orderType = :orderType or :orderTypeValue is null) ");}
		if(sampleMovementCriteria.getSupplierCode()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) ");}
		if(sampleMovementCriteria.getPharmCompanyCode()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) ");}
		if(sampleMovementCriteria.getTestCode()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.sampleTest.testCode = :testCode or :testCode is null) ");}
		if(sampleMovementCriteria.getBatchNum()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.batchNum = :batchNum or :batchNum is null) ");}
		if(sampleMovementCriteria.getExpiryDate()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.expiryDate = :expiryDate or :expiryDate is null) ");}
		if(sampleMovementCriteria.getScheduleNum()!=null){
			enquirySampleMovementSql.append("and (o.sampleTestSchedule.scheduleNum = :scheduleNum or :scheduleNum is null) ");}
		
		if(sampleMovementCriteria.getInstitutionCode()!=null){
			movLocationList.add(sampleMovementCriteria.getInstitutionCode());}
		if(sampleMovementCriteria.getLabCode()!=null){
			movLocationList.add(sampleMovementCriteria.getLabCode());}
		
		if(movLocationList!=null && movLocationList.size()>0){
			enquirySampleMovementSql.append("and o.locationCode in :movLocationList ");}
		
		if(sampleMovementCriteria.getInventorySelectionType().equals("I")){
			enquirySampleMovementSql.append("and o.locationCode in :institutionList ");
		}
		else if(sampleMovementCriteria.getInventorySelectionType().equals("L")){
			enquirySampleMovementSql.append("and o.locationCode in :labList ");
		}
		
		
		enquirySampleMovementSql.append("order by o.createDate ");
		
		return enquirySampleMovementSql;
	}
	
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	@Remove
	public void destroy() {
		if (sampleMovementList !=null){
			sampleMovementList = null;
		}
	}
}
