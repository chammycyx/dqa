package hk.org.ha.model.pms.dqa.vo.suppperf;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class ConflictOfInterestRpt {

	private String reportName;
	private String reportContent;
	private String conflictFlag;
	private String conflictMspName;
	private String conflictRelationship;
	private String conflictByName;
	private String conflictByRank;
	private String conflictDate;
	private String docNum;
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getReportContent() {
		return reportContent;
	}
	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}
	public void setConflictFlag(String conflictFlag) {
		this.conflictFlag = conflictFlag;
	}
	public String getConflictFlag() {
		return conflictFlag;
	}
	public String getConflictMspName() {
		return conflictMspName;
	}
	public void setConflictMspName(String conflictMspName) {
		this.conflictMspName = conflictMspName;
	}
	public String getConflictRelationship() {
		return conflictRelationship;
	}
	public void setConflictRelationship(String conflictRelationship) {
		this.conflictRelationship = conflictRelationship;
	}
	public String getConflictByName() {
		return conflictByName;
	}
	public void setConflictByName(String conflictByName) {
		this.conflictByName = conflictByName;
	}
	public String getConflictByRank() {
		return conflictByRank;
	}
	public void setConflictByRank(String conflictByRank) {
		this.conflictByRank = conflictByRank;
	}
	public String getConflictDate() {
		return conflictDate;
	}
	public void setConflictDate(String conflictDate) {
		this.conflictDate = conflictDate;
	}
	public String getDocNum() {
		return docNum;
	}
	public void setDocNum(String docNum) {
		this.docNum = docNum;
	}
	
}
