package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.exception.ConcurrentUpdateException;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.dqa.udt.coa.PassFlag;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyCriteria;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaDiscrepancyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaDiscrepancyListServiceBean implements CoaDiscrepancyListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<CoaDiscrepancy> coaDiscrepancyList;

	@In(create=true)
	private CoaBatchServiceLocal coaBatchService;
	
	public void retrieveCoaDiscrepancyListForReportDiscrepancy(CoaDiscrepancyCriteria coaDiscrepancyCriteria) {
		logger.debug("retrieveCoaDiscrepancyListForReportDiscrepancy "+coaDiscrepancyCriteria);
		coaDiscrepancyList = em.createNamedQuery("CoaDiscrepancy.findForReportDiscrepancy")
		.setParameter("passFlag", PassFlag.Passed)
		.setParameter("coaStatus", coaDiscrepancyCriteria.getCoaStatus())
		.setParameter("itemCode", coaDiscrepancyCriteria.getItemCode())
		.setParameter("orderType", coaDiscrepancyCriteria.getOrderType())
		.setParameter("contractNum", coaDiscrepancyCriteria.getContractNum())
		.setParameter("supplierCode", coaDiscrepancyCriteria.getSupplierCode())	
		.setParameter("orderTypeD", OrderType.DirectPurchase)
		.getResultList();
	}		

	public void retrieveCoaDiscrepancyListByCoaBatch(CoaBatch coaBatch) {
		logger.debug("retrieveCoaDiscrepancyListByCoaBatch #0", coaBatch);
		coaDiscrepancyList = em.createNamedQuery("CoaDiscrepancy.findByCoaBatchPassFlag")
							   .setHint(QueryHints.FETCH, "o.coaDiscrepancyKey")
							   .setHint(QueryHints.LEFT_FETCH, "o.parentCoaDiscrepancy")
							   .setHint(QueryHints.LEFT_FETCH, "o.childCoaDiscrepancyList")
							   .setParameter("coaBatch", coaBatch)
							   .setParameter("passFlag", PassFlag.Passed)
							   .getResultList();
		for( CoaDiscrepancy coaDiscrepancy:coaDiscrepancyList ) {
			coaDiscrepancy.getChildCoaDiscrepancyList().size();
		}
	}	

	public void retrieveCoaDiscrepancyListByCoaBatchId(Long coaBatchId) {
		logger.debug("retrieveCoaDiscrepancyListByCoaBatchId #0", coaBatchId);
		coaDiscrepancyList = em.createNamedQuery("CoaDiscrepancy.findByCoaBatchId")
							   .setHint(QueryHints.FETCH, "o.coaDiscrepancyKey")
							   .setHint(QueryHints.LEFT_FETCH, "o.parentCoaDiscrepancy")
							   .setHint(QueryHints.LEFT_FETCH, "o.childCoaDiscrepancyList")
		                       .setParameter("coaBatchId", coaBatchId)
		                       .getResultList();
	}
	
	private CoaDiscrepancy getLockedCoaBatch(CoaDiscrepancy discrepancy) {
		return (CoaDiscrepancy) em.createQuery("select o from CoaDiscrepancy o where o.coaDiscrepancyId = :discrepancyId")
		.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
		.setParameter("discrepancyId", discrepancy.getCoaDiscrepancyId())
		.getSingleResult();	
	}
	
	private CoaBatch getLockedCoaBatch(CoaBatch coaBatchIn) {
		return (CoaBatch) em.createNamedQuery("CoaBatch.findByCoaBatchId")
		.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
		.setParameter("coaBatchId", coaBatchIn.getCoaBatchId())
		.getSingleResult();	
	}
	
	public void updateCoaDiscrepancyList(Collection<CoaDiscrepancy> coaDiscrepancyList) {
		logger.debug("updateCoaDiscrepancyList #0", coaDiscrepancyList);
		
		CoaBatch parentCoaBatch = null;
		CoaBatch childCoaBatch = null;
		CoaDiscrepancy discrepancyInDB;
		HashSet<CoaBatch> coaBatchHS = new HashSet<CoaBatch>();
		
		//Update Parent & Child CoaDiscrepancies		
		for(CoaDiscrepancy coaDiscrepancy:coaDiscrepancyList) {
			
			if( coaDiscrepancy.isEnable() ) {
				
				discrepancyInDB = getLockedCoaBatch(coaDiscrepancy);
				if (!discrepancyInDB.getVersion().equals(coaDiscrepancy.getVersion())) {
					throw new ConcurrentUpdateException("Can't update CoaDiscrepancy id="+discrepancyInDB.getCoaDiscrepancyId());					
				}
				
				for( CoaDiscrepancy childCoaDiscrepancy:coaDiscrepancy.getChildCoaDiscrepancyList() ) {
					
					if( childCoaDiscrepancy.getPassFlag() == PassFlag.Passed ) { continue; }
					
					if( childCoaDiscrepancy.isEnable() ) {// pass child COA Discrepancy
						childCoaDiscrepancy.setPassFlag( PassFlag.Passed );									
					} else {
						childCoaDiscrepancy.setParentCoaDiscrepancy(null);
					}
					em.merge( childCoaDiscrepancy );
					
					coaBatchHS.add( childCoaDiscrepancy.getCoaBatch() );
				}			
			
				coaDiscrepancy.setPassFlag( PassFlag.Passed );				
				em.merge( coaDiscrepancy );	
				parentCoaBatch = coaDiscrepancy.getCoaBatch();
			}
		}
		em.flush();				

		CoaBatch coaBatchInDB = getLockedCoaBatch(parentCoaBatch);
		if (!coaBatchInDB.getVersion().equals(parentCoaBatch.getVersion())) {			
			throw new ConcurrentUpdateException("Can't update coa batch status");
		}
		
		coaBatchInDB.getCoaDiscrepancyList().size();
		
		if( checkAllDiscrepancyHasPassed( coaBatchInDB ) ) {
			coaBatchService.updateCoaBatchVerStatus( coaBatchInDB, 
					CoaStatus.Pass, 
					coaBatchInDB.getCoaBatchVer().getDiscrepancyStatus() );
		}
		
		//Update Child CoaBatch
		Iterator<CoaBatch> it = coaBatchHS.iterator();		
		while( it.hasNext() ) {
			childCoaBatch = it.next();
			
			if( childCoaBatch.getCoaBatchVer().getCoaStatus() == CoaStatus.Pass ) { continue; }
			
			childCoaBatch = em.find(CoaBatch.class, childCoaBatch.getCoaBatchId());
			childCoaBatch.getCoaDiscrepancyList().size();									
			
			if ( checkAllDiscrepancyHasPassed( childCoaBatch ) ) {
				coaBatchService.updateCoaBatchVerStatus( childCoaBatch,
						CoaStatus.Pass, 
						childCoaBatch.getCoaBatchVer().getDiscrepancyStatus() );
			} else if ( childCoaBatch.getCoaBatchVer().getDiscrepancyStatus() == DiscrepancyStatus.DiscrepancyUnderGrouping 
					&& !checkAllDiscrepancyHasGroup(childCoaBatch) ){
				coaBatchService.updateCoaBatchVerStatus( childCoaBatch,
					CoaStatus.DiscrepancyClarificationInProgress, 
					DiscrepancyStatus.PharmacistFollowUp );				
			}
			
			forceUpdateCoaBatch( childCoaBatch );
		}		
				
		forceUpdateCoaBatch( coaBatchInDB );

		em.flush();
	}		

	private void forceUpdateCoaBatch(CoaBatch coaBatchIn){
		//trigger update version count of CoaBatch even the entity has no change
		CoaBatch cb = em.find(CoaBatch.class, coaBatchIn.getCoaBatchId());	
		Calendar cal = Calendar.getInstance();
		cb.setDiscrepancyActionDate(cal.getTime());
		em.merge(cb);	
	}
	
	private boolean checkAllDiscrepancyHasPassed(CoaBatch coaBatchIn) {
		
		for(CoaDiscrepancy coaDiscrepancy:coaBatchIn.getCoaDiscrepancyList()) {
			if(coaDiscrepancy.getPassFlag() == PassFlag.NotYetPassed) {
				return false;
			}
		}
		return true;
			
	}

	public boolean checkAllDiscrepancyHasGroup(CoaBatch coaBatchIn) {

		for( CoaDiscrepancy coaDiscrepancy:coaBatchIn.getCoaDiscrepancyList() ) {
			if ((coaDiscrepancy.getPassFlag() == PassFlag.NotYetPassed) && (coaDiscrepancy.getParentCoaDiscrepancy() == null)) {
				return false;
			}
		}				

		return true;
	}
	
	public void createCoaDiscrepancyList(CoaBatch coaBatchIn, Collection<CoaDiscrepancyKey> coaDiscrepancyKeyList) {

		int numOfParent = 0;
		int numOfInsert = 0;

		CoaBatch mergedCoaBatch = em.merge(coaBatchIn);

		for(CoaDiscrepancyKey coaDiscrepancyKey:coaDiscrepancyKeyList){
			if(coaDiscrepancyKey.isEnable()){				
				logger.debug("createCoaDiscrepancy");					

				++numOfInsert;
				CoaDiscrepancy coaDiscrepancy = new CoaDiscrepancy();
				if(coaDiscrepancyKey.getParentCoaDiscrepancy() != null){
					coaDiscrepancyKey.getParentCoaDiscrepancy().addChildCoaDiscrepancy(coaDiscrepancy);
					++numOfParent;
				}
				coaDiscrepancy.setCoaBatch(mergedCoaBatch);
				coaDiscrepancy.setCoaDiscrepancyKey(coaDiscrepancyKey);				
				coaDiscrepancy.setPassFlag(PassFlag.NotYetPassed);
				em.merge(coaDiscrepancy);
			}
		}

		if(numOfParent < numOfInsert) {
			coaBatchService.updateCoaBatchVerStatus(mergedCoaBatch, CoaStatus.DiscrepancyClarificationInProgress, DiscrepancyStatus.PharmacistFollowUp);
		} else {			
			coaBatchService.updateCoaBatchVerStatus(mergedCoaBatch, CoaStatus.DiscrepancyClarificationInProgress, DiscrepancyStatus.DiscrepancyUnderGrouping);
		}						
		em.flush();	
	}

	@Remove
	public void destroy(){
		if(coaDiscrepancyList != null) {
			coaDiscrepancyList = null;  
		}
	}

}
