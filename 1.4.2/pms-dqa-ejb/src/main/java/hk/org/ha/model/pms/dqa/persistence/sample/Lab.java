package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "LAB")
@NamedQueries({
	@NamedQuery(name = "Lab.findAll", query = "select o from Lab o"),
	@NamedQuery(name = "Lab.findByLabCode", query = "select o from Lab o where o.labCode = :labCode")
})
@Customizer(AuditCustomizer.class)
public class Lab extends VersionEntity{

	private static final long serialVersionUID = -8655905658326830669L;

	@Id
	@Column(name="LAB_CODE")
	private String labCode;

	@Column(name="LAB_NAME")
	private String labName;

	@OneToOne
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;
	
	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public String getLabName() {
		return labName;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}
	
}
