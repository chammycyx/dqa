package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ConfirmCheckListFlag implements StringValuedEnum {
	
	NullValue("-", "-"),
	YES("Y", "Yes"),	
	NO("N", "No");		
	
    private final String dataValue;
    private final String displayValue;
        
    ConfirmCheckListFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<ConfirmCheckListFlag> {

		private static final long serialVersionUID = 6368919235452551382L;

		@Override
    	public Class<ConfirmCheckListFlag> getEnumClass() {
    		return ConfirmCheckListFlag.class;
    	}
    }
}
