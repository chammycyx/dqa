package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.udt.FileExtension;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("documentService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DocumentServiceBean implements DocumentServiceLocal, Serializable {

	private static final long serialVersionUID = -6198401895503834085L;

	@Logger
	private Log logger;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;

	private FileItem fileItem;
	
	@Out
	private String leftWindowsProperties;
	
	@Out
	private String rightWindowsProperties;
	
	@Out
	private String normalWindowsProperties;
	
	private String uploadPath;
	
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
	
	public void setLeftWindowsProperties(String leftWindowsProperties) {
		this.leftWindowsProperties = leftWindowsProperties;
	}

	public void setRightWindowsProperties(String rightWindowsProperties) {
		this.rightWindowsProperties = rightWindowsProperties;
	}

	public void setNormalWindowsProperties(String normalWindowsProperties) {
		this.normalWindowsProperties = normalWindowsProperties;
	}
	
	public void setFileItem(FileItem fileItem){
		this.fileItem = fileItem;
	}	
	
	public void openPdfDocument() throws IOException {
		uploadPath = ( uploadPath.charAt(uploadPath.length()-1) != '/' )?uploadPath+"/":uploadPath;
		String filePath = (fileItem.getFilePath().charAt(0) == '/')?fileItem.getFilePath().substring(1):fileItem.getFilePath();
		File file = new File(uploadPath + filePath);
		logger.debug("Open pdf file #0", file);		
		String contentId = reportProvider.storeReport(file, new DocumentData.DocumentType("pdf", "application/pdf"));
		reportProvider.redirectReport(contentId);												
	}
	
	public void openFileItemDocument() throws IOException {
		uploadPath = ( uploadPath.charAt(uploadPath.length()-1) != '/' )?uploadPath+"/":uploadPath;
		String filePath = (fileItem.getFilePath().charAt(0) == '/')?fileItem.getFilePath().substring(1):fileItem.getFilePath();
		File file = new File(uploadPath + filePath);
		
		String fileExt = StringUtils.trim(file.getName().substring(file.getName().lastIndexOf(".")+1, file.getName().length())).toLowerCase(); 
		FileExtension fileExtension = FileExtension.dataValueOf("."+fileExt);
		String mimeType = fileExtension==null?"application/"+fileExt:fileExtension.getDisplayValue();
		logger.debug("fileExt:#0, mimeType:#1", fileExt, mimeType);
		reportProvider.redirectReport(
				reportProvider.storeReport(
						file, 
						new DocumentData.DocumentType(
								fileExt, 
								mimeType
						)
				)
		);
	}
	
	@Remove
	public void destroy(){
		if ( leftWindowsProperties != null ) {
			leftWindowsProperties = null;			
		}
		if ( rightWindowsProperties != null ) {
			rightWindowsProperties = null;			
		}
		if ( normalWindowsProperties != null ) {
			normalWindowsProperties = null;			
		}
	}

}
