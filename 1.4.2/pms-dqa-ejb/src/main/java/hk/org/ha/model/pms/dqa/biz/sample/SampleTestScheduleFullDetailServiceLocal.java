package hk.org.ha.model.pms.dqa.biz.sample;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleFullDetailServiceLocal {

	void retrieveSampleTestScheduleFullDetailByScheduleId(Long scheduleId);
	
	void destory();
}
