package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.model.pms.dqa.vo.sample.DdrRptCriteria;
import javax.ejb.Local;

@Local
public interface DdrRptServiceLocal {

	void retrieveDdrList(DdrRptCriteria ddrRptCriteria);
	
	void generateDdrRpt();
	
	void destroy();
}
