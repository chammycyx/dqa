package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MicroBioNationalPharmStd implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	HarmonizedMethodUSPandEuP("MNP1", "Harmonized Method-USP (61) & (62) and EuP (2.6.12 & 2.6.13)"),
	USPorEuP("MNP2", "USP (71) or EuP (2.6.1)"),
	Other("O", "Other");
	
    private final String dataValue;
    private final String displayValue;
        
    MicroBioNationalPharmStd(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<MicroBioNationalPharmStd> {

		private static final long serialVersionUID = -1623620317837922548L;

		@Override
    	public Class<MicroBioNationalPharmStd> getEnumClass() {
    		return MicroBioNationalPharmStd.class;
    	}
    }
}
