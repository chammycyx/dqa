package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleFileListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class SampleTestScheduleFileListServiceBean implements SampleTestScheduleFileListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestScheduleFile> sampleTestScheduleFileList;
	
	public List<SampleTestScheduleFile> retrieveSampleTestScheduleFileListBySmpleTestFileId(Long sampleTestFileId){
		logger.debug("retrieveSampleTestScheduleFileList");
		
		List<SampleTestScheduleFile> list = em.createNamedQuery("SampleTestScheduleFile.findBySampleTestFileId")
			.setParameter("sampleTestFileId", sampleTestFileId)
			.setHint(QueryHints.FETCH, "o.sampleTestFile")
			.getResultList();
		
		if(list == null || list.size()==0)
		{
			list = null;
		}
		else
		{
			for(SampleTestScheduleFile sampleTestScheduleFileLoop:list)
			{
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getSupplier();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getSupplier().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getPharmCompany();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getPharmCompany().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getManufacturer();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getManufacturer().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getInstitution();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getTestFrequency();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getPayment();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getLab();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getMicroBioTest();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getChemicalAnalysis();
				sampleTestScheduleFileLoop.getSampleTestFile().getSupplier();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getSupplier()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getSupplier().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().postLoad();
			}
		}
		
		return list;
		
	}
	
	public void retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat(RecordStatus recordStatus, ScheduleStatus scheduleStatus, SampleTestFileCat fileCat){
		logger.debug("retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat");
		
		sampleTestScheduleFileList = em.createNamedQuery("SampleTestScheduleFile.findByRecordStatusScheduleStatusFileCat")
									.setParameter("recordStatus", recordStatus)
									.setParameter("scheduleStatus", scheduleStatus)
									.setParameter("fileCat", fileCat)
									.setHint(QueryHints.FETCH, "o.sampleTestFile")
									.setHint(QueryHints.LEFT_FETCH, "o.sampleTestFile.fileItem")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.testFrequency")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.lab")
									.setHint(QueryHints.FETCH, "o.sampleTestSchedule.institution")
									.getResultList();
		
		if(sampleTestScheduleFileList == null || sampleTestScheduleFileList.size()==0)
		{
			sampleTestScheduleFileList = null;
		}
		else
		{
			for(SampleTestScheduleFile sampleTestScheduleFileLoop:sampleTestScheduleFileList)
			{
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getSupplier();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getSupplier().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getPharmCompany();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getPharmCompany().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getManufacturer();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getContract().getManufacturer().getContact();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getInstitution();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getTestFrequency();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getPayment();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getLab();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getMicroBioTest();
				sampleTestScheduleFileLoop.getSampleTestSchedule().getChemicalAnalysis();
				sampleTestScheduleFileLoop.getSampleTestFile().getSupplier();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getSupplier()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getSupplier().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getPharmCompany().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer();
				if(sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer()!=null)
				{
					sampleTestScheduleFileLoop.getSampleTestFile().getManufacturer().getContact();
				}
				sampleTestScheduleFileLoop.getSampleTestFile().postLoad();
			}
		}
	}
	
	@Remove
	public void destroy() {
	
	}
	
}
