package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.udt.suppperf.FaxType;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_DETAIL")
@NamedQueries({
	@NamedQuery(name = "FaxDetail.findByUserCode", query = "select o from FaxDetail o where o.faxSender.userCode = :userCode ")
})
@Customizer(AuditCustomizer.class)
public class FaxDetail extends VersionEntity {

	private static final long serialVersionUID = -1683863765179459048L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxDetailSeq")
	@SequenceGenerator(name = "faxDetailSeq", sequenceName = "SEQ_FAX_DETAIL", initialValue=10000)
	@Id
	@Column(name="FAX_DETAIL_ID", nullable=false)
	private Long faxDetailId;
	
	@Converter(name = "FaxDetail.faxType", converterClass = FaxType.Converter.class )
	@Convert("FaxDetail.faxType")
	@Column(name="FAX_TYPE", length=1)
	private FaxType faxType;
		
	@Column(name="FAX_TO", length=500)
	private String faxTo;
	
	@Column(name="FAX_CC_ENCL", length=500)
	private String faxCcEncl;
	
	@Column(name="FAX_ATTN", length=100)
	private String faxAttn;
	
	@Column(name="FAX_NUM", length=20)
	private String faxNum;
	
	@Column(name="REF_NUM", length=15)
	private String refNum;
	
	@Column(name="SUPP_MANUF_FLAG", length=1)
	private String suppManufFlag;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_INIT_HOSP_ID")
	private FaxInitHosp faxInitHosp;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_INIT_SUPP_ID")
	private FaxInitSupp faxInitSupp;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_INTERIM_HOSP_ID")
	private FaxInterimHosp faxInterimHosp;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_INTERIM_SUPP_ID")
	private FaxInterimSupp faxInterimSupp;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FAX_FINAL_HOSP_ID")
	private FaxFinalHosp faxFinalHosp;
	
	@ManyToOne
	@JoinColumn(name="USER_CODE")
	private UserInfo faxSender;
	

	public Long getFaxDetailId() {
		return faxDetailId;
	}

	public void setFaxDetailId(Long faxDetailId) {
		this.faxDetailId = faxDetailId;
	}

	public FaxType getFaxType() {
		return faxType;
	}

	public void setFaxType(FaxType faxType) {
		this.faxType = faxType;
	}

	public String getFaxTo() {
		return faxTo;
	}

	public void setFaxTo(String faxTo) {
		this.faxTo = faxTo;
	}

	public String getFaxCcEncl() {
		return faxCcEncl;
	}

	public void setFaxCcEncl(String faxCcEncl) {
		this.faxCcEncl = faxCcEncl;
	}

	public String getFaxAttn() {
		return faxAttn;
	}

	public void setFaxAttn(String faxAttn) {
		this.faxAttn = faxAttn;
	}

	public String getFaxNum() {
		return faxNum;
	}

	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	
	public String getSuppManufFlag() {
		return suppManufFlag;
	}

	public void setSuppManufFlag(String suppManufFlag) {
		this.suppManufFlag = suppManufFlag;
	}
	
	public FaxInitHosp getFaxInitHosp() {
		return faxInitHosp;
	}

	public void setFaxInitHosp(FaxInitHosp faxInitHosp) {
		this.faxInitHosp = faxInitHosp;
	}

	public FaxInitSupp getFaxInitSupp() {
		return faxInitSupp;
	}

	public void setFaxInitSupp(FaxInitSupp faxInitSupp) {
		this.faxInitSupp = faxInitSupp;
	}

	public FaxInterimHosp getFaxInterimHosp() {
		return faxInterimHosp;
	}

	public void setFaxInterimHosp(FaxInterimHosp faxInterimHosp) {
		this.faxInterimHosp = faxInterimHosp;
	}

	public FaxInterimSupp getFaxInterimSupp() {
		return faxInterimSupp;
	}

	public void setFaxInterimSupp(FaxInterimSupp faxInterimSupp) {
		this.faxInterimSupp = faxInterimSupp;
	}

	public FaxFinalHosp getFaxFinalHosp() {
		return faxFinalHosp;
	}

	public void setFaxFinalHosp(FaxFinalHosp faxFinalHosp) {
		this.faxFinalHosp = faxFinalHosp;
	}
	
	public UserInfo getFaxSender() {
		return faxSender;
	}

	public void setFaxSender(UserInfo faxSender) {
		this.faxSender = faxSender;
	}

}
