package hk.org.ha.model.pms.dqa.biz;

import javax.ejb.Stateless;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.coa.CoaResultServiceLocal;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("dqaBatchService")
@MeasureCalls()
public class DqaBatchServiceBean implements DqaBatchServiceLocal {

	@In(create = true)
	private CoaResultServiceLocal coaResultService;
	
	@In(create = true)
	private PhsImportServiceLocal phsImportService;
	
	@In(create = true)
	private ErpContractImportServiceLocal erpContractImportService;
	
	@Override
	public Boolean genCoaResultToErp() {
		return coaResultService.getCoaResult();
	}

	@Override
	public Boolean importPhsSupplier() {
		return phsImportService.importSupplier();
	}

	@Override
	public Boolean importPhsCompany() {
		return phsImportService.importCompany();
	}

	@Override
	public Boolean importErpContract() {
		return erpContractImportService.importErpContract();
	}

	@Override
	public Boolean importPhsStockMonthlyExp() {
		return phsImportService.importStockMonthlyExp();
	}

	@Override
	public Boolean updateFuncSeqNumYear() {
		return phsImportService.updateFuncSeqNumYear();
	}

}
