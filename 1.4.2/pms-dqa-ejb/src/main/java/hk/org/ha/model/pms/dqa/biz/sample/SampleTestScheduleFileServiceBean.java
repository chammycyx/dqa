package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Collection;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleFileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleFileServiceBean implements SampleTestScheduleFileServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private SampleTestFileServiceLocal sampleTestFileService;
   
	@In(required = false)
	private SampleTestSchedule sampleTestSchedule;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileMOA;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileFPS;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileVR;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileFullDetailMOA;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileFullDetailFPS;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileFullDetailVR;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFileFullDetailTR;
	
	@Out(required = false)
	private SampleTestScheduleFile sampleTestScheduleFile;
	
	private static final String SCHED_FIND_BY_ID_SQL = "SampleTestScheduleFile.findByScheduleId";
	private static final String SCHED_ID = "scheduleId";
	private static final String O_SAMPLE_TEST_FILE = "o.sampleTestFile";
	private static final String O_FILE_ITEM = "o.sampleTestFile.fileItem";
	private static final String O_SAMPLE_TEST_SCHED = "o.sampleTestSchedule";
	private static final String O_SAMPLE_TEST = "o.sampleTestSchedule.sampleTest";
	private static final String O_CONTRACT = "o.sampleTestSchedule.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.sampleTestSchedule.contract.supplier";
	private static final String O_CONTRACT_MANUF = "o.sampleTestSchedule.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.sampleTestSchedule.contract.pharmCompany";
	
	private boolean success;
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleFileForDocument(Long scheduleId){
		logger.debug("retrieveSampleTestScheduleFileForDocument");
		sampleTestScheduleFileVR = null;
		sampleTestScheduleFileMOA=null;
		sampleTestScheduleFileFPS=null;
		
		List<SampleTestScheduleFile> schedFileList = em.createNamedQuery(SCHED_FIND_BY_ID_SQL)
		.setParameter(SCHED_ID, scheduleId)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_FILE)
		.setHint(QueryHints.FETCH, O_FILE_ITEM)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_SCHED)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.setHint(QueryHints.FETCH, O_CONTRACT)
		.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
		.setHint(QueryHints.FETCH, O_CONTRACT_MANUF)
		.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
//		.setHint(QueryHints.FETCH, "o.sampleItem")
//		.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
//		.setHint(QueryHints.FETCH, "o.sampleItem.drugItem")
		.getResultList();

		sampleTestScheduleFileMOA = null;
		sampleTestScheduleFileFPS = null;
		sampleTestScheduleFileVR = null;
		
		for (SampleTestScheduleFile sfile: schedFileList){
			if ((SampleTestFileCat.MethodofAssay).equals(sfile.getSampleTestFile().getFileCat())){
				logger.debug("...sampleTestScheduleFile MOA set");
				sampleTestScheduleFileMOA = sfile;
				
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getSupplier();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getSupplier().getContact();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getPharmCompany();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getPharmCompany().getContact();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getManufacturer();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getContract().getManufacturer().getContact();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getInstitution();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getTestFrequency();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getLab();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getMicroBioTest();
				sampleTestScheduleFileMOA.getSampleTestSchedule().getChemicalAnalysis();
				sampleTestScheduleFileMOA.getSampleTestFile().getSupplier();
				sampleTestScheduleFileMOA.getSampleTestFile().getSupplier().getContact();
				sampleTestScheduleFileMOA.getSampleTestFile().getPharmCompany();
				sampleTestScheduleFileMOA.getSampleTestFile().getPharmCompany().getContact();
				sampleTestScheduleFileMOA.getSampleTestFile().getManufacturer();
				sampleTestScheduleFileMOA.getSampleTestFile().getManufacturer().getContact();
				
			}else if ((SampleTestFileCat.FinishedProductSpecification).equals(sfile.getSampleTestFile().getFileCat())){
				logger.debug("...sampleTestScheduleFile FPS set");
				sampleTestScheduleFileFPS = sfile;
				
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getSupplier();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getSupplier().getContact();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getPharmCompany();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getPharmCompany().getContact();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getManufacturer();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getContract().getManufacturer().getContact();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getInstitution();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getTestFrequency();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getLab();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getMicroBioTest();
				sampleTestScheduleFileFPS.getSampleTestSchedule().getChemicalAnalysis();
				sampleTestScheduleFileFPS.getSampleTestFile().getSupplier();
				sampleTestScheduleFileFPS.getSampleTestFile().getSupplier().getContact();
				sampleTestScheduleFileFPS.getSampleTestFile().getPharmCompany();
				sampleTestScheduleFileFPS.getSampleTestFile().getPharmCompany().getContact();
				sampleTestScheduleFileFPS.getSampleTestFile().getManufacturer();
				sampleTestScheduleFileFPS.getSampleTestFile().getManufacturer().getContact();
				
			}else if ((SampleTestFileCat.ValidationReport).equals(sfile.getSampleTestFile().getFileCat())){
				logger.debug("...sampleTestScheduleFile VR set");
				sampleTestScheduleFileVR = sfile;
				
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getSupplier();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getSupplier().getContact();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getPharmCompany();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getPharmCompany().getContact();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getManufacturer();
				sampleTestScheduleFileVR.getSampleTestSchedule().getContract().getManufacturer().getContact();
				sampleTestScheduleFileVR.getSampleTestSchedule().getInstitution();
				sampleTestScheduleFileVR.getSampleTestSchedule().getTestFrequency();
				sampleTestScheduleFileVR.getSampleTestSchedule().getLab();
				sampleTestScheduleFileVR.getSampleTestSchedule().getMicroBioTest();
				sampleTestScheduleFileVR.getSampleTestSchedule().getChemicalAnalysis();
				sampleTestScheduleFileVR.getSampleTestFile().getSupplier();
				sampleTestScheduleFileVR.getSampleTestFile().getSupplier().getContact();
				sampleTestScheduleFileVR.getSampleTestFile().getPharmCompany();
				sampleTestScheduleFileVR.getSampleTestFile().getPharmCompany().getContact();
				sampleTestScheduleFileVR.getSampleTestFile().getManufacturer();
				sampleTestScheduleFileVR.getSampleTestFile().getManufacturer().getContact();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleFileFullDetailForDocument(Long scheduleId){
		logger.debug("retrieveSampleTestScheduleFileFullDetailForDocument");
		sampleTestScheduleFileFullDetailVR = null;
		sampleTestScheduleFileFullDetailMOA=null;
		sampleTestScheduleFileFullDetailFPS=null;
		sampleTestScheduleFileFullDetailTR=null;
		
		List<SampleTestScheduleFile> schedFileList = em.createNamedQuery(SCHED_FIND_BY_ID_SQL)
		.setParameter(SCHED_ID, scheduleId)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_FILE)
		.setHint(QueryHints.FETCH, O_FILE_ITEM)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_SCHED)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.setHint(QueryHints.FETCH, O_CONTRACT)
		.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
		.setHint(QueryHints.FETCH, O_CONTRACT_MANUF)
		.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
//		.setHint(QueryHints.FETCH, "o.sampleItem")
//		.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
//		.setHint(QueryHints.FETCH, "o.sampleItem.drugItem")
		.getResultList();

		sampleTestScheduleFileFullDetailMOA = null;
		sampleTestScheduleFileFullDetailFPS = null;
		sampleTestScheduleFileFullDetailVR = null;
		sampleTestScheduleFileFullDetailTR = null;
		
		for (SampleTestScheduleFile sfile: schedFileList){
			if (sfile.getSampleTestFile().getFileCat() == SampleTestFileCat.MethodofAssay){
				logger.debug("...sampleTestScheduleFileFullDetail MOA set");
				sampleTestScheduleFileFullDetailMOA = sfile;
			}else if (sfile.getSampleTestFile().getFileCat() == SampleTestFileCat.FinishedProductSpecification){
				logger.debug("...sampleTestScheduleFileFullDetail FPS set");
				sampleTestScheduleFileFullDetailFPS = sfile;
			}else if (sfile.getSampleTestFile().getFileCat() == SampleTestFileCat.ValidationReport){
				logger.debug("...sampleTestScheduleFileFullDetail VR set");
				sampleTestScheduleFileFullDetailVR = sfile;
			}else if (sfile.getSampleTestFile().getFileCat() == SampleTestFileCat.TestResultReport){
				logger.debug("...sampleTestScheduleFileFullDetail TR set");
				sampleTestScheduleFileFullDetailTR = sfile;
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleFileByScheduleIdFileCat(Long scheduleId, SampleTestFileCat fileCat){
		List<SampleTestScheduleFile> sampleTestScheduleFileList = em.createNamedQuery("SampleTestScheduleFile.findByScheduleIdFileCat")
		.setParameter(SCHED_ID, scheduleId)
		.setParameter("fileCat", fileCat)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_FILE)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_SCHED)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.setHint(QueryHints.FETCH, O_CONTRACT)
		.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
		.setHint(QueryHints.FETCH, O_CONTRACT_MANUF)
		.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.testFrequency")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.lab")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.institution")
		.setHint(QueryHints.BATCH, O_FILE_ITEM)
		.getResultList();
		
		if(sampleTestScheduleFileList==null || sampleTestScheduleFileList.size()==0)
		{
			sampleTestScheduleFile=null;
		}
		else
		{
			sampleTestScheduleFile=sampleTestScheduleFileList.get(0);
			
			sampleTestScheduleFile.getSampleTestSchedule().getContract();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getSupplier();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getSupplier().getContact();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getPharmCompany();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getPharmCompany().getContact();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getManufacturer();
			sampleTestScheduleFile.getSampleTestSchedule().getContract().getManufacturer().getContact();
			sampleTestScheduleFile.getSampleTestSchedule().getInstitution();
			sampleTestScheduleFile.getSampleTestSchedule().getTestFrequency();
			sampleTestScheduleFile.getSampleTestSchedule().getPayment();
			sampleTestScheduleFile.getSampleTestSchedule().getLab();
			sampleTestScheduleFile.getSampleTestSchedule().getMicroBioTest();
			sampleTestScheduleFile.getSampleTestSchedule().getChemicalAnalysis();
			sampleTestScheduleFile.getSampleTestFile().getSupplier();
			sampleTestScheduleFile.getSampleTestFile().getSupplier().getContact();
			sampleTestScheduleFile.getSampleTestFile().getPharmCompany();
			sampleTestScheduleFile.getSampleTestFile().getPharmCompany().getContact();
			sampleTestScheduleFile.getSampleTestFile().getManufacturer();
			sampleTestScheduleFile.getSampleTestFile().getManufacturer().getContact();
//			sampleTestScheduleFile.getSampleTestFile().postLoad(); //?
		}
		
		
		if((fileCat == SampleTestFileCat.TestResultReport) &&
				sampleTestScheduleFile!=null	)
		{
			sampleTestFileService.addSampleTestFileForTestResult(sampleTestScheduleFile.getSampleTestFile());
		}
	}
	
	@SuppressWarnings("unchecked")
	public SampleTestScheduleFile findSampleTestScheduleFileByScheduleIdFileCat(Long scheduleId, SampleTestFileCat fileCat){
		SampleTestScheduleFile sampleTestScheduleFileFind = null;
		
		List<SampleTestScheduleFile> sampleTestScheduleFileList = em.createNamedQuery("SampleTestScheduleFile.findByScheduleIdFileCat")
		.setParameter(SCHED_ID, scheduleId)
		.setParameter("fileCat", fileCat)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_FILE)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST_SCHED)
		.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
		.setHint(QueryHints.FETCH, O_CONTRACT)
		.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
		.setHint(QueryHints.FETCH, O_CONTRACT_MANUF)
		.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.testFrequency")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
		.setHint(QueryHints.FETCH, "o.sampleTestSchedule.lab")
		.setHint(QueryHints.BATCH, O_FILE_ITEM)
		.setHint(QueryHints.BATCH, "o.sampleTestSchedule.institution")
		.getResultList();
		
		if(sampleTestScheduleFileList==null || sampleTestScheduleFileList.size()==0)
		{
			sampleTestScheduleFileFind=null;
		}
		else
		{
			sampleTestScheduleFileFind=sampleTestScheduleFileList.get(0);
			
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getSupplier();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getSupplier().getContact();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getPharmCompany();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getPharmCompany().getContact();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getManufacturer();
			sampleTestScheduleFileFind.getSampleTestSchedule().getContract().getManufacturer().getContact();
			sampleTestScheduleFileFind.getSampleTestSchedule().getInstitution();
			sampleTestScheduleFileFind.getSampleTestSchedule().getTestFrequency();
			sampleTestScheduleFileFind.getSampleTestSchedule().getPayment();
			sampleTestScheduleFileFind.getSampleTestSchedule().getLab();
			sampleTestScheduleFileFind.getSampleTestSchedule().getMicroBioTest();
			sampleTestScheduleFileFind.getSampleTestSchedule().getChemicalAnalysis();
			sampleTestScheduleFileFind.getSampleTestFile().getSupplier();
			sampleTestScheduleFileFind.getSampleTestFile().getSupplier().getContact();
			sampleTestScheduleFileFind.getSampleTestFile().getPharmCompany();
			sampleTestScheduleFileFind.getSampleTestFile().getPharmCompany().getContact();
			sampleTestScheduleFileFind.getSampleTestFile().getManufacturer();
			sampleTestScheduleFileFind.getSampleTestFile().getManufacturer().getContact();
//			sampleTestScheduleFileFind.getSampleTestFile().postLoad(); //?
		}
		
		return sampleTestScheduleFileFind;
		
	}
	

	public void createSampleTestScheduleFileForSchedConfirm(SampleTestSchedule scheduleIn, Collection<SampleTestFile> fileList){
		logger.debug("createSampleTestScheduleFileForSchedConfirm");
		success = false;
		SampleTestSchedule sampleTestScheduleGetLazyIn=getLazySampleTestSchedule(scheduleIn);
		if (!fileList.isEmpty()){
			SampleTestScheduleFile stsf;
			for (SampleTestFile stf: fileList){
				stsf = new SampleTestScheduleFile();
				stsf.setSampleTestSchedule(sampleTestScheduleGetLazyIn);
				stsf.setSampleTestFile(stf);
				em.persist(stsf);
				em.flush();
				success = true;
			}	
		}
	}
	
	
	public void createSampleTestScheduleFileForTestResult(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("createSampleTestScheduleFileForTestResult");
		SampleTestSchedule sampleTestScheduleGetLazyIn=getLazySampleTestSchedule(sampleTestScheduleIn);
		SampleTestScheduleFile sampleTestScheduleFileFind = null;
		sampleTestScheduleFileFind = findSampleTestScheduleFileByScheduleIdFileCat(sampleTestScheduleGetLazyIn.getScheduleId(),SampleTestFileCat.TestResultReport);
		
		if (sampleTestScheduleFileFind==null)
		{
			SampleTestScheduleFile sampleTestScheduleFileInsert = new SampleTestScheduleFile();
			
			SampleTestFile sampleTestFileInsert = new SampleTestFile();
			
			sampleTestFileInsert.setFileCat(SampleTestFileCat.TestResultReport);
			sampleTestFileInsert.setItemCode(sampleTestScheduleGetLazyIn.getSampleItem().getItemCode());
			sampleTestFileInsert.setSupplier(sampleTestScheduleGetLazyIn.getContract().getSupplier());
			sampleTestFileInsert.getSupplier().setContact(sampleTestScheduleGetLazyIn.getContract().getSupplier().getContact());
			sampleTestFileInsert.setManufacturer(sampleTestScheduleGetLazyIn.getContract().getManufacturer());
			sampleTestFileInsert.getManufacturer().setContact(sampleTestScheduleGetLazyIn.getContract().getManufacturer().getContact());
			sampleTestFileInsert.setPharmCompany(sampleTestScheduleGetLazyIn.getContract().getPharmCompany());
			sampleTestFileInsert.getPharmCompany().setContact(sampleTestScheduleGetLazyIn.getContract().getPharmCompany().getContact());
			em.persist(sampleTestFileInsert);
			em.flush();
			
			sampleTestScheduleFileInsert.setSampleTestFile(sampleTestFileInsert);
			sampleTestScheduleFileInsert.setSampleTestSchedule(sampleTestScheduleGetLazyIn);
			
			em.persist(sampleTestScheduleFileInsert);
			em.flush();
		}
	}
	
	public void addSampleTestScheduleFileForDocument(SampleTestFile sampleTestFileIn){
		logger.debug("addSampleTestScheduleFileForDocment");
		sampleTestSchedule = getLazySampleTestSchedule(sampleTestSchedule);
		if (sampleTestFileIn !=null){
			// for lazy relation
			sampleTestFileIn.getSupplier();
			sampleTestFileIn.getSupplier().getContact();
			sampleTestFileIn.getPharmCompany();
			sampleTestFileIn.getPharmCompany().getContact();
			sampleTestFileIn.getManufacturer();
			sampleTestFileIn.getManufacturer().getContact();
			sampleTestFileIn.postLoad();
		
		
			if (sampleTestFileIn.getFileCat() == SampleTestFileCat.MethodofAssay){
				logger.debug("...sampleTestScheduleFile MOA ADD");
				sampleTestScheduleFileMOA = new SampleTestScheduleFile(); 
				sampleTestScheduleFileMOA.setSampleTestFile(sampleTestFileIn);
				sampleTestScheduleFileMOA.setSampleTestSchedule(sampleTestSchedule);
				
				logger.debug("schedule num : #0", (sampleTestSchedule!=null?sampleTestSchedule.getScheduleNum():"null"));
				
				
			}else if (sampleTestFileIn.getFileCat() == SampleTestFileCat.FinishedProductSpecification){
				logger.debug("...sampleTestScheduleFile FPS ADD");
				sampleTestScheduleFileFPS = new SampleTestScheduleFile(); 
				sampleTestScheduleFileFPS.setSampleTestFile(sampleTestFileIn);
				sampleTestScheduleFileFPS.setSampleTestSchedule(sampleTestSchedule);
				
				logger.debug("schedule num : #0", (sampleTestSchedule!=null?sampleTestSchedule.getScheduleNum():"null"));
			}else if (sampleTestFileIn.getFileCat() == SampleTestFileCat.ValidationReport){
				logger.debug("...sampleTestScheduleFile VR ADD");
	
				sampleTestScheduleFileVR = new SampleTestScheduleFile(); 
				sampleTestScheduleFileVR.setSampleTestFile(sampleTestFileIn);
				sampleTestScheduleFileVR.setSampleTestSchedule(sampleTestSchedule);
				
				logger.debug("schedule num : #0", (sampleTestSchedule!=null?sampleTestSchedule.getScheduleNum():"null"));
			}
		}
	}
	
	public void deleteSampleTestScheduleFile(SampleTestScheduleFile sampleTestScheduleFileIn){
		logger.debug("deleteSampleTestScheduleFile");
		success = false;
		sampleTestScheduleFileIn.setSampleTestFile(null);
		sampleTestScheduleFileIn.setSampleTestSchedule(null);
		
		em.merge(sampleTestScheduleFileIn);
		em.remove(sampleTestScheduleFileIn);
		em.flush();
		
		success = true;
	}
	
	public void updateSampleTestScheduleFileForDocument(List<SampleTestScheduleFile> sampleTestScheduleFileList){
		logger.debug("updateSampleTestScheduleFileForDocument");
		success = false;
		
		
		if (!sampleTestScheduleFileList.isEmpty()){
			for (SampleTestScheduleFile stsf : sampleTestScheduleFileList){
				getLazySampleTestSchedule(stsf.getSampleTestSchedule());
				boolean isDetach = (stsf.getSampleTestSchedule() == null);
				SampleTestFile sampleTestFile = stsf.getSampleTestFile();
				if (sampleTestFile != null){
					if (isDetach){
						
						if (sampleTestFile.getFileCat() == SampleTestFileCat.MethodofAssay){
							sampleTestScheduleFileMOA = null;
						}else if (sampleTestFile.getFileCat() == SampleTestFileCat.FinishedProductSpecification){
							sampleTestScheduleFileFPS = null;
						}else if (sampleTestFile.getFileCat() == SampleTestFileCat.ValidationReport){
							stsf.setSampleTestFile(null);
							sampleTestScheduleFileVR = null;
							
						}
						em.remove(em.merge(stsf));
						em.flush();
					}
					else
					{
						em.merge(stsf);
						em.flush();
					}
				}
				
					
			}
			//em.flush();
			success = true;
		}
	}

	// virtually link the doc object show in flex
	public void updateSampleTestScheduleFileForDocLink(SampleTestFile sampleTestFileIn , SampleTestScheduleFile orgSampleTestSchedFile){
		logger.debug("updateSampleTestScheduleFileForDocLink");
		
		if (orgSampleTestSchedFile == null){
			
			addSampleTestScheduleFileForDocument(sampleTestFileIn);
		}else {
			if (orgSampleTestSchedFile.getSampleTestSchedule()==null)
			{
				addSampleTestScheduleFileForDocument(sampleTestFileIn);
			}
			else
			{
				getLazySampleTestSchedule(orgSampleTestSchedFile.getSampleTestSchedule());
				orgSampleTestSchedFile.getSampleTestSchedule().getPayment();
				orgSampleTestSchedFile.getSampleTestSchedule().getContract();
				if(orgSampleTestSchedFile.getSampleTestSchedule().getContract()!=null)
				{
					orgSampleTestSchedFile.getSampleTestSchedule().getContract().getSupplier();
					orgSampleTestSchedFile.getSampleTestSchedule().getContract().getManufacturer();
					orgSampleTestSchedFile.getSampleTestSchedule().getContract().getPharmCompany();
					
					if(orgSampleTestSchedFile.getSampleTestSchedule().getContract().getSupplier()!=null)
					{
						orgSampleTestSchedFile.getSampleTestSchedule().getContract().getSupplier().getContact();
					}
					
					if(orgSampleTestSchedFile.getSampleTestSchedule().getContract().getManufacturer()!=null)
					{
						orgSampleTestSchedFile.getSampleTestSchedule().getContract().getManufacturer().getContact();						
					}
					
					if(orgSampleTestSchedFile.getSampleTestSchedule().getContract().getPharmCompany()!=null)
					{
						orgSampleTestSchedFile.getSampleTestSchedule().getContract().getPharmCompany().getContact();						
					}
				}
				orgSampleTestSchedFile.getSampleTestSchedule().getInstitution();
				orgSampleTestSchedFile.getSampleTestSchedule().getTestFrequency();
				orgSampleTestSchedFile.getSampleTestSchedule().getLab();
				orgSampleTestSchedFile.getSampleTestSchedule().getMicroBioTest();
				orgSampleTestSchedFile.getSampleTestSchedule().getChemicalAnalysis();
			
			
				if (sampleTestFileIn !=null){
					// for lazy relation
					sampleTestFileIn.getSupplier();
					sampleTestFileIn.getPharmCompany();
					sampleTestFileIn.getPharmCompany().getContact();
					sampleTestFileIn.getManufacturer();
					sampleTestFileIn.getManufacturer().getContact();
	//				sampleTestFileIn.postLoad(); 
					sampleTestFileIn.getSupplier().getContact();
					
					
					if (sampleTestFileIn.getFileCat() == SampleTestFileCat.MethodofAssay){
						logger.debug("...sampleTestScheduleFile MOA update link");
						
						sampleTestScheduleFileMOA = orgSampleTestSchedFile;	
						sampleTestScheduleFileMOA.setSampleTestFile(sampleTestFileIn);
						
					}else if (sampleTestFileIn.getFileCat() == SampleTestFileCat.FinishedProductSpecification){
						logger.debug("...sampleTestScheduleFile FPS update link");
						 
						sampleTestScheduleFileFPS = orgSampleTestSchedFile;
						sampleTestScheduleFileFPS.setSampleTestFile(sampleTestFileIn);
						
					}else if (sampleTestFileIn.getFileCat() == SampleTestFileCat.ValidationReport){
						logger.debug("...sampleTestScheduleFile VR update link");
	 
						sampleTestScheduleFileVR = orgSampleTestSchedFile;
						sampleTestScheduleFileVR.setSampleTestFile(sampleTestFileIn);
					}
				}
			}
		}
	}
	
	public void updateSampleTestScheduleFileForTestResult(SampleTestScheduleFile sampleTestScheduleFileIn, SampleTestFile sampleTestFileIn, String actionType){
		logger.debug("updateSampleTestScheduleFileForTestResult");
		success = false;
		SampleTestFile sampleTestFileRemove=null;
		
		getLazySampleTestSchedule(sampleTestScheduleFileIn.getSampleTestSchedule());
		
		if(sampleTestScheduleFileIn.getSampleTestFile().getSampleTestFileId().equals(sampleTestFileIn.getSampleTestFileId()))
		{
			sampleTestFileRemove = em.find(SampleTestFile.class, sampleTestFileIn.getSampleTestFileId());
			
			if (sampleTestFileIn.getFileItem()!=null)
			{
				sampleTestScheduleFileIn.getSampleTestFile().setFileItem(sampleTestFileIn.getFileItem());
				em.merge(sampleTestScheduleFileIn.getSampleTestFile().getFileItem());
				sampleTestFileRemove=null;
			}
			else
			{
				SampleTestFile sampleTestFileInsert = new SampleTestFile();
				sampleTestFileInsert.setFileCat(sampleTestFileIn.getFileCat());
				sampleTestFileInsert.setSupplier(sampleTestFileIn.getSupplier());
				sampleTestFileInsert.setManufacturer(sampleTestFileIn.getManufacturer());
				sampleTestFileInsert.setPharmCompany(sampleTestFileIn.getPharmCompany());
				sampleTestFileInsert.setFileItem(null);
				em.persist(sampleTestFileInsert);
				
				sampleTestScheduleFileIn.setSampleTestFile(sampleTestFileInsert);
			}
		}
		else
		{	
			if(sampleTestScheduleFileIn.getSampleTestFile().getFileItem()==null)
			{ 
				sampleTestFileRemove = em.find(SampleTestFile.class, sampleTestScheduleFileIn.getSampleTestFile().getSampleTestFileId());
			}
			
			sampleTestScheduleFileIn.setSampleTestFile(sampleTestFileIn);
			
			if (sampleTestFileIn.getFileItem()!=null)
			{
				em.merge(sampleTestScheduleFileIn.getSampleTestFile().getFileItem());
			}
			
		}
		em.merge(sampleTestScheduleFileIn.getSampleTestFile());
		em.merge(sampleTestScheduleFileIn.getSampleTestSchedule());
		sampleTestScheduleFile=em.merge(sampleTestScheduleFileIn);
		em.flush();
		
		
		if(sampleTestFileRemove!=null)
		{
			if(sampleTestFileRemove.getFileItem()==null)
			{
				em.remove(sampleTestFileRemove);
				em.flush();	
			}
		}

		
		if(actionType.equals("Confirm"))
		{
			sampleTestScheduleFile.getSampleTestSchedule().setScheduleStatus(ScheduleStatus.Payment);
			em.merge(sampleTestScheduleFile.getSampleTestSchedule());
			em.flush();
		}
		
		success = true;	
	}
	
	
	public boolean isSuccess() {
		return success;
	}

	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		if(sampleTestScheduleLazy!=null)
		{
			sampleTestScheduleLazy.getPayment();
			sampleTestScheduleLazy.getContract();
			if(sampleTestScheduleLazy.getContract()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier();
				sampleTestScheduleLazy.getContract().getManufacturer();
				sampleTestScheduleLazy.getContract().getPharmCompany();
				
				if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
				{
					sampleTestScheduleLazy.getContract().getSupplier().getContact();
				}
				
				if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
				{
					sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
				}
				
				if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
				{
					sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
				}
			}
			sampleTestScheduleLazy.getInstitution();
			sampleTestScheduleLazy.getTestFrequency();
			sampleTestScheduleLazy.getLab();
			sampleTestScheduleLazy.getMicroBioTest();
			sampleTestScheduleLazy.getChemicalAnalysis();
		}
		
		return sampleTestScheduleLazy;
	}
	
    @Remove
	public void destroy() {
    	if (sampleTestScheduleFileMOA != null){
    		sampleTestScheduleFileMOA = null;
    	}
    	if (sampleTestScheduleFileFPS != null){
    		sampleTestScheduleFileFPS = null;
    	}
    	if (sampleTestScheduleFileVR != null){
    		sampleTestScheduleFileVR = null;
    	}
    	if (sampleTestScheduleFileFullDetailMOA != null){
    		sampleTestScheduleFileFullDetailMOA = null;
    	}
    	if (sampleTestScheduleFileFullDetailFPS != null){
    		sampleTestScheduleFileFullDetailFPS = null;
    	}
    	if (sampleTestScheduleFileFullDetailVR != null){
    		sampleTestScheduleFileFullDetailVR = null;
    	}
    	if (sampleTestScheduleFileFullDetailTR != null){
    		sampleTestScheduleFileFullDetailTR = null;
    	}
    	
    	
	}

}
