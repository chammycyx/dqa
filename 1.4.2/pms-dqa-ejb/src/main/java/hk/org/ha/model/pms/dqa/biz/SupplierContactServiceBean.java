package hk.org.ha.model.pms.dqa.biz;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("supplierContactService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SupplierContactServiceBean implements SupplierContactServiceLocal {
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Supplier supplier;
	
	@In(required = false)
	@Out(required = false)
	private SupplierContact supplierContact;
	
	@In
	private StatusMessages statusMessages;
	
	private boolean updateSucceed;
	
	public void retrieveSupplierContactBySupplierCodeContactId(String supplierCode, Long contactId) {
		logger.info("retrieveSupplierContactBySupplierCodeContactId :#0 , #1 " ,supplierCode,contactId );
		try{	
			supplierContact = (SupplierContact) em.createNamedQuery("SupplierContact.findBySupplierCodeContactId")
			.setParameter("supplierCode", supplierCode)
			.setParameter("contactId", contactId)
			.setHint(QueryHints.FETCH, "o.contact")
			.getSingleResult();	

		}
		catch (NoResultException e){
			supplierContact = null;
		}
	}

	public void addSupplierContact() {
		logger.info("addSupplierContact ");
	
		supplierContact = new SupplierContact();
		supplierContact.setContact(new Contact());
		supplierContact.setSupplier(supplier);
		supplier.getContact();
	}
	
	@SuppressWarnings("unchecked")
	public List retrieveContactIdListByFirstNameLastNameEmail(Contact contact){
		List existing = em.createNamedQuery("Contact.findByFirstNameLastNameEmail")
		.setParameter("firstName", contact.getFirstName().toUpperCase(Locale.ENGLISH))
		.setParameter("lastName", contact.getLastName().toUpperCase(Locale.ENGLISH))
		.setParameter("email", contact.getEmail().toUpperCase(Locale.ENGLISH))
			.getResultList();
		
		return existing;
	}
	
	@SuppressWarnings("unchecked")
	public List retrieveSupplierContactIdListByFirstNameLastNameEmail(SupplierContact supplierContact){
		List existing = em.createNamedQuery("SupplierContact.findByFirstNameLastNameEmail")
		.setParameter("firstName", supplierContact.getContact().getFirstName().toUpperCase(Locale.ENGLISH))
		.setParameter("lastName", supplierContact.getContact().getLastName().toUpperCase(Locale.ENGLISH))
		.setParameter("email", supplierContact.getContact().getEmail().toUpperCase(Locale.ENGLISH))
		.setParameter("supplierCode", supplierContact.getSupplier().getSupplierCode())
			.getResultList();
		
		return existing;
	}
	
	
	@SuppressWarnings("unchecked")
	public void createSupplierContact() {
		logger.info("createSupplierContact :#0 #1",  supplierContact.getContact().getFirstName(),
				supplierContact.getContact().getLastName());
		
		supplierContact.getSupplier().getContact();
		
		updateSucceed = false;
		List existing = new ArrayList<String>();
		
		if (supplierContact.getContact() !=null ){
			existing = retrieveSupplierContactIdListByFirstNameLastNameEmail(supplierContact);
		}
		
		
		if (existing.size() == 0) {
			em.persist(supplierContact);
			supplier.getSupplierContactList().add(supplierContact);
			em.flush();
			
			updateSucceed = true;
		}else {
			statusMessages.addToControl("txtFirstName", "Contact already exists");
			statusMessages.addToControl("txtLastName", "Contact already exists");
			statusMessages.addToControl("txtEmail", "Email already exists");
		}	
	}
	
	@SuppressWarnings("unchecked")
	public void updateSupplierContact() {
		logger.info("updateSupplierContact :#0 ",  supplierContact.getContact().getFirstName());
		Contact tmpContact = supplierContact.getContact();
		
		boolean duplicated = true;

		updateSucceed = false;
		supplierContact.getSupplier().getContact();
		
		List existingContact = retrieveSupplierContactIdListByFirstNameLastNameEmail(supplierContact);
		
		if (existingContact.size() == 1 ) {
			if ( ((Long)existingContact.get(0)).equals(tmpContact.getContactId()) ){ //check updating the same contact 
				
				supplierContact = em.merge(supplierContact);

				em.flush();
				updateSucceed = true;
				duplicated = false;
			}
		}else if (existingContact.size() ==0){ //update to other name/ email
			supplierContact = em.merge(supplierContact);
			
			em.flush();
			updateSucceed = true;
			duplicated = false;
		}
		
		if (duplicated){
			logger.debug("duplicated #0 #1", tmpContact.getFirstName(),tmpContact.getEmail() );

			statusMessages.addToControl("txtName", "Contact - #{supplierContact.getContact().getName()} already exists");
			statusMessages.addToControl("txtEmail", "Email already exists");
		}
	}
	
	public boolean isUpdateSucceed(){
		
		return updateSucceed;
	}
	
	
	public void deleteSupplierContact(SupplierContact supplierContact) {
		logger.info("deleteSupplierContact #0",  supplierContact.getContact().getFirstName());

		em.remove(em.merge(supplierContact));
		em.flush();
	}
    
    @Remove
	public void destroy() {
		if(supplierContact!=null) {
			supplierContact = null;
		}		
	}


}
