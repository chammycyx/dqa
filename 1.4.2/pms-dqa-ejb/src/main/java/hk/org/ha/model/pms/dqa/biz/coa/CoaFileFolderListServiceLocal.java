package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;


import javax.ejb.Local;

@Local
public interface CoaFileFolderListServiceLocal {	
	
	void retrieveCoaFileFolderListForEmailToSupp(CoaBatch coaBatchIn);
	
	void retrieveCoaFileFolderListForReminderToSupp(CoaBatch coaBatchIn);
	
	void retrieveCoaFileFolderListWithoutBatchOthers(Long coaBatchId);
	
	void destroy();
}
