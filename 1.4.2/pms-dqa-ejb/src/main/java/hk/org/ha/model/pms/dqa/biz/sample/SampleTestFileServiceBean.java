package hk.org.ha.model.pms.dqa.biz.sample;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FileItemManagerLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestFileService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestFileServiceBean implements SampleTestFileServiceLocal {
	
	@In(create=true)
	private FileItemManagerLocal fileItemManager;
	
	@In(create=true)
	private SampleTestScheduleFileListServiceLocal sampleTestScheduleFileListService; 
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private SampleTestFile sampleTestFile;
	
	@Out(required = false)
	private SampleTestFile sampleTestFileTR;
	
	private static final String FILE_PATH_PREFIX ="SAMPLE";
	//private static final String DATE_FORMAT = "ddMMyyyy";
	//private static final String NAME_DELIMITER ="_";
	private static final String PATH_DELIMITER ="/";
	
	private boolean success;
	
	private String uploadPath;
	private String errorCode;
	
	public SampleTestFile uploadFile(byte[] bytes, String fileStrParam, SampleTestFile sampleTestFileIn)
	{
		String fileStrIn = fileStrParam.trim();
		logger.debug("uploadFile : #0 ", fileStrIn);
		success = false;
		errorCode ="0019";
		
		if (fileStrIn!=null && bytes !=null){

			String ext = (fileStrIn.lastIndexOf('.')==-1)?"":fileStrIn.substring(fileStrIn.lastIndexOf('.')+1,fileStrIn.length());
			String dbfileName = formatFileName(sampleTestFileIn);
			String dbFilePath = formatFilePath(sampleTestFileIn);

			String diskFilePath = uploadPath + PATH_DELIMITER + dbFilePath ;

			sampleTestFileIn.getFileItem().setFileName(dbfileName);
			
			
			//for maintain lazy relationship
			sampleTestFileIn.getSupplier().getContact();
			sampleTestFileIn.getManufacturer().getContact();
			sampleTestFileIn.getPharmCompany().getContact();

			File fDir = new File(diskFilePath);

			if (!fDir.exists()){
				boolean dirCreated = fDir.mkdirs();
				logger.debug("new folder : #0 , #1",dirCreated, fDir.getPath());
			}

			File f = new File(diskFilePath, dbfileName+"."+ext);
			
			logger.debug("file path : "+ f.getPath());
			int fileNum = 1;
			String newFileName ="";
			if (fDir.exists()){
				while(f.exists()){
					newFileName = dbfileName+"("+ fileNum +")."+ ext;

					f = new File(diskFilePath, newFileName);
					fileNum++;
				}
				
				if (fileItemManager.uploadFile(f, bytes)){
					logger.info("File uploaded : #0", f.getPath());
					
					sampleTestFileIn.getFileItem().setFileName(dbfileName);
					sampleTestFileIn.getFileItem().setFilePath(dbFilePath + PATH_DELIMITER+f.getName());
					
					createSampleTestFile(sampleTestFileIn);
					success = true;
					errorCode = null;
				}
			}
			
			if (!success){
				if (sampleTestFile!=null){
					sampleTestFile.getFileItem().setFileName(fileStrIn.substring(0, fileStrIn.indexOf('.')-1));
					sampleTestFile.getFileItem().setFilePath(fileStrIn);
				}
				if (f != null && f.exists()){
					fileItemManager.deleteFile(f);
					logger.info("Process failure , File deleted : #0", f.getPath());
				}
			}
		}
		return sampleTestFile;
		
	}
	
	private String formatFileName(SampleTestFile sampleTestFileIn){
		//Date today = new Date();
		//SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		//String dateStr = (sdf.format(today));
		
		/*String dbfileName = sampleTestFileIn.getItemCode() + NAME_DELIMITER + sampleTestFileIn.getSupplier().getSupplierCode() 
		+ NAME_DELIMITER + sampleTestFileIn.getManufacturer().getCompanyCode() 
		+ NAME_DELIMITER + sampleTestFileIn.getPharmCompany().getCompanyCode() 
		+ NAME_DELIMITER + sampleTestFileIn.getFileCat().getDataValue()  + NAME_DELIMITER + dateStr;*/
		String dbfileName=sampleTestFileIn.getFileItem().getFileName();
	
		return dbfileName;
	}
	
	
	private String formatFilePath(SampleTestFile sampleTestFileIn){
		
		String dbFilePath = PATH_DELIMITER + FILE_PATH_PREFIX + PATH_DELIMITER + sampleTestFileIn.getItemCode()
		+ PATH_DELIMITER + sampleTestFileIn.getSupplier().getSupplierCode() 
		+ PATH_DELIMITER + sampleTestFileIn.getManufacturer().getCompanyCode()
		+ PATH_DELIMITER + sampleTestFileIn.getPharmCompany().getCompanyCode()
		+ PATH_DELIMITER + sampleTestFileIn.getFileCat().getDataValue();
		
		return dbFilePath;
	}
	
	public void retrieveSampleTestFileBySampleTestFileId(Long sampleTestFileId){
		logger.debug("retrieveSampleTestFileBySampleTestFileId #0", sampleTestFileId);
		sampleTestFile = em.find(SampleTestFile.class, sampleTestFileId);
		sampleTestFile.getSupplier();
		sampleTestFile.getManufacturer();
		sampleTestFile.getPharmCompany();
		sampleTestFile.getSupplier().getContact();
		sampleTestFile.getManufacturer().getContact();
		sampleTestFile.getPharmCompany().getContact();
	}
	
	@SuppressWarnings("unchecked")
	public SampleTestFile retrieveSampleTestFileForValidation(String itemCode, String supplierCode, String manufCode, String pharmCode){

		Calendar c = Calendar.getInstance(); 
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
		
		SampleTestFile sampleTestFileExist = null;
		List<SampleTestFile> fileList = em.createNamedQuery("SampleTestFile.findForValidation")
			.setParameter("itemCode", itemCode)
			.setParameter("supplierCode", supplierCode)
			.setParameter("manufCode", manufCode)
			.setParameter("pharmCode", pharmCode)
			.setParameter("startDate", new Date(), TemporalType.DATE)
			.setParameter("endDate", sqlEndDate, TemporalType.DATE)
			.getResultList();
		
		if (!fileList.isEmpty()){
			sampleTestFileExist = fileList.get(0);
			sampleTestFileExist.getSupplier();
			sampleTestFileExist.getManufacturer();
			sampleTestFileExist.getPharmCompany();
		}
		
		return sampleTestFileExist;
	} 
	
	public void validateSampleTestFile(SampleTestFile sampleTestFileIn){
		success = false;
		SampleTestFile sampleTestFileDB = retrieveSampleTestFileForValidation(
				sampleTestFileIn.getItemCode(), 
				sampleTestFileIn.getSupplier().getSupplierCode(),
				sampleTestFileIn.getManufacturer().getCompanyCode(),
				sampleTestFileIn.getPharmCompany().getCompanyCode()
				);
		if (sampleTestFileDB!=null){
			
			success = true;
		}
		
	}
	
	public void addSampleTestFile(){
		logger.debug("addSampleTestFile");
		FileItem fileItemNew = new FileItem();
		
		Supplier supplierNew= new Supplier();
		Company pharmComapanyNew= new Company();
		Company manufacturerNew = new Company();
		Contact supplierContactNew = new Contact();
		Contact pharmCompanyContactNew = new Contact();
		Contact manufacturerContactNew = new Contact();
		
		sampleTestFile = new SampleTestFile();
		sampleTestFile.setFileItem(fileItemNew);
		sampleTestFile.setSupplier(supplierNew);
		sampleTestFile.setPharmCompany(pharmComapanyNew);
		sampleTestFile.setManufacturer(manufacturerNew);
		sampleTestFile.getSupplier().setContact(supplierContactNew);
		sampleTestFile.getPharmCompany().setContact(pharmCompanyContactNew);
		sampleTestFile.getManufacturer().setContact(manufacturerContactNew);
		
		
	}
	
	public void addSampleTestFileForTestResult(SampleTestFile sampleTestFileIn){
		logger.debug("addSampleTestFileForTestResult");
		
		if(sampleTestFileIn==null)
		{
			sampleTestFileTR=null;
		}
		else
		{
			if (sampleTestFileIn.getFileCat() == SampleTestFileCat.TestResultReport)
			{
				sampleTestFileTR = sampleTestFileIn;
				
				sampleTestFileTR.postLoad();
				sampleTestFileTR.getSupplier();
				sampleTestFileTR.getManufacturer();
				sampleTestFileTR.getPharmCompany();
				sampleTestFileTR.getSupplier().getContact();
				sampleTestFileTR.getManufacturer().getContact();
				sampleTestFileTR.getPharmCompany().getContact();
			}
		}
	}
	

	public void createSampleTestFile(SampleTestFile sampleTestFileIn){
		logger.debug("createSampleTestFile");
		sampleTestFile = null;
		success = false;
		em.persist(sampleTestFileIn.getFileItem());
		em.persist(sampleTestFileIn);
		em.flush();
		sampleTestFile = sampleTestFileIn;
		
//		sampleTestFile.postLoad();
		sampleTestFile.getSupplier();
		sampleTestFile.getManufacturer();
		sampleTestFile.getPharmCompany();
		sampleTestFile.getSupplier().getContact();
		sampleTestFile.getManufacturer().getContact();
		sampleTestFile.getPharmCompany().getContact();
		
		success = true;
	}
	
	public void updateSampleTestFile(){
		logger.debug("updateSampleTestFile");
		success = false;
		sampleTestFile.getSupplier();
		sampleTestFile.getManufacturer();
		sampleTestFile.getPharmCompany();
		sampleTestFile.getSupplier().getContact();
		sampleTestFile.getManufacturer().getContact();
		sampleTestFile.getPharmCompany().getContact();
		
		em.merge(sampleTestFile.getFileItem());
		em.merge(sampleTestFile);
		em.flush();
		success = true;
	}
	
	public void deleteSampleTestFile(SampleTestFile sampleTestFileIn){
		logger.debug("deleteSampleTestFile");
		success = false;
		List<SampleTestScheduleFile> scheduleFileList = sampleTestScheduleFileListService.retrieveSampleTestScheduleFileListBySmpleTestFileId(sampleTestFileIn.getSampleTestFileId());

		sampleTestFile.getSupplier();
		sampleTestFile.getManufacturer();
		sampleTestFile.getPharmCompany();
		sampleTestFile.getSupplier().getContact();
		sampleTestFile.getManufacturer().getContact();
		sampleTestFile.getPharmCompany().getContact();
				
		if (scheduleFileList==null || scheduleFileList.size()==0){
			em.remove(em.merge(sampleTestFileIn));
			em.flush();
			success = true;
		}else {
			errorCode = "0045";
		}
	}
	
	public boolean isSuccess() {
		return success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	@Remove
	public void destroy(){
		if(sampleTestFile!=null){
			sampleTestFile = null;
		}
	}
	
}
