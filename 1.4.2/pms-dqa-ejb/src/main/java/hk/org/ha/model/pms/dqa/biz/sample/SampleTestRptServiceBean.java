package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventory;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestRptServiceBean implements SampleTestRptServiceLocal {


	@PersistenceContext
	private EntityManager em;
	
	private static final String UNCHECKED = "unchecked";
	private static final String DATE_TIME_FORMAT = "yyyy/MM/dd";
	
	private List<SampleTestInventory> sampleTestInventoryList;
	
	private SimpleDateFormat sdf;
	
	@SuppressWarnings(UNCHECKED)
	private List<SampleMovement> retrieveSampleMovementDataList(Collection<Long> sampleMovementKeyList){
		
		List<SampleMovement> dataList = em.createNamedQuery("SampleMovement.findBySampleMovementKeyList")
			.setParameter("keyList",sampleMovementKeyList )
			.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		    .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
			.getResultList();
		
		return dataList;
	}
	
    @SuppressWarnings(UNCHECKED)
	public void retrieveSampleMovementList(Collection<Long> sampleItemKeyList){
    	
    	sampleTestInventoryList = new ArrayList<SampleTestInventory>();
    	
    	List<SampleMovement> sampleMovementList = retrieveSampleMovementDataList(sampleItemKeyList);
    	
    	if (sampleMovementList.size() > 0){
    		
	    	for (SampleMovement sampleMovement : sampleMovementList ){
	    		SampleTestInventory tmpSampleTestInventory = constructSampleTestInventoryRptData(sampleMovement);
	    		
	    		sampleTestInventoryList.add(tmpSampleTestInventory);
	    	}
    	}
    }
   
	private SampleTestInventory constructSampleTestInventoryRptData(SampleMovement sampleMovement) {

		SampleTestInventory sampleTestInventory = new SampleTestInventory();
		
		sampleTestInventory.setInvDate( sampleMovement.getCreateDate() );
		sampleTestInventory.setItemCode(sampleMovement.getSampleTestSchedule().getSampleItem().getItemCode());
		sampleTestInventory.setItemDesc(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getFullDrugDesc());
		sampleTestInventory.setTest(sampleMovement.getSampleTestSchedule().getSampleTest().getTestCode());
		sampleTestInventory.setBaseUnit(sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug()==null?"":sampleMovement.getSampleTestSchedule().getSampleItem().getDmDrug().getBaseUnit());
		sampleTestInventory.setMovementQty(sampleMovement.getMovementQty());
		sampleTestInventory.setCdp(sampleMovement.getSampleTestSchedule().getUnitPrice());
		sampleTestInventory.setLocation(sampleMovement.getLocationCode());
		sampleTestInventory.setBatchNum(sampleMovement.getSampleTestSchedule().getBatchNum());
		sampleTestInventory.setExpiryDate( sampleMovement.getSampleTestSchedule().getExpiryDate() );
		sampleTestInventory.setOrderType(sampleMovement.getSampleTestSchedule().getOrderType().getDataValue()); 
		sampleTestInventory.setSupplier(sampleMovement.getSampleTestSchedule().getContract().getSupplier().getSupplierCode());
		sampleTestInventory.setManufacturer(sampleMovement.getManufCode());
		sampleTestInventory.setPharmCompany(sampleMovement.getSampleTestSchedule().getContract().getPharmCompany().getCompanyCode());
		sampleTestInventory.setScheduleNum(sampleMovement.getSampleTestSchedule().getScheduleNum());
		sampleTestInventory.setAdjustFlag(sampleMovement.getAdjustFlag());
		sampleTestInventory.setAdjustReason(sampleMovement.getAdjustReason());
		
		return sampleTestInventory;
	}
    
    public List<SampleTestInventory> getSampleTestInventoryList(){
    	return sampleTestInventoryList;
    }
    
    @Remove
	public void destroy(){
    	sampleTestInventoryList = null;
	}

}
