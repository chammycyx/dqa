package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleTestListServiceLocal {
	List<SampleTest> retrieveSampleTestListForReport();
	
	void retrieveSampleTestList();
	
	void destroy();
}
