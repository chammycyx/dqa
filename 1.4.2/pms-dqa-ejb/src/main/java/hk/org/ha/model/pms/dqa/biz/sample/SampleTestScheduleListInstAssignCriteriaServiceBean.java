package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmInstDrug;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.StockMonthlyExp;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;
import hk.org.ha.service.biz.pms.dms.interfaces.DmsDqaServiceJmsRemote;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleListInstAssignCriteriaService")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleListInstAssignCriteriaServiceBean implements SampleTestScheduleListInstAssignCriteriaServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestSchedule> sampleTestScheduleInstAssignCriteriaList;
	
	@In
	private DmsDqaServiceJmsRemote dmsDqaServiceProxy;
	
	private static final String UNCHECKED = "unchecked";
	private static final String RECORD_STATUS = "recordStatus";
	private static final String SCHEDULE_STATUS = "scheduleStatus";
	private static final String ITEM_CODE = "itemCode";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String O_CONTRACT = "o.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.contract.supplier";
	private static final String O_CONTRACT_MANUFACTURER = "o.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.contract.pharmCompany";
	private static final String O_SAMPLE_ITEM = "o.sampleItem";
	private static final String O_SAMPLE_ITEM_RISK_LEVEL = "o.sampleItem.riskLevel";
	private static final String O_INSTITUTION = "o.institution";
	
	private boolean success;
	
	private String errorCode;
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForInstAssignByCriteria(InstAssignmentCriteria instAssignmentCriteriaIn){
		logger.debug("retrieveSampleTestScheduleListForInstAssignByCriteria" );
		
		success = false;
		errorCode = null;
		
		StringBuffer enquiryDetailSql = new StringBuffer();
		enquiryDetailSql.append("select o from SampleTestSchedule o where o.recordStatus= :recordStatus ");
		enquiryDetailSql.append("and o.scheduleStatus = :scheduleStatus ");
		enquiryDetailSql.append("and o.institution is null ");
		enquiryDetailSql.append("and (o.sampleItem.itemCode = :itemCode or :itemCode is null) ");
		enquiryDetailSql.append("and (o.scheduleMonth = :scheduleMonth or :scheduleMonth is null ) ");
		enquiryDetailSql.append("and (o.sampleTest.testCode = :testCode or :testCode is null) ");
		enquiryDetailSql.append("and (o.orderType = :orderType or :orderTypeValue is null) ");
		enquiryDetailSql.append("and (o.sampleItem.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) ");
		enquiryDetailSql.append("order by o.sampleItem.itemCode ");

		Query q = em.createQuery(enquiryDetailSql.toString())
			.setParameter(RECORD_STATUS, RecordStatus.Active)
			.setParameter(SCHEDULE_STATUS, ScheduleStatus.InstitutionAssignment)
			.setParameter(ITEM_CODE, instAssignmentCriteriaIn.getItemCode())
			.setParameter("scheduleMonth", instAssignmentCriteriaIn.getScheduleMonth())
			.setParameter("testCode", instAssignmentCriteriaIn.getTestCode())
			.setParameter("orderType", instAssignmentCriteriaIn.getOrderType())
			.setParameter("orderTypeValue", (instAssignmentCriteriaIn.getOrderType()==null?null:instAssignmentCriteriaIn.getOrderType().getDataValue()))
			.setParameter("riskLevelCode", instAssignmentCriteriaIn.getRiskLevelCode());


		q=q.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.BATCH, O_INSTITUTION)
			.setHint(QueryHints.BATCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.BATCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.BATCH, O_CONTRACT_PHARMCOMPANY);		
		
		sampleTestScheduleInstAssignCriteriaList = q.getResultList();
		em.clear();
		
		if (sampleTestScheduleInstAssignCriteriaList==null || sampleTestScheduleInstAssignCriteriaList.size()==0)
		{
			sampleTestScheduleInstAssignCriteriaList=null;
			success = false;
			errorCode = "0008";
		}
		else
		{
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleInstAssignCriteriaList ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				sampleTestScheduleFind.getContract().getSupplier().getContact();
				sampleTestScheduleFind.getContract().getManufacturer();
				sampleTestScheduleFind.getContract().getManufacturer().getContact();
				sampleTestScheduleFind.getContract().getPharmCompany();
				sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
		
			sampleTestScheduleInstAssignCriteriaList = institutionAssignment(sampleTestScheduleInstAssignCriteriaList);
			success = true;
		}
	}
	
	public List<SampleTestSchedule> institutionAssignment(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("institutionAssignment" );
		
		List<StockMonthlyExp> stockMonthlyExpListIn = null;
		List<StockMonthlyExp> prevStockMonthlyExpListIn = null;
		String prevItemCode = "";
		List<String> itemCodeList = constructItemCodeList(sampleTestScheduleListIn);
		Map<String, List<DmInstDrug>> dmInstDrugListMap = dmsDqaServiceProxy.retrieveDmInstDrugListMap(itemCodeList);
		
		
		// retrieve stock_monthly_exp
		Calendar lastMonthCal = Calendar.getInstance(); 
		lastMonthCal.add(Calendar.MONTH, -1);
		
		stockMonthlyExpListIn = em.createNamedQuery("StockMonthlyExp.findByYearMonthItemList")
								.setParameter("year", lastMonthCal.get(Calendar.YEAR))
								.setParameter("month", lastMonthCal.get(Calendar.MONTH)+1)
								.setParameter("itemCodeList", itemCodeList)
								.getResultList();
			

		Map<String, List<StockMonthlyExp>> stockMonthlyExpListMap = constructStockMonthlyExpMap(stockMonthlyExpListIn);
		
		for(SampleTestSchedule sampleTestScheduleIn:sampleTestScheduleListIn)
		{
			if(prevItemCode.equals(sampleTestScheduleIn.getSampleItem().getItemCode()))
			{
				stockMonthlyExpListIn = prevStockMonthlyExpListIn;
			}
			else
			{
				stockMonthlyExpListIn = removeInvalidInstitution(sampleTestScheduleIn, dmInstDrugListMap, stockMonthlyExpListMap);
			}
			
			if(stockMonthlyExpListIn==null || stockMonthlyExpListIn.size()==0){
				sampleTestScheduleIn.setInstitution(null);
			}
			else
			{
				Collections.shuffle(stockMonthlyExpListIn);
				sampleTestScheduleIn.setInstitution(stockMonthlyExpListIn.get(0).getInstitution());
			}
			
			prevItemCode = sampleTestScheduleIn.getSampleItem().getItemCode();
			prevStockMonthlyExpListIn = stockMonthlyExpListIn;
		}
		
		return sampleTestScheduleListIn;
	}
	
	public List<String> constructItemCodeList(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("constructItemCodeList" );
		List<String> itemCodeList = new ArrayList<String>();
		String prevItemCode = "";
		for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleListIn ) {
			String itemCodeFind = sampleTestScheduleFind.getSampleItem().getItemCode();
			
			if(!itemCodeFind.equals(prevItemCode))
			{
				itemCodeList.add(itemCodeFind);
			}
			
			prevItemCode = itemCodeFind;
		}
		return itemCodeList;
	}
	
	public Map<String, List<StockMonthlyExp>> constructStockMonthlyExpMap(List<StockMonthlyExp> stockMonthlyExpListIn){
		logger.debug("constructStockMonthlyExpMap" );
		
		Map<String, List<StockMonthlyExp>> stockMonthlyExpListMap = new HashMap<String, List<StockMonthlyExp>>();
		
		for (StockMonthlyExp sme:stockMonthlyExpListIn) {
			if (stockMonthlyExpListMap.containsKey(sme.getItemCode())) {
				stockMonthlyExpListMap.get(sme.getItemCode()).add(sme);
			} else {
				List<StockMonthlyExp> tmpStockMonthlyExpList = new ArrayList<StockMonthlyExp>();
				tmpStockMonthlyExpList.add(sme);
				stockMonthlyExpListMap.put(sme.getItemCode(), tmpStockMonthlyExpList);
			}
		}

		return stockMonthlyExpListMap;
	}
	
	public List<StockMonthlyExp> removeInvalidInstitution(SampleTestSchedule sampleTestScheduleIn, Map<String, List<DmInstDrug>> dmInstDrugListMapIn, Map<String, List<StockMonthlyExp>> stockMonthlyExpListMapIn){
		logger.debug("removeInvalidInstitution" );

		if (	(stockMonthlyExpListMapIn.containsKey(sampleTestScheduleIn.getSampleItem().getItemCode())) &&
				(dmInstDrugListMapIn.containsKey(sampleTestScheduleIn.getSampleItem().getItemCode()))
			)
		{
			List<StockMonthlyExp> stockMonthlyExpListReturn = stockMonthlyExpListMapIn.get(sampleTestScheduleIn.getSampleItem().getItemCode());
		
			for (Iterator it = stockMonthlyExpListReturn.iterator();it.hasNext();)
			{
				StockMonthlyExp sme= (StockMonthlyExp)it.next();
				int find=0;
				for(DmInstDrug drug:dmInstDrugListMapIn.get(sampleTestScheduleIn.getSampleItem().getItemCode()))
				{
					if(sme.getInstitution().getInstitutionCode().equals(drug.getInstitution().getInstitutionCode()))
					{
						find=1;
						break;
					}
				}
				if(find==0)
				{
					it.remove();
				}
			}
			
			return stockMonthlyExpListReturn;
		}
		else
		{
			return null;
		}
	}
	
	public void updateSampleTestScheduleForInstAssignmentDetailPopup(SampleTestSchedule sampleTestScheduleIn, String actionType, List<SampleTestSchedule> sampleTestScheduleListIn)
	{
		logger.debug("updateSampleTestScheduleForInstAssignmentDetailPopup" );
		success = false;
		SampleTestSchedule sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleIn);
		
		if(actionType.equals("Confirm"))
		{
			sampleTestScheduleGetLazyIn.setScheduleStatus(ScheduleStatus.DrugSample);
		}
		em.merge(sampleTestScheduleGetLazyIn);
		em.flush();
		
		for(int i=0; i<sampleTestScheduleListIn.size(); i++)
		{
			SampleTestSchedule sts=sampleTestScheduleListIn.get(i);

			if(sampleTestScheduleGetLazyIn.getScheduleId().equals(sts.getScheduleId()))
			{
				sampleTestScheduleListIn.set(i, sampleTestScheduleGetLazyIn);
				break;
			}
		}
		
		if(actionType.equals("Confirm")){
			for (Iterator it = sampleTestScheduleListIn.iterator();it.hasNext();)
			{
				SampleTestSchedule stsConfirm = (SampleTestSchedule)it.next();  	
				if(sampleTestScheduleGetLazyIn.getScheduleId().equals(stsConfirm.getScheduleId()))
				{
					it.remove();
					break;
				}
			}
		}
		
		sampleTestScheduleInstAssignCriteriaList = sampleTestScheduleListIn;
		for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleInstAssignCriteriaList ) {
			sampleTestScheduleFind.getPayment();
			sampleTestScheduleFind.getContract();
			sampleTestScheduleFind.getContract().getSupplier();
			sampleTestScheduleFind.getContract().getSupplier().getContact();
			sampleTestScheduleFind.getContract().getManufacturer();
			sampleTestScheduleFind.getContract().getManufacturer().getContact();
			sampleTestScheduleFind.getContract().getPharmCompany();
			sampleTestScheduleFind.getContract().getPharmCompany().getContact();
			sampleTestScheduleFind.getInstitution();
			sampleTestScheduleFind.getTestFrequency();
			sampleTestScheduleFind.getLab();
			sampleTestScheduleFind.getMicroBioTest();
			sampleTestScheduleFind.getChemicalAnalysis();
		}
		
		success = true;
	}
	
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	@Remove
	public void destroy() {
		if (sampleTestScheduleInstAssignCriteriaList !=null){
			sampleTestScheduleInstAssignCriteriaList = null;
		}
	}
}