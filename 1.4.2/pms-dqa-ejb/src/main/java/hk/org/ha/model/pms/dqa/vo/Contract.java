package hk.org.ha.model.pms.dqa.vo;

import hk.org.ha.model.pms.dqa.udt.ActionType;
import hk.org.ha.model.pms.dqa.udt.CancelFlag;
import hk.org.ha.model.pms.dqa.udt.ClosureStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Contract")
@XmlType(propOrder={"contractHeaderId","contractNum","phsContractNum","revisionNum",
					"contractStatus","supplierCode","supplierName","supplierFaxNum",
					"supplierAddress1","supplierAddress2","supplierAddress3","city",
					"paymentTerms","currency","effectiveStartDate","effectiveEndDate",
				    "contractType","cancelFlag","closureStatus","actionType","contractLines"})
public class Contract {
	
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private Long contractHeaderId;
	private String contractNum;
	private String phsContractNum;
	private String revisionNum;
	private String contractStatus;
	private String supplierCode;
	private String supplierName;
	private String supplierFaxNum;
	private String supplierAddress1;
	private String supplierAddress2;
	private String supplierAddress3;
	private String city;
	private String paymentTerms;
	private String currency;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	private String contractType;		
	private CancelFlag cancelFlag;
	
	private ClosureStatus closureStatus = null;
	private ActionType actionType;
	
	private List<ContractLine> contractLines = new ArrayList<ContractLine>();
	
	public void setContractHeaderId(Long contractHeaderId) {
		this.contractHeaderId = contractHeaderId;
	}
	
	@XmlElement(name="contract_header_id")
	public Long getContractHeaderId() {
		return contractHeaderId;
	}
	
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	
	@XmlElement(name="contract_num")
	public String getContractNum() {
		return contractNum;
	}
	
	public void setPhsContractNum(String phsContractNum) {
		this.phsContractNum = phsContractNum;
	}
	
	@XmlElement(name="phs_contract_num")
	public String getPhsContractNum() {
		return phsContractNum;
	}
	
	public void setRevisionNum(String revisionNum) {
		this.revisionNum = revisionNum;
	}
	
	@XmlElement(name="revision_num")
	public String getRevisionNum() {
		return revisionNum;
	}
	
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	
	@XmlElement(name="contract_status")
	public String getContractStatus() {
		return contractStatus;
	}
	
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	
	@XmlElement(name="supplier_code")
	public String getSupplierCode() {
		return supplierCode;
	}	
			
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	@XmlElement(name="supplier_name")
	public String getSupplierName() {
		return supplierName;
	}
	
	public void setSupplierFaxNum(String supplierFaxNum) {
		this.supplierFaxNum = supplierFaxNum;
	}
	
	@XmlElement(name="supplier_fax_num")
	public String getSupplierFaxNum() {
		return supplierFaxNum;
	}

	public void setSupplierAddress1(String supplierAddress1) {
		this.supplierAddress1 = supplierAddress1;
	}
		
	@XmlElement(name="supplier_address1")
	public String getSupplierAddress1() {
		return supplierAddress1;
	}

	public void setSupplierAddress2(String supplierAddress2) {
		this.supplierAddress2 = supplierAddress2;
	}
	
	@XmlElement(name="supplier_address2")
	public String getSupplierAddress2() {
		return supplierAddress2;
	}
	
	public void setSupplierAddress3(String supplierAddress3) {
		this.supplierAddress3 = supplierAddress3;
	}	

	@XmlElement(name="supplier_address3")
	public String getSupplierAddress3() {
		return supplierAddress3;
	}
		
	public void setCity(String city) {
		this.city = city;
	}
	
	@XmlElement(name="city")
	public String getCity() {
		return city;
	}
		
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	
	@XmlElement(name="payment_terms")
	public String getPaymentTerms() {
		return paymentTerms;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@XmlElement(name="currency")
	public String getCurrency() {
		return currency;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		if (effectiveStartDate != null){
			this.effectiveStartDate = new Date(effectiveStartDate.getTime());
		}
	}
	
	@XmlElement(name="effective_start_date")
	public Date getEffectiveStartDate() {
		return (effectiveStartDate != null) ? new Date(effectiveStartDate.getTime()) : null;
	}
	
	public void setEffectiveEndDate(Date effectiveEndDate) {
		if (effectiveEndDate != null){
			this.effectiveEndDate = new Date(effectiveEndDate.getTime());
		}
	}
	
	@XmlElement(name="effective_end_date")
	public Date getEffectiveEndDate() {
		return (effectiveEndDate != null) ? new Date(effectiveEndDate.getTime()) : null;
	}
	
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	@XmlElement(name="contract_type")
	public String getContractType() {
		return contractType;
	}

	public void setCancelFlag(CancelFlag cancelFlag) {
		this.cancelFlag = cancelFlag;
	}
	
	@XmlElement(name="cancel_flag")
	public CancelFlag getCancelFlag() {
		return cancelFlag;
	}

	public void setClosureStatus(ClosureStatus closureStatus) {
		this.closureStatus = closureStatus;
	}
	
	@XmlElement(name="closure_status")	
	public ClosureStatus getClosureStatus() {
		return closureStatus;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}
	
	@XmlElement(name="action_type")
	public ActionType getActionType() {
		return actionType;
	}

	public void setContractLines(List<ContractLine> contractLines) {
		this.contractLines = contractLines;
	}
	
	@XmlElementWrapper(name="ContractLines")
	@XmlElement(name="ContractLine", type=ContractLine.class)
	public List<ContractLine> getContractLines() {
		return contractLines;
	}
	
	@Override
	public String toString() {
		return "Contract [contractHeaderId=" + contractHeaderId + 
		", contractNum=" + contractNum + 
		", phsContractNum=" + phsContractNum + 
		", revisionNum=" + revisionNum + 
		", contractStatus=" + contractStatus + 
		", supplierCode=" + supplierCode + 
		", supplierName=" + supplierName + 
		", supplierFaxNum=" + supplierFaxNum + 
		", supplierAddress1=" + supplierAddress1 + 
		", supplierAddress2=" + supplierAddress2 + 
		", supplierAddress3=" + supplierAddress3 + 
		", city=" + city + 
		", paymentTerms=" + paymentTerms + 
		", currency=" + currency + 
		", effectiveStartDate=" + effectiveStartDate + 
		", effectiveEndDate=" + effectiveEndDate + 
		", cancelFlag=" + cancelFlag + 
		", closureStatus=" + closureStatus + 
		", actionType=" + actionType + 
		", contractLineList=" + contractLines + "]";
	}
	
}
