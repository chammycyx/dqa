package hk.org.ha.model.pms.dqa.biz.coa;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;

import javax.ejb.Local;

@Local
public interface CoaFileFolderListForFileItemMaintServiceLocal {
	
	List<CoaFileFolder> retrieveCoaFileFolderForFileItemMaint(CoaFileFolder coaFileFolder);
	
	void destroy();
}
