package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("erpContractListService")
@RemoteDestination
@MeasureCalls
public class ErpContractListServiceBean implements ErpContractListServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;			
	
	@In(create = true)
	private ErpContractServiceLocal erpContractService;
	
	//@In(create = true)
	//private SampleTestScheduleNextServiceLocal sampleTestScheduleNextService;
	
	@In(create = true)
	private SampleTestScheduleListServiceLocal sampleTestScheduleListService;
	
	//@In(create = true)
	//private SampleTestScheduleServiceLocal sampleTestScheduleService;

	@SuppressWarnings("unchecked")
	public List<Contract> retrieveContractListByContractNumModuleType( String contractNum, ModuleType moduleType) {
		logger.debug("retrieveContractListByContractNumModuleType #0 #1", contractNum, moduleType);
		return em.createNamedQuery( "Contract.findByContractNumModuleType" )
		         .setParameter( "contractNum" , contractNum)
		         .setParameter( "moduleType", moduleType)
		         .getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Contract> retrieveContractListByContractNumItemCodeModuleType( String contractNum, String itemCode, ModuleType moduleType) {
		logger.debug("retrieveContractListByContractNumItemCodeModuleType #0 #1 #2", contractNum, itemCode, moduleType);
		return em.createNamedQuery( "Contract.findByContractNumItemCodeModuleType" )
		         .setParameter( "contractNum" , contractNum)
		         .setParameter( "itemCode", itemCode)
		         .setParameter( "moduleType", moduleType)
		         .getResultList();
	}
	
	public void updateContractListForSuspension( hk.org.ha.model.pms.dqa.vo.Contract ch, ModuleType moduleType ) {		
		logger.debug("updateContractListForSuspension #0 #1", ch.getContractNum(), moduleType);
		List<Contract> contractList = retrieveContractListByContractNumModuleType( ch.getContractNum(), moduleType );
		for( Contract contract:contractList ) {	
			contract.setContractStatus( ch.getContractStatus() );
			erpContractService.updateContractForSuspension( contract );
		}				
	}
	
	public void updateContractListForSampleTest( hk.org.ha.model.pms.dqa.vo.Contract ch){
		logger.debug("updateContractListForSampleTest #0 ", ch.getContractNum());
		
		for( ContractLine cl : ch.getContractLines() ) {
			List<SampleTestSchedule> sampleTestScheduleListFind = sampleTestScheduleListService.retrieveSampleTestScheduleListByContractLine(cl);
			if( sampleTestScheduleListFind != null ){
				for (SampleTestSchedule sampleTestScheduleFind : sampleTestScheduleListFind){
					erpContractService.updateContractForSampleTest(sampleTestScheduleFind, ch, cl);
				}
			} 
			else {
				logger.info("Contract Line ID #0 for Sample Test not exist", cl.getContractLineId());
			}
			
			
			/*if( ContractStatus.Approved.getDataValue().equals( ch.getContractStatus() ) )
			{
				if( !(CancelFlag.Y == cl.getCancelFlag() || ClosureStatus.CLOSED == cl.getClosureStatus()) ) 
				{
					//For Chem
					sampleTestScheduleNew = sampleTestScheduleNextService.checkForInsertNextSampleTestScheduleByErpContractTestCode(ch, cl, "CHEM");
					if(sampleTestScheduleNew!=null)
					{
						sampleTestScheduleService.createNextSampleTestScheduleByErpContract(sampleTestScheduleNew, ch, cl);
					}
					
					//For Micro
					sampleTestScheduleNew = sampleTestScheduleNextService.checkForInsertNextSampleTestScheduleByErpContractTestCode(ch, cl, "MICRO");
					if(sampleTestScheduleNew!=null)
					{
						sampleTestScheduleService.createNextSampleTestScheduleByErpContract(sampleTestScheduleNew, ch, cl);
					}
				}
			}*/
			
		}
	}

}
