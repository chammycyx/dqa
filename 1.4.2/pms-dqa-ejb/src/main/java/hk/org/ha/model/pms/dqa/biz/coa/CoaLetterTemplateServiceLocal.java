package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import javax.ejb.Local;

@Local
public interface CoaLetterTemplateServiceLocal {

	void retrieveLetterTemplateByDiscrepancyStatusReminderNum(DiscrepancyStatus discrepancyStatus, Integer reminder);
	
	void retrieveLetterTemplateByCode(String code);
	
	void destroy();
}
