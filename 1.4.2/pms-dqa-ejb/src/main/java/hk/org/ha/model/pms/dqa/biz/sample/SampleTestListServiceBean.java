package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestListServiceBean implements SampleTestListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<SampleTest> sampleTestList;
	
	@SuppressWarnings("unchecked")
	public List<SampleTest> retrieveSampleTestListForReport(){
		logger.debug("retrieveSampleTestListForReport");
		
		return em.createNamedQuery("SampleTest.findAll")
			.getResultList();
	}
	
	public void retrieveSampleTestList(){
		
		sampleTestList = retrieveSampleTestListForReport();
	}
	
	@Remove
	public void destroy(){
		if(sampleTestList != null) {
			sampleTestList = null;  
		}
	}
	
	
}
