package hk.org.ha.model.pms.dqa.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.SuspendFlag;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COMPANY")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name = "Company.findAll", query = "select o from Company o order by o.companyCode"),
	@NamedQuery(name = "Company.findByCompanyCode", query = "select o from Company o where o.companyCode = :companyCode"),
	@NamedQuery(name = "Company.findLikeCompanyCode", query = "select o from Company o where o.companyCode like :companyCode order by o.companyCode"),
	@NamedQuery(name = "Company.findByManufFlag", query = "select o from Company o where o.manufFlag = :manufFlag order by o.companyCode"),
	@NamedQuery(name = "Company.findByCompanyCodeManufFlag", query = "select o from Company o where o.companyCode = :companyCode and o.manufFlag = :manufFlag order by o.companyCode"),
	@NamedQuery(name = "Company.findByCompanyCodePharmCompanyFlag", query = "select o from Company o where o.companyCode = :companyCode and o.pharmCompanyFlag = :pharmCompanyFlag order by o.companyCode"),
	@NamedQuery(name = "Company.findByPharmCompanyFlag", query = "select o from Company o where o.pharmCompanyFlag = :pharmCompanyFlag order by o.companyCode"),
	@NamedQuery(name = "Company.findLikeManufacturerCode", query = "select o from Company o where o.companyCode like :manufacturerCode and o.manufFlag = :manufFlag order by o.companyCode"),
	@NamedQuery(name = "Company.findLikePharmCompanyCode", query = "select o from Company o where o.companyCode like :pharmCompanyCode and o.pharmCompanyFlag = :pharmCompanyFlag order by o.companyCode")
	})
public class Company extends VersionEntity {

	private static final long serialVersionUID = 882691852127886676L;
	
	@Id
	@Column(name="COMPANY_CODE", nullable=false)
	private String companyCode;

	@Column(name="COMPANY_NAME", length=40, nullable=false)
	private String companyName;

	@Converter(name = "Company.manufFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("Company.manufFlag")
	@Column(name="MANUF_FLAG", length=1)
	private YesNoFlag manufFlag;

	@Converter(name = "Company.pharmCompanyFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("Company.pharmCompanyFlag")
	@Column(name="PHARM_COMPANY_FLAG", length=1)
	private YesNoFlag pharmCompanyFlag;

	
	@Converter(name = "Company.suspendFlag", converterClass = SuspendFlag.Converter.class )
	@Convert("Company.suspendFlag")
	@Column(name="SUSPEND_FLAG", length=1)
	private SuspendFlag suspendFlag;

	@Column(name="UPLOAD_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date uploadDate;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;
	
    @OneToMany(mappedBy="company")
    private List<CompanyContact> companyContactList;
    
	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setManufFlag(YesNoFlag manufFlag) {
		this.manufFlag = manufFlag;
	}

	public YesNoFlag getManufFlag() {
		return manufFlag;
	}


	public void setPharmCompanyFlag(YesNoFlag pharmCompanyFlag) {
		this.pharmCompanyFlag = pharmCompanyFlag;
	}

	public YesNoFlag getPharmCompanyFlag() {
		return pharmCompanyFlag;
	}

	public void setSuspendFlag(SuspendFlag suspendFlag) {
		this.suspendFlag = suspendFlag;
	}

	public SuspendFlag getSuspendFlag() {
		return suspendFlag;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getUploadDate() {
		return (uploadDate != null) ? new Date(uploadDate.getTime()) : null;
	}

	public void setUploadDate(Date uploadDate) {		
		if (uploadDate != null) {
			this.uploadDate = new Date(uploadDate.getTime());
		} else {
			this.uploadDate = null;
		}
		
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}
	
	public void setCompanyContactList(List<CompanyContact> companyContactList) {
		this.companyContactList = companyContactList;
	}

	public List<CompanyContact> getCompanyContactList() {

		if (companyContactList == null) {
	    	companyContactList = new ArrayList<CompanyContact>();
	    }
		return companyContactList;
	}
	
}
