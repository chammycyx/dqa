package hk.org.ha.model.pms.dqa.udt.suppperf;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CaseStatus implements StringValuedEnum {
	
	Active("A", "Active"),
	Closed("C", "Closed"),
	Suspended("S", "Suspended");	
	
    private final String dataValue;
    private final String displayValue;
        
    CaseStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<CaseStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<CaseStatus> getEnumClass() {
    		return CaseStatus.class;
    	}
    }
}
