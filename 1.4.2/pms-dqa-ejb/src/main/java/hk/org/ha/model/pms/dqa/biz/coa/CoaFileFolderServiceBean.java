package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderManagerBean.CoaFileFolderResult;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmailAtt;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;

import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("coaFileFolderService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaFileFolderServiceBean implements CoaFileFolderServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@In(create=true)
	private CoaOutstandBatchManagerLocal coaOutstandBatchManager;
	
	@In(create=true)
	private CoaInEmailAttManagerLocal coaInEmailAttManager;
	
	@In(create=true)
	private CoaFileFolderManagerLocal coaFileFolderManager;
	
	private Boolean success;

	private Boolean duplicated;

	private String physicalFilePath;

	public String upload(byte[] bytes, String fileIn, CoaFileFolder coaFileFolderIn) 
	{  
		CoaFileFolderResult coaFileFolderResult = coaFileFolderManager.upload(bytes, fileIn, coaFileFolderIn);
		success = coaFileFolderResult.isSuccess();
		duplicated = coaFileFolderResult.isDuplicate();
		physicalFilePath = coaFileFolderResult.getPhysicalFilePath();
		return physicalFilePath;
	}
	
	public void doUpload(byte[] bytes, String fileIn, CoaFileFolder coaFileFolderIn) {		
		upload(bytes, fileIn, coaFileFolderIn);
		
		if (success) {
			coaFileFolderIn.getFileItem().setFilePath(physicalFilePath);
			createCoaFileFolder(coaFileFolderIn);
		
			String batchNum = coaFileFolderIn.getCoaBatch().getBatchNum();
			Contract contract = coaFileFolderIn.getCoaBatch().getCoaItem().getContract();
			if( batchNum != null ) {			
				CoaOutstandBatch coaOutstandBatch = coaOutstandBatchManager.isOutstandBatchExist(contract, batchNum);
				if(coaOutstandBatch != null) {
					coaOutstandBatchManager.updateCoaOutstandBatch( coaOutstandBatch, coaFileFolderIn, new Date() );
				}
			}
		}
	}
	
	public void setUploadPath(String uploadPath) {
	}

	public boolean isSuccess(){

		return success;
	}

	public boolean isDuplicated(){

		return duplicated;
	}
	
	public String getPhysicalFilePath(){
		return physicalFilePath;
	}

	@SuppressWarnings("unchecked")
	public CoaFileFolder retrieveCoaFileFolderByCoaFileFolderId(Long coaFileFolderId){
		logger.debug("retrieveCoaFileFolderByCoaFileFolderId #0", coaFileFolderId);
		List<CoaFileFolder> folderList = (List<CoaFileFolder>)em.createNamedQuery("CoaFileFolder.findByCoaFileFolderId")
										   .setParameter("coaFileFolderId", coaFileFolderId)
										   .getResultList();
		if(folderList.size() > 0) {
			return folderList.get(0);			
		}
		return null;
	}
	
	public void createCoaFileFolder(CoaFileFolder coaFileFolderin){
		success = coaFileFolderManager.createCoaFileFolder(coaFileFolderin);
	}
			
	public void updateCoaFileFolder(CoaFileFolder coaFileFolderin){
		logger.debug("updateCoaFileFolder");
		success = false;
		if (coaFileFolderin != null){
			em.merge(coaFileFolderin.getFileItem());
			em.merge(coaFileFolderin);
			em.flush();
			success = true;
		}
	}
	
	public void deleteCoaFileFolder(CoaFileFolder coaFileFolderIn){
		logger.debug("deleteCoaFileFolder");
		success = false;
		if (coaFileFolderIn!=null)
		{
			
			CoaOutstandBatch osBatch = coaOutstandBatchManager.retrieveCoaOutStandBatchForCoaFileFolder(coaFileFolderIn);
			
			logger.debug("coa outstand batch exist : #0", (osBatch!=null));
			
			if (osBatch != null){
				osBatch.setCoaFileFolder(null);
				em.merge(osBatch);
			}
			
			CoaInEmailAtt emailAtt = coaInEmailAttManager.retrieveCoaInEmailAttForCoaFileFolder(coaFileFolderIn);
			
			logger.debug("coa email att exist : #0", (osBatch!=null));
			
			if (emailAtt != null){
				emailAtt.setCoaFileFolder(null);
				em.merge(emailAtt);
			}
			
			em.remove(em.merge(coaFileFolderIn));
			em.flush();

			success = coaFileFolderManager.deletePhysicalFile(coaFileFolderIn.getFileItem().getFilePath());
		}
	}

	@Remove
	public void destroy(){

	}
}
