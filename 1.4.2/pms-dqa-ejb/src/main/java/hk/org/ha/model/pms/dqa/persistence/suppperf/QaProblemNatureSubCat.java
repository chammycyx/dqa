package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_PROBLEM_NATURE_SUB_CAT")

@NamedQueries({
	@NamedQuery(name = "QaProblemNatureSubCat.findByQaProblemId", query = "select o from QaProblemNatureSubCat o where o.qaProblem.qaProblemId = :qaProblemId "),
	@NamedQuery(name = "QaProblemNatureSubCat.updateByQaProblemId", query = "update QaProblemNatureSubCat o set o.recordStatus = :recordStatus where o.qaProblem.qaProblemId = :qaProblemId  ")

})

@Customizer(AuditCustomizer.class)
public class QaProblemNatureSubCat extends VersionEntity {

	private static final long serialVersionUID = 908329040076159469L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaProblemNatureSubCatSeq")
	@SequenceGenerator(name = "qaProblemNatureSubCatSeq", sequenceName = "SEQ_QA_PROBLEM_NATURE_SUB_CAT", initialValue=10000)
	@Id
	@Column(name="QA_PROBLEM_NATURE_SUB_CAT_ID", nullable=false)
	private Long qaProblemNatureSubCatId;

	@Converter(name = "PharmProblemNature.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("PharmProblemNature.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@ManyToOne
	@JoinColumn(name="PROBLEM_NATURE_SUB_CAT_ID")
	private ProblemNatureSubCat problemNatureSubCat;
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	

	
	
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public ProblemNatureSubCat getProblemNatureSubCat() {
		return problemNatureSubCat;
	}

	public void setProblemNatureSubCat(ProblemNatureSubCat problemNatureSubCat) {
		this.problemNatureSubCat = problemNatureSubCat;
	}

	public Long getQaProblemNatureSubCatId() {
		return qaProblemNatureSubCatId;
	}

	public void setQaProblemNatureSubCatId(Long qaProblemNatureSubCatId) {
		this.qaProblemNatureSubCatId = qaProblemNatureSubCatId;
	}

	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}

	

}
