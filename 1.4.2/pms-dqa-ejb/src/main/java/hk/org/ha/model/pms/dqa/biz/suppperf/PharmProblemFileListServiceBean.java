package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemFile;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemFileListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemFileListServiceBean implements PharmProblemFileListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<PharmProblemFile> pharmProblemFileList;
	
	private boolean success;
	
	private String errorCode;
	
	@SuppressWarnings("unchecked")
	public void retrievePharmProblemFileListByQaProblem(QaProblem qaProblemIn){
		logger.debug("retrievePharmProblemFileListByQaProblem");
		
		pharmProblemFileList = em.createNamedQuery("PharmProblemFile.findByCaseNumRecordStatus")
								.setParameter("caseNum", qaProblemIn.getCaseNum())
								.setParameter("recordStatus", RecordStatus.Active)
								.setHint(QueryHints.FETCH, "o.pharmProblem")
								.setHint(QueryHints.FETCH, "o.pharmProblem.problemHeader")
								.setHint(QueryHints.FETCH, "o.pharmProblem.institution")
								.setHint(QueryHints.FETCH, "o.pharmProblem.contract")
								.setHint(QueryHints.FETCH, "o.pharmProblem.contract.supplier")
								.setHint(QueryHints.FETCH, "o.pharmProblem.contract.manufacturer")
								.setHint(QueryHints.FETCH, "o.pharmProblem.contract.pharmCompany")
								.setHint(QueryHints.FETCH, "o.pharmProblem.pharmBatchNumList")
								.setHint(QueryHints.BATCH, "o.pharmProblem.pharmProblemCountryList")
								.setHint(QueryHints.FETCH, "o.pharmProblem.pharmProblemNatureList")
								.setHint(QueryHints.BATCH, "o.pharmProblem.sendCollectSample")
								.setHint(QueryHints.BATCH, "o.pharmProblem.pharmProblemFileList")
								.setHint(QueryHints.BATCH, "o.pharmProblem.qaProblem")
								.getResultList();
						
		if(pharmProblemFileList!=null && pharmProblemFileList.size()>0)
		{
			for(PharmProblemFile pcf:pharmProblemFileList)
			{
				getLazyPharmProblem(pcf.getPharmProblem());
			}
		}
		else
		{
			pharmProblemFileList=null;
		}
	}
	
	public PharmProblem getLazyPharmProblem(PharmProblem pharmProblemLazy){
		// lazy
		pharmProblemLazy.getPharmProblemCountryList();
		pharmProblemLazy.getPharmProblemCountryList().size();
		pharmProblemLazy.getPharmBatchNumList();
		pharmProblemLazy.getPharmBatchNumList().size();
		pharmProblemLazy.getPharmProblemNatureList();
		pharmProblemLazy.getPharmProblemNatureList().size();
		pharmProblemLazy.getPharmProblemFileList();
		pharmProblemLazy.getPharmProblemFileList().size();
		pharmProblemLazy.getSendCollectSample();
		pharmProblemLazy.getInstitution();
		pharmProblemLazy.getProblemHeader();
		pharmProblemLazy.getQaProblem();
		pharmProblemLazy.getContract();
		if(pharmProblemLazy.getContract()!=null)
		{
			pharmProblemLazy.getContract().getSupplier();
			pharmProblemLazy.getContract().getManufacturer();
			pharmProblemLazy.getContract().getPharmCompany();
			
			if(pharmProblemLazy.getContract().getSupplier()!=null)
			{
				pharmProblemLazy.getContract().getSupplier().getContact();
			}
			if(pharmProblemLazy.getContract().getManufacturer()!=null)
			{
				pharmProblemLazy.getContract().getManufacturer().getContact();
			}
			if(pharmProblemLazy.getContract().getPharmCompany()!=null)
			{
				pharmProblemLazy.getContract().getPharmCompany().getContact();
			}
		}
		
		if(pharmProblemLazy.getSendCollectSample()!=null)
		{
			pharmProblemLazy.getSendCollectSample().getSendToQa();
			pharmProblemLazy.getSendCollectSample().getSendToSupp();
		}
		
		return pharmProblemLazy;
	}
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
		if (pharmProblemFileList != null){
			pharmProblemFileList = null;
		}
	}
}
