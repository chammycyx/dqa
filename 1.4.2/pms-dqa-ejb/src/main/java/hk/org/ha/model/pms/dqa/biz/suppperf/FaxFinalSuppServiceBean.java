package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxSummary;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxFinalSuppRpt;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("faxFinalSuppService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FaxFinalSuppServiceBean implements FaxFinalSuppServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;
	
	private boolean success;
	
	private String errorCode;
	
	private String refNum;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	   
	private JRDataSource dataSource;


	public void createFaxFinalSupp(QaProblem qaProblemIn, EventLog eventLogIn, List<FaxDetailPharmProblem> faxDetailPharmProblemListIn){	
		logger.debug("createFaxFinalSupp");
		
		success = false;

		refNum = null;
		refNum = funcSeqNumService.retrieveNextFRNSeqNum();
		
		if (refNum!=null){
			success = true;
		}
		else{
			success = false;
			errorCode = "0090";
		}
		
		if(success)
		{
			FaxDetail faxDetailIn = eventLogIn.getFaxDetail();
			faxDetailIn.setRefNum(refNum);
			em.persist(faxDetailIn);
			em.flush();
			
			em.persist(eventLogIn);
			em.flush();
		
			if(qaProblemIn.getFaxSummary().getFaxSummaryId()==null)
			{
				FaxSummary faxSummaryIn = qaProblemIn.getFaxSummary();
				em.persist(faxSummaryIn);
				em.flush();
				
				em.merge(qaProblemIn);
				em.flush();
			}
			else
			{
				em.merge(qaProblemIn.getFaxSummary());
				em.flush();
			}
			
			for(FaxDetailPharmProblem fdpc : faxDetailPharmProblemListIn)
			{
				em.persist(fdpc);
				em.flush();
			}
		}
	}
	
	public void createFaxFinalSuppRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag){
		logger.debug("createFaxInitHospRpt");
		
		List<FaxFinalSuppRpt> faxFinalSuppRptList = new ArrayList<FaxFinalSuppRpt>();

		faxFinalSuppRptList.add(mapFaxFinalSuppRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag));

		dataSource = new JRBeanCollectionDataSource(faxFinalSuppRptList);
		
	}
	
	private FaxFinalSuppRpt mapFaxFinalSuppRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag)
	{
		FaxFinalSuppRpt rpt = new FaxFinalSuppRpt();
		
		FaxDetail faxDetail = eventLogIn.getFaxDetail();
		QaProblem qaProblem = qaProblemIn;
		
		if(faxDetail.getCreateDate()==null)
		{
			rpt.setFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(new Date()));
		}
		else
		{
			rpt.setFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getCreateDate()));
		}
		
		List<PharmProblem> pharmProblemList = qaProblem.getPharmProblemList();
		
		HashSet<String> contractNumSet = new HashSet<String>();
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			if(pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.Contract) ||
				pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.StandingQuotation))
			{
				if(reGenFlag)
				{
					contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));
				}
				else
				{
					if (pharmProblem.isSelected()){
						contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));}
				}
			}
		}
		String contractNumList = "";
		int ci=0;
		StringBuffer contractBuf = new StringBuffer();
		for (String contractNum : contractNumSet)
		{
			contractBuf.append(contractNum);
			if (ci != contractNumSet.size()-1){
				contractBuf.append(", ");
			}
			ci++;
		}
		contractNumList = contractBuf.toString();
		rpt.setContractNo(contractNumList);
		rpt.setRefNum(refNumIn);
		rpt.setFullDrugDesc(qaProblem.getProblemHeader().getDmDrug().getFullDrugDesc());
		
		if (faxDetail.getSuppManufFlag().equalsIgnoreCase("M")){
			rpt.setManuf(qaProblem.getManufacturer().getCompanyName());}
		else if (faxDetail.getSuppManufFlag().equalsIgnoreCase("S")){
			rpt.setManuf(qaProblem.getSupplier().getSupplierName());}
		
		rpt.setItemCode(qaProblem.getProblemHeader().getItemCode());
		rpt.setProblemDetail(qaProblem.getQaProblemNature().getProblemDetail());
		
		List<QaBatchNum> batchNumList = qaProblem.getQaBatchNumList();
		String batchNumAll = "";
		StringBuffer batchNumBuf = new StringBuffer();
		for (int i=0;i<batchNumList.size();i++)
		{
			QaBatchNum batchNum = (QaBatchNum)batchNumList.get(i);
			batchNumBuf.append(batchNum.getBatchNum());
			if (i != (batchNumList.size() -1))
			{
				batchNumBuf.append(", ");
			}
		}
		batchNumAll = batchNumBuf.toString();
		rpt.setBatchNum(batchNumAll);
		rpt.setCaseNum(qaProblem.getCaseNum());
		rpt.setOrderType(qaProblem.getProblemHeader().getOrderType().getDisplayValue());
		rpt.setSimlarProblem(qaProblem.getSimilarProblem().getDisplayValue());
		rpt.setFaxFrom(faxDetail.getFaxSender().getContact().getFirstName() + " " + (faxDetail.getFaxSender().getContact().getLastName()==null?" ":faxDetail.getFaxSender().getContact().getLastName()));
		rpt.setAttn((faxDetail.getFaxAttn())==null?"":(faxDetail.getFaxAttn()));
		rpt.setQaFax(faxDetail.getFaxSender().getContact().getFax());
		rpt.setQaOfficePhone(faxDetail.getFaxSender().getContact().getOfficePhone());
		rpt.setFaxTo(faxDetail.getFaxTo());
		rpt.setSupplierFax(faxDetail.getFaxNum());
		
		return rpt;
	}
	
	public void generateFaxFinalSuppRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/faxFinalSupp.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public String getRefNum() {
		return refNum;
	}
	
	@Remove
	public void destroy(){
		
	}
}
