package hk.org.ha.model.pms.dqa.vo.sample;

import hk.org.ha.model.pms.dqa.udt.OrderType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleItemFull {
	
	private String itemCode;
	private String itemDesc;
	private String baseUnit;
	private OrderType orderType;
	private String commodityGroup;
	private String regStatus;
	private String releaseIndicator;
	private String riskLevelCode;
	private String riskLevelDesc;
	private String speicalCat;
	private String chemAnalysisQtyReq;
	private String chemAnalysisQtyReqDesc;
	private String microBioTestQtyReq;
	private String microBioTestQtyReqDesc;
	private String validTestQtyReq;
	private String validTestQtyReqDesc;
	private String chemAnalysisRemark;
	private String microBioTestRemark;
	private String remark;
	private String chemAnalysisExCode;
	private String chemAnalysisExDesc;
	private String microBioTestExCode;
	private String microBioTestExDesc;
	
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getItemDesc() {
		return itemDesc;
	}
	
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	
	
	public String getChemAnaylsisQtyReq() {
		return chemAnalysisQtyReq;
	}
	
	public void setChemAnaylsisQtyReq(String chemAnalysisQtyReq) {
		this.chemAnalysisQtyReq = chemAnalysisQtyReq;
	}
	
	public String getMicroBioTestQtyReq() {
		return microBioTestQtyReq;
	}
	
	public void setMicroBioTestQtyReq(String microBioTestQtyReq) {
		this.microBioTestQtyReq = microBioTestQtyReq;
	}
	
	public String getValidTestQtyReq() {
		return validTestQtyReq;
	}

	public void setValidTestQtyReq(String validTestQtyReq) {
		this.validTestQtyReq = validTestQtyReq;
	}
	
	public String getCommodityGroup() {
		return commodityGroup;
	}

	public void setCommodityGroup(String commodityGroup) {
		this.commodityGroup = commodityGroup;
	}
	
	public String getBaseUnit() {
		return baseUnit;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	
	public String getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}
	
	public String getReleaseIndicator() {
		return releaseIndicator;
	}

	public void setReleaseIndicator(String releaseIndicator) {
		this.releaseIndicator = releaseIndicator;
	}
	
	public String getRiskLevelCode() {
		return riskLevelCode;
	}

	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}
	
	public String getRiskLevelDesc() {
		return riskLevelDesc;
	}

	public void setRiskLevelDesc(String riskLevelDesc) {
		this.riskLevelDesc = riskLevelDesc;
	}
	
	public String getChemAnalysisQtyReq() {
		return chemAnalysisQtyReq;
	}

	public void setChemAnalysisQtyReq(String chemAnalysisQtyReq) {
		this.chemAnalysisQtyReq = chemAnalysisQtyReq;
	}
	
	public String getChemAnalysisQtyReqDesc() {
		return chemAnalysisQtyReqDesc;
	}

	public void setChemAnalysisQtyReqDesc(String chemAnalysisQtyReqDesc) {
		this.chemAnalysisQtyReqDesc = chemAnalysisQtyReqDesc;
	}

	public String getMicroBioTestQtyReqDesc() {
		return microBioTestQtyReqDesc;
	}

	public void setMicroBioTestQtyReqDesc(String microBioTestQtyReqDesc) {
		this.microBioTestQtyReqDesc = microBioTestQtyReqDesc;
	}

	public String getValidTestQtyReqDesc() {
		return validTestQtyReqDesc;
	}

	public void setValidTestQtyReqDesc(String validTestQtyReqDesc) {
		this.validTestQtyReqDesc = validTestQtyReqDesc;
	}

	public String getChemAnalysisRemark() {
		return chemAnalysisRemark;
	}

	public void setChemAnalysisRemark(String chemAnalysisRemark) {
		this.chemAnalysisRemark = chemAnalysisRemark;
	}

	public String getMicroBioTestRemark() {
		return microBioTestRemark;
	}

	public void setMicroBioTestRemark(String microBioTestRemark) {
		this.microBioTestRemark = microBioTestRemark;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getChemAnalysisExCode() {
		return chemAnalysisExCode;
	}

	public void setChemAnalysisExCode(String chemAnalysisExCode) {
		this.chemAnalysisExCode = chemAnalysisExCode;
	}

	public String getChemAnalysisExDesc() {
		return chemAnalysisExDesc;
	}

	public void setChemAnalysisExDesc(String chemAnalysisExDesc) {
		this.chemAnalysisExDesc = chemAnalysisExDesc;
	}

	public String getMicroBioTestExCode() {
		return microBioTestExCode;
	}

	public void setMicroBioTestExCode(String microBioTestExCode) {
		this.microBioTestExCode = microBioTestExCode;
	}

	public String getMicroBioTestExDesc() {
		return microBioTestExDesc;
	}

	public void setMicroBioTestExDesc(String microBioTestExDesc) {
		this.microBioTestExDesc = microBioTestExDesc;
	}
	
	public String getSpeicalCat() {
		return speicalCat;
	}

	public void setSpeicalCat(String speicalCat) {
		this.speicalCat = speicalCat;
	}
}
