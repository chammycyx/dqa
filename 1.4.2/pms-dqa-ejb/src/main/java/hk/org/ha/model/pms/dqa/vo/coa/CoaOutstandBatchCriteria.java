package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaOutstandBatchCriteria {

	private Date fromCreateDate;
	
	private Date toCreateDate;
	
	private Date fromReceiveDate;
	
	private Date toReceiveDate;
	
	private boolean outstand;
	
	public void setFromCreateDate(Date fromCreateDate) {
		if (fromCreateDate != null){
			this.fromCreateDate = new Date(fromCreateDate.getTime());
		}
	}

	public Date getFromCreateDate() {
		return (fromCreateDate != null)? new Date(fromCreateDate.getTime()) : null;
	}

	public void setToCreateDate(Date toCreateDate) {
		if (toCreateDate != null){
			this.toCreateDate = new Date(toCreateDate.getTime());
		}
	}

	public Date getToCreateDate() {
		return (toCreateDate != null)? new Date(toCreateDate.getTime()) : null;
	}

	public void setFromReceiveDate(Date fromReceiveDate) {
		if (fromReceiveDate != null){
			this.fromReceiveDate = new Date(fromReceiveDate.getTime());
		}
	}

	public Date getFromReceiveDate() {
		return (fromReceiveDate != null)? new Date(fromReceiveDate.getTime()) : null;
	}

	public void setToReceiveDate(Date toReceiveDate) {
		if (toReceiveDate != null){
			this.toReceiveDate = new Date(toReceiveDate.getTime());
		}
	}

	public Date getToReceiveDate() {
		return (toReceiveDate != null)? new Date(toReceiveDate.getTime()) : null;
	}

	public boolean isOutstand() {
		return outstand;
	}

	public void setOutstand(boolean outstand) {
		this.outstand = outstand;
	}
	
	@Override
	public String toString(){
		return "[fromCreateDate:"+this.fromCreateDate
			+ ", toCreateDate:"+this.toCreateDate
			+ ", fromReceiveDate:"+this.fromReceiveDate
			+ ", toReceiveDate:"+this.toReceiveDate
			+ ", isOutstand:"+this.outstand + "]";
	}	
}
