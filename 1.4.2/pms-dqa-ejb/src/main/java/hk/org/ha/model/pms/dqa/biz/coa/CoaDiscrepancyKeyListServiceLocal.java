package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CoaDiscrepancyKeyListServiceLocal {

	void retrieveCoaDiscrepancyKeyList();
	
	List<CoaDiscrepancyKey> retrieveCoaDiscrepancyKeyListForReport();	
	
	void destroy();
	
}
