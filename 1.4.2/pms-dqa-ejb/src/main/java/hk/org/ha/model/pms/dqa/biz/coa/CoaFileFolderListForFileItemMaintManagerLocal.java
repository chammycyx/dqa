package hk.org.ha.model.pms.dqa.biz.coa;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;

import javax.ejb.Local;

@Local
public interface CoaFileFolderListForFileItemMaintManagerLocal {
	
	List<CoaFileFolder> retrieveCoaFileFolderForFileItemMaint(CoaFileFolder coaFileFolder);
}
