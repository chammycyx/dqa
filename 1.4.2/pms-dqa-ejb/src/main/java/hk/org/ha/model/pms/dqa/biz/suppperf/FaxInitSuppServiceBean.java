package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInitSupp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxSummary;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxInitSuppRpt;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("faxInitSuppService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FaxInitSuppServiceBean implements FaxInitSuppServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;
	
	private boolean success;
	
	private String errorCode;
	
	private String refNum;

	@In
	private ReportProvider<JRDataSource> reportProvider;
	   
	private JRDataSource dataSource;

	public void createFaxInitSupp(QaProblem qaProblemIn, EventLog eventLogIn, List<FaxDetailPharmProblem> faxDetailPharmProblemListIn){	
		logger.debug("createFaxInitSupp");
		
		success = false;

		refNum = null;
		refNum = funcSeqNumService.retrieveNextFRNSeqNum();
		
		if (refNum!=null){
			success = true;
		}
		else{
			success = false;
			errorCode = "0090";
		}
		
		if(success)
		{
			FaxInitSupp faxInitSuppIn = eventLogIn.getFaxDetail().getFaxInitSupp();
			em.persist(faxInitSuppIn);
			em.flush();
			
			FaxDetail faxDetailIn = eventLogIn.getFaxDetail();
			faxDetailIn.setRefNum(refNum);
			em.persist(faxDetailIn);
			em.flush();
			
			em.persist(eventLogIn);
			em.flush();
		
			if(qaProblemIn.getFaxSummary().getFaxSummaryId()==null)
			{
				FaxSummary faxSummaryIn = qaProblemIn.getFaxSummary();
				em.persist(faxSummaryIn);
				em.flush();
				
				em.merge(qaProblemIn);
				em.flush();
			}
			else
			{
				em.merge(qaProblemIn.getFaxSummary());
				em.flush();
			}
			
			for(FaxDetailPharmProblem fdpc : faxDetailPharmProblemListIn)
			{
				em.persist(fdpc);
				em.flush();
			}
		}
	}
	
	public void createFaxInitSuppRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag){
		logger.debug("createFaxInitSuppRpt");
		
		List<FaxInitSuppRpt> faxInitSuppRptList = new ArrayList<FaxInitSuppRpt>();

		faxInitSuppRptList.add(mapFaxInitSuppRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag));

		dataSource = new JRBeanCollectionDataSource(faxInitSuppRptList);
		
	}
	
	private FaxInitSuppRpt mapFaxInitSuppRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag)
	{
		FaxInitSuppRpt rpt = new FaxInitSuppRpt();
		
		FaxDetail faxDetail = eventLogIn.getFaxDetail();
		FaxInitSupp faxInitSupp = faxDetail.getFaxInitSupp();
		QaProblem qaProblem = qaProblemIn;
		
		if(faxDetail.getCreateDate()==null)
		{
			rpt.setFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(new Date()));
		}
		else
		{
			rpt.setFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getCreateDate()));
		}
		
		List<PharmProblem> pharmProblemList = qaProblem.getPharmProblemList();
		
		String faxTo = faxDetail.getFaxTo();
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			String inst = 
				pharmProblem.getInstitution().getInstitutionName() + "(" + DqaRptUtil.getDateStrDDMMMYYYY(pharmProblem.getProblemDate()) + ")";
			faxTo = faxTo.replaceAll(pharmProblem.getInstitution().getInstitutionCode(), inst);
		}
		
		rpt.setCaseNum(qaProblem.getCaseNum());
		rpt.setInstRptName(faxInitSupp.getInstRptName());
		rpt.setInstRptDate(DqaRptUtil.getDateStrDDMMMYYYY(faxInitSupp.getInstRptDate()));
		rpt.setFaxTo(faxTo);
		rpt.setAttn(findFaxAttn(faxDetail));
		rpt.setFaxNum(faxDetail.getFaxNum());
		
		HashSet<String> contractNumSet = new HashSet<String>();
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			if(pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.Contract) ||
				pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.StandingQuotation))
			{
				if(reGenFlag)
				{
					contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));
				}
				else
				{
					if (pharmProblem.isSelected()){
						contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));}
				}
			}
		}
		
		int ci=0;
		StringBuffer contractBuf = new StringBuffer();
		for (String contractNum : contractNumSet)
		{
			contractBuf.append(contractNum);
			if (ci != contractNumSet.size()-1){
				contractBuf.append(", ");
			}
			ci++;
		}
		rpt.setContractNo(contractBuf.toString());
		rpt.setRefNum(refNumIn);
		rpt.setFullDrugDesc(qaProblem.getProblemHeader().getDmDrug().getFullDrugDesc());
		rpt.setOrderType(qaProblem.getProblemHeader().getOrderType().getDataValue());
		rpt.setProblemDetail(qaProblem.getQaProblemNature().getProblemDetail());
		
		List<QaBatchNum> batchNumList = qaProblem.getQaBatchNumList();
		String batchNumAll = "";
		StringBuffer batchNumBuf = new StringBuffer();
		for (int i=0;i<batchNumList.size();i++)
		{
			QaBatchNum batchNum = (QaBatchNum)batchNumList.get(i);
			batchNumBuf.append(batchNum.getBatchNum());
			if (i != (batchNumList.size() -1))
			{
				batchNumBuf.append(", ");
			}
		}
		batchNumAll=batchNumBuf.toString();
		rpt.setBatchNum(batchNumAll);
		
		StringBuffer instBuf = new StringBuffer();
		int ii = 0;
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			instBuf.append( 
				pharmProblem.getInstitution().getInstitutionName() + "(" + DqaRptUtil.getDateStrDDMMMYYYY(pharmProblem.getProblemDate()) + ")"
			);
			
			if (ii != pharmProblemList.size()-1){
				instBuf.append(", ");
			}
			
			ii++;
		}
		rpt.setInst(instBuf.toString());
		
		rpt.setDistributeListDate(DqaRptUtil.getDateStrDDMMMYYYY(faxInitSupp.getDistributeListDate()));
		if (faxInitSupp.getDistributeListDate() != null){
			rpt.setDistributeListFlag(Boolean.TRUE);}
		else{
			rpt.setDistributeListFlag(null);}
		
		rpt.setConfirmAvailDate(DqaRptUtil.getDateStrDDMMMYYYY(faxInitSupp.getConfirmAvailDate()));
		if (faxInitSupp.getConfirmAvailDate() != null){
			rpt.setConfirmAvailFlag(Boolean.TRUE);}
		else{
			rpt.setConfirmAvailFlag(null);}
		
		
		rpt.setInstContactUser(findInstContactUser(faxInitSupp));
		rpt.setInstContactRank(findInstContactRank(faxInitSupp));
		rpt.setInstContactPhone(findInstContactPhone(faxInitSupp));
		rpt.setInstContactEmail(findInstContactEmail(faxInitSupp));
		rpt.setInstName(findInstName(faxInitSupp));
		rpt.setCollectSampleDate(DqaRptUtil.getDateStrDDMMMYYYY(faxInitSupp.getCollectSampleDate()));
		if (faxInitSupp.getCollectSampleDate() != null){
			rpt.setCollectSampleFlag(Boolean.TRUE);}
		else{
			rpt.setCollectSampleFlag(null);}
		
		rpt.setReferSampleFlag(DqaRptUtil.getBoolean(faxInitSupp.getReferToEmailFlag()));
		
		
		rpt.setProvideExplainDate(DqaRptUtil.getDateStrDDMMMYYYY(faxInitSupp.getProvideExplainDate()));
		
		rpt.setProvideExplainFlag(findProvideExplainFlag(faxInitSupp.getProvideExplainDate()));
		
		rpt.setFaxFromUser(faxDetail.getFaxSender().getContact().getFirstName() + " " + (faxDetail.getFaxSender().getContact().getLastName()==null?" ":faxDetail.getFaxSender().getContact().getLastName()));
		rpt.setFaxFromPhone(faxDetail.getFaxSender().getContact().getOfficePhone()==null?(faxDetail.getFaxSender().getContact().getMobilePhone()==null?"":(faxDetail.getFaxSender().getContact().getMobilePhone())):(faxDetail.getFaxSender().getContact().getOfficePhone()));
		rpt.setFaxFromEmail(faxDetail.getFaxSender().getContact().getEmail()==null?"":(faxDetail.getFaxSender().getContact().getEmail()));
		
		return rpt;
	}
	
	public Boolean findProvideExplainFlag(Date provideExplainDate){
		if(provideExplainDate!=null)
		{
			return Boolean.TRUE;
		}
		else
		{
			return Boolean.FALSE;
		}
	}
	
	
	public String findFaxAttn(FaxDetail faxDetailIn){
		return (faxDetailIn.getFaxAttn())==null?"":(faxDetailIn.getFaxAttn());
	}
	
	public String findInstContactUser(FaxInitSupp faxInitSuppIn){
		return faxInitSuppIn.getInstContactUser()==null?"":faxInitSuppIn.getInstContactUser();
	}
	public String findInstContactRank(FaxInitSupp faxInitSuppIn){
		return faxInitSuppIn.getInstContactRank()==null?"":faxInitSuppIn.getInstContactRank();
	}
	public String findInstContactPhone(FaxInitSupp faxInitSuppIn){
		return faxInitSuppIn.getInstContactPhone()==null?"":faxInitSuppIn.getInstContactPhone();
	}
	public String findInstContactEmail(FaxInitSupp faxInitSuppIn){
		return faxInitSuppIn.getInstContactEmail()==null?"":faxInitSuppIn.getInstContactEmail();
	}
	public String findInstName(FaxInitSupp faxInitSuppIn){
		return faxInitSuppIn.getInstName()==null?"":faxInitSuppIn.getInstName();
	}
	
	public void generateFaxInitSuppRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/faxInitSupp.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public String getRefNum() {
		return refNum;
	}
	
	@Remove
	public void destroy(){
		
	}
}
