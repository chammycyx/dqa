package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaLetterTemplateService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaLetterTemplateServiceBean implements CoaLetterTemplateServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private LetterTemplate letterTemplate;
	
	@In
	private Interpolator interpolator;		
	
	public void retrieveLetterTemplateByDiscrepancyStatusReminderNum(DiscrepancyStatus discrepancyStatus, Integer reminder){
		if( discrepancyStatus == DiscrepancyStatus.EmailToSupplier ) {
			
			retrieveLetterTemplateByCode("ETS");
			
		} else if( discrepancyStatus == DiscrepancyStatus.ReminderToSupplier ) {
			
			if( reminder != null && reminder >= 1 ) {				
				retrieveLetterTemplateByCode("RTSM");
			} else {
				retrieveLetterTemplateByCode("RTS");
			}
			
		}
	}		
	
	public void retrieveLetterTemplateByCode(String code) {
		logger.debug("retrieveLetterTemplateByCode #0", code);
		LetterTemplate tmpletterTemplate = em.find(LetterTemplate.class, code);
		
		letterTemplate = new LetterTemplate();
		letterTemplate.setCode(tmpletterTemplate.getCode());
		letterTemplate.setName(tmpletterTemplate.getName());
		letterTemplate.setSubject(interpolator.interpolate(tmpletterTemplate.getSubject()));
		letterTemplate.setContent(interpolator.interpolate(tmpletterTemplate.getContent()));
		
	}
	
	@Remove
	public void destroy(){
		if (letterTemplate != null) {
			letterTemplate = null;
		}
	}
	
}
