package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetail;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxFinalHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxSummary;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.FaxFinalHospRpt;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("faxFinalHospService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class FaxFinalHospServiceBean implements FaxFinalHospServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;
	
	private boolean success;
	
	private String errorCode;
	
	private String refNum;

	@In
	private ReportProvider<JRDataSource> reportProvider;
	   
	private JRDataSource dataSource;
	
	public void createFaxFinalHosp(QaProblem qaProblemIn, EventLog eventLogIn, List<FaxDetailPharmProblem> faxDetailPharmProblemListIn){	
		logger.debug("createFaxFinalHosp");
		
		success = false;

		refNum = null;
		refNum = funcSeqNumService.retrieveNextFRNSeqNum();
		
		if (refNum!=null){
			success = true;
		}
		else{
			success = false;
			errorCode = "0090";
		}
		
		if(success)
		{
			FaxFinalHosp faxFinalHospIn = eventLogIn.getFaxDetail().getFaxFinalHosp();
			em.persist(faxFinalHospIn);
			em.flush();
			
			FaxDetail faxDetailIn = eventLogIn.getFaxDetail();
			faxDetailIn.setRefNum(refNum);
			em.persist(faxDetailIn);
			em.flush();
			
			em.persist(eventLogIn);
			em.flush();
		
			if(qaProblemIn.getFaxSummary().getFaxSummaryId()==null)
			{
				FaxSummary faxSummaryIn = qaProblemIn.getFaxSummary();
				em.persist(faxSummaryIn);
				em.flush();
				
				em.merge(qaProblemIn);
				em.flush();
			}
			else
			{
				em.merge(qaProblemIn.getFaxSummary());
				em.flush();
			}
			
			for(FaxDetailPharmProblem fdpc : faxDetailPharmProblemListIn)
			{
				em.persist(fdpc);
				em.flush();
			}
		}
	}
	
	public void createFaxFinalHospRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag){
		logger.debug("createFaxFinalHospRpt");
		
		List<FaxFinalHospRpt> faxFinalHospRptList = new ArrayList<FaxFinalHospRpt>();

		faxFinalHospRptList.add(mapFaxFinalHospRpt(qaProblemIn, eventLogIn, refNumIn, reGenFlag));

		dataSource = new JRBeanCollectionDataSource(faxFinalHospRptList);
		
	}
	
	private FaxFinalHospRpt mapFaxFinalHospRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag)
	{
		FaxFinalHospRpt rpt = new FaxFinalHospRpt();
		
		FaxDetail faxDetail = eventLogIn.getFaxDetail();
		FaxFinalHosp faxFinalHosp = faxDetail.getFaxFinalHosp();
		QaProblem qaProblem = qaProblemIn;
		
		if(faxDetail.getFaxInitHosp()==null)
		{
			rpt.setInitFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(new Date()));
		}
		else
		{
			rpt.setInitFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getFaxInitHosp().getCreateDate()));
		}
		
		if(faxDetail.getCreateDate()==null)
		{
			rpt.setFinalFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(new Date()));
		}
		else
		{
			rpt.setFinalFaxCreateDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getFaxFinalHosp().getCreateDate()));
		}
		
		List<PharmProblem> pharmProblemList = qaProblem.getPharmProblemList();
		
		String faxTo = faxDetail.getFaxTo();
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			String inst = 
				pharmProblem.getInstitution().getInstitutionCode() /*+ "(" + DqaRptUtil.getDateStrDDMMMYYYY(pharmProblem.getProblemDate()) + ")"*/;
			faxTo = faxTo.replaceAll(pharmProblem.getInstitution().getInstitutionCode(), inst);
		}
		
		rpt.setFaxTo(faxTo);
		
		HashSet<String> contractNumSet = new HashSet<String>();
		for (PharmProblem pharmProblem : pharmProblemList)
		{
			if(pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.Contract) ||
				pharmProblem.getProblemHeader().getOrderType().equals(OrderTypeAll.StandingQuotation))
			{
				if(reGenFlag)
				{
					contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));
				}
				else
				{
					if (pharmProblem.isSelected()){
						contractNumSet.add((pharmProblem.getContract().getContractNum()==null?"":(pharmProblem.getContract().getContractNum())) + (pharmProblem.getContract().getContractSuffix()==null?"":("-"+pharmProblem.getContract().getContractSuffix())));}
				}
			}
		}
		String contractNumList = "";
		int ci=0;
		StringBuffer contractBuf = new StringBuffer();
		for (String contractNum : contractNumSet)
		{
			contractBuf.append(contractNum);
			if (ci != contractNumSet.size()-1){
				contractBuf.append(", ");
			}
			ci++;
		}
		contractNumList = contractBuf.toString();
		rpt.setContractNo(contractNumList);
		rpt.setRefNum(refNumIn);
		rpt.setCaseNum(qaProblem.getCaseNum());
		rpt.setItemCode(qaProblem.getProblemHeader().getItemCode());
		rpt.setFullDrugDesc(qaProblem.getProblemHeader().getDmDrug().getFullDrugDesc());
		rpt.setOrderType(qaProblem.getProblemHeader().getOrderType().getDataValue());
		
		if (faxDetail.getSuppManufFlag().equalsIgnoreCase("M")){
			rpt.setManuf(qaProblem.getManufacturer().getCompanyName());}
		else if (faxDetail.getSuppManufFlag().equalsIgnoreCase("S")){
			rpt.setManuf(qaProblem.getSupplier().getSupplierName());}
		
		rpt.setProblemDetail(qaProblem.getQaProblemNature().getProblemDetail());
		
		List<QaBatchNum> batchNumList = qaProblem.getQaBatchNumList();
		String batchNumAll = "";
		StringBuffer batchNumBuf = new StringBuffer();
		for (int i=0;i<batchNumList.size();i++)
		{
			QaBatchNum batchNum = (QaBatchNum)batchNumList.get(i);
			batchNumBuf.append(batchNum.getBatchNum());
			if (i != (batchNumList.size() -1))
			{
				batchNumBuf.append(", ");
			}
		}
		batchNumAll = batchNumBuf.toString();
		rpt.setBatchNum(batchNumAll);
		rpt.setFaxFrom(faxDetail.getFaxSender().getContact().getFirstName() + " " + (faxDetail.getFaxSender().getContact().getLastName()==null?" ":faxDetail.getFaxSender().getContact().getLastName()));
		if(faxDetail.getFaxInitHosp()!=null)
		{
			rpt.setBatchSuspDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getFaxInitHosp().getBatchSuspDate()));
			rpt.setProductRecallDesc(faxDetail.getFaxInitHosp().getProductRecallDesc());
			rpt.setResponseRptDate(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getFaxInitHosp().getResponseRptDate()));
			rpt.setIndex0Flag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getIndex0Flag()));
			rpt.setIndex1Flag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getIndex1Flag()));
			rpt.setInspectAllFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getInspectAllFlag()));
			rpt.setRandomInspectFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getRandomInspectFlag()));
			rpt.setRptFindingFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getRptFindingFlag()));
			rpt.setIndex1Date(DqaRptUtil.getDateStrDDMMMYYYY(faxDetail.getFaxInitHosp().getIndex1Date()));
			rpt.setIndex2Flag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getIndex2Flag()));
			rpt.setBatchSuspFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getBatchSuspFlag()));
			rpt.setProductRecallFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getProductRecallFlag()));
			rpt.setInitLevelFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getInstLevelFlag()));
			rpt.setPatLevelFlag(DqaRptUtil.getBoolean(faxDetail.getFaxInitHosp().getPatLevelFlag()));
			rpt.setQaFax(faxDetail.getFaxSender().getContact().getFax());
		}
		
		if (faxFinalHosp.getOtherDesc() != null) {
			rpt.setOtherFlag(Boolean.TRUE);
		}
		
		rpt.setInvestRptFlag(DqaRptUtil.getBoolean(faxFinalHosp.getInvestRptFlag()));
		rpt.setNoActionFlag(DqaRptUtil.getBoolean(faxFinalHosp.getNoActionFlag()));
		rpt.setRelUseOfHoldFlag(DqaRptUtil.getBoolean(faxFinalHosp.getRelBatchFlag()));
		rpt.setStkReplaceFlag(DqaRptUtil.getBoolean(faxFinalHosp.getStkReplaceFlag()));
		rpt.setOtherDesc(faxFinalHosp.getOtherDesc()==null?"":(faxFinalHosp.getOtherDesc()));
		
		return rpt;
	}
	
	public void generateFaxFinalHospRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/faxFinalHosp.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public String getRefNum() {
		return refNum;
	}
	
	@Remove
	public void destroy(){
		
	}
}
