package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "INSTITUTION")
@NamedQueries({
	@NamedQuery(name = "Institution.findByPcuFlagRecordStatus", query = "select o from Institution o where o.pcuFlag = :pcuFlag and o.recordStatus = :recordStatus order by o.institutionCode"),
	@NamedQuery(name = "Institution.findByPcuFlag", query = "select o from Institution o where o.pcuFlag = :pcuFlag order by o.institutionCode"),
	@NamedQuery(name = "Institution.findByInstitutionCode", query = "select o from Institution o where o.institutionCode = :institutionCode order by o.institutionCode"),
	@NamedQuery(name = "Institution.findFullInstitutionList", query = "select o from Institution o where o.recordStatus = :recordStatus order by o.institutionCode"),
	@NamedQuery(name = "Institution.findByPcuCodeRecordStatus", query = "select o from Institution o where o.pcuCode = :pcuCode and o.recordStatus = :recordStatus order by o.institutionCode")
})
@Customizer(AuditCustomizer.class)
public class Institution extends VersionEntity {

	private static final long serialVersionUID = 1302203499896666252L;

	@Id
	@Column(name="INSTITUTION_CODE", length=3, nullable=false)
	private String institutionCode;

	@Column(name="INSTITUTION_NAME", nullable=false)
	private String institutionName;

	@Column(name="GOPC_FLAG", length=1)
	private String gopcFlag;

	@Column(name="INSTITUTION_CLUSTER", length=3)
	private String institutionCluster;

	@Converter(name = "Institution.pcuFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("Institution.pcuFlag")
	@Column(name="PCU_FLAG", length=1)
	private YesNoFlag pcuFlag;

	@Column(name="PCU_CODE", length=3)
	private String pcuCode;
	
	@Column(name="RECORD_STATUS")
	private String recordStatus;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;
	
	@Transient
	public boolean selected;
	
	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getGopcFlag() {
		return gopcFlag;
	}

	public void setGopcFlag(String gopcFlag) {
		this.gopcFlag = gopcFlag;
	}


	public void setInstitutionCluster(String institutionCluster) {
		this.institutionCluster = institutionCluster;
	}

	public String getInstitutionCluster() {
		return institutionCluster;
	}

	public void setPcuFlag(YesNoFlag pcuFlag) {
		this.pcuFlag = pcuFlag;
	}

	public YesNoFlag getPcuFlag() {
		return pcuFlag;
	}

	public String getPcuCode() {
		return pcuCode;
	}

	public void setPcuCode(String pcuCode) {
		this.pcuCode = pcuCode;
	}
	
	public String getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}
	
}
