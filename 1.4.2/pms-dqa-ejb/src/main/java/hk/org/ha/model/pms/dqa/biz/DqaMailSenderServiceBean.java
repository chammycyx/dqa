package hk.org.ha.model.pms.dqa.biz;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.fmk.pms.jms.MessageCreator;
import hk.org.ha.fmk.pms.jms.MessageProducerInf;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.naming.NamingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("dqaMailSenderService")
@RemoteDestination
@MeasureCalls
public class DqaMailSenderServiceBean implements DqaMailSenderServiceLocal {

	@Logger
	private Log logger;
	
	public static final String JAXB_ERROR = "JAXB error";
	public static final String JMS_ERROR = "JMS error";
	public static final String NAMING_ERROR = "Naming error";
	
	@In
    private MessageProducerInf dqaMailServiceMessageProducer;
	
	private Marshaller marshaller = null;			

	private String uploadPath;
	
	private String backupOutEmail;
	
	private String fromEmail;
	
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
	
	public void setBackupOutEmail(String backupOutEmail) {
		this.backupOutEmail = backupOutEmail;
	}
	
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	@PostConstruct
	public void init() {
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(EaiMessage.class);
			marshaller = jaxbContext.createMarshaller();			
		} catch (JAXBException e) {
			logger.error(JAXB_ERROR, e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendBackupInEmail(final Object eaiBackupInMessage, final Map attHashMap, String backupInEmail) {
		logger.debug("sendBackupInEmail ");
		try {
			// back up incoming email
			final StringWriter writer = new StringWriter();
			// for bypass the granite generator error
			final EaiMessage eaiMessage = (EaiMessage)eaiBackupInMessage; 
			
			eaiMessage.setCcs(new String[]{""});
			eaiMessage.setBccs(new String[]{""});
			
			eaiMessage.setTos(new String[]{backupInEmail});
			marshaller.marshal(eaiBackupInMessage, writer);
			logger.debug("backup send : #0",writer.toString());

			MessageCreator backupInCreator = new InnerBackUpMessageCreator ( eaiMessage.getType(), writer.toString(), attHashMap);
			
			if (backupInCreator != null){
				dqaMailServiceMessageProducer.send( backupInCreator );		
			}
		} catch (JAXBException e1) {
			logger.error(JAXB_ERROR , e1);
		} catch (JMSException e2) {
			logger.error(JMS_ERROR , e2);
		} catch (NamingException e3) {
			logger.error(NAMING_ERROR, e3);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static class InnerBackUpMessageCreator implements MessageCreator{
		private String type;
		private String xml;
		private Map attHashMap;
		
		public InnerBackUpMessageCreator(String type, String xml, Map attHashMap){
			this.type = type;
			this.xml = xml;
			this.attHashMap = attHashMap;
		}
		
		public Message createMessage(Session session)
		throws JMSException {
			MapMessage out = session.createMapMessage();
													
			out.setString(type, xml);

			// copy the attachments to out message
			Iterator it = attHashMap.entrySet().iterator();
			while( it.hasNext() ) {
				Map.Entry entry = (Map.Entry)it.next();
				out.setBytes( (String)entry.getKey(), (byte[])entry.getValue() );
			}
			return out;
		}
	}

	public void sendEmail(final LetterTemplate letterTemplate) {
		logger.debug("DqaMailSenderServiceBean.sendEmail #0", letterTemplate.getSubject());		
		
		try {
			logger.debug("backupOutEmail #0, fromEmail #1, uploadPath #2 ", backupOutEmail, fromEmail,uploadPath );
		    dqaMailServiceMessageProducer.send(new InnerMessageCreator(backupOutEmail, fromEmail, uploadPath, marshaller, letterTemplate, logger) );
		} catch (JMSException e2) {
			logger.error(JMS_ERROR,e2);
		} catch (NamingException e3) {
			logger.error(NAMING_ERROR,e3);
		}
	}

	public static class InnerMessageCreator implements MessageCreator{
		private LetterTemplate letterTemplate;
		private String backupOutEmail;
		private String fromEmail;
		private String uploadPath;
		private Marshaller marshaller;
		private Log logger;

		public InnerMessageCreator(String backupOutEmail, String fromEmail, String uploadPath, Marshaller marshaller, LetterTemplate letterTemplate, Log logger){
			this.backupOutEmail = backupOutEmail;
			this.fromEmail = fromEmail;
			this.uploadPath = uploadPath;
			this.marshaller = marshaller;
			this.letterTemplate = letterTemplate;
			this.logger = logger;
		}

		public Message createMessage(Session session)  throws JMSException {
			MapMessage msg = session.createMapMessage();

			EaiMessage eaiMessage = new EaiMessage();
			eaiMessage.setType("email");
			eaiMessage.setMimeType("text/html; charset=UTF-8");

			eaiMessage.setFrom( fromEmail );

			if (letterTemplate.getToList().length >0){
				String[] toArray =letterTemplate.getToList();
				if (!toArray[0].isEmpty()){
					eaiMessage.setTos( letterTemplate.getToList() );
				}
			}
		
			if (letterTemplate.getCcList().length >0){
				String[] ccArray =letterTemplate.getCcList();
				if (!ccArray[0].isEmpty()){
					eaiMessage.setCcs( letterTemplate.getCcList() );
				}
			}
			
			eaiMessage.setBccs( new String[] {backupOutEmail} );
			eaiMessage.setSubject( letterTemplate.getSubject() );
			eaiMessage.setContent( letterTemplate.getContent() );	
			eaiMessage.setSentDate( new Date() );		

			StringWriter writer = new StringWriter();
			try {
				marshaller.marshal(eaiMessage, writer);
			} catch (JAXBException e1) {
				logger.error(JAXB_ERROR,e1);
			}						
			logger.debug("sendMail :: #0 ",writer.toString());
			msg.setString(eaiMessage.getType(), writer.toString());
			try {
				if(letterTemplate.getCoaFileFolderList()!=null)
				{
					for( CoaFileFolder cff : letterTemplate.getCoaFileFolderList() ) {
						if( cff.isEnable() ) {
							
							FileItem f = cff.getFileItem();
							logger.debug("........upload file path : #0", f.getFilePath());			
							msg.setBytes( f.getFileName() + "." + f.getFileType(), 
									IOUtils.toByteArray(new FileInputStream( uploadPath + f.getFilePath() ))
							);
						}
					}												
				}
				
				if(letterTemplate.getSampleTestFileList()!=null)
				{
					for( SampleTestFile stf : letterTemplate.getSampleTestFileList() ) {
						if( stf.isSelected()) {
							
							FileItem f = stf.getFileItem();
							logger.debug("........upload file path : #0", f.getFilePath());			
							msg.setBytes( f.getFileName() + "." + f.getFileType(), 
									IOUtils.toByteArray(new FileInputStream( uploadPath + f.getFilePath() ))
							);
						}
					}												
				}
			} catch (IOException e) {
				logger.error("Fail to read files",e);
			}	

			return msg;
		}
	}
	
	
	@Remove
	public void destroy(){
		
	}
}
