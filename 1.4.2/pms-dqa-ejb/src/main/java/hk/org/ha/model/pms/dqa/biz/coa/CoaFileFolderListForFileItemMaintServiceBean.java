package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.FileCategory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaFileFolderListForFileitemMaintService")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaFileFolderListForFileItemMaintServiceBean implements CoaFileFolderListForFileItemMaintServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	public List<CoaFileFolder> retrieveCoaFileFolderForFileItemMaint(CoaFileFolder coaFileFolder){

		logger.debug("retrieveCoaFileFolderForFileItemMaint #0",coaFileFolder.getCoaFileFolderId());

		CoaItem tempCoaItem = coaFileFolder.getCoaBatch().getCoaItem();

		List<CoaFileFolder> coaFileFolderList =  em.createNamedQuery("CoaFileFolder.findCoaFileFolderForFileItemMaint")
		.setParameter("itemCode", tempCoaItem.getContract().getItemCode() )
		.setParameter("coaItemId", tempCoaItem.getCoaItemId())
		.setParameter("supplierCode", tempCoaItem.getContract().getSupplier().getSupplierCode() )
		.setParameter("orderType",  tempCoaItem.getOrderType())			  
		.setParameter("fileCategory", coaFileFolder.getFileCat())
		.setParameter("effectiveDate", (coaFileFolder.getFileCat()==FileCategory.Batch?null:coaFileFolder.getEffectiveDate()))
		.setParameter("batchNum", (coaFileFolder.getFileCat()==FileCategory.Batch?coaFileFolder.getCoaBatch().getBatchNum():null) )
		.setHint(QueryHints.FETCH, "o.fileItem")
		.setHint(QueryHints.FETCH, "o.coaBatch")
		.setHint(QueryHints.FETCH, "o.coaBatch.coaItem.contract")
		.getResultList(); 

		for (  CoaFileFolder coaFileFolderFind : coaFileFolderList){
			coaFileFolderFind.getCoaBatch();
			coaFileFolderFind.getFileItem();
		}
		return coaFileFolderList;
	}

	@Remove
	public void destroy() {
	}
}
