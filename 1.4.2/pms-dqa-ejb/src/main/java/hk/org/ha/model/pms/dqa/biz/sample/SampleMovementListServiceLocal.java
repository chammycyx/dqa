package hk.org.ha.model.pms.dqa.biz.sample;


import hk.org.ha.model.pms.dqa.vo.sample.SampleMovementCriteria;
import javax.ejb.Local;

@Local
public interface SampleMovementListServiceLocal {
	
	void retrieveSampleMovementListBySampleMovementCriteria(SampleMovementCriteria sampleMovementCriteria);

	void destroy(); 
}
