package hk.org.ha.model.pms.dqa.persistence;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@NamedQueries({
	@NamedQuery(name = "LetterTemplate.findByCode", query = "select o from LetterTemplate o where o.code = :code"),
	@NamedQuery(name = "LetterTemplate.findByModuleTypeRecordStatus", query = "select o from LetterTemplate o where o.moduleType = :moduleType and o.recordStatus = :recordStatus")
})
@Table(name = "LETTER_TEMPLATE")
public class LetterTemplate extends VersionEntity {

	private static final long serialVersionUID = -5251381253658426661L;

	@Id
    @Column(name="CODE", length = 10, nullable=false)
	private String code;
	
	@Column(name="NAME", length = 100)
	private String name;
	
	@Column(name="SUBJECT", length = 200)
	private String subject;
	
	@Lob
	@Column(name="CONTENT")
	private String content;
	
	@Column(name="MODULE_TYPE", length = 1)
	private String moduleType;

	@Column(name = "JAXB_FLAG")
    private Boolean jaxbFlag;
	
	@Converter(name = "LetterTemplate.recordStatus", converterClass = RecordStatus.Converter.class)
	@Convert("LetterTemplate.recordStatus")
	@Column(name="RECORD_STATUS", length = 1, nullable = false)
	private RecordStatus recordStatus;

	@Transient
	private String[] toList;
	
	@Transient
	private String[] ccList;
	
	@Transient
	private String[] bccList;
	
	@Transient
	private List<CoaFileFolder> coaFileFolderList;
	
	@Transient
	private List<SampleTestFile> sampleTestFileList;
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public void setToList(String[] toList) {
		if (toList != null){
			this.toList = toList.clone();
		}
	}
	
	public String[] getToList() {
		return (toList != null && toList.length >0) ? toList.clone() : new String[0];
	}
	
	public void setCcList(String[] ccList) {
		if (ccList != null){ 
			this.ccList = ccList.clone();
		}
	}
	
	public String[] getCcList() {
		return (ccList != null && ccList.length>0) ? ccList.clone() : new String[0];
	}

	public void setBccList(String[] bccList) {
		if (bccList != null){
			this.bccList = bccList.clone();
		}
	}
	
	public String[] getBccList() {
		return (bccList != null && bccList.length>0) ? bccList.clone() : new String[0];
	}

	public void setCoaFileFolderList(List<CoaFileFolder> coaFileFolderList) {
		this.coaFileFolderList = coaFileFolderList;
	}

	public List<CoaFileFolder> getCoaFileFolderList() {
		return coaFileFolderList;
	}
	
	public List<SampleTestFile> getSampleTestFileList() {
		return sampleTestFileList;
	}

	public void setSampleTestFileList(List<SampleTestFile> sampleTestFileList) {
		this.sampleTestFileList = sampleTestFileList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public void setJaxbFlag(Boolean jaxbFlag) {
		this.jaxbFlag = jaxbFlag;
	}

	public Boolean getJaxbFlag() {
		return jaxbFlag;
	}

}
