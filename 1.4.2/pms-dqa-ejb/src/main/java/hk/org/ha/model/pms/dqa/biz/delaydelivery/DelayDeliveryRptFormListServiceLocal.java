package hk.org.ha.model.pms.dqa.biz.delaydelivery;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormCriteria;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;

import javax.ejb.Local;

@Local
public interface DelayDeliveryRptFormListServiceLocal {
	
	List<DelayDeliveryRptFormContactData> retrieveDelayDeliveryRptFormContactList(DelayDeliveryRptForm delayDeliveryRptForm);
	
	List<DelayDeliveryRptForm> retrieveDraftDelayDeliveryRptFormList(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn );
	
	List<DelayDeliveryRptForm> retrieveDelayDeliveryRptFormList(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn );
	
	List<DelayDeliveryRptFormPoData> retrieveDelayDeliveryRptFormPoList(DelayDeliveryRptForm delayDeliveryRptForm, String baseUnit);
	
	List<DelayDeliveryRptFormContactData> retrieveDelayDeliveryRptFormContactQuickAddList(Institution institution);
	
	DelayDeliveryRptFormContactData retrieveDelayDeliveryRptFormContact(DelayDeliveryRptFormContactData delayDeliveryRptFormContactData);
	
	DelayDeliveryRptFormData retrieveDelayDeliveryRptFormData(DelayDeliveryRptForm delayDeliveryRptForm);
	
	void destroy();
}
