package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MicroBioTestParam implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	MicrobialTestforNonSterileProductsOnly("MTP1", "Microbial Test for Non-sterile Products ONLY"),
	MicrobialTestForNonSterileProductsAndValidation("MTP2", "Microbial Test for Non-sterile Products and VALIDATION"),
	SterilityTest("MTP3", "Sterility Test for Sterile Products");
	
    private final String dataValue;
    private final String displayValue;
        
    MicroBioTestParam(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<MicroBioTestParam> {

		private static final long serialVersionUID = -1623620317837922548L;

		@Override
    	public Class<MicroBioTestParam> getEnumClass() {
    		return MicroBioTestParam.class;
    	}
    }
}
