package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import javax.ejb.Local;

@Local
public interface CoaBatchServiceLocal {
	
	boolean retrieveCoaBatchByCoaBatch( CoaBatch coaBatchIn , CoaStatus status);
	
	boolean retrieveCoaBatchByCoaBatchId( Long coaBatchId , CoaStatus status );
	
	void createCoaBatch(CoaBatch coaBatchIn);
	
	void updateCoaBatch();
	
	void updateCoaBatchStatus( CoaBatch coaBatch, CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus );
	
	void updateCoaBatchVerStatus( CoaBatch coaBatchIn, CoaStatus coaStatus, DiscrepancyStatus discrepancyStatus );
	
	void updateCoaBatchForRevert( CoaBatchVer coaBatchVer );
	
	void updateCoaBatchForFailCoa( CoaBatch coaBatchIn );
	
	void updateCoaBatchForErpInterface( CoaBatch coaBatchIn );
	
	void createEmailLogForCoa( CoaBatch coaBatchIn, final LetterTemplate letterTemplate, EmailType emailType );
	
	void deleteCoaBatch( CoaBatch coaBatchIn );
	
	boolean isDuplicated();
	
	String getErrorCode();

	void destroy(); 
}
