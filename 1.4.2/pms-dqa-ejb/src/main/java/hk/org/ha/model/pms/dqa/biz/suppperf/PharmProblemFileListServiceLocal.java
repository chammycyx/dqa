package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;

import javax.ejb.Local;

@Local
public interface PharmProblemFileListServiceLocal {
	
	void retrievePharmProblemFileListByQaProblem(QaProblem qaProblemIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
