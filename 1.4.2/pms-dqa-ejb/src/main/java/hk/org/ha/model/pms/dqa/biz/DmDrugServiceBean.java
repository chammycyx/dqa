package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugServiceBean implements DmDrugServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmDrugCacherInf dmDrugCacher;

	@Out(required = false)
	private DmDrug dmDrug;

	private boolean drugItemExist;
	
	public DmDrug retrieveDmDrug(String itemCode)
	{
		logger.debug("retrieveDmDrug #0", itemCode);
		
		drugItemExist = false;		
		DmDrug dmDrugFind = dmDrugCacher.getDrugByItemCode(itemCode);
		if (dmDrugFind != null) {
			
			drugItemExist = true;
		}
		logger.debug("retrieveDmDrug result #0", drugItemExist);
		return dmDrugFind;
		
	}
	
	public void retrieveDmDrugByItemCode(String itemCode)
	{
		logger.debug("retrieveDmDrugByItemCode #0", itemCode);

		dmDrug = retrieveDmDrug(itemCode);

		logger.debug("retrieveDmDrugByItemCode result #0", drugItemExist);
	}
	
	@Restrict("#{!identity.loggedIn}")	
	public DmDrug retrieveDmDrugByItemCodeForErp( String itemCode ) {
		logger.info( "retrieveDrugItemForErp #0", itemCode );		
		return retrieveDmDrug(itemCode);
	}
	
	
	
	public boolean isDrugItemExist() {
		return drugItemExist;
	}

	@Remove
	public void destroy() 
	{
		if (dmDrug != null) {
			dmDrug = null;
		}
	}
}
