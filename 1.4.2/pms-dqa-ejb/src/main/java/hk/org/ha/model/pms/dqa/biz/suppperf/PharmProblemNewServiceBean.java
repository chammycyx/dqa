package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;

import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.HasFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmProblemNewService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class PharmProblemNewServiceBean implements PharmProblemNewServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	//@Out(required = false)
	//private PharmProblem pharmProblemNew;
	
	@In(create =true)
	private QaProblemServiceLocal qaProblemService;
	
	private boolean success;
	
	private String errorCode;
		
	public void updateNewPharmProblemWithQaProblem(PharmProblem pharmProblemIn, QaProblem qaProblemIn, String caseType, List<QaBatchNum> qaBatchNumListIn){
		logger.debug("updateNewPharmProblemWithQaProblem");
		
		success = false;
		errorCode = validatePharmProblemCaseProblem(pharmProblemIn, qaProblemIn);
		
		if(errorCode==null)
		{
			success = true;
		}
		
		
		if(success)
		{
			//Check if the original case number of pharm problem has been changed to a new one or not
			checkIfCaseNumberChangeAndUpdateOldCase(pharmProblemIn, qaProblemIn);
			
			if(caseType.equals("NewCase"))
			{
				if(pharmProblemIn.getProblemStatus()==ProblemStatus.Active)
				{
					qaProblemIn.setCaseStatus(CaseStatus.Active);
				}
				else if(pharmProblemIn.getProblemStatus()==ProblemStatus.Closed)
				{
					qaProblemIn.setCaseStatus(CaseStatus.Closed);
				}
				else if(pharmProblemIn.getProblemStatus()==ProblemStatus.Suspended)
				{
					qaProblemIn.setCaseStatus(CaseStatus.Suspended);
				}
				
				qaProblemIn.setSimilarProblem(HasFlag.HasNot);
				qaProblemIn.setRecordStatus(RecordStatus.Active);
				qaProblemIn.setProblemHeader(pharmProblemIn.getProblemHeader());
				
				createNewQaProblem(pharmProblemIn, qaProblemIn);
			}
			else if(caseType.equals("OldCase"))
			{
				int noOfPharmProblem=0;
				for (PharmProblem pc : qaProblemIn.getPharmProblemList())
				{
					if(pc.getRecordStatus()==RecordStatus.Active && !pc.getPharmProblemId().equals(pharmProblemIn.getPharmProblemId()))
					{
						noOfPharmProblem++;
					}
				}
				
				if(noOfPharmProblem>0)
				{
					qaProblemIn.setSimilarProblem(HasFlag.Has);
				}
				else
				{
					qaProblemIn.setSimilarProblem(HasFlag.HasNot);
				}
				
				if(qaProblemIn.getCaseStatus()==CaseStatus.Active)
				{
					pharmProblemIn.setProblemStatus(ProblemStatus.Active);
				}
				else if(qaProblemIn.getCaseStatus()==CaseStatus.Closed)
				{
					pharmProblemIn.setProblemStatus(ProblemStatus.Closed);
				}
				else if(qaProblemIn.getCaseStatus()==CaseStatus.Suspended)
				{
					pharmProblemIn.setProblemStatus(ProblemStatus.Suspended);
				}
				
				pharmProblemIn.setQaProblem(qaProblemIn);
	
				//save qaProblem
				qaProblemService.saveQaProblem(qaProblemIn);
				
				//save qaBatchNum
				if(qaBatchNumListIn!=null)
				{
					for(QaBatchNum qbn : qaBatchNumListIn)
					{
						qbn.setQaProblem(qaProblemIn);
						em.persist(qbn);
						em.flush();
					}
				}
				
				if(pharmProblemIn.getSendCollectSample()!=null)
				{
					em.merge(pharmProblemIn.getSendCollectSample());
					em.flush();
				}
				
				//save pharmProblem
				em.merge(pharmProblemIn);
				em.flush();
				
			}
		}
	}
	
	public void checkIfCaseNumberChangeAndUpdateOldCase(PharmProblem pharmProblemIn, QaProblem qaProblemIn){
		logger.debug("checkIfCaseNumberChangeAndUpdateOldCase");
		
		
		if(pharmProblemIn.getQaProblem()!=null)
		{
			if(!pharmProblemIn.getQaProblem().getCaseNum().equals(qaProblemIn.getCaseNum()))
			{
				QaProblem qaProblemFind = qaProblemService.getQaProblemByCaseNum(pharmProblemIn.getQaProblem().getCaseNum());
				if(qaProblemFind!=null)
				{
					int noOfPharmProblem=0;
					
					List<QaBatchNum> qbnList = qaProblemFind.getQaBatchNumList();
					QaProblemNature qcn = qaProblemFind.getQaProblemNature();
					
					
					for (PharmProblem pc : qaProblemFind.getPharmProblemList())
					{
						if(pc.getRecordStatus()==RecordStatus.Active && !(pc.getPharmProblemId().equals(pharmProblemIn.getPharmProblemId())))
						{
							noOfPharmProblem++;
						}
					}

					if(noOfPharmProblem>1)
					{
						
						qaProblemFind.setSimilarProblem(HasFlag.Has);
					}
					else
					{
						qaProblemFind.setSimilarProblem(HasFlag.HasNot);
					}
					
					em.merge(qaProblemFind);
					em.flush();
				
					if(noOfPharmProblem==0)
					{
						
						for (QaBatchNum qbn : qbnList)
						{
							qbn.getBatchSuspList();
							if(qbn.getBatchSuspList()==null)
							{
								qbn.setQaProblem(null);
								qbn=em.merge(qbn);
								em.remove(qbn);
								em.flush();
							}
							else if(qbn.getBatchSuspList().size()==0)
							{
								qbn.setBatchSuspList(null);
								qbn.setQaProblem(null);
								qbn=em.merge(qbn);
								em.remove(qbn);
								em.flush();
							}
						}
						
						qcn.setProblemDetail(null);
						qcn.setPhysicalDefectFlag(null);
						qcn.setPackagingDefectFlag(null);
						qcn.setAppearDiscFlag(null);
						qcn.setForeignMatterFlag(null);
						qcn.setOtherFlag(null);
						em.merge(qcn);
						em.flush();

					}
				}
			}
		}
	}
	
	void createNewQaProblem(PharmProblem pharmProblemIn, QaProblem qaProblemIn){
		logger.debug("createNewQaProblem");
		
		List<QaBatchNum> qaBatchNumListInsert = new ArrayList();
		QaProblemNature qaProblemNatureInsert = new QaProblemNature();
		
		if(pharmProblemIn.getPharmBatchNumList()!=null)
		{
			for(PharmBatchNum pbn : pharmProblemIn.getPharmBatchNumList())
			{
				QaBatchNum qaBatchNumInsert = new QaBatchNum();
				qaBatchNumInsert.setBatchNum(pbn.getBatchNum());
				qaBatchNumInsert.setQaProblem(qaProblemIn);
				qaBatchNumListInsert.add(qaBatchNumInsert);
			}
		}
		
		qaProblemIn.setSupplier(pharmProblemIn.getContract().getSupplier());
		qaProblemIn.setManufacturer(pharmProblemIn.getContract().getManufacturer());
		qaProblemIn.setPharmCompany(pharmProblemIn.getContract().getPharmCompany());
		
		qaProblemNatureInsert.setProblemDetail(pharmProblemIn.getProblemDetail());
		
		qaProblemIn.setQaProblemNature(qaProblemNatureInsert);
		
		em.persist(qaProblemNatureInsert);
		em.flush();
		
		em.persist(qaProblemIn);
		em.flush();
		
		if(qaBatchNumListInsert!=null && qaBatchNumListInsert.size()>0)
		{
			for (QaBatchNum qbn : qaBatchNumListInsert)
			{
				em.persist(qbn);
				em.flush();
			}
		}
		
		pharmProblemIn.setQaProblem(qaProblemIn);
		
		if(pharmProblemIn.getSendCollectSample()!=null)
		{
			em.merge(pharmProblemIn.getSendCollectSample());
			em.flush();
		}
		
		em.merge(pharmProblemIn);
		em.flush();
		
		
	}
	
	public String validatePharmProblemCaseProblem(PharmProblem pharmProblemIn, QaProblem qaProblemIn){
		logger.debug("validatePharmProblemCaseProblem");
		if(qaProblemIn.getQaProblemId()!=null)
		{
			if(!pharmProblemIn.getProblemHeader().getItemCode().equals(qaProblemIn.getProblemHeader().getItemCode()))
			{
				return "0115";
			}
			else if(!pharmProblemIn.getContract().getSupplier().getSupplierCode().equals(qaProblemIn.getSupplier().getSupplierCode()))
			{
				return "0115";
			}
			else if(!pharmProblemIn.getContract().getManufacturer().getCompanyCode().equals(qaProblemIn.getManufacturer().getCompanyCode()))
			{
				return "0115";
			}
			else if(!pharmProblemIn.getContract().getPharmCompany().getCompanyCode().equals(qaProblemIn.getPharmCompany().getCompanyCode()))
			{
				return "0115";
			}
			else{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public PharmProblem getLazyPharmProblem(PharmProblem pharmProblemLazy){
		// lazy
		pharmProblemLazy.getPharmProblemCountryList();
		pharmProblemLazy.getPharmProblemCountryList().size();
		pharmProblemLazy.getPharmBatchNumList();
		pharmProblemLazy.getPharmBatchNumList().size();
		pharmProblemLazy.getPharmProblemNatureList();
		pharmProblemLazy.getPharmProblemNatureList().size();
		pharmProblemLazy.getPharmProblemFileList();
		pharmProblemLazy.getPharmProblemFileList().size();
		pharmProblemLazy.getSendCollectSample();
		pharmProblemLazy.getInstitution();
		pharmProblemLazy.getProblemHeader();
		pharmProblemLazy.getQaProblem();
		pharmProblemLazy.getContract();
		if(pharmProblemLazy.getContract()!=null)
		{
			pharmProblemLazy.getContract().getSupplier();
			pharmProblemLazy.getContract().getManufacturer();
			pharmProblemLazy.getContract().getPharmCompany();
			
			if(pharmProblemLazy.getContract().getSupplier()!=null)
			{
				pharmProblemLazy.getContract().getSupplier().getContact();
			}
			if(pharmProblemLazy.getContract().getManufacturer()!=null)
			{
				pharmProblemLazy.getContract().getManufacturer().getContact();
			}
			if(pharmProblemLazy.getContract().getPharmCompany()!=null)
			{
				pharmProblemLazy.getContract().getPharmCompany().getContact();
			}
		}
		
		if(pharmProblemLazy.getSendCollectSample()!=null)
		{
			pharmProblemLazy.getSendCollectSample().getSendToQa();
			pharmProblemLazy.getSendCollectSample().getSendToSupp();
		}
		
		return pharmProblemLazy;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
		
	@Remove
	public void destroy(){
		
	}
}
