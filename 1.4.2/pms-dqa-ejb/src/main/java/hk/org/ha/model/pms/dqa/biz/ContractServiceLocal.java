package hk.org.ha.model.pms.dqa.biz;


import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;

import javax.ejb.Local;


@Local
public interface ContractServiceLocal {
		
	void retrieveContractForCoaLastestLiveContract(String contractNum, String itemCode);
				
	void retrieveContractForSampleTest(String contractNum, String contractSuffix, OrderType orderType, String itemCode);
	
	Contract retrieveContractForSampleTestValidation(String contractNum, String contractSuffix, OrderType orderType, String itemCode, ModuleType moduleType);
	
	Contract findContractByItemContractNumContractSuffixContractTypeModuleType(
																			String itemCode,
																			String contractNum,
																			String contractSuffix,
																			String contractType, 
																			ModuleType moduleType);
	
	Contract copyContract(Contract contractIn, ModuleType moduleTypeIn);
	
	Contract getLazyContract(Contract contractLazy);
	
	boolean isSuccess();
	
    void destroy();
}
