package hk.org.ha.model.pms.dqa.biz.suppperf;

import javax.ejb.Local;

@Local
public interface QaProblemListForLastFiveCaseNumServiceLocal {
	
	void retrieveQaProblemListWithLastFiveCaseNum();
	
	void destroy();

}
