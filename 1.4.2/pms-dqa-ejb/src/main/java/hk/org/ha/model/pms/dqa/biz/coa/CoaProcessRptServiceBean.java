package hk.org.ha.model.pms.dqa.biz.coa;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptData;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaProcessRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaProcessRptServiceBean implements CoaProcessRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String REPORT_NAME = "COA Process Report";	
	private static final String DATE_FORMAT = "dd-MMM-yyyy";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy HH:mm";
	
	private List<CoaProcessRptData> coaProcessRptList;
	
	private String reportParameters;
	
	private String hashKey;

	private SimpleDateFormat sdf;

	@SuppressWarnings("unchecked")
	private List retrieveCoaBatchListData(CoaProcessRptCriteria coaProcessRptCriteria){
		logger.debug("getCoaBatchListData");
		
		ReportGroupByType groupType = coaProcessRptCriteria.getGroupType();
		String supplierCodeIn = coaProcessRptCriteria.getSupplierCode();
		String itemCodeIn = coaProcessRptCriteria.getItemCode();
		Date startDate = coaProcessRptCriteria.getStartDate();

		Calendar c = Calendar.getInstance(); 
		c.setTime(coaProcessRptCriteria.getEndDate());
		c.add(Calendar.DATE, 1);  
		Date sqlEndDate = c.getTime();
	
		String sqlSelect ="Select ";
		String sqlStr ="from CoaBatch o where o.createDate >=:startDate and o.createDate < :endDate and (o.coaItem.contract.itemCode = :itemCode or :itemCode is null) " +
			"and (o.coaItem.contract.supplier.supplierCode = :supplierCode or 'All' = :supplierCode ) ";
		String sqlGroup ="group by ";
		String sqlOrder ="order by ";
	
		if (groupType == ReportGroupByType.SupplierCode){
			sqlSelect += "o.coaItem.contract.supplier.supplierCode ";
			sqlGroup += "o.coaItem.contract.supplier.supplierCode ";
			sqlOrder += "o.coaItem.contract.supplier.supplierCode ";

		}else if (groupType == ReportGroupByType.ItemCode){
			sqlSelect += "o.coaItem.contract.itemCode ";
			sqlGroup += "o.coaItem.contract.itemCode ";
			sqlOrder += "o.coaItem.contract.itemCode ";
			
		}else {
			sqlSelect += " o.coaItem.contract.supplier.supplierCode, o.coaItem.contract.itemCode ";
			sqlGroup += " o.coaItem.contract.supplier.supplierCode, o.coaItem.contract.itemCode ";
			sqlOrder += " o.coaItem.contract.supplier.supplierCode, o.coaItem.contract.itemCode ";
		}
		sqlSelect += ", o.coaBatchVer.coaStatus, count(o.coaBatchVer) ";
		sqlGroup += " , o.coaBatchVer.coaStatus ";
		sqlOrder += " , o.coaBatchVer.coaStatus ";

		List reportList = em.createQuery(sqlSelect + sqlStr +  sqlGroup + sqlOrder)
		.setParameter("startDate", startDate, TemporalType.DATE)
		.setParameter("endDate", sqlEndDate, TemporalType.DATE)
		.setParameter("itemCode", itemCodeIn)
		.setParameter("supplierCode", supplierCodeIn)
		.getResultList();
		
		return reportList;
	}
	
	private void setReportParameters(CoaProcessRptCriteria coaProcessRptCriteria){
		ReportGroupByType groupType = coaProcessRptCriteria.getGroupType();
		String supplierCodeIn = coaProcessRptCriteria.getSupplierCode();
		String itemCodeIn = coaProcessRptCriteria.getItemCode();
		Date startDate = coaProcessRptCriteria.getStartDate();
		Date endDate = coaProcessRptCriteria.getEndDate();
	
		if (groupType == ReportGroupByType.SupplierCode){
		
			reportParameters = "Supplier Code="+ supplierCodeIn;
		}else if (groupType == ReportGroupByType.ItemCode){

			reportParameters = "Item Code=" +  (itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn);
		}else {
			reportParameters = "Supplier Code="+ supplierCodeIn 
				+ ", Item Code=" +  (itemCodeIn==null||itemCodeIn.trim().equals("")?"All":itemCodeIn);
		}
		sdf = new SimpleDateFormat(DATE_FORMAT);
		String startDateStr = (sdf.format(startDate));
		String endDateStr = (sdf.format(endDate));
		
		reportParameters += ", COA Batch Create Date=" + startDateStr+ " to " +endDateStr +", Group by="+groupType.getDisplayValue();
	}
	
	private Param constructParam(ReportGroupByType groupType, Object o){
		
		Param param; 
		if (groupType == ReportGroupByType.SupplierCode){
			param = new Param(Array.get(o, 0).toString(),"", 1, 2 );	
		}else if (groupType == ReportGroupByType.ItemCode){
			param = new Param("", Array.get(o, 0).toString(), 1, 2 );			
		}else {	
			param = new Param(Array.get(o, 0).toString(), Array.get(o, 1).toString(),  2, 3 );
		}
		return param;
	}
	
	
	private String retrieveReportHashKey(ReportGroupByType groupType, Param param ){
		if (groupType == ReportGroupByType.SupplierCode){
			return param.getSupplierCode();
		}else if (groupType == ReportGroupByType.ItemCode){
			return param.getItemCode();
		}else {
			return param.getItemCode() + param.getSupplierCode();
		}
	}

	
	@SuppressWarnings("unchecked")
	public void retrieveCoaProcessList(CoaProcessRptCriteria coaProcessRptCriteria){
		logger.debug("retrieveCoaProcessList");
		
		coaProcessRptList = new ArrayList<CoaProcessRptData>();
		
		List reportList = retrieveCoaBatchListData(coaProcessRptCriteria);
		
		setReportParameters(coaProcessRptCriteria);
		
		ReportGroupByType groupType = coaProcessRptCriteria.getGroupType();
		
		if (reportList !=null  ){
			HashMap<String , CoaProcessRptData> coaProcessHM = new HashMap<String , CoaProcessRptData>();
			CoaProcessRptData cp;
			CoaStatus coaStatus;
			int freq;

			Param param ;
			
			for (Object o : reportList){
				if (o.getClass().isArray()){

					param = constructParam(groupType, o);
					hashKey = retrieveReportHashKey(groupType, param);

					if (coaProcessHM.containsKey(hashKey)){
						cp = (CoaProcessRptData)coaProcessHM.get(hashKey);
					}else{
						cp = new CoaProcessRptData();
						coaProcessHM.put(hashKey, cp);
						coaProcessRptList.add(cp);
					} 
						cp.setSupplierCode(param.getSupplierCode());
						cp.setItemCode(param.getItemCode());

					if (Array.get(o, param.getStatusIndex()) !=null){
						coaStatus = (CoaStatus)Array.get(o, param.getStatusIndex());

						freq = ((Long)Array.get(o, param.getFreqIndex())).intValue();
						if (coaStatus == CoaStatus.Pass){
							cp.setPass(freq);
						}else if (coaStatus == CoaStatus.Fail){
							cp.setFail(freq);
						}else if (coaStatus == CoaStatus.VerificationInProgress){
							cp.setVerificationInProgress(freq);
						}else if (coaStatus == CoaStatus.DiscrepancyClarificationInProgress){
							cp.setDiscrepancy(freq);
						}
					}
				}
			}
		}
		logger.debug("coaProcessRptList report list size : #0", coaProcessRptList.size());
	}
	
	public List<CoaProcessRptData> getCoaProcessRptList(){
		return coaProcessRptList;
	}


	public String getReportName() {
		return REPORT_NAME;
	}

	public String getReportParameters() {
		return reportParameters;
	}

	public String getRetrieveDate() {
		String retrieveDate;
		sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
		retrieveDate = (sdf.format(new Date()));
		return retrieveDate;
	}

	@Remove
	public void destroy(){
		coaProcessRptList = null;
	}
	
	public static class Param
	{
		private String supplierCode =""; 
		private String itemCode ="";
		private int statusIndex = 1;
		private int freqIndex = 2;
	
		public Param() {
			super();
		}
		
		public Param(String supplierCode, String itemCode, int statusIndex,
				int freqIndex) {
			super();
			this.itemCode = itemCode;
			this.supplierCode = supplierCode;
			this.statusIndex = statusIndex;
			this.freqIndex = freqIndex;
		}


		public String getSupplierCode() {
			return supplierCode;
		}
		
		public void setSupplierCode(String supplierCode) {
			this.supplierCode = supplierCode;
		}
		
		public String getItemCode() {
			return itemCode;
		}
		
		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}
		
		public int getStatusIndex() {
			return statusIndex;
		}
		
		public void setStatusIndex(int statusIndex) {
			this.statusIndex = statusIndex;
		}
		
		public int getFreqIndex() {
			return freqIndex;
		}

		public void setFreqIndex(int freqIndex) {
			this.freqIndex = freqIndex;
		} 
	}

}
