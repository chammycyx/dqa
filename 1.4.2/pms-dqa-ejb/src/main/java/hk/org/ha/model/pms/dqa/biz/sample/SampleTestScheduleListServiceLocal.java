package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.ContractLine;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestResultCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleTestResult;
import javax.ejb.Local;

@Local
public interface SampleTestScheduleListServiceLocal {
	void retrieveSampleTestScheduleListByCriteria(SampleTestScheduleCriteria sampleTestScheduleCriteria);
	
	void retrieveSampleTestScheduleListByRecordStatusScheduleStatus(RecordStatus recordStatus, ScheduleStatus scheduleStatus);
	
	void retrieveSampleTestScheduleListByTestResultCriteria(SampleTestResultCriteria sampleTestResultCriteria);
	
	void retrieveSampleTestScheduleListByEnquiryDetailCriteria(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria);
	
	void retrieveSampleTestScheduleListReport(Collection<Long> sampleTestScheduleKeyList);

	void retrieveSampleTestScheduleListByCriteriaReport(SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria);
	
	List<SampleTestSchedule> retrieveSampleTestScheduleListForOutStanding(String itemCode, String testCode);
	
	//void retrieveSampleTestScheduleListForCopy(Long scheduleId, String itemCode);
	
	void copySampleTestSchedule(Collection<SampleTestSchedule> sampleTestScheduleCopyList);
	
	List<SampleTestSchedule> retrieveSampleTestScheduleListForUpdateLastDocDate(String itemCode, String supplierCode, String manufCode, String pharmCode, Long scheduleId);
	
	List<SampleTestSchedule> retrieveSampleTestScheduleIdListForTestFreqMaint(RecordStatus recordStatus, ScheduleStatus scheduleStatus, Long frequencyId);
	
	//void updateSampleTestScheduleListForInstAssignment(Collection<SampleTestSchedule> sampleTestScheduleIAList);
	
	List<SampleTestSchedule> retrieveSampleTestScheduleListByContractLine(ContractLine contractLineIn);
	
	void resetSampleTestScheduleActionFlag(Calendar modifyDate);
	
	void updateSampleTestScheduleListForInstAssignment(List<SampleTestSchedule> sampleTestScheduleListIn);
	
	List<SampleTestScheduleTestResult> getSampleTestScheduleTestResultList();
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy(); 
}
