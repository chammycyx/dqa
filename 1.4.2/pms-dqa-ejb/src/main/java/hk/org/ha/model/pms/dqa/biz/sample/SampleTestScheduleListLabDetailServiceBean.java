package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;


import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleListLabDetailService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleListLabDetailServiceBean implements SampleTestScheduleListLabDetailServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private List<SampleTestSchedule> sampleTestScheduleDetailList;
	
	@In(create =true)
	private SampleMovementServiceLocal sampleMovementService;
	
	@In(create =true)
	private SampleTestScheduleFileServiceLocal sampleTestScheduleFileService;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;

	
	private static final String UNCHECKED = "unchecked";
//	private static final String RECORD_STATUS = "recordStatus";
//	private static final String SCHEDULE_STATUS = "scheduleStatus";
//	private static final String ITEM_CODE = "itemCode";
	private static final String O_SAMPLE_TEST= "o.sampleTest";
	private static final String O_CONTRACT = "o.contract";
	private static final String O_CONTRACT_SUPPLIER = "o.contract.supplier";
	private static final String O_CONTRACT_MANUFACTURER = "o.contract.manufacturer";
	private static final String O_CONTRACT_PHARMCOMPANY = "o.contract.pharmCompany";
	private static final String O_SAMPLE_ITEM = "o.sampleItem";
	private static final String O_SAMPLE_ITEM_RISK_LEVEL = "o.sampleItem.riskLevel";
	private static final String O_INSTITUTION = "o.institution";
//	private static final String O_PAYMENT = "o.payment";
	private static final String O_LAB = "o.lab";
	
	private boolean success;
	
	private String errorCode = null;
	
	
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNum(String labCode, String testCode, String labCollectNum){
		logger.debug("retrieveSampleTestScheduleListForLabCollectionByLabTestCollectNum" );
		errorCode=null;
		sampleTestScheduleDetailList= em.createNamedQuery("SampleTestSchedule.findByLabTestCollectNumScheduleStatusRecordStatus")
			.setParameter("labCode", labCode)
			.setParameter("testCode", testCode)
			.setParameter("labCollectNum", labCollectNum)
			.setParameter("scheduleStatus", ScheduleStatus.LabCollection)
			.setParameter("recordStatus", RecordStatus.Active)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.FETCH, O_INSTITUTION)
			.setHint(QueryHints.FETCH, O_LAB)
			.getResultList();
		
		if(sampleTestScheduleDetailList==null || sampleTestScheduleDetailList.size()==0)
		{
			errorCode = "0008";
		}
		else
		{
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleDetailList ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				sampleTestScheduleFind.getContract().getSupplier().getContact();
				sampleTestScheduleFind.getContract().getManufacturer();
				sampleTestScheduleFind.getContract().getManufacturer().getContact();
				sampleTestScheduleFind.getContract().getPharmCompany();
				sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public void retrieveSampleTestScheduleListForLabTestByLabTestOrderType(String labCode, String testCode, OrderType orderType){
		logger.debug("retrieveSampleTestScheduleListForLabTestByLabTestOrderType" );
		errorCode=null;
		sampleTestScheduleDetailList= em.createNamedQuery("SampleTestSchedule.findByLabTestOrderTypeScheduleStatusRecordStatus")
			.setParameter("labCode", labCode)
			.setParameter("testCode", testCode)
			.setParameter("orderType", orderType)
			.setParameter("scheduleStatus", ScheduleStatus.LabTest)
			.setParameter("recordStatus", RecordStatus.Active)
			.setHint(QueryHints.FETCH, O_SAMPLE_TEST)
			.setHint(QueryHints.FETCH, O_CONTRACT)
			.setHint(QueryHints.FETCH, O_CONTRACT_SUPPLIER)
			.setHint(QueryHints.FETCH, O_CONTRACT_MANUFACTURER)
			.setHint(QueryHints.FETCH, O_CONTRACT_PHARMCOMPANY)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM)
			.setHint(QueryHints.FETCH, O_SAMPLE_ITEM_RISK_LEVEL)
			.setHint(QueryHints.FETCH, O_INSTITUTION)
			.setHint(QueryHints.FETCH, O_LAB)
			.getResultList();
		
		if(sampleTestScheduleDetailList==null || sampleTestScheduleDetailList.size()==0)
		{
			errorCode = "0008";
		}
		else
		{
			for( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleDetailList ) {
				sampleTestScheduleFind.getPayment();
				sampleTestScheduleFind.getContract();
				sampleTestScheduleFind.getContract().getSupplier();
				sampleTestScheduleFind.getContract().getSupplier().getContact();
				sampleTestScheduleFind.getContract().getManufacturer();
				sampleTestScheduleFind.getContract().getManufacturer().getContact();
				sampleTestScheduleFind.getContract().getPharmCompany();
				sampleTestScheduleFind.getContract().getPharmCompany().getContact();
				sampleTestScheduleFind.getInstitution();
				sampleTestScheduleFind.getTestFrequency();
				sampleTestScheduleFind.getLab();
				sampleTestScheduleFind.getMicroBioTest();
				sampleTestScheduleFind.getChemicalAnalysis();
			}
		}
	}
	

	@SuppressWarnings(UNCHECKED)
	public void updateSampleTestScheduleForLabCollection(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("updateSampleTestScheduleForLabCollection" );
		success = false;
		errorCode = null;
		
		for ( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleListIn){
			sampleTestScheduleFind = getLazySampleTestSchedule(sampleTestScheduleFind);
			sampleTestScheduleFind.setScheduleStatus(ScheduleStatus.TestResult);
			em.merge(sampleTestScheduleFind);
			em.flush();
			sampleMovementService.createSampleMovementForNormalSchedule(sampleTestScheduleFind, ScheduleStatus.LabCollection);
			sampleTestScheduleFileService.createSampleTestScheduleFileForTestResult(sampleTestScheduleFind);
		}
		
		success = true;	
	}
	
	
	@SuppressWarnings(UNCHECKED)
	public void updateSampleTestScheduleForLabTest(List<SampleTestSchedule> sampleTestScheduleListIn){
		logger.debug("updateSampleTestScheduleForLabTest" );
		success = false;
		errorCode = null;
		
		String labCollectNum = retrieveLabCollectNum();
		
		for ( SampleTestSchedule sampleTestScheduleFind:sampleTestScheduleListIn){
			sampleTestScheduleFind = getLazySampleTestSchedule(sampleTestScheduleFind);
			sampleTestScheduleFind.setScheduleStatus(ScheduleStatus.LabCollection);
			sampleTestScheduleFind.setLabCollectNum(labCollectNum);
			
			if(sampleTestScheduleFind.getSampleTest().getTestCode().equals("MICRO"))
			{
				em.merge(sampleTestScheduleFind.getMicroBioTest());
			}
			else if(sampleTestScheduleFind.getSampleTest().getTestCode().equals("CHEM"))
			{
				em.merge(sampleTestScheduleFind.getChemicalAnalysis());
			}
			em.merge(sampleTestScheduleFind);
			em.flush();
		}
		
		success = true;	
	}
	
	
	private String retrieveLabCollectNum(){
		logger.debug("....retrieveLabCollectNum");
		String scheduleNumStr = "";
		
		Integer labCollectNum = funcSeqNumService.retrieveNextFuncSeqNum("CN");

		if (labCollectNum!=null){
			NumberFormat numformat = new DecimalFormat("00000000");
			scheduleNumStr = numformat.format(labCollectNum.intValue());
		}
		scheduleNumStr = "CN" + scheduleNumStr;
		logger.debug("gen/ retreive labCollectNum : #0 ", scheduleNumStr);

		return scheduleNumStr;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}

	@Remove
	public void destroy() {
		if (sampleTestScheduleDetailList !=null){
			sampleTestScheduleDetailList = null;
		}
	}
}
