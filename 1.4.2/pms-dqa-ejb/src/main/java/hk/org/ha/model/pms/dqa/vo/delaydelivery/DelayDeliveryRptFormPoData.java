package hk.org.ha.model.pms.dqa.vo.delaydelivery;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DelayDeliveryRptFormPoData {
	private byte[] fileData;
	private FileItem fileItem;
	private String refUploadFileName;
	private Long delayDeliveryRptFormPoId;
	private Long poNum;
	private Date approveDate;
	private Long outstandQty;
	private Long orderQty;
	private Long quotationLeadTime;
	private Date supplierReplyDate;
	private String baseUnit;
	private String packSize;
	
	public byte[] getFileData() {
		return (fileData!=null)?(fileData.clone()):(new byte[0]);
	}
	public void setFileData(byte[] fileData) {
		if(fileData!=null)
		{
			this.fileData = fileData.clone();
		}
	}
	public FileItem getFileItem() {
		return fileItem;
	}
	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}
	public String getRefUploadFileName() {
		return refUploadFileName;
	}
	public void setRefUploadFileName(String refUploadFileName) {
		this.refUploadFileName = refUploadFileName;
	}
	
	public Long getPoNum() {
		return poNum;
	}
	public void setPoNum(Long poNum) {
		this.poNum = poNum;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public Long getOutstandQty() {
		return outstandQty;
	}
	public void setOutstandQty(Long outstandQty) {
		this.outstandQty = outstandQty;
	}
	public Long getQuotationLeadTime() {
		return quotationLeadTime;
	}
	public void setQuotationLeadTime(Long quotationLeadTime) {
		this.quotationLeadTime = quotationLeadTime;
	}
	public Long getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(Long orderQty) {
		this.orderQty = orderQty;
	}
	public Date getSupplierReplyDate() {
		return supplierReplyDate;
	}
	public void setSupplierReplyDate(Date supplierReplyDate) {
		this.supplierReplyDate = supplierReplyDate;
	}
	public Long getDelayDeliveryRptFormPoId() {
		return delayDeliveryRptFormPoId;
	}
	public void setDelayDeliveryRptFormPoId(Long delayDeliveryRptFormPoId) {
		this.delayDeliveryRptFormPoId = delayDeliveryRptFormPoId;
	}
	public String getBaseUnit() {
		return baseUnit;
	}
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	public String getPackSize() {
		return packSize;
	}
	public void setPackSize(String packSize) {
		this.packSize = packSize;
	}
	
}
