package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxFinalHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxInitHosp;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemCountry;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRptCriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("auditRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AuditRptServiceBean implements AuditRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	private List<AuditRpt> auditRptList;

	@Override
	public List<AuditRpt> getAuditRptList() {
		// TODO Auto-generated method stub
		return auditRptList;
	}
	
	@SuppressWarnings("unchecked")	
	public List<AuditRpt> retrieveAuditRpt(AuditRptCriteria auditRptCriteria) {
		logger.debug("retrieveAuditListData");

		Date startDate = auditRptCriteria.getStartDate();
		Date endDate = auditRptCriteria.getEndDate();
		auditRptList = new ArrayList<AuditRpt>();
		List<PharmProblem> pharmProblemResultList;
		
		if(StringUtils.equals(auditRptCriteria.getInstitutionCode(),"*")){
			pharmProblemResultList = 
				em.createNamedQuery("PharmProblem.findByProblemDate")
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				
				.setHint(QueryHints.FETCH, "o.qaProblem")
				.setHint(QueryHints.BATCH, "o.qaProblem.eventLogList")
				.setHint(QueryHints.BATCH, "o.qaProblem.qaBatchNumList")
				.setHint(QueryHints.BATCH, "o.pharmProblemNatureList")
				.setHint(QueryHints.BATCH, "o.pharmProblemCountryList")
				.getResultList();
		}else{
			
			pharmProblemResultList = 
				em.createNamedQuery("PharmProblem.findByProblemDateAndInstitution")
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.setParameter("institutionCode", auditRptCriteria.getInstitutionCode())
				
				.setHint(QueryHints.FETCH, "o.qaProblem")
				.setHint(QueryHints.BATCH, "o.qaProblem.eventLogList")
				.setHint(QueryHints.BATCH, "o.qaProblem.qaBatchNumList")
				.setHint(QueryHints.BATCH, "o.pharmProblemNatureList")
				.setHint(QueryHints.BATCH, "o.pharmProblemCountryList")
				.getResultList();
			
		}

		for (PharmProblem p : pharmProblemResultList){
			AuditRpt a = new AuditRpt();
			a.setCaseNum(p.getQaProblem().getCaseNum());
			a.setInstitutionCode(p.getInstitution().getInstitutionCode());
			a.setInstitutionName(p.getInstitution().getInstitutionName());
			a.setDepartment("");
			a.setReportedBy(p.getProblemByType().getDisplayValue());
			a.setReportDate(p.getProblemDate());
			a.setItemCode(p.getContract().getItemCode());
			a.setItemDescription(p.getContract().getDmDrug().getFullDrugDesc());
			a.setQuantityWithProblem(p.getAffectQty());
			a.setSupplierCode(p.getContract().getSupplier().getSupplierCode());
			a.setSupplierName(p.getContract().getSupplier().getSupplierName());
			a.setManufacturerCode(p.getContract().getManufacturer().getCompanyCode());
			a.setManufacturerName(p.getContract().getManufacturer().getCompanyName());
			a.setPharmCompanyCode(p.getContract().getPharmCompany().getCompanyCode());
			a.setPharmCompanyName(p.getContract().getPharmCompany().getCompanyName());
			
			
			StringBuffer batchNum = new StringBuffer();	

			ArrayList<QaBatchNum> qaBatchNumList = new ArrayList<QaBatchNum>();
			qaBatchNumList.addAll(p.getQaProblem().getQaBatchNumList());
			Collections.sort(qaBatchNumList, new QaBatchNumComparator());

			for (QaBatchNum b : qaBatchNumList){
				batchNum.append( b.getBatchNum() );			
				batchNum.append(", ");			
			}	
			if(!StringUtils.isEmpty(batchNum.toString())){
				a.setBatchNum(batchNum.substring(0, batchNum.length()-2));
			}
			StringBuffer category = new StringBuffer();	
			StringBuffer subCategory = new StringBuffer();
			ArrayList<PharmProblemNature> pharmProblemNatureList = new ArrayList<PharmProblemNature>();
			pharmProblemNatureList.addAll(p.getPharmProblemNatureList());
			Collections.sort(pharmProblemNatureList, new PharmProblemNatureComparator());
			HashSet<String> catDesc = new HashSet<String>();
			HashSet<String> subCatDesc = new HashSet<String>();

			for (PharmProblemNature n : pharmProblemNatureList){
				String descString = n.getProblemNatureSubCat().getProblemNatureCat().getCatDesc();
				if(!catDesc.contains(descString)){
					catDesc.add(descString);
					category.append(descString).append(", ");
				}
				descString = n.getProblemNatureSubCat().getSubCatDesc();
				if(!subCatDesc.contains(descString)){
					subCatDesc.add(descString);
					subCategory.append(descString).append(", ");
				}
			}

			if(!StringUtils.isEmpty(category.toString())){
				a.setCategory(category.substring(0, category.length()-2));
			}
			a.setNatureOfProblem(subCategory.substring(0, subCategory.length()-2));
			a.setProblemDetail(p.getProblemDetail());
			a.setHowDiscovered("");
			a.setAssessmentOfProblem("");
			a.setImmediateActionTaken("");
			
			
			if(p.getQaProblem() != null){
				if(p.getQaProblem().getClassification() != null && p.getQaProblem().getClassification().getClassificationCode()!= null){
					a.setRiskIndex(p.getQaProblem().getClassification().getClassificationCode().getDisplayValue());
				}else{
					a.setRiskIndex("");
				}
				
			}else{
				a.setRiskIndex("");
			}
			
			if(p.getQaProblem() != null){
				List<EventLog> eList = p.getQaProblem().getEventLogList();
				for (EventLog e : eList){
					if(e.getFaxDetail() != null){
						if(e.getFaxDetail().getFaxFinalHosp() != null){
							FaxFinalHosp f = e.getFaxDetail().getFaxFinalHosp();
	
							a.setInvestigationReport(f.getInvestRptFlag() != null ? f.getInvestRptFlag().getDataValue() :"");
							a.setNoFurtherActionRequiredFlag(f.getNoActionFlag() != null ? f.getNoActionFlag().getDataValue():"");
							a.setBatchCanBeReleasedFlag(f.getRelBatchFlag() != null? f.getRelBatchFlag().getDataValue():"");
							a.setStockReplacementFlag(f.getStkReplaceFlag() != null? f.getStkReplaceFlag().getDataValue():"");
							a.setOthers(f.getOtherDesc());
	
						}
						if(e.getFaxDetail().getFaxInitHosp() != null){
							FaxInitHosp i = e.getFaxDetail().getFaxInitHosp();
							a.setBatchSuspensionFlag(i.getBatchSuspFlag() != null? i.getBatchSuspFlag().getDataValue():"");
							a.setProductRecallFlag(i.getProductRecallFlag() != null?i.getProductRecallFlag().getDataValue():"");
						}
					}
	
				}
			}

			a.setCaseStatus(p.getQaProblem().getCaseStatus().getDisplayValue());
			
			a.setCaseCreateDate(p.getQaProblem().getCreateDate());
			a.setInvestigationReportDate(p.getQaProblem().getInvestigationReportDate());
			a.setCaseCloseDate(p.getQaProblem().getCaseCloseDate());

			StringBuffer countryCode = new StringBuffer();	
			StringBuffer countryName = new StringBuffer();

			ArrayList<PharmProblemCountry> pharmProblemCountryList = new ArrayList<PharmProblemCountry>();
			pharmProblemCountryList.addAll(p.getPharmProblemCountryList());
			Collections.sort(pharmProblemCountryList, new PharmProblemCountryComparator());


			for (PharmProblemCountry c : pharmProblemCountryList){

				if(c.getCountry().getRecordStatus().equals(RecordStatus.Active))
					countryCode.append( c.getCountry().getCountryCode() );			
				countryCode.append(", ");			
				countryName.append( c.getCountry().getCountryName() );			
				countryName.append(", ");		
			}	
			
			if(!StringUtils.isEmpty(countryCode.toString())){
				a.setCountryCode(countryCode.substring(0, countryCode.length()-2));
			}
			if(!StringUtils.isEmpty(countryName.toString())){
				a.setCountryName(countryName.substring(0, countryName.length()-2));
			}
			
			auditRptList.add(a);
		}

		logger.debug("retrieveAuditListData---end");


		return auditRptList;

	}

	@Remove
	public void destroy(){
		auditRptList = null;
	}

	public static class QaBatchNumComparator implements Comparator<QaBatchNum>,Serializable
	{
		private static final long serialVersionUID = 8032402230292096246L;

		public int compare(QaBatchNum c1, QaBatchNum c2){
			return c1.getBatchNum() .compareTo(c2.getBatchNum()  );
		}						
	}
	public static class PharmProblemCountryComparator implements Comparator<PharmProblemCountry>,Serializable
	{

		private static final long serialVersionUID = 8032402230292096246L;

		@Override
		public int compare(PharmProblemCountry o1, PharmProblemCountry o2) {
			return o1.getCountry().getCountryCode().compareTo(o2.getCountry().getCountryCode());

		}						
	}
	public static class PharmProblemNatureComparator implements Comparator<PharmProblemNature>,Serializable
	{
		private static final long serialVersionUID = 8032402230292096246L;

		public int compare(PharmProblemNature n1, PharmProblemNature n2){
			int order1 = n1.getProblemNatureSubCat().getProblemNatureCat().getProblemNatureParam().getDisplayOrder()
			.compareTo(n2.getProblemNatureSubCat().getProblemNatureCat().getProblemNatureParam().getDisplayOrder());
			if(order1 == 0){
				int order2 = n1.getProblemNatureSubCat().getProblemNatureCat().getDisplayOrder()
				.compareTo(n2.getProblemNatureSubCat().getProblemNatureCat().getDisplayOrder());
				if(order2 == 0){
					int order3 = n1.getProblemNatureSubCat().getDisplayOrder()
					.compareTo(n2.getProblemNatureSubCat().getDisplayOrder());
					return order3;
				}else return order2;
			}else return order1;

		}						
	}
}
