package hk.org.ha.model.pms.dqa.biz.suppperf;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFormData;

import javax.ejb.Local;

@Local
public interface PharmProblemDraftSaveServiceLocal {
		
	PharmProblemFormData createPharmProblemFormData();
	Contract constructContractSave(PharmProblem pharmProblemIn);
	boolean createPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean);
	boolean createUpdateDraftPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList);
	boolean updateToNullProblemStausPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
			Boolean sendEmailBoolean);
	PharmProblemFormData retrievePharmProblemFormData(PharmProblem pharmProblemIn);
	void destroy();
}
