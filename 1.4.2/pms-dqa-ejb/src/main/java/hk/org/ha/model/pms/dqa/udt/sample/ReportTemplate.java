package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ReportTemplate implements StringValuedEnum {
	
	
	ALL("ALL", "All"),
	TR("TR", "Test Result");
	
	
    private final String dataValue;
    private final String displayValue;
        
    ReportTemplate(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Result> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<Result> getEnumClass() {
    		return Result.class;
    	}
    }
}
