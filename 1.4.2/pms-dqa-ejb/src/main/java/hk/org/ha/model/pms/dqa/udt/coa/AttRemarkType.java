package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum AttRemarkType implements StringValuedEnum {
	
	NullValue("N", "Null value"),
	Pass("P", "Pass"),	
	InvalidBatchCoaFileName("1", "Invalid batch COA filename"),
	CoaExceptionReportFileCorrupt("2", "COA Exception Report file corrupt"),	
	InvalidFileContent("3", "Invalid content in COA Exception Report"),
	CoaItemNotExist("4", "COA item does not exist"),
	ContractOfCoaItemExpired("5", "Contract of COA item expired"),
	SameBatchCOAExist("6", " Same Batch COA existed"),
	UploadFileFailure("7", "Upload file failure");
	
		
	
    private final String dataValue;
    private final String displayValue;
        
    AttRemarkType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<AttRemarkType> {

		private static final long serialVersionUID = 6368919235452551382L;

		@Override
    	public Class<AttRemarkType> getEnumClass() {
    		return AttRemarkType.class;
    	}
    }
}
