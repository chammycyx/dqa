package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestFileCriteria;
import javax.ejb.Local;

@Local
public interface SampleTestFileListServiceLocal {
	
	void retrieveSampleTestFileList(SampleTestFileCriteria sampleTestScheduleDocumentCriteria);
	
	void retrieveSampleTestFileListForSchedConfirm(String itemCode, String supplierCode, String manufCode, String pharmCode );

	void retrieveSampleTestFileListBySampleTestFileCat(SampleTestFileCat fileCat);
	
	void retrieveSampleTestFileListBySampleTestFileCatContract(SampleTestFileCat fileCat, Contract contractIn);
	
	void retrieveSampleTestFileListBySampleTestSchedule(SampleTestSchedule sampleTestScheduleIn);
	
	void destroy();

}
