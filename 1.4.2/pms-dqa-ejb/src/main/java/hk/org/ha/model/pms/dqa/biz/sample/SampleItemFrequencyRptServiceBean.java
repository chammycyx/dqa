package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFrequency;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleItemFrequencyRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleItemFrequencyRptServiceBean implements SampleItemFrequencyRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private SampleTestListServiceLocal sampleTestListService;
	
	private static final String UNCHECKED = "unchecked";
	private static final String NA = "NA";
	private static final String ALL = "All";
	private static final String DATE_TIME_FORMAT = "yyyy/MM/dd";
	private static final String MICROBIOLOGY_TEST = "MICRO";
	private static final String CHEMICAL_ANAYLYSIS = "CHEM";
	
	private List<SampleItemFrequency> sampleItemFrequencyReportList;
	
	@SuppressWarnings(UNCHECKED)
	private List<SampleItem> retrieveSampleDataList(Collection<Long> sampleItemKeyList){
		
		List<SampleItem> dataList = em.createNamedQuery("SampleItem.findBySampleItemId")
			.setParameter("keyList",sampleItemKeyList )
			.setHint(QueryHints.BATCH, "o.exclusionTestList")
			.getResultList();
		
		for (SampleItem si : dataList){
    		si.getExclusionTestList().size();
    	}
		return dataList;
	}
	
	@SuppressWarnings(UNCHECKED)
	private List<TestFrequency> retrieveFreqDataList(){
		
		return em.createNamedQuery("TestFrequency.findAll")
		.setHint(QueryHints.BATCH, "o.riskLevel")
		.getResultList();
	}
	
	
    @SuppressWarnings(UNCHECKED)
	public void retrieveSampleItemFrequencyList(Collection<Long> sampleItemKeyList){
    	logger.debug("retrieveSampleItemFrequencyList");
    	SimpleDateFormat sdf;
    	Map hm = new HashMap();
    	String keyStr;
    	
    	List<TestFrequency> freqList = retrieveFreqDataList();
    	List<SampleItem> sampleItemList = retrieveSampleDataList(sampleItemKeyList);
    	sampleItemFrequencyReportList = new ArrayList<SampleItemFrequency>();
    	    	
    	if (sampleItemList.size() > 0){
	    	if (freqList.size() > 0){
	    		for (TestFrequency testFrequency :  freqList){
	    			String orderStr = (testFrequency.getOrderType()==null?ALL:testFrequency.getOrderType());
	    			String levelStr = (testFrequency.getRiskLevel()==null?ALL:testFrequency.getRiskLevel().getRiskLevelCode());
	    			keyStr = testFrequency.getSampleTest().getTestCode()+'-'+ orderStr +'-'+ levelStr;
			
	    			hm.put(keyStr, testFrequency.getFrequencyPerYear().getDisplayValue());
	    		}
	    	}
	    	
	    	SampleItemFrequency sampleItemFrequency;
	    	List<SampleTest> testList = sampleTestListService.retrieveSampleTestListForReport();
	    	
	    	for (SampleItem sampleItem : sampleItemList ){
		    	sampleItemFrequency = constructSampleItemFrequencyData(sampleItem, new Date(),
		    			 sampleItem.getExclusionTestList(), testList,  hm);
		    	
		    	if (sampleItemFrequency !=null){
		    		sampleItemFrequencyReportList.add(sampleItemFrequency);
		    	}
	    	} 	
    	}	
    } 
   
    
    @SuppressWarnings(UNCHECKED)
	private SampleItemFrequency constructSampleItemFrequencyData(SampleItem sampleItem, Date updateDate,
    		List<ExclusionTest> exList, List<SampleTest> testList, Map hm){
    	boolean exCHEM = false;
    	boolean exMICRO = false;
    	String keyAllOrderStr;
    	String keyAllLevelStr;
    	String keyStr;
    	String freq;
    	SampleItemFrequency sampleItemFrequency = new SampleItemFrequency();

    	sampleItemFrequency.setItemCode(sampleItem.getItemCode());
    	
    	if (sampleItem.getDmDrug() != null && sampleItem.getDmDrug().getFullDrugDesc() != null){
    		sampleItemFrequency.setItemDesc(sampleItem.getDmDrug().getFullDrugDesc());
    	}
    	
    	sampleItemFrequency.setValidTestQtyReq(sampleItem.getValidTestQtyReqDesc());
    	sampleItemFrequency.setUpdateDate(updateDate);
    	
    	for(ExclusionTest exTest : exList){

			if (exTest.getSampleTest().getTestCode().equals(CHEMICAL_ANAYLYSIS) ){
				sampleItemFrequency.setChemAnaylsisQtyReq(NA);
				exCHEM = true;
			}else if (exTest.getSampleTest().getTestCode().equals(MICROBIOLOGY_TEST)){
				sampleItemFrequency.setMicroBioTestQtyReq(NA);
				exMICRO = true;
			}
		}
		if (!exCHEM){
			sampleItemFrequency.setChemAnaylsisQtyReq(sampleItem.getChemAnalysisQtyReqDesc());
		}
		if (!exMICRO) {
			sampleItemFrequency.setMicroBioTestQtyReq(sampleItem.getMicroBioTestQtyReqDesc());
		}	
		for (SampleTest sampleTest : testList){		
			keyAllOrderStr = sampleTest.getTestCode() +"-"+ALL+"-"+ sampleItem.getRiskLevel().getRiskLevelCode();
			keyAllLevelStr = sampleTest.getTestCode() +'-'+ (sampleItem.getDmDrug()==null?"":(sampleItem.getDmDrug().getDmProcureInfo().getOrderType())) + "-"+ALL;		
			keyStr = sampleTest.getTestCode() +'-'+ (sampleItem.getDmDrug()==null?"":(sampleItem.getDmDrug().getDmProcureInfo().getOrderType())) + '-'+ sampleItem.getRiskLevel().getRiskLevelCode();
		
			if (hm.containsKey(keyAllOrderStr)){
				freq = (String)hm.get(keyAllOrderStr);
			}else if (hm.containsKey(keyAllLevelStr)){
				freq = (String)hm.get(keyAllLevelStr);
			}else if (hm.containsKey(keyStr)){
    			freq = (String)hm.get(keyStr);
    		}else {
    			freq = "";
    		}
			
			if (sampleTest.getTestCode().equals(CHEMICAL_ANAYLYSIS)){
				if (exCHEM){
    				sampleItemFrequency.setChemAnalysisFreq(NA);
    			}else{
    				sampleItemFrequency.setChemAnalysisFreq(freq);
    			}
			}else if (sampleTest.getTestCode().equals(MICROBIOLOGY_TEST)){
				if (exMICRO){
    				sampleItemFrequency.setMicroBioTestFreq(NA);
    			}else {
    				sampleItemFrequency.setMicroBioTestFreq(freq);
    			}
			}
		}
    	
    	return sampleItemFrequency;
    }
    
    public List<SampleItemFrequency> getSampleItemFrequencyReportList(){
    	return sampleItemFrequencyReportList;
    }
    
    @Remove
	public void destroy(){
    	sampleItemFrequencyReportList = null;
	}

}
