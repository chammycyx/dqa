package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.FaxType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class QaProblemCriteria {
	
	private String itemCode;
	private OrderTypeAll orderType;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromCreateDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toCreateDate;
	private String caseNum;
	private ProblemClassification caseClassification;
	private CaseStatus caseStatus;
	private FaxType faxType;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromEventLogDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toEventLogDate;
	
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public OrderTypeAll getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	public Date getFromCreateDate() {
		return (fromCreateDate!=null)?new Date(fromCreateDate.getTime()):null;
	}
	public void setFromCreateDate(Date fromCreateDate) {
		if(fromCreateDate!=null)
		{
			this.fromCreateDate = new Date(fromCreateDate.getTime());
		}
	}
	public Date getToCreateDate() {
		return (toCreateDate!=null)?new Date(toCreateDate.getTime()):null;
	}
	public void setToCreateDate(Date toCreateDate) {
		if(toCreateDate!=null)
		{
			this.toCreateDate = new Date(toCreateDate.getTime());
		}
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	
	public ProblemClassification getCaseClassification() {
		return caseClassification;
	}
	public void setCaseClassification(ProblemClassification caseClassification) {
		this.caseClassification = caseClassification;
	}
	public CaseStatus getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(CaseStatus caseStatus) {
		this.caseStatus = caseStatus;
	}
	public FaxType getFaxType() {
		return faxType;
	}
	public void setFaxType(FaxType faxType) {
		this.faxType = faxType;
	}
	public Date getFromEventLogDate() {
		return (fromEventLogDate!=null)?new Date(fromEventLogDate.getTime()):null;
	}
	public void setFromEventLogDate(Date fromEventLogDate) {
		if(fromEventLogDate!=null)
		{
			this.fromEventLogDate = new Date(fromEventLogDate.getTime());
		}
	}
	public Date getToEventLogDate() {
		return (toEventLogDate!=null)?new Date(toEventLogDate.getTime()):null;
	}
	public void setToEventLogDate(Date toEventLogDate) {
		if(toEventLogDate!=null)
		{
			this.toEventLogDate = new Date(toEventLogDate.getTime());
		}
	}
	
	
}
