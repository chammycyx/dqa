package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.service.biz.pms.dqa.interfaces.DqaSubscriberJmsRemote;

import javax.ejb.Local;

@Local
public interface DqaSubscriberLocal extends DqaSubscriberJmsRemote {
}
