package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qaProblemListForCaseNumService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class QaProblemListForCaseNumServiceBean implements QaProblemListForCaseNumServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	@Out(required = false)
	private List<QaProblem> qaProblemListForCaseNum;
	
	public static final EventLogComparator EVENT_LOG_COMPARATOR = new EventLogComparator();
	public static final QaProblemComparator QA_PROBLEM_COMPARATOR = new QaProblemComparator();
	
		
	public void retrieveQaProblemListLikeCaseNumByItemSMP(String caseNum, String itemCode, String supplierCode, String manufCode, String pharmCompanyCode){
		logger.debug("retrieveQaProblemListLikeCaseNumbyItemSMP");
		
				
		qaProblemListForCaseNum = em.createNamedQuery("QaProblem.findLikeCaseNumByItemSMP")
								.setParameter("caseNum", caseNum+"%")
								.setParameter("itemCode", itemCode)
								.setParameter("supplierCode", supplierCode)
								.setParameter("manufCode", manufCode)
								.setParameter("pharmCompanyCode", pharmCompanyCode)
								.setParameter("recordStatus", RecordStatus.Active)
								.setHint(QueryHints.FETCH, "o.problemHeader")
								.setHint(QueryHints.FETCH, "o.supplier")
								.setHint(QueryHints.FETCH, "o.manufacturer")
								.setHint(QueryHints.FETCH, "o.pharmCompany")
								.setHint(QueryHints.BATCH, "o.pharmProblemList")
								.setHint(QueryHints.BATCH, "o.qaBatchNumList")
								.setHint(QueryHints.BATCH, "o.qaProblemNature")
								.setHint(QueryHints.BATCH, "o.classification")
								.getResultList();
		
		if(qaProblemListForCaseNum!=null && qaProblemListForCaseNum.size()>0)
		{
			for(QaProblem qc:qaProblemListForCaseNum)
			{
				getLazyQaProblem(qc);
			}
		}
		else
		{
			qaProblemListForCaseNum=null;
		}
		
	}
	
	
	public void retrieveQaProblemListLikeCaseNum(String caseNum, String loginInstCodeIn, Institution pcuIn, Institution institutionIn){
		logger.debug("retrieveQaProblemListLikeCaseNum");
		
		List<QaProblem> qaProblemListForCaseNumAllInstitutionFlag = null; 
		
		if(pcuIn == null && institutionIn == null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblem.findLikeCaseNum")
									.setParameter("caseNum", caseNum+"%")
									.setParameter("recordStatus", RecordStatus.Active)
									.setHint(QueryHints.FETCH, "o.problemHeader")
									.setHint(QueryHints.FETCH, "o.supplier")
									.setHint(QueryHints.FETCH, "o.manufacturer")
									.setHint(QueryHints.FETCH, "o.pharmCompany")
									.setHint(QueryHints.BATCH, "o.pharmProblemList")
									.setHint(QueryHints.BATCH, "o.qaBatchNumList")
									.setHint(QueryHints.BATCH, "o.qaProblemNature")
									.setHint(QueryHints.BATCH, "o.classification")
									.getResultList();
		}
		else if(pcuIn!=null && institutionIn == null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblemInstitution.findLikeCaseNumPcuCode")
										.setParameter("caseNum", caseNum+"%")
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("pcuCode", pcuIn.getPcuCode())
										.getResultList();
			
			
			qaProblemListForCaseNumAllInstitutionFlag = em.createNamedQuery("QaProblemInstitution.findLikeCaseNumAllInstitutionFlag")
										.setParameter("caseNum", caseNum+"%")
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("allInstitutionFlag", YesNoFlag.Yes)
										.getResultList();
			
		}
		else if(pcuIn!=null && institutionIn != null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblemInstitution.findLikeCaseNumInstitutionCode")
										.setParameter("caseNum", caseNum+"%")
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("institutionCode", institutionIn.getInstitutionCode())
										.getResultList();
			
			qaProblemListForCaseNumAllInstitutionFlag = em.createNamedQuery("QaProblemInstitution.findLikeCaseNumAllInstitutionFlag")
										.setParameter("caseNum", caseNum+"%")
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("allInstitutionFlag", YesNoFlag.Yes)
										.getResultList();
		}
		
		/*if(qaProblemListForCaseNum!=null && qaProblemListForCaseNum.size()>0)
		{
			for(QaProblem qc:qaProblemListForCaseNum)
			{
				getLazyQaProblem(qc);
			}
		}
		else
		{
			qaProblemListForCaseNum=null;
		}*/
		
		if(qaProblemListForCaseNumAllInstitutionFlag!=null && qaProblemListForCaseNumAllInstitutionFlag.size()>0)
		{
			if(qaProblemListForCaseNum==null)
			{
				qaProblemListForCaseNum = qaProblemListForCaseNumAllInstitutionFlag;
			}
			else
			{
				qaProblemListForCaseNum.addAll(qaProblemListForCaseNumAllInstitutionFlag);
			}
		}
		
		if(qaProblemListForCaseNum!=null && qaProblemListForCaseNum.size()>0)
		{
			for(QaProblem qc:qaProblemListForCaseNum)
			{
				getLazyQaProblem(qc);
			}
			Collections.sort(qaProblemListForCaseNum, QA_PROBLEM_COMPARATOR);
		}
		else
		{
			qaProblemListForCaseNum=null;
		}
	}
	
	public void retrieveQaProblemListByCaseNumInst(String caseNum, String loginInstCodeIn, Institution pcuIn, Institution institutionIn){
		logger.debug("retrieveQaProblemListByCaseNumInst");
		
		List<QaProblem> qaProblemListForCaseNumAllInstitutionFlag = null; 
		
		if(pcuIn == null && institutionIn == null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblem.findByCaseNumRecordStatus")
									.setParameter("caseNum", caseNum)
									.setParameter("recordStatus", RecordStatus.Active)
									.setHint(QueryHints.FETCH, "o.problemHeader")
									.setHint(QueryHints.FETCH, "o.supplier")
									.setHint(QueryHints.FETCH, "o.manufacturer")
									.setHint(QueryHints.FETCH, "o.pharmCompany")
									.setHint(QueryHints.BATCH, "o.pharmProblemList")
									.setHint(QueryHints.BATCH, "o.qaBatchNumList")
									.setHint(QueryHints.BATCH, "o.qaProblemNature")
									.setHint(QueryHints.BATCH, "o.classification")
									.getResultList();
		}
		else if(pcuIn!=null && institutionIn == null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblemInstitution.findByCaseNumPcuCode")
										.setParameter("caseNum", caseNum)
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("pcuCode", pcuIn.getPcuCode())
										.getResultList();
			
			
			qaProblemListForCaseNumAllInstitutionFlag = em.createNamedQuery("QaProblemInstitution.findByCaseNumAllInstitutionFlag")
										.setParameter("caseNum", caseNum)
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("allInstitutionFlag", YesNoFlag.Yes)
										.getResultList();
			
		}
		else if(pcuIn!=null && institutionIn != null)
		{
			qaProblemListForCaseNum = em.createNamedQuery("QaProblemInstitution.findByCaseNumInstitutionCode")
										.setParameter("caseNum", caseNum)
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("institutionCode", institutionIn.getInstitutionCode())
										.getResultList();
			
			qaProblemListForCaseNumAllInstitutionFlag = em.createNamedQuery("QaProblemInstitution.findByCaseNumAllInstitutionFlag")
										.setParameter("caseNum", caseNum)
										.setParameter("recordStatus", RecordStatus.Active)
										.setParameter("allInstitutionFlag", YesNoFlag.Yes)
										.getResultList();
		}
		
		/*if(qaProblemListForCaseNum!=null && qaProblemListForCaseNum.size()>0)
		{
			for(QaProblem qc:qaProblemListForCaseNum)
			{
				getLazyQaProblem(qc);
			}
		}
		else
		{
			qaProblemListForCaseNum=null;
		}*/
		
		if(qaProblemListForCaseNumAllInstitutionFlag!=null && qaProblemListForCaseNumAllInstitutionFlag.size()>0)
		{
			if(qaProblemListForCaseNum==null)
			{
				qaProblemListForCaseNum = qaProblemListForCaseNumAllInstitutionFlag;
			}
			else
			{
				qaProblemListForCaseNum.addAll(qaProblemListForCaseNumAllInstitutionFlag);
			}
		}
		
		if(qaProblemListForCaseNum!=null && qaProblemListForCaseNum.size()>0)
		{
			for(QaProblem qc:qaProblemListForCaseNum)
			{
				getLazyQaProblem(qc);
			}
			Collections.sort(qaProblemListForCaseNum, QA_PROBLEM_COMPARATOR);
		}
		else
		{
			qaProblemListForCaseNum=null;
		}
	}
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		Collections.sort(qaProblemLazy.getEventLogList(), EVENT_LOG_COMPARATOR);
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
			pc.getPharmBatchNumList();
			pc.getPharmBatchNumList().size();
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
		}
		return qaProblemLazy;
	}
	
	private static class EventLogComparator implements Comparator<EventLog>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private Date getEventLogDate(EventLog el) {
        	return el.getEventLogDate();
        }
        
        public int compare(EventLog el1, EventLog el2) {

            Date date1 = getEventLogDate(el1);
            Date date2 = getEventLogDate(el2);

            if (!date1.equals(date2)) {
            	return date1.compareTo(date2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}
	
	private static class QaProblemComparator implements Comparator<QaProblem>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private String getCaseNum(QaProblem el) {
        	return el.getCaseNum();
        }
        
        public int compare(QaProblem el1, QaProblem el2) {

            String caseNum1 = getCaseNum(el1);
            String caseNum2 = getCaseNum(el2);

            if (!caseNum1.equals(caseNum2)) {
            	return caseNum1.compareTo(caseNum2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}

	
		
	@Remove
	public void destroy(){
		if (qaProblemListForCaseNum != null){
			qaProblemListForCaseNum = null;
		}
	}
}
