package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MicroBioTestExclusion  implements StringValuedEnum {
	
	BIO("BIO", "Biological products  including vaccines, blood products, immunoglobulins"),
	CM("CM", "Contrast media (e.g barium sulphate suspension, iomeprol injection) "),
	DCA("DCA", "Disinfectant / cleansing agents (e.g alcohol-based hand rub, povidone iodine surgical scrub) "),
	RP("RP", "Radioactive pharmaceuticals (e.g sodium iodide (i-131) therapy capsule, technetium-99m sulphur colloid injection)"),
	TA("TA", "Testing agents / diagnostics reagents (e.g indigo carmine injection) "),
	DH("DH", "All items from Pharmaceutical Manufacturers, Department of Health"),
	DD("DD", "Dangerous Drug"),
	EI("EI", "Expensive Item"),
	Other("O", "Other");
	
	private final String dataValue;
    private final String displayValue;
	
	MicroBioTestExclusion(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static class Converter extends StringValuedEnumConverter<MicroBioTestExclusion> {

		private static final long serialVersionUID = 3601829209059683642L;

		@Override
    	public Class<MicroBioTestExclusion> getEnumClass() {
    		return MicroBioTestExclusion.class;
    	}
    }
}
