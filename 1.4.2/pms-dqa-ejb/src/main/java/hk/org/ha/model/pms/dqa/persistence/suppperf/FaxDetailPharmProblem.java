package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_DETAIL_PHARM_PROBLEM")
@Customizer(AuditCustomizer.class)
public class FaxDetailPharmProblem extends VersionEntity {

	private static final long serialVersionUID = -1934348975452809369L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxDetailPharmProblemSeq")
	@SequenceGenerator(name = "faxDetailPharmProblemSeq", sequenceName = "SEQ_FAX_DETAIL_PHARM_PROBLEM", initialValue=10000)
	@Id
	@Column(name="FAX_DETAIL_PHARM_PROBLEM_ID", nullable=false)
	private Long faxDetailPharmProblemId;
	
	@ManyToOne
	@JoinColumn(name="FAX_DETAIL_ID")
	private FaxDetail faxDetail;
	
	@ManyToOne
	@JoinColumn(name="PHARM_PROBLEM_ID")
	private PharmProblem pharmProblem;

	public Long getFaxDetailPharmProblemId() {
		return faxDetailPharmProblemId;
	}

	public void setFaxDetailPharmProblemId(Long faxDetailPharmProblemId) {
		this.faxDetailPharmProblemId = faxDetailPharmProblemId;
	}
	
	public FaxDetail getFaxDetail() {
		return faxDetail;
	}

	public void setFaxDetail(FaxDetail faxDetail) {
		this.faxDetail = faxDetail;
	}

	public PharmProblem getPharmProblem() {
		return pharmProblem;
	}

	public void setPharmProblem(PharmProblem pharmProblem) {
		this.pharmProblem = pharmProblem;
	}


}
