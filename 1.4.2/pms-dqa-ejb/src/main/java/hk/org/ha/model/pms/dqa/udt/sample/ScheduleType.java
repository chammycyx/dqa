package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ScheduleType implements StringValuedEnum{
	
	Manual("M", "Manual"),
	System("S", "System");
	
	private final String dataValue;
	private final String displayValue;
	
	ScheduleType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }      
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }
    
    public static class Converter extends StringValuedEnumConverter<ScheduleType> {
   
		private static final long serialVersionUID = -8588798940492081712L;

		@Override
    	public Class<ScheduleType> getEnumClass() {
    		return ScheduleType.class;
    	}
    	
    	
    }
	
}
