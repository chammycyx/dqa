package hk.org.ha.model.pms.dqa.biz.suppperf;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;

import javax.ejb.Local;

@Local
public interface BatchSuspServiceLocal {
	
	void retrieveBatchSuspByBatchSusp(BatchSusp batchSuspIn);
	
	void createBatchSusp(List<BatchSusp> batchSuspListIn);
	
	void updateBatchSusp(BatchSusp batchSuspIn);
	
	void deleteBatchSusp(BatchSusp batchSuspIn);
	
	void destroy();
	
}
