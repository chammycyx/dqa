package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Specification implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	BP("BP", "B.P."),
	USP("USP", "U.S.P."),
	OTH("OTH", "Others: (Manufacturer's method of analysis and specifications)");
	
    private final String dataValue;
    private final String displayValue;
        
    Specification(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Specification> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<Specification> getEnumClass() {
    		return Specification.class;
    	}
    }
}
