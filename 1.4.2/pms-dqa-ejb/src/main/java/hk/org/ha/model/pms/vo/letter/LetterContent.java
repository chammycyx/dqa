package hk.org.ha.model.pms.vo.letter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LetterContent")
@ExternalizedBean(type=DefaultExternalizer.class)
public class LetterContent {
	
	@XmlElement
	private String subjectInfo;
	
	@XmlElement
	private String senderPosition;
	
	@XmlElement
	private String refNum;
	
	@XmlElement
	private String senderTelephoneNum;
	
	@XmlElement
	private String senderFaxNum;
	
	@XmlElement
	private String sendDate;
	
	@XmlElement
	private String receiverPosition;
	
	@XmlElement
	private String receiverName;
	
	@XmlElement
	private String receiverCompany;
	
	@XmlElement
	private String receiverAddress;
	
	@XmlElement
	private String receiverFax;
	
	@XmlElement
	private String salutation;	
	
	@XmlElement
	private String receiverAttention;		
	
	@XmlElement(name="paragraph", required = true)
	private List<String> paragraphList;
	
	@XmlElement
	private String valediction;
	
	@XmlElement
	private String signature;
	
	@XmlElement
	private String headOfficeAddress;
	
	@XmlElement
	private String headOfficePhone;
	
	@XmlElement
	private String headOfficeFax;	
	
	@XmlElement
	private String footerInfo;
	
	public void setSubjectInfo(String subjectInfo) {
		this.subjectInfo = subjectInfo;
	}
	
	public String getSubjectInfo() {
		return subjectInfo;
	}
	
	public String getSenderPosition() {
		return senderPosition;
	}

	public void setSenderPosition(String senderPosition) {
		this.senderPosition = senderPosition;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getRefNum() {
		return refNum;
	}
	
	public String getSenderTelephoneNum() {
		return senderTelephoneNum;
	}

	public void setSenderTelephoneNum(String senderTelephoneNum) {
		this.senderTelephoneNum = senderTelephoneNum;
	}

	public String getSenderFaxNum() {
		return senderFaxNum;
	}

	public void setSenderFaxNum(String senderFaxNum) {
		this.senderFaxNum = senderFaxNum;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public void setReceiverPosition(String receiverPosition) {
		this.receiverPosition = receiverPosition;
	}

	public String getReceiverPosition() {
		return receiverPosition;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverCompany(String receiverCompany) {
		this.receiverCompany = receiverCompany;
	}

	public String getReceiverCompany() {
		return receiverCompany;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverFax(String receiverFax) {
		this.receiverFax = receiverFax;
	}

	public String getReceiverFax() {
		return receiverFax;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getSalutation() {
		return salutation;
	}

	public String getReceiverAttention() {
		return receiverAttention;
	}

	public void setReceiverAttention(String receiverAttention) {
		this.receiverAttention = receiverAttention;
	}

	public List<String> getParagraphList() {
		return paragraphList;
	}

	public void setParagraphList(List<String> paragraphList) {
		this.paragraphList = paragraphList;
	}

	public void setValediction(String valediction) {
		this.valediction = valediction;
	}

	public String getValediction() {
		return valediction;
	}

	public void addParagraph(String paragraph) {
		if (this.paragraphList == null) {
			this.paragraphList = new ArrayList<String>();
		}
		this.paragraphList.add(paragraph);
	}
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getHeadOfficeAddress() {
		return headOfficeAddress;
	}

	public void setHeadOfficeAddress(String headOfficeAddress) {
		this.headOfficeAddress = headOfficeAddress;
	}
	
	public void setHeadOfficePhone(String headOfficePhone) {
		this.headOfficePhone = headOfficePhone;
	}

	public String getHeadOfficePhone() {
		return headOfficePhone;
	}

	public void setHeadOfficeFax(String headOfficeFax) {
		this.headOfficeFax = headOfficeFax;
	}

	public String getHeadOfficeFax() {
		return headOfficeFax;
	}

	public String getFooterInfo() {
		return footerInfo;
	}

	public void setFooterInfo(String footerInfo) {
		this.footerInfo = footerInfo;
	}
	
	public static LetterContent buildContentForReleaseRetentionSample() {
		LetterContent letterContent = new LetterContent();
		letterContent.setSenderPosition("Chief Pharmacist, HA");
		letterContent.setRefNum("{0} in PHS/QA/1");
		letterContent.setReceiverPosition("DM (Pharm)");
		letterContent.setReceiverAttention("(Attn: {0})");
		letterContent.setSubjectInfo("Release of Retention Sample from Suspend Store");
		letterContent.addParagraph("Please be informed that the Continuous Quality Assurance (CQA) testing of the above item is completed. The suspended retention sample can now be released for use.");
		letterContent.addParagraph("The quantity of suspended retention sample is {0} {1} from batch {2}. Please enter the transaction of the stock transfer in ERP system accordingly.");
		letterContent.addParagraph("Should you have any enquiries, please contact {0} {1} {2} at {3}.");
		letterContent.setSignature("({0}\n{1}\n{2})\nfor Chief Pharmacist,\nHospital Authority");		
		letterContent.setHeadOfficeAddress("7/F, Multicentre Block A, Pamela Youde Nethersole Eastern Hospital, 3 Lok Man Road, Chai Wan, Hong Kong");
		return letterContent;
	}
	
	public static LetterContent buildContentForConflictOfInterest() {
		LetterContent letterContent = new LetterContent();
		letterContent.setSubjectInfo("Pharmaceutical Product Problem Report\n\n(Declaration of Conflict of Interest)");
		letterContent.addParagraph("In order to uphold and protect HA's and your reputation, you should avoid obligations to business associates resulting from advantages, gifts or entertainment received in, or due to your official capacity which could compromise your position in any way. As a personal responsibility, you should declare any potential conflict of interest and abstain from engaging in situations that may lead to perceived bias in decision-making during the reporting.");
		letterContent.addParagraph("In respect of the pharmaceutical product problem reporting, are you or any member of your family or close personal friends (e.g. friends who know each other for a long time and have frequent social relationships), a director, officer, sole owner, partner, employee, consultant or advisor to any of the Company/Manufacturer/Supplier under this report:");
		return letterContent;
	}
	
	public static void main(String[] args) throws JAXBException {
		
		LetterContent letterContent = buildContentForConflictOfInterest();
		JAXBContext jaxbContext = JAXBContext.newInstance(LetterContent.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
		
		marshaller.marshal(letterContent, System.out);
	}
}
