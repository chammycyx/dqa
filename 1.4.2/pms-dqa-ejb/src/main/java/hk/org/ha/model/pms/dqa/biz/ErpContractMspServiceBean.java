package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import hk.org.ha.model.pms.dqa.vo.ContractLine;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("erpContractMspService")
@RemoteDestination
@MeasureCalls
public class ErpContractMspServiceBean implements ErpContractMspServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;	
	
	@In(create=true)
	private CompanyServiceLocal companyService;	
	
	@SuppressWarnings("unchecked")
	public ContractMsp retrieveContractMspByContractIdErpContractMspId( Long contractId, Long erpContractMspId ) {
		logger.debug("retrieveContractMspByContractIdContractMspId #0 #1", contractId, erpContractMspId);
		List<ContractMsp> contractMspList = em.createNamedQuery( "ContractMsp.findByContractIdErpContractMspId" )
		 								  .setParameter( "contractId", contractId )
		 								  .setParameter( "erpContractMspId", erpContractMspId )
		 								  .getResultList();
		return ( contractMspList.size() > 0 )? contractMspList.get( 0 ): null;
	}
	
	public void createContractMsp( Contract c, ContractLine cl, hk.org.ha.model.pms.dqa.vo.ContractMsp cm ) {
		logger.debug("createContractMsp #0 #1 #2", c.getContractNum(), cl.getContractLineId(), cm.getMspRecordId());
		ContractMsp cMsp = new ContractMsp();
		cMsp.setContract( c );		
		
		cMsp.setCountryOfOrigin( cm.getCountryOfOrigin() );
		cMsp.setErpContractMspId( cm.getMspRecordId() );
		cMsp.setManufacturer( 
				companyService.retrieveCompanyByCompanyCodeManufFlag( 
						cm.getManufCode(), YesNoFlag.Yes ) 
		);
		cMsp.setPharmCompany( 
				companyService.retrieveCompanyByCompanyCodePharmCompanyFlag(
						cm.getPharmCompanyCode(), YesNoFlag.Yes ) 
		);
		cMsp.setStartDate( cm.getEffectiveStartDate() );
		cMsp.setEndDate( cm.getEffectiveEndDate() );
		cMsp.setRecordStatus( RecordStatus.Active );
		em.persist( cMsp );
		em.flush();
	}

	public void updateContractMsp( ContractMsp cMsp, hk.org.ha.model.pms.dqa.vo.ContractMsp cm ) {
		logger.debug("updateContractMsp #0 #1", cMsp.getContractMspId(), cm.getMspRecordId());
		cMsp.setStartDate( cm.getEffectiveStartDate() );
		cMsp.setEndDate( cm.getEffectiveEndDate() );
		cMsp.setManufacturer(
				companyService.retrieveCompanyByCompanyCodeManufFlag( 
						cm.getManufCode(), YesNoFlag.Yes ) 
		);
		cMsp.setPharmCompany( 
				companyService.retrieveCompanyByCompanyCodePharmCompanyFlag(
						cm.getPharmCompanyCode(), YesNoFlag.Yes ) 
		);
		cMsp.setCountryOfOrigin( cm.getCountryOfOrigin() );
		cMsp.setRecordStatus( RecordStatus.Active );
		em.merge( cMsp );
		em.flush();		
	}
	
}
