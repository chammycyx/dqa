package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleFullDetailService")
//@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleFullDetailServiceBean implements SampleTestScheduleFullDetailServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

		
	@Out(required = false)
	private SampleTestSchedule sampleTestScheduleFullDetail;
	
	
	private static final String SCHEDULE_ID = "scheduleId";
	
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleFullDetailByScheduleId(Long scheduleId){
		logger.debug("retrieveSampleTestScheduleFullDetailByScheduleId : #0 ", scheduleId);
		
		List<SampleTestSchedule> sampleTestScheduleListFind;
		
		sampleTestScheduleListFind = em.createNamedQuery("SampleTestSchedule.findByScheduleId")
									.setParameter(SCHEDULE_ID, scheduleId)
									.setHint(QueryHints.FETCH, "o.sampleTest")
									.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
									.setHint(QueryHints.BATCH, "o.institution")
									.setHint(QueryHints.BATCH, "o.lab")
									.setHint(QueryHints.BATCH, "o.chemicalAnalysis")
									.setHint(QueryHints.BATCH, "o.microBioTest")
									.setHint(QueryHints.BATCH, "o.payment")
									.getResultList();
		 
		if (sampleTestScheduleListFind==null || sampleTestScheduleListFind.size()==0){
			sampleTestScheduleFullDetail=null;}
		else
		{
			sampleTestScheduleFullDetail =  sampleTestScheduleListFind.get(0);
			
			sampleTestScheduleFullDetail.getSampleTest();
			sampleTestScheduleFullDetail.getPayment();
			sampleTestScheduleFullDetail.getContract();
			sampleTestScheduleFullDetail.getContract().getSupplier();
			sampleTestScheduleFullDetail.getContract().getSupplier().getContact();
			sampleTestScheduleFullDetail.getContract().getManufacturer();
			sampleTestScheduleFullDetail.getContract().getManufacturer().getContact();
			sampleTestScheduleFullDetail.getContract().getPharmCompany();
			sampleTestScheduleFullDetail.getContract().getPharmCompany().getContact();
			sampleTestScheduleFullDetail.getInstitution();
			sampleTestScheduleFullDetail.getTestFrequency();
			sampleTestScheduleFullDetail.getLab();
			sampleTestScheduleFullDetail.getMicroBioTest();
			sampleTestScheduleFullDetail.getChemicalAnalysis();
			sampleTestScheduleFullDetail.getSampleItem().getExclusionTestList().size();
			
		}
	}
	
	@Remove
	public void destory(){
		if (sampleTestScheduleFullDetail!=null){
			sampleTestScheduleFullDetail = null;
		}
		
	}
	
}
