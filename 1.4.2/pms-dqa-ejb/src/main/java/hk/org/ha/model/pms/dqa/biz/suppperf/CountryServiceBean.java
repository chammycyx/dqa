package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("countryService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CountryServiceBean implements CountryServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private Country country;
	
	@SuppressWarnings("unchecked")
	public void retrieveCountryByCountryCode(String countryCode){	
		logger.debug("retrieveCountryByCountryCode #0", countryCode);
								
		List<Country> countrys = em.createNamedQuery("Country.findByCountryCode")
									.setParameter("countryCode", countryCode)
									.setParameter("recordStatus",RecordStatus.Active)
									.getResultList();
		
		if(countrys!=null && countrys.size()>0)
		{
			country = countrys.get(0);
		}
		else
		{
			country = null;
		}
			
	}
	
	@SuppressWarnings("unchecked")
	public Country findCountryByCountryCode(String countryCode){
		logger.debug("findCountryByCountryCode", countryCode);

		List<Country> countrys = em.createNamedQuery("Country.findByCountryCode")
									.setParameter("countryCode", countryCode)
									.setParameter("recordStatus",RecordStatus.Active)
									.getResultList();
		
		if(countrys!=null && countrys.size()>0)
		{
			return countrys.get(0);
		}
		else
		{
			return null;
		}
	}
	
	@Remove
	public void destroy(){
		if(country!=null) {
			country = null;
		}
	}
}
