package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import java.util.List;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleFileListServiceLocal {
	
	List<SampleTestScheduleFile> retrieveSampleTestScheduleFileListBySmpleTestFileId(Long sampleTestFileId);
	
	void retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat(RecordStatus recordStatus, ScheduleStatus scheduleStatus, SampleTestFileCat fileCat);
	
	void destroy();

}
