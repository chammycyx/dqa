package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;

import javax.ejb.Local;

@Local
public interface QaProblemListForCaseNumServiceLocal {
	
	void retrieveQaProblemListLikeCaseNumByItemSMP(String caseNum, String itemCode, String supplierCode, String manufCode, String pharmCompanyCode);
	
	void retrieveQaProblemListLikeCaseNum(String caseNum, String loginInstCodeIn, Institution pcuIn, Institution institutionIn);
	
	void retrieveQaProblemListByCaseNumInst(String caseNum, String loginInstCodeIn, Institution pcuIn, Institution institutionIn);
	
	void destroy();

}
