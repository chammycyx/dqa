package hk.org.ha.model.pms.persistence;

import hk.org.ha.fmk.pms.entity.CreateDate;
import hk.org.ha.fmk.pms.entity.CreateUser;
import hk.org.ha.fmk.pms.entity.UpdateDate;
import hk.org.ha.fmk.pms.entity.UpdateUser;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable {

	@UpdateDate
	@Column(name = "MODIFY_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@CreateDate
	@Column(name = "CREATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@UpdateUser
	@Column(name = "MODIFY_USER", length = 15, nullable = false)
	private String modifyUser;

	@CreateUser
	@Column(name = "CREATE_USER", length = 15, nullable = false)
	private String createUser;

	public Date getModifyDate() {
		return (modifyDate != null) ? new Date(modifyDate.getTime()) : null;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = new Date(modifyDate.getTime());
	}

	public Date getCreateDate() {
		return (createDate != null) ? new Date(createDate.getTime()) : null;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = new Date(createDate.getTime());
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
}
