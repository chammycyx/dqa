package hk.org.ha.model.pms.dqa.persistence.coa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.udt.coa.ReplyEmailTo;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COA_REPLY_EMAIL")
@Customizer(AuditCustomizer.class)
public class CoaReplyEmail extends VersionEntity {

	private static final long serialVersionUID = 702530506230257898L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaReplyEmailSeq")
	@SequenceGenerator(name="coaReplyEmailSeq", sequenceName="SEQ_COA_REPLY_EMAIL", initialValue=10000)
	@Column(name="COA_REPLY_EMAIL_ID")
	private Long coaReplyEmailId;

	@Converter(name = "CoaReplyEmail.replyEmailTo", converterClass = ReplyEmailTo.Converter.class )
	@Convert("CoaReplyEmail.replyEmailTo")
	@Column(name="REPLY_EMAIL_TO", length=1)
	private ReplyEmailTo replyEmailTo;
	
	@OneToOne
	@JoinColumn(name="EMAIL_ID")
	private EmailLog emailLog;
	
	@ManyToOne
	@JoinColumn(name="COA_IN_EMAIL_ID")
	private CoaInEmail coaInEmail;
	
	public void setCoaReplyEmailId(Long coaReplyEmailId) {
		this.coaReplyEmailId = coaReplyEmailId;
	}

	public Long getCoaReplyEmailId() {
		return coaReplyEmailId;
	}

	public void setReplyEmailTo(ReplyEmailTo replyEmailTo) {
		this.replyEmailTo = replyEmailTo;
	}

	public ReplyEmailTo getReplyEmailTo() {
		return replyEmailTo;
	}

	public void setEmailLog(EmailLog emailLog) {
		this.emailLog = emailLog;
	}

	public EmailLog getEmailLog() {
		return emailLog;
	}

	public void setCoaInEmail(CoaInEmail coaInEmail) {
		this.coaInEmail = coaInEmail;
	}

	public CoaInEmail getCoaInEmail() {
		return coaInEmail;
	}
}
