package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum SampleTestFileCat implements StringValuedEnum  {
	
	MethodofAssay("MOA" , "Method of Assay"),
	FinishedProductSpecification("FPS", "Finished Product Specification"),
	ValidationReport("VR", "Validation Report"),
	TestResultReport("TR", "Test Result Report");
	
	private final String dataValue;
	private final String displayValue;
	
	private SampleTestFileCat(String dataValue, String displayValue) {
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	public String getDataValue() {
		return dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}
	
	public static class Converter extends StringValuedEnumConverter<SampleTestFileCat> {

		private static final long serialVersionUID = -3006038336982322723L;

		@Override
    	public Class<SampleTestFileCat> getEnumClass() {
    		return SampleTestFileCat.class;
    	}
    }
}
