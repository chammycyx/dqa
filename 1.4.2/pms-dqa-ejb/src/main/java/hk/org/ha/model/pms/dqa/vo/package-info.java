@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class)
})  
package hk.org.ha.model.pms.dqa.vo;

import hk.org.ha.model.pms.dqa.util.DateAdapter;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;   
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;   