package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum FileExtension implements StringValuedEnum {

	BMP(".bmp","image/bmp"),
	GIF(".gif","image/gif"),
	JPG(".jpg","image/jpeg"),
	JPEG(".jpeg","image/jpeg"),
	PDF(".pdf","application/pdf"),
	PNG(".png","image/png"),
	TIF(".tif","image/tiff"),
	TIFF(".tiff","image/tiff");
	
	private final String dataValue;
    private final String displayValue;
        
    FileExtension(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }
	
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}

	public static FileExtension dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(FileExtension.class, dataValue);
	}
	
	public static class Converter extends StringValuedEnumConverter<FileExtension> {
		private static final long serialVersionUID = 1L;
		@Override
    	public Class<FileExtension> getEnumClass() {
    		return FileExtension.class;
    	}
    }
}
