package hk.org.ha.model.pms.dqa.cacher.mock;

import hk.org.ha.fmk.pms.cache.BaseCacher;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dms.persistence.DmProcureSummary;
import hk.org.ha.model.pms.dms.vo.dqa.DqaDrugSupplier;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("dmDrugCacher")
@Scope(ScopeType.APPLICATION)
@Install(precedence=Install.MOCK)
public class MockDmDrugCacher extends BaseCacher implements DmDrugCacherInf {

	public DmDrug getDrugByItemCode(String itemCode) {

		List<DmDrug> tmpList = initDmDrugListForDqa();

		for (DmDrug dg : tmpList){
			if (dg.getItemCode().equals(itemCode) ){
				return dg;
			}
		}
		return null;
		
	}

	public List<DmDrug> getDrugListByItemCode(String prefixItemCode) {
		List <DmDrug> outList = new ArrayList<DmDrug>();
		List<DmDrug> tmpList = initDmDrugListForDqa();
		for (DmDrug dg : tmpList){
			if (dg.getItemCode().startsWith(prefixItemCode) ){
				
				outList.add(dg);
			}
		}
		return outList;
	}
	
	public List<DmDrug> initDmDrugListForDqa(){
		ArrayList<DmDrug> dmList = new ArrayList<DmDrug>();
		
		// 01
		InnerDmDrug dg01 = new InnerDmDrug();
		dg01.setItemCode("PARA01");
		dg01.setFullDrugDesc("PARACETAMOL TABLET 500MG");
		DmProcureInfo dg01ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier = new DqaDrugSupplier();
		DmProcureSummary dmProcureSummary = new DmProcureSummary();
		dg01ProcureInfo.setOrderType("C");
		dqaDrugSupplier.setItemCode("PARA01");
		dqaDrugSupplier.setRegStatus("N");
		dqaDrugSupplier.setReleaseIndicator("Y");
		dmProcureSummary.setAverageUnitPrice(1.1);
		dg01.setDmProcureInfo(dg01ProcureInfo);
		dg01.setDqaDrugSupplier(dqaDrugSupplier);
		dg01.setDmProcureSummary(dmProcureSummary);
		dg01.setDangerousDrug("N");
		
		dmList.add(dg01);
		
		// 02
		InnerDmDrug dg02 = new InnerDmDrug();
		dg02.setItemCode("5FLU05");
		dg02.setFullDrugDesc("5FLUCYTOSINE CAPSULE 500MG");
		DmProcureInfo dg02ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary2 = new DmProcureSummary();
		DqaDrugSupplier dqaDrugSupplier2 = new DqaDrugSupplier();
		dg02ProcureInfo.setOrderType("C");
		dmProcureSummary2.setAverageUnitPrice(1.2);
		dqaDrugSupplier2.setItemCode("5FLU05");
		dqaDrugSupplier2.setRegStatus("N");
		dqaDrugSupplier2.setReleaseIndicator("Y");
		dg02.setDmProcureInfo(dg02ProcureInfo);
		dg02.setDmProcureSummary(dmProcureSummary2);
		dg02.setDqaDrugSupplier(dqaDrugSupplier2);
		dg02.setDangerousDrug("N");
		dmList.add(dg02);
		
		//03
		InnerDmDrug dg03 = new InnerDmDrug();
		dg03.setItemCode("PARA02");
		dg03.setFullDrugDesc("PARACETAMOL ELIXIR 125MG/5ML");
		DmProcureInfo dg03ProcureInfo = new DmProcureInfo();
		DqaDrugSupplier dqaDrugSupplier3 = new DqaDrugSupplier();
		DmProcureSummary dmProcureSummary3 = new DmProcureSummary();
		dg03ProcureInfo.setOrderType("D");
		dqaDrugSupplier3.setItemCode("PARA02");
		dqaDrugSupplier3.setRegStatus("N");
		dqaDrugSupplier3.setReleaseIndicator("Y");
		dmProcureSummary3.setAverageUnitPrice(1.3);
		dg03.setDmProcureInfo(dg03ProcureInfo);
		dg03.setDqaDrugSupplier(dqaDrugSupplier3);
		dg03.setDmProcureSummary(dmProcureSummary3);
		dg03.setDangerousDrug("N");
		
		dmList.add(dg03);
		
		//04
		InnerDmDrug dg04 = new InnerDmDrug();
		dg04.setItemCode("OXYG02");
		dg04.setFullDrugDesc("OXYGEN MEDICAL 0.32CUM(D) CYLINDER");
		DmProcureInfo dg04ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary4 = new DmProcureSummary();
		DqaDrugSupplier dqaDrugSupplier4 = new DqaDrugSupplier();
		dg04ProcureInfo.setOrderType("C");
		dmProcureSummary4.setAverageUnitPrice(1.4);
		dqaDrugSupplier4.setItemCode("OXYG02");
		dqaDrugSupplier4.setRegStatus("N");
		dqaDrugSupplier4.setReleaseIndicator("Y");
		dg04.setDmProcureInfo(dg04ProcureInfo);
		dg04.setDmProcureSummary(dmProcureSummary4);
		dg04.setDqaDrugSupplier(dqaDrugSupplier4);
		dg04.setDangerousDrug("N");
		
		dmList.add(dg04);
		
		//05
		InnerDmDrug dg05 = new InnerDmDrug();
		dg05.setItemCode("ALBU01");
		dg05.setFullDrugDesc("ALBUMIN INJECTION 0.25G/ML 50ML");
		DqaDrugSupplier dqaDrugSupplier5 = new DqaDrugSupplier();
		DmProcureInfo dg05ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary5 = new DmProcureSummary();
		dg05ProcureInfo.setOrderType("D");
		dqaDrugSupplier5.setItemCode("ALBU01");
		dqaDrugSupplier5.setRegStatus("N");
		dqaDrugSupplier5.setReleaseIndicator("Y");
		dmProcureSummary5.setAverageUnitPrice(1.5);
		dg05.setDmProcureInfo(dg05ProcureInfo);
		dg05.setDqaDrugSupplier(dqaDrugSupplier5);
		dg05.setDmProcureSummary(dmProcureSummary5);
		dg05.setDangerousDrug("Y");
		
		dmList.add(dg05);
		
		//06
		InnerDmDrug dg06 = new InnerDmDrug();
		dg06.setItemCode("AUGM01");
		dg06.setFullDrugDesc("AUGMENTIN (OR EQUIV) TABLET 375MG");
		DmProcureInfo dg06ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary6 = new DmProcureSummary();
		DqaDrugSupplier dqaDrugSupplier6 = new DqaDrugSupplier();
		dg06ProcureInfo.setOrderType("C");
		dmProcureSummary6.setAverageUnitPrice(1.6);
		dqaDrugSupplier6.setItemCode("AUGM01");
		dqaDrugSupplier6.setRegStatus("N");
		dqaDrugSupplier6.setReleaseIndicator("Y");
		dg06.setDmProcureInfo(dg06ProcureInfo);
		dg06.setDmProcureSummary(dmProcureSummary6);
		dg06.setDqaDrugSupplier(dqaDrugSupplier6);
		dg06.setDangerousDrug("Y");
		
		dmList.add(dg06);
		
		//07
		InnerDmDrug dg07 = new InnerDmDrug();
		dg07.setItemCode("INFL05");
		dg07.setFullDrugDesc("INFLUENZA VACCINE 1DOSE(S)");
		DqaDrugSupplier dqaDrugSupplier7 = new DqaDrugSupplier();
		DmProcureInfo dg07ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary7 = new DmProcureSummary();
		dg07ProcureInfo.setOrderType("D");
		dqaDrugSupplier7.setItemCode("INFL05");
		dqaDrugSupplier7.setRegStatus("N");
		dqaDrugSupplier7.setReleaseIndicator("Y");
		dmProcureSummary7.setAverageUnitPrice(1.7);
		dg07.setDmProcureInfo(dg07ProcureInfo);
		dg07.setDqaDrugSupplier(dqaDrugSupplier7);
		dg07.setDmProcureSummary(dmProcureSummary7);
		dg07.setDangerousDrug("N");
		
		dmList.add(dg07);
		
		
		//08
		InnerDmDrug dg08 = new InnerDmDrug();
		dg08.setItemCode("MAGN08");
		dg08.setFullDrugDesc("MAGNESIUM HYDROXIDE SUSPENSION 7-8.8% WW");
		DmProcureInfo dg08ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary8 = new DmProcureSummary();
		DqaDrugSupplier dqaDrugSupplier8 = new DqaDrugSupplier();
		dg08ProcureInfo.setOrderType("C");
		dmProcureSummary8.setAverageUnitPrice(1.8);
		dqaDrugSupplier8.setItemCode("MAGN08");
		dqaDrugSupplier8.setRegStatus("N");
		dqaDrugSupplier8.setReleaseIndicator("Y");
		dg08.setDmProcureInfo(dg08ProcureInfo);
		dg08.setDmProcureSummary(dmProcureSummary8);
		dg08.setDqaDrugSupplier(dqaDrugSupplier8);
		dg08.setDangerousDrug("Y");
		
		dmList.add(dg08);
		
		//09
		InnerDmDrug dg09 = new InnerDmDrug();
		dg09.setItemCode("CHLO2F");
		dg09.setFullDrugDesc("CHLORPHENIRAMINE MALEATE SYRUP 2MG/5ML 120ML");
		DmProcureInfo dg09ProcureInfo = new DmProcureInfo();
		DmProcureSummary dmProcureSummary9 = new DmProcureSummary();
		DqaDrugSupplier dqaDrugSupplier9 = new DqaDrugSupplier();
		dg09ProcureInfo.setOrderType("C");
		dmProcureSummary9.setAverageUnitPrice(1.9);
		dqaDrugSupplier9.setItemCode("CHLO2F");
		dqaDrugSupplier9.setRegStatus("N");
		dqaDrugSupplier9.setReleaseIndicator("Y");
		dg09.setDmProcureInfo(dg09ProcureInfo);
		dg09.setDmProcureSummary(dmProcureSummary9);
		dg09.setDqaDrugSupplier(dqaDrugSupplier9);
		dg09.setDangerousDrug("N");
		
		dmList.add(dg09);
		
		return dmList;
		
		
	}
	
	@SuppressWarnings("serial")
	public static class InnerDmDrug extends DmDrug{
		
		private String fullDrugDesc;
		
		public String getFullDrugDesc(){	
			System.out.println("**** " +fullDrugDesc);
			return fullDrugDesc;
		}
		
		public void setFullDrugDesc(String fullDrugDescIn){
			fullDrugDesc = fullDrugDescIn;
		}
	}

	@Override
	public List<DmDrug> getDrugList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initDrugCache(List<DmDrug> dmDrugList) {
		// TODO Auto-generated method stub
	}
	
}

