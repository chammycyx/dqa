package hk.org.ha.model.pms.dqa.vo;

import hk.org.ha.model.pms.dqa.udt.CancelFlag;
import hk.org.ha.model.pms.dqa.udt.ClosureStatus;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="ContractLine")
@XmlType(propOrder={"contractLineId","contractLineNum","itemCode","itemDesc",
					"unitPrice","unitOfMeasure","unitOfMeasureName","baseUnit","packSize",
					"buyQty","getFreeQty","effectiveEndDate","cancelFlag","closureStatus","contractMsps"})
public class ContractLine {

	private Long contractLineId;
	private String contractLineNum;
	private String itemCode;
	private String itemDesc;
	private Double unitPrice;
	private String unitOfMeasure;
	private String unitOfMeasureName;
	private String baseUnit;
	private Integer packSize;
	private Integer buyQty;
	private Integer getFreeQty;
	private Date effectiveEndDate;
	private CancelFlag cancelFlag;
	private ClosureStatus closureStatus;
	private List<ContractMsp> contractMsps;

	public void setContractLineId(Long contractLineId) {
		this.contractLineId = contractLineId;
	}
	
	@XmlElement(name="contract_line_id")
	public Long getContractLineId() {
		return contractLineId;
	}

	public void setContractLineNum(String contractLineNum) {
		this.contractLineNum = contractLineNum;
	}
	
	@XmlElement(name="contract_line_num")
	public String getContractLineNum() {
		return contractLineNum;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	@XmlElement(name="item_code")
	public String getItemCode() {
		return itemCode;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	
	@XmlElement(name="item_description")
	public String getItemDesc() {
		return itemDesc;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	@XmlElement(name="unit_price")
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	@XmlElement(name="unit_of_measure")
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasureName(String unitOfMeasureName) {
		this.unitOfMeasureName = unitOfMeasureName;
	}
	
	@XmlElement(name="unit_of_measure_name")
	public String getUnitOfMeasureName() {
		return unitOfMeasureName;
	}

	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
	@XmlElement(name="base_unit")
	public String getBaseUnit() {
		return baseUnit;
	}

	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}
	
	@XmlElement(name="pack_size")
	public Integer getPackSize() {
		return packSize;
	}

	public void setBuyQty(Integer buyQty) {
		this.buyQty = buyQty;
	}	
	
	@XmlElement(name="buy_quantity")
	public Integer getBuyQty() {
		return buyQty;
	}

	public void setGetFreeQty(Integer getFreeQty) {
		this.getFreeQty = getFreeQty;
	}
	
	@XmlElement(name="get_free_quantity")
	public Integer getGetFreeQty() {
		return getFreeQty;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		if (effectiveEndDate != null){
			this.effectiveEndDate = new Date(effectiveEndDate.getTime());
		}
	}
	
	@XmlElement(name="effective_end_date")
	public Date getEffectiveEndDate() {
		return (effectiveEndDate != null) ? new Date(effectiveEndDate.getTime()) : null;
	}

	public void setCancelFlag(CancelFlag cancelFlag) {
		this.cancelFlag = cancelFlag;
	}
	
	@XmlElement(name="cancel_flag")
	public CancelFlag getCancelFlag() {
		return cancelFlag;
	}

	public void setClosureStatus(ClosureStatus closureStatus) {
		this.closureStatus = closureStatus;
	}
	
	@XmlElement(name="closure_status")
	public ClosureStatus getClosureStatus() {
		return closureStatus;
	}

	public void setContractMsps(List<ContractMsp> contractMspList) {
		this.contractMsps = contractMspList;
	}
	
	@XmlElementWrapper(name="ContractMSPs")
	@XmlElement(name="ContractMSP", type=ContractMsp.class)
	public List<ContractMsp> getContractMsps() {
		return contractMsps;
	}
	
	@Override
	public String toString() {
		return "ContractLine [contractLineId=" + contractLineId + 
		", contractLineNum=" + contractLineNum + 
		", itemCode=" + itemCode + 
		", itemDesc=" + itemDesc + 
		", unitPrice=" + unitPrice + 
		", unitOfMeasure=" + unitOfMeasure + 
		", unitOfMeasureName=" + unitOfMeasureName + 
		", baseUnit=" + baseUnit + 
		", packSize=" + packSize + 
		", buyQty=" + buyQty + 
		", getFreeQty=" + getFreeQty + 
		", effectiveEndDate=" + effectiveEndDate + 
		", cancelFlag=" + cancelFlag + 
		", closureStatus=" + closureStatus + 
		", contractMsps=" + contractMsps + "]"; 
	}
}
