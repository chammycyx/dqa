<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:tv="org.granite.tide.validators.*"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("coaFileInventoryView")]
	</fx:Metadata>

	<fx:Declarations>
		<mx:StringValidator maxLength="6" source="{txtItemCode}" property="text" required="true"/>
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[	
			import hk.org.ha.event.pms.dqa.RetrieveSupplierCodeListEvent;
			import hk.org.ha.event.pms.dqa.coa.RefreshCoaFileInventoryViewEvent;
			import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListByCriteriaEvent;
			import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
			import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
			import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
			import hk.org.ha.model.pms.dqa.udt.FileCategory;
			import hk.org.ha.model.pms.dqa.udt.OrderType;
			import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
			import hk.org.ha.model.pms.dqa.vo.coa.CoaFileFolderCriteria;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			
			import mx.collections.ArrayCollection;
			import mx.controls.Button;
			import mx.controls.LinkButton;
			import mx.events.CalendarLayoutChangeEvent;
			import mx.events.FlexEvent;
			import mx.events.ItemClickEvent;
			
			import spark.events.IndexChangeEvent;
			
			private var coaFileFolder:CoaFileFolder;
			private var coaBatch:CoaBatch;
			private var coaItem:CoaItem;
			private var todayDate:Date;
			
			private var msgProp:SystemMessagePopupProp;
			
			[In] [Bindable]
			public var supplierCodeList:ArrayCollection;
			
			[In] [Bindable]
			public var coaFileFolderInventoryList:ArrayCollection;
			
			[Bindable]
			public var fileCat:ArrayCollection = new ArrayCollection(FileCategory.constants);
			
			[Bindable]
			public var orderType:ArrayCollection = new ArrayCollection(OrderType.constants);
			
			[Bindable]
			private var coaFileFolderCriteria:CoaFileFolderCriteria;
						
			public override function onShowLater():void{	
				clear();
			}
			
			public override function onHide():void{
				fileItemGrid.dataProvider = null;
				coaFileFolderInventoryList = null;
			}
			
			public function refreshView():void{
				if (supplierCodeList !=null) {
					supplierCodeList.addItemAt("All",0);
					suppCodeDropDownList.selectedIndex=0;
				}
			}
			
			public override function onShow():void 
			{	
				var event:Event = new RefreshCoaFileInventoryViewEvent();
				dispatchEvent(new RetrieveSupplierCodeListEvent(event));
				
				if (!toolbar.hasInited()) {
					toolbar.retrieveFunc = retrieve;
					toolbar.clearFunc = clear;
					toolbar.init();
				}
				
				txtItemCode.setFocus();
			}		
			
			private function clear():void{
				resetForm(pSearch);
				txtItemCode.errorString = "";
				dfdStartDate.errorString = "";
				dfdEndDate.errorString = "";
				fileItemGrid.dataProvider = null;
				txtContractNo.enabled = true;
				suppCodeDropDownList.selectedIndex = 0;
				categoryDropDownList.selectedIndex = 0;
				orderTypeDropDownList.selectedIndex = 0;
				
				todayDate = new Date();
				dfdStartDate.selectedDate = new Date( (todayDate.fullYear - 1),todayDate.month,(todayDate.date +1) );
				dfdEndDate.selectedDate = todayDate;
				
				coaFileFolderCriteria = new CoaFileFolderCriteria();
				coaFileFolderCriteria.batchNum ="";
				coaFileFolderCriteria.contractNum ="";
				coaFileFolderCriteria.startDate = new Date( (todayDate.fullYear - 1),todayDate.month,(todayDate.date +1) );
				coaFileFolderCriteria.endDate = todayDate;
			}
			
			private function validate():Boolean{
				if (dfdStartDate.selectedDate == null) {
					this.showMessage("0059");
					dfdStartDate.setFocus();
					return false;
					
				} else if (dfdEndDate.selectedDate == null) {
					this.showMessage("0059");
					dfdEndDate.setFocus();
					return false;
					
				}
				return true;
			}
			
			private function retrieve():void{
				if(validate()
				   && validateForm(pSearch, suppCodeDropDownList, categoryDropDownList, 
				                            orderTypeDropDownList, txtContractNo, txtBatchNo, dfdStartDate, dfdEndDate)){
					(fileItemGrid.dataProvider as ArrayCollection).sort = null;
					(fileItemGrid.dataProvider as ArrayCollection).refresh();
		
					coaFileFolderCriteria.itemCode = coaFileFolderCriteria.itemCode; 
					coaFileFolderCriteria.contractNum = coaFileFolderCriteria.contractNum; 
					coaFileFolderCriteria.batchNum = coaFileFolderCriteria.batchNum;
					coaFileFolderCriteria.supplierCode =suppCodeDropDownList.selectedItem as String ;
					coaFileFolderCriteria.fileCat = categoryDropDownList.selectedItem as FileCategory ;
					coaFileFolderCriteria.orderType = orderTypeDropDownList.selectedItem as OrderType;
					coaFileFolderCriteria.startDate = dfdStartDate.selectedDate; 
					coaFileFolderCriteria.endDate = dfdEndDate.selectedDate;
					
					fileItemGrid.dataProvider = coaFileFolderInventoryList;
					dispatchEvent(
						new RetrieveCoaFileFolderListByCriteriaEvent(
							coaFileFolderCriteria));
				}	
			}
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			private function orderType_changeHandler(evt:IndexChangeEvent):void{
				if (evt.currentTarget.selectedItem !=null){
					if (evt.currentTarget.selectedItem.dataValue == "D"){
						txtContractNo.enabled = false;
						coaFileFolderCriteria.contractNum ="";
					
					}else {		
						txtContractNo.enabled = true;				
					}
				}		
			}
			
			private function orderTypeDropDownListFunc(item: Object):String{
				if (item != null){
					return item.dataValue
				}else {
					return "";
				}
			}
			
			protected function date_changeHandler(event:CalendarLayoutChangeEvent):void
			{				
				if( dfdStartDate!=null && dfdEndDate !=null ){
					try{
						if (dfdStartDate.selectedDate.time > dfdEndDate.selectedDate.time){					
							if (dfdStartDate.errorString.length == 0 )
								dfdStartDate.errorString="Begin date must smaller than end date!";
							if (dfdEndDate.errorString.length == 0)
								dfdEndDate.errorString="Begin date must smaller than end date!";
						}else {
							dfdStartDate.errorString="";	
							dfdEndDate.errorString="";
						}
					}
					catch(e:Error){
					}
				}
			}
			
			private function createDateLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return dateFormatter.format(item.createDate);
			}
			
			private function effectiveDateLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return dateFormatter.format(item.effectiveDate);
			}
			
			private function passDateLabelFunc(item:Object, column:DataGridColumn):String 
			{
					return dateFormatter.format(item.coaPassDate);
				
			}

			protected function dfdStartDate_changeHandler(event:CalendarLayoutChangeEvent):void
			{
				// TODO Auto-generated method stub
			}
			
			private function fileCatLabelFunc(data:Object):String 
			{
				return data.displayValue;
			}
			
			private function categoryLabelFunc(item:Object, column:DataGridColumn):String 
			{
				return item.fileCat.displayValue;
			}

		]]>
	</fx:Script>

	<fc:layout>
		<s:VerticalLayout gap="0"/>
	</fc:layout>
	
	<fc:Toolbar id="toolbar" width="100%"/>
	
	<s:VGroup width="100%" height="100%" gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10">
		<s:HGroup width="100%" gap="10">
			<mx:Panel id="pSearch" width="100%" title="Search">
				<s:VGroup id="ctrlGrp" width="100%" paddingTop="10" paddingBottom="10">
					<s:HGroup height="100%" width="100%" verticalAlign="middle" paddingLeft="10">
						<s:Label text="Item Code" width="70"/>		
						<fc:UppercaseTextInput id="txtItemCode" text="@{coaFileFolderCriteria.itemCode}" width="100" maxChars="6"/>
						<mx:Spacer width="50"/>						
						<s:Label text="Supplier Code" width="90"/>
						<s:DropDownList id="suppCodeDropDownList" dataProvider="{supplierCodeList}"  width="100"  selectedIndex="0"  requireSelection="false"/>
						<mx:Spacer width="50"/>
						<s:Label text="Category" width="60" />
						<s:DropDownList id="categoryDropDownList"  dataProvider="{fileCat}" labelFunction="fileCatLabelFunc"
										width="180" selectedIndex="0"  requireSelection="false" >
						</s:DropDownList>						
					</s:HGroup>
					<s:HGroup height="100%" width="100%" verticalAlign="middle"  paddingLeft="10" >
						<s:Label text="Order Type" width="70" />
						<s:DropDownList id="orderTypeDropDownList" labelFunction="orderTypeDropDownListFunc" dataProvider="{orderType}" change="orderType_changeHandler(event);" selectedIndex="0"  width="100"/>
						<mx:Spacer width="50"/>
						<s:Label text="Contract No." width="90"/>
						<fc:UppercaseTextInput id="txtContractNo" text="@{coaFileFolderCriteria.contractNum}" width="100" />
						<mx:Spacer width="50"/>
						<s:Label text="Batch No." id="batchNo"  width="60"/>
						<fc:UppercaseTextInput id="txtBatchNo" maxChars="20" text="@{coaFileFolderCriteria.batchNum}" width="180"/>
					</s:HGroup>	
					<s:HGroup height="100%" width="100%" verticalAlign="middle"  paddingLeft="10">
						<s:Label text="Create Date" width="70" />
						<calendar:AbbDateField editable="true" id="dfdStartDate" width="126" yearNavigationEnabled="true" change="date_changeHandler(event)"/>
						<s:HGroup height="100%" verticalAlign="middle">
							<s:Label top="10" text=" to "/>
						</s:HGroup>
						<calendar:AbbDateField editable="true" id ="dfdEndDate" yearNavigationEnabled="true" width="126" change="date_changeHandler(event)"/>
					</s:HGroup>	
				</s:VGroup>
			</mx:Panel>	
		</s:HGroup>
		
		<s:Group width="100%" height="90%" left="0" right="10" top="0" bottom="10">
			<mx:DataGrid id="fileItemGrid" variableRowHeight="true" wordWrap="true" 
						 dataProvider="{coaFileFolderInventoryList}" width="100%" height="100%"
						 verticalScrollPolicy="on"> 
				<mx:columns>
					<mx:DataGridColumn headerText="Name" width="0.3"  dataField="fileItem.fileName"
									   draggable="false" resizable="false">                                   
						<mx:itemRenderer>	
							<fx:Component>
								<s:MXDataGridItemRenderer>
									<s:Group  width="100%" height="100%" clipAndEnableScrolling="true" verticalCenter="3">
										<s:Label text="{data.fileItem.fileName}" fontWeight="normal"   textDecoration="underline"  color="blue" toolTip="Click to Open"  
												 click="viewPdf(data.fileItem)" >
											<fx:Script>
												<![CDATA[   
													import hk.org.ha.event.pms.dqa.OpenPdfDocumentEvent;
													import hk.org.ha.model.pms.dqa.persistence.FileItem;
													private function viewPdf(data:FileItem):void {	
														dispatchEvent(new OpenPdfDocumentEvent(data, "_blank"));
													}	
												]]>
											</fx:Script> 
										</s:Label>
									</s:Group>
								</s:MXDataGridItemRenderer>
							</fx:Component>
						</mx:itemRenderer>						
					</mx:DataGridColumn>
					
					<mx:DataGridColumn headerText="Category"  dataField="fileCat" labelFunction="categoryLabelFunc" itemRenderer="mx.controls.Label" 
									   draggable="false" resizable="false" width="0.1" />
					
					<mx:DataGridColumn headerText="Contract No." dataField="coaBatch.coaItem.contract.nonNullContractNum"
									   draggable="false" resizable="false" width="0.1"/>
					
					<mx:DataGridColumn headerText="Batch No." width="0.1" dataField="coaBatch.nonNullBatchNum"
									   draggable="false" resizable="false"/>
					
					<mx:DataGridColumn headerText="Description" width="0.15" dataField="fileItem.fileDesc"
									   draggable="false" resizable="false" itemRenderer="mx.controls.Label"/>
					
					<mx:DataGridColumn headerText="Create Date" width="0.1" dataField="fileItem.createDate"
									   draggable="false" resizable="false" labelFunction="createDateLabelFunc"/>
					
					<mx:DataGridColumn headerText="Effective Date" width="0.1"  dataField="effectiveDate"
									   draggable="false" resizable="false" labelFunction="effectiveDateLabelFunc"/>
					
					<mx:DataGridColumn headerText="Pass Date" width="0.1" dataField="coaPassDate"
									   draggable="false" resizable="false" labelFunction="passDateLabelFunc"/>
					
					<mx:DataGridColumn headerText="Pass By" width="0.1" dataField="coaPassBy"
									   draggable="false" resizable="false"/>
				</mx:columns>
			</mx:DataGrid>	
		</s:Group>
	</s:VGroup>
</fc:ExtendedNavigatorContent>
