<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:tv="org.granite.tide.validators.*"
	xmlns:tsv="org.granite.tide.seam.validators.*"
	width="100%" height="100%">
	<fx:Metadata>
		[Name("userInfoPopup")]
	</fx:Metadata>
	
	<fx:Declarations>
		
		<tv:TideInputValidator required="true" source="{txtUserCode}" property="text" entity="{userInfo}" entityProperty="userCode"/>
		<tv:TideInputValidator required="true" source="{txtFirstName}" property="text" entity="{userInfo.contact}" entityProperty="firstName"/>
		<tv:TideInputValidator required="true" source="{txtLastName}" property="text" entity="{userInfo.contact}" entityProperty="lastName"/>
		<tsv:TideControlValidator id="validatorUserCode" enabled="false" source="{txtUserCode}" property="text" required="false" />
		<tsv:TideControlValidator id="validatorFirstName" enabled="false" source="{txtFirstName}" property="text" required="false" />
		<tsv:TideControlValidator id="validatorLastName" enabled="false" source="{txtLastName}" property="text" required="false" />
		<tsv:TideControlValidator id="validatorEmail" enabled="false" source="{txtEmail}" property="text" required="false"  />
		<mx:PhoneNumberValidator minDigits="8"  allowedFormatChars="()- .+"  source="{txtOfficePhone}" property="text" required="false"/>
		<mx:PhoneNumberValidator minDigits="8" allowedFormatChars="()- .+"  source="{txtFax}" property="text" required="false"/>
		<mx:EmailValidator source="{txtEmail}" property="text" required="true"/>
		
	</fx:Declarations>
	
	<fc:states>		
		<s:State name="Add"/>
		<s:State name="Edit"/>		
	</fc:states>
	
	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.dqa.CreateUserInfoEvent;
			//import hk.org.ha.event.pms.dqa.RetrieveUserInfoListByRecordStatusEvent;
			import hk.org.ha.event.pms.dqa.UpdateUserInfoEvent;
			import hk.org.ha.event.pms.dqa.show.ShowUserInfoViewEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dqa.persistence.UserInfo;
			import hk.org.ha.model.pms.dqa.udt.RecordStatus;
			import hk.org.ha.model.pms.dqa.udt.TitleType;
			
			import mx.collections.ArrayCollection;
			import mx.events.ValidationResultEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			
			import spark.components.DropDownList;
			import spark.events.IndexChangeEvent;
			
			[In][Bindable]
			public var userInfo:UserInfo;
			
			
			[Bindable]
			public var titleTypeList:ArrayCollection = new ArrayCollection(TitleType.constants);
			
			public var msgProp:SystemMessagePopupProp;	
			
			public function refreshView():void{
				
			}
			
			private function setContactTitleList(titlename:TitleType, thisObject:Object):int
			{			
				
				var selectedIndex:int = 0;
				
				if ( titleTypeList.getItemAt(0) == TitleType.NIL){
					titleTypeList.removeItemAt(0);
				}	
				
				if (titlename != null ){
					
					for (var i:int=0; i<titleTypeList.length; i++){
						if(titleTypeList[i] == titlename)
						{
							selectedIndex = i;
							if (thisObject !=null)
							{
								thisObject.selectedItem = titleTypeList[i];
							}
							break;
						}
					}
				}
				return selectedIndex;
			}
			
			
			protected function numValidator_validHandler(obj:DropDownList, evt:ValidationResultEvent):void {
				obj.errorString = "";
			}
			
			protected function numValidator_invalidHandler(obj:DropDownList, evt:ValidationResultEvent):void {
				obj.errorString = "This field is required."
			}
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{ 
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			public function showYNMessage(errorCode:String, yesfunc:Function=null, nofunc:Function=null):void{ 
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				msgProp.setYesNoButton = true;
				msgProp.yesHandler = yesfunc;
				msgProp.noHandler = nofunc;
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			protected function save():void{
				if (cbxTitle.selectedIndex < 0 ){
					cbxTitle.errorString = "This field is required."
				}else if(cbxTitle.selectedItem == TitleType.NIL){
					cbxTitle.errorString = "This field is required."
				}else{
					cbxTitle.errorString = "";
					
					if (userInfo!=null && validateForm(pUserInfo)) {
						showYNMessage("0038",
							okSave,
							function(evt:Event){
								var temp:UIComponent = evt.currentTarget as UIComponent;
								PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
							}
						);
					}
				}
			}
			
			protected function okSave(evt:Event):void{
				
					validatorUserCode.enabled= true;
					validatorFirstName.enabled= true;
					validatorLastName.enabled= true;
					validatorEmail.enabled = true;
					
					userInfo.userCode = StringUtil.trim(txtUserCode.text).toLowerCase();
					userInfo.contact.title = cbxTitle.selectedItem;
					userInfo.contact.firstName = StringUtil.trim(txtFirstName.text);
					userInfo.contact.lastName = StringUtil.trim(txtLastName.text);
					userInfo.contact.position = StringUtil.trim(txtPosition.text);
					userInfo.contact.officePhone = StringUtil.trim(txtOfficePhone.text);
					userInfo.contact.fax = StringUtil.trim(txtFax.text);
					userInfo.contact.email = StringUtil.trim(txtEmail.text.toLowerCase());
					userInfo.contact.address1 = StringUtil.trim(txtAddress1.text);
					userInfo.contact.address2 = StringUtil.trim(txtAddress2.text);
					userInfo.contact.address3 = StringUtil.trim(txtAddress3.text);
					
					validatorUserCode.enabled= true;
					validatorFirstName.enabled= true;
					validatorLastName.enabled= true;
					validatorEmail.enabled = true;
					
					var event:Event = new ShowUserInfoViewEvent();
					if (this.currentState=="Add"){
						dispatchEvent(new CreateUserInfoEvent(userInfo, event));
					}else {
						dispatchEvent(new UpdateUserInfoEvent(userInfo, event));
					}
					
					var temp:UIComponent = evt.currentTarget as UIComponent;
					PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
			}
			
			public function closeWin():void
			{
				PopUpManager.removePopUp(this);
			}
			
			public function cbxEmuLabelFunc(data: Object):String{
				return data.dataValue + ' - ' + data.displayValue;
			}
			
			public function cbxEmuExtLabelFunc(data: Object):String{
				return data.dataValue;
			}
			
			protected function cbxTitle_changeHandler(evt:IndexChangeEvent):void
			{
				if (cbxTitle.selectedIndex < 0 ){
					cbxTitle.errorString = "This field is required."
				}else if(cbxTitle.selectedItem == TitleType.NIL){
					cbxTitle.errorString = "This field is required."
				}else{
					cbxTitle.errorString = "";
				}
			}
		]]>
	</fx:Script>
	
	<s:Panel id="pUserInfo" title.Add="Add User Contact" title.Edit="Edit User Contact" >
		<s:layout>
			<s:VerticalLayout gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10"/>
		</s:layout>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="User Code" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtUserCode" maxChars="6" text="@{userInfo.userCode}" width="100" enabled.Edit="false"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">	
			<s:Label text="Title" width="100" />
			<mx:Spacer width="19"/>
			<fc:ExtendedDropDownList id="cbxTitle" width="100" dataProvider="{titleTypeList}"
									 prompt="" enabled.Add="true" enabled.Edit="true"
									 labelField="dataValue" labelFunction="cbxEmuLabelFunc"
									 extLabelFunction="cbxEmuExtLabelFunc"
									 selectedIndex="{setContactTitleList(userInfo.contact.title, cbxTitle)}"
									 change="cbxTitle_changeHandler(event)"
									 />
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="First Name" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtFirstName" maxChars="40" text="@{userInfo.contact.firstName}" width="100"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Last Name" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtLastName" maxChars="40" text="@{userInfo.contact.lastName}" width="100"/>
		</s:HGroup>
		
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Position" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtPosition" maxChars="50" text="@{userInfo.contact.position}" width="200"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Phone" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtOfficePhone" maxChars="20" text="@{userInfo.contact.officePhone}" width="100"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Fax" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtFax" maxChars="20" text="@{userInfo.contact.fax}" width="100"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Email" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtEmail" maxChars="256" text="@{userInfo.contact.email}" width="200"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Address 1" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtAddress1" maxChars="100" text="@{userInfo.contact.address1}" width="500"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Address 2" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtAddress2" maxChars="100" text="@{userInfo.contact.address2}" width="500"/>
		</s:HGroup>
		<s:HGroup width="100%" horizontalAlign="left" verticalAlign="middle">
			<s:Label text="Address 3" width="100"/>
			<mx:Spacer width="19"/>
			<s:TextInput id="txtAddress3" maxChars="100" text="@{userInfo.contact.address3}" width="500"/>
		</s:HGroup>
		
		<s:HGroup width="100%" horizontalAlign="right">
			<s:Button label="Save" click="save()" />
			<s:Button label="Cancel" click="closeWin()"/>
		</s:HGroup>
	</s:Panel>
</fc:ExtendedNavigatorContent>
