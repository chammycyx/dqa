package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MissingBatchCoaDateType implements StringValuedEnum {


	All("A", "All"),
	Last1Month("1", "Last 1 Month"),
	Last3Month("3", "Last 3 Months"),
	Last6Month("6", "Last 6 Months"),
	Last1Year("12", "Last 1 Year"),
	Others("O", "Others");
	
    private final String dataValue;
	private final String displayValue;

	MissingBatchCoaDateType(final String dataValue, final String displayValue){
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}

	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static class Converter extends StringValuedEnumConverter<MissingBatchCoaDateType> {
		private static final long serialVersionUID = 1L;

		@Override
    	public Class<MissingBatchCoaDateType> getEnumClass() {
    		return MissingBatchCoaDateType.class;
    	}
    }

}
