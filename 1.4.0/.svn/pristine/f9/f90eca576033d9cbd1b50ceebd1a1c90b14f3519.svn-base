<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent 
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*" 
	xmlns:util="hk.org.ha.pms.dqa.util.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 
	xmlns:s="library://ns.adobe.com/flex/spark" 
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	width="100%" height="100%">
	
	<fx:Declarations>
		<!-- Place non-visual elements (e.g., services, value objects) here -->
	</fx:Declarations>
	
	<fx:Metadata>
		[Name("institutionAssignmentPopup")]
	</fx:Metadata>

	<fx:Script>
		<![CDATA[
			import hk.org.ha.event.pms.dqa.sample.RefreshSampleItemListEvent;
			import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemForSampleTestEvent;
			import hk.org.ha.event.pms.dqa.sample.RetrieveAllSampleItemByItemCodeRecordStatusEvent;
			import hk.org.ha.event.pms.dqa.sample.RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent;
			import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent;
			import hk.org.ha.event.pms.dqa.sample.popup.ShowInstitutionAssignmentDetailPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
			import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
			import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
			import hk.org.ha.model.pms.dqa.udt.OrderType;
			import hk.org.ha.model.pms.dqa.udt.RecordStatus;
			import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopup;
			import hk.org.ha.fmk.pms.flex.components.lookup.LookupPopupProp;
			import hk.org.ha.fmk.pms.flex.components.lookup.ShowLookupPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.*;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.utils.PopupUtil;
			
			import mx.collections.ArrayCollection;
			import mx.collections.ICollectionView;
			import mx.collections.IViewCursor;
			import mx.collections.ListCollectionView;
			import mx.controls.Alert;
			import mx.controls.ToolTip;
			import mx.controls.dataGridClasses.DataGridColumn;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.CalendarLayoutChangeEvent;
			import mx.events.FlexEvent;
			import mx.events.ItemClickEvent;
			import mx.events.ListEvent;
			import mx.events.ValidationResultEvent;
			import mx.managers.PopUpManager;
			import mx.utils.StringUtil;
			
			import spark.components.DropDownList;
			import spark.events.DropDownEvent;
			import spark.events.TextOperationEvent; 
			
			private static const screenName:String ="institutionAssignmentPopup";
			
			[In][Bindable]
			public var sampleItemList:ArrayCollection;
			
			[In][Bindable]
			public var riskLevelList:ArrayCollection;
			
			[In][Bindable]
			public var sampleTestList:ArrayCollection;
			
			private var lookupProp:LookupPopupProp;
			public var msgProp:SystemMessagePopupProp;
			
			public function closeWin():void
			{
				PopUpManager.removePopUp(this);
			}
			
			private function sampleItemCodeLookup():void {
				itemDesc.text = "";	
				var event:Event = new RefreshSampleItemListEvent(screenName);
				dispatchEvent(new RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent(instAssignmentCriteria_itemCode.text, RecordStatus.Active, event));
			}
			
			public function refreshSampleItemList():void{				
				lookupProp = new LookupPopupProp();
				
				lookupProp.columns = [
					LookupPopupProp.getColumn("Item Code","itemCode"), 
					LookupPopupProp.getColumn("Item Description","singleLevelDmDrugFullDrugDesc"),
					LookupPopupProp.getColumn("Order Type","singleLevelOrderType"),
					LookupPopupProp.getColumn("Risk Level","singleLevelRiskLevelCode")
				];
				
				lookupProp.doubleClickHandler = sampleItemLookupDoubleClickHandler;
				lookupProp.okHandler=sampleItemLookupOkHandler;
				lookupProp.title = "Sample Item Lookup";
				lookupProp.lookupWinWidth = 600;
				lookupProp.columns[0].width = 100;
				lookupProp.columns[1].width = 200;
				lookupProp.columns[2].width = 100;
				lookupProp.columns[3].width = 100;
				lookupProp.dataProvider = sampleItemList;
				
				dispatchEvent(new ShowLookupPopupEvent(lookupProp));	
			}
			
			private function sampleItemLookupOkHandler(evt:MouseEvent):void{
				var lookupPopup:LookupPopup = (UIComponent(evt.currentTarget).parentDocument) as LookupPopup;
				var tmpItem:SampleItem = lookupPopup.lookupGrid.selectedItem as SampleItem;
				
				if(lookupPopup.lookupGrid.dataProvider!=null)
				{
					(lookupPopup.lookupGrid.dataProvider as ArrayCollection).sort = null;
					(lookupPopup.lookupGrid.dataProvider as ArrayCollection).refresh();
				}
				
				sampleItemSubmitData(tmpItem, evt);
			}
			
			private function sampleItemLookupDoubleClickHandler(evt:ListEvent):void {
				var sampleItemRetrieve:SampleItem = evt.itemRenderer.data as SampleItem;
				sampleItemSubmitData(sampleItemRetrieve, evt);
			}
			
			private function sampleItemSubmitData(tmpSampleItem:SampleItem , evt:Event):void{
				if ( tmpSampleItem !=null){
					instAssignmentCriteria_itemCode.text = tmpSampleItem.itemCode; 
					itemDesc.text = tmpSampleItem.dmDrug.fullDrugDesc;
					
					var temp:UIComponent = evt.currentTarget as UIComponent;
					PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
				}
			}
			
			public function setSampleItemName(sampleItemIn:SampleItem):void{
				if(sampleItemIn != null && sampleItemIn.dmDrug != null && sampleItemIn.itemCode != null){
					
					itemDesc.text = sampleItemIn.dmDrug.fullDrugDesc.toString();
				}else{
					itemDesc.text = "";
				}
			}
			
			protected function instAssignmentCriteria_itemCode_changeHandler(event:TextOperationEvent):void
			{
				if (instAssignmentCriteria_itemCode.text!="" && instAssignmentCriteria_itemCode.text.length >1){
					btnItemCodeLookup.enabled = true;
					
					itemDesc.text = "";
					if (instAssignmentCriteria_itemCode.text.length >=6){
						dispatchEvent(new RetrieveAllSampleItemByItemCodeRecordStatusEvent(instAssignmentCriteria_itemCode.text, RecordStatus.Active, screenName));
					}
				}else {
					btnItemCodeLookup.enabled = false;
					if (itemDesc.text != ""){
						itemDesc.text = "";
					}
				}
			}
			
			
			protected function instAssignmentCriteria_schedMonth_changeHandler(event:CalendarLayoutChangeEvent):void
			{
				initScheduleMonth();
			}
			
			protected function instAssignmentCriteria_schedMonth_focusOutHandler(event:FocusEvent):void
			{
				initScheduleMonth();
			}
			
			private function initScheduleMonth():void{
				var tmpDate:Date = instAssignmentCriteria_schedMonth.selectedDate as Date;
				if (tmpDate !=null && tmpDate.date>1){
					instAssignmentCriteria_schedMonth.selectedDate = new Date(tmpDate.fullYear, tmpDate.month,  1 );
				}	
			}
			
			public function showMessage(errorCode:String, oKfunc:Function=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				
				if (oKfunc != null){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
				}else {
					msgProp.setOkButtonOnly = true;
				}
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
				
			protected function assign():void{
				var cnt:int = 0;
				var instAssignmentCriteriaIn:InstAssignmentCriteria = new InstAssignmentCriteria;
				if(instAssignmentCriteria_itemCode.text != "" &&
					itemDesc.text!= "")
				{
					instAssignmentCriteriaIn.itemCode = instAssignmentCriteria_itemCode.text;
					cnt++;
				}
				
				if(instAssignmentCriteria_schedMonth.selectedDate != null)
				{
					instAssignmentCriteriaIn.scheduleMonth = instAssignmentCriteria_schedMonth.selectedDate;
					cnt++;
				}
				
				if(instAssignmentCriteria_testCode.selectedIndex != -1)
				{
					instAssignmentCriteriaIn.testCode = (instAssignmentCriteria_testCode.selectedItem == null?null:(instAssignmentCriteria_testCode.selectedItem as SampleTest).testCode);
					cnt++;
				}
				
				if(instAssignmentCriteria_orderType.selectedIndex != -1)
				{
					instAssignmentCriteriaIn.orderType = (instAssignmentCriteria_orderType.selectedItem == null?null:(instAssignmentCriteria_orderType.selectedItem as OrderType));
					cnt++;
				}
				
				if(instAssignmentCriteria_riskLevel.selectedIndex != -1)
				{
					instAssignmentCriteriaIn.riskLevelCode = (instAssignmentCriteria_riskLevel.selectedItem == null?null:(instAssignmentCriteria_riskLevel.selectedItem as RiskLevel).riskLevelCode);
					cnt++;
				}
				
				
				var event:Event = new ShowInstitutionAssignmentDetailPopupEvent();
				var retrieveEvent:Event = new RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent(instAssignmentCriteriaIn,event);
				if(cnt>=2)
				{
					dispatchEvent(retrieveEvent);
					PopUpManager.removePopUp(this);
				}
				else
				{
					showMessage("0062");
				}
			}
			
			private function cbxOpen(evt:DropDownEvent, obj:DropDownList, widthIn:int):void {
				obj.dropDown.width = widthIn;
			}
			
			public function cbxEmuLabelFunc(data: Object):String{
				return data.dataValue + ' - ' + data.displayValue;
			}
			
			public function cbxEmuExtLabelFunc(data: Object):String{
				return data.dataValue;
			}
			
			public function cbxTestCodeLabelFunc(data:SampleTest):String{
				return data.testCode +' - ' + data.testName;
			}
			
			public function cbxTestCodeExtLabelFunc(data:SampleTest):String{
				return data.testCode;
			}
			
			public function cbxRiskLevelLabelFunc(data:RiskLevel):String{
				return data.riskLevelCode +' - ' + data.riskLevelDesc;
			}
			
			public function cbxRiskLevelExtLabelFunc(data:RiskLevel):String{
				return data.riskLevelCode;
			}
			
		]]>
	</fx:Script>
	
	<s:Panel id="pInstitutionAssignment" title="Institution Assignment" width="100%" height="100%" >
		<s:VGroup width="100%" height="100%" paddingTop="10" paddingBottom="10" paddingLeft="10" paddingRight="10">
			<s:HGroup verticalAlign="middle" width="100%">
				<s:Label text="By Item Code" width="140"/>
				<fc:UppercaseTextInput id="instAssignmentCriteria_itemCode" maxChars="6" width="120" change="instAssignmentCriteria_itemCode_changeHandler(event)"/>
				<util:LookupButton id="btnItemCodeLookup" enabled="false" click="sampleItemCodeLookup()" />
				<s:TextInput id="itemDesc" width="300" text="" enabled="false"/>
			</s:HGroup>
			<s:HGroup verticalAlign="middle">
				<fx:Script>
					<![CDATA[
						import mx.events.CalendarLayoutChangeEvent;
					]]>
				</fx:Script>
				<s:Label text="By Schedule Month" width="140"/>
				<calendar:AbbDateField id="instAssignmentCriteria_schedMonth" width="120" 
									   change="instAssignmentCriteria_schedMonth_changeHandler(event)" focusOut="instAssignmentCriteria_schedMonth_focusOutHandler(event)"/>
			</s:HGroup>
			<s:HGroup verticalAlign="middle">
				<s:Label text="By Test" width="140"/>
				<fc:ExtendedDropDownList id="instAssignmentCriteria_testCode" dataProvider="{sampleTestList}" width="120"
										 labelField="testCode" labelFunction="cbxTestCodeLabelFunc"
										 extLabelFunction="cbxTestCodeExtLabelFunc"
										 open="cbxOpen(event, instAssignmentCriteria_testCode, 200)">
				</fc:ExtendedDropDownList>
			</s:HGroup>
			<s:HGroup verticalAlign="middle">
				<s:Label text="By Order Type" width="140" />
				<fc:ExtendedDropDownList id="instAssignmentCriteria_orderType" dataProvider="{new ArrayCollection( OrderType.constants)}" width="120"
										 labelField="dataValue" labelFunction="cbxEmuLabelFunc"
										 extLabelFunction="cbxEmuExtLabelFunc"
										 open="cbxOpen(event, instAssignmentCriteria_orderType, 220)">
				</fc:ExtendedDropDownList>
			</s:HGroup>
			<s:HGroup verticalAlign="middle">
				<s:Label text="By Risk Level" width="140" />
				<fc:ExtendedDropDownList id="instAssignmentCriteria_riskLevel" dataProvider="{riskLevelList}" width="120"
										 labelField="riskLevelCode" labelFunction="cbxRiskLevelLabelFunc"
										 extLabelFunction="cbxRiskLevelExtLabelFunc"
										 open="cbxOpen(event, instAssignmentCriteria_riskLevel, 200)">
				</fc:ExtendedDropDownList>
			</s:HGroup>
			<s:HGroup width="100%" horizontalAlign="right" >
				<s:Button label="Assign" click="assign()"/>
				<s:Button label="Cancel" click="closeWin()"/>
			</s:HGroup>
		</s:VGroup>	
	</s:Panel>
</fc:ExtendedNavigatorContent>
