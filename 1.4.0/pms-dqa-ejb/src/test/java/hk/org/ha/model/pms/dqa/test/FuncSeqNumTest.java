package hk.org.ha.model.pms.dqa.test;

import hk.org.ha.model.pms.dqa.biz.CompanyContactServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.udt.TitleType;

import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class FuncSeqNumTest extends SeamTest 
{
	@Test 
	public void testFuncSeqNumComponent() throws Exception
	{
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{
				//Login 
	            invokeMethod("#{identity.login}");
	            
	            // funcSeqNumService
	            Integer nextNum = (Integer)invokeMethod("#{funcSeqNumService.getNextFuncSeqNum('SN')}");
				assert nextNum > 0;
				
				//retrieveNextFRNSeqNum
				invokeMethod("#{funcSeqNumService.retrieveNextFRNSeqNum()}");
			}		
		}.run();
	}
}
