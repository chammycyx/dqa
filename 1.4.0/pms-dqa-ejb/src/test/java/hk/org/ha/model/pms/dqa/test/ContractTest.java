package hk.org.ha.model.pms.dqa.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ContractTest extends SeamTest {
	@Test
	public void contractComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //retrieve List
	            invokeMethod("#{contractListService.retrieveContractListLikeContractByItemCode('','','PARA01')}");
	            List<Contract> contractListFind = (List<Contract>)getValue("#{contractList}");
	            assert contractListFind.size() >0;
	            
	            Contexts.getSessionContext().set("orderTypeIn", OrderType.Contract);
	            
	            invokeMethod("#{contractListService.retrieveContractListForSampleTest('','',orderTypeIn,'PARA01')}");
	            contractListFind = (List<Contract>)getValue("#{contractList}");
	            assert contractListFind.size() >=0;
	            
	            //retrieve DP Contract
	            Contexts.getSessionContext().set("moduleTypeIn", ModuleType.SampleTest);
	            Contract contractFind = (Contract)invokeMethod("#{contractManager.retrieveDirectPurchaseContract('PARA02','ABBO', moduleTypeIn)}");
	            assert contractFind!=null;
	            
	            //retrieveContractForSampleTest
	            
	            invokeMethod("#{contractService.retrieveContractForSampleTest('HOC100-36-1','',orderTypeIn,'PARA01')}");
	            contractFind = (Contract)getValue("#{contract}");
	            assert contractFind!=null;
	            
	            
                //retrieve contractLisr by contractNum, moduleType
	            Contexts.getSessionContext().set("moduleTypeIn", ModuleType.All);
	            contractListFind = (List<Contract>)invokeMethod("#{erpContractListService.retrieveContractListByContractNumModuleType('HOC100-01',moduleTypeIn)}");
	            assert contractListFind.size() >0;
	            
	            //retrieve contractLisr by contractNum, itemCode, moduleType
	            Contexts.getSessionContext().set("moduleTypeIn", ModuleType.All);
	            contractListFind = (List<Contract>)invokeMethod("#{erpContractListService.retrieveContractListByContractNumItemCodeModuleType('HOC100-01','PARA01',moduleTypeIn)}");
	            assert contractListFind.size() >0;
	            
	            //retrieveContractMspListByContractId
	            List<ContractMsp> cmspList = (List<ContractMsp>)invokeMethod("#{erpContractMspListService.retrieveContractMspListByContractId(1)}");
	            
	            //retrieveContractMspByContractIdErpContractMspId
	            ContractMsp cmOut = (ContractMsp)invokeMethod("#{erpContractMspService.retrieveContractMspByContractIdErpContractMspId(1,1)}"); 
	            assert cmOut==null;
	            
	            //retrieveContractListLikeContractByItemCodeOrderType
	            Contexts.getSessionContext().set("orderTypeAllIn", OrderTypeAll.Contract);
	            invokeMethod("#{contractListService.retrieveContractListLikeContractByItemCodeOrderType('HOC100-36-1','','PARA01',orderTypeAllIn)}");
	            
	            
	       }
		}.run();
	}

}
