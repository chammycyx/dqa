package hk.org.ha.model.pms.dqa.test;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.InterfaceFlag;

import java.util.List;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ErpInterfaceTest extends SeamTest {

	@Test 
	public void testErpContractComponent() throws Exception {
	
		new ComponentTest() {
					
			protected void testComponents() throws Exception {
				Boolean isSuccess = (Boolean)invokeMethod("#{erpContractImportService.importErpContractTest('testDrugContractCreation.xml','D_ERP_CONTRACT')}");
				assert isSuccess == true;
				
				isSuccess = (Boolean)invokeMethod("#{erpContractImportService.importErpContractTest('testDrugContractSuspension.xml','D_ERP_CONTRACT')}");
				assert isSuccess == true;
				
				isSuccess = (Boolean)invokeMethod("#{erpContractImportService.importErpContractTest('testDrugContractAppend.xml','D_ERP_CONTRACT')}");
				assert isSuccess == true;
			}
			
		}.run();
		
	}
	
	@Test 
	public void testCoaResultComponent() throws Exception {
	
		new ComponentTest() {
								
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
	
				List<CoaBatch> coaBatchList = (List<CoaBatch>)invokeMethod("#{coaBatchListService.retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus()}");
				assert coaBatchList.size() > 0;
				
				for( CoaBatch cb:coaBatchList ) {
					assert cb.getCoaBatchVer().getCoaStatus() == CoaStatus.Pass;
					assert cb.getInterfaceFlag() == InterfaceFlag.No;					
					invokeMethod("#{coaBatchService.retrieveCoaBatchByCoaBatchId("+cb.getCoaBatchId()+")}");					
					invokeMethod("#{coaBatchService.updateCoaBatchForErpInterface( coaBatch )}");
					CoaBatch coaBatch = (CoaBatch) getValue("#{coaBatch}");
					assert coaBatch.getInterfaceDate() != null;
					assert coaBatch.getInterfaceFlag() == InterfaceFlag.Yes;
				}
				
			}
			
		}.run();
		
	}
	
}
