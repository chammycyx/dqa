package hk.org.ha.model.pms.dqa.coa.test;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaEmailEnquiryTest extends SeamTest {
	
	@Test
	public void testIncomingEmailEnqComponent() throws Exception {
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception {
				// Login
	            invokeMethod("#{identity.login}");
	            invokeMethod("#{coaInEmailListService.testRetrieveCoaInEmailListForInEmailEnq()}");	            
	            List<CoaInEmail> coaInEmailList = (List<CoaInEmail>)getValue("#{coaInEmailList}");	            
	            assert coaInEmailList.size() >= 0;
			}
		}.run();
	}
	
	@Test
	public void testOutstandBatchCoaEnqComponent() throws Exception {
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			@Override
			protected void testComponents() throws Exception {
				// Login
	            invokeMethod("#{identity.login}");            
	            invokeMethod("#{coaOutstandBatchListService.testRetrieveCoaOutstandBatchListForOutstandBatchCoa()}");	            
	            List<CoaOutstandBatch> coaOutstandBatchList = (List<CoaOutstandBatch>)getValue("#{coaOutstandBatchList}");	            
	            assert coaOutstandBatchList.size() >= 0;
			}
		}.run();
	}
	
}
