package hk.org.ha.model.pms.dqa.sample.test;

import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleItemMaintTest extends SeamTest {
	@Test
	public void testSampleItemMaintComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login
	            invokeMethod("#{identity.login}");
				
	            //////// Sample Item ////////
	            //Add Sample Item
	            SampleItem si = new SampleItem();

	            invokeMethod("#{riskLevelService.retrieveRiskLevelByRiskLevelCode('L')}");
	            RiskLevel rk = (RiskLevel)getValue("#{riskLevel}");
	            
	            si.setItemCode("5FLU05");
	            si.postLoad();
	            
	            si.setRiskLevel(rk);
	            si.setMicroBioTestQtyReq(MicroBioTestQtyReq.Is10g);
	            si.setChemAnalysisQtyReq(ChemAnalysisQtyReq.Is10Units);
	            si.setValidTestQtyReq(ValidTestQtyReq.Is10g);
	            si.setRecordStatus(RecordStatus.Active);
	            si.setSpeicalCat(SpecialCat.HC);
	            
	            Contexts.getSessionContext().set("sampleItemIn", si);
	            invokeMethod("#{sampleItemService.createSampleItem(sampleItemIn)}");
	              
	            //Retrieve Sample Item
	            Contexts.getSessionContext().set("sampleItemIn", si);
	            invokeMethod("#{sampleItemService.retrieveSampleItemByItemCodeRecordStatus(sampleItemIn.itemCode, sampleItemIn.recordStatus)}");
   
	            SampleItem siFind = (SampleItem)getValue("#{sampleItem}");
	            assert getValue("#{sampleItem!=null}").equals(true); 

	            //Update Sample Item
	            siFind.setRemark("Testing");
	            Contexts.getSessionContext().set("sampleItemIn", siFind);
	            invokeMethod("#{sampleItemService.updateSampleItem(sampleItemIn)}");
	            
	            invokeMethod("#{sampleItemService.retrieveSampleItemByItemCodeRecordStatus(sampleItemIn.itemCode, sampleItemIn.recordStatus)}");
	            assert getValue("#{sampleItem.remark}").equals("Testing"); 
	            
	            //Delete Sample Item
	            siFind = (SampleItem)getValue("#{sampleItem}");
	            Contexts.getSessionContext().set("sampleItemIn", siFind);
	            invokeMethod("#{sampleItemService.deleteSampleItem(sampleItemIn)}");
  
	            SampleItemCriteria siCriteria = new SampleItemCriteria();
	            siCriteria.setRiskLevelCode("L");
	            siCriteria.setRecordStatus(RecordStatus.Active);
	            Contexts.getSessionContext().set("sampleItemMaintCriteria", siCriteria);
	            
	            invokeMethod("#{sampleItemListService.retrieveSampleItemListByCriteria(sampleItemMaintCriteria)}");
	            
	            //validateSampleTestScheduleForCreate
	            SampleTestSchedule sampleTestScheduleIn= new SampleTestSchedule();
	            SampleItem sampleItemIn = new SampleItem();
	            SampleTest sampleTestIn = new SampleTest();
	            
	            sampleTestIn.setTestCode("MICRO");
	            sampleItemIn.setItemCode("PARA01");
	            sampleTestScheduleIn.setScheduleMonth(new Date());
	            sampleTestScheduleIn.setSampleTest(sampleTestIn);
	            sampleTestScheduleIn.setSampleItem(sampleItemIn);
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId(33)}");
	            sampleTestScheduleIn = (SampleTestSchedule)getValue("#{sampleTestSchedule}");
	            
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleIn);
	            invokeMethod("#{sampleTestScheduleValidateService.validateSampleTestScheduleForCreate(sampleTestScheduleIn)}");
	            assert getValue("#{success}")==null; 
	            
	         
	            //validateSampleTestScheduleForUpdate
	            invokeMethod("#{sampleTestScheduleValidateService.validateSampleTestScheduleForUpdate(sampleTestScheduleIn)}");
	            assert getValue("#{success}")==null;
	            
	            //retrieveSampleItemByItemCode
	            siFind =(SampleItem)invokeMethod("#{sampleItemService.retrieveSampleItemByItemCode('PARA01')}");
	            assert siFind!=null;
	            
	            //retrieveSampleItemForSchedule
	            invokeMethod("#{sampleItemService.retrieveSampleItemForSchedule('PARA01')}");
	            siFind = (SampleItem)getValue("#{sampleItem}");
	            assert siFind!=null;

	            //retrieveSampleItemForSampleTest
	            invokeMethod("#{sampleItemService.retrieveSampleItemForSampleTest('PARA01')}");
	            siFind = (SampleItem)getValue("#{sampleItem}");
	            assert siFind!=null;
	            
	            //retrieveAllSampleItemByItemCodeRecordStatus
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            invokeMethod("#{sampleItemService.retrieveAllSampleItemByItemCodeRecordStatus('PARA01', recordStatusIn)}");
	            
	            //retrieveSampleItemForSampleTestValidation
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            siFind =(SampleItem)invokeMethod("#{sampleItemService.retrieveSampleItemForSampleTestValidation('PARA01', recordStatusIn)}");
	            assert siFind!=null;
	            
	            //addSampleItem
	            Contexts.getSessionContext().set("sampleItem", new SampleItem());
	            invokeMethod("#{sampleItemService.addSampleItem()}");
	            
	            //retrieveSampleItemListForSampleTest
	            invokeMethod("#{sampleItemListService.retrieveSampleItemListForSampleTest(recordStatusIn, 'PARA01')}");
	            List<SampleItem> siList = (List<SampleItem>)getValue("#{sampleItemList}");
	            assert siList.size()>0; 
	            
	            //retrieveAllSampleItemListLikeItemCodeRecordStatus
	            invokeMethod("#{sampleItemListService.retrieveAllSampleItemListLikeItemCodeRecordStatus(recordStatusIn, 'PARA01')}");
	            siList = (List<SampleItem>)getValue("#{sampleItemList}");
	            assert siList.size()>0;
	            
	            //getSampleItemExportList
	            siList = (List<SampleItem>)invokeMethod("#{sampleItemListService.getSampleItemExportList()}");
	            assert siList==null;
	            
	            
	            
			}
		}.run();
	}

}
