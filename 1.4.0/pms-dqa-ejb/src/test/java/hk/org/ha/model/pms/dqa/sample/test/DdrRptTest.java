package hk.org.ha.model.pms.dqa.sample.test;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;
import java.util.Date;
import java.text.SimpleDateFormat;
import hk.org.ha.model.pms.dqa.vo.sample.DdrRptCriteria;

public class DdrRptTest extends SeamTest {
	@Test
	public void testDdrRptComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DdrRptCriteria ddrCriteria = new DdrRptCriteria();
				Date startDate;
				Date endDate;
				
				startDate = sdf.parse("2010-01-01 00:00:00");
				endDate = sdf.parse("2011-12-31 00:00:00");
				
				ddrCriteria.setStartDate(startDate);
				ddrCriteria.setEndDate(endDate);
				ddrCriteria.setDangerousDrug("N");
				
				//Login 
	            invokeMethod("#{identity.login}");
				
	            Contexts.getSessionContext().set("ddrRptCriteria", ddrCriteria);
	            
	            invokeMethod("#{ddrRptService.retrieveDdrList(ddrRptCriteria)}");
				}
		}.run();
	}
}
