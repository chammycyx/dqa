package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class LabTestDetailTest extends SeamTest {
	@Test
	public void labTestDetailComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve All Schedule Record with schedule status = "LT"
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.LabTest);
	            
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByRecordStatusScheduleStatus(recordStatusIn,scheduleStatusIn)}");
	            
	            List<SampleTestSchedule> sTSFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleList}");
	            assert sTSFind.size() >0;
	            
	            //Retrieve one of the record with schedule status = "LT"	
	            Contexts.getSessionContext().set("orderTypeIn", OrderType.Contract);
	            
	            invokeMethod("#{sampleTestScheduleListLabDetailService.retrieveSampleTestScheduleListForLabTestByLabTestOrderType('SGS','MICRO',orderTypeIn)}");
	            List<SampleTestSchedule> sTSDFind = (List<SampleTestSchedule>)getValue("#{sampleTestScheduleDetailList}");
	            
	            assert sTSDFind.size() >0;
	            
	            SampleTestSchedule sampleTestScheduleConfirm = sTSFind.get(0);
	            SampleTestSchedule sampleTestScheduleDelete = sTSFind.get(1);
	            SampleTestSchedule sampleTestScheduleReverse = sTSFind.get(2);
	            
	            //Confirm
	
	            	           
	            List<SampleTestSchedule> sTSConfirm=new ArrayList<SampleTestSchedule>();
	            sTSConfirm.add(sampleTestScheduleConfirm);
	            Contexts.getSessionContext().set("sampleTestScheduleIn", sTSConfirm);
	            invokeMethod("#{sampleTestScheduleListLabDetailService.updateSampleTestScheduleForLabTest(sampleTestScheduleIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleConfirm.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.LabCollection);

	            //Delete
	            Contexts.getSessionContext().set("deleteReasonIn", CancelSchedReason.ReferenceStandardNotAvailable);
	            Contexts.getSessionContext().set("deleteSampleTestScheduleIn", sampleTestScheduleDelete);
	            
	            invokeMethod("#{sampleTestScheduleService.deleteSampleTestSchedule(deleteSampleTestScheduleIn,deleteReasonIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleDelete.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.recordStatus}").equals(RecordStatus.Deleted);
	            
	            //Reverse
	            
	            Contexts.getSessionContext().set("reverseSampleTestScheduleIn", sampleTestScheduleReverse);
	            
	            invokeMethod("#{sampleTestScheduleService.reverseSampleTestScheduleStatus(reverseSampleTestScheduleIn)}");
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId("+sampleTestScheduleReverse.getScheduleId()+")}");
	            assert getValue("#{sampleTestSchedule.scheduleStatus}").equals(ScheduleStatus.DrugSample);
	            
	            
	            // retrieve lab list
	            invokeMethod("#{labListService.retrieveLabList()}");
	            List<Lab> labListFind = (List<Lab>)getValue("#{labList}");
	            assert labListFind.size()>0;
	            
	            //retrieveSampleTestScheduleListForLabTestByLabTestOrderType
	            invokeMethod("#{sampleTestScheduleListLabDetailService.retrieveSampleTestScheduleListForLabTestByLabTestOrderType('AAA','MICRO',orderTypeIn)}");
	            

			}
		}.run();
	}

}
