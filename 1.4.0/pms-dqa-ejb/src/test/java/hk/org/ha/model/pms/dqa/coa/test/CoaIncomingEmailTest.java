package hk.org.ha.model.pms.dqa.coa.test;

import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaIncomingEmailTest extends SeamTest {
	@Test
	public void testIncomingCoaExceptionReportComponent() throws Exception{
		new ComponentTest() {
			
			protected void testComponents() throws Exception {				
				Boolean isSuccess = (Boolean)invokeMethod("#{replyEmailService.testReplyMessage('COA Exception Report.xls')}");
				assert isSuccess == true;
			}
		}.run();
	}

	@Test
	public void testIncomingPDFFileComponent() throws Exception{
		new ComponentTest() {
			
			protected void testComponents() throws Exception {				
				Boolean isSuccess = (Boolean)invokeMethod("#{replyEmailService.testReplyMessage('837893_PARA01_HOC100-01_DKSH.pdf')}");
				assert isSuccess == true;
			}
		}.run();
	}
	
	@Test
	public void testReplyEmailComponent() throws Exception{
		new ComponentTest() {
			
			protected void testComponents() throws Exception {				
				Boolean isSuccess = (Boolean)invokeMethod("#{replyEmailService.testReplyMessage('837893_PARA01_HOC100-01_DKSH.pdf')}");
				assert isSuccess == true;
			}
		}.run();
	}
	
}
