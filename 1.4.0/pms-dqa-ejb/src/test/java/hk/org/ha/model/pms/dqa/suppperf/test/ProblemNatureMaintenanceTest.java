package hk.org.ha.model.pms.dqa.suppperf.test;

import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureCat;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.Date;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ProblemNatureMaintenanceTest extends SeamTest {
	@Test
	public void problemNatureMaintenanceComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //add ProblemNatureParam
	            invokeMethod("#{problemNatureParamService.addProblemNatureParam()}");
	            
	            //create ProblemNatureParam
	            ProblemNatureParam problemNatureParamIn = new ProblemNatureParam();
	            problemNatureParamIn.setParamDesc("David Param Test");
	            problemNatureParamIn.setDisplayOrder(99);
	            problemNatureParamIn.setRecordStatus(RecordStatus.Active);
	            
	            Contexts.getSessionContext().set("problemNatureParamIn", problemNatureParamIn);
	            invokeMethod("#{problemNatureParamService.createProblemNatureParam(problemNatureParamIn)}");
	            
	            //select ProblemNatureParam
	            invokeMethod("#{problemNatureParamService.retrieveProblemNatureParamByProblemNatureParamId(problemNatureParamIn)}");
	            assert getValue("#{problemNatureParam!=null}").equals(true);
	            
	            problemNatureParamIn = (ProblemNatureParam)getValue("#{problemNatureParam}");
	            
	            //update ProblemNatureParam
	            problemNatureParamIn.setDisplayOrder(98);
	            Contexts.getSessionContext().set("problemNatureParamIn", problemNatureParamIn);
	            invokeMethod("#{problemNatureParamService.updateProblemNatureParam(problemNatureParamIn)}");
	            
	            
	            //add ProblemNatureCat
	            invokeMethod("#{problemNatureCatService.addProblemNatureCat()}");
	            
	            //create ProblemNatureCat
	            ProblemNatureCat problemNatureCatIn = new ProblemNatureCat();
	            problemNatureCatIn.setCatDesc("David Cat Test");
	            problemNatureCatIn.setDisplayOrder(99);
	            problemNatureCatIn.setRecordStatus(RecordStatus.Active);
	            problemNatureCatIn.setProblemNatureParam(problemNatureParamIn);
	            
	            Contexts.getSessionContext().set("problemNatureCatIn", problemNatureCatIn);
	            invokeMethod("#{problemNatureCatService.createProblemNatureCat(problemNatureCatIn)}");
	            		
	            //select ProblemNatureCat
	            invokeMethod("#{problemNatureCatService.retrieveProblemNatureCatByProblemNatureCatId(problemNatureCatIn)}");
	            assert getValue("#{problemNatureCat!=null}").equals(true);
	            
	            problemNatureCatIn = (ProblemNatureCat)getValue("#{problemNatureCat}");
	            
	            //update ProblemNatureCat
	            problemNatureCatIn.setDisplayOrder(98);
	            Contexts.getSessionContext().set("problemNatureCatIn", problemNatureCatIn);
	            invokeMethod("#{problemNatureCatService.updateProblemNatureCat(problemNatureCatIn)}");
	            
	            //add ProblemNatureSubCat
	            invokeMethod("#{problemNatureSubCatService.addProblemNatureSubCat()}");
	            
	            //create ProblemNatureSubCat
	            ProblemNatureSubCat problemNatureSubCatIn = new ProblemNatureSubCat();
	            problemNatureSubCatIn.setSubCatDesc("David Sub Cat Test");
	            problemNatureSubCatIn.setDisplayOrder(99);
	            problemNatureSubCatIn.setRecordStatus(RecordStatus.Active);
	            problemNatureSubCatIn.setProblemNatureCat(problemNatureCatIn);
	            
	            Contexts.getSessionContext().set("problemNatureSubCatIn", problemNatureSubCatIn);
	            invokeMethod("#{problemNatureSubCatService.createProblemNatureSubCat(problemNatureSubCatIn)}");
	            		
	            //select ProblemNatureSubCat
	            invokeMethod("#{problemNatureSubCatService.retrieveProblemNatureSubCatByProblemNatureSubCatId(problemNatureSubCatIn)}");
	            assert getValue("#{problemNatureSubCat!=null}").equals(true);
	            
	            problemNatureSubCatIn = (ProblemNatureSubCat)getValue("#{problemNatureSubCat}");
	            
	            //update ProblemNatureSubCat
	            problemNatureSubCatIn.setDisplayOrder(98);
	            Contexts.getSessionContext().set("problemNatureSubCatIn", problemNatureSubCatIn);
	            invokeMethod("#{problemNatureSubCatService.updateProblemNatureSubCat(problemNatureSubCatIn)}");
	            
	            //delete ProblemNatureSubCat
	            invokeMethod("#{problemNatureSubCatService.deleteProblemNatureSubCat(problemNatureSubCatIn)}");
	            
	            //delete ProblemNatureCat
	            invokeMethod("#{problemNatureCatService.deleteProblemNatureCat(problemNatureCatIn)}");
	            
	            //delete ProblemNatureParam
	            invokeMethod("#{problemNatureParamService.deleteProblemNatureParam(problemNatureParamIn)}");
	            
	            //retrieve ProblemNatureParamList
	            invokeMethod("#{problemNatureParamListService.retrieveProblemNatureParamList()}");
	            assert getValue("#{problemNatureParamList!=null}").equals(true);
	            
                //retrieve ProblemNatureCatList
	            ProblemNatureParam problemNatureParamIn2 = new ProblemNatureParam();
	            problemNatureParamIn2.setProblemNatureParamId(Long.valueOf(1));
	            Contexts.getSessionContext().set("problemNatureParamIn2", problemNatureParamIn2);
	            invokeMethod("#{problemNatureParamService.retrieveProblemNatureParamByProblemNatureParamId(problemNatureParamIn2)}");
	            assert getValue("#{problemNatureParam!=null}").equals(true);
	            
	            problemNatureParamIn2 = (ProblemNatureParam)getValue("#{problemNatureParam}");
	            Contexts.getSessionContext().set("problemNatureParamIn2", problemNatureParamIn2);
	            invokeMethod("#{problemNatureCatListService.retrieveProblemNatureCatList(problemNatureParamIn2)}");
	            assert getValue("#{problemNatureCatList!=null}").equals(true);
	            
	            
	            //retrieve ProblemNatureSubCatList
	            invokeMethod("#{problemNatureSubCatListService.retrieveProblemNatureSubCatListFull()}");
	            assert getValue("#{problemNatureSubCatList!=null}").equals(true);
	            
	            ProblemNatureCat problemNatureCatIn2 = new ProblemNatureCat();
	            problemNatureCatIn2.setProblemNatureCatId(Long.valueOf(1));
	            Contexts.getSessionContext().set("problemNatureCatIn2", problemNatureCatIn2);
	            invokeMethod("#{problemNatureCatService.retrieveProblemNatureCatByProblemNatureCatId(problemNatureCatIn2)}");
	            assert getValue("#{problemNatureCat!=null}").equals(true);
	            
	            problemNatureCatIn2 = (ProblemNatureCat)getValue("#{problemNatureCat}");
	            Contexts.getSessionContext().set("problemNatureCatIn2", problemNatureCatIn2);
	            invokeMethod("#{problemNatureSubCatListService.retrieveProblemNatureSubCatList(problemNatureCatIn2)}");
	            assert getValue("#{problemNatureSubCatList!=null}").equals(true);
	            
	            
	            
	       }
		}.run();
	}

}
