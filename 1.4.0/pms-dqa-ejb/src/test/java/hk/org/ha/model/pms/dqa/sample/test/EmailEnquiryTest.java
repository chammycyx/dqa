package hk.org.ha.model.pms.dqa.sample.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleLetter;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class EmailEnquiryTest extends SeamTest {
	@Test
	public void emailEnquiryComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				
				DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date fromDate = dfm.parse("01/01/2010");
	            Date toDate = dfm.parse("01/01/2012");
				
				//Login
	            invokeMethod("#{identity.login}");
				
	            //Retrieve
	            SampleTestEmailEnquiryCriteria sampleTestEmailEnquiryCriteria = new SampleTestEmailEnquiryCriteria();
	            
	            sampleTestEmailEnquiryCriteria.setItemCode("PARA01");
	            sampleTestEmailEnquiryCriteria.setFromSendDate(fromDate);
	            sampleTestEmailEnquiryCriteria.setToSendDate(toDate);
	            
				
	            Contexts.getSessionContext().set("sampleTestEmailEnquiryCriteriaIn", sampleTestEmailEnquiryCriteria);
	            invokeMethod("#{sampleTestScheduleLetterListService.retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(sampleTestEmailEnquiryCriteriaIn)}");
	            
	            List<SampleTestScheduleLetter> sTSLFind = (List<SampleTestScheduleLetter>)getValue("#{sampleTestScheduleLetterList}");
	            assert sTSLFind==null;
	            
			}    
		}.run();
	}

}
