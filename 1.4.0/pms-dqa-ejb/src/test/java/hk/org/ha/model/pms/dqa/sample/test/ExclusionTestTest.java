package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleType;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class ExclusionTestTest extends SeamTest {
	@Test
	public void exclusionTestComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //retrieve ExclusionTest List
	            invokeMethod("#{exclusionTestListService.retrieveExclusionTestList()}");
	            List<ExclusionTest> exclusionTestListFind = (List<ExclusionTest>)getValue("#{exclusionTestList}");
	            assert exclusionTestListFind.size()>0;
	            
	            //check duplicate
	            ExclusionTest exclusionTestCheck = new ExclusionTest();
	            SampleTest sampleTestCheck = new SampleTest();
	            
	            sampleTestCheck.setTestCode("MICRO");
	            exclusionTestCheck.setSampleTest(sampleTestCheck);
	            
	            List<ExclusionTest> exclusionTestListCheck = new ArrayList();
	            ExclusionTest exclusionTestCheck2 = new ExclusionTest();
	            SampleTest sampleTestCheck2 = new SampleTest();
	            
	            sampleTestCheck2.setTestCode("MICRO");
	            exclusionTestCheck2.setSampleTest(sampleTestCheck2);
	            exclusionTestListCheck.add(exclusionTestCheck2);
	            
	            Contexts.getSessionContext().set("exclusionTestIn", exclusionTestCheck);
	            Contexts.getSessionContext().set("exclusionTestListIn", exclusionTestListCheck);
	            
	            invokeMethod("#{exclusionTestService.checkDuplicatedExclusionTest(exclusionTestIn, exclusionTestListIn)}");
	            
	       }
		}.run();
	}

}
