package hk.org.ha.model.pms.dqa.coa.test;

import java.util.Calendar;
import java.util.List;


import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
import hk.org.ha.model.pms.dqa.udt.FileCategory;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaItemMaintTest extends SeamTest {
	@Test
	public void testCoaItemMaintComponent() throws Exception{
		new ComponentTest() {
	
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            CoaItem coaItem = new CoaItem();
	            Contract contract = new Contract();
	            Supplier supplier = new Supplier();
	            Company company = new Company();
	            
	            //////// Contract Type coa////////
	            // check contract - TEST_C1 is new
	            invokeMethod("#{contractService.retrieveContractForCoaLastestLiveContract('TEST_C1', 'PARA01')}");
	            assert getValue("#{contract==null}").equals(true); 
	            
	            Contexts.getSessionContext().set("moduleType", ModuleType.COA);
	            invokeMethod("#{contractListService.retrieveContractListForCoaLiveContractList('TEST_C1', 'PARA01')}");
	            List<Contract> cList= (List)getValue("#{contractList}");
	            assert cList.size() == 0;
	            
	            //check drug Item exist            
	            invokeMethod("#{dmDrugService.retrieveDmDrugByItemCode('PARA01')}");
	            assert getValue("#{dmDrug!=null}").equals(true);
	            
	            invokeMethod("#{dmDrugListService.retrieveDmDrugListLikeItemCode('PARA01')}");
	            List<DmDrug> dList= (List)getValue("#{dmDrugList}");
	            assert dList.size() > 0; 
	          
	            
	            //load supplierList
	            invokeMethod("#{supplierListService.retrieveSupplierList()}");
	            List<Supplier> suppList= (List)getValue("#{supplierList}");
	            assert suppList.size() >0; 
	            
	            
	            //Add new Coa Item
	            contract.setItemCode("PARA01");
	            contract.postLoad();
	            
	            
	            invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('DKSH')}");
	            supplier = (Supplier)getValue("#{supplier}");
	            
//	            manufacturer.setManuCode("VPL");
//	            contract.setManufacturer(manufacturer);
	            company.setCompanyCode("VPL");
	            contract.setManufacturer(company);
	            contract.setSupplier(supplier);
	            
	            contract.setContractNum("TEST_C1");
	            Calendar cal = Calendar.getInstance();
	            cal.set(2010, 9, 30);
	            contract.setStartDate(cal.getTime()); //30-OCT-2010 , month start from "zero"
	            cal.set(2012,10,30);
	            contract.setEndDate(cal.getTime());    //30-NOV-2012
 
	            coaItem.setContract(contract);
	            coaItem.setOrderType(OrderType.Contract);
	            
	            Contexts.getSessionContext().set("coaItemIn", coaItem);
	            invokeMethod("#{coaItemService.createCoaItem(coaItemIn)}");
	            
	            //Retrieve Coa Item
	            invokeMethod("#{coaItemService.retrieveCoaItemByCoaItem(coaItemIn)}");
	            Contexts.getSessionContext().set("coaItemIn", coaItem);
	            
	            CoaItem coaItemFind = (CoaItem)getValue("#{coaItem}");
	            assert getValue("#{coaItem!=null}").equals(true); 

	            //Update Coa Item
	            invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('3M')}");
	            supplier = (Supplier)getValue("#{supplier}");
	            	            
	            coaItemFind.getContract().setSupplier(supplier);
	            
	            Contexts.getSessionContext().set("coaItem", coaItemFind);
	            invokeMethod("#{coaItemService.updateCoaItem()}");
	                
	            assert getValue("#{coaItem.contract.supplier.supplierCode}").equals("3M");
	            assert getValue("#{coaItem.contract.supplier.supplierCode == 'DKSH'}").equals(false);
	            
	            
	            //Add coa Batch
	            CoaBatch cb = new CoaBatch();
	            cb.setBatchNum("TEST_B001");
	            cb.setCoaItem(coaItemFind);
	            CoaBatchVer cbv = new CoaBatchVer();
	            cbv.setRecordStatus(RecordStatus.Active);
	            cbv.setCoaStatus(CoaStatus.VerificationInProgress);
	            cbv.setDiscrepancyStatus(DiscrepancyStatus.NullValue);
	            cb.setCoaBatchVer(cbv);
	            cbv.setCoaBatch(cb);
	            
	            Contexts.getSessionContext().set("coaBatchIn", cb);
	            invokeMethod("#{coaBatchService.createCoaBatch(coaBatchIn)}");
	            
	            // retrieve coaItem batch list
	            Contexts.getSessionContext().set("coaItemIn", coaItemFind);
	            invokeMethod("#{coaItemService.retrieveCoaItemByCoaItem(coaItemIn)}");
	            	            
	            coaItemFind = (CoaItem)getValue("#{coaItem}");  
	            assert coaItemFind.getCoaBatchList().size()> 0; 
	            int batchListCnt = coaItemFind.getCoaBatchList().size();
	            
	            // retrieve newly added coaBatch
	            Contexts.getSessionContext().set("batchNum", cb.getBatchNum());
	            invokeMethod("#{coaBatchManager.retrieveCoaBatchByBatchNumCoaItemId(batchNum," + coaItemFind.getCoaItemId() +")}");
	            
	            cb = (CoaBatch)getValue("#{coaBatch}");
	            
	            assert  getValue("#{coaBatch.batchNum}").equals("TEST_B001"); 
	            
	            // Add file item	            
	        	CoaFileFolder cff = new CoaFileFolder();
				FileItem fileItem = new FileItem();
				fileItem.setFileName("Tesing");
				fileItem.setFileType("pdf");
				fileItem.setFilePath("");
				fileItem.setFileDesc("");
				fileItem.setModuleType(ModuleType.COA);
				
				cff.setFileItem(fileItem);	
	            cff.setFileCat(FileCategory.Batch);
	            cff.setEffectiveDate(Calendar.getInstance().getTime());
	            
	            cff.setCoaBatch(cb);
	            Contexts.getSessionContext().set("coaFileFolderIn", cff);
	            invokeMethod("#{coaFileFolderService.createCoaFileFolder(coaFileFolderIn)}");
	            
	            invokeMethod("#{coaFileFolderListForCoaItemMaintService.retrieveCoaFileFolderListForCoaItemMaint(coaItem)}");
	            List<CoaFileFolder> cffList = (List<CoaFileFolder>)getValue("#{coaFileFolderForCoaItemMaintList}");
	            assert cffList.size()> 0; 
//	            int cffListCnt = cffList.size();
	            
	            //Delete file item
	            cff = cffList.get(0);
	            Contexts.getSessionContext().set("coaFileFolderIn", cff);
	            invokeMethod("#{coaFileFolderService.deleteCoaFileFolder(coaFileFolderIn)}");
	            invokeMethod("#{coaFileFolderListForCoaItemMaintService.retrieveCoaFileFolderListForCoaItemMaint(coaItem)}");
	            cffList = (List<CoaFileFolder>)getValue("#{coaFileFolderForCoaItemMaintList}");
//	            assert cffList.size() == cffListCnt-1; 

	            // Delete coaBatch
	            invokeMethod("#{coaBatchService.deleteCoaBatch(coaBatch)}");
	            
	            // retrieve coaItem batch list
	            Contexts.getSessionContext().set("coaItemIn", coaItemFind);
	            invokeMethod("#{coaItemService.retrieveCoaItemByCoaItem(coaItemIn)}");
	            coaItemFind = (CoaItem)getValue("#{coaItem}");  
	            assert coaItemFind.getCoaBatchList().size()== batchListCnt -1 ; 
	            
	            
	            //Delete Coa Item
	            Contexts.getSessionContext().set("coaItemIn", coaItemFind);
	            invokeMethod("#{coaItemService.deleteCoaItem(coaItemIn)}");

			}
		}.run();
	}

}
