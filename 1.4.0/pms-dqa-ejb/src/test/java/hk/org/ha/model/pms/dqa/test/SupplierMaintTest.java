package hk.org.ha.model.pms.dqa.test;

import hk.org.ha.model.pms.dqa.biz.SupplierContactServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import java.util.Date;
import java.util.List;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SupplierMaintTest extends SeamTest 
{
	@Test 
	public void testSupplierComponent() throws Exception
	{
		new ComponentTest() {
			
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception
			{
				//Login 
	            invokeMethod("#{identity.login}");
	            
	            // retrieveSupplierCodeList
	            invokeMethod("#{supplierCodeListService.retrieveSupplierCodeList()}");
	            List<String> supplierCodeList = (List<String>) getValue("#{supplierCodeList}");
				assert supplierCodeList.size() >0;
				
				// retrieveSupplier
				invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('DKSH')}");
				Supplier supplier = (Supplier) getValue("#{supplier}");
				assert  getValue("#{supplier.supplierName}").equals("DKSH HONG KONG LIMITED");
				
				assert supplier.getSupplierContactList().size() ==2;
				
				// Add Supplier Contact
				invokeMethod("#{supplierContactService.addSupplierContact()}");
				
				SupplierContact supplierContact = (SupplierContact) getValue("#{supplierContact}");
				
				supplierContact.getContact().setMobilePhone("90019002");
				supplierContact.getContact().setOfficePhone("20010002");
				supplierContact.getContact().setOptContactPoint("N");
				supplierContact.getContact().setTitle(TitleType.Mr);
				supplierContact.getContact().setEmail("john@dksh.com");
				supplierContact.getContact().setFirstName("John");
				supplierContact.getContact().setLastName("W");
				invokeMethod("#{supplierContactService.createSupplierContact()}");
				
				// checking
				invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('DKSH')}");
				supplier = (Supplier) getValue("#{supplier}");
			
				assert supplier.getSupplierContactList().size() ==3;
				
				// retrieve Supplier Contact
				invokeMethod("#{supplierContactService.retrieveSupplierContactBySupplierCodeContactId(supplier.getSupplierCode(), " +
						"supplier.getSupplierContactList().get(2).getContact().getContactId())}");				
				supplierContact = (SupplierContact) getValue("#{supplierContact}");
				
				assert supplierContact!= null;
				
				Date modifyDate =supplierContact.getContact().getModifyDate();
				
				// update Suppier Contact
				supplierContact.getContact().setLastName("Wong");
				Contexts.getSessionContext().set("supplierContact ", supplierContact);
				invokeMethod("#{supplierContactService.updateSupplierContact()}");
				
				SupplierContactServiceLocal supplierContactService = (SupplierContactServiceLocal) this.getValue("#{supplierContactService}");
				supplierContactService.retrieveSupplierContactBySupplierCodeContactId(supplier.getSupplierCode(),supplier.getSupplierContactList().get(2).getContact().getContactId());

				supplierContact = (SupplierContact) getValue("#{supplierContact}");
				
				Date modifyDate2 = supplierContact.getContact().getModifyDate();
				
				assert supplierContact.getContact().getLastName().equals("Wong");
				
				assert modifyDate2.compareTo(modifyDate) > 0;
				// Delete supplier Contact
				invokeMethod("#{supplierContactService.deleteSupplierContact(supplierContact)}");
				
				invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('DKSH')}");
				supplier = (Supplier) getValue("#{supplier}");

				assert supplier.getSupplierContactList().size() ==2;
				
				//retrieveSupplierPharmCompanyManufacturerContactList
				SampleTestSchedule sampleTestScheduleIn = new SampleTestSchedule();
				Lab labIn = new Lab();
				Contract contractIn = new Contract();
				Institution institutionIn = new Institution();
				Supplier supplierIn = new Supplier();
				Company pharmCompanyIn = new Company();
				Company manufacturerIn = new Company();
				
				labIn.setLabCode("CU");
				institutionIn.setInstitutionCode("PYN");
				supplierIn.setSupplierCode("3M");
				pharmCompanyIn.setCompanyCode("3M");
				manufacturerIn.setCompanyCode("3M");
				
				contractIn.setSupplier(supplierIn);
				contractIn.setPharmCompany(pharmCompanyIn);
				contractIn.setManufacturer(manufacturerIn);
				sampleTestScheduleIn.setLab(labIn);
				sampleTestScheduleIn.setContract(contractIn);
				sampleTestScheduleIn.setInstitution(institutionIn);
				
				Contexts.getSessionContext().set("sampleTestScheduleIn", sampleTestScheduleIn);
				
				invokeMethod("#{supplierPharmCompanyManufacturerContactListService.retrieveSupplierPharmCompanyManufacturerContactList(sampleTestScheduleIn)}");
				List<SupplierPharmCompanyManufacturerContact> spcmcs = (List<SupplierPharmCompanyManufacturerContact>) getValue("#{supplierPharmCompanyManufacturerContactList}");
				assert spcmcs.size()==0;
				
				//retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen
				invokeMethod("#{supplierPharmCompanyManufacturerContactListService.retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen(sampleTestScheduleIn)}");
				spcmcs = (List<SupplierPharmCompanyManufacturerContact>) getValue("#{supplierPharmCompanyManufacturerContactList}");
				assert spcmcs.size()>0;
				
				
				//findSupplierBySupplierCode
				Contexts.getSessionContext().set("supplierCode", "3M");
	            invokeMethod("#{supplierService.findSupplierBySupplierCode(supplierCode)}");
	            
	            //retrieveSupplierListLike
	            invokeMethod("#{supplierListService.retrieveSupplierListLike(supplierCode)}");
	            
	            //retrieveSupplierBySupplierCode
	            invokeMethod("#{supplierService.retrieveSupplierBySupplierCode('AAA')}");
	            
	            //findSupplierBySupplierCode
	            invokeMethod("#{supplierService.findSupplierBySupplierCode('AAA')}");
	            
			}			
		}.run();
	}
}
