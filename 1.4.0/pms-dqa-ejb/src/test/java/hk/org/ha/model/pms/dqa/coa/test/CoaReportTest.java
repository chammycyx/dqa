package hk.org.ha.model.pms.dqa.coa.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.biz.coa.MissingBatchCoaRptServiceLocal;
import hk.org.ha.model.pms.dqa.udt.coa.MissingBatchCoaDateType;
import hk.org.ha.model.pms.dqa.udt.coa.ReportDateType;
import hk.org.ha.model.pms.dqa.udt.coa.ReportGroupByType;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyAnalysisRptData;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyRptData;
import hk.org.ha.model.pms.dqa.vo.coa.CoaOutstandBatchCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptData;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessingDateRptData;
import hk.org.ha.model.pms.dqa.vo.coa.MissingBatchCoaRptData;
import hk.org.ha.model.pms.dqa.vo.coa.MissingCoaItemRptData;

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class CoaReportTest extends SeamTest {
	@Test
		public void testCoaProcessRptComponent() throws Exception{
		new ComponentTest() {
			@SuppressWarnings("unchecked")
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            
	            // COA Process Report
	            //================================ 
	            DateFormat dfm = new SimpleDateFormat("dd/MM/yyyy");
	            Date startDate = dfm.parse("29/07/2009");
	            Date endDate = dfm.parse("30/07/2012");
	            
	            CoaProcessRptCriteria cprc = new CoaProcessRptCriteria();	            
	            cprc.setSupplierCode("All");
	            cprc.setStartDate(startDate);
	            cprc.setEndDate(endDate);
	            
	            cprc.setGroupType(ReportGroupByType.SupplierCode);
	            Contexts.getSessionContext().set("coaProcessReportCriteria", cprc);
	            invokeMethod("#{coaProcessRptService.retrieveCoaProcessList(coaProcessReportCriteria)}");
	            
	            List<CoaProcessRptData> coaProcessRptList= (List)getValue("#{coaProcessRptService.getCoaProcessRptList()}");
	            
	            assert coaProcessRptList.size() > 0;
	            
	            cprc.setGroupType(ReportGroupByType.ItemCode);
	            Contexts.getSessionContext().set("coaProcessReportCriteria", cprc);
	            invokeMethod("#{coaProcessRptService.retrieveCoaProcessList(coaProcessReportCriteria)}");
	            
	            coaProcessRptList= (List)getValue("#{coaProcessRptService.getCoaProcessRptList()}");
	            
	            assert coaProcessRptList.size() > 0;
	            
	            cprc.setGroupType(ReportGroupByType.All);
	            Contexts.getSessionContext().set("coaProcessReportCriteria", cprc);
	            invokeMethod("#{coaProcessRptService.retrieveCoaProcessList(coaProcessReportCriteria)}");
	            
	            coaProcessRptList= (List)getValue("#{coaProcessRptService.getCoaProcessRptList()}");
	            
	            assert coaProcessRptList.size() > 0;

	            // COA Discrepancy Report
	            //================================
	            CoaDiscrepancyRptCriteria cdrc = new CoaDiscrepancyRptCriteria();
	            cdrc.setSupplierCode("All");
	            cdrc.setStartDate(startDate);
	            cdrc.setEndDate(endDate);
	            cdrc.setGroupType(ReportGroupByType.All);
	            cdrc.setReportDateType(ReportDateType.CreateDate);
	            
	            
	            Contexts.getSessionContext().set("coaDiscrepancyRptCriteria", cdrc);
	            invokeMethod("#{coaDiscrepancyRptService.retrieveCoaDiscrepancyList(coaDiscrepancyRptCriteria)}");
	            List<CoaDiscrepancyRptData> coaDiscrepancyRptList= (List)getValue("#{coaDiscrepancyRptService.getCoaDiscrepancyRptList()}");
	            
	            assert coaDiscrepancyRptList.size() > 0;
	            
	            
	            cdrc.setGroupType(ReportGroupByType.SupplierCode);
	            Contexts.getSessionContext().set("coaDiscrepancyRptCriteria", cdrc);
	            invokeMethod("#{coaDiscrepancyRptService.retrieveCoaDiscrepancyList(coaDiscrepancyRptCriteria)}");
	            coaDiscrepancyRptList= (List)getValue("#{coaDiscrepancyRptService.getCoaDiscrepancyRptList()}");
	            
	            assert coaDiscrepancyRptList.size() > 0;

	            cdrc.setGroupType(ReportGroupByType.ItemCode);
	            Contexts.getSessionContext().set("coaDiscrepancyRptCriteria", cdrc);
	            invokeMethod("#{coaDiscrepancyRptService.retrieveCoaDiscrepancyList(coaDiscrepancyRptCriteria)}");
	            coaDiscrepancyRptList= (List)getValue("#{coaDiscrepancyRptService.getCoaDiscrepancyRptList()}");
	            
	            assert coaDiscrepancyRptList.size() > 0;
	   
	            
	            // COA Discrepancy Analysis Report
	            //================================
	            CoaDiscrepancyAnalysisRptCriteria cdarc = new CoaDiscrepancyAnalysisRptCriteria();
	            cdarc.setSupplierCode("All");
	            cdarc.setStartDate(startDate);
	            cdarc.setEndDate(endDate);
	          
	            Contexts.getSessionContext().set("coaDiscrepancyAnalysisRptCriteria", cdarc);
	            invokeMethod("#{coaDiscrepancyAnalysisRptService.retrieveCoaDiscrepancyAnalysisList(coaDiscrepancyAnalysisRptCriteria)}");
	            List<CoaDiscrepancyAnalysisRptData> coaDiscrepancyAnalysisRptList= (List)getValue("#{coaDiscrepancyAnalysisRptService.getCoaDiscrepancyAnalysisRptList()}");
	            
	            assert coaDiscrepancyAnalysisRptList.size() > 0;
	            
	            // Missing COA item Report
	            //================================
	            invokeMethod("#{missingCoaItemRptService.retrieveMissingCoaItemRptList()}");
	            List<MissingCoaItemRptData> missingCoaItemRptList= (List)getValue("#{missingCoaItemRptService.getMissingCoaItemRptList()}");
	            
	            assert missingCoaItemRptList.size() >= 0;
	            
	            
	            // Missing batch COA report
	            //================================
	            MissingBatchCoaRptServiceLocal missingBatchCoaRptService = (MissingBatchCoaRptServiceLocal) getInstance("missingBatchCoaRptService");
	            missingBatchCoaRptService.retrieveMissingBatchCoaRptList(MissingBatchCoaDateType.All, new Date(), new Date());
	            List<MissingBatchCoaRptData> missingBatchCoaRptList= (List)getValue("#{missingBatchCoaRptService.getMissingBatchCoaRptList()}");
	            
	            assert missingBatchCoaRptList.size() >=0;
	            
	            // COA Processing Date report
	            //================================
	            CoaProcessingDateRptCriteria cpdrc = new CoaProcessingDateRptCriteria();
	            cpdrc.setSupplierCode("All");
	            cpdrc.setStartDate(startDate);
	            cpdrc.setEndDate(endDate);
	            
	            Contexts.getSessionContext().set("coaProcessingDateRptCriteria", cpdrc);
	            invokeMethod("#{coaProcessingDateRptService.retrieveCoaProcessingDateRptList(coaProcessingDateRptCriteria)}");
	            List<CoaProcessingDateRptData> coaProcessingDateRptList= (List)getValue("#{coaProcessingDateRptService.getCoaProcessingDateRptList()}");
	            
	            assert coaProcessingDateRptList.size() > 0;
	            
	            //////////////////////////////////////////
	            MissingBatchCoaRptData missingBatchCoaRptDataInit = new MissingBatchCoaRptData();
	            missingBatchCoaRptDataInit.setContractNum("");
	            missingBatchCoaRptDataInit.setOrderType("");
	            missingBatchCoaRptDataInit.setItemCode("");
	            missingBatchCoaRptDataInit.setSupplierCode("");
	            missingBatchCoaRptDataInit.setManufacturerCode("");
	            missingBatchCoaRptDataInit.setPharmCompanyCode("");
	            missingBatchCoaRptDataInit.setCreateDate(startDate);
	            missingBatchCoaRptDataInit.setStartDate(startDate);
	            missingBatchCoaRptDataInit.setEndDate(endDate);
	            
	            missingBatchCoaRptDataInit.getContractNum();
	            missingBatchCoaRptDataInit.getOrderType();
	            missingBatchCoaRptDataInit.getItemCode();
	            missingBatchCoaRptDataInit.getSupplierCode();
	            missingBatchCoaRptDataInit.getManufacturerCode();
	            missingBatchCoaRptDataInit.getPharmCompanyCode();
	            missingBatchCoaRptDataInit.getCreateDate();
	            missingBatchCoaRptDataInit.getStartDate();
	            missingBatchCoaRptDataInit.getEndDate();
	           
	            ////////////////////////////////////////////
	            CoaProcessingDateRptData  coaProcessingDateRptDataInit = new CoaProcessingDateRptData();
	            coaProcessingDateRptDataInit.setSupplierCode("");
	            coaProcessingDateRptDataInit.setManuCode("");
	            coaProcessingDateRptDataInit.setPharmCompanyCode("");
	            coaProcessingDateRptDataInit.setItemCode("");
	            coaProcessingDateRptDataInit.setOrderType("");
	            coaProcessingDateRptDataInit.setContractNum("");
	            coaProcessingDateRptDataInit.setBatchNum("");
	            coaProcessingDateRptDataInit.setVerDate(startDate);
	            coaProcessingDateRptDataInit.setDiscGroupDate(startDate);
	            coaProcessingDateRptDataInit.setPharmDate(startDate);
	            coaProcessingDateRptDataInit.setEmailToSupplierDate(startDate);
	            coaProcessingDateRptDataInit.setFirstReminderDate(startDate);
	            coaProcessingDateRptDataInit.setPassDate(startDate);
	            coaProcessingDateRptDataInit.setFailDate(startDate);
	            coaProcessingDateRptDataInit.setFailRemark("");
	            
	            coaProcessingDateRptDataInit.getSupplierCode();
	            coaProcessingDateRptDataInit.getManuCode();
	            coaProcessingDateRptDataInit.getPharmCompanyCode();
	            coaProcessingDateRptDataInit.getItemCode();
	            coaProcessingDateRptDataInit.getOrderType();
	            coaProcessingDateRptDataInit.getContractNum();
	            coaProcessingDateRptDataInit.getBatchNum();
	            coaProcessingDateRptDataInit.getVerDate();
	            coaProcessingDateRptDataInit.getDiscGroupDate();
	            coaProcessingDateRptDataInit.getPharmDate();
	            coaProcessingDateRptDataInit.getEmailToSupplierDate();
	            coaProcessingDateRptDataInit.getFirstReminderDate();
	            coaProcessingDateRptDataInit.getPassDate();
	            coaProcessingDateRptDataInit.getFailDate();
	            coaProcessingDateRptDataInit.getFailRemark();
	            ////////////////////////////////////////////
	            CoaProcessRptData coaProcessRptDataInit = new CoaProcessRptData();
	            coaProcessRptDataInit.setSupplierCode("");
	            coaProcessRptDataInit.setItemCode("");
	            coaProcessRptDataInit.setPass(0);
	            coaProcessRptDataInit.setFail(0);
	            coaProcessRptDataInit.setVerificationInProgress(0);
	            coaProcessRptDataInit.setDiscrepancy(0);
	            coaProcessRptDataInit.setTotal(0);
	            
	            coaProcessRptDataInit.getSupplierCode();
	            coaProcessRptDataInit.getItemCode();
	            coaProcessRptDataInit.getPass();
	            coaProcessRptDataInit.getFail();
	            coaProcessRptDataInit.getVerificationInProgress();
	            coaProcessRptDataInit.getDiscrepancy();
	            coaProcessRptDataInit.getTotal();
	            
	            ////////////////////////////////////////////
	            CoaProcessingDateRptCriteria coaProcessingDateRptCriteriaInit = new CoaProcessingDateRptCriteria();
	            coaProcessingDateRptCriteriaInit.setItemCode("");
	            coaProcessingDateRptCriteriaInit.setContractNum("");
	            
	            coaProcessingDateRptCriteriaInit.getItemCode();
	            coaProcessingDateRptCriteriaInit.getContractNum();
	            ////////////////////////////////////////////
	            CoaDiscrepancyRptData  coaDiscrepancyRptDataInit = new CoaDiscrepancyRptData();
	            coaDiscrepancyRptDataInit.setSupplierCode("");
	            coaDiscrepancyRptDataInit.setItemCode("");
	            coaDiscrepancyRptDataInit.setDiscrepancyUnderGrouping(0);
	            coaDiscrepancyRptDataInit.setPharmacistFollowUp(0);
	            coaDiscrepancyRptDataInit.setEmailToSupplier(0);
	            coaDiscrepancyRptDataInit.setReminderToSupplier1st(0);
	            coaDiscrepancyRptDataInit.setReminderToSupplier2nd(0);
	            coaDiscrepancyRptDataInit.setReminderToSupplierFuther(0);
	            
	            coaDiscrepancyRptDataInit.getSupplierCode();
	            coaDiscrepancyRptDataInit.getItemCode();
	            coaDiscrepancyRptDataInit.getDiscrepancyUnderGrouping();
	            coaDiscrepancyRptDataInit.getPharmacistFollowUp();
	            coaDiscrepancyRptDataInit.getEmailToSupplier();
	            coaDiscrepancyRptDataInit.getReminderToSupplier1st();
	            coaDiscrepancyRptDataInit.getReminderToSupplier2nd();
	            coaDiscrepancyRptDataInit.getReminderToSupplierFuther();
	            
	            ////////////////////////////////////////////
	            CoaOutstandBatchCriteria coaOutstandBatchCriteriaInit = new CoaOutstandBatchCriteria();
	            coaOutstandBatchCriteriaInit.setFromReceiveDate(startDate);
	            coaOutstandBatchCriteriaInit.setToReceiveDate(endDate);
	            
	            coaOutstandBatchCriteriaInit.getFromReceiveDate();
	            coaOutstandBatchCriteriaInit.getToReceiveDate();
	            
	            ////////////////////////////////////////////
	            MissingCoaItemRptData missingCoaItemRptDataInit = new MissingCoaItemRptData();
	            missingCoaItemRptDataInit.setContractNum("");
	            missingCoaItemRptDataInit.setOrderType("");
	            missingCoaItemRptDataInit.setItemCode("");
	            missingCoaItemRptDataInit.setSupplierCode("");
	            missingCoaItemRptDataInit.setManufacturerCode("");
	            missingCoaItemRptDataInit.setUploadDate(startDate);
	            missingCoaItemRptDataInit.setPharmCompanyCode("");
	            
	            missingCoaItemRptDataInit.getContractNum();
	            missingCoaItemRptDataInit.getOrderType();
	            missingCoaItemRptDataInit.getItemCode();
	            missingCoaItemRptDataInit.getSupplierCode();
	            missingCoaItemRptDataInit.getManufacturerCode();
	            missingCoaItemRptDataInit.getUploadDate();
	            missingCoaItemRptDataInit.getPharmCompanyCode();
	            ////////////////////////////////////////////
	            
	            CoaDiscrepancyAnalysisRptCriteria coaDiscrepancyAnalysisRptCriteriaInit = new CoaDiscrepancyAnalysisRptCriteria();
	            coaDiscrepancyAnalysisRptCriteriaInit.setItemCode("");
	            
	            coaDiscrepancyAnalysisRptCriteriaInit.getItemCode();
	            ////////////////////////////////////////////
	            CoaProcessRptCriteria coaProcessRptCriteriaInit = new CoaProcessRptCriteria();
	            coaProcessRptCriteriaInit.setItemCode("");
	            
	            coaProcessRptCriteriaInit.getItemCode();
	            ////////////////////////////////////////////
	            CoaDiscrepancyRptCriteria  coaDiscrepancyRptCriteriaInit  = new CoaDiscrepancyRptCriteria ();
	            coaDiscrepancyRptCriteriaInit.setItemCode("");
	            
	            coaDiscrepancyRptCriteriaInit.getItemCode();
			}
		}.run();
	}
}
