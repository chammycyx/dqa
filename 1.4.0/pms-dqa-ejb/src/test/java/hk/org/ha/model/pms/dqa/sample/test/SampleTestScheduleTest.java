package hk.org.ha.model.pms.dqa.sample.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Collection;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dms.persistence.DmProcureInfo;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.RegStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ReleaseIndicator;
import hk.org.ha.model.pms.dqa.udt.sample.ReportTemplate;
import hk.org.ha.model.pms.dqa.udt.sample.Result;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleType;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;

import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.dqa.vo.sample.DdrRpt;
import hk.org.ha.model.pms.dqa.vo.sample.DrugItemCodeModifyDate;
import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleEnquiryDetailCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFrequency;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFull;
import hk.org.ha.model.pms.dqa.vo.sample.SampleMovementCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventory;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestInventoryStockOnhand;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestResultCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleCriteria;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestScheduleTestResult;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import org.jboss.seam.annotations.Out;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.mock.SeamTest;
import org.testng.annotations.Test;

public class SampleTestScheduleTest extends SeamTest {
	@Test
	public void sampleTestScheduleComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				
				//Login
	            invokeMethod("#{identity.login}");
	            
	            //retrieve
	            invokeMethod("#{sampleTestScheduleFullDetailService.retrieveSampleTestScheduleFullDetailByScheduleId(22)}");
	            SampleTestSchedule sampleTestScheduleFind = (SampleTestSchedule)getValue("#{sampleTestScheduleFullDetail}");
	            assert sampleTestScheduleFind!=null;
	            
	            invokeMethod("#{sampleTestScheduleService.retrieveSampleTestScheduleForDocument(22)}");
	            sampleTestScheduleFind = (SampleTestSchedule)getValue("#{sampleTestSchedule}");
	            assert sampleTestScheduleFind!=null;
	            
	            //add
	            SampleTestSchedule sampleTestScheduleAdd = new SampleTestSchedule();
	            Contexts.getSessionContext().set("sampleTestSchedule", sampleTestScheduleAdd);
	            invokeMethod("#{sampleTestScheduleService.addSampleTestSchedule()}");
	            
	            //createSampleTestSchedule
	            SampleTestSchedule sampleTestScheduleConfirm = new SampleTestSchedule();
	            
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            invokeMethod("#{sampleItemService.retrieveSampleItemByItemCodeRecordStatus('PARA01', recordStatusIn)}");
	            SampleItem siFind = (SampleItem)getValue("#{sampleItem}");
	       
	            sampleTestScheduleConfirm.setSampleItem(siFind);
	            
	            Contexts.getSessionContext().set("orderTypeIn", OrderType.Contract);
	            invokeMethod("#{contractService.retrieveContractForSampleTest('HOC100-36-1','',orderTypeIn,'PARA01')}");
	            Contract contractFind = (Contract)getValue("#{contract}");
	            
	            invokeMethod("#{sampleTestListService.retrieveSampleTestList()}");
	            List<SampleTest> sampleTestListFind = (List<SampleTest>)getValue("#{sampleTestList}");
	            assert sampleTestListFind.size()>0;
	            
	            sampleTestScheduleConfirm.setContract(contractFind);
	            sampleTestScheduleConfirm.setOrderType(OrderType.Contract);
	            sampleTestScheduleConfirm.setScheduleStatus(ScheduleStatus.ScheduleGen);
	            sampleTestScheduleConfirm.setRecordStatus(RecordStatus.Active);
	            sampleTestScheduleConfirm.setScheduleType(ScheduleType.Manual);
	            sampleTestScheduleConfirm.setSampleTest(sampleTestListFind.get(0));
	            
	            Contexts.getSessionContext().set("sampleTestSchedule", sampleTestScheduleConfirm);
	            invokeMethod("#{sampleTestScheduleService.createSampleTestSchedule(false)}");
	            
	            //createNextSchedule
	            SampleTestSchedule sampleTestScheduleConfirm2 = new SampleTestSchedule();
	       
	            sampleTestScheduleConfirm2.setSampleItem(siFind);
	            sampleTestScheduleConfirm2.setContract(contractFind);
	            sampleTestScheduleConfirm2.setOrderType(OrderType.Contract);
	            sampleTestScheduleConfirm2.setScheduleStatus(ScheduleStatus.ScheduleGen);
	            sampleTestScheduleConfirm2.setRecordStatus(RecordStatus.Active);
	            sampleTestScheduleConfirm2.setScheduleType(ScheduleType.Manual);
	            sampleTestScheduleConfirm2.setSampleTest(sampleTestListFind.get(0));
	            
	            Contexts.getSessionContext().set("sampleTestSchedule", sampleTestScheduleConfirm2);
	            invokeMethod("#{sampleTestScheduleService.createNextSchedule(sampleTestSchedule)}");

	            //retrieveSampleTestScheduleListForCopy
	            Contexts.getSessionContext().set("scheduleId", Long.valueOf(1));
	            Contexts.getSessionContext().set("itemCode", "PARA01");
	            invokeMethod("#{sampleTestScheduleCopyListService.retrieveSampleTestScheduleListForCopy(scheduleId, itemCode)}");
	            
	            //retrieveSampleTestScheduleListForOutStanding
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListForOutStanding('AAAA01', 'CHEM')}");
	            
	            //retrieveSampleTestScheduleListForUpdateLastDocDate
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListForUpdateLastDocDate('AAAA01', 'SSS', 'MMM', 'PPP', 1)}");
	            
	            //retrieveSampleTestScheduleIdListForTestFreqMaint
	            Contexts.getSessionContext().set("recordStatusIn", RecordStatus.Active);
	            Contexts.getSessionContext().set("scheduleStatusIn", ScheduleStatus.Complete);
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleIdListForTestFreqMaint(recordStatusIn, scheduleStatusIn, 1)}");
	            
	            
	            ///////////////////////////////////////////////////////////////////////////////////////////
	            //init SampleTestScheduelTestResult
	            SampleTestScheduleTestResult sampleTestScheduleTestResultInit = new SampleTestScheduleTestResult();
	            sampleTestScheduleTestResultInit.setScheduleStatus("D");
	            sampleTestScheduleTestResultInit.setScheduleMonth(currentDate);
	            sampleTestScheduleTestResultInit.setScheduleNum("AAA");
	            sampleTestScheduleTestResultInit.setTestReportDate(currentDate);
	            sampleTestScheduleTestResultInit.setItemCode("AAA");
	            sampleTestScheduleTestResultInit.setItemDesc("AAA");
	            sampleTestScheduleTestResultInit.setTestCode("AAA");
	            sampleTestScheduleTestResultInit.setOrderType("C");
	            sampleTestScheduleTestResultInit.setRiskLevelCode("L");
	            sampleTestScheduleTestResultInit.setLabCode("L");
	            sampleTestScheduleTestResultInit.setTestPrice(1.1);
	            sampleTestScheduleTestResultInit.setPrIssueDate(currentDate);
	            sampleTestScheduleTestResultInit.setPoNum("A");
	            sampleTestScheduleTestResultInit.setSupplierCode("AAA");
	            sampleTestScheduleTestResultInit.setManufCode("AAA");
	            sampleTestScheduleTestResultInit.setPharmCompanyCode("AAA");
	            sampleTestScheduleTestResultInit.setTestResult("A");
	            sampleTestScheduleTestResultInit.setScheduleConfirmDate(currentDate);
	            sampleTestScheduleTestResultInit.setScheduleStartDate(currentDate);
	            sampleTestScheduleTestResultInit.setRecordStatus("A");
	            sampleTestScheduleTestResultInit.setCancelReason("A");
	            sampleTestScheduleTestResultInit.setBaseUnit("TAB");
	            sampleTestScheduleTestResultInit.setContractNum("A");
	            sampleTestScheduleTestResultInit.setPackSize(1);
	            sampleTestScheduleTestResultInit.setPackPrice(1.1);
	            sampleTestScheduleTestResultInit.setCurrencyCode("HKD");
	            sampleTestScheduleTestResultInit.setBatchNum("B1");
	            sampleTestScheduleTestResultInit.setExpiryDate("012013");
	            sampleTestScheduleTestResultInit.setReqQty(1);
	            sampleTestScheduleTestResultInit.setLabCollectNum("A");
	            sampleTestScheduleTestResultInit.setLabCollectDate(currentDate);
	            sampleTestScheduleTestResultInit.setTestResultDesc("A");
	            sampleTestScheduleTestResultInit.setRefDrugCost(1.1);
	            sampleTestScheduleTestResultInit.setInstitutionCode("PYN");
	            sampleTestScheduleTestResultInit.setRawMaterial("A");
	            sampleTestScheduleTestResultInit.setValidationRpt("A");
	            sampleTestScheduleTestResultInit.setValidationRptValidityFlag("Y");
	            sampleTestScheduleTestResultInit.setMicroTestParam("A");
	            sampleTestScheduleTestResultInit.setMicroNationalPharmStd("A");
	            sampleTestScheduleTestResultInit.setAcceptCriteria("A");
	            sampleTestScheduleTestResultInit.setAcceptCriteriaDesc("A");
	            sampleTestScheduleTestResultInit.setSpecification("A");
	            sampleTestScheduleTestResultInit.setMoa("A");
	            sampleTestScheduleTestResultInit.setFps("A");
	            sampleTestScheduleTestResultInit.setRefStd("A");
	            sampleTestScheduleTestResultInit.setRefStdReqFlag("A");
	            sampleTestScheduleTestResultInit.setRetentionQty(1);
	            sampleTestScheduleTestResultInit.setChemNationalPharmStd("A");
	            sampleTestScheduleTestResultInit.setChemTest("A");
	            sampleTestScheduleTestResultInit.setChemTestDesc("A");
	            sampleTestScheduleTestResultInit.setChequeAmount(1.1);
	            sampleTestScheduleTestResultInit.setChequeNum("A");
	            sampleTestScheduleTestResultInit.setChequeReceiveDate(currentDate);
	            sampleTestScheduleTestResultInit.setChequeSendDate(currentDate);
	            sampleTestScheduleTestResultInit.setInvRefNum("A");
	            sampleTestScheduleTestResultInit.setInvReceiveDate(currentDate);
	            sampleTestScheduleTestResultInit.setInvSendDate(currentDate);
	            sampleTestScheduleTestResultInit.setPaymentRemark("A");
	            
	            
	            sampleTestScheduleTestResultInit.getScheduleStatus();
	            sampleTestScheduleTestResultInit.getScheduleMonth();
	            sampleTestScheduleTestResultInit.getScheduleNum();
	            sampleTestScheduleTestResultInit.getTestReportDate();
	            sampleTestScheduleTestResultInit.getItemCode();
	            sampleTestScheduleTestResultInit.getItemDesc();
	            sampleTestScheduleTestResultInit.getTestCode();
	            sampleTestScheduleTestResultInit.getOrderType();
	            sampleTestScheduleTestResultInit.getRiskLevelCode();
	            sampleTestScheduleTestResultInit.getLabCode();
	            sampleTestScheduleTestResultInit.getTestPrice();
	            sampleTestScheduleTestResultInit.getPrIssueDate();
	            sampleTestScheduleTestResultInit.getPoNum();
	            sampleTestScheduleTestResultInit.getSupplierCode();
	            sampleTestScheduleTestResultInit.getManufCode();
	            sampleTestScheduleTestResultInit.getPharmCompanyCode();
	            sampleTestScheduleTestResultInit.getTestResult();
	            sampleTestScheduleTestResultInit.getScheduleConfirmDate();
	            sampleTestScheduleTestResultInit.getScheduleStartDate();
	            sampleTestScheduleTestResultInit.getRecordStatus();
	            sampleTestScheduleTestResultInit.getCancelReason();
	            sampleTestScheduleTestResultInit.getBaseUnit();
	            sampleTestScheduleTestResultInit.getContractNum();
	            sampleTestScheduleTestResultInit.getPackSize();
	            sampleTestScheduleTestResultInit.getPackPrice();
	            sampleTestScheduleTestResultInit.getCurrencyCode();
	            sampleTestScheduleTestResultInit.getBatchNum();
	            sampleTestScheduleTestResultInit.getExpiryDate();
	            sampleTestScheduleTestResultInit.getReqQty();
	            sampleTestScheduleTestResultInit.getLabCollectNum();
	            sampleTestScheduleTestResultInit.getLabCollectDate();
	            sampleTestScheduleTestResultInit.getTestResultDesc();
	            sampleTestScheduleTestResultInit.getRefDrugCost();
	            sampleTestScheduleTestResultInit.getInstitutionCode();
	            sampleTestScheduleTestResultInit.getRawMaterial();
	            sampleTestScheduleTestResultInit.getValidationRpt();
	            sampleTestScheduleTestResultInit.getValidationRptValidityFlag();
	            sampleTestScheduleTestResultInit.getMicroTestParam();
	            sampleTestScheduleTestResultInit.getMicroNationalPharmStd();
	            sampleTestScheduleTestResultInit.getAcceptCriteria();
	            sampleTestScheduleTestResultInit.getAcceptCriteriaDesc();
	            sampleTestScheduleTestResultInit.getSpecification();
	            sampleTestScheduleTestResultInit.getMoa();
	            sampleTestScheduleTestResultInit.getFps();
	            sampleTestScheduleTestResultInit.getRefStd();
	            sampleTestScheduleTestResultInit.getRefStdReqFlag();
	            sampleTestScheduleTestResultInit.getRetentionQty();
	            sampleTestScheduleTestResultInit.getChemNationalPharmStd();
	            sampleTestScheduleTestResultInit.getChemTest();
	            sampleTestScheduleTestResultInit.getChemTestDesc();
	            sampleTestScheduleTestResultInit.getChequeAmount();
	            sampleTestScheduleTestResultInit.getChequeNum();
	            sampleTestScheduleTestResultInit.getChequeReceiveDate();
	            sampleTestScheduleTestResultInit.getChequeSendDate();
	            sampleTestScheduleTestResultInit.getInvRefNum();
	            sampleTestScheduleTestResultInit.getInvReceiveDate();
	            sampleTestScheduleTestResultInit.getInvSendDate();
	            sampleTestScheduleTestResultInit.getPaymentRemark();
	            
	            ///////////////////////////////////////////////////////////////////////////////////////////
	            DrugItemCodeModifyDate drugItemCodeModifyDateInit = new DrugItemCodeModifyDate();
	            drugItemCodeModifyDateInit.setItemCode("PARA01");
	            drugItemCodeModifyDateInit.setModifyDate(currentDate);
	            
	            drugItemCodeModifyDateInit.getItemCode();
	            drugItemCodeModifyDateInit.getModifyDate();
	            	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleEnquiryDetailCriteria sampleEnquiryDetailCriteriaInit = new SampleEnquiryDetailCriteria();
	            sampleEnquiryDetailCriteriaInit.setScheduleNum("A");
	            sampleEnquiryDetailCriteriaInit.setOrderType(OrderType.Contract);
	            sampleEnquiryDetailCriteriaInit.setSupplierCode("3M");
	            sampleEnquiryDetailCriteriaInit.setManufCode("3M");
	            sampleEnquiryDetailCriteriaInit.setPharmCompanyCode("3M");
	            sampleEnquiryDetailCriteriaInit.setTestCode("CHEM");
	            sampleEnquiryDetailCriteriaInit.setRiskLevelCode("L");
	            sampleEnquiryDetailCriteriaInit.setLabCode("A");
	            sampleEnquiryDetailCriteriaInit.setFromPrIssueDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setToPrIssueDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setInstitutionCode("PYN");
	            sampleEnquiryDetailCriteriaInit.setContractNum("A");
	            sampleEnquiryDetailCriteriaInit.setContractSuffix("A");
	            sampleEnquiryDetailCriteriaInit.setContractStartDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setContractEndDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setBatchNum("A");
	            sampleEnquiryDetailCriteriaInit.setExpiryDate("A");
	            sampleEnquiryDetailCriteriaInit.setFromReceiveDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setToReceiveDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setFromLabCollectDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setToLabCollectDate(currentDate);
	            sampleEnquiryDetailCriteriaInit.setCollectNum("A");
	            sampleEnquiryDetailCriteriaInit.setPoNum("A");
	            sampleEnquiryDetailCriteriaInit.setTestResult("A");
	            
	            sampleEnquiryDetailCriteriaInit.getScheduleNum();
	            sampleEnquiryDetailCriteriaInit.getOrderType();
	            sampleEnquiryDetailCriteriaInit.getSupplierCode();
	            sampleEnquiryDetailCriteriaInit.getManufCode();
	            sampleEnquiryDetailCriteriaInit.getPharmCompanyCode();
	            sampleEnquiryDetailCriteriaInit.getTestCode();
	            sampleEnquiryDetailCriteriaInit.getRiskLevelCode();
	            sampleEnquiryDetailCriteriaInit.getLabCode();
	            sampleEnquiryDetailCriteriaInit.getFromPrIssueDate();
	            sampleEnquiryDetailCriteriaInit.getToPrIssueDate();
	            sampleEnquiryDetailCriteriaInit.getInstitutionCode();
	            sampleEnquiryDetailCriteriaInit.getContractNum();
	            sampleEnquiryDetailCriteriaInit.getContractSuffix();
	            sampleEnquiryDetailCriteriaInit.getContractStartDate();
	            sampleEnquiryDetailCriteriaInit.getContractEndDate();
	            sampleEnquiryDetailCriteriaInit.getBatchNum();
	            sampleEnquiryDetailCriteriaInit.getExpiryDate();
	            sampleEnquiryDetailCriteriaInit.getFromReceiveDate();
	            sampleEnquiryDetailCriteriaInit.getToReceiveDate();
	            sampleEnquiryDetailCriteriaInit.getFromLabCollectDate();
	            sampleEnquiryDetailCriteriaInit.getToLabCollectDate();
	            sampleEnquiryDetailCriteriaInit.getCollectNum();
	            sampleEnquiryDetailCriteriaInit.getPoNum();
	            sampleEnquiryDetailCriteriaInit.getTestResult();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleTestScheduleCriteria sampleTestScheduleCriteriaInit = new SampleTestScheduleCriteria();
	            sampleTestScheduleCriteriaInit.setScheduleMonth(currentDate);
	            sampleTestScheduleCriteriaInit.setItemCode("PARA01");
	            sampleTestScheduleCriteriaInit.setOrderType(OrderType.Contract);
	            sampleTestScheduleCriteriaInit.setSupplierCode("3M");
	            sampleTestScheduleCriteriaInit.setManufCode("3M");
	            sampleTestScheduleCriteriaInit.setPharmCode("3M");
	            
	            sampleTestScheduleCriteriaInit.getScheduleMonth();
	            sampleTestScheduleCriteriaInit.getItemCode();
	            sampleTestScheduleCriteriaInit.getOrderType();
	            sampleTestScheduleCriteriaInit.getSupplierCode();
	            sampleTestScheduleCriteriaInit.getManufCode();
	            sampleTestScheduleCriteriaInit.getPharmCode();
	            /////////////////////////////////////////////////////////////////////////////////////////
	            InstAssignmentCriteria instAssignmentCriteriaInit = new InstAssignmentCriteria();
	            instAssignmentCriteriaInit.setScheduleMonth(currentDate);
	            instAssignmentCriteriaInit.setItemCode("PARA01");
	            instAssignmentCriteriaInit.setRiskLevelCode("L");
	            
	            instAssignmentCriteriaInit.getScheduleMonth();
	            instAssignmentCriteriaInit.getItemCode();
	            instAssignmentCriteriaInit.getRiskLevelCode();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleItemCriteria sampleItemCriteria = new SampleItemCriteria();
	            sampleItemCriteria.setItemCode("PARA01");
	            sampleItemCriteria.setOrderType(OrderType.Contract);
	            sampleItemCriteria.setTestCode("CHEM");
	            sampleItemCriteria.setExclusionCode("BIO");
	            
	            sampleItemCriteria.getItemCode();
	            sampleItemCriteria.getOrderType();
	            sampleItemCriteria.getTestCode();
	            sampleItemCriteria.getExclusionCode();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleMovementCriteria sampleMovementCriteriaInit = new SampleMovementCriteria();
	            sampleMovementCriteriaInit.setInstitutionCode("PYN");
	            sampleMovementCriteriaInit.setOrderType(OrderType.Contract);
	            sampleMovementCriteriaInit.setSupplierCode("3M");
	            sampleMovementCriteriaInit.setPharmCompanyCode("3M");
	            sampleMovementCriteriaInit.setLabCode("A");
	            sampleMovementCriteriaInit.setTestCode("CHEM");
	            sampleMovementCriteriaInit.setBatchNum("B");
	            sampleMovementCriteriaInit.setExpiryDate("A");
	            sampleMovementCriteriaInit.setScheduleNum("A");
	            
	            sampleMovementCriteriaInit.getInstitutionCode();
	            sampleMovementCriteriaInit.getOrderType();
	            sampleMovementCriteriaInit.getSupplierCode();
	            sampleMovementCriteriaInit.getPharmCompanyCode();
	            sampleMovementCriteriaInit.getLabCode();
	            sampleMovementCriteriaInit.getTestCode();
	            sampleMovementCriteriaInit.getBatchNum();
	            sampleMovementCriteriaInit.getExpiryDate();
	            sampleMovementCriteriaInit.getScheduleNum();
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleItemFrequency sampleItemFrequencyInit = new SampleItemFrequency();
	            sampleItemFrequencyInit.setHeaderInfo("A");
	            sampleItemFrequencyInit.setUpdateDate(new Date());
	            sampleItemFrequencyInit.setItemCode("PARA01");
	            sampleItemFrequencyInit.setItemDesc("PARA01");
	            sampleItemFrequencyInit.setChemAnalysisFreq("A");
	            sampleItemFrequencyInit.setChemAnaylsisQtyReq("A");
	            sampleItemFrequencyInit.setMicroBioTestFreq("A");
	            sampleItemFrequencyInit.setMicroBioTestQtyReq("A");
	            sampleItemFrequencyInit.setValidTestQtyReq("A");
	            
	            sampleItemFrequencyInit.getHeaderInfo();
	            sampleItemFrequencyInit.getUpdateDate();
	            sampleItemFrequencyInit.getItemCode();
	            sampleItemFrequencyInit.getItemDesc();
	            sampleItemFrequencyInit.getChemAnalysisFreq();
	            sampleItemFrequencyInit.getChemAnaylsisQtyReq();
	            sampleItemFrequencyInit.getMicroBioTestFreq();
	            sampleItemFrequencyInit.getMicroBioTestQtyReq();
	            sampleItemFrequencyInit.getValidTestQtyReq();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleTestResultCriteria SampleTestResultCriteriaInit = new SampleTestResultCriteria();
	            SampleTestResultCriteriaInit.setTestCode("CHEM");
	            SampleTestResultCriteriaInit.setSupplierCode("3M");
	            SampleTestResultCriteriaInit.setManufCode("3M");
	            SampleTestResultCriteriaInit.setPharmCompanyCode("3M");
	            SampleTestResultCriteriaInit.setTestResult(Result.PASS);
	            
	            SampleTestResultCriteriaInit.getTestCode();
	            SampleTestResultCriteriaInit.getSupplierCode();
	            SampleTestResultCriteriaInit.getManufCode();
	            SampleTestResultCriteriaInit.getPharmCompanyCode();
	            SampleTestResultCriteriaInit.getTestResult();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleItemFull sampleItemFullInit = new SampleItemFull();
	            sampleItemFullInit.setItemCode("PARA01");
	            sampleItemFullInit.setItemDesc("PARA01");
	            sampleItemFullInit.setChemAnalysisQtyReq("A");
	            sampleItemFullInit.setMicroBioTestQtyReq("A");
	            sampleItemFullInit.setValidTestQtyReq("A");
	            sampleItemFullInit.setCommodityGroup("D");
	            sampleItemFullInit.setBaseUnit("TAB");
	            sampleItemFullInit.setOrderType(OrderType.Contract);
	            sampleItemFullInit.setRegStatus("A");
	            sampleItemFullInit.setReleaseIndicator("A");
	            sampleItemFullInit.setRiskLevelCode("L");
	            sampleItemFullInit.setRiskLevelDesc("LOW");
	            sampleItemFullInit.setChemAnalysisQtyReq("A");
	            sampleItemFullInit.setChemAnalysisQtyReqDesc("A");
	            sampleItemFullInit.setMicroBioTestQtyReq("A");
	            sampleItemFullInit.setMicroBioTestQtyReqDesc("A");
	            sampleItemFullInit.setValidTestQtyReq("A");
	            sampleItemFullInit.setValidTestQtyReqDesc("A");
	            sampleItemFullInit.setChemAnalysisRemark("A");
	            sampleItemFullInit.setMicroBioTestRemark("A");
	            sampleItemFullInit.setRemark("A");
	            sampleItemFullInit.setChemAnalysisExCode("A");
	            sampleItemFullInit.setChemAnalysisExDesc("A");
	            sampleItemFullInit.setMicroBioTestExCode("A");
	            sampleItemFullInit.setMicroBioTestExDesc("A");
	            sampleItemFullInit.setSpeicalCat("A");
	            
	            sampleItemFullInit.getItemCode();
	            sampleItemFullInit.getItemDesc();
	            sampleItemFullInit.getChemAnalysisQtyReq();
	            sampleItemFullInit.getMicroBioTestQtyReq();
	            sampleItemFullInit.getValidTestQtyReq();
	            sampleItemFullInit.getCommodityGroup();
	            sampleItemFullInit.getBaseUnit();
	            sampleItemFullInit.getOrderType();
	            sampleItemFullInit.getRegStatus();
	            sampleItemFullInit.getReleaseIndicator();
	            sampleItemFullInit.getRiskLevelCode();
	            sampleItemFullInit.getRiskLevelDesc();
	            sampleItemFullInit.getChemAnalysisQtyReq();
	            sampleItemFullInit.getChemAnalysisQtyReqDesc();
	            sampleItemFullInit.getMicroBioTestQtyReq();
	            sampleItemFullInit.getMicroBioTestQtyReqDesc();
	            sampleItemFullInit.getValidTestQtyReq();
	            sampleItemFullInit.getValidTestQtyReqDesc();
	            sampleItemFullInit.getChemAnalysisRemark();
	            sampleItemFullInit.getMicroBioTestRemark();
	            sampleItemFullInit.getRemark();
	            sampleItemFullInit.getChemAnalysisExCode();
	            sampleItemFullInit.getChemAnalysisExDesc();
	            sampleItemFullInit.getMicroBioTestExCode();
	            sampleItemFullInit.getMicroBioTestExDesc();
	            sampleItemFullInit.getSpeicalCat();
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleTestInventory  sampleTestInventoryInit = new SampleTestInventory();
	            sampleTestInventoryInit.setInvDate(new Date());
	            sampleTestInventoryInit.setItemCode("");
	            sampleTestInventoryInit.setItemDesc("");
	            sampleTestInventoryInit.setTest("");
	            sampleTestInventoryInit.setBaseUnit("");
	            sampleTestInventoryInit.setMovementQty(0);
	            sampleTestInventoryInit.setCdp(0.0);
	            sampleTestInventoryInit.setLocation("");
	            sampleTestInventoryInit.setBatchNum("");
	            sampleTestInventoryInit.setExpiryDate("");
	            sampleTestInventoryInit.setOrderType("C");
	            sampleTestInventoryInit.setSupplier("");
	            sampleTestInventoryInit.setManufacturer("");
	            sampleTestInventoryInit.setPharmCompany("");
	            sampleTestInventoryInit.setScheduleNum("");
	            sampleTestInventoryInit.setAdjustFlag("Y");
	            sampleTestInventoryInit.setAdjustReason("");
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleTestInventoryStockOnhand SampleTestInventoryStockOnhandInit = new SampleTestInventoryStockOnhand();
	            SampleTestInventoryStockOnhandInit.setItemCode("");
	            SampleTestInventoryStockOnhandInit.setItemDesc("");
	            SampleTestInventoryStockOnhandInit.setBaseUnit("");
	            SampleTestInventoryStockOnhandInit.setOnHandQty(0);
	            SampleTestInventoryStockOnhandInit.setSpecialCat("");
	            
	            SampleTestInventoryStockOnhandInit.getItemCode();
	            SampleTestInventoryStockOnhandInit.getItemDesc();
	            SampleTestInventoryStockOnhandInit.getBaseUnit();
	            SampleTestInventoryStockOnhandInit.getOnHandQty();
	            SampleTestInventoryStockOnhandInit.getSpecialCat();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactInit = new SupplierPharmCompanyManufacturerContact();
	            supplierPharmCompanyManufacturerContactInit.setEnable(true);
	            supplierPharmCompanyManufacturerContactInit.setName("");
	            
	            supplierPharmCompanyManufacturerContactInit.getEnable();
	            supplierPharmCompanyManufacturerContactInit.getName();
	            
	            /////////////////////////////////////////////////////////////////////////////////////////
	            DdrRpt ddrRptInit = new DdrRpt();
	            ddrRptInit.setModifyDate("");
	            ddrRptInit.setNatureOfTrx("");
	            ddrRptInit.setSupplierName("");
	            ddrRptInit.setInvoiceNo("");
	            ddrRptInit.setMovementQty(0);
	            ddrRptInit.setOnHandQty(0);
	            
	            ddrRptInit.getModifyDate();
	            ddrRptInit.getNatureOfTrx();
	            ddrRptInit.getSupplierName();
	            ddrRptInit.getInvoiceNo();
	            ddrRptInit.getMovementQty();
	            ddrRptInit.getOnHandQty();
	            /////////////////////////////////////////////////////////////////////////////////////////
	            SampleTestEmailEnquiryCriteria sampleTestEmailEnquiryCriteriaInit = new SampleTestEmailEnquiryCriteria();
	            sampleTestEmailEnquiryCriteriaInit.setLetterCode("");
	            sampleTestEmailEnquiryCriteriaInit.getLetterCode();
	            /////////////////////////////////////////////////////////////////////////////////////////
	       }
		}.run();
	}
	
	@Test
	public void testSampleTestScheduleListReportComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				//Login 
	            invokeMethod("#{identity.login}");
				
	            Collection<Long> keyList = new ArrayList<Long>();
	            
	            keyList.add(1L);
	            keyList.add(2L);
	            
	            Contexts.getSessionContext().set("sampleTestScheduleKeyList", keyList);
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListReport(sampleTestScheduleKeyList)}");
			}
		}.run();
	}
	
	@Test
	public void testSampleTestScheduleListReportByCriteriaTestResultComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				//Login 
	            invokeMethod("#{identity.login}");
				
	            SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria = new SampleEnquiryDetailCriteria();
	            sampleEnquiryDetailCriteria.setReportTemplate(ReportTemplate.TR);
	            sampleEnquiryDetailCriteria.setItemCode("PARA01");
	            sampleEnquiryDetailCriteria.setFromTestReportDate(currentDate);
	            sampleEnquiryDetailCriteria.setToTestReportDate(currentDate);
	            
	            Contexts.getSessionContext().set("sampleEnquiryDetailCriteria", sampleEnquiryDetailCriteria);
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteriaReport(sampleEnquiryDetailCriteria)}");
			}
		}.run();
	}
	
	@Test
	public void testSampleTestScheduleListReportByCriteriaAllResultComponent() throws Exception{
		new ComponentTest() {
			protected void testComponents() throws Exception {
				Date currentDate = new Date();
				//Login 
	            invokeMethod("#{identity.login}");
				
	            SampleEnquiryDetailCriteria sampleEnquiryDetailCriteria = new SampleEnquiryDetailCriteria();
	            sampleEnquiryDetailCriteria.setReportTemplate(ReportTemplate.ALL);
	            sampleEnquiryDetailCriteria.setItemCode("PARA01");
	            sampleEnquiryDetailCriteria.setFromTestReportDate(currentDate);
	            sampleEnquiryDetailCriteria.setToTestReportDate(currentDate);
	            
	            Contexts.getSessionContext().set("sampleEnquiryDetailCriteria", sampleEnquiryDetailCriteria);
	            invokeMethod("#{sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteriaReport(sampleEnquiryDetailCriteria)}");
			}
		}.run();
	}


}
