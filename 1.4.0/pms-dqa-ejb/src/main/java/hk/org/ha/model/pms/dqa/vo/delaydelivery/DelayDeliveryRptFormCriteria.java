package hk.org.ha.model.pms.dqa.vo.delaydelivery;

import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.delaydelivery.DelayDeliveryRptFormStatus;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DelayDeliveryRptFormCriteria {
	private ArrayList<String> institutionList;
	private String itemCode;
	private OrderTypeAll orderType;
	private ArrayList<DelayDeliveryRptFormStatus> delayDeliveryRptFormStatusList;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromReportDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date toReportDate;
	private String contractNum;
	private String contractSuffix;
	
	private boolean allInstitutionFlag;
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	
	public Date getFromReportDate() {
		return fromReportDate;
	}
	public void setFromReportDate(Date fromReportDate) {
		this.fromReportDate = fromReportDate;
	}
	public Date getToReportDate() {
		return toReportDate;
	}
	public void setToReportDate(Date toReportDate) {
		this.toReportDate = toReportDate;
	}
	public OrderTypeAll getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderTypeAll orderType) {
		this.orderType = orderType;
	}
	
	public ArrayList<String> getInstitutionList() {
		return institutionList;
	}
	public void setInstitutionList(ArrayList<String> institutionList) {
		this.institutionList = institutionList;
	}
	public ArrayList<DelayDeliveryRptFormStatus> getDelayDeliveryRptFormStatusList() {
		return delayDeliveryRptFormStatusList;
	}
	public void setDelayDeliveryRptFormStatusList(
			ArrayList<DelayDeliveryRptFormStatus> delayDeliveryRptFormStatusList) {
		this.delayDeliveryRptFormStatusList = delayDeliveryRptFormStatusList;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getContractSuffix() {
		return contractSuffix;
	}
	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}
	public boolean isAllInstitutionFlag() {
		return allInstitutionFlag;
	}
	public void setAllInstitutionFlag(boolean allInstitutionFlag) {
		this.allInstitutionFlag = allInstitutionFlag;
	}
	
	
	
	
	
	
	
}
