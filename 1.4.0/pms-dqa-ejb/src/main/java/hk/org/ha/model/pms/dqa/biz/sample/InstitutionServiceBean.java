package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("institutionService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class InstitutionServiceBean implements InstitutionServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private Institution institution;
	
	private boolean success;

	private String errorCode;
	
	public void retrieveInstitutionByInstitution(Institution institutionIn){
		logger.debug("retrieveInstitutionByInstitution");
		institution = retrieveInstitutionByInstitutionForValidation(institutionIn.getInstitutionCode());
	}
	
	private Institution retrieveInstitutionByInstitutionForValidation(String  institutionCode){
		logger.debug("retrieveInstitutionByInstitutionForValidation");		
		return em.find(Institution.class, institutionCode);
	}
	
	public Institution retrieveInstitutionForPhs( String institutionCode )
	{
		logger.debug("retrieveInstitutionForPhs");
		return em.find(Institution.class, institutionCode);
	}
	
	public void addInstitution(){
		logger.debug("addInstitution");
		
		institution = new Institution();
		institution.setContact(new Contact());
		
	}


	@SuppressWarnings("unchecked")
	public void createInstitution(Institution institutionIn){
		logger.debug("createInstitution");
		success = false;
		errorCode = null;
		
		List<Institution> institutionList = null;
		Contact contact=null;
 	
		institutionList = em.createNamedQuery("Institution.findByInstitutionCode")
		.setParameter("institutionCode", institutionIn.getInstitutionCode())
		.getResultList();
		
		if (institutionList == null || institutionList.size() == 0){
			if(institutionIn.getContact().getContactId()==null)
			{
				contact = institutionIn.getContact();
				em.persist(contact);
				institutionIn.setContact(contact);
			}
			institutionIn.setRecordStatus(RecordStatus.Active.getDataValue());
			em.persist(institutionIn);
			em.flush();
			success = true;
		}else if(institutionList.size()>0 && institutionList.get(0).getRecordStatus().equals(RecordStatus.Deleted.getDataValue())){
			if(institutionIn.getContact().getContactId()==null)
			{
				contact = institutionIn.getContact();
				em.persist(contact);
				institutionIn.setContact(contact);
			}
			Institution institutionFind = institutionList.get(0);
			institutionFind.setRecordStatus(RecordStatus.Active.getDataValue());
			institutionFind.setContact(institutionIn.getContact());
			em.merge(institutionFind);
			success = true;	
		}else{
			errorCode = "0007";
		}
	}

	public void updateInstitution(Institution institutionIn){
		logger.debug("updateInstitution");
		success = false;
		errorCode = null;
		Contact contact=null;
		
		if(institutionIn.getContact().getContactId()==null)
		{
			contact = institutionIn.getContact();
			em.persist(contact);
			institutionIn.setContact(contact);
		}
		else{
			em.merge(institutionIn.getContact());
		}

		em.merge(institutionIn);		
		em.flush();
		success = true;		
	}
	
	@SuppressWarnings("unchecked")
	public void deleteInstitution(Institution institutionIn){
		logger.debug("deleteInstitution");
		success = false;
		Institution institutionFind = retrieveInstitutionByInstitutionForValidation(institutionIn.getInstitutionCode());
		
		if (institutionFind != null){
			List<Contact> contactList=null;
			if (institutionIn.getContact()!=null){
				contactList = em.createNamedQuery("Contact.findByContactId")
				.setParameter("contactId", institutionIn.getContact().getContactId())
				.getResultList();
				
				institutionFind.setContact(null);
				institutionFind.setRecordStatus(RecordStatus.Deleted.getDataValue());
				em.merge(institutionFind);
				em.flush();
			}
			else
			{
				institutionFind.setContact(null);
				institutionFind.setRecordStatus(RecordStatus.Deleted.getDataValue());
				em.merge(institutionFind);
				em.flush();
			}
			
			if (contactList!=null && contactList.size()>0)
			{
				em.remove(contactList.get(0));
				em.flush();
			}
			
			
			success = true;		
		}
	}
	
	public boolean isSuccess(){
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	@Remove
	public void destroy(){
		if (institution != null){
			institution = null;
		}
	}
}
