package hk.org.ha.model.pms.dqa.vo.suppperf;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FaxInitSuppRpt {
	private String faxCreateDate;
	private String faxTo;
	private String faxNum;
	private String refNum;
	private String caseNum;
	private String fullDrugDesc;
	private String inst;
	private String distributeListDate;
	private Boolean distributeListFlag;
	private String confirmAvailDate;
	private Boolean confirmAvailFlag;
	private String collectSampleDate;
	private Boolean collectSampleFlag;
	private Boolean referSampleFlag;
	private String instContactUser;
	private String instContactRank;
	private String instContactPhone;
	private String instContactEmail;
	private String provideExplainDate;
	private Boolean provideExplainFlag;
	private String faxFromUser;
	private String contractNo;
	private String faxFromPhone;
	private String faxFromEmail;
	private String attn;
	private String problemDetail;
	private String orderType;
	private String instName;
	private String batchNum;
	private String instRptName;
	private String instRptDate;
	
	
	
	public String getFaxCreateDate() {
		return faxCreateDate;
	}
	public void setFaxCreateDate(String faxCreateDate) {
		this.faxCreateDate = faxCreateDate;
	}
	public String getFaxTo() {
		return faxTo;
	}
	public void setFaxTo(String faxTo) {
		this.faxTo = faxTo;
	}
	public String getFaxNum() {
		return faxNum;
	}
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	public String getInst() {
		return inst;
	}
	public void setInst(String inst) {
		this.inst = inst;
	}
	public String getDistributeListDate() {
		return distributeListDate;
	}
	public void setDistributeListDate(String distributeListDate) {
		this.distributeListDate = distributeListDate;
	}
	public Boolean getDistributeListFlag() {
		return distributeListFlag;
	}
	public void setDistributeListFlag(Boolean distributeListFlag) {
		this.distributeListFlag = distributeListFlag;
	}
	public String getConfirmAvailDate() {
		return confirmAvailDate;
	}
	public void setConfirmAvailDate(String confirmAvailDate) {
		this.confirmAvailDate = confirmAvailDate;
	}
	public Boolean getConfirmAvailFlag() {
		return confirmAvailFlag;
	}
	public void setConfirmAvailFlag(Boolean confirmAvailFlag) {
		this.confirmAvailFlag = confirmAvailFlag;
	}
	public String getCollectSampleDate() {
		return collectSampleDate;
	}
	public void setCollectSampleDate(String collectSampleDate) {
		this.collectSampleDate = collectSampleDate;
	}
	public Boolean getCollectSampleFlag() {
		return collectSampleFlag;
	}
	public void setCollectSampleFlag(Boolean collectSampleFlag) {
		this.collectSampleFlag = collectSampleFlag;
	}
	public Boolean getReferSampleFlag() {
		return referSampleFlag;
	}
	public void setReferSampleFlag(Boolean referSampleFlag) {
		this.referSampleFlag = referSampleFlag;
	}
	public String getProvideExplainDate() {
		return provideExplainDate;
	}
	public void setProvideExplainDate(String provideExplainDate) {
		this.provideExplainDate = provideExplainDate;
	}
	public Boolean getProvideExplainFlag() {
		return provideExplainFlag;
	}
	public void setProvideExplainFlag(Boolean provideExplainFlag) {
		this.provideExplainFlag = provideExplainFlag;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getInstContactUser() {
		return instContactUser;
	}
	public void setInstContactUser(String instContactUser) {
		this.instContactUser = instContactUser;
	}
	public String getInstContactRank() {
		return instContactRank;
	}
	public void setInstContactRank(String instContactRank) {
		this.instContactRank = instContactRank;
	}
	public String getInstContactPhone() {
		return instContactPhone;
	}
	public void setInstContactPhone(String instContactPhone) {
		this.instContactPhone = instContactPhone;
	}
	public String getInstContactEmail() {
		return instContactEmail;
	}
	public void setInstContactEmail(String instContactEmail) {
		this.instContactEmail = instContactEmail;
	}
	public String getFaxFromUser() {
		return faxFromUser;
	}
	public void setFaxFromUser(String faxFromUser) {
		this.faxFromUser = faxFromUser;
	}
	public String getFaxFromPhone() {
		return faxFromPhone;
	}
	public void setFaxFromPhone(String faxFromPhone) {
		this.faxFromPhone = faxFromPhone;
	}
	public String getFaxFromEmail() {
		return faxFromEmail;
	}
	public void setFaxFromEmail(String faxFromEmail) {
		this.faxFromEmail = faxFromEmail;
	}
	public String getAttn() {
		return attn;
	}
	public void setAttn(String attn) {
		this.attn = attn;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getInstRptName() {
		return instRptName;
	}
	public void setInstRptName(String instRptName) {
		this.instRptName = instRptName;
	}
	public String getInstRptDate() {
		return instRptDate;
	}
	public void setInstRptDate(String instRptDate) {
		this.instRptDate = instRptDate;
	}
}