package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacherInf;
import hk.org.ha.model.pms.dqa.vo.DmDrugData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("dmDrugListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DmDrugListServiceBean implements DmDrugListServiceLocal {

	@Logger
	private Log logger;

	@In
	private DmDrugCacherInf dmDrugCacher;

	@Out(required = false)
	private List<DmDrug> dmDrugList;
	
	@Out(required = false)
	private List<DmDrugData> dmDrugDataList;
	
	
	private Comparator comparator = new DmDrugDataComparator();
    
	
	public void retrieveDmDrugListLikeItemCode(String prefixItemCode)
	{
		logger.info("retrieveDmDrugListLikeItemCode #0 ", prefixItemCode);

		dmDrugList = dmDrugCacher.getDrugListByItemCode(prefixItemCode);
		dmDrugDataList = new ArrayList<DmDrugData>();
		
		
		for (DmDrug dmDrug:dmDrugList) {
			if(dmDrug!=null)
			{
				dmDrug.getDmProcureInfo().getOrderType();
				
				
				DmDrugData d = new DmDrugData();
				d.setItemCode(dmDrug.getItemCode());
				d.setBaseUnit(dmDrug.getBaseUnit());
				d.setItemDesc(dmDrug.getFullDrugDesc());
				d.setOrderType(dmDrug.getDmProcureInfo().getOrderType());
				dmDrugDataList.add(d);	
				
				
				
				
			}
		}
		
		
		Collections.sort( dmDrugDataList, comparator);	
		
		logger.info("dmDrugList size #0", dmDrugList.size());
		logger.info("dmDrugDataList size #0", dmDrugDataList.size());
	}
	
	
	public static class DmDrugDataComparator implements Comparator<DmDrugData>,Serializable
	{

		private static final long serialVersionUID = 8032402230292096246L;

		public int compare(DmDrugData d1, DmDrugData d2){
			return d1.getItemCode()
					 .compareTo( 
							 d2.getItemCode()
				      );
		}						
	}
	
	

	@Remove
	public void destroy() 
	{
		if (dmDrugList != null) {
			dmDrugList = null;
		}
	}
}
