package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_FINAL_HOSP")
/*@NamedQueries({
	@NamedQuery(name = "FaxFinalHosp.findByFaxFinalHospId", query = "select o from FaxFinalHosp o where o.faxFinalHospId = :faxFinalHospId ")
})*/
@Customizer(AuditCustomizer.class)
public class FaxFinalHosp extends VersionEntity {

	private static final long serialVersionUID = -3548972782494547551L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxFinalHospSeq")
	@SequenceGenerator(name = "faxFinalHospSeq", sequenceName = "SEQ_FAX_FINAL_HOSP", initialValue=10000)
	@Id
	@Column(name="FAX_FINAL_HOSP_ID", nullable=false)
	private Long faxFinalHospId;
	
	@Converter(name = "FaxFinalHosp.investRptFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxFinalHosp.investRptFlag")
	@Column(name="INVEST_RPT_FLAG", length=1)
	private YesNoFlag investRptFlag;
	
	@Converter(name = "FaxFinalHosp.noActionFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxFinalHosp.noActionFlag")
	@Column(name="NO_ACTION_FLAG", length=1)
	private YesNoFlag noActionFlag;
	
	@Converter(name = "FaxFinalHosp.relBatchFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxFinalHosp.relBatchFlag")
	@Column(name="REL_BATCH_FLAG", length=1)
	private YesNoFlag relBatchFlag;
	
	@Converter(name = "FaxFinalHosp.stkReplaceFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxFinalHosp.stkReplaceFlag")
	@Column(name="STK_REPLACE_FLAG", length=1)
	private YesNoFlag stkReplaceFlag;
	
	/*@Converter(name = "FaxFinalHosp.otherFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxFinalHosp.otherFlag")
	@Column(name="OTHER_FLAG", length=1)
	private YesNoFlag otherFlag;*/
	
	@Column(name="OTHER_DESC", length=100)
	private String otherDesc;

	public Long getFaxFinalHospId() {
		return faxFinalHospId;
	}

	public void setFaxFinalHospId(Long faxFinalHospId) {
		this.faxFinalHospId = faxFinalHospId;
	}

	public YesNoFlag getInvestRptFlag() {
		return investRptFlag;
	}

	public void setInvestRptFlag(YesNoFlag investRptFlag) {
		this.investRptFlag = investRptFlag;
	}

	public YesNoFlag getNoActionFlag() {
		return noActionFlag;
	}

	public void setNoActionFlag(YesNoFlag noActionFlag) {
		this.noActionFlag = noActionFlag;
	}

	public YesNoFlag getRelBatchFlag() {
		return relBatchFlag;
	}

	public void setRelBatchFlag(YesNoFlag relBatchFlag) {
		this.relBatchFlag = relBatchFlag;
	}

	public YesNoFlag getStkReplaceFlag() {
		return stkReplaceFlag;
	}

	public void setStkReplaceFlag(YesNoFlag stkReplaceFlag) {
		this.stkReplaceFlag = stkReplaceFlag;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}

	/*public YesNoFlag getOtherFlag() {
		return otherFlag;
	}

	public void setOtherFlag(YesNoFlag otherFlag) {
		this.otherFlag = otherFlag;
	}*/

		
		
		

}
