package hk.org.ha.model.pms.dqa.interfaces;

import javax.ejb.Remote;

@Remote
public interface DqaService {

	Boolean getCoaResult(String jobId);
	
	Boolean importErpContract(String jobId);
	
	Boolean importPhsSupplier(String jobId);
	
	Boolean importPhsCompany(String jobId);
	
	Boolean importPhsInstitution(String jobId);
	
	Boolean importPhsStockMonthlyExp(String jobId);
	
	Boolean updateFuncSeqNumYear(String jobId);
	
}
