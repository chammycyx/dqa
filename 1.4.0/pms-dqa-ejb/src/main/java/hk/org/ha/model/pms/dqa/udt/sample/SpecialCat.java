package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum SpecialCat implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	HC("HC", "Hazardous Chemical"),
	CT("CT", "Cytotoxics");
	
    private final String dataValue;
    private final String displayValue;
        
    SpecialCat(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<SpecialCat> {

		private static final long serialVersionUID = 3072291143463268317L;

		@Override
    	public Class<SpecialCat> getEnumClass() {
    		return SpecialCat.class;
    	}
    }
}
