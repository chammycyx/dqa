package hk.org.ha.model.pms.dqa.persistence.coa;

import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name = "COA_DISCREPANCY_KEY")
@NamedQuery(name = "CoaDiscrepancyKey.findAll", query = "select o from CoaDiscrepancyKey o where o.recordStatus = :recordStatus order by o.orderSeq")
public class CoaDiscrepancyKey extends VersionEntity {

	private static final long serialVersionUID = -6614633583174691228L;

	@Id
	@Column(name="DISCREPANCY_ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaDiscrepancyKeySeq")
	@SequenceGenerator(name="coaDiscrepancyKeySeq", sequenceName="SEQ_COA_DISCREPANCY_KEY", initialValue=10000)
	private Long discrepancyId;

	@Column(name="DISCREPANCY_DESC", length = 200, nullable = false)
	private String discrepancyDesc;

	@Column(name="ORDER_SEQ")
	private Integer orderSeq;

	@Converter(name = "CoaDiscrepancyKey.recordStatus", converterClass = RecordStatus.Converter.class)
	@Convert("CoaDiscrepancyKey.recordStatus")
	@Column(name="RECORD_STATUS", length = 1)
	private RecordStatus recordStatus;
	
	@Transient
	private boolean enable;

	@Transient
	private CoaDiscrepancy parentCoaDiscrepancy;
	
	public Long getDiscrepancyId() {
		return discrepancyId;
	}

	public void setDiscrepancyId(Long discrepancyId) {
		this.discrepancyId = discrepancyId;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public CoaDiscrepancyKey() {
	}

	public void setDiscrepancyDesc(String discrepancyDesc) {
		this.discrepancyDesc = discrepancyDesc;
	}

	public String getDiscrepancyDesc() {
		return discrepancyDesc;
	}

	public void setOrderSeq(Integer orderSeq) {
		this.orderSeq = orderSeq;
	}

	public Integer getOrderSeq() {
		return orderSeq;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setParentCoaDiscrepancy(CoaDiscrepancy parentCoaDiscrepancy) {
		this.parentCoaDiscrepancy = parentCoaDiscrepancy;
	}

	public CoaDiscrepancy getParentCoaDiscrepancy() {
		return parentCoaDiscrepancy;
	}

}

