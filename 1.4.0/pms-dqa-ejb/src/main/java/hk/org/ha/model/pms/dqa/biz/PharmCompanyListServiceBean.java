package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmCompanyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmCompanyListServiceBean implements PharmCompanyListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Company> pharmCompanyList;
    
    @SuppressWarnings("unchecked")
	public void retrievePharmCompanyList(){
    	logger.debug("retrievePharmCompanyList");
    	
    	pharmCompanyList = em.createNamedQuery("Company.findByPharmCompanyFlag")
    		.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
    		.setHint(QueryHints.LEFT_FETCH, "o.contact")
    		.getResultList();
    }
    
    @SuppressWarnings("unchecked")
	public void retrievePharmCompanyListLike(String pharmCompanyCode){
    	logger.debug("retrievePharmCompanyListLike");
    	
    	pharmCompanyList = em.createNamedQuery("Company.findLikePharmCompanyCode")
    		.setParameter("pharmCompanyCode", pharmCompanyCode+"%")
    		.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
    		.setHint(QueryHints.LEFT_FETCH, "o.contact")
    		.getResultList();
    }
    
    
    @Remove
	public void destroy(){
		if ( pharmCompanyList != null ){
			pharmCompanyList = null;
		}
	}
}
