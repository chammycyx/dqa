package hk.org.ha.model.pms.dqa.biz.coa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.util.FTPHelper;

import javax.ejb.Stateless;

import org.apache.commons.io.IOUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

import au.com.bytecode.opencsv.CSVWriter;

@Stateless
@Scope(ScopeType.SESSION)
@Name("coaResultService")
@RemoteDestination
@MeasureCalls
public class CoaResultServiceBean implements CoaResultServiceLocal {

	@Logger
	private Log logger;
	
	@In(create = true)
	private CoaBatchListServiceLocal coaBatchListService;
	
	@In(create = true)
	private CoaBatchServiceLocal coaBatchService;
	
	private static final char DELIMITER = ',';
	private static final String NEW_LINE = "\n";
	private static final char DOUBLE_QUOTE = '"';
	
	public static final String CONF_DIR = "conf.dir";
	public static final String ERP_DIR_PROPERTIES = "/erp-dir.properties";
	
	//mode to decide transfer file method
	private String fileTransferMode;
	private static final String SFTP = "sftp";
	private static final String FTP = "ftp";
	
	//sftp setup
	private ChannelSftp dqaCoaResultChannelSftp;
	private String sftpWorkDir;
	
	private String workDir;
	
	private String configDirSubfolder;
	
	public void setConfigDirSubfolder(String configDirSubfolder){
		this.configDirSubfolder = configDirSubfolder;
	}
	
	private void initDir(String connectType) {
		Properties profile = new Properties();		
		String configDir = System.getProperty(CONF_DIR);
		FileInputStream fis=null;
        try {        	
    		fis = new FileInputStream( configDir + "/" + configDirSubfolder + ERP_DIR_PROPERTIES );
			profile.load( fis );			
			
			workDir = profile.getProperty(connectType+".workDir");
		} catch (IOException e) {
			logger.error("Fail to read file.", e);
		}finally {
			if (fis!=null){
				try {
					fis.close();
				} catch (IOException e) {
					logger.error("Fail to read file", e);
				}
			}
		}
	}
	
	public Boolean getCoaResult(String jobId) {
		logger.debug("getCoaResult #0",jobId);
		
		Contract contract;
		String fileName = String.format("QA_RESULT_%1$td%1$tm%1$tY%1$tH%1$tM%1$tS.txt", new Object[]{new Date()});		
		List<CoaBatch> coaBatchList;
		
		logger.info("File transfer mode is #0", fileTransferMode);
		
		if (fileTransferMode.equals(FTP))
		{
			File resultFile;
			initDir( "coaResult" );
			
			try {			
				
				coaBatchList = coaBatchListService.retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus();
				
				resultFile = new File( workDir, fileName );
				CSVWriter writer = new CSVWriter( new FileWriter( resultFile ), DELIMITER, DOUBLE_QUOTE, DOUBLE_QUOTE, NEW_LINE );
							
				for( CoaBatch cb:coaBatchList ) {
					contract = cb.getCoaItem().getContract();
					
					writer.writeNext(
							new String[]{
									contract.getItemCode(),
									contract.getSupplier().getSupplierCode(),
									contract.getManufacturer().getCompanyCode(),
									cb.getBatchNum(),
									String.format("%1$tY/%1$tm/%1$td %1$tH:%1$tM:%1$tS", new Object[]{cb.getCoaBatchVer().getCreateDate()})
							}
					);				
					coaBatchService.updateCoaBatchForErpInterface( cb );
				}
				
				writer.close();
				
				if( resultFile.exists() ) {
					FTPHelper ftpHelper = new FTPHelper("coaResult", configDirSubfolder);
					ftpHelper.upload(resultFile.getAbsolutePath(), resultFile.getName());
					ftpHelper.quit();
//					resultFile.deleteOnExit();
					
					logger.info("Send coa result file '#0', it contains #1 record(s)", resultFile.getName(), coaBatchList.size());
				}
						
				return true;
				
			} catch (IOException e) {			
				logger.error("Fail to read file.", e);
			}
		}
		else if (fileTransferMode.equals(SFTP))
		{
			File resultFile = new File( sftpWorkDir, fileName );
			FileInputStream fileInputStream = null;
			try {					
				coaBatchList = coaBatchListService.retrieveCoaBatchListByInterfaceFlagCoaStatusRecordStatus();
				CSVWriter writer = new CSVWriter( new FileWriter( resultFile ), DELIMITER, DOUBLE_QUOTE, DOUBLE_QUOTE, NEW_LINE );
				for( CoaBatch cb:coaBatchList ) {
					contract = cb.getCoaItem().getContract();
					
					writer.writeNext(
							new String[]{
									contract.getItemCode(),
									contract.getSupplier().getSupplierCode(),
									contract.getManufacturer().getCompanyCode(),
									cb.getBatchNum(),
									String.format("%1$tY/%1$tm/%1$td %1$tH:%1$tM:%1$tS", new Object[]{cb.getCoaBatchVer().getCreateDate()})
							}
					);				
					coaBatchService.updateCoaBatchForErpInterface( cb );
				}
				
				writer.close();
				dqaCoaResultChannelSftp = (ChannelSftp) Component.getInstance("dqaCoaResultChannelSftp");
				fileInputStream = new FileInputStream(resultFile);
				dqaCoaResultChannelSftp.put(fileInputStream, fileName);
				
				logger.info("Send coa result file '#0', it contains #1 record(s)", resultFile.getName(), coaBatchList.size());
				
				return true;
				
			} catch (IOException e) {			
				logger.error("Fail to read or write or close file.", e);
			} catch (SftpException e) {
				logger.error("Fail to upload file.", e);
			}
			finally 
			{
				if (fileInputStream != null)
				{
					IOUtils.closeQuietly(fileInputStream);
				}
			}
		}
		else 
		{
			logger.error("File transfer mode not correct. Mode can only be " + SFTP + " or " + FTP);
		}
		
		return false;
	}

}
