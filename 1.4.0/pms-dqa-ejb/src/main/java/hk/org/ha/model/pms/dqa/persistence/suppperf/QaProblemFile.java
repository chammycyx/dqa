package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "QA_PROBLEM_FILE")
@Customizer(AuditCustomizer.class)
public class QaProblemFile extends VersionEntity {

	private static final long serialVersionUID = 9153136921150342004L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qaProblemFileSeq")
	@SequenceGenerator(name = "qaProblemFileSeq", sequenceName = "SEQ_QA_PROBLEM_FILE", initialValue=10000)
	@Id
	@Column(name="QA_PROBLEM_FILE_ID", nullable=false)
	private Long qaProblemFileId;
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	
	@ManyToOne
	@JoinColumn(name="FILE_ID")
	private FileItem fileItem;
	
	@Converter(name = "QaProblemFile.shareFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("QaProblemFile.shareFlag")
	@Column(name="SHARE_FLAG", length=1)
	private YesNoFlag shareFlag;
	
	@Transient
	private String actionType;

	public Long getQaProblemFileId() {
		return qaProblemFileId;
	}

	public void setQaProblemFileId(Long qaProblemFileId) {
		this.qaProblemFileId = qaProblemFileId;
	}
	
	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}

	public FileItem getFileItem() {
		return fileItem;
	}

	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}

	public void setShareFlag(YesNoFlag shareFlag) {
		this.shareFlag = shareFlag;
	}

	public YesNoFlag getShareFlag() {
		return shareFlag;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

}
