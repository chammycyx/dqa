package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;

import javax.ejb.Local;

@Local
public interface SupplierPharmCompanyManufacturerContactListServiceLocal {

	void retrieveMspContactListByContract(Contract contract);
	
	void retrieveSupplierPharmCompanyManufacturerContactList(SampleTestSchedule sampleTestScheduleIn);
	
	void retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen(SampleTestSchedule sampleTestScheduleIn);
	
    void destroy();
}
