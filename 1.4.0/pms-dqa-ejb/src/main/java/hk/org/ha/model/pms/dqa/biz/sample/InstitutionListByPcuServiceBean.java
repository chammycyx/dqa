package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("institutionListByPcuService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class InstitutionListByPcuServiceBean implements InstitutionListByPcuServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Institution> institutionListByPcu;
	
	@SuppressWarnings("unchecked")
	public void retrieveInstitutionListByPcu(String pcuCode){
		logger.debug("retrieveInstitutionListByPcu");
		
		institutionListByPcu = em.createNamedQuery("Institution.findByPcuCodeRecordStatus")
			.setParameter("pcuCode", pcuCode)
			.setParameter("recordStatus", "A")
			.setHint(QueryHints.REFRESH, HintValues.TRUE)
			.setHint(QueryHints.FETCH, "o.contact")
			.getResultList();
	}
	
	@Remove
	public void destroy(){
		if (institutionListByPcu != null){
			institutionListByPcu = null;
		}
	}
	
}
