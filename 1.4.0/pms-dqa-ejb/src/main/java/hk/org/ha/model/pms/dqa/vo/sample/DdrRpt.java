package hk.org.ha.model.pms.dqa.vo.sample;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DdrRpt {
	
	private String fullDrugDesc;
	private String baseUnit;
	private String modifyDate;
	private String natureOfTrx;
	private String manufName; 
	private String supplierName;
	private String invoiceNo;
	private String batchNo;
	private Integer packSize;
	private Integer movementQty;
	private Integer onHandQty;
	
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	
	public String getBaseUnit() {
		return baseUnit;
	}
	
	public void setBaseUnit(String baseUnit) {
		this.baseUnit = baseUnit;
	}
	
	public String getModifyDate() {
		return modifyDate;
	}
	
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	public String getNatureOfTrx() {
		return natureOfTrx;
	}

	public void setNatureOfTrx(String natureOfTrx) {
		this.natureOfTrx = natureOfTrx;
	}
	
	public String getSupplierName() {
		return supplierName;
	}
	
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
	public Integer getMovementQty() {
		return movementQty;
	}
	
	public void setMovementQty(Integer movementQty) {
		this.movementQty = movementQty;
	}
	
	public Integer getOnHandQty() {
		return onHandQty;
	}
	
	public void setOnHandQty(Integer onHandQty) {
		this.onHandQty = onHandQty;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Integer getPackSize() {
		return packSize;
	}

	public void setPackSize(Integer packSize) {
		this.packSize = packSize;
	}

	public String getManufName() {
		return manufName;
	}

	public void setManufName(String manufName) {
		this.manufName = manufName;
	}
	
	
}
