package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum MicroBioAcceptCriteria implements StringValuedEnum {
	
	Blank(" ","(Blank)"),
	MAC1("MAC1", "Oral Use Preparation without raw material of natural origins (TPC, Y&M & E. Coli)"),
	MAC2("MAC2", "Rectal Use Preparation (TPC, Y&M)"),
	MAC3("MAC3", "Nasal Use Preparation (TPC, Y&M, S.aureus & P. aeruginosa)"),
	MAC4("MAC4", "Topical Use Preparation (TPC, Y&M, S. aureus & P. aeruginosa)"),
	MAC5("MAC5", "Vaginal Use Preparation (TPC, Y&M, S. aureus, P. aeruginosa & C. albicans)"),
	MAC6("MAC6", "Inhalation Use Preparation (TPC, Y&M, S. aureus, P. aeruginosa & Bile-tolerant gram negative bacteria)"),
	MAC7("MAC7", "Transdermal Patches (TPC, Y&M, S. aureus & P. aeruginosa)"),
	MAC8("MAC8", "Oral Use Preparation with raw material of natural origins (TPC, Y&M, E Coli, S. aureus,Salmonella & Bile-tolerant gram negative bacteria)"),
	MAC9("MAC9", "Sterile Preparation"),
	Others("O", "Others");
	
    private final String dataValue;
    private final String displayValue;
        
    MicroBioAcceptCriteria(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<MicroBioAcceptCriteria> {

		private static final long serialVersionUID = -1623620317837922548L;

		@Override
    	public Class<MicroBioAcceptCriteria> getEnumClass() {
    		return MicroBioAcceptCriteria.class;
    	}
    }
}
