package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("riskLevelListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RiskLevelListServiceBean implements RiskLevelListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<RiskLevel> riskLevelList;
	
	@SuppressWarnings("unchecked")
	public void retrieveRiskLevelListByRecordStatus(RecordStatus recordStatus){
		logger.debug("retrieveRiskLevelListByRecordStatus");
		
		riskLevelList = em.createNamedQuery("RiskLevel.findByRecordStatus")
		.setParameter("recordStatus", recordStatus)
		.getResultList();

	}
	
	@Remove
	public void destroy(){
		if(riskLevelList != null) {
			riskLevelList = null;  
		}
	}

}
