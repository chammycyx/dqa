package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CoaStatus implements StringValuedEnum {
	
	NullValue("N", "Null value"),
	VerificationInProgress("V", "Verification in progress"),
	DiscrepancyClarificationInProgress("D", "Discrepancy clarification in progress"),
	Pass("P", "Pass"),
	Fail("F", "Fail");
	
    private final String dataValue;
    private final String displayValue;
        
    CoaStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<CoaStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<CoaStatus> getEnumClass() {
    		return CoaStatus.class;
    	}
    }
}
