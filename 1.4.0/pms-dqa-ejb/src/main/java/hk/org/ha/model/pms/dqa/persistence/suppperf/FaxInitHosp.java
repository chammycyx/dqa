package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_INIT_HOSP")
/*@NamedQueries({
	@NamedQuery(name = "FaxInitHosp.findByFaxInitHospId", query = "select o from FaxInitHosp o where o.faxInitHospId = :faxInitHospId ")
})*/
@Customizer(AuditCustomizer.class)
public class FaxInitHosp extends VersionEntity {
	
	private static final long serialVersionUID = -7377033912251852640L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxInitHospSeq")
	@SequenceGenerator(name = "faxInitHospSeq", sequenceName = "SEQ_FAX_INIT_HOSP", initialValue=10000)
	@Id
	@Column(name="FAX_INIT_HOSP_ID", nullable=false)
	private Long faxInitHospId;
	
	@Column(name="RESPONSE_RPT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date responseRptDate;
	
	@Converter(name = "FaxInitHosp.index0Flag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.index0Flag")
	@Column(name="INDEX0_FLAG", length=1)
	private YesNoFlag index0Flag;	
	
	@Converter(name = "FaxInitHosp.index1Flag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.index1Flag")
	@Column(name="INDEX1_FLAG", length=1)
	private YesNoFlag index1Flag;
	
	@Converter(name = "FaxInitHosp.inspectAllFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.inspectAllFlag")
	@Column(name="INSPECT_ALL_FLAG", length=1)
	private YesNoFlag inspectAllFlag;
	
	@Converter(name = "FaxInitHosp.randomInspectFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.randomInspectFlag")
	@Column(name="RANDOM_INSPECT_FLAG", length=1)
	private YesNoFlag randomInspectFlag;
	
	@Converter(name = "FaxInitHosp.rptFindingFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.rptFindingFlag")
	@Column(name="RPT_FINDING_FLAG", length=1)
	private YesNoFlag rptFindingFlag;
	
	@Column(name="INDEX1_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date index1Date;
	
	@Converter(name = "FaxInitHosp.index2Flag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.index2Flag")
	@Column(name="INDEX2_FLAG", length=1)
	private YesNoFlag index2Flag;
	
	@Converter(name = "FaxInitHosp.batchSuspFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.batchSuspFlag")
	@Column(name="BATCH_SUSP_FLAG", length=1)
	private YesNoFlag batchSuspFlag;
	
	@Column(name="BATCH_SUSP_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date batchSuspDate;
	
	@Converter(name = "FaxInitHosp.productRecallFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.productRecallFlag")
	@Column(name="PRODUCT_RECALL_FLAG", length=1)
	private YesNoFlag productRecallFlag;
	
	@Converter(name = "FaxInitHosp.instLevelFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.instLevelFlag")
	@Column(name="INST_LEVEL_FLAG", length=1)
	private YesNoFlag instLevelFlag;
	
	@Converter(name = "FaxInitHosp.patLevelFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.patLevelFlag")
	@Column(name="PAT_LEVEL_FLAG", length=1)
	private YesNoFlag patLevelFlag;
	
	@Column(name="PRODUCT_RECALL_DESC", length=200)
	private String productRecallDesc;
	
	@Converter(name = "FaxInitHosp.noActionFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.noActionFlag")
	@Column(name="NO_ACTION_FLAG", length=1)
	private YesNoFlag noActionFlag;
	
	@Converter(name = "FaxInitHosp.batchRelFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.batchRelFlag")
	@Column(name="BATCH_REL_FLAG", length=1)
	private YesNoFlag batchRelFlag;
	
	@Converter(name = "FaxInitHosp.stkReplaceFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.stkReplaceFlag")
	@Column(name="STK_REPLACE_FLAG", length=1)
	private YesNoFlag stkReplaceFlag;
	
	@Converter(name = "FaxInitHosp.otherFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInitHosp.otherFlag")
	@Column(name="OTHER_FLAG", length=1)
	private YesNoFlag otherFlag;

	public Long getFaxInitHospId() {
		return faxInitHospId;
	}

	public void setFaxInitHospId(Long faxInitHospId) {
		this.faxInitHospId = faxInitHospId;
	}

	public Date getResponseRptDate() {
		return responseRptDate;
	}

	public void setResponseRptDate(Date responseRptDate) {
		if (responseRptDate != null) {
			this.responseRptDate = new Date(responseRptDate.getTime());
		} else {
			this.responseRptDate = null;
		}
	}

	public YesNoFlag getIndex0Flag() {
		return index0Flag;
	}

	public void setIndex0Flag(YesNoFlag index0Flag) {
		this.index0Flag = index0Flag;
	}

	public YesNoFlag getIndex1Flag() {
		return index1Flag;
	}

	public void setIndex1Flag(YesNoFlag index1Flag) {
		this.index1Flag = index1Flag;
	}

	public YesNoFlag getInspectAllFlag() {
		return inspectAllFlag;
	}

	public void setInspectAllFlag(YesNoFlag inspectAllFlag) {
		this.inspectAllFlag = inspectAllFlag;
	}

	public YesNoFlag getRandomInspectFlag() {
		return randomInspectFlag;
	}

	public void setRandomInspectFlag(YesNoFlag randomInspectFlag) {
		this.randomInspectFlag = randomInspectFlag;
	}

	public YesNoFlag getRptFindingFlag() {
		return rptFindingFlag;
	}

	public void setRptFindingFlag(YesNoFlag rptFindingFlag) {
		this.rptFindingFlag = rptFindingFlag;
	}

	public Date getIndex1Date() {
		return index1Date;
	}

	public void setIndex1Date(Date index1Date) {
		if (index1Date!=null) {
			this.index1Date = new Date(index1Date.getTime());
		} else {
			this.index1Date = null;
		}
	}

	public YesNoFlag getIndex2Flag() {
		return index2Flag;
	}

	public void setIndex2Flag(YesNoFlag index2Flag) {
		this.index2Flag = index2Flag;
	}

	public YesNoFlag getBatchSuspFlag() {
		return batchSuspFlag;
	}

	public void setBatchSuspFlag(YesNoFlag batchSuspFlag) {
		this.batchSuspFlag = batchSuspFlag;
	}

	public Date getBatchSuspDate() {
		return (batchSuspDate!=null)?new Date(batchSuspDate.getTime()):null;
	}

	public void setBatchSuspDate(Date batchSuspDate) {
		if(batchSuspDate!=null) {
			this.batchSuspDate = new Date(batchSuspDate.getTime());
		} else {
			this.batchSuspDate = null;
		}
	}

	public YesNoFlag getProductRecallFlag() {
		return productRecallFlag;
	}

	public void setProductRecallFlag(YesNoFlag productRecallFlag) {
		this.productRecallFlag = productRecallFlag;
	}

	public YesNoFlag getInstLevelFlag() {
		return instLevelFlag;
	}

	public void setInstLevelFlag(YesNoFlag instLevelFlag) {
		this.instLevelFlag = instLevelFlag;
	}

	public YesNoFlag getPatLevelFlag() {
		return patLevelFlag;
	}

	public void setPatLevelFlag(YesNoFlag patLevelFlag) {
		this.patLevelFlag = patLevelFlag;
	}

	public String getProductRecallDesc() {
		return productRecallDesc;
	}

	public void setProductRecallDesc(String productRecallDesc) {
		this.productRecallDesc = productRecallDesc;
	}

	public YesNoFlag getNoActionFlag() {
		return noActionFlag;
	}

	public void setNoActionFlag(YesNoFlag noActionFlag) {
		this.noActionFlag = noActionFlag;
	}

	public YesNoFlag getBatchRelFlag() {
		return batchRelFlag;
	}

	public void setBatchRelFlag(YesNoFlag batchRelFlag) {
		this.batchRelFlag = batchRelFlag;
	}

	public YesNoFlag getStkReplaceFlag() {
		return stkReplaceFlag;
	}

	public void setStkReplaceFlag(YesNoFlag stkReplaceFlag) {
		this.stkReplaceFlag = stkReplaceFlag;
	}

	public YesNoFlag getOtherFlag() {
		return otherFlag;
	}

	public void setOtherFlag(YesNoFlag otherFlag) {
		this.otherFlag = otherFlag;
	}
}
