package hk.org.ha.model.pms.dqa.udt.coa;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum InterfaceFlag implements StringValuedEnum {
		
	Yes("Y", "Yes"),
	No("N", "No");
	
    private final String dataValue;
    private final String displayValue;
        
    InterfaceFlag(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<InterfaceFlag> {

		private static final long serialVersionUID = 80825529061623951L;

		@Override
    	public Class<InterfaceFlag> getEnumClass() {
    		return InterfaceFlag.class;
    	}
    }
}
