package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Collection;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;

import javax.ejb.Local;

@Local
public interface SampleItemListServiceLocal {

	void retrieveSampleItemListByCriteria(SampleItemCriteria sampleItemCriteria);
	
	List<SampleItem> retrieveSampleItemListByRecordStatusRiskLevelCodeForValidation(RecordStatus recordStatus, String riskLevelCode);
	
	void retrieveSampleItemExportList(Collection<Long> sampleItemKeyList);
	
	List<SampleItem> getSampleItemExportList();
	
	void retrieveAllSampleItemListLikeItemCodeRecordStatus(RecordStatus recordStatus, String itemCode);
	
	void retrieveSampleItemListForSampleTest(RecordStatus recordStatus, String itemCode);
	
	List<SampleItem> retrieveSampleItemListByTestFrequency(TestFrequency testFrequencyIn, OrderType orderTypeIn);
	
	void destroy();

}
