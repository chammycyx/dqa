package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("userInfoListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class UserInfoListServiceBean implements UserInfoListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<UserInfo> userInfoList;

	@SuppressWarnings("unchecked")
	public void retrieveUserInfoList(){
		logger.debug("retrieveUserInfoList");
		userInfoList = (List<UserInfo>)em.createNamedQuery("UserInfo.findAll")
						 .setHint(QueryHints.FETCH, "o.contact")
	                     .getResultList();
	}
	
    @Remove
	public void destroy(){	
		if ( userInfoList != null ) {
			userInfoList = null;
		}
	}

}
