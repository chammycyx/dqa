package hk.org.ha.model.pms.dqa.biz;

import java.util.Iterator;
import java.util.Map;

import hk.org.ha.fmk.pms.eai.mail.EaiMessage;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
import hk.org.ha.model.pms.dqa.udt.EmailType;
import hk.org.ha.model.pms.dqa.udt.ModuleType;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("emailLogManager")
@MeasureCalls
public class EmailLogManagerBean implements EmailLogManagerLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private String fromEmail;

	@SuppressWarnings("unchecked")
	public EmailLog createEmailLog(final EaiMessage eaiMessage, final Map attHashMap, EmailType emailType ){
		logger.debug("createEmailLog #0", eaiMessage.getSubject());
		
		StringBuffer attachmentNames = new StringBuffer();	
		Iterator it = attHashMap.entrySet().iterator();				
		while( it.hasNext() ) {
			Map.Entry attachment = (Map.Entry)it.next();			
			attachmentNames.append( (String)attachment.getKey() );			
			attachmentNames.append(";");			
		}	
				
		EmailLog emailLog = new EmailLog();
		emailLog.setEmailFrom( eaiMessage.getFrom() );
		emailLog.setEmailTo( StringUtils.join(eaiMessage.getTos(), ";") );
		emailLog.setEmailCc( StringUtils.join(eaiMessage.getCcs(), ";") );
		emailLog.setEmailBcc( StringUtils.join(eaiMessage.getBccs(), ";") );
		emailLog.setSubject( eaiMessage.getSubject() );		
		emailLog.setContent( eaiMessage.getContent() );
		if(attachmentNames.length() != 0) {
			emailLog.setAttFileNames( attachmentNames.substring(0, attachmentNames.length()-1) );
		}
		emailLog.setEmailType( emailType );
		emailLog.setModuleType( ModuleType.COA );		
		em.persist( emailLog );
		em.flush();
		
		return emailLog;
	}
	
	public EmailLog createEmailLogByLetterTemplate(final LetterTemplate letterTemplate, EmailType emailType) {
		logger.debug("createEmailLogByLetterTemplate #0", letterTemplate.getSubject());
		
		EmailLog emailLog = new EmailLog();
		emailLog.setEmailFrom( fromEmail );
		emailLog.setEmailTo( StringUtils.join(letterTemplate.getToList(), ";") );
		emailLog.setEmailCc( StringUtils.join(letterTemplate.getCcList(), ";") );
		emailLog.setEmailBcc( StringUtils.join(letterTemplate.getBccList(), ";") );
		emailLog.setSubject( letterTemplate.getSubject() );		
		emailLog.setContent( letterTemplate.getContent() );
		
		StringBuffer strBuf = new StringBuffer();
		for( CoaFileFolder cff : letterTemplate.getCoaFileFolderList() ) {
			if( cff.isEnable() ) {
				FileItem f = cff.getFileItem();
				strBuf.append( f.getFileName() );
				strBuf.append( ";" );
			}
		} 
		
		String attachmentNames = strBuf.toString();
		if(attachmentNames.length() > 0) {
			emailLog.setAttFileNames( attachmentNames.substring(0, attachmentNames.length() - 1) );
		}
		emailLog.setEmailType( emailType );
		emailLog.setModuleType( ModuleType.COA );		
		em.persist( emailLog );
		em.flush();
		
		return emailLog;
	}
}
