package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FaxSummaryRpt {
	private String caseStatus;
	private String caseNum;
	private String classification;
	private String itemCode;
	private String itemDesc;
	private String orderType;
	private String supplier;
	private String manuf;
	private String pharmCom;
	private String problemDetail;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastInitHospDate;
	private String initHospCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastInitSuppDate;
	private String initSuppCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastInterimHospDate;
	private String interimHospCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastInterimSuppDate;
	private String interimSuppCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastFinalHospDate;
	private String finalHospCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastFinalSuppDate;
	private String finalSuppCount;
	@Temporal(TemporalType.TIMESTAMP)
	private Date caseCreateDate;
	
	public String getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getManuf() {
		return manuf;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getPharmCom() {
		return pharmCom;
	}
	public void setPharmCom(String pharmCom) {
		this.pharmCom = pharmCom;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}	
	public Date getLastInitHospDate() {
		return (lastInitHospDate!=null)?new Date(lastInitHospDate.getTime()):null;
	}
	public void setLastInitHospDate(Date lastInitHospDate) {
		if(lastInitHospDate!=null)
		{
			this.lastInitHospDate = new Date(lastInitHospDate.getTime());
		}
	}
	public String getInitHospCount() {
		return initHospCount;
	}
	public void setInitHospCount(String initHospCount) {
		this.initHospCount = initHospCount;
	}
	public Date getLastInitSuppDate() {
		return (lastInitSuppDate!=null)?new Date(lastInitSuppDate.getTime()):null;
	}
	public void setLastInitSuppDate(Date lastInitSuppDate) {
		if(lastInitSuppDate!=null)
		{
			this.lastInitSuppDate = new Date(lastInitSuppDate.getTime());
		}
	}
	public String getInitSuppCount() {
		return initSuppCount;
	}
	public void setInitSuppCount(String initSuppCount) {
		this.initSuppCount = initSuppCount;
	}
	public Date getLastInterimHospDate() {
		return (lastInterimHospDate!=null)?new Date(lastInterimHospDate.getTime()):null;
	}
	public void setLastInterimHospDate(Date lastInterimHospDate) {
		if(lastInterimHospDate!=null)
		{
			this.lastInterimHospDate = new Date(lastInterimHospDate.getTime());
		}
	}
	public String getInterimHospCount() {
		return interimHospCount;
	}
	public void setInterimHospCount(String interimHospCount) {
		this.interimHospCount = interimHospCount;
	}
	public Date getLastInterimSuppDate() {
		return (lastInterimSuppDate!=null)?new Date(lastInterimSuppDate.getTime()):null;
	}
	public void setLastInterimSuppDate(Date lastInterimSuppDate) {
		if(lastInterimSuppDate!=null)
		{
			this.lastInterimSuppDate = new Date(lastInterimSuppDate.getTime());
		}
	}
	public String getInterimSuppCount() {
		return interimSuppCount;
	}
	public void setInterimSuppCount(String interimSuppCount) {
		this.interimSuppCount = interimSuppCount;
	}
	public Date getLastFinalHospDate() {
		return (lastFinalHospDate!=null)?new Date(lastFinalHospDate.getTime()):null;
	}
	public void setLastFinalHospDate(Date lastFinalHospDate) {
		if(lastFinalHospDate!=null)
		{
			this.lastFinalHospDate = new Date(lastFinalHospDate.getTime());
		}
	}
	public String getFinalHospCount() {
		return finalHospCount;
	}
	public void setFinalHospCount(String finalHospCount) {
		this.finalHospCount = finalHospCount;
	}
	public Date getLastFinalSuppDate() {
		return (lastFinalSuppDate!=null)?new Date(lastFinalSuppDate.getTime()):null;
	}
	public void setLastFinalSuppDate(Date lastFinalSuppDate) {
		if(lastFinalSuppDate!=null)
		{
			this.lastFinalSuppDate = new Date(lastFinalSuppDate.getTime());
		}
	}
	public String getFinalSuppCount() {
		return finalSuppCount;
	}
	public void setFinalSuppCount(String finalSuppCount) {
		this.finalSuppCount = finalSuppCount;
	}
	public Date getCaseCreateDate() {
		return (caseCreateDate!=null)?new Date(caseCreateDate.getTime()):null;
	}
	public void setCaseCreateDate(Date caseCreateDate) {
		if(caseCreateDate!=null)
		{
			this.caseCreateDate = new Date(caseCreateDate.getTime());
		}
	}
	
}