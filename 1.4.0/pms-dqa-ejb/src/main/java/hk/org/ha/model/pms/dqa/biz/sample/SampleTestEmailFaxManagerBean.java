package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.ApplicationProp;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.TitleType;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.IAMemoContent;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.sample.IAMemoContentVo;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierMemoContentVo;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

@AutoCreate
@Stateless
@Name("sampleTestEmailFaxManager")
@MeasureCalls
public class SampleTestEmailFaxManagerBean implements SampleTestEmailFaxManagerLocal {

	@PersistenceContext
	private EntityManager em;
	
	@In
	private Interpolator interpolator;
	
	@Logger
	private Log logger;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate sampleTestLetterTemplateLab;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate sampleTestLetterTemplateSupplier;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate sampleTestLetterTemplateInstitution;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate templateTitle;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate templateTitleInstitution;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private LetterTemplate templateTitleSupplier;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private IAMemoContentVo iAMemoContentVo;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private SupplierMemoContentVo supplierMemoContentVo;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private UserInfo userInfoForLetterTemplate;
	
	@In(scope=ScopeType.SESSION, required=false)
	@Out(scope=ScopeType.SESSION, required=false)
	private String refNumInst;
	
	@Out(required = false)
	private String refNumSupplier;
	
	@In
	private ApplicationProp applicationProp;
	
	private static final String COMPANY_CODE = "companyCode";

	private static final String EIGHT_ZERO = "00000000";
	private static final String FILL_0 = "{0}";
	private static final String FILL_10 = "{10}";
	private static final String FILL_1 = "{1}";
	private static final String FILL_11 = "{11}";
	private static final String FILL_2 = "{2}";
	private static final String FILL_12 = "{12}";
	private static final String EMPTY_STRING = " ";
	
	private LetterTemplate constructLetterTemplateForIADQType(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SampleTestSchedule sampleTestScheduleIn)
	{
		StringBuilder fullSB = new StringBuilder();
		
		String dateFormat = "dd MMM yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String scheduleNumStr = "";
		Integer iARefNum = funcSeqNumService.retrieveNextFuncSeqNum("IA");

		if (iARefNum!=null){
			NumberFormat numformat = new DecimalFormat(EIGHT_ZERO);
			scheduleNumStr = numformat.format(iARefNum.intValue());
		}
		
		
		refNumInst = scheduleNumStr;
		refNumSupplier = null;
		
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{
			scheduleNumStr,//ref IA
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax(),
			sdf.format(new Date()),
			sampleTestScheduleIn.getInstitution().getInstitutionCode(),
			sampleTestScheduleIn.getInstitution().getContact().constructSupplierInfo(),
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc() +"("+sampleTestScheduleIn.getSampleItem().getItemCode()+")",
			sampleTestScheduleIn.getContract().getManufacturer().getCompanyName(),
			sampleTestScheduleIn.getContract().getSupplier().getSupplierName(),
			(int) Math.ceil(((sampleTestScheduleIn.getReqQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getReqQty())) + (sampleTestScheduleIn.getChemicalAnalysis()==null?0.0:(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()))))/Double.valueOf(sampleTestScheduleIn.getContract().getPackSize())),
		} );	
		
		fullContent = fullContent.replace("{10}", "{0}");
		fullContent = fullContent.replace("{11}", "{1}");
		fullContent = fullContent.replace("{12}", "{2}");
		fullContent = fullContent.replace("{13}", "{3}");
		fullContent = fullContent.replace("{14}", "{4}");
		fullContent = fullContent.replace("{15}", "{5}");
		fullContent = fullContent.replace("{16}", "{2}");
		fullContent = fullContent.replace("{17}", "{3}");
		fullContent = fullContent.replace("{18}", "{4}");
		
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
			sampleTestScheduleIn.getContract().getPackSize(),
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getBaseUnit(),
			userInfoIn.getContact().getTitle(),
			EMPTY_STRING + userInfoIn.getContact().getFirstName(),
			(userInfoIn.getContact().getLastName()==null?"":EMPTY_STRING + userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone()
		} );	

		fullSB.append(fullContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()) + " ("+scheduleNumStr+")");
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		return sampleTestLetterTemplateIn;
	}
	
	@SuppressWarnings("unchecked")
	private List retrieveMspAddress123(SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn){
		List<Supplier> supplierListFind = null;
		List<Company> pharmCompanyListFind = null;
		List<Company> manufacturerListFind = null;
		
		String address1 = "";
		String address2 = "";
		String address3 = "";
		
		List<String> addresss = new ArrayList();
		
		if("Supplier".equals(supplierPharmCompanyManufacturerContactIn.getContactType()))
		{
			supplierListFind = em.createNamedQuery("Supplier.findBySupplierCode")
			.setParameter("supplierCode", supplierPharmCompanyManufacturerContactIn.getCode())
			.getResultList();
			
			if(supplierListFind!=null && supplierListFind.size()>0)
			{
				address1=supplierListFind.get(0).getContact().getAddress1();
				address2=supplierListFind.get(0).getContact().getAddress2();
				address3=supplierListFind.get(0).getContact().getAddress3();
			}
		}
		else if("Pharm Company".equals(supplierPharmCompanyManufacturerContactIn.getContactType()))
		{
			pharmCompanyListFind = em.createNamedQuery("Company.findByCompanyCodePharmCompanyFlag")
			.setParameter(COMPANY_CODE, supplierPharmCompanyManufacturerContactIn.getCode())
			.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
			.getResultList();
			
			if(pharmCompanyListFind!=null && pharmCompanyListFind.size()>0)
			{
				address1=pharmCompanyListFind.get(0).getContact().getAddress1();
				address2=pharmCompanyListFind.get(0).getContact().getAddress2();
				address3=pharmCompanyListFind.get(0).getContact().getAddress3();
			}
		}
		else if("Manufacturer".equals(supplierPharmCompanyManufacturerContactIn.getContactType()))
		{
			manufacturerListFind = em.createNamedQuery("Company.findByCompanyCodeManufFlag")
			.setParameter(COMPANY_CODE, supplierPharmCompanyManufacturerContactIn.getCode())
			.setParameter("manufFlag", YesNoFlag.Yes)
			.getResultList();
			
			if(manufacturerListFind!=null && manufacturerListFind.size()>0)
			{
				address1=manufacturerListFind.get(0).getContact().getAddress1();
				address2=manufacturerListFind.get(0).getContact().getAddress2();
				address3=manufacturerListFind.get(0).getContact().getAddress3();
			}
		}
		
		addresss.add(address1);
		addresss.add(address2);
		addresss.add(address3);
		
		return addresss;
	}
	
	private LetterTemplate constructLetterTemplateForIACType(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SampleTestSchedule sampleTestScheduleIn)
	{
		StringBuilder fullSB = new StringBuilder();
		
		String dateFormat = "dd MMM yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String scheduleNumStr = "";
		Integer iARefNum = funcSeqNumService.retrieveNextFuncSeqNum("IA");
		
		if (iARefNum!=null){
			NumberFormat numformat = new DecimalFormat(EIGHT_ZERO);
			scheduleNumStr = numformat.format(iARefNum.intValue());
		}
		
		refNumInst = scheduleNumStr;
		
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{
			scheduleNumStr,//ref IA
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax(),
			sdf.format(new Date()),
			sampleTestScheduleIn.getInstitution().getInstitutionCode(),
			sampleTestScheduleIn.getInstitution().getContact().constructSupplierInfo(),
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc() +"("+sampleTestScheduleIn.getSampleItem().getItemCode()+")",
			sampleTestScheduleIn.getContract().getManufacturer().getCompanyName(),
			sampleTestScheduleIn.getContract().getSupplier().getSupplierName(),
			(int) Math.ceil(((sampleTestScheduleIn.getReqQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getReqQty())) + (sampleTestScheduleIn.getChemicalAnalysis()==null?0.0:(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()))))/Double.valueOf(sampleTestScheduleIn.getContract().getPackSize())),
		} );	
		
		fullContent = fullContent.replace("{10}", "{0}");
		fullContent = fullContent.replace("{11}", "{1}");
		fullContent = fullContent.replace("{12}", "{2}");
		fullContent = fullContent.replace("{13}", "{3}");
		fullContent = fullContent.replace("{14}", "{4}");
		fullContent = fullContent.replace("{15}", "{5}");
		fullContent = fullContent.replace("{16}", "{2}");
		fullContent = fullContent.replace("{17}", "{3}");
		fullContent = fullContent.replace("{18}", "{4}");
				
		fullContent = interpolator.interpolate( fullContent, new Object[]{  			
				sampleTestScheduleIn.getContract().getPackSize(),
				sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getBaseUnit(),
				userInfoIn.getContact().getTitle(),
				EMPTY_STRING + userInfoIn.getContact().getFirstName(),
				(userInfoIn.getContact().getLastName()==null?"":EMPTY_STRING + userInfoIn.getContact().getLastName()),
				userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone()
		} );	

		fullSB.append(fullContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()) + " (" +scheduleNumStr+ ")");
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		return sampleTestLetterTemplateIn;
	}
	
	@SuppressWarnings("unchecked")
	private LetterTemplate constructLetterTemplateForIASupplierCType(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, SampleTestSchedule sampleTestScheduleIn)
	{
		String address1 = "";
		String address2 = "";
		String address3 = "";
		
		String dateFormat = "dd MMM yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		
		String scheduleNumStr = "";
		Integer sSRefNum = funcSeqNumService.retrieveNextFuncSeqNum("SS");
		

		if (sSRefNum!=null){
			NumberFormat numformat = new DecimalFormat(EIGHT_ZERO);
			scheduleNumStr = numformat.format(sSRefNum.intValue());
		}

		refNumSupplier = scheduleNumStr;
		
		
		List<String> addresss = retrieveMspAddress123(supplierPharmCompanyManufacturerContactIn);
		address1 = addresss.get(0);
		address2 = addresss.get(1);
		address3 = addresss.get(2);
		
		if (address1 == null) {
			address1 = "";
		}
		
		if (address2 == null) {
			address2 = "";
		}
		
		if (address3 == null) {
			address3 = "";
		}
		
		StringBuilder fullSB = new StringBuilder();
		
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{
			scheduleNumStr,//refer SS
			supplierPharmCompanyManufacturerContactIn.getContact().getTitle(),
			supplierPharmCompanyManufacturerContactIn.getContact().getFirstName(),
			(supplierPharmCompanyManufacturerContactIn.getContact().getLastName()==null?"" : supplierPharmCompanyManufacturerContactIn.getContact().getLastName()),
			supplierPharmCompanyManufacturerContactIn.getContact().getPosition()==null?"":supplierPharmCompanyManufacturerContactIn.getContact().getPosition(),
			address1,
			address2,
			address3,
			supplierPharmCompanyManufacturerContactIn.getContact().getFax()==null?"":supplierPharmCompanyManufacturerContactIn.getContact().getFax(),
			sdf.format(new Date())
		} );	
		
		fullContent = fullContent.replace(FILL_10, FILL_0);
		fullContent = fullContent.replace(FILL_11, FILL_1);
		fullContent = fullContent.replace(FILL_12, FILL_2);
		fullContent = fullContent.replace("{13}", "{3}");
		fullContent = fullContent.replace("{14}", "{4}");
		fullContent = fullContent.replace("{15}", "{5}");
		fullContent = fullContent.replace("{16}", "{6}");
		fullContent = fullContent.replace("{17}", "{7}");
		fullContent = fullContent.replace("{18}", "{8}");
		fullContent = fullContent.replace("{19}", "{9}");
		
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
				supplierPharmCompanyManufacturerContactIn.getContact().getTitle(),
				(supplierPharmCompanyManufacturerContactIn.getContact().getLastName()==null?supplierPharmCompanyManufacturerContactIn.getContact().getFirstName() : supplierPharmCompanyManufacturerContactIn.getContact().getLastName()),
				sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc(),
				(sampleTestScheduleIn.getTestPrice()==null?0:sampleTestScheduleIn.getTestPrice()),
				sampleTestScheduleIn.getInstitution().getInstitutionName(),
				sampleTestScheduleIn.getContract().getContractNum() + (sampleTestScheduleIn.getContract().getContractSuffix()==null?"":"/"+sampleTestScheduleIn.getContract().getContractSuffix()),
				sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc(),
				(int) Math.ceil(((sampleTestScheduleIn.getReqQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getReqQty())) + (sampleTestScheduleIn.getChemicalAnalysis()==null?0.0:(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()==null?0.0:Double.valueOf(sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty()))))/Double.valueOf(sampleTestScheduleIn.getContract().getPackSize())),
				sampleTestScheduleIn.getContract().getPackSize(),
				sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getBaseUnit()
		} );
		
		fullContent = fullContent.replace("{20}", FILL_0);
		fullContent = fullContent.replace("{21}", FILL_1);
		fullContent = fullContent.replace("{22}", FILL_2);
		fullContent = fullContent.replace("{23}", "{3}");
		fullContent = fullContent.replace("{24}", "{4}");
		fullContent = fullContent.replace("{25}", "{5}");
		fullContent = fullContent.replace("{26}", "{6}");
		
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
				userInfoIn.getContact().getTitle(),
				userInfoIn.getContact().getFirstName(),
				(userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
				(supplierPharmCompanyManufacturerContactIn.getContact().getOfficePhone()==null?"":supplierPharmCompanyManufacturerContactIn.getContact().getOfficePhone()),
				(userInfoIn.getContact().getAddress1()==null?"":userInfoIn.getContact().getAddress1()),
				(userInfoIn.getContact().getAddress2()==null?"":userInfoIn.getContact().getAddress2()),
				(userInfoIn.getContact().getAddress3()==null?"":userInfoIn.getContact().getAddress3())
				
		} );

		fullSB.append(fullContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()) + " (" + scheduleNumStr + ")");
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		return sampleTestLetterTemplateIn;
	}
	
	@SuppressWarnings("unchecked")
	private SupplierMemoContentVo constructFaxTemplateForIASupplierCType(LetterTemplate fullTemplateIn, SampleTestSchedule sampleTestScheduleIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, UserInfo defaultCpoContactUserInfoIn, UserInfo cpoSignatureUserInfo)
	{
		supplierMemoContentVo = new SupplierMemoContentVo();		
		String dateFormat = "dd MMMM yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);		
		Contact cpoSignatureContact = cpoSignatureUserInfo.getContact();
		DateTime sendDateTime = new DateTime();
		sendDateTime = sendDateTime.plusDays(applicationProp.getLetterToSuppSendDateBuffer());
		if (sendDateTime.getDayOfWeek() == DateTimeConstants.SATURDAY) {
			sendDateTime = sendDateTime.plusDays(2);
		} else if (sendDateTime.getDayOfWeek() == DateTimeConstants.SUNDAY){
			sendDateTime = sendDateTime.plusDays(1);
		}
		supplierMemoContentVo.setSendDate(sendDateTime.toDate());
		
		supplierMemoContentVo.setSampleTestScheduleFax(getLazySampleTestSchedule(sampleTestScheduleIn));
		supplierMemoContentVo.setUserInfoFax(cpoSignatureUserInfo);
		
		supplierMemoContentVo.setToSupplier(supplierPharmCompanyManufacturerContactIn.getCode() + " - " + supplierPharmCompanyManufacturerContactIn.getName());
		supplierMemoContentVo.setToSupplierContactPerson(
					supplierPharmCompanyManufacturerContactIn.getContact().getFirstName() 
					+ " "
					+ supplierPharmCompanyManufacturerContactIn.getContact().getLastName() 
					+ (supplierPharmCompanyManufacturerContactIn.getContact().getPosition()!=null
						?
						" ("+ supplierPharmCompanyManufacturerContactIn.getContact().getPosition() + ")"
						:
						""
					)
				);		
		supplierMemoContentVo.setSupplierName(
				StringUtils.equals(supplierPharmCompanyManufacturerContactIn.getName(), supplierPharmCompanyManufacturerContactIn.getContact().getFirstName())
				?
				null
				:
				supplierPharmCompanyManufacturerContactIn.getName()
		);
		
		List<String> addresss = retrieveMspAddress123(supplierPharmCompanyManufacturerContactIn);
		supplierMemoContentVo.setContractNum(sampleTestScheduleIn.getContract().getContractNum());
		supplierMemoContentVo.setReceiverTitle((supplierPharmCompanyManufacturerContactIn.getContact().getTitle() == TitleType.NIL?"":
			supplierPharmCompanyManufacturerContactIn.getContact().getTitle().getDisplayValue()));
		supplierMemoContentVo.setReceiverFirstName(supplierPharmCompanyManufacturerContactIn.getContact().getFirstName());
		supplierMemoContentVo.setReceiverLastName((supplierPharmCompanyManufacturerContactIn.getContact().getLastName()==null?"":
			supplierPharmCompanyManufacturerContactIn.getContact().getLastName()));
		supplierMemoContentVo.setPosition(supplierPharmCompanyManufacturerContactIn.getContact().getPosition()==null?"":
			supplierPharmCompanyManufacturerContactIn.getContact().getPosition());
		supplierMemoContentVo.setAddress1((StringUtils.isBlank(addresss.get(0))?"":addresss.get(0)));
		supplierMemoContentVo.setAddress2((StringUtils.isBlank(addresss.get(1))?"":addresss.get(1)));
		supplierMemoContentVo.setAddress3((StringUtils.isBlank(addresss.get(2))?"":addresss.get(2)));
		supplierMemoContentVo.setReceiverTel((supplierPharmCompanyManufacturerContactIn.getContact().getOfficePhone()==null?"":
			supplierPharmCompanyManufacturerContactIn.getContact().getOfficePhone()));
		supplierMemoContentVo.setReceiverFax((supplierPharmCompanyManufacturerContactIn.getContact().getFax()==null?"":
			supplierPharmCompanyManufacturerContactIn.getContact().getFax()));
		supplierMemoContentVo.setItemCode((sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":
			sampleTestScheduleIn.getSampleItem().getDmDrug().getItemCode()));
		supplierMemoContentVo.setDrugDesc((sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":
			sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc()) + " (" + supplierMemoContentVo.getItemCode() + ")");
		supplierMemoContentVo.setFaxMemoParagraph1(IAMemoContent.IS1.getDisplayValue());
		supplierMemoContentVo.setMemoDate(sdf.format(supplierMemoContentVo.getSendDate()));
		supplierMemoContentVo.setSenderTitle(cpoSignatureContact.getTitle() == TitleType.NIL?"":cpoSignatureContact.getTitle().getDisplayValue());
		supplierMemoContentVo.setSenderFirstName(StringUtils.isBlank(cpoSignatureContact.getFirstName())?"":cpoSignatureContact.getFirstName());
		supplierMemoContentVo.setSenderLastName(StringUtils.isBlank(cpoSignatureContact.getLastName())?"":cpoSignatureContact.getLastName());
		
		supplierMemoContentVo.setRequiredQty(sampleTestScheduleIn.buildActualQtyForFax());
		supplierMemoContentVo.constructParagraph2();
		supplierMemoContentVo.constructParagraph3();
		
		supplierMemoContentVo.setFaxMemoParagraph4(IAMemoContent.IS4.getDisplayValue() + constructUserContact(defaultCpoContactUserInfoIn));
		supplierMemoContentVo.setFaxMemoParagraph5(IAMemoContent.IS5.getDisplayValue());
		return supplierMemoContentVo;
	}
	
	private String constructUserContact(UserInfo userInfoIn)
	{
		Contact contact = userInfoIn.getContact();
		return contact.getTitle() + (StringUtils.isBlank(contact.getFirstName())?"":EMPTY_STRING + contact.getFirstName()) + 
			(StringUtils.isBlank(contact.getLastName())?"":EMPTY_STRING + contact.getLastName()) + " at " + (StringUtils.isBlank(contact.getOfficePhone())?"":EMPTY_STRING + contact.getOfficePhone() + ".");
	}
	
	private LetterTemplate constructMicroLabLetterTemplateForLab(LetterTemplate headerTemplateIn, LetterTemplate bodyTemplateIn, LetterTemplate footerTemplateIn, UserInfo userInfoIn, List<SampleTestSchedule> scheduleListIn)
	{
		StringBuilder headerSB = new StringBuilder();
		StringBuilder bodySB = new StringBuilder();
		StringBuilder footerSB = new StringBuilder();
		
		String headerContent = interpolator.interpolate( headerTemplateIn.getContent(), new Object[]{  
			scheduleListIn.get(0).getLab().getLabName(),
			scheduleListIn.get(0).getLab().getContact().getFirstName(),
			(scheduleListIn.get(0).getLab().getContact().getLastName()==null?"" : scheduleListIn.get(0).getLab().getContact().getLastName()),
			(scheduleListIn.get(0).getLab().getContact().getPosition()==null?"":scheduleListIn.get(0).getLab().getContact().getPosition())
			} );	
		headerSB.append(headerContent);
		
		int itemNo = 1 ;
		String bodyContent="";
		for (SampleTestSchedule sts : scheduleListIn ){
			bodyContent="";
			bodyContent = interpolator.interpolate( bodyTemplateIn.getContent(), new Object[]{ itemNo , 
																(sts.getSampleItem().getItemCode()==null?"":sts.getSampleItem().getItemCode()),
																(sts.getSampleItem().getDmDrug()==null?"":(sts.getSampleItem().getDmDrug().getFullDrugDesc())),
																sts.getContract().getManufacturer().getCompanyName(),
																(sts.getBatchNum()==null?"":sts.getBatchNum()),
																(sts.getExpiryDate()==null?"":sts.getExpiryDate()),
																sts.getLabCollectQty() + " " + (sts.getSampleItem().getDmDrug()==null?"":(sts.getSampleItem().getDmDrug().getBaseUnit())),
																(sts.getMicroBioTest()==null?"" : (sts.getMicroBioTest().getTestParam()==null?"":sts.getMicroBioTest().getTestParam().getDisplayValue())),
																(sts.getMicroBioTest()==null?"" : (sts.getMicroBioTest().getNationalPharmStdDesc()==null?"":sts.getMicroBioTest().getNationalPharmStdDesc())),
																(sts.getMicroBioTest()==null?"" : (sts.getMicroBioTest().getAcceptCriteriaDesc()==null?"":sts.getMicroBioTest().getAcceptCriteriaDesc()))
																} );	
			bodySB.append(bodyContent);
			itemNo++;
		}
		
		String footerContent = interpolator.interpolate( footerTemplateIn.getContent(), new Object[]{  
			userInfoIn.getContact().getFirstName() + " " + (userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getPosition()==null?"":userInfoIn.getContact().getPosition(),
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax()
			} );	
		footerSB.append(footerContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(headerSB);
		newBodyContent.append(bodySB);
		newBodyContent.append(footerSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(headerTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(headerTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(headerTemplateIn.getSubject()));
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		
		return sampleTestLetterTemplateIn;
	}
	
	private LetterTemplate constructChemLabLetterTemplateForLab(LetterTemplate headerTemplateIn, LetterTemplate bodyTemplateIn, LetterTemplate footerTemplateIn, UserInfo userInfoIn, List<SampleTestSchedule> scheduleListIn)
	{
		StringBuilder headerSB = new StringBuilder();
		StringBuilder bodySB = new StringBuilder();
		StringBuilder footerSB = new StringBuilder();
		
		String headerContent = interpolator.interpolate( headerTemplateIn.getContent(), new Object[]{  
			scheduleListIn.get(0).getLab().getLabName(),
			scheduleListIn.get(0).getLab().getContact().getFirstName(),
			(scheduleListIn.get(0).getLab().getContact().getLastName()==null?"" : scheduleListIn.get(0).getLab().getContact().getLastName()),
			scheduleListIn.get(0).getLab().getContact().getPosition()==null?"":scheduleListIn.get(0).getLab().getContact().getPosition()
			} );	
		headerSB.append(headerContent);
		
		int itemNo = 1 ;
		String bodyContent="";
		for (SampleTestSchedule sts : scheduleListIn ){
			bodyContent="";
			bodyContent = interpolator.interpolate( bodyTemplateIn.getContent(), new Object[]{ itemNo ,
																(sts.getSampleItem().getItemCode()==null?"":sts.getSampleItem().getItemCode()),
																(sts.getSampleItem().getDmDrug()==null?"":(sts.getSampleItem().getDmDrug().getFullDrugDesc())),
																sts.getContract().getManufacturer().getCompanyName(),
																(sts.getBatchNum()==null?"":sts.getBatchNum()),
																(sts.getExpiryDate()==null?"":sts.getExpiryDate()),
																sts.getLabCollectQty() + " " + (sts.getSampleItem().getDmDrug()==null?"":(sts.getSampleItem().getDmDrug().getBaseUnit())),
																(sts.getChemicalAnalysis()==null?"" : (sts.getChemicalAnalysis().getSpecification()==null?"":sts.getChemicalAnalysis().getSpecification().getDisplayValue())),
																(sts.getChemicalAnalysis()==null?"" : (sts.getChemicalAnalysis().getTestDesc()==null?"":sts.getChemicalAnalysis().getTestDesc()))
																} );	
			bodySB.append(bodyContent);
			itemNo++;
		}
		
		String footerContent = interpolator.interpolate( footerTemplateIn.getContent(), new Object[]{  
			userInfoIn.getContact().getFirstName() + " " + (userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getPosition()==null?"":userInfoIn.getContact().getPosition(),
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax()
			} );	
		footerSB.append(footerContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(headerSB);
		newBodyContent.append(bodySB);
		newBodyContent.append(footerSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(headerTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(headerTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(headerTemplateIn.getSubject()));
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		
		return sampleTestLetterTemplateIn;
	}
	
	private LetterTemplate constructMicroLabLetterTemplateForDocument(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SampleTestSchedule sampleTestScheduleIn)
	{
		StringBuilder fullSB = new StringBuilder();
		
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{  
			sampleTestScheduleIn.getLab().getLabName(),
			sampleTestScheduleIn.getLab().getContact().getFirstName(),
			(sampleTestScheduleIn.getLab().getContact().getLastName()==null?"" : sampleTestScheduleIn.getLab().getContact().getLastName()),
			sampleTestScheduleIn.getLab().getContact().getPosition()==null?"":sampleTestScheduleIn.getLab().getContact().getPosition(),
			sampleTestScheduleIn.getSampleItem().getItemCode()==null?"":sampleTestScheduleIn.getSampleItem().getItemCode(),	
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc(),
			(sampleTestScheduleIn.getMicroBioTest()==null?"":(sampleTestScheduleIn.getMicroBioTest().getTestParam()==null?"":sampleTestScheduleIn.getMicroBioTest().getTestParam().getDisplayValue())),
			(sampleTestScheduleIn.getMicroBioTest()==null?"":(sampleTestScheduleIn.getMicroBioTest().getNationalPharmStdDesc()==null?"":sampleTestScheduleIn.getMicroBioTest().getNationalPharmStdDesc())),
			userInfoIn.getContact().getFirstName() + " " + (userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getPosition()==null?"":userInfoIn.getContact().getPosition()
			//userInfoIn.getContact().getFax()
			} );	
		
		fullContent = fullContent.replace(FILL_10, FILL_0);
		fullContent = fullContent.replace(FILL_11, FILL_1);
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax()
		} );	

		fullSB.append(fullContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()));
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		
		return sampleTestLetterTemplateIn;
	}
	
	private LetterTemplate constructChemLabLetterTemplateForDocument(LetterTemplate fullTemplateIn,  UserInfo userInfoIn, SampleTestSchedule sampleTestScheduleIn)
	{
		StringBuilder fullSB = new StringBuilder();	
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{  
			sampleTestScheduleIn.getLab().getLabName(),
			sampleTestScheduleIn.getLab().getContact().getFirstName(),
			(sampleTestScheduleIn.getLab().getContact().getLastName()==null?"" : sampleTestScheduleIn.getLab().getContact().getLastName()),
			sampleTestScheduleIn.getLab().getContact().getPosition()==null?"":sampleTestScheduleIn.getLab().getContact().getPosition(),
			sampleTestScheduleIn.getSampleItem().getItemCode()==null?"":sampleTestScheduleIn.getSampleItem().getItemCode(),	
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc(),
			(sampleTestScheduleIn.getChemicalAnalysis()==null?"":(sampleTestScheduleIn.getChemicalAnalysis().getSpecification().getDisplayValue())),
			(sampleTestScheduleIn.getChemicalAnalysis()==null?"":(sampleTestScheduleIn.getChemicalAnalysis().getTestDesc()==null?"":sampleTestScheduleIn.getChemicalAnalysis().getTestDesc())),
			userInfoIn.getContact().getFirstName() + " " + (userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getPosition()==null?"":userInfoIn.getContact().getPosition()
			} );
		
		fullContent = fullContent.replace(FILL_10, FILL_0);
		fullContent = fullContent.replace(FILL_11, FILL_1);
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
			userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax()
		} );
		
		fullSB.append(fullContent);
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()));
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		
		return sampleTestLetterTemplateIn;
	}
	
	public void retrieveLetterTemplateForSchedule(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn){
		LetterTemplate fullTemplate;
		fullTemplate= retrieveLetterTemplateByCode("S_L1_SUPP");
		templateTitle = fullTemplate;
		sampleTestLetterTemplateSupplier  = constructLetterTemplateForSchedule(fullTemplate, userInfoIn, supplierPharmCompanyManufacturerContactIn, sampleTestScheduleIn);
		
		sampleTestLetterTemplateSupplier.setSubject(sampleTestLetterTemplateSupplier.getSubject() + " - " + sampleTestScheduleIn.getSampleItem().getItemCode());
		
		sampleTestLetterTemplateLab = new LetterTemplate();
		sampleTestLetterTemplateInstitution  = new LetterTemplate();
		templateTitleInstitution=new LetterTemplate();
		templateTitleSupplier=new LetterTemplate();
		userInfoForLetterTemplate = userInfoIn;
		refNumInst = null;
		refNumSupplier = null;
		iAMemoContentVo = new IAMemoContentVo();
	}
	
	public LetterTemplate constructLetterTemplateForSchedule(LetterTemplate fullTemplateIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, SampleTestSchedule sampleTestScheduleIn)
	{
		StringBuilder fullSB = new StringBuilder();
		
		String fullContent = interpolator.interpolate( fullTemplateIn.getContent(), new Object[]{
			supplierPharmCompanyManufacturerContactIn.getCode(),
			supplierPharmCompanyManufacturerContactIn.getContact().getTitle()==null?"":supplierPharmCompanyManufacturerContactIn.getContact().getTitle(),
			supplierPharmCompanyManufacturerContactIn.getContact().getFirstName(),
			(supplierPharmCompanyManufacturerContactIn.getContact().getLastName()==null?"" : supplierPharmCompanyManufacturerContactIn.getContact().getLastName()),
			supplierPharmCompanyManufacturerContactIn.getContact().getPosition()==null?"":", "+supplierPharmCompanyManufacturerContactIn.getContact().getPosition(),
			userInfoIn.getContact().getFirstName() + " " + (userInfoIn.getContact().getLastName()==null?"":userInfoIn.getContact().getLastName()),
			userInfoIn.getContact().getPosition()==null?"":userInfoIn.getContact().getPosition(),
			userInfoIn.getContact().getOfficePhone()==null?"":userInfoIn.getContact().getOfficePhone(),
			userInfoIn.getContact().getFax()==null?"":userInfoIn.getContact().getFax(),
			sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc()
			
		} );	
		
		fullContent = fullContent.replace(FILL_10, FILL_0);
		fullContent = fullContent.replace(FILL_11, FILL_1);
		fullContent = fullContent.replace(FILL_12, FILL_2);
		fullContent = interpolator.interpolate( fullContent, new Object[]{  
				sampleTestScheduleIn.getSampleItem().getDmDrug()==null?"":sampleTestScheduleIn.getSampleItem().getDmDrug().getFormCode(),
				sampleTestScheduleIn.getContract().getSupplier().getSupplierName(),
				sampleTestScheduleIn.getContract().getManufacturer().getCompanyName()
		} );	

		fullSB.append(fullContent);
		
		StringBuilder newBodyContent = new StringBuilder();
		newBodyContent.append(fullSB);
		
		LetterTemplate sampleTestLetterTemplateIn = new LetterTemplate();
		sampleTestLetterTemplateIn.setCode(fullTemplateIn.getCode());
		sampleTestLetterTemplateIn.setName(fullTemplateIn.getName());	
		sampleTestLetterTemplateIn.setSubject(interpolator.interpolate(fullTemplateIn.getSubject()));
		sampleTestLetterTemplateIn.setContent(newBodyContent.toString());
		return sampleTestLetterTemplateIn;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	public LetterTemplate retrieveLetterTemplateByCode(String code){
		
		logger.debug("retrieveLetterTemplateByCode #0", code);
		LetterTemplate tmpLetterTemplate = em.find(LetterTemplate.class, code);
		
		LetterTemplate letterTemplate = new LetterTemplate();
		letterTemplate.setCode(tmpLetterTemplate.getCode());
		letterTemplate.setName(tmpLetterTemplate.getName());	
		letterTemplate.setSubject(tmpLetterTemplate.getSubject()==null?"":interpolator.interpolate(tmpLetterTemplate.getSubject()));
		letterTemplate.setContent(tmpLetterTemplate.getContent()==null?"":interpolator.interpolate(tmpLetterTemplate.getContent()));
		
		return letterTemplate;
	} 
	
	public void retrieveLetterTemplateForInstitutionAssignment(SampleTestSchedule sampleTestScheduleIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn, UserInfo defaultCpoContactUserInfo, UserInfo cpoSignatureUserInfo){
		LetterTemplate fullInstitutionTemplate = null;
		LetterTemplate fullSupplierTemplate = null;
		supplierMemoContentVo = new SupplierMemoContentVo();
		
		if(sampleTestScheduleIn.getOrderType().equals(OrderType.Contract))
		{
			fullInstitutionTemplate= retrieveLetterTemplateByCode("IA_C_HOSP");
			fullSupplierTemplate= retrieveLetterTemplateByCode("IA_SR_SUPP");
			
			templateTitleInstitution = fullInstitutionTemplate;
			templateTitleSupplier = fullSupplierTemplate;
			
			sampleTestLetterTemplateInstitution  = constructLetterTemplateForIACType(fullInstitutionTemplate, defaultCpoContactUserInfo, sampleTestScheduleIn);
			sampleTestLetterTemplateSupplier  = constructLetterTemplateForIASupplierCType(fullSupplierTemplate, defaultCpoContactUserInfo, supplierPharmCompanyManufacturerContactIn, sampleTestScheduleIn);
						
			sampleTestLetterTemplateInstitution.setSubject(sampleTestLetterTemplateInstitution.getSubject() + " - " + sampleTestScheduleIn.getSampleItem().getItemCode());
			sampleTestLetterTemplateSupplier.setSubject(sampleTestLetterTemplateSupplier.getSubject() + " - " + sampleTestScheduleIn.getSampleItem().getItemCode());
			
			supplierMemoContentVo  = constructFaxTemplateForIASupplierCType(fullSupplierTemplate, sampleTestScheduleIn, supplierPharmCompanyManufacturerContactIn, defaultCpoContactUserInfo, cpoSignatureUserInfo);
			supplierMemoContentVo.setRefNum(refNumSupplier);
		}
		else
		{
			fullInstitutionTemplate = retrieveLetterTemplateByCode("IA_DQ_HOSP");
			templateTitleInstitution = fullInstitutionTemplate;
			sampleTestLetterTemplateInstitution  = constructLetterTemplateForIADQType(fullInstitutionTemplate, defaultCpoContactUserInfo, sampleTestScheduleIn);
			
			sampleTestLetterTemplateInstitution.setSubject(sampleTestLetterTemplateInstitution.getSubject() + " - " + sampleTestScheduleIn.getSampleItem().getItemCode());
			
			templateTitleSupplier = new LetterTemplate();
			sampleTestLetterTemplateSupplier  = new LetterTemplate();			
		}
			
		iAMemoContentVo = new IAMemoContentVo();
		iAMemoContentVo.setSampleTestScheduleFax(getLazySampleTestSchedule(sampleTestScheduleIn));
		iAMemoContentVo.setSupplierPharmCompanyManufacturerContactFax(supplierPharmCompanyManufacturerContactIn);
		iAMemoContentVo.setUserInfoFax(cpoSignatureUserInfo);
		iAMemoContentVo.setFaxMemoParagraph1(IAMemoContent.IA1.getDisplayValue() + sampleTestScheduleIn.buildActualQtyForFax() + ".");
		if (sampleTestScheduleIn.getOrderType().equals(OrderType.Contract))
		{
			iAMemoContentVo.setFaxMemoParagraph2(IAMemoContent.IA2C.getDisplayValue());
			iAMemoContentVo.setFaxMemoParagraph3(IAMemoContent.IA3C.getDisplayValue());

			if (sampleTestScheduleIn.getChemicalAnalysis()!=null && sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty() != null) {
				iAMemoContentVo.setFaxMemoParagraph4(Interpolator.instance().interpolate(
						IAMemoContent.IA4C.getDisplayValue(), 
						new Object[] {sampleTestScheduleIn.buildActualRetentionQtyForFax()}
				));
				iAMemoContentVo.setFaxMemoParagraphOther(IAMemoContent.IA5C.getDisplayValue() + constructUserContact(defaultCpoContactUserInfo));
			} else {
				iAMemoContentVo.setFaxMemoParagraph4(IAMemoContent.IA5C.getDisplayValue() + constructUserContact(defaultCpoContactUserInfo));
			}
		}
		else
		{
			if (sampleTestScheduleIn.getOrderType().getDataValue().equals("D") || sampleTestScheduleIn.getOrderType().getDataValue().equals("Q")) {
				iAMemoContentVo.setFaxMemoParagraph2(IAMemoContent.IA2DQ.getDisplayValue());
				iAMemoContentVo.setFaxMemoParagraph3(IAMemoContent.IA3DQ.getDisplayValue());
				if (sampleTestScheduleIn.getChemicalAnalysis()!=null && sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty() != null) {
					iAMemoContentVo.setFaxMemoParagraph4(
							Interpolator.instance().interpolate(
									IAMemoContent.IA4C.getDisplayValue(), 
									new Object[] {sampleTestScheduleIn.buildActualRetentionQtyForFax()}		
					));
					iAMemoContentVo.setFaxMemoParagraphOther(IAMemoContent.IA5DQ.getDisplayValue() + constructUserContact(defaultCpoContactUserInfo));	
				} else {
					iAMemoContentVo.setFaxMemoParagraph4(IAMemoContent.IA5DQ.getDisplayValue() + constructUserContact(defaultCpoContactUserInfo));
				}
			} else {
				//suspected block of codes, it may not be hit 
				iAMemoContentVo.setFaxMemoParagraph2("");
				iAMemoContentVo.setFaxMemoParagraph3("");
				iAMemoContentVo.setFaxMemoParagraph4("");
				iAMemoContentVo.setFaxMemoParagraphOther("");
			}
		}
		
		iAMemoContentVo.setRefNum(refNumInst);
		
		DateTime sendDateTime = new DateTime();
		sendDateTime = sendDateTime.plusDays(applicationProp.getMemoToHospSendDateBuffer());
		if (sendDateTime.getDayOfWeek() == DateTimeConstants.SATURDAY) {
			sendDateTime = sendDateTime.plusDays(2);
		} else if (sendDateTime.getDayOfWeek() == DateTimeConstants.SUNDAY){
			sendDateTime = sendDateTime.plusDays(1);
		}
		iAMemoContentVo.setSendDate(sendDateTime.toDate());
		
		sampleTestLetterTemplateLab = new LetterTemplate();
		templateTitle = new LetterTemplate();
		userInfoForLetterTemplate = defaultCpoContactUserInfo;
	}
	
	public void retrieveLetterTemplateForLabTest(List<SampleTestSchedule> scheduleList, SampleTestSchedule sampleTestScheduleIn, UserInfo userInfoIn, SupplierPharmCompanyManufacturerContact supplierPharmCompanyManufacturerContactIn){
		LetterTemplate headerTemplate = null;
		LetterTemplate bodyTemplate = null;
		LetterTemplate footerTemplate = null;
		
		LetterTemplate fullTemplate = null;
		
		iAMemoContentVo = new IAMemoContentVo();
		supplierMemoContentVo = new SupplierMemoContentVo();
		
		if(scheduleList!=null)
		{
			//for Lab Test (LT)
			if(scheduleList.get(0).getScheduleStatus().equals(ScheduleStatus.LabTest))
			{
				String scheduleNumStr = "";
				Integer cNRefNum = funcSeqNumService.getNextFuncSeqNum("CN");
				
				if (cNRefNum!=null){
					NumberFormat numformat = new DecimalFormat(EIGHT_ZERO);
					scheduleNumStr = "CN" + numformat.format(cNRefNum.intValue());
				}
				
				if(scheduleList.get(0).getSampleTest().getTestCode().equals("MICRO")){
					headerTemplate = retrieveLetterTemplateByCode("LT_MIC_H");
					bodyTemplate = retrieveLetterTemplateByCode("LT_MIC_B");
					footerTemplate = retrieveLetterTemplateByCode("LT_MIC_F");
					templateTitle = retrieveLetterTemplateByCode("LT_MIC");
					
					sampleTestLetterTemplateLab = constructMicroLabLetterTemplateForLab(headerTemplate, bodyTemplate, footerTemplate, userInfoIn, scheduleList);
					sampleTestLetterTemplateLab.setCode(templateTitle.getCode());
					sampleTestLetterTemplateLab.setSubject(sampleTestLetterTemplateLab.getSubject() + " ("+scheduleNumStr+ ")");
					
				}
				else
				{
					headerTemplate = retrieveLetterTemplateByCode("LT_CHEM_H");
					bodyTemplate = retrieveLetterTemplateByCode("LT_CHEM_B");
					footerTemplate = retrieveLetterTemplateByCode("LT_CHEM_F");
					templateTitle = retrieveLetterTemplateByCode("LT_CHEM");
					
					sampleTestLetterTemplateLab = constructChemLabLetterTemplateForLab(headerTemplate, bodyTemplate, footerTemplate, userInfoIn, scheduleList);
					sampleTestLetterTemplateLab.setCode(templateTitle.getCode());
					sampleTestLetterTemplateLab.setSubject(sampleTestLetterTemplateLab.getSubject() + " ("+scheduleNumStr+ ")");
				}
			}
		}
		else if(sampleTestScheduleIn!=null)
		{
			//for Document (D)
			if(sampleTestScheduleIn.getScheduleStatus().equals(ScheduleStatus.Document))
			{
				if(sampleTestScheduleIn.getSampleTest().getTestCode().equals("MICRO")){
					fullTemplate= retrieveLetterTemplateByCode("D_MIC_LAB");
					templateTitle = fullTemplate;
					sampleTestLetterTemplateLab = constructMicroLabLetterTemplateForDocument(fullTemplate, userInfoIn, sampleTestScheduleIn);
				}
				else
				{
					fullTemplate= retrieveLetterTemplateByCode("D_CHEM_LAB");
					templateTitle = fullTemplate;
					sampleTestLetterTemplateLab  = constructChemLabLetterTemplateForDocument(fullTemplate, userInfoIn, sampleTestScheduleIn);
				}
				
				sampleTestLetterTemplateLab.setSubject(sampleTestLetterTemplateLab.getSubject() + " - " + sampleTestScheduleIn.getSampleItem().getItemCode());
			}
		}
		sampleTestLetterTemplateSupplier = new LetterTemplate();
		sampleTestLetterTemplateInstitution  = new LetterTemplate();
		templateTitleInstitution=new LetterTemplate();
		templateTitleSupplier=new LetterTemplate();
		userInfoForLetterTemplate = userInfoIn;
		refNumInst = null;
		refNumSupplier = null;
		
	}
}
