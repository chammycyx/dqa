package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qaProblemListForLastFiveCaseNumService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class QaProblemListForLastFiveCaseNumServiceBean implements QaProblemListForLastFiveCaseNumServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
		
	@Out(required = false)
	private List<QaProblem> qaProblemListForLastFiveCaseNum;
	
	
		
	public void retrieveQaProblemListWithLastFiveCaseNum(){
		logger.debug("qaProblemListForLastFiveCaseNum");
		
		
		qaProblemListForLastFiveCaseNum=new ArrayList<QaProblem>();
		
		List<QaProblem> tmpQaProblemListForLastFiveCaseNum = em.createNamedQuery("QaProblem.findAllWithOrderByCreateDateDesc")
								.setHint(QueryHints.FETCH, "o.problemHeader")
								.setHint(QueryHints.FETCH, "o.supplier")
								.setHint(QueryHints.FETCH, "o.manufacturer")
								.setHint(QueryHints.FETCH, "o.pharmCompany")
								.setHint(QueryHints.BATCH, "o.pharmProblemList")
								.setHint(QueryHints.BATCH, "o.qaBatchNumList")
								.setHint(QueryHints.BATCH, "o.qaProblemNature")
								.setHint(QueryHints.BATCH, "o.classification")
								.setMaxResults(5)
								.getResultList();
		
		if(tmpQaProblemListForLastFiveCaseNum!=null && tmpQaProblemListForLastFiveCaseNum.size()>0)
		{
			for(QaProblem qc:tmpQaProblemListForLastFiveCaseNum)
			{
				getLazyQaProblem(qc);
				qaProblemListForLastFiveCaseNum.add(qc);
			}
		}
		else
		{
			qaProblemListForLastFiveCaseNum=null;
		}
		
	}
	
	
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
			pc.getPharmBatchNumList();
			pc.getPharmBatchNumList().size();
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
		}
		return qaProblemLazy;
	}
	
	
		
	@Remove
	public void destroy(){
		if (qaProblemListForLastFiveCaseNum != null){
			qaProblemListForLastFiveCaseNum = null;
		}
	}
}
