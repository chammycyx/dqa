package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ChemAnalysisExclusion  implements StringValuedEnum {
	
	BIO("BIO", "Biological products such as vaccines, blood products, immunoglobulins, enzymes"),
	BIT("BIT", " Biotechnology-derived products such as rDNA recombinant insulins, interferons"),
	CM("CM", "Contrast media (e.g barium sulphate suspension, iomeprol injection) "),
	RP("RP", "Radioactive pharmaceuticals (e.g sodium iodide (i-131) therapy capsule, technetium-99m sulphur colloid injection)"),
	MV("MV", "Multivitamin and mineral preparations/ nutrition products (iv and po)"),
	MES("MES", "PD/HF/HD fluids and other multiple electrolyte solutions"),
	HI("HI", "Products with herbal ingredients"),
	MG("MG", "Medical gases"),
	AI("3AI", "Drugs with more than three active ingredients "),
	DD("DD", "Dangerous Drug"),
	EI("EI", "Expensive Item"),
	SDF("SDF", "Special formulation: transdermal patch"),
	O("O", "Other");
	
	private final String dataValue;
    private final String displayValue;
	
	ChemAnalysisExclusion(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static class Converter extends StringValuedEnumConverter<ChemAnalysisExclusion> {

		private static final long serialVersionUID = 3601829209059683642L;

		@Override
    	public Class<ChemAnalysisExclusion> getEnumClass() {
    		return ChemAnalysisExclusion.class;
    	}
    }
}
