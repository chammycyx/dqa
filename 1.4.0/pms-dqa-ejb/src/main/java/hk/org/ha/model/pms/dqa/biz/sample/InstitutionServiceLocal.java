package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;

import javax.ejb.Local;

@Local
public interface InstitutionServiceLocal {
	
	void retrieveInstitutionByInstitution(Institution institutionIn);
	
	void addInstitution();
	
	void createInstitution(Institution institutionIn);
	
	void updateInstitution(Institution institutionIn);
	
	void deleteInstitution(Institution institutionIn);
	
	Institution retrieveInstitutionForPhs( String institutionCode );
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();
}
