package hk.org.ha.model.pms.dqa.biz.suppperf;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;
import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspQtyRpt;

import javax.ejb.Local;

@Local
public interface BatchSuspListServiceLocal {

	void retrieveBatchSuspListByBatchSuspCriteria(BatchSuspCriteria batchSuspCriteriaIn);
	
	void retrieveBatchSuspQtyRptByBatchSuspList(List<BatchSusp> batchSuspList);
	
	List<BatchSuspQtyRpt> getBatchSuspQtyRptList();
	
	String getReportName();
	
	String getReportParameters();
	
	String getRetrieveDate();
	
    void destroy();
}
