package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.sample.Letter;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleLetter;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.vo.sample.SampleTestEmailEnquiryCriteria;


import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleTestScheduleLetterListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleLetterListServiceBean implements SampleTestScheduleLetterListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<SampleTestScheduleLetter> sampleTestScheduleLetterList;

	
	
	@SuppressWarnings("unchecked")
	public void retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria(SampleTestEmailEnquiryCriteria sampleTestEmailEnquiryCriteria){
		logger.debug("retrieveSampleTestScheduleLetterListBySampleTestEmailEnquiryCriteria" );
		
			
		StringBuffer emailEnquirySql = new StringBuffer();
		emailEnquirySql.append("select o from SampleTestScheduleLetter o "); 
		emailEnquirySql.append("where (o.sampleTestSchedule.scheduleStatus = :scheduleStatus or :scheduleStatusValue is null) ");
		emailEnquirySql.append("and (o.sampleTestSchedule.scheduleNum = :scheduleNum or :scheduleNum is null) ");
		emailEnquirySql.append("and (o.sampleTestSchedule.sampleItem.itemCode = :itemCode or :itemCode is null) ");
		emailEnquirySql.append("and (o.letter.letterCode = :letterCode or :letterCode is null) ");
		emailEnquirySql.append("and (o.letter.sendAtSchedStatus = :sendAtScheduleStatus or :sendAtScheduleStatusValue is null) ");
		emailEnquirySql.append("and (o.letter.sendDate >= :fromSendDate or :fromSendDate is null) ");
		emailEnquirySql.append("and (o.letter.sendDate < :toSendDate or :toSendDate is null) ");
			
			
		if(sampleTestEmailEnquiryCriteria.getSupplierCode()!=null){
			emailEnquirySql.append("and o.sampleTestSchedule.contract.supplier.supplierCode = :supplierCode ");}
		if(sampleTestEmailEnquiryCriteria.getManufCode() != null){
			emailEnquirySql.append("and o.sampleTestSchedule.contract.manufacturer.companyCode = :manufCode ");}
		if(sampleTestEmailEnquiryCriteria.getPharmCompanyCode() !=null){
			emailEnquirySql.append("and o.sampleTestSchedule.contract.pharmCompany.companyCode = :pharmCompanyCode ");}
		if(sampleTestEmailEnquiryCriteria.getInstitutionCode() != null){
			emailEnquirySql.append("and o.sampleTestSchedule.institution.institutionCode = :institutionCode ");}
		if(sampleTestEmailEnquiryCriteria.getLabCode() != null){
			emailEnquirySql.append("and o.sampleTestSchedule.lab.labCode = :labCode ");}
			
			
		Query q = em.createQuery(emailEnquirySql.toString())
					.setParameter("scheduleStatus", sampleTestEmailEnquiryCriteria.getScheduleStatus())
					.setParameter("scheduleStatusValue", sampleTestEmailEnquiryCriteria.getScheduleStatus()==null?null:sampleTestEmailEnquiryCriteria.getScheduleStatus().getDataValue())
					.setParameter("scheduleNum", sampleTestEmailEnquiryCriteria.getScheduleNum())
					.setParameter("itemCode", sampleTestEmailEnquiryCriteria.getItemCode())
					.setParameter("letterCode", sampleTestEmailEnquiryCriteria.getLetterCode())
					.setParameter("sendAtScheduleStatus", sampleTestEmailEnquiryCriteria.getSendAtScheduleStatus())
					.setParameter("sendAtScheduleStatusValue", sampleTestEmailEnquiryCriteria.getSendAtScheduleStatus()==null?null:sampleTestEmailEnquiryCriteria.getSendAtScheduleStatus().getDataValue())
					.setParameter("fromSendDate", sampleTestEmailEnquiryCriteria.getFromSendDate(), TemporalType.DATE)
					.setParameter("toSendDate", sampleTestEmailEnquiryCriteria.getToSendDate(), TemporalType.DATE);

		if(sampleTestEmailEnquiryCriteria.getSupplierCode()!=null){
			q=q.setParameter("supplierCode", sampleTestEmailEnquiryCriteria.getSupplierCode());}
		if(sampleTestEmailEnquiryCriteria.getManufCode() != null){
			q=q.setParameter("manufCode", sampleTestEmailEnquiryCriteria.getManufCode());}
		if(sampleTestEmailEnquiryCriteria.getPharmCompanyCode() !=null){
			q=q.setParameter("pharmCompanyCode", sampleTestEmailEnquiryCriteria.getPharmCompanyCode());}
		if(sampleTestEmailEnquiryCriteria.getLabCode() != null){
			q=q.setParameter("labCode", sampleTestEmailEnquiryCriteria.getLabCode());}
		if(sampleTestEmailEnquiryCriteria.getInstitutionCode() != null){
			q=q.setParameter("institutionCode", sampleTestEmailEnquiryCriteria.getInstitutionCode());}
									
		q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule")
		   .setHint(QueryHints.FETCH, "o.letter")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleItem.riskLevel")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.sampleTest")
		   .setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract");
	    /*if(sampleTestEmailEnquiryCriteria.getSupplierCode()!=null){
		   q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.supplier");}
		if(sampleTestEmailEnquiryCriteria.getManufCode() != null){
		   q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.manufacturer");}
		if(sampleTestEmailEnquiryCriteria.getPharmCompanyCode() !=null){
		   q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.contract.pharmCompany");}*/
		if(sampleTestEmailEnquiryCriteria.getInstitutionCode() != null){
		   q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.institution");}
		if(sampleTestEmailEnquiryCriteria.getLabCode() != null){
		   q=q.setHint(QueryHints.FETCH, "o.sampleTestSchedule.lab");}
		
									   
		sampleTestScheduleLetterList = q.getResultList();
				
		if(sampleTestScheduleLetterList == null || sampleTestScheduleLetterList.size()==0){
		   sampleTestScheduleLetterList = null;}
		else
		{
			for(SampleTestScheduleLetter stsl:sampleTestScheduleLetterList)
			{
				if(stsl.getSampleTestSchedule()!=null)
				{
					getLazySampleTestSchedule(stsl.getSampleTestSchedule());
				}
			}
		}
			
	}
	
	public void createSampleTestScheduleLetter(LetterTemplate letterTemplateIn, String refNumIn, List<SampleTestSchedule> sampleTestScheduleListIn, SampleTestSchedule sampleTestScheduleIn){
		logger.debug("createSampleTestScheduleLetter" );
		
		SampleTestScheduleLetter sampleTestScheduleLetterInsert = null;
		Letter letterInsert = null;
		
		String to = "";
		to = StringUtils.join(letterTemplateIn.getToList(),";");
		if(!to.trim().equals(""))
		{
			to=to +";";
		}
		
		String cc = "";
		cc = StringUtils.join(letterTemplateIn.getCcList(),";");
		if(!cc.trim().equals(""))
		{
			cc=cc +";";
		}
		
		if(sampleTestScheduleListIn!=null && sampleTestScheduleListIn.size()>0)
		{
			for(SampleTestSchedule sts:sampleTestScheduleListIn)
			{
				sampleTestScheduleLetterInsert = new SampleTestScheduleLetter();
				letterInsert = new Letter();
				letterInsert.setSubject(letterTemplateIn.getSubject());
				letterInsert.setSendDate(new Date());
				letterInsert.setSendAtSchedStatus(sts.getScheduleStatus());
				letterInsert.setRefNum(refNumIn);
				letterInsert.setLetterType("E");
				letterInsert.setLetterTo(to);
				//letterInsert.setLetterFrom(letterTemplateIn.get);
				letterInsert.setLetterCode(letterTemplateIn.getCode());
				letterInsert.setLetterCc(cc);
				letterInsert.setContent(letterTemplateIn.getContent());
				em.persist(letterInsert);
				em.flush();
				
				sampleTestScheduleLetterInsert.setLetter(letterInsert);
				sampleTestScheduleLetterInsert.setSampleTestSchedule(sts);
				em.persist(sampleTestScheduleLetterInsert);
				em.flush();
			}
		}
		else if(sampleTestScheduleIn!=null)
		{
			sampleTestScheduleLetterInsert = new SampleTestScheduleLetter();
			letterInsert = new Letter();
			
			letterInsert.setSubject(letterTemplateIn.getSubject());
			letterInsert.setSendDate(new Date());
			letterInsert.setSendAtSchedStatus(sampleTestScheduleIn.getScheduleStatus());
			if(sampleTestScheduleIn.getScheduleStatus().equals(ScheduleStatus.Schedule))
			{
				letterInsert.setRefNum(null);
			}
			else
			{
				letterInsert.setRefNum(refNumIn);
			}
			
			if(letterTemplateIn.getSampleTestFileList()!=null)
			{
				for(SampleTestFile stf:letterTemplateIn.getSampleTestFileList())
				{
					if(stf.isSelected())
					{
						if(letterInsert.getAttFileNames()==null)
						{
							letterInsert.setAttFileNames(stf.getFileItem().getFileName());
						}
						else
						{
							letterInsert.setAttFileNames(letterInsert.getAttFileNames() + "; " +stf.getFileItem().getFileName());
						}
					}
				}
			}
			
			letterInsert.setLetterType("E");
			letterInsert.setLetterTo(to);
			//letterInsert.setLetterFrom(letterTemplateIn.get);
			letterInsert.setLetterCode(letterTemplateIn.getCode());
			letterInsert.setLetterCc(cc);
			letterInsert.setContent(letterTemplateIn.getContent());
			em.persist(letterInsert);
			em.flush();
			
			sampleTestScheduleLetterInsert.setLetter(letterInsert);
			sampleTestScheduleLetterInsert.setSampleTestSchedule(sampleTestScheduleIn);
			em.persist(sampleTestScheduleLetterInsert);
			em.flush();
			
		}
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	

	@Remove
	public void destroy() {
		if (sampleTestScheduleLetterList !=null){
			sampleTestScheduleLetterList = null;
		}
	}
}
