package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemBy;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "PHARM_PROBLEM")
@NamedQueries({
	@NamedQuery(name = "PharmProblem.findByPharmProblemId", query = "select o from PharmProblem o where o.pharmProblemId = :pharmProblemId and o.recordStatus=:recordStatus "),
	@NamedQuery(name = "PharmProblem.findByProblemDateAndInstitution", query = "select o from PharmProblem o where o.problemDate >= :startDate and o.problemDate <= :endDate and o.recordStatus = hk.org.ha.model.pms.dqa.udt.RecordStatus.Active and o.institution.institutionCode = :institutionCode order by o.problemDate"),
	@NamedQuery(name = "PharmProblem.findByProblemDate", query = "select o from PharmProblem o where o.problemDate >= :startDate and o.problemDate <= :endDate and o.recordStatus = hk.org.ha.model.pms.dqa.udt.RecordStatus.Active order by o.problemDate"),
	@NamedQuery(name = "PharmProblem.findByNullProblemStatus", query = "select o from PharmProblem o where o.recordStatus=:recordStatus and o.problemStatus is null order by o.createDate desc ")
	
})
@Customizer(AuditCustomizer.class)
public class PharmProblem extends VersionEntity {

	private static final long serialVersionUID = -3683227013454881779L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pharmProblemSeq")
	@SequenceGenerator(name = "pharmProblemSeq", sequenceName = "SEQ_PHARM_PROBLEM", initialValue=10000)
	@Id
	@Column(name="PHARM_PROBLEM_ID", nullable=false)
	private Long pharmProblemId;

	@Column(name="PROBLEM_NUM", length=11, nullable=false)
	private String problemNum;
	
	@Converter(name = "PharmProblem.problemStatus", converterClass = ProblemStatus.Converter.class )
	@Convert("PharmProblem.problemStatus")
	@Column(name="PROBLEM_STATUS", length=1)
	private ProblemStatus problemStatus;

	@Column(name="PROBLEM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date problemDate;
	
	@Column(name="AFFECT_QTY")
    private Integer affectQty;
	
	@Column(name="DELETE_REASON", length=255)
	private String deleteReason;
	
	@Column(name="PROBLEM_BY_NAME", length=50)
	private String problemByName;
	
	@Column(name="PROBLEM_BY_RANK", length=30)
	private String problemByRank;
	
	@Converter(name = "PharmProblem.problemByType", converterClass = ProblemBy.Converter.class )
	@Convert("PharmProblem.problemByType")
	@Column(name="PROBLEM_BY_TYPE", length=1)
	private ProblemBy problemByType;
	
	@Column(name="PROBLEM_BY_TYPE_DESC", length=30)
	private String problemByTypeDesc;
	
	@Column(name="COORDINATOR_NAME", length=50)
	private String coordinatorName;
	
	@Converter(name = "PharmProblem.coordinatorRank", converterClass = Rank.Converter.class )
	@Convert("PharmProblem.coordinatorRank")
	@Column(name="COORDINATOR_RANK", length=30)
	private Rank coordinatorRank;
	
	@Column(name="COORDINATOR_PHONE", length=15)
	private String coordinatorPhone;
	
	@Column(name="PROBLEM_DETAIL", length=500)
	private String problemDetail;

	@Column(name = "CONFLICT_FLAG", nullable = false)
    private Boolean conflictFlag;
	
	@Column(name="CONFLICT_MSP_NAME", length=150)
	private String conflictMspName;
	
	@Column(name="CONFLICT_RELATIONSHIP", length=150)
	private String conflictRelationship;
	
	@Column(name="CONFLICT_BY_NAME", length=150)
	private String conflictByName;
	
	@Column(name="CONFLICT_BY_RANK", length=50)
	private String conflictByRank;
	
	@Column(name="CONFLICT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date conflictDate;
	
	@Converter(name = "Classification.classificationCode", converterClass = ProblemClassification.Converter.class )
	@Convert("Classification.classificationCode")
	@Column(name="CLASSIFICATION_CODE", length=3)
	private ProblemClassification classificationCode;
	
	@Converter(name = "PharmProblem.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("PharmProblem.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@OneToMany(mappedBy="pharmProblem")
    private List<PharmProblemCountry> pharmProblemCountryList;
	
	@OneToMany(mappedBy="pharmProblem")
    private List<PharmBatchNum> pharmBatchNumList;
	
	@OneToMany(mappedBy="pharmProblem")
    private List<PharmProblemNature> pharmProblemNatureList;
	
	@OneToMany(mappedBy="pharmProblem")
    private List<PharmProblemFile> pharmProblemFileList;
	
	@OneToOne
	@JoinColumn(name="CONTRACT_ID")
	private Contract contract;
	
	@OneToOne
	@JoinColumn(name="PROBLEM_HEADER_ID")
	private ProblemHeader problemHeader;
	
	@OneToOne
	@JoinColumn(name="SEND_COLLECT_SAMPLE_ID")
	private SendCollectSample sendCollectSample;
	
	@ManyToOne
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;

	@ManyToOne
	@JoinColumn(name="RPT_INSTITUTION_CODE")
	private Institution rptInstitution;
	
	@ManyToOne
	@JoinColumn(name="QA_PROBLEM_ID")
	private QaProblem qaProblem;
	
	@Transient
	private boolean selected;

	
	public Long getPharmProblemId() {
		return pharmProblemId;
	}

	public void setPharmProblemId(Long pharmProblemId) {
		this.pharmProblemId = pharmProblemId;
	}

	public String getProblemNum() {
		return problemNum;
	}

	public void setProblemNum(String problemNum) {
		this.problemNum = problemNum;
	}

	public ProblemStatus getProblemStatus() {
		return problemStatus;
	}

	public void setProblemStatus(ProblemStatus problemStatus) {
		this.problemStatus = problemStatus;
	}

	public Date getProblemDate() {
		return (problemDate!=null)?new Date(problemDate.getTime()):null;
	}

	public void setProblemDate(Date problemDate) {
		if (problemDate!=null) {
			this.problemDate = new Date(problemDate.getTime());
		} else {
			this.problemDate = null;
		}
	}

	public Integer getAffectQty() {
		return affectQty;
	}

	public void setAffectQty(Integer affectQty) {
		this.affectQty = affectQty;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public String getProblemByName() {
		return problemByName;
	}

	public void setProblemByName(String problemByName) {
		this.problemByName = problemByName;
	}

	public String getProblemByRank() {
		return problemByRank;
	}

	public void setProblemByRank(String problemByRank) {
		this.problemByRank = problemByRank;
	}

	public ProblemBy getProblemByType() {
		return problemByType;
	}

	public void setProblemByType(ProblemBy problemByType) {
		this.problemByType = problemByType;
	}

	public String getProblemByTypeDesc() {
		return problemByTypeDesc;
	}

	public void setProblemByTypeDesc(String problemByTypeDesc) {
		this.problemByTypeDesc = problemByTypeDesc;
	}

	public String getCoordinatorName() {
		return coordinatorName;
	}

	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}

	public Rank getCoordinatorRank() {
		return coordinatorRank;
	}

	public void setCoordinatorRank(Rank coordinatorRank) {
		this.coordinatorRank = coordinatorRank;
	}

	public String getCoordinatorPhone() {
		return coordinatorPhone;
	}

	public void setCoordinatorPhone(String coordinatorPhone) {
		this.coordinatorPhone = coordinatorPhone;
	}

	public String getProblemDetail() {
		return problemDetail;
	}

	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}

	public void setConflictFlag(Boolean conflictFlag) {
		this.conflictFlag = conflictFlag;
	}

	public Boolean getConflictFlag() {
		return conflictFlag;
	}

	public void setConflictMspName(String conflictMspName) {
		this.conflictMspName = conflictMspName;
	}

	public String getConflictMspName() {
		return conflictMspName;
	}

	public void setConflictRelationship(String conflictRelationship) {
		this.conflictRelationship = conflictRelationship;
	}

	public String getConflictRelationship() {
		return conflictRelationship;
	}

	public void setConflictByName(String conflictByName) {
		this.conflictByName = conflictByName;
	}

	public String getConflictByName() {
		return conflictByName;
	}

	public void setConflictByRank(String conflictByRank) {
		this.conflictByRank = conflictByRank;
	}

	public String getConflictByRank() {
		return conflictByRank;
	}

	public void setConflictDate(Date conflictDate) {
		this.conflictDate = conflictDate;
	}

	public Date getConflictDate() {
		return conflictDate;
	}

	public void setClassificationCode(ProblemClassification classificationCode) {
		this.classificationCode = classificationCode;
	}

	public ProblemClassification getClassificationCode() {
		return classificationCode;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public List<PharmProblemCountry> getPharmProblemCountryList() {
		return pharmProblemCountryList;
	}

	public void setPharmProblemCountryList(
			List<PharmProblemCountry> pharmProblemCountryList) {
		this.pharmProblemCountryList = pharmProblemCountryList;
	}
	
	public List<PharmBatchNum> getPharmBatchNumList() {
		return pharmBatchNumList;
	}

	public void setPharmBatchNumList(List<PharmBatchNum> pharmBatchNumList) {
		this.pharmBatchNumList = pharmBatchNumList;
	}
	
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	public ProblemHeader getProblemHeader() {
		return problemHeader;
	}

	public void setProblemHeader(ProblemHeader problemHeader) {
		this.problemHeader = problemHeader;
	}

	public SendCollectSample getSendCollectSample() {
		return sendCollectSample;
	}

	public void setSendCollectSample(SendCollectSample sendCollectSample) {
		this.sendCollectSample = sendCollectSample;
	}
	
	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	
	public void setRptInstitution(Institution rptInstitution) {
		this.rptInstitution = rptInstitution;
	}

	public Institution getRptInstitution() {
		return rptInstitution;
	}

	public QaProblem getQaProblem() {
		return qaProblem;
	}

	public void setQaProblem(QaProblem qaProblem) {
		this.qaProblem = qaProblem;
	}
	
	public List<PharmProblemNature> getPharmProblemNatureList() {
		return pharmProblemNatureList;
	}

	public void setPharmProblemNatureList(List<PharmProblemNature> pharmProblemNatureList) {
		this.pharmProblemNatureList = pharmProblemNatureList;
	}
	
	public List<PharmProblemFile> getPharmProblemFileList() {
		return pharmProblemFileList;
	}

	public void setPharmProblemFileList(
			List<PharmProblemFile> pharmProblemFileList) {
		this.pharmProblemFileList = pharmProblemFileList;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}
}
 