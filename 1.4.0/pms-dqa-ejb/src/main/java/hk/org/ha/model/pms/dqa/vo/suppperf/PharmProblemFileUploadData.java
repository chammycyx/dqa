package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PharmProblemFileUploadData {
	
	private byte[] fileData;
	private FileItem fileItem;
	private String refUploadFileName;
	private String institutionCode;
	private String itemCode;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	
	
	public byte[] getFileData() {
		return (fileData!=null)?(fileData.clone()):(new byte[0]);
	}
	public void setFileData(byte[] fileData) {
		if(fileData!=null)
		{
			this.fileData = fileData.clone();
		}
	}
	public FileItem getFileItem() {
		return fileItem;
	}
	public void setFileItem(FileItem fileItem) {
		this.fileItem = fileItem;
	}
	public String getRefUploadFileName() {
		return refUploadFileName;
	}
	public void setRefUploadFileName(String refUploadFileName) {
		this.refUploadFileName = refUploadFileName;
	}
	public String getInstitutionCode() {
		return institutionCode;
	}
	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getManufCode() {
		return manufCode;
	}
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}
	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	
}
