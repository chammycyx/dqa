package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ValidTestQtyReq  implements StringValuedEnum {
	
	Is10g("VR1", "10g"),
	Is20g("VR2", "20g"),
	Is20to30g("VR3", "20-30g"),
	Other("O", "Other");
	
	private final String dataValue;
    private final String displayValue;
	
	ValidTestQtyReq(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
	
	@Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }  
    
    public static class Converter extends StringValuedEnumConverter<ValidTestQtyReq> {

		private static final long serialVersionUID = 3601829209059683642L;

		@Override
    	public Class<ValidTestQtyReq> getEnumClass() {
    		return ValidTestQtyReq.class;
    	}
    }
}
