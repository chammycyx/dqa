package hk.org.ha.model.pms.dqa.biz.sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.biz.ApplicationProp;
import hk.org.ha.model.pms.dqa.biz.FuncSeqNumServiceLocal;
import hk.org.ha.model.pms.dqa.biz.LetterTemplateManagerLocal;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.sample.ReleaseRetentionSampleMemo;
import hk.org.ha.model.pms.vo.letter.LetterContent;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.log.Log;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

@Stateful
@Scope(ScopeType.SESSION)
@Name("releaseRetentionSampleRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ReleaseRetentionSampleRptServiceBean implements ReleaseRetentionSampleRptServiceLocal {

	@Logger
	private Log logger;
	
	@In
	private LetterTemplateManagerLocal letterTemplateManager;

	@In
	private ApplicationProp applicationProp;
	
	private static final String JAXB_CONTEXT_LETTER = "hk.org.ha.model.pms.vo.letter";
	
	private JaxbWrapper<LetterContent> rxJaxbWrapper;
	
	private static final String SPACE = "          ";
	
	private JRDataSource dataSource;
	
	@In
	private Interpolator interpolator;

	@In
	private ReportProvider<JRDataSource> reportProvider;
	
	@In(create =true)
	private FuncSeqNumServiceLocal funcSeqNumService;
	
	private void initJaxbWrapper() {
		if (rxJaxbWrapper == null) {
			rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_LETTER);
		}
	}
	
	private DateTime plusDaysExcludeHoliday(DateTime startDate, int days) {
		int count = 0;
		DateTime dateTime = startDate;
		
		while (count < days) {
			dateTime = dateTime.plusDays(1);
			if (dateTime.getDayOfWeek() == DateTimeConstants.SATURDAY) {
				dateTime = dateTime.plusDays(2);
			} else if (dateTime.getDayOfWeek() == DateTimeConstants.SUNDAY){
				dateTime = dateTime.plusDays(1);
			}
			count++;
		}
		
		return dateTime;
	}
	
	public ReleaseRetentionSampleMemo retrieveReleaseRetentionSampleMemo(UserInfo defaultCpoContact, UserInfo cpoSignature, SampleTestSchedule sampleTestScheduleIn) {
		
		initJaxbWrapper();
	 	
		LetterTemplate lt = letterTemplateManager.retrieveLetterTemplateByCode("RORS");
	 	LetterContent lc = rxJaxbWrapper.unmarshall(lt.getContent());	 		 	
	 	
	 	ReleaseRetentionSampleMemo releaseRetentionSampleMemo = new ReleaseRetentionSampleMemo();
	 	
	 	releaseRetentionSampleMemo.setDefaultCpoContact(defaultCpoContact);
	 	releaseRetentionSampleMemo.setCpoSignature(cpoSignature);
	 	
	 	releaseRetentionSampleMemo.setFrom(lc.getSenderPosition());
	 	releaseRetentionSampleMemo.setRefNum(funcSeqNumService.getNextFuncSeqNumString("RS","00000000"));
	 	releaseRetentionSampleMemo.setRefNumDesc(
	 			interpolator.interpolate(lc.getRefNum(),
	 					new Object[] {releaseRetentionSampleMemo.getRefNum()}
	 			)
	 	);
	 	
	 	releaseRetentionSampleMemo.setTo(
	 			interpolator.interpolate(lc.getReceiverPosition(), new Object[]{
								sampleTestScheduleIn.getInstitution().getInstitutionCode()
							}
				)		
	 	);
	 	
	 	releaseRetentionSampleMemo.setTelephone(cpoSignature.getContact().getOfficePhone());
	 	releaseRetentionSampleMemo.setFax(cpoSignature.getContact().getFax());
	 	
		releaseRetentionSampleMemo.setSendDate(plusDaysExcludeHoliday(
				new DateTime(), applicationProp.getReleaseRetentionSampleSendDateBuffer()).toDate());
		
		Contact instContact = sampleTestScheduleIn.getInstitution().getContact();
		releaseRetentionSampleMemo.setAttn(instContact.getTitle()+" "+instContact.getFirstName()+" "+instContact.getLastName()+" "+instContact.getPosition());
		releaseRetentionSampleMemo.setAttnDesc(
				interpolator.interpolate(lc.getReceiverAttention(), 
				new Object[] {releaseRetentionSampleMemo.getAttn()})
		);
		
		releaseRetentionSampleMemo.setMemoTitle(lc.getSubjectInfo());
		
		releaseRetentionSampleMemo.setItemDesc(sampleTestScheduleIn.getSampleItem().getDmDrug().getFullDrugDesc() + " (" + sampleTestScheduleIn.getSampleItem().getItemCode() + ")");
		releaseRetentionSampleMemo.setManufacturer("Manufacturer: "+sampleTestScheduleIn.getContract().getManufacturer().getCompanyName());
		releaseRetentionSampleMemo.setSupplier("Supplier: "+sampleTestScheduleIn.getContract().getSupplier().getSupplierName());
		
		if (lc.getParagraphList().size() > 0) {
			releaseRetentionSampleMemo.setParagraph1(lc.getParagraphList().get(0));
		}
		if (lc.getParagraphList().size() > 1) {
			int userInputRetentionQty = sampleTestScheduleIn.getChemicalAnalysis()!=null?sampleTestScheduleIn.getChemicalAnalysis().getRetentionQty().intValue():0;
			releaseRetentionSampleMemo.setParagraph2(					
					interpolator.interpolate(
							lc.getParagraphList().get(1), new Object[]{
								userInputRetentionQty,
								sampleTestScheduleIn.getSampleItem().getDmDrug().getBaseUnit(),
								sampleTestScheduleIn.getBatchNum()
							}
					)
			);
		}
		
		if (lc.getParagraphList().size() > 2) {
			releaseRetentionSampleMemo.setParagraph3(
					interpolator.interpolate(
							lc.getParagraphList().get(2), new Object[]{
								defaultCpoContact.getContact().getTitle(),
								defaultCpoContact.getContact().getFirstName(),
								defaultCpoContact.getContact().getLastName(),
								defaultCpoContact.getContact().getOfficePhone()
							}
					)
			);
		}
		
		releaseRetentionSampleMemo.setSignature(cpoSignature.getContact().getTitle()+" "+cpoSignature.getContact().getFirstName()+" "+cpoSignature.getContact().getLastName());
		
		releaseRetentionSampleMemo.setHeadOfficeAddress(lc.getHeadOfficeAddress());
		
	 	return releaseRetentionSampleMemo;
	}
	
	public void createReleaseRetentionSampleMemo(ReleaseRetentionSampleMemo releaseRetentionSampleMemo)
	{
		logger.debug("createReleaseRetentionSampleMemo");
		
		List<ReleaseRetentionSampleMemo> releaseRetentionSampleMemoList = new ArrayList<ReleaseRetentionSampleMemo>();	
		releaseRetentionSampleMemoList.add(releaseRetentionSampleMemo);
		
		releaseRetentionSampleMemo.setParagraph2("2."+SPACE.concat(releaseRetentionSampleMemo.getParagraph2()));
		releaseRetentionSampleMemo.setParagraph3("3."+SPACE.concat(releaseRetentionSampleMemo.getParagraph3()));
		
		if (releaseRetentionSampleMemo.getSendDate() != null) {
			releaseRetentionSampleMemo.setMemoDate(new SimpleDateFormat("dd MMM yyyy").format(releaseRetentionSampleMemo.getSendDate()));
		}
		
		dataSource = new JRBeanCollectionDataSource(releaseRetentionSampleMemoList);
	}
	
	public void generateReleaseRetentionSampleMemo()
	{
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/releaseRetentionSampleMemo.jasper", parameters, dataSource); 
		
		reportProvider.redirectReport(contentId);
	}
	
	@Remove
	public void destroy(){
	}
}
