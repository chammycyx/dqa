package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;

import javax.ejb.Local;

@Local
public interface RiskLevelServiceLocal {
	
	void retrieveRiskLevelByRiskLevelCode(String riskLevelCode);
	
	void addRiskLevel();
	
	void createRiskLevel();
	
	void updateRiskLevel();
	
	void deleteRiskLevel(RiskLevel riskLevelIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
