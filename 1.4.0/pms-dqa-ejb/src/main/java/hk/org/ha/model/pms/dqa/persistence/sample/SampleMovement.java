package hk.org.ha.model.pms.dqa.persistence.sample;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "SAMPLE_MOVEMENT")
@NamedQueries({
	@NamedQuery(name = "SampleMovement.findBySampleMovementCriteria", query = "select o from SampleMovement o where " +
			"(o.sampleTestSchedule.institution.institutionCode = :institutionCode or :institutionCode is null) " +
			"and (o.sampleTestSchedule.sampleItem.itemCode = :itemCode or :itemCode is null) " +
			"and (o.sampleTestSchedule.orderType = :orderType or :orderTypeValue is null) " +
			"and (o.sampleTestSchedule.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) " +
			"and (o.manufCode = :manufCode or :manufCode is null) " + 
			"and (o.sampleTestSchedule.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) " +
			"and (o.sampleTestSchedule.lab.labCode = :labCode or :labCode is null) " +
			"and (o.sampleTestSchedule.sampleTest.testCode = :testCode or :testCode is null) " + 
			"and (o.sampleTestSchedule.batchNum = :batchNum or :batchNum is null) " +
			"and (o.sampleTestSchedule.expiryDate = :expiryDate or :expiryDate is null) " +
			"and (o.sampleTestSchedule.scheduleNum = :scheduleNum or :scheduleNum is null) " +
			"and (o.createDate >= :fromInventoryDate ) " + 
			"and (o.createDate < :toInventoryDate ) " +
			"order by o.createDate " ),
			
	@NamedQuery(name = "SampleMovement.findBySampleMovementKeyList", query = "select o from SampleMovement o where o.sampleLogId in :keyList "),
	@NamedQuery(name = "SampleMovement.findBySampleLogId", query = "select o from SampleMovement o where o.sampleLogId = :sampleLogId "),
	@NamedQuery(name = "SampleMovement.findNewestByItemCode", query = "select o from SampleMovement o where o.sampleTestSchedule.sampleItem.itemCode = :itemCode order by o.createDate Desc "),
	@NamedQuery(name = "SampleMovement.findModifyDateByItemCode", query = "select NEW hk.org.ha.model.pms.dqa.vo.sample.DrugItemCodeModifyDate(o.sampleTestSchedule.sampleItem.itemCode, max(o.modifyDate)) from SampleMovement o group by o.sampleTestSchedule.sampleItem.itemCode order by o.sampleTestSchedule.sampleItem.itemCode"),
	@NamedQuery(name = "SampleMovement.findByItemCodeModifyDate", query = "select o from SampleMovement o where o.sampleTestSchedule.sampleItem.itemCode = :itemCode and o.modifyDate = :modifyDate"),
	@NamedQuery(name = "SampleMovement.findByModifyDate", query = "select o from SampleMovement o where o.modifyDate >= :startDate and o.modifyDate < :endDate order by o.sampleTestSchedule.sampleItem.itemCode, o.modifyDate")
})
@Customizer(AuditCustomizer.class)
public class SampleMovement extends VersionEntity {

	private static final long serialVersionUID = 3113388299647132022L;

	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleMovementSeq")
	@SequenceGenerator(name = "sampleMovementSeq", sequenceName = "SEQ_SAMPLE_MOVEMENT", initialValue=10000)
	@Column(name="SAMPLE_LOG_ID")
	private Long sampleLogId;

	@Column(name="LOCATION_CODE", length=4)
	private String locationCode;

	@Column(name="MANUF_CODE", length=4)
	private String manufCode;
	
	@Column(name="MOVEMENT_QTY")
	private Integer movementQty;
	
	@Column(name="ON_HAND_QTY")
	private Integer onHandQty;
	
	@Column(name="ACTION_CODE", length=1)
	private String actionCode;

	@Column(name="ACTION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date actionDate;
	
	@Column(name="ADJUST_FLAG", length = 1)
	private String adjustFlag;
	
	@Column(name="ADJUST_REASON", length = 50)
	private String adjustReason;

	@ManyToOne
	@JoinColumn(name="SCHEDULE_ID")
	private SampleTestSchedule sampleTestSchedule;
	
	public Long getSampleLogId() {
		return sampleLogId;
	}

	public void setSampleLogId(Long sampleLogId) {
		this.sampleLogId = sampleLogId;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public Integer getMovementQty() {
		return movementQty;
	}

	public void setMovementQty(Integer movementQty) {
		this.movementQty = movementQty;
	}

	public void setOnHandQty(Integer onHandQty) {
		this.onHandQty = onHandQty;
	}

	public Integer getOnHandQty() {
		return onHandQty;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public Date getActionDate() {
		return (actionDate != null) ? new Date(actionDate.getTime()) : null;
	}

	public void setActionDate(Date actionDate) {
		if (actionDate != null) {
			this.actionDate = new Date(actionDate.getTime());
		} else {
			this.actionDate = null;
		}
	}

	public void setAdjustFlag(String adjustFlag) {
		this.adjustFlag = adjustFlag;
	}

	public String getAdjustFlag() {
		return adjustFlag;
	}

	public String getAdjustReason() {
		return adjustReason;
	}

	public void setAdjustReason(String adjustReason) {
		this.adjustReason = adjustReason;
	}

	public void setSampleTestSchedule(SampleTestSchedule sampleTestSchedule) {
		this.sampleTestSchedule = sampleTestSchedule;
	}

	public SampleTestSchedule getSampleTestSchedule() {
		return sampleTestSchedule;
	}
	
}
