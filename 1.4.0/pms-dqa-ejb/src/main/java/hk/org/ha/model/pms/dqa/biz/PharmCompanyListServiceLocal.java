package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface PharmCompanyListServiceLocal {
	
	void retrievePharmCompanyList();
	
	void retrievePharmCompanyListLike(String pharmCompanyCode);
	
	void destroy();

}
