package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class BatchSuspQtyRpt {
	private String pcuCode;
	private String inst;
	private String caseNum;
	private String itemCode;
	private String itemDesc;
	private String classification;
	private String batchNum;
	private String qtyOnHand;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	public String getPcuCode() {
		return pcuCode;
	}
	public void setPcuCode(String pcuCode) {
		this.pcuCode = pcuCode;
	}
	public String getInst() {
		return inst;
	}
	public void setInst(String inst) {
		this.inst = inst;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public String getQtyOnHand() {
		return qtyOnHand;
	}
	public void setQtyOnHand(String qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}
	public Date getUpdateDate() {
		return (updateDate!=null)?new Date(updateDate.getTime()):null;
	}
	public void setUpdateDate(Date updateDate) {
		if(updateDate!=null)
		{
			this.updateDate = new Date(updateDate.getTime());
		}
	}
}