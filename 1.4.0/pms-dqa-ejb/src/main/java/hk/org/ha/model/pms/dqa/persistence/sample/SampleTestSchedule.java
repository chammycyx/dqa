package hk.org.ha.model.pms.dqa.persistence.sample;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.CancelSchedReason;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleType;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SAMPLE_TEST_SCHEDULE")
@NamedQueries({
	@NamedQuery(name = "SampleTestSchedule.findByScheduleId", query = "select o from SampleTestSchedule o where o.scheduleId= :scheduleId"),
			
	@NamedQuery(name = "SampleTestSchedule.findByTestResultCriteria", query = "select o from SampleTestSchedule o where o.recordStatus= :recordStatus " +
			"and (o.sampleItem.itemCode = :itemCode or :itemCode is null) " +
			"and (o.sampleTest.testCode = :testCode or :testCode is null) " +
			"and (o.contract.supplier.supplierCode = :supplierCode or :supplierCode is null) " +
			"and (o.contract.manufacturer.companyCode = :manufCode or :manufCode is null) " +
			"and (o.contract.pharmCompany.companyCode = :pharmCompanyCode or :pharmCompanyCode is null) " +
			"and (o.testResult = :testResult or :testResultValue is null ) " +
			"and (o.testResult is not null ) " +
			"and (o.testReportDate >= :fromTestReportDate ) " +
			"and (o.testReportDate < :toTestReportDate ) " ),
			
	 @NamedQuery(name = "SampleTestSchedule.findForCopySchedule", query = "select o from SampleTestSchedule o where o.recordStatus= :recordStatus " +
	 		"and o.scheduleStatus in :scheduleStatusList " +
	 		"and o.scheduleId <> :scheduleId and o.sampleItem.itemCode = :itemCode" ),
	 		
	 @NamedQuery(name = "SampleTestSchedule.findForUpdateLastDocDate", query = "select o from SampleTestSchedule o where o.recordStatus= :recordStatus " +
			"and o.scheduleStatus in :scheduleStatusList " +
			"and o.scheduleId <> :scheduleId and o.sampleItem.itemCode = :itemCode " +
			"and o.contract.supplier.supplierCode = :supplierCode and o.contract.manufacturer.companyCode = :manufCode " +
			"and o.contract.pharmCompany.companyCode = :pharmCode"),
			
	 @NamedQuery(name = "SampleTestSchedule.findForOutstanding" , query = "select o from SampleTestSchedule o where o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus <> :scheduleStatus and o.sampleItem.itemCode = :itemCode and o.sampleTest.testCode = :testCode"),
	 		
	 @NamedQuery(name = "SampleTestSchedule.findByItemCodeTestCodeScheduleMonthRecordStatus" , query = "select o from SampleTestSchedule o where o.recordStatus = :recordStatus " +
	 		"and o.sampleItem.itemCode = :itemCode and o.sampleTest.testCode = :testCode  and o.scheduleMonth = :scheduleMonth"),
	 		
	 @NamedQuery(name = "SampleTestSchedule.findByItemCodeScheduleMonthRecordStatus" , query = "select o from SampleTestSchedule o where o.recordStatus = :recordStatus " +
	 		"and o.sampleItem.itemCode = :itemCode and o.scheduleMonth = :scheduleMonth"),
	 		
	 @NamedQuery(name = "SampleTestSchedule.findForLastDocRequestDate", query = "select max(o.lastDocRequestDate) from SampleTestSchedule o " +
	 				"where o.lastDocRequestDate is not null and o.sampleItem.itemCode = :itemCode and o.contract.supplier.supplierCode = :supplierCode " +
	 				"and o.contract.manufacturer.companyCode = :manufCode and o.contract.pharmCompany.companyCode = :pharmCode " +
	 				"and o.recordStatus = :recordStatus "),
	 @NamedQuery(name = "SampleTestSchedule.findByRecordStatusScheduleStatus", query = "select o from SampleTestSchedule o where o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus = :scheduleStatus"),
	 @NamedQuery(name = "SampleTestSchedule.findForTestFreqMaint", query = "select o from SampleTestSchedule o where o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus <> :scheduleStatus and o.testFrequency.frequencyId = :frequencyId"),
	 	
	 @NamedQuery(name = "SampleTestSchedule.findOutstandingScheduleByItemCodeTestCodeRecordStatus", query = "select o from SampleTestSchedule o where o.sampleItem.itemCode=:itemCode " +
	 		"and o.sampleTest.testCode = :testCode " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus <> :scheduleStatusP " +
	 		"and o.scheduleStatus <> :scheduleStatusC " +
	 		"and (o.actionFlag <> :actionFlag or o.actionFlag is null) "),
	@NamedQuery(name = "SampleTestSchedule.findOutstandingScheduleByTestCodeRecordStatus", query = "select o from SampleTestSchedule o where " +
	 		"o.sampleTest.testCode = :testCode " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus <> :scheduleStatusP " +
	 		"and o.scheduleStatus <> :scheduleStatusC " +
	 		"and (o.actionFlag <> :actionFlag or o.actionFlag is null) "),
	@NamedQuery(name = "SampleTestSchedule.findCompletedIn5YearScheduleByTestCodeRecordStatusScheduleMonth", query = "select o from SampleTestSchedule o where " +
	 		"o.sampleTest.testCode = :testCode " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and (o.scheduleStatus = :scheduleStatusP " +
	 		"or o.scheduleStatus = :scheduleStatusC) " +
	 		"and (o.actionFlag <> :actionFlag or o.actionFlag is null) " +
	 		"and o.scheduleMonth >= :fiveYearScheduleMonth "),
	@NamedQuery(name = "SampleTestSchedule.findByLabTestCollectNumScheduleStatusRecordStatus", query = "select o from SampleTestSchedule o where o.lab.labCode=:labCode " +
	 		"and o.sampleTest.testCode = :testCode " +
	 		"and o.labCollectNum = :labCollectNum " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus = :scheduleStatus "),
	@NamedQuery(name = "SampleTestSchedule.findByLabTestOrderTypeScheduleStatusRecordStatus", query = "select o from SampleTestSchedule o where o.lab.labCode=:labCode " +
	 		"and o.sampleTest.testCode = :testCode " +
	 		"and o.orderType = :orderType " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus = :scheduleStatus "),
	@NamedQuery(name = "SampleTestSchedule.findByInstitutionItemScheduleStatusRecordStatus", query = "select o from SampleTestSchedule o where o.institution.institutionCode=:institutionCode " +
	 		"and o.sampleItem.itemCode = :itemCode " +
	 		"and o.recordStatus = :recordStatus " +
	 		"and o.scheduleStatus = :scheduleStatus "),
	@NamedQuery(name = "SampleTestSchedule.findByContractLineIdModuleTypeRecordStatus", query = "select o from SampleTestSchedule o where o.recordStatus=:recordStatus " +
	 		"and o.contract.erpContractLineId = :erpContractLineId " +
	 		"and o.contract.moduleType = :moduleType "),
	@NamedQuery(name = "SampleTestSchedule.findByItemCodeTestCodeScheduleStatusRecordStatus", query = "select o from SampleTestSchedule o where o.recordStatus=:recordStatus " +
	 		"and (o.scheduleStatus = :scheduleStatusP or o.scheduleStatus = :scheduleStatusC) " +
	 		"and o.sampleTest.testCode = :testCode " +
	 		"and o.sampleItem.itemCode = :itemCode " +
	 		"order by o.scheduleId Desc "),
	@NamedQuery(name = "SampleTestSchedule.findByItemTestActionFlagModifyDate", query = "select o from SampleTestSchedule o where o.recordStatus=:recordStatus " +
	 		"and o.scheduleStatus = :scheduleStatus " +
	 		"and o.sampleTest.testCode = :testCode " +
	 		"and o.sampleItem.itemCode = :itemCode " +
	 		"and o.actionFlag = :actionFlag " +
	 		"and o.modifyDate >= :modifyDate "),
	@NamedQuery(name = "SampleTestSchedule.updateSampleTestScheduleResetActionFlagByModifyDate", query = "update SampleTestSchedule o " +
			"set o.actionFlag=null " +
			"where o.actionFlag = :actionFlag " +
			"and o.recordStatus = :recordStatus " +
			"and o.scheduleStatus = :scheduleStatus " +
			"and o.modifyDate >= :modifyDate "),
			
			@NamedQuery(name = "SampleTestSchedule.findByScheduleIdList", query = "select o from SampleTestSchedule o where o.scheduleId in :keyList")
})		
@Customizer(AuditCustomizer.class)
public class SampleTestSchedule extends VersionEntity {	

	private static final long serialVersionUID = 3489058727187918333L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleTestScheduleSeq")
	@SequenceGenerator(name = "sampleTestScheduleSeq", sequenceName = "SEQ_SAMPLE_TEST_SCHED", initialValue=10000)
	@Id
	@Column(name="SCHEDULE_ID", nullable = false)
	private Long scheduleId;

	@Column(name="SCHEDULE_NUM",length = 15, nullable = true)
	private String scheduleNum;

	@Converter(name="SampleTestSchedule.scheduleStatus", converterClass = ScheduleStatus.Converter.class )
	@Convert("SampleTestSchedule.scheduleStatus")
	@Column(name="SCHEDULE_STATUS", length = 2, nullable = false)
	private ScheduleStatus scheduleStatus;
	
	@Converter(name="SampleTestSchedule.scheduleType", converterClass = ScheduleType.Converter.class )
	@Convert("SampleTestSchedule.scheduleType")
	@Column(name="SCHEDULE_TYPE", length = 1, nullable = false)
	private ScheduleType scheduleType;

	@Column(name="SCHEDULE_CONFIRM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduleConfirmDate;

	@Column(name="SCHEDULE_START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduleStartDate;

	@Converter(name = "SampleTestSchedule.orderType", converterClass = OrderType.Converter.class)
	@Convert("SampleTestSchedule.orderType")
	@Column(name="ORDER_TYPE", length = 1, nullable = false)
	private OrderType orderType;
	
	@Column(name="BATCH_NUM", length=20)
	private String batchNum;

	@Column(name="EXPIRY_DATE", length=6)
	private String expiryDate;
	
	@Column(name="REQ_QTY")
	private Integer reqQty;

	@Column(name="REC_QTY")
	private Integer recQty;
	
	@Column(name="RECEIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date receiveDate;

	@Column(name="LAB_COLLECT_NUM", length = 15)
	private String labCollectNum;

	@Column(name="LAB_COLLECT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date labCollectDate;
	
	@Column(name="LAB_COLLECT_QTY")
	private Integer labCollectQty;


	@Column(name="TEST_RESULT", length = 3)
	private String testResult;

	@Column(name="TEST_RESULT_DESC", length = 80)
	private String testResultDesc;

	@Column(name="TEST_REPORT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date testReportDate;
	
	@Column(name="TEST_PRICE")
    private Double testPrice;
	
	@Column(name="REF_DRUG_COST")
	private Double refDrugCost;
	
	@Column(name="UNIT_PRICE")
	private Double unitPrice;
	
	@Converter(name = "SampleTestSchedule.cancelReason", converterClass = CancelSchedReason.Converter.class)
	@Convert("SampleTestSchedule.cancelReason")
	@Column(name="CANCEL_SCHED_REASON", length = 3)
	private CancelSchedReason cancelSchedReason;
	
	@Column(name="NO_NEXT_SCHED_REASON", length=40)
	private String noNextSchedReason;
	
	@Column(name="LAST_DOC_REQUEST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastDocRequestDate;

	@Converter(name="SampleTestSchedule.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("SampleTestSchedule.recordStatus")
	@Column(name="RECORD_STATUS", nullable = false, length = 1)
	private RecordStatus recordStatus;
	
	@Column(name="LAST_SCHEDULE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastSchedule;
	
	@Column(name="ACTION_FLAG", length = 1)
	private String actionFlag;
	
	@Transient
	private boolean toCopy;
	
	@Transient
	private Integer totalQty;
	
	@Transient
	private boolean selected;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PAYMENT_ID")
	private Payment payment;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FREQUENCY_ID")
	private TestFrequency testFrequency;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAB_CODE")
	private Lab lab;
	
	@OneToOne
	@JoinColumn(name="CONTRACT_ID")
	private Contract contract;
	
	@ManyToOne
	@JoinColumn(name="SAMPLE_ITEM_ID")
	private SampleItem sampleItem;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;
	
	@Column(name="SCHEDULE_MONTH")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduleMonth;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MICRO_BIO_TEST_ID", nullable = true)
	private MicroBioTest microBioTest;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CHEMICAL_ANALYSIS_ID", nullable = true)
	private ChemicalAnalysis chemicalAnalysis;

	@ManyToOne
	@JoinColumn(name="TEST_CODE", nullable = false )
	private SampleTest sampleTest;
	
	public String buildActualQtyForFax() {
		String requiredQty;
		int userInputRequiredQty = getReqQty()==null?0:getReqQty().intValue();
		int userInputRetentionQty = getChemicalAnalysis() == null || getChemicalAnalysis().getRetentionQty()==null?0:getChemicalAnalysis().getRetentionQty().intValue();
		int qty = (int) Math.ceil((userInputRequiredQty + userInputRetentionQty)/Double.valueOf(getContract().getPackSize()).doubleValue());
		
		int systemCalculatedQty = qty * getContract().getPackSize().intValue();
		int actualQty = userInputRequiredQty + userInputRetentionQty;
		
		if (actualQty != systemCalculatedQty) {
			requiredQty = String.valueOf(actualQty) + " ";
		} else {
			requiredQty = qty + " X " + getContract().getPackSize() + " ";
		}
		requiredQty += (getSampleItem().getDmDrug()==null?"":getSampleItem().getDmDrug().getBaseUnit());
		return requiredQty;
	}
	
	public String buildActualRetentionQtyForFax() {
		String requiredQty;
		int userInputRetentionQty = getChemicalAnalysis() == null || getChemicalAnalysis().getRetentionQty()==null?0:getChemicalAnalysis().getRetentionQty().intValue();
		int qty = (int) Math.ceil(userInputRetentionQty/Double.valueOf(getContract().getPackSize()).doubleValue());
		int systemCalculatedQty = qty * getContract().getPackSize().intValue();
		if (userInputRetentionQty != systemCalculatedQty) {
			requiredQty = String.valueOf(userInputRetentionQty) + " ";
		} else {
			requiredQty = qty + " X " + getContract().getPackSize() + " ";
		}
		requiredQty += (getSampleItem().getDmDrug()==null?"":getSampleItem().getDmDrug().getBaseUnit());
		return requiredQty;
	}
	
	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}

	public String getScheduleNum() {
		return scheduleNum;
	}

	public void setScheduleStatus(ScheduleStatus scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	public ScheduleStatus getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}

	public ScheduleType getScheduleType() {
		return scheduleType;
	}

	public void setScheduleConfirmDate(Date scheduleConfirmDate) {		
		if (scheduleConfirmDate != null) {
			this.scheduleConfirmDate = new Date(scheduleConfirmDate.getTime());
		} else {
			this.scheduleConfirmDate = null;
		}
	}

	public Date getScheduleConfirmDate() {
		return (scheduleConfirmDate != null) ? new Date(scheduleConfirmDate.getTime()) : null;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getBatchNum() {
		return batchNum;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setReqQty(Integer reqQty) {
		this.reqQty = reqQty;
	}

	public Integer getReqQty() {
		return reqQty;
	}

	public Integer getRecQty() {
		return recQty;
	}

	public void setRecQty(Integer recQty) {
		this.recQty = recQty;
	}

	public void setReceiveDate(Date receiveDate) {
		if (receiveDate != null) {
			this.receiveDate = new Date(receiveDate.getTime());
		}
		else{
			this.receiveDate = null;
		}
	}

	public Date getReceiveDate() {
		return (receiveDate != null) ? new Date(receiveDate.getTime()) : null;
	}

	public void setLabCollectNum(String labCollectNum) {
		this.labCollectNum = labCollectNum;
	}

	public String getLabCollectNum() {
		return labCollectNum;
	}

	public void setLabCollectDate(Date labCollectDate) {
		if (labCollectDate != null) {
			this.labCollectDate = new Date(labCollectDate.getTime());
		}
		else
		{
			this.labCollectDate = null;
		}
	}

	public Date getLabCollectDate() {
		return (labCollectDate != null) ? new Date(labCollectDate.getTime()) : null;
	}
	
	public Integer getLabCollectQty() {
		return labCollectQty;
	}

	public void setLabCollectQty(Integer labCollectQty) {
		this.labCollectQty = labCollectQty;
	}

	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}

	public String getTestResult() {
		return testResult;
	}

	public void setTestResultDesc(String testResultDesc) {
		this.testResultDesc = testResultDesc;
	}

	public String getTestResultDesc() {
		return testResultDesc;
	}

	public void setTestReportDate(Date testReportDate) {
		if (testReportDate != null) {
			this.testReportDate = new Date(testReportDate.getTime());
		} else {
			this.testReportDate = null;
		}
	}

	public Date getTestReportDate() {
		return (testReportDate != null) ? new Date(testReportDate.getTime()) : null;
	}

	public Double getTestPrice() {
		return testPrice;
	}

	public void setTestPrice(Double testPrice) {
		this.testPrice = testPrice;
	}

	public Double getRefDrugCost() {
		return refDrugCost;
	}

	public void setRefDrugCost(Double refDrugCost) {
		this.refDrugCost = refDrugCost;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}


	public void setCancelSchedReason(CancelSchedReason cancelSchedReason) {
		this.cancelSchedReason = cancelSchedReason;
	}

	public CancelSchedReason getCancelSchedReason() {
		return cancelSchedReason;
	}
	
	public String getNoNextSchedReason() {
		return noNextSchedReason;
	}

	public void setNoNextSchedReason(String noNextSchedReason) {
		this.noNextSchedReason = noNextSchedReason;
	}

	public void setLastDocRequestDate(Date lastDocRequestDate) {
		if (lastDocRequestDate != null) {
			this.lastDocRequestDate  = new Date(lastDocRequestDate.getTime());
		} else {
			this.lastDocRequestDate  = null;
		}
	}

	public Date getLastDocRequestDate() {		
		return (lastDocRequestDate != null) ? new Date(lastDocRequestDate.getTime()) : null;
	}


	public Date getLastSchedule() {
		return (lastSchedule !=null) ? new Date(lastSchedule.getTime()): null;
	}

	public void setLastSchedule(Date lastSchedule) {
		if (lastSchedule !=null){
			this.lastSchedule = new Date(lastSchedule.getTime());
		}
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public boolean isToCopy() {
		return toCopy;
	}

	public void setToCopy(boolean toCopy) {
		this.toCopy = toCopy;
	}

	public void setScheduleStartDate(Date scheduleStartDate) {
		if (scheduleStartDate != null) {
			this.scheduleStartDate = new Date(scheduleStartDate.getTime());
		} else {
			this.scheduleStartDate = null;
		}
	}

	public Date getScheduleStartDate() {
		return (scheduleStartDate != null) ? new Date(scheduleStartDate.getTime()) : null;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setTestFrequency(TestFrequency testFrequency) {
		this.testFrequency = testFrequency;
	}

	public TestFrequency getTestFrequency() {
		return testFrequency;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public Lab getLab() {
		return lab;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Contract getContract() {
		return contract;
	}
	
	public void setSampleItem(SampleItem sampleItem) {
		this.sampleItem = sampleItem;
	}

	public SampleItem getSampleItem() {
		return sampleItem;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public Institution getInstitution() {
		return institution;
	}
	
	public void setScheduleMonth(Date scheduleMonth) {
		if (scheduleMonth != null){
			this.scheduleMonth = new Date(scheduleMonth.getTime());
		}
	}

	public Date getScheduleMonth() {
		return (scheduleMonth != null) ? new Date(scheduleMonth.getTime()) : null;
	}


	public void setChemicalAnalysis(ChemicalAnalysis chemicalAnalysis) {
		this.chemicalAnalysis = chemicalAnalysis;
	}

	public ChemicalAnalysis getChemicalAnalysis() {
		return chemicalAnalysis;
	}

	public SampleTest getSampleTest() {
		return sampleTest;
	}

	public void setSampleTest(SampleTest sampleTest) {
		this.sampleTest = sampleTest;
	}

	public void setMicroBioTest(MicroBioTest microBioTest) {
		this.microBioTest = microBioTest;
	}

	public MicroBioTest getMicroBioTest() {
		return microBioTest;
	}
	
	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return selected;
	}
}
