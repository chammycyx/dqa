package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("companyService")
@RemoteDestination
@MeasureCalls
public class CompanyServiceBean implements CompanyServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private Company company;
	
	private static final String COMPANY_CODE = "companyCode";
	
	@SuppressWarnings("unchecked")	
	public Company retrieveCompanyByCompanyCodeManufFlag( String manufCode, YesNoFlag manufFlag ) {
		logger.debug( "retrieveCompanyByCompanyCodeManufFlag #0 #1", manufCode, manufFlag );
		List<Company> manufList = em.createNamedQuery( "Company.findByCompanyCodeManufFlag" )
										   .setParameter(COMPANY_CODE, manufCode)
										   .setParameter("manufFlag", manufFlag)
										   .getResultList();
		
		if(  manufList.size() > 0  ) {
			return manufList.get( 0 );
		}
		return null;				
	}
	
	@SuppressWarnings("unchecked")	
	public Company retrieveCompanyByCompanyCodePharmCompanyFlag( String pharmCompanyCode, YesNoFlag pharmCompanyFlag ) {
		logger.debug( "retrieveCompanyByCompanyCodePharmCompanyFlag #0 #1", pharmCompanyCode, pharmCompanyFlag );
		List<Company> pharmCompanyList = em.createNamedQuery( "Company.findByCompanyCodePharmCompanyFlag" )
										   .setParameter(COMPANY_CODE, pharmCompanyCode)
										   .setParameter("pharmCompanyFlag", pharmCompanyFlag)
										   .getResultList();		
		if( pharmCompanyList.size() > 0 ) {
			return pharmCompanyList.get( 0 );
		}
		return null;			
	}
	
	public void retrieveCompanyByCompanyCode( String pharmCompanyCode ) {
		logger.debug( "retrieveCompanyByCompanyCode #0", pharmCompanyCode );
		
		List<Company> companyListFind = em.createNamedQuery( "Company.findByCompanyCode" )
									   .setParameter(COMPANY_CODE, pharmCompanyCode.trim())
									   .setHint(QueryHints.REFRESH, HintValues.TRUE)
									   .setHint(QueryHints.FETCH, "o.contact")
									   .setHint(QueryHints.BATCH, "o.companyContactList")
									   .setHint(QueryHints.BATCH, "o.companyContactList.contact")
									   .getResultList();
		if(companyListFind!= null && companyListFind.size()>0)
		{
			company = companyListFind.get(0);
			for (CompanyContact companyContact : company.getCompanyContactList()) {
				companyContact.getCompany();
			}
		}
		else{
			company = null;
		}
		
	}
	
	public Company retrieveCompanyForPhs( String companyCode ){
		logger.debug( "retrieveCompanyForPhs #0", companyCode );
		
		List<Company> companyListFind = em.createNamedQuery( "Company.findByCompanyCode" )
									   .setParameter(COMPANY_CODE, companyCode.trim())
									   .setHint(QueryHints.REFRESH, HintValues.TRUE)
									   .setHint(QueryHints.FETCH, "o.contact")
									   .setHint(QueryHints.BATCH, "o.companyContactList")
									   .setHint(QueryHints.BATCH, "o.companyContactList.contact")
									   .getResultList();
		if(companyListFind!= null && companyListFind.size()>0)
		{
			return companyListFind.get(0);
		}
		else{
			return null;
		}
	}
	
	public Company findManufacturerByCompanyCode(String companyCode){
		logger.debug( "findManufacturerByCompanyCode #0", companyCode );
		
		List<Company> manufacturers = em.createNamedQuery("Company.findByCompanyCodeManufFlag")
										.setParameter("companyCode", companyCode)
										.setParameter("manufFlag", YesNoFlag.Yes)
										.getResultList();
		
		if(manufacturers!=null && manufacturers.size()>0)
		{
			return manufacturers.get(0);
		}
		else
		{
			return null;
		}
	}
	
	@Remove
	public void destroy() {
		if ( company != null ){
			company = null;}
	}
}
