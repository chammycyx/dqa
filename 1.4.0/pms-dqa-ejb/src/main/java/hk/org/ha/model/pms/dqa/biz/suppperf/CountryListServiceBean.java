package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.suppperf.Country;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("countryListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CountryListServiceBean implements CountryListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Country> countryList;

	@SuppressWarnings("unchecked")
	public void retrieveCountryListLikeCountryCode(String countryCode){
		logger.debug("retrieveSupplierListLike");
		countryList = em.createNamedQuery("Country.findLikeCountryCode")
			.setParameter("countryCode", countryCode+"%")
			.setParameter("recordStatus",RecordStatus.Active)
			.getResultList();
	}
	
    @Remove
	public void destroy(){	
		if (countryList != null){
			countryList = null;
		}
	}

}
