package hk.org.ha.model.pms.dqa.vo.coa;

import java.util.Date;

public class MissingCoaItemRptData {
	
	private String contractNum = "";
	
	private String orderType = "";
	
	private String itemCode = "";
	
	private String supplierCode = "";
	
	private String manufacturerCode = "";
	
	private Date uploadDate = null;
	
	private String pharmCompanyCode = "";
	
	private String itemDesc = "";
	
	private Date startDate = null;
	
	private Date endDate = null;

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufacturerCode() {
		return manufacturerCode;
	}

	public void setManufacturerCode(String manufacturerCode) {
		this.manufacturerCode = manufacturerCode;
	}

	public Date getUploadDate() {
		return (uploadDate != null) ? new Date(uploadDate.getTime()) : null;
	}

	public void setUploadDate(Date uploadDate) {
		if (uploadDate != null){
			this.uploadDate = new Date(uploadDate.getTime());
		}
	}
	
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	} 
}
