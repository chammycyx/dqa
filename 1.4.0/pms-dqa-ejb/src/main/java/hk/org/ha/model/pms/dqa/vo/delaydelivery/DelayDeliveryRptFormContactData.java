package hk.org.ha.model.pms.dqa.vo.delaydelivery;

import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.TitleType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class DelayDeliveryRptFormContactData {
	private Integer priority;
	private Long refNum;
	private Long contactId;
    private String firstName;
	
    private String lastName;
	
    private TitleType title;
    
    private Rank position;
	
    private String officePhone;

    
    private String email;


	public Integer getPriority() {
		return priority;
	}


	public void setPriority(Integer priority) {
		this.priority = priority;
	}


	public Long getRefNum() {
		return refNum;
	}


	public void setRefNum(Long refNum) {
		this.refNum = refNum;
	}


	public Long getContactId() {
		return contactId;
	}


	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public TitleType getTitle() {
		return title;
	}


	public void setTitle(TitleType title) {
		this.title = title;
	}


	public Rank getPosition() {
		return position;
	}


	public void setPosition(Rank position) {
		this.position = position;
	}


	public String getOfficePhone() {
		return officePhone;
	}


	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	
}
