package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.Calendar;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaOutstandBatch;
import hk.org.ha.model.pms.dqa.vo.coa.CoaOutstandBatchCriteria;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.SESSION)
@Name("coaOutstandBatchListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CoaOutstandBatchListServiceBean implements CoaOutstandBatchListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private List<CoaOutstandBatch> coaOutstandBatchList;
	
	@Out(required = false)
	private Integer maxCreateDateMonthRangeForOutstandBatchCoa;
	
	@Out(required = false)
	private Integer defaultCreateDateMonthRangeForOutstandBatchCoa;

	public void setMaxCreateDateMonthRangeForOutstandBatchCoa(
			Integer maxCreateDateMonthRangeForOutstandBatchCoa) {
		this.maxCreateDateMonthRangeForOutstandBatchCoa = maxCreateDateMonthRangeForOutstandBatchCoa;
	}
	
	public Integer getMaxCreateDateMonthRangeForOutstandBatchCoa() {
		return maxCreateDateMonthRangeForOutstandBatchCoa;
	}

	public void setDefaultCreateDateMonthRangeForOutstandBatchCoa(
			Integer defaultCreateDateMonthRangeForOutstandBatchCoa) {
		this.defaultCreateDateMonthRangeForOutstandBatchCoa = defaultCreateDateMonthRangeForOutstandBatchCoa;
	}
	
	public Integer getDefaultCreateDateMonthRangeForOutstandBatchCoa() {
		return defaultCreateDateMonthRangeForOutstandBatchCoa;
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveCoaOutstandBatchListForOutstandBatchCoa( CoaOutstandBatchCriteria criteria ){
		logger.debug("retrieveCoaOutstandBatchListForOutstandBatchCoa #0", criteria.getFromCreateDate());
		
		coaOutstandBatchList = (List<CoaOutstandBatch>) em.createNamedQuery("CoaOutstandBatch.findForOutstandBatchCoa")
														  .setParameter("fromCreateDate", criteria.getFromCreateDate())
														  .setParameter("toCreateDate", criteria.getToCreateDate())
														  .setParameter("fromReceiveDate", criteria.getFromReceiveDate())
														  .setParameter("toReceiveDate", criteria.getToReceiveDate())
														  .setParameter("isOutstand", (criteria.isOutstand()?"Y":null))
														  .setHint(QueryHints.FETCH, "o.contract")
														  .setHint(QueryHints.FETCH, "o.contract.supplier")
														  .setHint(QueryHints.FETCH, "o.contract.manufacturer")
														  .setHint(QueryHints.LEFT_FETCH, "o.contract.manufacturer.contact")
														  .getResultList();
	}
	//For seam test case only
	public void testRetrieveCoaOutstandBatchListForOutstandBatchCoa() {
		CoaOutstandBatchCriteria criteria = new CoaOutstandBatchCriteria();
		Calendar cal = Calendar.getInstance();
		cal.set( Calendar.HOUR, 0);
		cal.set( Calendar.MINUTE, 0);
		cal.set( Calendar.SECOND, 0);
		cal.set( Calendar.MILLISECOND, 0);
		cal.add( Calendar.DATE, 1);
		criteria.setToCreateDate( cal.getTime() );
		cal.add( Calendar.MONTH, -1);
		criteria.setFromCreateDate( cal.getTime() );
		criteria.setFromReceiveDate( null );
		criteria.setToReceiveDate( null );
		criteria.setOutstand( false );
		retrieveCoaOutstandBatchListForOutstandBatchCoa( criteria );
	}
	
	@Remove
	public void destroy(){
		if ( maxCreateDateMonthRangeForOutstandBatchCoa != null ) {
			maxCreateDateMonthRangeForOutstandBatchCoa = null;
		}
		
		if ( defaultCreateDateMonthRangeForOutstandBatchCoa != null ) {
			defaultCreateDateMonthRangeForOutstandBatchCoa = null;
		}
		
		if(coaOutstandBatchList != null) {
			coaOutstandBatchList = null;
		}

	}
}
