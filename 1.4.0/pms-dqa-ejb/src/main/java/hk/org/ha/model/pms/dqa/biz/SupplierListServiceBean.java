package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Supplier;

import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("supplierListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SupplierListServiceBean implements SupplierListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Supplier> supplierList;

	@SuppressWarnings("unchecked")
	public void retrieveSupplierList(){
		logger.debug("retrieveSupplierList");
		supplierList = em.createNamedQuery("Supplier.findAll").setHint(QueryHints.FETCH, "o.contact")
			.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveSupplierListLike(String supplierCode){
		logger.debug("retrieveSupplierListLike");
		supplierList = em.createNamedQuery("Supplier.findLikeSupplierCode")
			.setParameter("supplierCode", supplierCode+"%")
			.setHint(QueryHints.FETCH, "o.contact")
			.getResultList();
	}
	
    @Remove
	public void destroy(){	
		if (supplierList != null){
			supplierList = null;
		}
	}

}
