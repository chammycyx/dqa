package hk.org.ha.model.pms.dqa.udt.suppperf;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum RefNumSuffix implements StringValuedEnum {
	
	REFHOSP("REFHOSP", "PHS/QA/3"),
	REFSUPP("REFSUPP", "PHS/QA/4");	
	
    private final String dataValue;
    private final String displayValue;
        
    RefNumSuffix(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<RefNumSuffix> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<RefNumSuffix> getEnumClass() {
    		return RefNumSuffix.class;
    	}
    }
}
