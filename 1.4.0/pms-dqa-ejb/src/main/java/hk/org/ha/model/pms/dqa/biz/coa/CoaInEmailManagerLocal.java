package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;

import javax.ejb.Local;

@Local
public interface CoaInEmailManagerLocal {
	
	CoaInEmail createCoaInEmail( ValidateResultFlag validateSubjectResult, EmailLog emailLog );
	
	void updateCoaInEmail(CoaInEmail inEmail);
}
