package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import java.util.List;

import javax.ejb.Local;

@Local
public interface TestFrequencyListServiceLocal {
	
	 List<TestFrequency> retrieveTestFrequencyListByRecordStatusRiskLevelCode(RecordStatus recordStatus, String riskLevelCode);
	 
	 void retrieveTestFrequencyListByRecordStatus(RecordStatus recordStatus);
	 
	 List<TestFrequency> retrieveTestFrequencyListByTestCodeOrderTypeFullPercentage(String testCodeIn, OrderType orderTypeIn, Boolean fullPercentage);
	 
	 List<TestFrequency> checkTestFrequencyValid(String testCode, String riskLevelCode, String orderType);
	 
	 void destroy();
}
