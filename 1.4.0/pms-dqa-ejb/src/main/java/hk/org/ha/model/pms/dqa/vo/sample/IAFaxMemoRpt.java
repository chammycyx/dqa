package hk.org.ha.model.pms.dqa.vo.sample;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class IAFaxMemoRpt {
	private String ref;
	private String tel;
	private String fax;
	private String date;
	private String to;
	private String attn;
	private String itemDesc;
	private String manuf;
	private String supplier;
	private String paragraph1;
	private String paragraph2;
	private String paragraph3;
	private String paragraph4;
	private String paragraphOther;
	private String sender;
	private String p2No;
	private String p3No;
	private String p4No;
	private String pOthNo;
	
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getAttn() {
		return attn;
	}
	public void setAttn(String attn) {
		this.attn = attn;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getManuf() {
		return manuf;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getParagraph1() {
		return paragraph1;
	}
	public void setParagraph1(String paragraph1) {
		this.paragraph1 = paragraph1;
	}
	public String getParagraph2() {
		return paragraph2;
	}
	public void setParagraph2(String paragraph2) {
		this.paragraph2 = paragraph2;
	}
	public String getParagraph3() {
		return paragraph3;
	}
	public void setParagraph3(String paragraph3) {
		this.paragraph3 = paragraph3;
	}
	public String getParagraph4() {
		return paragraph4;
	}
	public void setParagraph4(String paragraph4) {
		this.paragraph4 = paragraph4;
	}
	public String getParagraphOther() {
		return paragraphOther;
	}
	public void setParagraphOther(String paragraphOther) {
		this.paragraphOther = paragraphOther;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getP2No() {
		return p2No;
	}
	public void setP2No(String p2No) {
		this.p2No = p2No;
	}
	public String getP3No() {
		return p3No;
	}
	public void setP3No(String p3No) {
		this.p3No = p3No;
	}
	public String getP4No() {
		return p4No;
	}
	public void setP4No(String p4No) {
		this.p4No = p4No;
	}
	public String getpOthNo() {
		return pOthNo;
	}
	public void setpOthNo(String pOthNo) {
		this.pOthNo = pOthNo;
	}
	
}