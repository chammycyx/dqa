package hk.org.ha.model.pms.dqa.vo.sample;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.sample.ReportTemplate;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.OrderType;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleEnquiryDetailCriteria {
	
	private ReportTemplate reportTemplate;
	private RecordStatus recordStatus;
	private Collection<ScheduleStatus> scheduleStatusList;
	private String scheduleNum;
	
	private String itemCode;
	private OrderType orderType;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	
	private String testCode;
	private String riskLevelCode;
	private String labCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromScheduleMonth;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toScheduleMonth;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromTestReportDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toTestReportDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromPrIssueDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toPrIssueDate;

	
	private String institutionCode;
	private String contractNum;
	private String contractSuffix;
	 	
	@Temporal(TemporalType.TIMESTAMP)
	private Date contractStartDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date contractEndDate;
	
	private String batchNum;
	private String expiryDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromReceiveDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toReceiveDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromLabCollectDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toLabCollectDate;
	
	private String collectNum;
	
	private String poNum;
	private String testResult;
	
	
	public void setReportTemplate(ReportTemplate reportTemplate) {
		this.reportTemplate = reportTemplate;
	}

	public ReportTemplate getReportTemplate() {
		return reportTemplate;
	}

		
	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	/*public ScheduleStatus getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(ScheduleStatus scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}*/

	public Collection<ScheduleStatus> getScheduleStatusList() {
		return scheduleStatusList;
	}
	
	public void setScheduleStatusList(Collection<ScheduleStatus> scheduleStatusList) {
		this.scheduleStatusList = scheduleStatusList;
	}
	
	public String getScheduleNum() {
		return scheduleNum;
	}

	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getRiskLevelCode() {
		return riskLevelCode;
	}

	public void setRiskLevelCode(String riskLevelCode) {
		this.riskLevelCode = riskLevelCode;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Date getFromScheduleMonth() {
		return (fromScheduleMonth != null) ? new Date(fromScheduleMonth.getTime()) :null ;
	}

	public void setFromScheduleMonth(Date fromScheduleMonth) {
		if (fromScheduleMonth != null){
			this.fromScheduleMonth = new Date(fromScheduleMonth.getTime());
		}
	}

	public Date getToScheduleMonth() {
		return (toScheduleMonth != null )? new Date(toScheduleMonth.getTime()) : null;
	}

	public void setToScheduleMonth(Date toScheduleMonth) {
		if (toScheduleMonth != null) {
			this.toScheduleMonth = new Date(toScheduleMonth.getTime());
		}
	}

	public Date getFromTestReportDate() {
		return (fromTestReportDate != null) ? new Date(fromTestReportDate.getTime()) : null;
	}

	public void setFromTestReportDate(Date fromTestReportDate) {
		if (fromTestReportDate != null) {
			this.fromTestReportDate = new Date(fromTestReportDate.getTime());
		}
	}

	public Date getToTestReportDate() {
		return (toTestReportDate != null) ? new Date(toTestReportDate.getTime()) : null;
	}

	public void setToTestReportDate(Date toTestReportDate) {
		if (toTestReportDate != null) {
			this.toTestReportDate = new Date(toTestReportDate.getTime() );
		}
	}

	public Date getFromPrIssueDate() {
		return (fromPrIssueDate != null) ? new Date(fromPrIssueDate.getTime()) : null ;
	}

	public void setFromPrIssueDate(Date fromPrIssueDate) {
		if (fromPrIssueDate != null) {
			this.fromPrIssueDate = new Date(fromPrIssueDate.getTime());
		}
	}

	public Date getToPrIssueDate() {
		return (toPrIssueDate != null) ? new Date(toPrIssueDate.getTime()) : null;
	}

	public void setToPrIssueDate(Date toPrIssueDate) {
		if (toPrIssueDate != null) {
			this.toPrIssueDate = new Date(toPrIssueDate.getTime());
		}
	}
	
	
	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	
	public String getContractSuffix() {
		return contractSuffix;
	}

	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}

	public Date getContractStartDate() {
		return (contractStartDate != null) ? new Date(contractStartDate.getTime()) : null;
	}

	public void setContractStartDate(Date contractStartDate) {
		if (contractStartDate != null) {
			this.contractStartDate = new Date(contractStartDate.getTime());
		}
		else
		{
			this.contractStartDate = null;
		}
	}

	public Date getContractEndDate() {
		return (contractEndDate != null) ? new Date(contractEndDate.getTime()) : null;
	}

	public void setContractEndDate(Date contractEndDate) {
		if (contractEndDate != null) {
			this.contractEndDate = new Date(contractEndDate.getTime());
		}
		else
		{
			this.contractEndDate = null;
		}
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getFromReceiveDate() {
		return (fromReceiveDate != null) ? new Date(fromReceiveDate.getTime()) : null;
	}

	public void setFromReceiveDate(Date fromReceiveDate) {
		if (fromReceiveDate != null) {
			this.fromReceiveDate = new Date(fromReceiveDate.getTime());
		}
		else
		{
			this.fromReceiveDate = null;
		}
	}

	public Date getToReceiveDate() {
		return (toReceiveDate != null) ? new Date(toReceiveDate.getTime()) : null;
	}

	public void setToReceiveDate(Date toReceiveDate) {
		if (toReceiveDate != null) {
			this.toReceiveDate = new Date(toReceiveDate.getTime());
		}
		else
		{
			this.toReceiveDate = null;
		}
	}

	public Date getFromLabCollectDate() {
		return (fromLabCollectDate != null) ? new Date(fromLabCollectDate.getTime()) : null;
	}

	public void setFromLabCollectDate(Date fromLabCollectDate) {
		if (fromLabCollectDate != null) {
			this.fromLabCollectDate = new Date(fromLabCollectDate.getTime());
		}
		else
		{
			this.fromLabCollectDate = null;
		}
	}
	
	public Date getToLabCollectDate() {
		return (toLabCollectDate != null) ? new Date(toLabCollectDate.getTime()) : null;
	}
	
	public void setToLabCollectDate(Date toLabCollectDate) {
		if (toLabCollectDate != null) {
			this.toLabCollectDate = new Date(toLabCollectDate.getTime());
		}
		else
		{
			this.toLabCollectDate = null;
		}
	}
	
	public String getCollectNum() {
		return collectNum;
	}
	
	public void setCollectNum(String collectNum) {
		this.collectNum = collectNum;
	}
	
	public String getPoNum() {
		return poNum;
	}

	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}

	public String getTestResult() {
		return testResult;
	}
	
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}


}
