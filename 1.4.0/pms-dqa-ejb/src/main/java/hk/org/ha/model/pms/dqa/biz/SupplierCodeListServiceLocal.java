package hk.org.ha.model.pms.dqa.biz;
import javax.ejb.Local;

@Local
public interface SupplierCodeListServiceLocal {
	
	void retrieveSupplierCodeList();
	
    void destroy();
}
