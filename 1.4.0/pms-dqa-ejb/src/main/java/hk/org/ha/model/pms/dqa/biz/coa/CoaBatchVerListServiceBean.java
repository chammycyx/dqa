package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaBatchVerListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaBatchVerListServiceBean implements CoaBatchVerListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaBatchVer> coaBatchVerList;
	
	public void retrieveCoaBatchVerListByCoaBatch(CoaBatch coaBatch) {
		logger.debug("retrieveCoaBatchVerListByCoaBatch #0", coaBatch.getCoaBatchId());		
		coaBatchVerList = em.createNamedQuery("CoaBatchVer.findByCoaBatchDiscrepancyStatus")
								.setParameter("coaBatch", coaBatch)
								.setParameter("discrepancyStatus", DiscrepancyStatus.DiscrepancyUnderGrouping)
								.setParameter("recordStatus", RecordStatus.Active)
								.getResultList();
		
	}
	
	public void retrieveCoaBatchVerListByCoaBatchId( Long coaBatchId ) {
		logger.debug("retrieveCoaBatchVerListByCoaBatchId #0", coaBatchId);
		coaBatchVerList = em.createNamedQuery("CoaBatchVer.findByCoaBatchId")							
		  					.setParameter("coaBatchId", coaBatchId)
		  					.setParameter("recordStatus", RecordStatus.Active)
		  					.getResultList();
	}
	
	@Remove
	public void destroy(){
		if( coaBatchVerList != null ) {
			coaBatchVerList = null;
		}
	}
}
