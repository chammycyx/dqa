package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.ContractMsp;
import hk.org.ha.model.pms.dqa.vo.ContractLine;

import javax.ejb.Local;

@Local
public interface ErpContractMspServiceLocal {

	ContractMsp retrieveContractMspByContractIdErpContractMspId( Long contractId, Long erpContractMspId );
	
	void createContractMsp( Contract c, ContractLine cl, hk.org.ha.model.pms.dqa.vo.ContractMsp cm);
	
	void updateContractMsp( ContractMsp cMsp, hk.org.ha.model.pms.dqa.vo.ContractMsp cm );

}
