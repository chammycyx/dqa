package hk.org.ha.model.pms.dqa.persistence.delaydelivery;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.FileItem;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatchVer;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;
@Entity
@Table(name = "DLD_RPT_FORM")
@Customizer(AuditCustomizer.class)
public class DelayDeliveryRptForm extends VersionEntity {
	
	private static final long serialVersionUID = -8641198309559595599L;

	@Id
	@Column(name = "REF_NUM", nullable = false)
	private Long refNum;
	
	@ManyToOne
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;
	
	@OneToOne
	@JoinColumn(name = "CONTRACT_ID")
	private Contract contract;
	
	@Column(name = "REPORT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date reportDate;
	
	@Column(name = "ON_HAND_QTY")
	private Long onHandQty;
	
	@Column(name = "AVG_MTH_CONSUMPT_QTY")
	private Long avgMthConsumptQty;
	
	@Column(name = "ON_HAND_LAST_FOR")
	private BigDecimal onHandLastFor;
	
	@Column(name = "CONFLICT_FLAG", nullable = false)
    private Boolean conflictFlag;
	
	@Column(name="CONFLICT_MSP_NAME", length=150)
	private String conflictMspName;
	
	@Column(name="CONFLICT_RELATIONSHIP", length=150)
	private String conflictRelationship;
	
	@Column(name="CONFLICT_BY_NAME", length=150)
	private String conflictByName;
	
	@Column(name="CONFLICT_BY_RANK", length=50)
	private String conflictByRank;
	
	@Column(name = "CONFLICT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date conflictDate;
	
	@OneToOne
	@JoinColumn(name = "CONFLICT_FILE_ID")
	private FileItem conflictFile;
	
	@OneToOne
	@JoinColumn(name = "DLD_RPT_FORM_VER_ID")
	private DelayDeliveryRptFormVer delayDeliveryRptFormVer;	
	
	@OneToMany(mappedBy = "DelayDeliveryRptForm" , cascade=CascadeType.REMOVE)
	private List<DelayDeliveryRptFormPo> delayDeliveryRptFormPoList;
	
	@OneToMany(mappedBy = "DelayDeliveryRptForm" , cascade=CascadeType.REMOVE)
	private List<DelayDeliveryRptFormContact> delayDeliveryRptFormContactList;
	
	
	public List<DelayDeliveryRptFormPo> getDelayDeliveryRptFormPoList() {
		return delayDeliveryRptFormPoList;
	}
	public void setDelayDeliveryRptFormPoList(
			List<DelayDeliveryRptFormPo> delayDeliveryRptFormPoList) {
		this.delayDeliveryRptFormPoList = delayDeliveryRptFormPoList;
	}
	public List<DelayDeliveryRptFormContact> getDelayDeliveryRptFormContactList() {
		return delayDeliveryRptFormContactList;
	}
	public void setDelayDeliveryRptFormContactList(
			List<DelayDeliveryRptFormContact> delayDeliveryRptFormContactList) {
		this.delayDeliveryRptFormContactList = delayDeliveryRptFormContactList;
	}
	public Long getRefNum() {
		return refNum;
	}
	public void setRefNum(Long refNum) {
		this.refNum = refNum;
	}
	

	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public Long getOnHandQty() {
		return onHandQty;
	}
	public void setOnHandQty(Long onHandQty) {
		this.onHandQty = onHandQty;
	}
	public Long getAvgMthConsumptQty() {
		return avgMthConsumptQty;
	}
	public void setAvgMthConsumptQty(Long avgMthConsumptQty) {
		this.avgMthConsumptQty = avgMthConsumptQty;
	}
	public BigDecimal getOnHandLastFor() {
		return onHandLastFor;
	}
	public void setOnHandLastFor(BigDecimal onHandLastFor) {
		this.onHandLastFor = onHandLastFor;
	}
	public String getConflictMspName() {
		return conflictMspName;
	}
	public void setConflictMspName(String conflictMspName) {
		this.conflictMspName = conflictMspName;
	}
	public String getConflictRelationship() {
		return conflictRelationship;
	}
	public void setConflictRelationship(String conflictRelationship) {
		this.conflictRelationship = conflictRelationship;
	}
	public String getConflictByName() {
		return conflictByName;
	}
	public void setConflictByName(String conflictByName) {
		this.conflictByName = conflictByName;
	}
	public String getConflictByRank() {
		return conflictByRank;
	}
	public void setConflictByRank(String conflictByRank) {
		this.conflictByRank = conflictByRank;
	}
	public Boolean getConflictFlag() {
		return conflictFlag;
	}
	public void setConflictFlag(Boolean conflictFlag) {
		this.conflictFlag = conflictFlag;
	}
	public FileItem getConflictFile() {
		return conflictFile;
	}
	public void setConflictFile(FileItem conflictFile) {
		this.conflictFile= conflictFile;
	}
	public DelayDeliveryRptFormVer getDelayDeliveryRptFormVer() {
		return delayDeliveryRptFormVer;
	}
	public void setDelayDeliveryRptFormVer(
			DelayDeliveryRptFormVer delayDeliveryRptFormVer) {
		this.delayDeliveryRptFormVer = delayDeliveryRptFormVer;
	}
	public Institution getInstitution() {
		return institution;
	}
	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	public Date getConflictDate() {
		return conflictDate;
	}
	public void setConflictDate(Date conflictDate) {
		this.conflictDate = conflictDate;
	}
	
	
	
	
	
}
