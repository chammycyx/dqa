package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;

import javax.ejb.Local;

@Local
public interface ProblemNatureCatListServiceLocal {
	
	void retrieveProblemNatureCatList(ProblemNatureParam problemNatureParamIn);
	
	void destroy();
	
}
