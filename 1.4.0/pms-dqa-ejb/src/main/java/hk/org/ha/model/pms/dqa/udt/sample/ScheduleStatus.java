package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ScheduleStatus implements StringValuedEnum{
	
	ScheduleGen("SG", "Schedule Generation"),
	Schedule("S", "Schedule"),
	Document("D", "Document"),
	InstitutionAssignment("IA", "Institution Assignment"),
	DrugSample("DS", "Drug Sample"),
	LabTest("LT", "Lab Test"),
	LabCollection("LC", "Lab Collection"),
	TestResult("TR", "Test Result"), 
	Payment("P", "Payment"),
	Complete("C", "Complete");

	private final String dataValue;
	private final String displayValue;

	ScheduleStatus(final String dataValue, final String displayValue){
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}        

	@Override
	public String getDataValue() {
		return this.dataValue;
	}

	@Override
	public String getDisplayValue() {
		return this.displayValue;
	}    

	public static class Converter extends StringValuedEnumConverter<ScheduleStatus> {

		private static final long serialVersionUID = 3365588302513687193L;

		@Override
		public Class<ScheduleStatus> getEnumClass() {
			return ScheduleStatus.class;
		}
	}
}
