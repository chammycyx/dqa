package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemFile;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;

import javax.ejb.Local;

@Local
public interface QaProblemFileServiceLocal {
	
	QaProblemFile uploadFile(QaProblemFileUploadData qaProblemFileUploadDataIn, QaProblem qaProblemIn);
	
	void deleteFile(QaProblemFileUploadData qaProblemFileUploadDataIn, QaProblem qaProblemIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
