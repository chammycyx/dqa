package hk.org.ha.model.pms.dqa.persistence.suppperf;

import java.util.Date;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "FAX_INTERIM_SUPP")
@Customizer(AuditCustomizer.class)
public class FaxInterimSupp extends VersionEntity {

	private static final long serialVersionUID = -5932116516416353725L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "faxInterimSuppSeq")
	@SequenceGenerator(name = "faxInterimSuppSeq", sequenceName = "SEQ_FAX_INTERIM_SUPP", initialValue=10000)
	@Id
	@Column(name="FAX_INTERIM_SUPP_ID", nullable=false)
	private Long faxInterimSuppId;
	
	@Column(name="REF_NUM", length=15)
	private String refNum;
	
	@Column(name="REF_NUM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date refNumDate;
	
	@Column(name="DOC_ON_BEFORE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date docOnBeforeDate;

	@Converter(name = "FaxInterimSupp.investRptFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimSupp.investRptFlag")
	@Column(name="INVEST_RPT_FLAG", length=1)
	private YesNoFlag investRptFlag;
	
	@Converter(name = "FaxInterimSupp.coaFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimSupp.coaFlag")
	@Column(name="COA_FLAG", length=1)
	private YesNoFlag coaFlag;
	
	@Converter(name = "FaxInterimSupp.impMeasureFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("FaxInterimSupp.impMeasureFlag")
	@Column(name="IMP_MEASURE_FLAG", length=1)
	private YesNoFlag impMeasureFlag;

	@Column(name="OTHER_DESC", length=200)
	private String otherDesc;

	public Long getFaxInterimSuppId() {
		return faxInterimSuppId;
	}

	public void setFaxInterimSuppId(Long faxInterimSuppId) {
		this.faxInterimSuppId = faxInterimSuppId;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public Date getRefNumDate() {
		return (refNumDate!=null)?new Date(refNumDate.getTime()):null;
	}

	public void setRefNumDate(Date refNumDate) {
		if (refNumDate!=null) {
			this.refNumDate = new Date(refNumDate.getTime());
		} else {
			this.refNumDate = null;
		}
	}

	public Date getDocOnBeforeDate() {
		return (docOnBeforeDate!=null)?new Date(docOnBeforeDate.getTime()):null;
	}

	public void setDocOnBeforeDate(Date docOnBeforeDate) {
		if (docOnBeforeDate!=null) {
			this.docOnBeforeDate = new Date(docOnBeforeDate.getTime());
		} else {
			this.docOnBeforeDate = null;
		}
	}

	public YesNoFlag getInvestRptFlag() {
		return investRptFlag;
	}

	public void setInvestRptFlag(YesNoFlag investRptFlag) {
		this.investRptFlag = investRptFlag;
	}

	public YesNoFlag getCoaFlag() {
		return coaFlag;
	}

	public void setCoaFlag(YesNoFlag coaFlag) {
		this.coaFlag = coaFlag;
	}

	public YesNoFlag getImpMeasureFlag() {
		return impMeasureFlag;
	}

	public void setImpMeasureFlag(YesNoFlag impMeasureFlag) {
		this.impMeasureFlag = impMeasureFlag;
	}

	public String getOtherDesc() {
		return otherDesc;
	}

	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
}
