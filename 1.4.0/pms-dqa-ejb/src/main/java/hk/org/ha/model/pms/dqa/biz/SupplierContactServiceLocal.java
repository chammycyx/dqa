package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;


import javax.ejb.Local;


@Local
public interface SupplierContactServiceLocal {
	
	void addSupplierContact();
	
	void createSupplierContact();
	
	void retrieveSupplierContactBySupplierCodeContactId(String supplierCode, Long contactId);
	
	void updateSupplierContact();
	
	boolean isUpdateSucceed();
	
	void deleteSupplierContact(SupplierContact supplierContact);
		
	void destroy();
	
}
