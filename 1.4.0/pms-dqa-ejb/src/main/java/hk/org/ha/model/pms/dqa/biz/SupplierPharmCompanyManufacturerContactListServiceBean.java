package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.Supplier;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.UserInfo;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.persistence.sample.Lab;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("supplierPharmCompanyManufacturerContactListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SupplierPharmCompanyManufacturerContactListServiceBean implements SupplierPharmCompanyManufacturerContactListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<SupplierPharmCompanyManufacturerContact> supplierPharmCompanyManufacturerContactList;
	
	private static final String COMPANY_CODE = "companyCode";

	@SuppressWarnings("unchecked")
	public void retrieveMspContactListByContract(Contract contract) {	
		logger.debug("retrieveMspContactListByContract ");
		
		List<Supplier> supplierListFind = null;
		List<Company> pharmCompanyListFind = null;
		List<Company> manufacturerListFind = null;
		supplierPharmCompanyManufacturerContactList = new ArrayList();
		
		if(contract!=null){
			if(contract.getSupplier()!=null)
			{
				supplierListFind = em.createNamedQuery("Supplier.findBySupplierCode")
													.setParameter("supplierCode", contract.getSupplier().getSupplierCode())
													.getResultList();
			}
			
			if(contract.getPharmCompany()!=null)
			{
				pharmCompanyListFind = em.createNamedQuery("Company.findByCompanyCodePharmCompanyFlag")
													.setParameter(COMPANY_CODE, contract.getPharmCompany().getCompanyCode())
													.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
													.getResultList();
			}
			
			if(contract.getManufacturer()!=null)
			{
				manufacturerListFind = em.createNamedQuery("Company.findByCompanyCodeManufFlag")
													.setParameter(COMPANY_CODE, contract.getManufacturer().getCompanyCode())
													.setParameter("manufFlag", YesNoFlag.Yes)
													.getResultList();
			}
		}
		
		if(supplierListFind != null && supplierListFind.size()>0)
		{
			for(SupplierContact sc:supplierListFind.get(0).getSupplierContactList())
			{
				addMspContactList(supplierListFind.get(0).getSupplierCode(), 
							      supplierListFind.get(0).getSupplierName(),
								  "Supplier",
								  sc.getContact());	
			}
		
			addMspContactList(supplierListFind.get(0).getSupplierCode(),
							  supplierListFind.get(0).getSupplierName(),
							  "Supplier",
							  supplierListFind.get(0).getContact());
		}
		
		if(pharmCompanyListFind != null && pharmCompanyListFind.size()>0)
		{
			for(CompanyContact cc:pharmCompanyListFind.get(0).getCompanyContactList())
			{
				addMspContactList(pharmCompanyListFind.get(0).getCompanyCode(),
								  pharmCompanyListFind.get(0).getCompanyName(),
								  "Pharm Company",
								  cc.getContact());
			}
			
			addMspContactList(pharmCompanyListFind.get(0).getCompanyCode(),
							  pharmCompanyListFind.get(0).getCompanyName(),
							  "Pharm Company",
							  pharmCompanyListFind.get(0).getContact());
		}
		
		if(pharmCompanyListFind!=null && pharmCompanyListFind.get(0).getCompanyCode()!=null &&manufacturerListFind!=null && manufacturerListFind.get(0).getCompanyCode()!=null)
		{
			if (!pharmCompanyListFind.get(0).getCompanyCode().equals(manufacturerListFind.get(0).getCompanyCode()))
			{
				if( manufacturerListFind.size()>0)
				{
					for(CompanyContact cc:manufacturerListFind.get(0).getCompanyContactList())
					{
						addMspContactList(manufacturerListFind.get(0).getCompanyCode(),
										  manufacturerListFind.get(0).getCompanyName(),
										  "Manufacturer",
										  cc.getContact());
					}
					
					addMspContactList(manufacturerListFind.get(0).getCompanyCode(),
									  manufacturerListFind.get(0).getCompanyName(),
									  "Manufacturer",
									  manufacturerListFind.get(0).getContact());
				}
			}
		}
	}
	
	private void addMspContactList(String code, String name, String type, Contact contact) {
		logger.debug("addMspContactList #0 #1 #2", code, name, type);
		
		if (!StringUtils.isEmpty(contact.getEmail())) {
			SupplierPharmCompanyManufacturerContact spcmc = null;
			
			spcmc = new SupplierPharmCompanyManufacturerContact();
			spcmc.setCode(code);
			spcmc.setName(name);
			spcmc.setContactType(type);
			spcmc.setContact(contact);
			
			supplierPharmCompanyManufacturerContactList.add(spcmc);
		}
	}
	
	public void retrieveSupplierPharmCompanyManufacturerContactList(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("retrieveSupplierPharmCompanyManufacturerContactList");
		
		retrieveMspContactListByContract(sampleTestScheduleIn.getContract());
	}	
	
	public void retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen(SampleTestSchedule sampleTestScheduleIn)
	{
		logger.debug("retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen");
		List<Supplier> supplierListFind = null;
		List<Company> pharmCompanyListFind = null;
		List<Company> manufacturerListFind = null;
		List<Institution> institutionListFind = null;
		List<UserInfo> userInfoListFind = null;
		List<Lab> labListFind = null;
		supplierPharmCompanyManufacturerContactList = new ArrayList();
		SupplierPharmCompanyManufacturerContact spcmc = null;
		
		if(sampleTestScheduleIn.getContract().getSupplier()!=null)
		{
			supplierListFind = em.createNamedQuery("Supplier.findBySupplierCode")
												.setParameter("supplierCode", sampleTestScheduleIn.getContract().getSupplier().getSupplierCode())
												.getResultList();
		}
		
		if(sampleTestScheduleIn.getContract().getPharmCompany()!=null)
		{
			pharmCompanyListFind = em.createNamedQuery("Company.findByCompanyCodePharmCompanyFlag")
												.setParameter(COMPANY_CODE, sampleTestScheduleIn.getContract().getPharmCompany().getCompanyCode())
												.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
												.getResultList();
		}
		
		if(sampleTestScheduleIn.getContract().getManufacturer()!=null)
		{
			manufacturerListFind = em.createNamedQuery("Company.findByCompanyCodeManufFlag")
												.setParameter(COMPANY_CODE, sampleTestScheduleIn.getContract().getManufacturer().getCompanyCode())
												.setParameter("manufFlag", YesNoFlag.Yes)
												.getResultList();
		}
		
		if(sampleTestScheduleIn.getInstitution()!=null)
		{
			institutionListFind = em.createNamedQuery("Institution.findByInstitutionCode")
												.setParameter("institutionCode", sampleTestScheduleIn.getInstitution().getInstitutionCode())
												.getResultList();
		}
		
		if(sampleTestScheduleIn.getLab()!=null)
		{
			labListFind = em.createNamedQuery("Lab.findByLabCode")
												.setParameter("labCode", sampleTestScheduleIn.getLab().getLabCode())
												.getResultList();
		}
		
		userInfoListFind = em.createNamedQuery("UserInfo.findAll")
						  .getResultList();
		
		if(supplierListFind != null && supplierListFind.size()>0)
		{
			for(SupplierContact sc:supplierListFind.get(0).getSupplierContactList())
			{
				spcmc = new SupplierPharmCompanyManufacturerContact();
				spcmc.setCode(supplierListFind.get(0).getSupplierCode());
				spcmc.setName(supplierListFind.get(0).getSupplierName());
				spcmc.setContactType("Supplier");
				spcmc.setContact(sc.getContact());
				
				supplierPharmCompanyManufacturerContactList.add(spcmc);
			}
		}
		
		if(pharmCompanyListFind != null && pharmCompanyListFind.size()>0)
		{
			for(CompanyContact cc:pharmCompanyListFind.get(0).getCompanyContactList())
			{
				spcmc = new SupplierPharmCompanyManufacturerContact();
				spcmc.setCode(pharmCompanyListFind.get(0).getCompanyCode());
				spcmc.setName(pharmCompanyListFind.get(0).getCompanyName());
				spcmc.setContactType("Pharm Company");
				spcmc.setContact(cc.getContact());
				
				supplierPharmCompanyManufacturerContactList.add(spcmc);
			}
		}
		
		if(manufacturerListFind != null && manufacturerListFind.size()>0)
		{
			for(CompanyContact cc:manufacturerListFind.get(0).getCompanyContactList())
			{
				spcmc = new SupplierPharmCompanyManufacturerContact();
				spcmc.setCode(manufacturerListFind.get(0).getCompanyCode());
				spcmc.setName(manufacturerListFind.get(0).getCompanyName());
				spcmc.setContactType("Manufacturer");
				spcmc.setContact(cc.getContact());
				
				supplierPharmCompanyManufacturerContactList.add(spcmc);
			}
		}
		
		if(institutionListFind != null && institutionListFind.size()>0)
		{
			Contact ic = institutionListFind.get(0).getContact();
			
			spcmc = new SupplierPharmCompanyManufacturerContact();
			spcmc.setCode(institutionListFind.get(0).getInstitutionCode());
			spcmc.setName(institutionListFind.get(0).getInstitutionName());
			spcmc.setContactType("Institution");
			spcmc.setContact(ic);
				
			supplierPharmCompanyManufacturerContactList.add(spcmc);
		}
		
		if(labListFind != null && labListFind.size()>0)
		{
			Contact lc = labListFind.get(0).getContact();
			
			spcmc = new SupplierPharmCompanyManufacturerContact();
			spcmc.setCode(labListFind.get(0).getLabCode());
			spcmc.setName(labListFind.get(0).getLabName());
			spcmc.setContactType("Lab");
			spcmc.setContact(lc);
				
			supplierPharmCompanyManufacturerContactList.add(spcmc);
		}
		
		if(userInfoListFind != null && userInfoListFind.size()>0)
		{
			for(UserInfo uic:userInfoListFind)
			{
				spcmc = new SupplierPharmCompanyManufacturerContact();
				spcmc.setCode(uic.getUserCode());
				spcmc.setContactType("User");
				spcmc.setContact(uic.getContact());
				
				supplierPharmCompanyManufacturerContactList.add(spcmc);
			}
		}
		
	}
	
    @Remove
	public void destroy(){	
		if (supplierPharmCompanyManufacturerContactList != null){
			supplierPharmCompanyManufacturerContactList = null;
		}
	}

}
