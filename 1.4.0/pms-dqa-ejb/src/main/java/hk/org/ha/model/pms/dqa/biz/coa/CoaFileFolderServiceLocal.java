package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;

import javax.ejb.Local;

@Local
public interface CoaFileFolderServiceLocal {
	
	void setUploadPath(String uploadPath);
	
	String upload(byte[] bytes, String fileName, CoaFileFolder coaFileFolderin);
	
	void doUpload(byte[] bytes, String fileName, CoaFileFolder coaFileFolderin);		
	
	CoaFileFolder retrieveCoaFileFolderByCoaFileFolderId(Long coaFileFolderId);
	
	void createCoaFileFolder(CoaFileFolder coaFileFolderin);
	
	void updateCoaFileFolder(CoaFileFolder coaFileFolderin);
	
	void deleteCoaFileFolder(CoaFileFolder coaFileFolderin);
	
	String getPhysicalFilePath();
	
	boolean isSuccess();
	
	void destroy();
}
