package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "SAMPLE_TEST")
@Customizer(AuditCustomizer.class)
@NamedQueries({
	@NamedQuery(name = "SampleTest.findAll", query = "select o from SampleTest o order by o.testCode")
})	
public class SampleTest extends VersionEntity {


	private static final long serialVersionUID = -2642665436143305072L;

	@Id
	@Column(name="TEST_CODE", length = 5, nullable=false)
	private String testCode;

	@Column(name="TEST_NAME", length = 40)
	private String testName;

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

}
