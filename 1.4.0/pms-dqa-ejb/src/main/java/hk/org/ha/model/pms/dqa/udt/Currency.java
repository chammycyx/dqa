package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Currency implements StringValuedEnum {
	
	HongKongDollar("HKD", "Hong Kong Dollar");
		
    private final String dataValue;
    private final String displayValue;
        
    Currency(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<Currency> {
		
		private static final long serialVersionUID = -960971210939952178L;

		@Override
    	public Class<Currency> getEnumClass() {
    		return Currency.class;
    	}
    }
}
