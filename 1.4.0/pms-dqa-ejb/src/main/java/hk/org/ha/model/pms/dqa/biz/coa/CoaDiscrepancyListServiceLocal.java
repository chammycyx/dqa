package hk.org.ha.model.pms.dqa.biz.coa;
import java.util.Collection;

import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancy;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.vo.coa.CoaDiscrepancyCriteria;

import javax.ejb.Local;

@Local
public interface CoaDiscrepancyListServiceLocal {
	
	void retrieveCoaDiscrepancyListForReportDiscrepancy(CoaDiscrepancyCriteria coaBatchCriteria);
	
	void retrieveCoaDiscrepancyListByCoaBatch(CoaBatch coaBatch);
	
	void retrieveCoaDiscrepancyListByCoaBatchId(Long coaBatchId);
	
	void updateCoaDiscrepancyList(Collection<CoaDiscrepancy> coaDiscrepancyList);
	
	void createCoaDiscrepancyList(CoaBatch coaBatch, Collection<CoaDiscrepancyKey> coaDiscrepancyKeyList);
	
	void destroy();
}
