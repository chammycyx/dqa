package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum SampleStatus implements StringValuedEnum {
	
	New("N", "New"),
	InProgress("I", "In Progress"),
	Completed("C", "Completed");
	
    private final String dataValue;
    private final String displayValue;
        
    SampleStatus(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<SampleStatus> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<SampleStatus> getEnumClass() {
    		return SampleStatus.class;
    	}
    }
}
