package hk.org.ha.model.pms.dqa.persistence;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@NamedQueries({
	@NamedQuery(name = "SupplierContact.findBySupplierCodeContactId", query = "select o from SupplierContact o  where o.supplier.supplierCode = :supplierCode and o.contact.contactId = :contactId  "),
	@NamedQuery(name = "SupplierContact.findBySupplierCode", query = "select o from SupplierContact o  where o.supplier.supplierCode = :supplierCode order by o.contact.firstName "),
	@NamedQuery(name = "SupplierContact.findByFirstNameLastNameEmail", query = "select o.contact.contactId from SupplierContact o where upper(o.contact.firstName) =:firstName " +
	"and upper(o.contact.lastName) =:lastName and upper(o.contact.email) =:email and o.supplier.supplierCode = :supplierCode ")
	
})
@Table(name = "SUPPLIER_CONTACT")
@Customizer(AuditCustomizer.class)
public class SupplierContact extends VersionEntity {

	private static final long serialVersionUID = 8097329575928290575L;

	@Id
	@Column(name = "SUPPLIER_CONTACT_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SupplierContactSeq")
	@SequenceGenerator(name = "SupplierContactSeq", sequenceName = "SEQ_SUPPLIER_CONTACT", initialValue=10000)
	private Long supplierContactId;

	@ManyToOne
	@JoinColumn(name="SUPPLIER_CODE")
	private Supplier supplier;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;	

	public SupplierContact() {
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setSupplierContactId(Long supplierContactId) {
		this.supplierContactId = supplierContactId;
	}

	public Long getSupplierContactId() {
		return supplierContactId;
	}


}
