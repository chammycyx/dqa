package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.sample.ChemTests;
import hk.org.ha.model.pms.dqa.udt.sample.DocStatus;
import hk.org.ha.model.pms.dqa.udt.sample.Specification;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "CHEMICAL_ANALYSIS")
@Customizer(AuditCustomizer.class)
public class ChemicalAnalysis extends VersionEntity {
	
	private static final long serialVersionUID = 4835243626779595541L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chemicalAnalysisSeq")
	@SequenceGenerator(name = "chemicalAnalysisSeq", sequenceName = "SEQ_CHEMICAL_ANALYSIS", initialValue=10000)
	@Id
	@Column(name="CHEMICAL_ANALYSIS_ID", nullable=false)
	private Long chemicalAnalysisId;

	@Converter(name = "ChemicalAnalysis.specification", converterClass = Specification.Converter.class )
	@Convert("ChemicalAnalysis.specification")
	@Column(name="SPECIFICATION", length = 3)
	private Specification specification;

	@Converter(name = "ChemicalAnalysis.moa", converterClass = DocStatus.Converter.class )
	@Convert("ChemicalAnalysis.moa")
	@Column(name="MOA", length = 4)
	private DocStatus moa;

	@Converter(name = "ChemicalAnalysis.fps", converterClass = DocStatus.Converter.class )
	@Convert("ChemicalAnalysis.fps")
	@Column(name="FPS", length = 4)
	private DocStatus fps;

	@Converter(name = "ChemicalAnalysis.refStd", converterClass = DocStatus.Converter.class )
	@Convert("ChemicalAnalysis.refStd")
	@Column(name="REF_STD", length = 4)
	private DocStatus refStd;

	@Converter(name = "ChemicalAnalysis.refStdReqFlag", converterClass = YesNoFlag.Converter.class )
	@Convert("ChemicalAnalysis.refStdReqFlag")
	@Column(name="REF_STD_REQ_FLAG", length = 1)
	private YesNoFlag refStdReqFlag;
	
	@Column(name="RETENTION_QTY")
	private Integer retentionQty;

	@Converter(name = "ChemicalAnalysis.test", converterClass = ChemTests.Converter.class )
	@Convert("ChemicalAnalysis.test")
	@Column(name="TEST", length = 3)
	private ChemTests test;
	
	@Column(name="TEST_DESC", length = 80)
	private String testDesc;
	
	@Column(name="SAVE_BEFORE_FLAG")
	private Boolean saveBeforeFlag;

	public Long getChemicalAnalysisId() {
		return chemicalAnalysisId;
	}

	public void setChemicalAnalysisId(Long chemicalAnalysisId) {
		this.chemicalAnalysisId = chemicalAnalysisId;
	}


	public void setSpecification(Specification specification) {
		this.specification = specification;
	}

	public Specification getSpecification() {
		return specification;
	}

	public DocStatus getMoa() {
		return moa;
	}

	public void setMoa(DocStatus moa) {
		this.moa = moa;
	}

	public DocStatus getFps() {
		return fps;
	}

	public void setFps(DocStatus fps) {
		this.fps = fps;
	}

	public DocStatus getRefStd() {
		return refStd;
	}

	public void setRefStd(DocStatus refStd) {
		this.refStd = refStd;
	}

	public void setRefStdReqFlag(YesNoFlag refStdReqFlag) {
		this.refStdReqFlag = refStdReqFlag;
	}

	public YesNoFlag getRefStdReqFlag() {
		return refStdReqFlag;
	}

	public Integer getRetentionQty() {
		return retentionQty;
	}

	public void setRetentionQty(Integer retentionQty) {
		this.retentionQty = retentionQty;
	}

	public void setTest(ChemTests test) {
		this.test = test;
	}

	public ChemTests getTest() {
		return test;
	}

	public void setTestDesc(String testDesc) {
		this.testDesc = testDesc;
	}

	public String getTestDesc() {
		return testDesc;
	}
	
	public Boolean getSaveBeforeFlag() {
		return saveBeforeFlag;
	}

	public void setSaveBeforeFlag(Boolean saveBeforeFlag) {
		this.saveBeforeFlag = saveBeforeFlag;
	}
}
