package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptCriteria;
import hk.org.ha.model.pms.dqa.vo.coa.CoaProcessRptData;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CoaProcessRptServiceLocal {
	
//	void retrieveCoaProcessList(String SupplierCode, String itemCode, ReportGroupByType groupType, Date startDate, Date endDate);
	
	void retrieveCoaProcessList(CoaProcessRptCriteria coaProcessReportCriteria);
	
	List<CoaProcessRptData> getCoaProcessRptList();
	
	String getReportName();

	String getReportParameters();

	String getRetrieveDate();
	
	void destroy();
}
