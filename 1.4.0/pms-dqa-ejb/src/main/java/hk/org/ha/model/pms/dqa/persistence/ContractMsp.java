package hk.org.ha.model.pms.dqa.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "CONTRACT_MSP")
@NamedQueries({
@NamedQuery(name = "ContractMsp.findByContractIdErpContractMspId", 
			query = "select o from ContractMsp o where o.contract.contractId = :contractId and o.erpContractMspId = :erpContractMspId"			
),
@NamedQuery(name = "ContractMsp.findByContractId", 
			query = "select o from ContractMsp o where o.contract.contractId = :contractId"			
)
})
@Customizer(AuditCustomizer.class)
public class ContractMsp extends VersionEntity {

	private static final long serialVersionUID = 6835791132925129311L;
	
	@Id    
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ContractMspSeq")
	@SequenceGenerator(name = "ContractMspSeq", sequenceName = "SEQ_CONTRACT_MSP", initialValue=10000)
	@Column(name="CONTRACT_MSP_ID")
	private Long contractMspId;
	
	@Column(name="COUNTRY_OF_ORIGIN", length=3)
	private String countryOfOrigin;

	@Column(name="START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
	
	@Column(name="END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
		
	@Converter(name = "ContractMsp.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("ContractMsp.recordStatus")
	@Column(name="RECORD_STATUS", length=1)
	private RecordStatus recordStatus;
	
	@Column(name="ERP_CONTRACT_MSP_ID")
	private Long erpContractMspId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTRACT_ID")
	private Contract contract;
	
	@ManyToOne
	@JoinColumn(name="MANUF_CODE", referencedColumnName="COMPANY_CODE")
	private Company manufacturer;
	
	@ManyToOne
	@JoinColumn(name="PHARM_COMPANY_CODE", referencedColumnName="COMPANY_CODE")
	private Company pharmCompany;
	
	public void setContractMspId(Long contractMspId) {
		this.contractMspId = contractMspId;
	}

	public Long getContractMspId() {
		return contractMspId;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setStartDate(Date startDate) {
		if (startDate != null){
			this.startDate = new Date(startDate.getTime());
		}
	}

	public Date getStartDate() {
		return (startDate != null) ? new Date(startDate.getTime()) : null;
	}

	public void setEndDate(Date endDate) {
		if (endDate != null){
			this.endDate = new Date(endDate.getTime());
		} else {
			this.endDate = null;
		}
	}

	public Date getEndDate() {
		return (endDate != null) ? new Date(endDate.getTime()) : null;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setErpContractMspId(Long erpContractMspId) {
		this.erpContractMspId = erpContractMspId;
	}

	public Long getErpContractMspId() {
		return erpContractMspId;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Contract getContract() {
		return contract;
	}

	public void setManufacturer(Company manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Company getManufacturer() {
		return manufacturer;
	}

	public void setPharmCompany(Company pharmCompany) {
		this.pharmCompany = pharmCompany;
	}

	public Company getPharmCompany() {
		return pharmCompany;
	}
}
