package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("pharmCompanyService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class PharmCompanyServiceBean implements PharmCompanyServiceLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	public Company  findPharmCompanyByCompanyCode(String companyCode){	
		logger.debug("findPharmCompanyByCompanyCode #0", companyCode);
								
		List<Company> pharmCompanys = em.createNamedQuery("Company.findByCompanyCodePharmCompanyFlag") 
										.setParameter("companyCode", companyCode)
										.setParameter("pharmCompanyFlag", YesNoFlag.Yes)
										.getResultList();
		
		if (pharmCompanys!= null && pharmCompanys.size()>0)
		{
			return pharmCompanys.get(0);
		}
		else
		{
			return null;
		}
	}
		
	@Remove
	public void destroy(){
		
	}
}
