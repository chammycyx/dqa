package hk.org.ha.model.pms.dqa.biz.coa;
import javax.ejb.Local;

@Local
public interface CoaChecklistKeyListServiceLocal {

	void retrieveCoaChecklistKeyList();
	
	void destroy();
}
