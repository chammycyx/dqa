package hk.org.ha.model.pms.dqa.biz;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("fileItemManager")
@MeasureCalls
public class FileItemManagerBean implements FileItemManagerLocal {
	@Logger
	private Log logger;

	public Boolean uploadFile(File fileIn, byte[] bytes){
		FileOutputStream fos = null;
		Boolean success = false;
		
		logger.debug("FileItemService upload file path: #0 ", fileIn.getPath() );
		try {
			fos = new FileOutputStream(fileIn);
			fos.write(bytes);
			fos.close();
			success = true;
		} catch (FileNotFoundException e1) {
			logger.error("File not found exception: #0",e1);
		}catch (IOException e2) {
			logger.error("File IO exception: #0",e2);
		} finally {
			if (fos !=null){
				try {
					fos.close();
				} catch (IOException e) {
					logger.error("File operation exception: #0",e);
				}
			}
		}
		return success;
	}
	
	public Boolean deleteFile(File fileIn){
		Boolean success = false;
		if(fileIn.exists())
		{
			logger.debug("FileItemService deleteFile : #0", fileIn.getPath());
			try {
				success = fileIn.delete();
				
				if (success){
					logger.debug("File delete success. ");
				}
			}catch (Exception e){
				logger.error("File operation exception: #0", e);	
			}
		}else{
			logger.error("File not exist : #0 ",fileIn.getPath());
		}
		return success;
	}
}
