package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchManagerBean.CoaBatchResult;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;

import javax.ejb.Local;

@Local
public interface CoaBatchManagerLocal {
	
	CoaBatchResult createCoaBatchForEmail( CoaBatch coaBatchIn );
	
	CoaBatchResult createCoaBatch(CoaBatch coaBatchIn);	
	
	CoaBatch retrieveCoaBatchByBatchNumCoaItemId( String batchNum, Long coaItemId );
	
	void updateCoaBatchForEmailLog( CoaBatch coaBatchIn );
}
