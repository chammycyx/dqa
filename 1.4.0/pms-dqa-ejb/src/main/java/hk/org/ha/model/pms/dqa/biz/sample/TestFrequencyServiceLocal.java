package hk.org.ha.model.pms.dqa.biz.sample;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;

import javax.ejb.Local;

@Local
public interface TestFrequencyServiceLocal {
	
	void retrieveTestFrequencyByTestFrequency(TestFrequency testFrequencyIn);
	
	void addTestFrequency();
	
	void createTestFrequency();
	
	void updateTestFrequency();
	
	void deleteTestFrequency(TestFrequency testFrequencyIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();
}
