package hk.org.ha.model.pms.dqa.biz;
import java.io.File;

import javax.ejb.Local;

@Local
public interface FileItemManagerLocal {
	
	Boolean uploadFile(File fileIn, byte[] bytes);
	
	Boolean deleteFile(File fileIn);	
}
