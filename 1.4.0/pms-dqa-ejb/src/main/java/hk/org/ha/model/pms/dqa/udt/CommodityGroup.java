package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CommodityGroup implements StringValuedEnum {
	
	Drug("D", "Drug"),
	Dressing("G", "Dressing"),
	Consumable("S", "Consumable");	
	
    private final String dataValue;
    private final String displayValue;
        
    CommodityGroup(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<CommodityGroup> {

		private static final long serialVersionUID = -4844612573024106630L;

		@Override
    	public Class<CommodityGroup> getEnumClass() {
    		return CommodityGroup.class;
    	}
    }
}
