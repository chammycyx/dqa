package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.biz.ContractServiceLocal;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateless
@Restrict("#{identity.loggedIn}")
@Name("sampleTestScheduleValidateService")
@RemoteDestination
@MeasureCalls
public class SampleTestScheduleValidateServiceBean implements SampleTestScheduleValidateServiceLocal {
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In(create=true)
	private SampleItemServiceLocal sampleItemService;
	
	@In(create=true)
	private ContractServiceLocal contractService;
	
	@In(create=true)
	private SampleTestScheduleListServiceLocal sampleTestScheduleListService;
	
	private boolean success;
	
	private String errorCode = null;

	private boolean isValidSampleItem(String itemCode, String testCode){
		logger.debug("......isValidSampleItem : #1 #0", itemCode, testCode);
		
		//check valid item
		SampleItem sampleItemDB = sampleItemService.retrieveSampleItemForSampleTestValidation(itemCode, RecordStatus.Active);
		
		if (sampleItemDB != null){
			
			for (ExclusionTest ex : sampleItemDB.getExclusionTestList()){
				if (testCode.equals(ex.getSampleTest().getTestCode())){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	private boolean isSampleTestScheduleValid(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("isSampleTestScheduleValid :#0", sampleTestScheduleIn.getScheduleId());
		SampleTestSchedule sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleIn);
		SampleItem sampleItemIn = sampleTestScheduleGetLazyIn.getSampleItem();
		SampleTest sampleTest = sampleTestScheduleGetLazyIn.getSampleTest();
		DmDrug dmDrug = null;
		String drugItemCode=null;
		if(sampleItemIn!=null)
		{
			dmDrug = sampleItemIn.getDmDrug();
		    drugItemCode = sampleItemIn.getItemCode();
		}
		
		Contract contractIn = sampleTestScheduleGetLazyIn.getContract();
		
		if (sampleItemIn == null || sampleTest==null || contractIn == null){
			errorCode = "0009";
			return false;
		}
		else if(drugItemCode ==null || dmDrug == null)
		{
			errorCode = "0009";
			return false;
		}
		else if(sampleItemIn.getRiskLevel().getProceedSampleTestFlag().equals(YesNoFlag.No))
		{
			errorCode = "0075";
			return false;
		}
		else if(contractIn.getPackPrice()==null || contractIn.getPackSize()==null)
		{
			errorCode = "0009";
			return false;	
		}
		else if(contractIn.getSupplier()==null || contractIn.getManufacturer()==null || contractIn.getPharmCompany()==null)
		{
			errorCode = "0009";
			return false;
		}
		
		boolean validItem = isValidSampleItem(drugItemCode, sampleTest.getTestCode());
		logger.debug("......is validItem #0", validItem);
		
		if (validItem ){
			logger.debug("drug order type #0", dmDrug.getDmProcureInfo().getOrderType());
		
			if (!dmDrug.getDmProcureInfo().getOrderType().equals(OrderType.DirectPurchase.getDataValue())){
				ModuleType moduleType= ModuleType.All;
				
				/*if(isUpdate){
					//moduleType = ModuleType.SampleTest; 
					moduleType = ModuleType.All;
				}else {
					moduleType = ModuleType.All;
				}*/
	
				Contract liveContract = contractService.retrieveContractForSampleTestValidation(contractIn.getContractNum(), contractIn.getContractSuffix(), OrderType.findByDataValue(dmDrug.getDmProcureInfo().getOrderType()), sampleItemIn.getItemCode(), moduleType);
				logger.debug("......is live Contract #0", (liveContract!=null));
				if (liveContract !=null){
					return true;
				}else { errorCode="0031"; }
			}else {
				logger.debug("Valid D Type schedule");
				return true;
			}	
		}else { errorCode = "0030"; }					
		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private SampleTestSchedule retrieveSampleTestScheduleByItemCodeTestCodeScheduleMonthRecordStatus(String itemCode, String testCode, Date scheduleMonth, RecordStatus recordStatus){
		logger.debug("retrieveSampleTestScheduleByItemCodeTestCodeScheduleMonthRecordStatus");
		
		List<SampleTestSchedule> list = em.createNamedQuery("SampleTestSchedule.findByItemCodeTestCodeScheduleMonthRecordStatus")
		.setParameter("recordStatus", recordStatus )
		.setParameter("itemCode", itemCode.trim())
		.setParameter("testCode", testCode.trim())
		.setParameter("scheduleMonth", scheduleMonth, TemporalType.DATE)
		.setHint(QueryHints.FETCH, "o.sampleTest")
		.setHint(QueryHints.FETCH, "o.contract")
		.setHint(QueryHints.FETCH, "o.contract.supplier")
		.setHint(QueryHints.FETCH, "o.contract.manufacturer")
		.setHint(QueryHints.FETCH, "o.contract.pharmCompany")
		.setHint(QueryHints.FETCH, "o.sampleItem.riskLevel")
		.setHint(QueryHints.LEFT_FETCH, "o.institution")
		.getResultList();
	
		if (list.size() >0){
			return  getLazySampleTestSchedule(list.get(0));
		}	
		return null;
	}
	
	public void validateSampleTestScheduleForCreate(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("validateSampleTestScheduleForCreate");
		success = false;

		SampleTestSchedule sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleIn);
		
		//boolean mandatoryCheck = isSampleTestScheduleValid(sampleTestScheduleIn, false);
		boolean mandatoryCheck = isSampleTestScheduleValid(sampleTestScheduleGetLazyIn);
		
		if (mandatoryCheck){
			
			SampleItem sampleItemIn = sampleTestScheduleGetLazyIn.getSampleItem();
			String itemCode = sampleItemIn.getItemCode();
			String testCode = sampleTestScheduleGetLazyIn.getSampleTest().getTestCode();
			
			// check duplicate schedule
			SampleTestSchedule scheduleDB = retrieveSampleTestScheduleByItemCodeTestCodeScheduleMonthRecordStatus(itemCode, testCode, sampleTestScheduleGetLazyIn.getScheduleMonth(), RecordStatus.Active);
			logger.debug("......Record not exist : #0", (scheduleDB != null));

			if (scheduleDB == null){
				// check no outstanding schedule
				List<SampleTestSchedule> scheduleList = sampleTestScheduleListService.retrieveSampleTestScheduleListForOutStanding(itemCode, testCode);
				logger.debug("......outstanding schedule size: #0", scheduleList.size());

				if (scheduleList.size() == 0){

					success = true;
				}else { errorCode="0007"; }
			}else { errorCode="0007"; }
		}	
		
		logger.debug("create validation :: #0 ", success);
	}
	
	
	/**
	 * for SampleTestScheduleServiceBean:
	 *  updateSampleTestSchedule
	 * 	updateSampleTestScheduleForSchedConfirm 
	 * 
	 * */ 
	public void validateSampleTestScheduleForUpdate(SampleTestSchedule sampleTestScheduleIn){
		logger.debug("validateSampleTestScheduleForUpdate" );
		
		SampleTestSchedule sampleTestScheduleGetLazyIn = getLazySampleTestSchedule(sampleTestScheduleIn);
		//success = isSampleTestScheduleValid(sampleTestScheduleIn, false);
		success = isSampleTestScheduleValid(sampleTestScheduleGetLazyIn);
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	public SampleTestSchedule getLazySampleTestSchedule(SampleTestSchedule sampleTestScheduleLazy){
		// lazy
		sampleTestScheduleLazy.getPayment();
		sampleTestScheduleLazy.getContract();
		if(sampleTestScheduleLazy.getContract()!=null)
		{
			sampleTestScheduleLazy.getContract().getSupplier();
			sampleTestScheduleLazy.getContract().getManufacturer();
			sampleTestScheduleLazy.getContract().getPharmCompany();
			
			if(sampleTestScheduleLazy.getContract().getSupplier()!=null)
			{
				sampleTestScheduleLazy.getContract().getSupplier().getContact();
			}
			
			if(sampleTestScheduleLazy.getContract().getManufacturer()!=null)
			{
				sampleTestScheduleLazy.getContract().getManufacturer().getContact();						
			}
			
			if(sampleTestScheduleLazy.getContract().getPharmCompany()!=null)
			{
				sampleTestScheduleLazy.getContract().getPharmCompany().getContact();						
			}
		}
		sampleTestScheduleLazy.getInstitution();
		sampleTestScheduleLazy.getTestFrequency();
		sampleTestScheduleLazy.getLab();
		sampleTestScheduleLazy.getMicroBioTest();
		sampleTestScheduleLazy.getChemicalAnalysis();
		
		return sampleTestScheduleLazy;
	}
	
	@Remove
	public void destory(){
		
	}
}
