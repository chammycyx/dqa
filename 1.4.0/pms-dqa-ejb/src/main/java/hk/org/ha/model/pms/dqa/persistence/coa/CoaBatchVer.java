package hk.org.ha.model.pms.dqa.persistence.coa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COA_BATCH_VER")
@NamedQueries( {
	@NamedQuery(name = "CoaBatchVer.findByCoaBatchDiscrepancyStatus", query = "select o from CoaBatchVer o where o.coaBatch.coaBatchVer.coaBatchVerId <> o.coaBatchVerId and o.coaBatch = :coaBatch and o.discrepancyStatus <> :discrepancyStatus and o.recordStatus = :recordStatus order by o.createDate asc"),
	@NamedQuery(name = "CoaBatchVer.findByCoaBatchId", query = "select o from CoaBatchVer o where o.coaBatch.coaBatchId = :coaBatchId and o.recordStatus = :recordStatus order by o.createDate asc")
})
public class CoaBatchVer extends VersionEntity {

	private static final long serialVersionUID = 5697629163623310163L;

	@Id
	@Column(name="COA_BATCH_VER_ID",nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaBatchVerSeq")
	@SequenceGenerator(name="coaBatchVerSeq", sequenceName="SEQ_COA_BATCH_VER", initialValue=10000)
	private Long coaBatchVerId;

	@Converter(name = "CoaBatchVer.coaStatus", converterClass = CoaStatus.Converter.class)
	@Convert("CoaBatchVer.coaStatus")
	@Column(name="COA_STATUS", length=1, nullable=false)
	private CoaStatus coaStatus;

	@Converter(name = "CoaBatchVer.discrepancyStatus", converterClass = DiscrepancyStatus.Converter.class)
	@Convert("CoaBatchVer.discrepancyStatus")
	@Column(name="DISCREPANCY_STATUS", length=1, nullable=false)
	private DiscrepancyStatus discrepancyStatus;

	@Column(name="REMINDER_NUM", nullable = true)
	private Integer reminderNum;

	@Converter(name = "CoaBatchVer.recordStatus", converterClass = RecordStatus.Converter.class)
	@Convert("CoaBatchVer.recordStatus")
	@Column(name="RECORD_STATUS", length=1, nullable=false)
	private RecordStatus recordStatus;
	
	@ManyToOne
	@JoinColumn(name="COA_BATCH_ID")
	private CoaBatch coaBatch;

	@ManyToOne
	@JoinColumn(name="EMAIL_LOG_ID")
	private EmailLog emailLog;

	public void setCoaBatchVerId(Long coaBatchVerId) {
		this.coaBatchVerId = coaBatchVerId;
	}

	public Long getCoaBatchVerId() {
		return coaBatchVerId;
	}

	public void setCoaBatch(CoaBatch coaBatch) {
		this.coaBatch = coaBatch;
	}

	public CoaBatch getCoaBatch() {
		return coaBatch;
	}

	public void setEmailLog(EmailLog emailLog) {
		this.emailLog = emailLog;
	}

	public EmailLog getEmailLog() {
		return emailLog;
	}

	public void setCoaStatus(CoaStatus coaStatus) {
		this.coaStatus = coaStatus;
	}

	public CoaStatus getCoaStatus() {
		return coaStatus;
	}

	public void setDiscrepancyStatus(DiscrepancyStatus discrepancyStatus) {
		this.discrepancyStatus = discrepancyStatus;
	}

	public DiscrepancyStatus getDiscrepancyStatus() {
		return discrepancyStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setReminderNum(Integer reminderNum) {
		this.reminderNum = reminderNum;
	}

	public Integer getReminderNum() {
		return reminderNum;
	}

}
