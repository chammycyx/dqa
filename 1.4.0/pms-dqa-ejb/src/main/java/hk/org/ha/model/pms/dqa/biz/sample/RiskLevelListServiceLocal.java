package hk.org.ha.model.pms.dqa.biz.sample;

import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import javax.ejb.Local;

@Local
public interface RiskLevelListServiceLocal {
	
	void retrieveRiskLevelListByRecordStatus(RecordStatus recordStatus);
	
	void destroy();
	
}
