package hk.org.ha.model.pms.dqa.biz;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.ModuleType;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("contractManager")
@MeasureCalls
public class ContractManagerBean implements ContractManagerLocal{
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private ApplicationProp applicationProp;
	
	private static final String DIRECT_PURCHASE="DP";
	private static final String UNCHECKED ="unchecked";
	private static final String O_SUPPLIER = "o.supplier";
	private static final String CONTRACT_NUM = "contractNum";
	private static final String ITEM_CODE = "itemCode";
	private static final String MODULE_TYPE = "moduleType";
	private static final String SUPPLIER_CODE = "supplierCode";
	
	private Integer expireMonthBuffer;	

	public void setExpireMonthBuffer(Integer expireMonthBuffer) {
		this.expireMonthBuffer = expireMonthBuffer;
	}
	
	public Contract retrieveContractForCoaEmail( String contractNum, String itemCode, String supplierCode ) {
		logger.debug( "retrieveContractForCoaEmail #0 #1 #2", contractNum, itemCode, supplierCode );
		if( DIRECT_PURCHASE.equals(contractNum) ) {
			logger.debug("DirectPurchase #0", contractNum);
			return retrieveDirectPurchaseContract( itemCode, supplierCode, ModuleType.COA );
		} else {
			logger.debug("Non-DirectPurchase #0", contractNum);
			return retrieveContract( contractNum, itemCode, supplierCode, ModuleType.COA ); 
		}		
	}
	
	@SuppressWarnings(UNCHECKED)
	public Contract retrieveDirectPurchaseContract( String itemCode, String supplierCode, ModuleType moduleType ) {
		logger.debug( "retrieveDirectPurchaseContract #0 #1", itemCode, supplierCode );
		List<Contract> contractList = em.createNamedQuery( "Contract.findByItemCodeSupplierCodeModuleType" )									    
									    .setParameter(ITEM_CODE, itemCode.trim())
									    .setParameter("supplierCode", supplierCode.trim())
									    .setParameter(MODULE_TYPE, moduleType)
									    .setHint(QueryHints.FETCH, "o.supplier")
									    .getResultList();
		if( contractList.size() > 0 ) {
			return contractList.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	private Contract retrieveContract( String contractNum, String itemCode, String supplierCode, ModuleType moduleType ) {
		logger.debug("retrieveContract contractNum:#0, itemCode:#1, supplierCode:#2, moduleType:#3", contractNum, itemCode, supplierCode, moduleType);
		List<Contract> contractList = em.createQuery("select o from Contract o " +
				"where o.contractNum =:contractNum and o.itemCode = :itemCode " +
				"and o.supplier.supplierCode = :supplierCode " +
				"and o.moduleType = :moduleType")
		.setHint(QueryHints.FETCH, O_SUPPLIER)
		.setParameter(CONTRACT_NUM, contractNum.trim())
		.setParameter(ITEM_CODE, itemCode.trim())
		.setParameter(SUPPLIER_CODE, supplierCode)
		.setParameter(MODULE_TYPE, moduleType)
		.getResultList();
		Contract c;
		if (contractList.size()>0){
			c = contractList.get(0);
			c.getSupplier();
			return c;
		}
		return null;
	}
	
	public boolean isExpiredForCoaEmail( Contract c ) {
		if (applicationProp.isEnableCheckLiveContract()) {
			logger.debug("check contract live - contractEndDate:#0 ",c.getEndDate());
			Calendar contractEndDate = Calendar.getInstance();
			Calendar today = Calendar.getInstance();
			
			contractEndDate.setTime( new Date(c.getEndDate().getTime()) );	
			contractEndDate.add(Calendar.MONTH, expireMonthBuffer);
			
			if(today.after(contractEndDate)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;			
		}
	}
	
	@SuppressWarnings(UNCHECKED)
	public Contract retrieveContractForCoaItemMaint(String contractNum, String supplierCode, String companyCode, String itemCode, ModuleType moduleType){
		logger.info("retrieveContractForCoaItemMaint");
		Contract ct;

		List<Contract> contractList =  em.createNamedQuery("Contract.findForCoaItemMaint" )
			.setParameter(CONTRACT_NUM, (contractNum==null)?null:contractNum.trim())
			.setParameter("supplierCode", supplierCode)
			.setParameter("companyCode", companyCode)
			.setParameter(ITEM_CODE, itemCode.trim())
			.setParameter(MODULE_TYPE, moduleType)
			.setHint(QueryHints.FETCH, O_SUPPLIER)
			.setHint(QueryHints.FETCH, "o.supplier.contact")
			.getResultList();
			
		if (contractList.size()>0){
			ct = contractList.get(0);
			ct.getSupplier();
		}else {
			ct= null;
		}
		return ct;
	}
}
