package hk.org.ha.model.pms.dqa.vo.sample;


import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SampleTestEmailEnquiryCriteria {
	
	private ScheduleStatus scheduleStatus;
	private String scheduleNum;
	private String itemCode;
	private String supplierCode;
	private String manufCode;
	private String pharmCompanyCode;
	private String institutionCode;
	private String labCode;
	private String letterCode;
	private ScheduleStatus sendAtScheduleStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromSendDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date toSendDate;

	public ScheduleStatus getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(ScheduleStatus scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	public String getScheduleNum() {
		return scheduleNum;
	}

	public void setScheduleNum(String scheduleNum) {
		this.scheduleNum = scheduleNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getManufCode() {
		return manufCode;
	}

	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}

	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getLetterCode() {
		return letterCode;
	}

	public void setLetterCode(String letterCode) {
		this.letterCode = letterCode;
	}

	public ScheduleStatus getSendAtScheduleStatus() {
		return sendAtScheduleStatus;
	}

	public void setSendAtScheduleStatus(ScheduleStatus sendAtScheduleStatus) {
		this.sendAtScheduleStatus = sendAtScheduleStatus;
	}

	public Date getFromSendDate() {
		return (fromSendDate != null ) ? new Date(fromSendDate.getTime()) : null;
	}

	public void setFromSendDate(Date fromSendDate) {
		if (fromSendDate != null) {
			this.fromSendDate = new Date(fromSendDate.getTime());
		}
	}

	public Date getToSendDate() {
		return (toSendDate != null) ? new Date(toSendDate.getTime()) : null;
	}

	public void setToSendDate(Date toSendDate) {
		if (toSendDate != null){
			this.toSendDate = new Date(toSendDate.getTime());
		}
	}
}
