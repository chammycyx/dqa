package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.util.PropertiesHelper;

import java.io.IOException;
import java.util.Properties;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("applicationProp")
@Scope(ScopeType.APPLICATION)
public class ApplicationProp {
	
	private static final String APPLICATION_PROPERTIES = "application.properties";
	private static final long RELOAD_INTERVAL = 120000L;
	private static final String DQA_CHECK_LIVE_CONTRACT = "dqa.check-live-contract";
	private static final String SAMPLE_LETTER_TO_SUPPLIER_SEND_DATE_BUFFER = "sample.letter-to-supplier.send-date-buffer";
	private static final String SAMPLE_MEMO_TO_HOSP_SEND_DATE_BUFFER = "sample.memo-to-hosp.send-date-buffer";
	private static final String SAMPLE_RELEASE_RETENTION_SAMPLE_SEND_DATE_BUFFER = "sample.release-retention-sample.send-date-buffer";
	
	public Properties getProperties() {
		try {
			return PropertiesHelper.getProperties(APPLICATION_PROPERTIES, Thread.currentThread().getContextClassLoader(), RELOAD_INTERVAL);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean isEnableCheckLiveContract() {
		return "true".equals(getProperties().getProperty(DQA_CHECK_LIVE_CONTRACT, "false"));
	}
	
	public int getLetterToSuppSendDateBuffer() {
		try {
			return Integer.valueOf(getProperties().getProperty(SAMPLE_LETTER_TO_SUPPLIER_SEND_DATE_BUFFER, "0")).intValue();
		} catch (NumberFormatException e) {			
			return 0;
		}
	}
	
	public int getMemoToHospSendDateBuffer() {
		try {
			return Integer.valueOf(getProperties().getProperty(SAMPLE_MEMO_TO_HOSP_SEND_DATE_BUFFER, "0")).intValue();
		} catch (NumberFormatException e) {			
			return 0;
		}
	}
	
	public int getReleaseRetentionSampleSendDateBuffer() {
		try {
			return Integer.valueOf(getProperties().getProperty(SAMPLE_RELEASE_RETENTION_SAMPLE_SEND_DATE_BUFFER, "0")).intValue();
		} catch (NumberFormatException e) {			
			return 0;
		}
	}
}

