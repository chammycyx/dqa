package hk.org.ha.model.pms.dqa.persistence.delaydelivery;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;
@Entity
@Table(name = "DLD_RPT_FORM_CONTACT")
@Customizer(AuditCustomizer.class)
public class DelayDeliveryRptFormContact extends VersionEntity  {

	private static final long serialVersionUID = -2058074306302359743L;
	
	@Id
	@Column(name="DLD_RPT_FORM_CONTACT_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "delayDeliveryRptFormContactSeq")
	@SequenceGenerator(name="delayDeliveryRptFormContactSeq", sequenceName="SEQ_DLD_RPT_FORM_CONTACT", initialValue=10000)
	private Long DelayDeliveryRptFormContactId;
	
	@ManyToOne
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;
	
	@ManyToOne
	@JoinColumn(name="REF_NUM")
	private DelayDeliveryRptForm delayDeliveryRptForm;
	
	@Column(name = "PRIORITY", nullable = false)	
	private Integer priority;
	
	public DelayDeliveryRptForm getDelayDeliveryRptForm() {
		return delayDeliveryRptForm;
	}
	public void setDelayDeliveryRptForm(DelayDeliveryRptForm delayDeliveryRptForm) {
		this.delayDeliveryRptForm = delayDeliveryRptForm;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Long getDelayDeliveryRptFormContactId() {
		return DelayDeliveryRptFormContactId;
	}
	public void setDelayDeliveryRptFormContactId(Long delayDeliveryRptFormContactId) {
		DelayDeliveryRptFormContactId = delayDeliveryRptFormContactId;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
	
}
