package hk.org.ha.model.pms.dqa.persistence.coa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.udt.coa.InEmailRemarkType;
import hk.org.ha.model.pms.dqa.udt.coa.ValidateResultFlag;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "COA_IN_EMAIL")
@NamedQueries( {
	@NamedQuery(name = "CoaInEmail.findForInEmailEnq", query = "select o from CoaInEmail o" +
																" where (:fromReceiveDate is null or o.createDate >= :fromReceiveDate)" +
																" and (:toReceiveDate is null or o.createDate < :toReceiveDate)" +
																" and (:resultFlagValue = 'A'" +
																" or o.resultFlag = :resultFlag )" +
																" order by o.createDate desc")
})
@Customizer(AuditCustomizer.class)
public class CoaInEmail extends VersionEntity {

	private static final long serialVersionUID = -6064633881940758721L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "coaInEmailSeq")
	@SequenceGenerator(name="coaInEmailSeq", sequenceName="SEQ_COA_IN_EMAIL", initialValue=10000)
	@Column(name="COA_IN_EMAIL_ID")
	private Long coaInEmailId;

	@Converter(name = "CoaInEmailAtt.resultFlag", converterClass = ValidateResultFlag.Converter.class )
	@Convert("CoaInEmailAtt.resultFlag")
	@Column(name="RESULT_FLAG", length=1)
	private ValidateResultFlag resultFlag;
	
	@Converter(name = "CoaInEmail.remarkType", converterClass = InEmailRemarkType.Converter.class )
	@Convert("CoaInEmail.remarkType")
	@Column(name="REMARK_TYPE", length=1)
	private InEmailRemarkType remarkType;
	
	@OneToOne
	@JoinColumn(name="EMAIL_ID")
	private EmailLog emailLog;
	
	@OneToMany(mappedBy = "coaInEmail")
	private List<CoaInEmailAtt> coaInEmailAttList;
	
	@OneToMany(mappedBy = "coaInEmail")
	private List<CoaReplyEmail> coaReplyEmailList;
	

	public void setCoaInEmailId(Long coaInEmailId) {
		this.coaInEmailId = coaInEmailId;
	}

	public Long getCoaInEmailId() {
		return coaInEmailId;
	}

	public void setResultFlag(ValidateResultFlag resultFlag) {
		this.resultFlag = resultFlag;
	}

	public ValidateResultFlag getResultFlag() {
		return resultFlag;
	}

	public void setRemarkType(InEmailRemarkType remarkType) {
		this.remarkType = remarkType;
	}

	public InEmailRemarkType getRemarkType() {
		return remarkType;
	}

	public void setEmailLog(EmailLog emailLog) {
		this.emailLog = emailLog;
	}

	public EmailLog getEmailLog() {
		return emailLog;
	}

	public void setCoaInEmailAttList(List<CoaInEmailAtt> coaInEmailAttList) {
		this.coaInEmailAttList = coaInEmailAttList;
	}

	public List<CoaInEmailAtt> getCoaInEmailAttList() {
		return coaInEmailAttList;
	}

	public void setCoaReplyEmailList(List<CoaReplyEmail> coaReplyEmailList) {
		this.coaReplyEmailList = coaReplyEmailList;
	}

	public List<CoaReplyEmail> getCoaReplyEmailList() {
		return coaReplyEmailList;
	}
	
}
