package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import java.util.List;

import javax.ejb.Local;

@Local
public interface PharmProblemNewServiceLocal {
	
	void updateNewPharmProblemWithQaProblem(PharmProblem pharmProblemIn, QaProblem qaProblemIn, String caseType, List<QaBatchNum> qaBatchNumListIn);

	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
