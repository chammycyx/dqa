package hk.org.ha.model.pms.dqa.biz.sample;

import javax.ejb.Local;

@Local
public interface PcuListForPopupServiceLocal {
	
	void retrievePcuListForPopup();
	
	void destroy();
}
