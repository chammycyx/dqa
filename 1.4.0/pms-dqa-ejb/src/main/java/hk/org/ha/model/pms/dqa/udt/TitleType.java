package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum TitleType implements StringValuedEnum {
	
	NIL("-", "-"),
	Mr("MR", "Mr"),
	Miss("MI", "Miss"),
	Ms("MS", "Ms"),
	Mrs("MRS", "Mrs");	
	
    private final String dataValue;
    private final String displayValue;
        
    TitleType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<TitleType> {

		private static final long serialVersionUID = -8293816105032872094L;

		@Override
    	public Class<TitleType> getEnumClass() {
    		return TitleType.class;
    	}
    }
}
