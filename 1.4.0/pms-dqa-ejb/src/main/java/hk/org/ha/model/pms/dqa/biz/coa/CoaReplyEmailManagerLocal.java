package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.EmailLog;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaInEmail;
import hk.org.ha.model.pms.dqa.udt.coa.ReplyEmailTo;

import javax.ejb.Local;

@Local
public interface CoaReplyEmailManagerLocal {
	void createCoaReplyEmail( CoaInEmail coaInEmail, EmailLog emailLog, ReplyEmailTo replyEmailTo );
}
