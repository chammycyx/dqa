package hk.org.ha.model.pms.dqa.persistence.sample;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "PAYMENT")
@Customizer(AuditCustomizer.class)
public class Payment extends VersionEntity {

	private static final long serialVersionUID = 1433264623863270681L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "paymentSeq")
	@SequenceGenerator(name = "paymentSeq", sequenceName = "SEQ_PAYMENT", initialValue=10000)
	@Id
	@Column(name="PAYMENT_ID", nullable=false)
	private Long paymentId;

	@Column(name="CHEQUE_AMOUNT")
	private Double chequeAmount;

	@Column(name="CHEQUE_NUM" , length = 15)
	private String chequeNum;

	@Column(name="CHEQUE_RECEIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date chequeReceiveDate;
	
	@Column(name="CHEQUE_SEND_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date chequeSendDate;
	
	@Column(name="PO_NUM", length = 15)
	private String poNum;
	
	@Column(name="PR_ISSUE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date prIssueDate;
	
	@Column(name="INV_RECEIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date invReceiveDate;

	@Column(name="INV_REF_NUM", length = 15)
	private String invRefNum;
	
	@Column(name="INV_SEND_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date invSendDate;
	
	@Column(name="REMARK", length = 80)
	private String remark;
	
	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}



	public Double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(Double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public String getChequeNum() {
		return chequeNum;
	}

	public void setChequeNum(String chequeNum) {
		this.chequeNum = chequeNum;
	}

	public void setChequeReceiveDate(Date chequeReceiveDate) {
		this.chequeReceiveDate = chequeReceiveDate;
	}

	public Date getChequeReceiveDate() {
		return chequeReceiveDate;
	}

	public Date getChequeSendDate() {
		return chequeSendDate;
	}

	public void setChequeSendDate(Date chequeSendDate) {
		this.chequeSendDate = chequeSendDate;
	}

	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}

	public String getPoNum() {
		return poNum;
	}

	public Date getPrIssueDate() {
		return prIssueDate;
	}

	public void setPrIssueDate(Date prIssueDate) {
		this.prIssueDate = prIssueDate;
	}

	public Date getInvReceiveDate() {
		return invReceiveDate;
	}

	public void setInvReceiveDate(Date invReceiveDate) {
		this.invReceiveDate = invReceiveDate;
	}

	public String getInvRefNum() {
		return invRefNum;
	}

	public void setInvRefNum(String invRefNum) {
		this.invRefNum = invRefNum;
	}

	public Date getInvSendDate() {
		return invSendDate;
	}

	public void setInvSendDate(Date invSendDate) {
		this.invSendDate = invSendDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
