package hk.org.ha.model.pms.dqa.biz.coa;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;

import javax.ejb.Local;

@Local
public interface CoaBatchVerListServiceLocal {
	
	void retrieveCoaBatchVerListByCoaBatch( CoaBatch coaBatch );
	
	void retrieveCoaBatchVerListByCoaBatchId( Long coaBatchId );
	
	void destroy();

}
