package hk.org.ha.model.pms.dqa.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import java.util.List;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;


@Stateful
@Scope(ScopeType.SESSION)
@Name("supplierCodeListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SupplierCodeListServiceBean implements SupplierCodeListServiceLocal {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;

	
	
	@Out(required = false)
	private List<String> supplierCodeList;
	

	@SuppressWarnings("unchecked")
	public void retrieveSupplierCodeList() {
    	logger.info("retrieveSupplierCodeList");
		supplierCodeList = em.createNamedQuery("Supplier.findAllSupplierCode")
		.getResultList();
	}
	
	
	@Remove
	public void destroy(){
	
		if(supplierCodeList!=null) {
			supplierCodeList = null;
		}
	}

}
