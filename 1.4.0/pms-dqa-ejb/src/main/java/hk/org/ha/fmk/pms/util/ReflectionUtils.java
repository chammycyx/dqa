package hk.org.ha.fmk.pms.util;

import java.lang.reflect.Method;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

public final class ReflectionUtils {

	private ReflectionUtils() {}
	
    public static void invoke(Object obj, String path) {
    	invoke(obj, nextTokenPos(path, 0), path);
    }
    
	private static void invoke(Object obj, int pos, String path) {

		if (obj instanceof Collection<?>) {
			
			Collection<?> collection = (Collection<?>) obj;
			for (Object child: collection) {
				invoke(child, pos, path);
			}
		} else if (obj != null && pos != -1) {
			
			try {
				int newPos = nextTokenPos(path, pos);
	    		String pathToken = newPos == -1 ? path.substring(pos) : path.substring(pos, newPos - 1);
    			Method method = obj.getClass().getMethod("get" + StringUtils.capitalize(pathToken));
    			Object child = method.invoke(obj);
    			invoke(child, newPos, path);
    		} catch (Exception ex) {
    		}
		}
    }
    
    private static int nextTokenPos(String path, int startPos) {
    	int nextPos = path.indexOf(".", startPos);
    	if (nextPos != -1) {
    		nextPos++;
    	}
    	return nextPos;
    }	
}
