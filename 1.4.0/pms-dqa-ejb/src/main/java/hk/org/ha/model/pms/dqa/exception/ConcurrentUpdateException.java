package hk.org.ha.model.pms.dqa.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class ConcurrentUpdateException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String type;

	public ConcurrentUpdateException(String type) {
		this.type = type;
	}	
	
	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
