package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemFile;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemInstitution;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblemNatureSubCat;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemStatus;
import hk.org.ha.model.pms.dqa.util.DqaRptUtil;
import hk.org.ha.model.pms.dqa.vo.suppperf.CaseFinalRpt;
import hk.org.ha.model.pms.dqa.vo.suppperf.QaProblemFileUploadData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;
import org.springframework.util.CollectionUtils;

@Stateful
@Scope(ScopeType.SESSION)
@Name("qaProblemService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class QaProblemServiceBean implements QaProblemServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private QaProblem qaProblem;
	
	@In(create =true)
	private QaProblemFileServiceLocal qaProblemFileService;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;
	   
	
	private JRDataSource dataSource;
	
	private boolean success;
	
	private String errorCode;
	
	public static final EventLogComparator EVENT_LOG_COMPARATOR = new EventLogComparator();
	public static final SupplierContactComparator SUPPLIER_CONTACT_COMPARATOR = new SupplierContactComparator();
	public static final CompanyContactComparator COMPANY_CONTACT_COMPARATOR = new CompanyContactComparator();
	
		
	public void retrieveQaProblemByCaseNum(String caseNumIn){
		logger.debug("retrieveQaProblemByCaseNum");
	
		success=false;
		
		List<QaProblem> qaProblems = em.createNamedQuery("QaProblem.findByCaseNumRecordStatus")
								.setParameter("caseNum", caseNumIn)
								.setParameter("recordStatus", RecordStatus.Active)
								.setHint(QueryHints.FETCH, "o.problemHeader")
								.setHint(QueryHints.FETCH, "o.supplier")
								.setHint(QueryHints.FETCH, "o.manufacturer")
								.setHint(QueryHints.FETCH, "o.pharmCompany")
								.setHint(QueryHints.BATCH, "o.pharmProblemList")
								.setHint(QueryHints.BATCH, "o.qaBatchNumList")
								.setHint(QueryHints.BATCH, "o.qaProblemNature")
								.setHint(QueryHints.BATCH, "o.classification")
								.setHint(QueryHints.BATCH, "o.qaProblemFileList")
								.setHint(QueryHints.BATCH, "o.eventLogList")
								.setHint(QueryHints.BATCH, "o.faxSummary")
								.getResultList();
		
		if(qaProblems!=null && qaProblems.size()>0)
		{
			qaProblem=qaProblems.get(0);
			qaProblem = getLazyQaProblem(qaProblem);
			success=true;
			
		}
		else
		{
			qaProblem=null;
		}
		
	}
	
	public QaProblem getQaProblemByCaseNum(String caseNumIn){
		logger.debug("getQaProblemByCaseNum");
	
		QaProblem qaProblemFind;
		
		List<QaProblem> qaProblems = em.createNamedQuery("QaProblem.findByCaseNumRecordStatus")
								.setParameter("caseNum", caseNumIn)
								.setParameter("recordStatus", RecordStatus.Active)
								.setHint(QueryHints.FETCH, "o.problemHeader")
								.setHint(QueryHints.FETCH, "o.supplier")
								.setHint(QueryHints.FETCH, "o.manufacturer")
								.setHint(QueryHints.FETCH, "o.pharmCompany")
								.setHint(QueryHints.BATCH, "o.pharmProblemList")
								.setHint(QueryHints.BATCH, "o.qaBatchNumList")
								.setHint(QueryHints.BATCH, "o.qaProblemNature")
								.setHint(QueryHints.BATCH, "o.classification")
								.setHint(QueryHints.BATCH, "o.qaProblemFileList")
								.setHint(QueryHints.BATCH, "o.eventLogList")
								.setHint(QueryHints.BATCH, "o.faxSummary")
								.getResultList();
		
		if(qaProblems!=null && qaProblems.size()>0)
		{
			qaProblemFind = qaProblems.get(0);
			qaProblemFind = getLazyQaProblem(qaProblemFind);
		}
		else
		{
			qaProblemFind=null;
		}
		
		return qaProblemFind;
	}
	
	public void retrieveQaProblemByQaProblem(QaProblem qaProblemIn){
		logger.debug("retrieveQaProblemByQaProblem");
		
		List<QaProblem> qaProblems = em.createNamedQuery("QaProblem.findByQaProblemId")
											   	.setParameter("qaProblemId", qaProblemIn.getQaProblemId())
											   	.setParameter("recordStatus", RecordStatus.Active)
											   	.setHint(QueryHints.FETCH, "o.problemHeader")
												.setHint(QueryHints.FETCH, "o.supplier")
												.setHint(QueryHints.FETCH, "o.manufacturer")
												.setHint(QueryHints.FETCH, "o.pharmCompany")
												.setHint(QueryHints.BATCH, "o.pharmProblemList")
												.setHint(QueryHints.BATCH, "o.qaBatchNumList")
												.setHint(QueryHints.BATCH, "o.qaProblemNature")
												.setHint(QueryHints.BATCH, "o.classification")
												.setHint(QueryHints.BATCH, "o.qaProblemFileList")
												.setHint(QueryHints.BATCH, "o.eventLogList")
												.setHint(QueryHints.BATCH, "o.faxSummary")
												.setHint(QueryHints.BATCH, "o.qaProblemNatureSubCatList")
												.getResultList();
		
		if(qaProblems==null || qaProblems.size()==0)
		{
			qaProblem = null;
		}
		else
		{
			qaProblem = qaProblems.get(0);
			
			
			qaProblem = getLazyQaProblem(qaProblem);
		}
		
		
		
		
		
		
		
		
	}
	
	public void deleteQaProblem(QaProblem qaProblemIn, String deleteReason){
		logger.debug("deleteQaProblem");
		
		QaProblem qaProblemInTemp = getLazyQaProblem(qaProblemIn);
		qaProblemInTemp.setDeleteReason(deleteReason);
		qaProblemInTemp.setRecordStatus(RecordStatus.Deleted);
		
		for(PharmProblem pc : qaProblemInTemp.getPharmProblemList())
		{
			if(pc.getRecordStatus()!=RecordStatus.Deleted)
			{
				pc.setDeleteReason(deleteReason);
				pc.setRecordStatus(RecordStatus.Deleted);
				em.merge(pc);
				em.flush();
			}
		}
		
		em.merge(qaProblemInTemp);
		em.flush();
	}
	
	public void updateQaProblemWithQaProblem(QaProblem qaProblemIn, 
			List<QaProblemFileUploadData> qaProblemFileUploadDataListIn, 
			List<QaProblemInstitution> qaProblemInstitutionListIn, 
			List<QaProblemNatureSubCat> qaProblemNatureSubCatIn, 
			YesNoFlag qaProblemInstitutionAllInstitutionFlagIn){
		logger.debug("updateQaProblemWithQaProblem");
		success=false;
		errorCode="";
		
		qaProblemIn.getQaBatchNumList().size();
		qaProblemIn.getEventLogList().size();
		
		List<QaBatchNum> addQaBatchNums = new ArrayList();
		List<QaBatchNum> updateQaBatchNums = new ArrayList();
		List<QaBatchNum> deleteQaBatchNums = new ArrayList();
			
		List<EventLog> addEventLogs = new ArrayList();
		List<EventLog> updateEventLogs = new ArrayList();
		List<EventLog> deleteEventLogs = new ArrayList();
		
		/////////////////
		for(EventLog elog : qaProblemIn.getEventLogList())
		{
			if(elog.getActionType()!=null)
			{
				if(elog.getActionType().equals("D")){
					deleteEventLogs.add(elog);
				}
				else if(elog.getActionType().equals("A")){
					elog.setQaProblem(qaProblemIn);
					addEventLogs.add(elog);
				}
				else if(elog.getActionType().equals("C")){
					elog.setQaProblem(qaProblemIn);
					updateEventLogs.add(elog);
				}
			}
		}
		
		for (Iterator it = qaProblemIn.getEventLogList().iterator();it.hasNext();)
		{
			EventLog el = (EventLog)it.next();
			
			if(el.getActionType()!=null)
			{
				it.remove();
			}
		}
		///////////////////////
		
		for(QaBatchNum qbnum : qaProblemIn.getQaBatchNumList())
		{
			if(qbnum.getActionType()!=null)
			{
				if(qbnum.getActionType().equals("D")){
					deleteQaBatchNums.add(qbnum);
				}
				else if(qbnum.getActionType().equals("A")){
					qbnum.setQaProblem(qaProblemIn);
					addQaBatchNums.add(qbnum);
				}
				else if(qbnum.getActionType().equals("C")){
					qbnum.setQaProblem(qaProblemIn);
					updateQaBatchNums.add(qbnum);
				}
			}
		}
		
		for (Iterator it = qaProblemIn.getQaBatchNumList().iterator();it.hasNext();)
		{
			QaBatchNum qbn = (QaBatchNum)it.next();
			
			if(qbn.getActionType()!=null)
			{
				it.remove();
			}
		}
		///////////////////////
		
		savePharmProblem(qaProblemIn);
		
		if(qaProblemIn.getClassification().getClassificationId()==null)
		{
			em.persist(qaProblemIn.getClassification());
			em.flush();
		}
		else
		{
			em.merge(qaProblemIn.getClassification());
			em.flush();
		}
		
		if(qaProblemIn.getQaProblemNature().getQaProblemNatureId()==null)
		{
			em.persist(qaProblemIn.getQaProblemNature());
			em.flush();
		}
		else
		{
			em.merge(qaProblemIn.getQaProblemNature());
			em.flush();
		}
		
		for (QaProblemFile qaProblemFile:qaProblemIn.getQaProblemFileList()) {
			em.merge(qaProblemFile);
			em.flush();
		}
		
		em.merge(qaProblemIn);
		em.flush();
		
		saveQaBatchNum(addQaBatchNums, updateQaBatchNums, deleteQaBatchNums);
		saveEventLog(addEventLogs, updateEventLogs, deleteEventLogs);
		saveQaProblemInstitution(qaProblemIn, qaProblemInstitutionListIn, qaProblemInstitutionAllInstitutionFlagIn);
		saveQaProblemNatureSubCat(qaProblemIn,qaProblemNatureSubCatIn);
		
		
		if(qaProblemFileUploadDataListIn!=null && qaProblemFileUploadDataListIn.size()>0)
		{
			for (QaProblemFileUploadData qaProblemFileUploadData : qaProblemFileUploadDataListIn)
			{
				if("A".equals(qaProblemFileUploadData.getActionType()))
				{
					qaProblemFileService.uploadFile(qaProblemFileUploadData, qaProblemIn);
				}
				else if("D".equals(qaProblemFileUploadData.getActionType()))
				{
					qaProblemFileService.deleteFile(qaProblemFileUploadData, qaProblemIn);
				}
			}
		}
		
		success=true;
		
	}
	
	private void saveQaProblemNatureSubCat(QaProblem qaProblemIn, List<QaProblemNatureSubCat> qaProblemNatureSubCatListIn) {
		
		List<QaProblemNatureSubCat> qaProblemNatureSubCatList = em.createNamedQuery("QaProblemNatureSubCat.findByQaProblemId")
		.setParameter("qaProblemId", qaProblemIn.getQaProblemId())
		.setHint(QueryHints.BATCH, "o.qaProblem")
		.getResultList();

		if(!CollectionUtils.isEmpty(qaProblemNatureSubCatList))
		{
			em.createNamedQuery("QaProblemNatureSubCat.updateByQaProblemId")
			.setParameter("qaProblemId", qaProblemIn.getQaProblemId())
			.setParameter("recordStatus", RecordStatus.Deleted)
			.executeUpdate();
			
			em.flush();
		}
			

		if(!CollectionUtils.isEmpty(qaProblemNatureSubCatListIn)){
			for(QaProblemNatureSubCat qaProblemNatureSubCatIn : qaProblemNatureSubCatListIn){
				QaProblemNatureSubCat newQaProblemNatureSubCat = new QaProblemNatureSubCat();
				newQaProblemNatureSubCat.setQaProblem(qaProblemIn);
				newQaProblemNatureSubCat.setProblemNatureSubCat(qaProblemNatureSubCatIn.getProblemNatureSubCat());
				
				newQaProblemNatureSubCat.setRecordStatus(RecordStatus.Active);
				em.persist(newQaProblemNatureSubCat);
				em.flush();
			}
		}
		
	}

	public void savePharmProblem(QaProblem qaProblemIn){
		for(PharmProblem pc:qaProblemIn.getPharmProblemList())
		{
			if(!pc.getProblemStatus().getDataValue().equals(qaProblemIn.getCaseStatus().getDataValue()))
			{
				if(qaProblemIn.getCaseStatus()==CaseStatus.Active)
				{
					pc.setProblemStatus(ProblemStatus.Active);
				}
				else if(qaProblemIn.getCaseStatus()==CaseStatus.Closed)
				{
					pc.setProblemStatus(ProblemStatus.Closed);
				}
				else if(qaProblemIn.getCaseStatus()==CaseStatus.Suspended)
				{
					pc.setProblemStatus(ProblemStatus.Suspended);
				}
				em.merge(pc);
				em.flush();
			}
		}
	}
	
	public void saveEventLog(List<EventLog> addEventLogs, List<EventLog> updateEventLogs, List<EventLog> deleteEventLogs){
		if(addEventLogs!=null && addEventLogs.size()>0)
		{
			for(EventLog ela : addEventLogs)
			{
				em.persist(ela);
				em.flush();
			}
		}
		
		if(updateEventLogs!=null && updateEventLogs.size()>0)
		{
			for(EventLog elu : updateEventLogs)
			{
				em.merge(elu);
				em.flush();
			}
		}
		
		if(deleteEventLogs!=null && deleteEventLogs.size()>0)
		{
			for(EventLog eld : deleteEventLogs)
			{
				eld.setQaProblem(null);
				eld=em.merge(eld);
				em.remove(eld);
				em.flush();
			}
		}
	}
	
	public void saveQaBatchNum(List<QaBatchNum> addQaBatchNums, List<QaBatchNum> updateQaBatchNums, List<QaBatchNum> deleteQaBatchNums){
		if(addQaBatchNums!=null && addQaBatchNums.size()>0)
		{
			for(QaBatchNum qbna : addQaBatchNums)
			{
				em.persist(qbna);
				em.flush();
			}
		}
		
		if(updateQaBatchNums!=null && updateQaBatchNums.size()>0)
		{
			for(QaBatchNum qbnu : updateQaBatchNums)
			{
				em.merge(qbnu);
				em.flush();
			}
		}
		
		if(deleteQaBatchNums!=null && deleteQaBatchNums.size()>0)
		{
			for(QaBatchNum qbnd : deleteQaBatchNums)
			{
				qbnd.setQaProblem(null);
				qbnd=em.merge(qbnd);
				em.remove(qbnd);
				em.flush();
			}
		}
	}
	
	public void saveQaProblemInstitution(QaProblem qaProblemIn, List<QaProblemInstitution> qaProblemInstitutionListIn, YesNoFlag qaProblemInstitutionAllInstitutionFlagIn)
	{
		List<QaProblemInstitution> qaProblemListWithQaProblemIdAllInstitutionFlagYes = em.createNamedQuery("QaProblemInstitution.findByQaProblemIdAllInstitutionFlag")
										.setParameter("qaProblemId", qaProblemIn.getQaProblemId())
										.setParameter("allInstitutionFlag", YesNoFlag.Yes)
										.setHint(QueryHints.BATCH, "o.qaProblem")
										.setHint(QueryHints.BATCH, "o.qaProblem.problemHeader")
										.setHint(QueryHints.BATCH, "o.qaProblem.supplier")
										.setHint(QueryHints.BATCH, "o.qaProblem.manufacturer")
										.setHint(QueryHints.BATCH, "o.qaProblem.pharmCompany")
										.setHint(QueryHints.BATCH, "o.qaProblem.pharmProblemList")
										.setHint(QueryHints.BATCH, "o.qaProblem.qaBatchNumList")
										.setHint(QueryHints.BATCH, "o.qaProblem.qaProblemNature")
										.setHint(QueryHints.BATCH, "o.qaProblem.classification")
										.setHint(QueryHints.BATCH, "o.institution")
										.getResultList();
		
		
		if(qaProblemInstitutionAllInstitutionFlagIn==YesNoFlag.Yes)
		{
			em.createNamedQuery("QaProblemInstitution.deleteByQaProblemIdAllInstitutionFlagN")
								.setParameter("qaProblemId", qaProblemIn.getQaProblemId())
								.setParameter("allInstitutionFlag", YesNoFlag.No)
								.executeUpdate();
			
			em.flush();
			
																
			if(qaProblemListWithQaProblemIdAllInstitutionFlagYes==null || qaProblemListWithQaProblemIdAllInstitutionFlagYes.size()==0)
			{
				QaProblemInstitution qaProblemInstitutionInsert = new QaProblemInstitution();
				qaProblemInstitutionInsert.setAllInstitutionFlag(YesNoFlag.Yes);
				qaProblemInstitutionInsert.setQaProblem(qaProblemIn);
				em.persist(qaProblemInstitutionInsert);
				em.flush();
			}
		}
		else
		{
			if(qaProblemListWithQaProblemIdAllInstitutionFlagYes!=null && qaProblemListWithQaProblemIdAllInstitutionFlagYes.size()>0)
			{
				QaProblemInstitution qaProblemInstitutionDelete = qaProblemListWithQaProblemIdAllInstitutionFlagYes.get(0);
				em.remove(qaProblemInstitutionDelete);
				em.flush();
			}
			
			for(QaProblemInstitution qci : qaProblemInstitutionListIn)
			{
				if(qci.isSelected())
				{
					if(qci.getQaProblemInstitutionId()==null)
					{
						em.persist(qci);
						em.flush();
					}
				}
				else
				{
					if(qci.getQaProblemInstitutionId()!=null)
					{
						qci.setQaProblem(null);
						qci.setInstitution(null);
						qci=em.merge(qci);
						em.remove(qci);
						em.flush();
						
					}
				}
			}
		}
	}
	
	public QaProblem getLazyQaProblem(QaProblem qaProblemLazy){
		// lazy
		
		qaProblemLazy.getQaBatchNumList();
		qaProblemLazy.getQaBatchNumList().size();
		qaProblemLazy.getEventLogList();
		qaProblemLazy.getEventLogList().size();
		Collections.sort(qaProblemLazy.getEventLogList(), EVENT_LOG_COMPARATOR);
		qaProblemLazy.getQaProblemNature();
		qaProblemLazy.getClassification();
		qaProblemLazy.getFaxSummary();
		qaProblemLazy.getQaProblemFileList();
		qaProblemLazy.getQaProblemFileList().size();
		qaProblemLazy.getPharmProblemList();
		qaProblemLazy.getPharmProblemList().size();
		qaProblemLazy.getQaProblemInstitutionList();
		qaProblemLazy.getQaProblemInstitutionList().size();
		
		qaProblemLazy.getProblemHeader();
		
		qaProblemLazy.getSupplier();
		qaProblemLazy.getManufacturer();
		qaProblemLazy.getPharmCompany();
		
		if(qaProblemLazy.getSupplier()!=null)
		{
			qaProblemLazy.getSupplier().getContact();
			qaProblemLazy.getSupplier().getSupplierContactList();
			qaProblemLazy.getSupplier().getSupplierContactList().size();
			Collections.sort(qaProblemLazy.getSupplier().getSupplierContactList(), SUPPLIER_CONTACT_COMPARATOR);
		}
		if(qaProblemLazy.getManufacturer()!=null)
		{
			qaProblemLazy.getManufacturer().getContact();
			qaProblemLazy.getManufacturer().getCompanyContactList();
			qaProblemLazy.getManufacturer().getCompanyContactList().size();
			Collections.sort(qaProblemLazy.getManufacturer().getCompanyContactList(), COMPANY_CONTACT_COMPARATOR);
		}
		if(qaProblemLazy.getPharmCompany()!=null)
		{
			qaProblemLazy.getPharmCompany().getContact();
			qaProblemLazy.getPharmCompany().getCompanyContactList();
			qaProblemLazy.getPharmCompany().getCompanyContactList().size();
			Collections.sort(qaProblemLazy.getPharmCompany().getCompanyContactList(), COMPANY_CONTACT_COMPARATOR);
		}
		
		for(QaBatchNum qbn : qaProblemLazy.getQaBatchNumList())
		{
			if(qbn.getBatchSuspList()!=null)
			{
				qbn.getBatchSuspList().size();
			}
		}
		
		for(PharmProblem pc : qaProblemLazy.getPharmProblemList())
		{
			pc.getInstitution();
			pc.getPharmBatchNumList();
			pc.getPharmBatchNumList().size();
		}
		
		for(EventLog el : qaProblemLazy.getEventLogList())
		{
			el.getFaxDetail();
			if(el.getFaxDetail()!=null)
			{
				el.getFaxDetail().getFaxInitHosp();
				el.getFaxDetail().getFaxInterimHosp();
				el.getFaxDetail().getFaxFinalHosp();
				el.getFaxDetail().getFaxInitSupp();
				el.getFaxDetail().getFaxInterimSupp();
				el.getFaxDetail().getFaxSender();
			}
		}
		
		for (Iterator it = qaProblemLazy.getPharmProblemList().iterator();it.hasNext();)
		{
			PharmProblem pc= (PharmProblem)it.next();
			pc.getPharmProblemNatureList();
			pc.getPharmProblemNatureList().size();
			for(Iterator natureIt = pc.getPharmProblemNatureList().iterator(); natureIt.hasNext();){
				PharmProblemNature ppN = (PharmProblemNature) natureIt.next();
				ppN.getProblemNatureSubCat();
				if(ppN.getRecordStatus().equals(RecordStatus.Deleted)){
					natureIt.remove();
				}
			}
			
			if(pc.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
			
		}
		
		
		for (Iterator it = qaProblemLazy.getQaProblemNatureSubCatList().iterator();it.hasNext();)
		{
			QaProblemNatureSubCat qaProblemNatureSubCat = (QaProblemNatureSubCat)it.next();
			
			if(qaProblemNatureSubCat.getRecordStatus().equals(RecordStatus.Deleted))
			{
				it.remove();
			}
			
		
		}
		
		
		return qaProblemLazy;
	}
	
	public void saveQaProblem(QaProblem qaProblemIn){
		em.merge(qaProblemIn);
		em.flush();
	}
	
	
	private static class EventLogComparator implements Comparator<EventLog>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private Date getEventLogDate(EventLog el) {
        	return el.getEventLogDate();
        }
        
        public int compare(EventLog el1, EventLog el2) {

            Date date1 = getEventLogDate(el1);
            Date date2 = getEventLogDate(el2);

            if (!date1.equals(date2)) {
            	return date1.compareTo(date2);
            }

            return CompareToBuilder.reflectionCompare(el1, el2);
        }
        
	}
	
	private static class SupplierContactComparator implements Comparator<SupplierContact>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private String getFirstName(SupplierContact sc1) {
        	return sc1.getContact().getFirstName();
        }
        
        public int compare(SupplierContact scc1, SupplierContact scc2) {

            String firstName1 = getFirstName(scc1);
            String firstName2 = getFirstName(scc2);

            if (!firstName1.equals(firstName2)) {
            	return firstName1.compareTo(firstName2);
            }

            return CompareToBuilder.reflectionCompare(scc1, scc2);
        }
        
	}
	
	private static class CompanyContactComparator implements Comparator<CompanyContact>, Serializable {

        private static final long serialVersionUID = -3628445142449361315L;

        private String getFirstName(CompanyContact cc1) {
        	return cc1.getContact().getFirstName();
        }
        
        public int compare(CompanyContact ccc1, CompanyContact ccc2) {

            String firstName1 = getFirstName(ccc1);
            String firstName2 = getFirstName(ccc2);

            if (!firstName1.equals(firstName2)) {
            	return firstName1.compareTo(firstName2);
            }

            return CompareToBuilder.reflectionCompare(ccc1, ccc2);
        }
        
	}
	
	public void createCaseFinalRpt(QaProblem qaProblemIn, SupplierContact supplierContactIn, CompanyContact companyContactIn)
	{
		logger.debug("createFaxInitHospRpt");
		
		List<CaseFinalRpt> caseFinalRptList = new ArrayList<CaseFinalRpt>();
		
		List<EventLog> eventLogList = qaProblemIn.getEventLogList();
		if(eventLogList==null || eventLogList.size()==0)
		{
			caseFinalRptList.add(mapCaseFinalRpt(qaProblemIn, null, null, supplierContactIn, companyContactIn));
		}
		else
		{
			for (EventLog eventLog:eventLogList)
			{
				caseFinalRptList.add(mapCaseFinalRpt(qaProblemIn, eventLog.getEventLogDate(), eventLog.getEventDesc(), supplierContactIn, companyContactIn));
			}
		}

		dataSource = new JRBeanCollectionDataSource(caseFinalRptList);
	}
	
	private CaseFinalRpt mapCaseFinalRpt(QaProblem qaProblemIn, Date eventLogDate, String eventLogDesc, SupplierContact supplierContactIn, CompanyContact companyContactIn)
	{
		CaseFinalRpt rpt = new CaseFinalRpt();
		
		rpt.setCaseNum(qaProblemIn.getCaseNum());
		rpt.setCaseStatus(qaProblemIn.getCaseStatus().getDisplayValue());
		rpt.setItemCode(
			qaProblemIn.getProblemHeader().getItemCode() + 
			"(" + qaProblemIn.getProblemHeader().getDmDrug().getFullDrugDesc() + ")");
		rpt.setClassification((qaProblemIn.getClassification())==null?"":((qaProblemIn.getClassification().getClassificationCode())==null?"":(qaProblemIn.getClassification().getClassificationCode().getDataValue())));
		
		StringBuffer batchNumBuf = new StringBuffer();
		int bi = 0;
		for (QaBatchNum batchNum:qaProblemIn.getQaBatchNumList())
		{
			batchNumBuf.append(batchNum.getBatchNum());
			if (bi != (qaProblemIn.getQaBatchNumList().size() -1)){
				batchNumBuf.append(", ");}
			
			bi++;
		}
		rpt.setBatchNum(batchNumBuf.toString());
		rpt.setSupplier(supplierContactIn.getSupplier().getSupplierName());
		rpt.setSupplierContact(supplierContactIn.getContact().getFirstName() + " " + ((supplierContactIn.getContact().getLastName())==null?"":(supplierContactIn.getContact().getLastName())));
		if (!DqaRptUtil.strIsEmptyOrNull(supplierContactIn.getContact().getOfficePhone())){
			rpt.setSupplierTel(supplierContactIn.getContact().getOfficePhone());}
		else if (!DqaRptUtil.strIsEmptyOrNull(supplierContactIn.getContact().getMobilePhone())){
			rpt.setSupplierTel(supplierContactIn.getContact().getMobilePhone());}
		rpt.setSupplierFax(supplierContactIn.getContact().getFax()==null?"":(supplierContactIn.getContact().getFax()));
		rpt.setManuf(companyContactIn==null?"":(companyContactIn.getCompany().getCompanyName()));
		rpt.setManufContact(companyContactIn==null?"":(companyContactIn.getContact().getFirstName() + " " + ((companyContactIn.getContact().getLastName())==null?"":(companyContactIn.getContact().getLastName()))));
		if(companyContactIn==null){
			rpt.setManufTel("");
		}
		else{
			if (!DqaRptUtil.strIsEmptyOrNull(companyContactIn.getContact().getOfficePhone())){
				rpt.setManufTel(companyContactIn.getContact().getOfficePhone());}
			else if (!DqaRptUtil.strIsEmptyOrNull(companyContactIn.getContact().getMobilePhone())){
				rpt.setManufTel(companyContactIn.getContact().getMobilePhone());}
		}
		rpt.setManufFax(companyContactIn==null?"":(companyContactIn.getContact().getFax()==null?"":companyContactIn.getContact().getFax()));
		
		rpt.setPhysicalDefectFlag(DqaRptUtil.getBoolean(qaProblemIn.getQaProblemNature().getPhysicalDefectFlag()));
		rpt.setPackagingDefectFlag(DqaRptUtil.getBoolean(qaProblemIn.getQaProblemNature().getPackagingDefectFlag()));
		rpt.setAppearDiscFlag(DqaRptUtil.getBoolean(qaProblemIn.getQaProblemNature().getAppearDiscFlag()));
		rpt.setForeignMatterFlag(DqaRptUtil.getBoolean(qaProblemIn.getQaProblemNature().getForeignMatterFlag()));
		rpt.setOtherFlag(DqaRptUtil.getBoolean(qaProblemIn.getQaProblemNature().getOtherFlag()));
		
		StringBuffer instBuf = new StringBuffer();
		int pi = 0;
		for (PharmProblem pharmCom:qaProblemIn.getPharmProblemList())
		{
			instBuf.append(pharmCom.getInstitution().getInstitutionCode() + "(" + DqaRptUtil.getDateStrDDMMMYYYY(pharmCom.getProblemDate()) + ")");
			if (pi != (qaProblemIn.getPharmProblemList().size() -1)){
				instBuf.append(", ");}
			
			pi++;
		}
		rpt.setInstitutions(instBuf.toString());
		rpt.setProblemDetails(qaProblemIn.getQaProblemNature().getProblemDetail()==null?"":qaProblemIn.getQaProblemNature().getProblemDetail());
		List<EventLog> eventLogList = qaProblemIn.getEventLogList();
		Collections.sort(eventLogList, EVENT_LOG_COMPARATOR);
		rpt.setEventLogDate(eventLogDate==null?"":(DqaRptUtil.getDateStrDDMMMYYYY(eventLogDate)));
		rpt.setEventLogs(eventLogDesc==null?"":(eventLogDesc));
		rpt.setSummary(qaProblemIn.getCaseSummary());
		rpt.setGenerationDate(DqaRptUtil.getDateStrDDMMMYYYYHHMMSS(new Date()));
		return rpt;
	}
	
	public void generateCaseFinalRpt() {
		//passing relevant information to the jasper report
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
		// Call framework reportProvider to generate report
		String contentId = reportProvider.generateReport("report/caseFinalRpt.jasper", parameters, dataSource);
		
		reportProvider.redirectReport(contentId);
	}
	
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	
		
	@Remove
	public void destroy(){
		if (qaProblem != null){
			qaProblem = null;
		}
	}
}
