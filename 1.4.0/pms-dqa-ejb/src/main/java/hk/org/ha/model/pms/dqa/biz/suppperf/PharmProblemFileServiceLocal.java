package hk.org.ha.model.pms.dqa.biz.suppperf;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemFile;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;

import javax.ejb.Local;

@Local
public interface PharmProblemFileServiceLocal {
	
	PharmProblemFile uploadFile(PharmProblemFileUploadData pharmProblemFileUploadDataIn, PharmProblem pharmProblemIn);
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
