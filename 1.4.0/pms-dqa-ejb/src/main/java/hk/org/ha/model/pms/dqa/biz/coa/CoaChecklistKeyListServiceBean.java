package hk.org.ha.model.pms.dqa.biz.coa;

import java.util.List;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaChecklistKey;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaChecklistKeyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaChecklistKeyListServiceBean implements CoaChecklistKeyListServiceLocal {

	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaChecklistKey> coaChecklistKeyList;
	
	public void retrieveCoaChecklistKeyList(){
		logger.debug("retrieveCoaChecklistKeyList");
		coaChecklistKeyList = em.createNamedQuery("CoaChecklistKey.findAll")
								.setParameter("recordStatus", RecordStatus.Active)
								.getResultList();
		int cnt=1;
		for(CoaChecklistKey cclk:coaChecklistKeyList) {
			cclk.setDisplayNum(cnt++);
		}		
	}
	
	@Remove
	public void destroy(){
		if(coaChecklistKeyList != null) {
			coaChecklistKeyList = null;  
		}
	}	
}
