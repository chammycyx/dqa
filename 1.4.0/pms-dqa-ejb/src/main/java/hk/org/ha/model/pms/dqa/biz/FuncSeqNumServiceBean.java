package hk.org.ha.model.pms.dqa.biz;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.FuncSeqNum;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.internal.jpa.EntityManagerImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateless
@Scope(ScopeType.STATELESS)
@Name("funcSeqNumService")
@MeasureCalls
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class FuncSeqNumServiceBean implements FuncSeqNumServiceLocal {
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	private static final String SIX_ZERO = "000000";
	
	public String getNextFuncSeqNumString(String funcCode, String paddingZero) {
		Integer refNum = retrieveNextFuncSeqNum(funcCode);
		String refNumStr = "";
		if (refNum!=null){
			NumberFormat numformat = new DecimalFormat(paddingZero);
			refNumStr = numformat.format(refNum.intValue());
		} else {
			return null;
		}
		return funcCode + refNumStr;
	}
	
	public Integer retrieveNextFuncSeqNum(String funcCode){
		logger.debug("retrieveNextFuncSeqNum : #0", funcCode);
		Integer nextNum = null;
		try{
			FuncSeqNum funcSeqNum = this.getEmi(em).find(FuncSeqNum.class, funcCode, LockModeType.WRITE);
			funcSeqNum.setLastNum(funcSeqNum.getLastNum()+1);
			nextNum = funcSeqNum.getLastNum();
		}catch (NoResultException e){
			return null;
		}
		return nextNum;
	}
	
	public Integer getNextFuncSeqNum(String funcCode){
		logger.debug("retrieveNextFuncSeqNum : #0", funcCode);
		Integer nextNum = null;
		try{
			FuncSeqNum funcSeqNum = this.getEmi(em).find(FuncSeqNum.class, funcCode, LockModeType.WRITE);
			nextNum = funcSeqNum.getLastNum()+1;
		}catch (NoResultException e){
			return null;
		}
		return nextNum;
	}
	
	public String retrieveNextFRNSeqNum(){
		logger.debug("retrieveNextFRNSeqNum");
		String funcCode = "FRN";
		Integer nextNum = null;
		String refNum = "";
		NumberFormat numformat = new DecimalFormat(SIX_ZERO);
		try{
			FuncSeqNum funcSeqNum = this.getEmi(em).find(FuncSeqNum.class, funcCode, LockModeType.WRITE);
			funcSeqNum.setLastNum(funcSeqNum.getLastNum()+1);
			nextNum = funcSeqNum.getLastNum();
			refNum = funcSeqNum.getFuncPrefix()+funcSeqNum.getYear()+numformat.format(nextNum.intValue());
		}catch (NoResultException e){
			return null;
		}
		return refNum;
	}
	
	private EntityManagerImpl getEmi(EntityManager em) {
		return (EntityManagerImpl) em.getDelegate();
	}
}
