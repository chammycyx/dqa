package hk.org.ha.model.pms.dqa.persistence.sample;

import flexjson.JSON;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dms.persistence.DmDrug;
import hk.org.ha.model.pms.dqa.cacher.DmDrugCacher;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.ChemAnalysisQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.MicroBioTestQtyReq;
import hk.org.ha.model.pms.dqa.udt.sample.SpecialCat;
import hk.org.ha.model.pms.dqa.udt.sample.ValidTestQtyReq;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "SAMPLE_ITEM") 
@NamedQueries({
	@NamedQuery(name = "SampleItem.findByItemCode", query = "select o from SampleItem o where o.itemCode = :itemCode"),	
	@NamedQuery(name = "SampleItem.findByItemCodeRecordStatus", query = "select o from SampleItem o where o.itemCode = :itemCode and o.recordStatus = :recordStatus"),
	@NamedQuery(name = "SampleItem.findLikeItemCodeRecordStatus", query = "select o from SampleItem o where o.itemCode like :itemCode and  o.recordStatus = :recordStatus"),
	@NamedQuery(name = "SampleItem.findByRecordStatus", query = "select o from SampleItem o where o.recordStatus = :recordStatus order by o.itemCode"),
	@NamedQuery(name = "SampleItem.findBySampleItemId", query = "select o from SampleItem o where o.sampleItemId in :keyList"),
	@NamedQuery(name = "SampleItem.findByRecordStatusRiskLevelCode", query = "select o from SampleItem o where o.recordStatus = :recordStatus and o.riskLevel.riskLevelCode = :riskLevelCode"),
	@NamedQuery(name = "SampleItem.findByCriteria", query = "select distinct(o.sampleItem) from ExclusionTest o where o.sampleItem.recordStatus = :recordStatus " +
			"and (o.sampleItem.itemCode = :itemCode or :itemCode is null)" +
			"and (o.sampleItem.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) " +
			"and (o.exclusionCode = :exclusionCode or :exclusionCode is null) " +
			"and (o.sampleTest.testCode = :testCode or :testCode is null)"),
	@NamedQuery(name = "SampleItem.findForSampleItemMaint", query = "select o from SampleItem o where o.recordStatus = :recordStatus " +
			"and (o.itemCode = :itemCode or :itemCode is null)" +
			"and (o.riskLevel.riskLevelCode = :riskLevelCode or :riskLevelCode is null) "),
	@NamedQuery(name = "SampleItem.findForSampleTest", query = "select o from SampleItem o where o.recordStatus = :recordStatus " +
			"and o.itemCode like :itemCode and o.riskLevel.recordStatus = :recordStatus " +
			"and o.riskLevel.proceedSampleTestFlag = :proceedSampleTestFlag"),
	@NamedQuery(name = "SampleItem.findForSampleTestValidation", query = "select o from SampleItem o where o.recordStatus = :recordStatus " +
			"and o.itemCode = :itemCode and o.riskLevel.recordStatus = :recordStatus " +
			"and o.riskLevel.proceedSampleTestFlag = :proceedSampleTestFlag"),
	@NamedQuery(name = "SampleItem.findByProceedSampleTestFlagRecordStatus", query = "select o from SampleItem o where o.recordStatus = :recordStatus " +
			"and o.riskLevel.recordStatus = :recordStatus and o.riskLevel.proceedSampleTestFlag = :proceedSampleTestFlag "),
	@NamedQuery(name = "SampleItem.findByRiskLevelCodeProceedSampleTestFlagRecordStatus", query = "select o from SampleItem o where o.recordStatus = :recordStatus " +
			"and o.riskLevel.recordStatus = :recordStatus and o.riskLevel.proceedSampleTestFlag = :proceedSampleTestFlag " +
			"and o.riskLevel.riskLevelCode = :riskLevelCode ")
			
})
@Customizer(AuditCustomizer.class)
public class SampleItem extends VersionEntity {


	private static final long serialVersionUID = -8067627947149211051L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sampleItemSeq")
	@SequenceGenerator(name = "sampleItemSeq", sequenceName = "SEQ_SAMPLE_ITEM", initialValue=10000)
	@Id
	@Column(name="SAMPLE_ITEM_ID", nullable = false)
    private Long sampleItemId;
	
	@ManyToOne
	@JoinColumn(name="RISK_LEVEL_CODE")
    private RiskLevel riskLevel;

	@Converter(name = "SampleItem.speicalCat", converterClass = SpecialCat.Converter.class )
	@Convert("SampleItem.speicalCat")
	@Column(name="SPECIAL_CATEGORY", length = 2)
	private SpecialCat speicalCat;
	
	@Converter(name = "SampleItem.ChemAnalysisQtyReq", converterClass = ChemAnalysisQtyReq.Converter.class )
	@Convert("SampleItem.ChemAnalysisQtyReq")
	@Column(name="CHEM_ANALYSIS_QTY_REQ", length = 4)
    private ChemAnalysisQtyReq chemAnalysisQtyReq;
	
	@Column(name="CHEM_ANALYSIS_QTY_REQ_DESC", length = 80)
    private String chemAnalysisQtyReqDesc;
	
	@Converter(name = "SampleItem.MicroBioTestQtyReq", converterClass = MicroBioTestQtyReq.Converter.class )
	@Convert("SampleItem.MicroBioTestQtyReq")
	@Column(name="MICRO_BIO_TEST_QTY_REQ", length = 4)
    private MicroBioTestQtyReq microBioTestQtyReq;
	
	@Column(name="MICRO_BIO_TEST_QTY_REQ_DESC", length = 80)
    private String microBioTestQtyReqDesc;
	
	@Converter(name = "SampleItem.ValidTestQtyReq", converterClass = ValidTestQtyReq.Converter.class )
	@Convert("SampleItem.ValidTestQtyReq")
	@Column(name="VALID_TEST_QTY_REQ", length = 4)
    private ValidTestQtyReq validTestQtyReq;
	
	@Column(name="VALID_TEST_QTY_REQ_DESC", length = 80)
    private String validTestQtyReqDesc;
	
	@Column(name="CHEM_ANALYSIS_REMARK", length = 80)
    private String chemAnalysisRemark;
	
	@Column(name="MICRO_BIO_TEST_REMARK", length = 80)
    private String microBioTestRemark;
	
	@Column(name="REMARK", length = 80)
    private String remark;
	
	@Converter(name = "SampleItem.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("SampleItem.recordStatus")
	@Column(name="RECORD_STATUS", length = 1)
    private RecordStatus recordStatus;
	
	@Column(name="EXPORT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date exportDate;
	
	@Column(name="EXCLUSION", length = 166)
    private String exclusion;
	
	@Transient
	private boolean selected;
	
	@Column(name="ITEM_CODE", length = 10)
	private String itemCode;
	
	@Transient
	private DmDrug dmDrug = null;
	
	@PostLoad
	public void postLoad() {
		if (itemCode != null) {
			setDmDrug(DmDrugCacher.instance().getDrugByItemCode(itemCode));
		}
	}
	
	@PrivateOwned
	@OneToMany(mappedBy="sampleItem", cascade=CascadeType.ALL)
	private List<ExclusionTest> exclusionTestList;

	public Long getSampleItemId() {
		return sampleItemId;
	}


	public void setSampleItemId(Long sampleItemId) {
		this.sampleItemId = sampleItemId;
	}	

	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getChemAnalysisQtyReqDesc() {
		return chemAnalysisQtyReqDesc;
	}


	public void setChemAnalysisQtyReqDesc(String chemAnalysisQtyReqDesc) {
		this.chemAnalysisQtyReqDesc = chemAnalysisQtyReqDesc;
	}


	public RiskLevel getRiskLevel() {
		return riskLevel;
	}


	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}


	public ChemAnalysisQtyReq getChemAnalysisQtyReq() {
		return chemAnalysisQtyReq;
	}


	public void setChemAnalysisQtyReq(ChemAnalysisQtyReq chemAnalysisQtyReq) {
		this.chemAnalysisQtyReq = chemAnalysisQtyReq;
	}


	public MicroBioTestQtyReq getMicroBioTestQtyReq() {
		return microBioTestQtyReq;
	}


	public void setMicroBioTestQtyReq(MicroBioTestQtyReq microBioTestQtyReq) {
		this.microBioTestQtyReq = microBioTestQtyReq;
	}


	public String getMicroBioTestQtyReqDesc() {
		return microBioTestQtyReqDesc;
	}


	public void setMicroBioTestQtyReqDesc(String microBioTestQtyReqDesc) {
		this.microBioTestQtyReqDesc = microBioTestQtyReqDesc;
	}


	public ValidTestQtyReq getValidTestQtyReq() {
		return validTestQtyReq;
	}


	public void setValidTestQtyReq(ValidTestQtyReq validTestQtyReq) {
		this.validTestQtyReq = validTestQtyReq;
	}


	public String getValidTestQtyReqDesc() {
		return validTestQtyReqDesc;
	}


	public void setValidTestQtyReqDesc(String validTestQtyReqDesc) {
		this.validTestQtyReqDesc = validTestQtyReqDesc;
	}


	public RecordStatus getRecordStatus() {
		return recordStatus;
	}


	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}


	public void setChemAnalysisRemark(String chemAnalysisRemark) {
		this.chemAnalysisRemark = chemAnalysisRemark;
	}


	public String getChemAnalysisRemark() {
		return chemAnalysisRemark;
	}


	public void setMicroBioTestRemark(String microBioTestRemark) {
		this.microBioTestRemark = microBioTestRemark;
	}


	public String getMicroBioTestRemark() {
		return microBioTestRemark;
	}


	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	public boolean isSelected() {
		return selected;
	}


	public void setExportDate(Date exportDate) {
		this.exportDate = new Date(exportDate.getTime());
	}


	public Date getExportDate() {
		return (exportDate != null) ? new Date(exportDate.getTime()) : null;
	}


	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}


	public String getExclusion() {
		return exclusion;
	}
	
	public List<ExclusionTest> getExclusionTestList() {
		if (exclusionTestList == null){
			exclusionTestList = new ArrayList<ExclusionTest>();
		}
		return exclusionTestList;
	}


	public void setExclusionTestList(List<ExclusionTest> exclusionTestList) {
		this.exclusionTestList = exclusionTestList;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setDmDrug(DmDrug dmDrug) {
		this.dmDrug = dmDrug;
	}

	@JSON(include=false)
	public DmDrug getDmDrug() {
		return dmDrug;
	}


	public void setSpeicalCat(SpecialCat speicalCat) {
		this.speicalCat = speicalCat;
	}


	public SpecialCat getSpeicalCat() {
		return speicalCat;
	}


}
