package hk.org.ha.model.pms.dqa.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OrderTypeAll implements StringValuedEnum {
	
	Contract("C", "Contract"),
	StandingQuotation("Q", "Standing Quotation"),
	DirectPurchase("D", "Direct Purchase"),
	SelfManufacturing("S", "Self Manufacturing"),
	CKMSM("M", "Manu. Items CKMS"),
	CKMSK("K", "Dist. Items CKMS"),
	Temporary("T", "Temporary");
	
    private final String dataValue;
    private final String displayValue;
        
    OrderTypeAll(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
    
    public static OrderTypeAll findByDataValue(String dataValue) {
    	return StringValuedEnumReflect.getObjectFromValue(OrderTypeAll.class, dataValue);
    }
        
	public static class Converter extends StringValuedEnumConverter<OrderTypeAll> {

		private static final long serialVersionUID = -3701625740094095965L;

		@Override
    	public Class<OrderTypeAll> getEnumClass() {
    		return OrderTypeAll.class;
    	}
    }
}
