package hk.org.ha.model.pms.dqa.vo.coa;

public class CoaProcessRptData {
	
	private String supplierCode ="";
	private String itemCode = "";
	private int	   pass =0;
	private int    fail =0;
	private int    verificationInProgress =0;
	private int    discrepancy =0;
	private int    total;
	
	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public int getPass() {
		return pass;
	}
	public void setPass(int pass) {
		this.pass = pass;
	}
	public int getFail() {
		return fail;
	}
	public void setFail(int fail) {
		this.fail = fail;
	}
	public int getVerificationInProgress() {
		return verificationInProgress;
	}
	public void setVerificationInProgress(int verificationInProgress) {
		this.verificationInProgress = verificationInProgress;
	}
	public int getDiscrepancy() {
		return discrepancy;
	}
	public void setDiscrepancy(int discrepancy) {
		this.discrepancy = discrepancy;
	}

	public int getTotal() {
		this.total = this.pass + this.fail + this.verificationInProgress + this.discrepancy ; 
		return total;
		
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
