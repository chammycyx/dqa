package hk.org.ha.model.pms.dqa.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="ContractMSP")
@XmlType(propOrder={"mspRecordId","manufCode","manufName","pharmCompanyCode",
					"pharmCompanyName","countryOfOrigin","effectiveStartDate","effectiveEndDate"})
public class ContractMsp {

	private Long mspRecordId;
	private String manufCode;
	private String manufName;
	private String pharmCompanyCode;
	private String pharmCompanyName;
	private String countryOfOrigin;
	private Date effectiveStartDate;
	private Date effectiveEndDate;
	
	public void setMspRecordId(Long mspRecordId) {
		this.mspRecordId = mspRecordId;
	}
	
	@XmlElement(name="msp_record_id")
	public Long getMspRecordId() {
		return mspRecordId;
	}
	
	public void setManufCode(String manufCode) {
		this.manufCode = manufCode;
	}
	
	@XmlElement(name="manufacturer_code")
	public String getManufCode() {
		return manufCode;
	}

	public void setManufName(String manufName) {
		this.manufName = manufName;
	}
	
	@XmlElement(name="manufacturer_name")
	public String getManufName() {
		return manufName;
	}

	public void setPharmCompanyCode(String pharmCompanyCode) {
		this.pharmCompanyCode = pharmCompanyCode;
	}
	
	@XmlElement(name="pharm_company_code")
	public String getPharmCompanyCode() {
		return pharmCompanyCode;
	}

	public void setPharmCompanyName(String pharmCompanyName) {
		this.pharmCompanyName = pharmCompanyName;
	}
	
	@XmlElement(name="pharm_company_name")
	public String getPharmCompanyName() {
		return pharmCompanyName;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	
	@XmlElement(name="country_of_origin")
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setEffectiveStartDate(Date effectiveStartDate) {
		if (effectiveStartDate != null){
			this.effectiveStartDate = new Date(effectiveStartDate.getTime());
		}
	}
	
	@XmlElement(name="effective_start_date")
	public Date getEffectiveStartDate() {
		return (effectiveStartDate != null)? new Date(effectiveStartDate.getTime()) : null;
	}

	public void setEffectiveEndDate(Date effectiveEndDate) {
		if (effectiveEndDate != null){
			this.effectiveEndDate = new Date(effectiveEndDate.getTime());
		}
	}
	
	@XmlElement(name="effective_end_date")
	public Date getEffectiveEndDate() {
		return (effectiveEndDate != null)? new Date(effectiveEndDate.getTime()) : null;
	}		
	
	@Override
	public String toString() {		
		return "ContractMSP [mspRecordId=" + mspRecordId + 
		", manufCode=" + manufCode + 
		", manufName=" + manufName + 
		", pharmCompanyCode=" + pharmCompanyCode + 
		", pharmCompanyName=" + pharmCompanyName + 
		", countryOfOrigin=" + countryOfOrigin + 
		", effectiveStartDate=" + effectiveStartDate + 
		", effectiveEndDate=" + effectiveEndDate + "]"; 

	}
	
}
