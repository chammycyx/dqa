package hk.org.ha.model.pms.dqa.biz;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
import hk.org.ha.model.pms.dqa.persistence.SupplierContact;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("companyContactService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CompanyContactServiceBean implements CompanyContactServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@In
	private Company company;
	
	@In(required = false)
	@Out(required = false)
	private CompanyContact companyContact;
	
	private boolean updateSucceed;
	
	@In
	private StatusMessages statusMessages;
	
	public boolean isUpdateSucceed(){
		
		return updateSucceed;
	}
	
	@SuppressWarnings("unchecked")
	public List retrieveContactIdListByFirstNameLastNameEmail(Contact contact){
		List existing = em.createNamedQuery("Contact.findByFirstNameLastNameEmail")
		.setParameter("firstName", contact.getFirstName().toUpperCase(Locale.ENGLISH))
		.setParameter("lastName", contact.getLastName().toUpperCase(Locale.ENGLISH))
		.setParameter("email", contact.getEmail().toUpperCase(Locale.ENGLISH))
			.getResultList();
		
		return existing;
	}
	
	@SuppressWarnings("unchecked")
	public List retrieveCompanyContactIdListByFirstNameLastNameEmail(CompanyContact companyContact){
		List existing = em.createNamedQuery("CompanyContact.findByFirstNameLastNameEmail")
		.setParameter("firstName", companyContact.getContact().getFirstName().toUpperCase(Locale.ENGLISH))
		.setParameter("lastName", companyContact.getContact().getLastName().toUpperCase(Locale.ENGLISH))
		.setParameter("email", companyContact.getContact().getEmail().toUpperCase(Locale.ENGLISH))
		.setParameter("companyCode", companyContact.getCompany().getCompanyCode())
			.getResultList();
		
		return existing;
	}
	
	public void addCompanyContact()
	{
		logger.info("addCompanyContact");
		
		companyContact = new CompanyContact();
		companyContact.setContact(new Contact());
		companyContact.setCompany(company);
		company.getContact();	
	}
	
	public void createCompanyContact()
	{
		logger.info("createCompanyContact :#0 #1", companyContact.getContact().getFirstName(),
				companyContact.getContact().getLastName());
		
		companyContact.getCompany().getContact();
		
		updateSucceed = false;
		List existing = new ArrayList<String>();
		
		if (companyContact.getContact() !=null ){
			existing = retrieveCompanyContactIdListByFirstNameLastNameEmail(companyContact);
		}
		
		if (existing.size() == 0) {
			logger.info("persist");
			em.persist(companyContact);
			company.getCompanyContactList().add(companyContact);
			em.flush();
			
			updateSucceed = true;
		}else {
			statusMessages.addToControl("txtFirstName", "Contact already exists");
			statusMessages.addToControl("txtLastName", "Contact already exists");
			statusMessages.addToControl("txtEmail", "Email already exists");
		}
		
		logger.info("createCompanyContact end");
	}
	
	public void retrieveCompanyContactByCompanyCodeContactId(String companyCode, Long contactId) {
		logger.info("retrieveCompanyContactByCompanyCodeContactId :#0 , #1 " , companyCode, contactId );
			
			List<CompanyContact> companyContactListFind = em.createNamedQuery("CompanyContact.findByCompanyCodeContactId")
			.setParameter("companyCode", companyCode)
			.setParameter("contactId", contactId)
			.setHint(QueryHints.FETCH, "o.contact")
			.getResultList();
			
			if(companyContactListFind==null || companyContactListFind.size()==0)
			{
				companyContact = null;
			}
			else
			{
				companyContact = companyContactListFind.get(0);
			}
		
	}
	
	public void updateCompanyContact()
	{
		logger.info("updateCompanyContact :#0 ",  companyContact.getContact().getFirstName());
		Contact tmpContact = companyContact.getContact();
		
		boolean duplicated = true;

		updateSucceed = false;
		companyContact.getCompany().getContact();
		
		List existingContact = retrieveCompanyContactIdListByFirstNameLastNameEmail(companyContact);
		
		if (existingContact.size() == 1 ) {
			if ( ((Long)existingContact.get(0)).equals(tmpContact.getContactId()) ){ //check updating the same contact 
				
				companyContact = em.merge(companyContact);

				em.flush();
				updateSucceed = true;
				duplicated = false;
			}
		}else if (existingContact.size() ==0){ //update to other name/ email
			companyContact = em.merge(companyContact);
			
			em.flush();
			updateSucceed = true;
			duplicated = false;
		}
		
		if (duplicated){
			logger.debug("duplicated #0 #1", tmpContact.getFirstName(),tmpContact.getEmail() );

			statusMessages.addToControl("txtName", "Contact - #{companyContact.getContact().getName()} already exists");
			statusMessages.addToControl("txtEmail", "Email already exists");
		}
	}
	
	public void deleteCompanyContact(CompanyContact myCompanyContact)
	{
		logger.info("deleteCompanyContact #0", myCompanyContact.getContact().getFirstName());

		em.remove(em.merge(myCompanyContact));
		em.flush();
	}
	
	@Remove
	public void destroy() {
		
	}
}