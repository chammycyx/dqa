package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.international.StatusMessages;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("riskLevelService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls

public class RiskLevelServiceBean implements RiskLevelServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	@Out(required = false)
	private RiskLevel riskLevel;
	
	private boolean success;
	
	private String errorCode;

	@In(create=true)
	private SampleItemListServiceLocal sampleItemListService;	
	
	@In(create=true)
	private TestFrequencyListServiceLocal testFrequencyListService;
		
	public void retrieveRiskLevelByRiskLevelCode(String riskLevelCode){
		logger.debug("retrieveRiskLevelByRiskLevelCode");

		riskLevel = retrieveRiskLevel(riskLevelCode);
	}

	private RiskLevel retrieveRiskLevel(String riskLevelCode){
		logger.debug("retrieveRiskLevel");
		return em.find(RiskLevel.class, riskLevelCode);
	}
	
	public void addRiskLevel(){
		logger.debug("addRiskLevel");
		
		riskLevel = new RiskLevel();
	}
	
	public void createRiskLevel(){
		logger.debug("createRiskLevel");
		success = false;
		RiskLevel riskLevelFind = retrieveRiskLevel(riskLevel.getRiskLevelCode());
		
		if (riskLevelFind == null){
			
			riskLevel.setRecordStatus(RecordStatus.Active);
			
			em.persist(riskLevel);
			em.flush();
			
			success = true;
		}else {
			if (riskLevelFind.getRecordStatus() == RecordStatus.Deleted){

				riskLevelFind.setRecordStatus(RecordStatus.Active);
				riskLevelFind.setRiskLevelDesc(riskLevel.getRiskLevelDesc());
				
				em.merge(riskLevelFind);
				em.flush();
				success = true;
			}else {
				success = false;
				errorCode = "0007";
			}
		}
	}
	
	public void updateRiskLevel(){
		logger.debug("updateRiskLevel");
		
		em.merge(riskLevel);
		em.flush();
		
		success = true;
	}
	
	public void deleteRiskLevel(RiskLevel riskLevelIn){
		logger.debug("deleteRiskLevel");
		success = false;
		String riskLevelCode = riskLevelIn.getRiskLevelCode();
		
		List<SampleItem> sampleItemActiveList = sampleItemListService.retrieveSampleItemListByRecordStatusRiskLevelCodeForValidation(RecordStatus.Active, riskLevelCode);
		
		if (sampleItemActiveList.size() == 0){
			
			List<TestFrequency> testFrequencyActiveList = testFrequencyListService.retrieveTestFrequencyListByRecordStatusRiskLevelCode(RecordStatus.Active, riskLevelCode);

			for (TestFrequency tf:testFrequencyActiveList){
				tf.setRecordStatus(RecordStatus.Deleted);
				em.merge(tf);
			}
			
			riskLevelIn.setRecordStatus(RecordStatus.Deleted);
			em.merge(riskLevelIn);
			em.flush();
			
			success = true;
		}else {
			success = false;
			errorCode = "0025";
		}
	}
	

	public boolean isSuccess() {
		return success;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	
	@Remove
	public void destroy(){
		if (riskLevel != null){
			riskLevel = null;
		}
	}
}
