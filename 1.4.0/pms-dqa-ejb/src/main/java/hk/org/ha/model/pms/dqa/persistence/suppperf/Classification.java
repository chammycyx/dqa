package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.suppperf.ProblemClassification;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "CLASSIFICATION")
@Customizer(AuditCustomizer.class)
public class Classification extends VersionEntity {

	private static final long serialVersionUID = -1021756901092609867L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "classificationSeq")
	@SequenceGenerator(name = "classificationSeq", sequenceName = "SEQ_CLASSIFICATION", initialValue=10000)
	@Id
	@Column(name="CLASSIFICATION_ID", nullable=false)
	private Long classificationId;

	@Converter(name = "Classification.classificationCode", converterClass = ProblemClassification.Converter.class )
	@Convert("Classification.classificationCode")
	@Column(name="CLASSIFICATION_CODE", length=3)
	private ProblemClassification classificationCode;
	
	@Column(name="ACTION_TAKEN", length=150)
	private String actionTaken;

	public Long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(Long classificationId) {
		this.classificationId = classificationId;
	}

	public ProblemClassification getClassificationCode() {
		return classificationCode;
	}

	public void setClassificationCode(ProblemClassification classificationCode) {
		this.classificationCode = classificationCode;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	
}
