package hk.org.ha.model.pms.dqa.biz;

import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("letterTemplateListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class LetterTemplateListServiceBean implements LetterTemplateListServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<LetterTemplate> letterTemplateList;

	@SuppressWarnings("unchecked")
	public void retrieveLetterTemplateListByModuleTypeRecordStatus(String moduleType, RecordStatus recordStatus){
		
		logger.debug("retrieveLetterTemplateListByModuleTypeRecordStatus");
		letterTemplateList = em.createNamedQuery("LetterTemplate.findByModuleTypeRecordStatus")
						 .setParameter("moduleType", moduleType)
						 .setParameter("recordStatus", recordStatus)
	                     .getResultList();
	}
	
    @Remove
	public void destroy(){	
		if ( letterTemplateList != null ) {
			letterTemplateList = null;
		}
	}

}
