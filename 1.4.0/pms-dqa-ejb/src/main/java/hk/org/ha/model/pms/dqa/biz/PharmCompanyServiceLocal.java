package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Company;

import javax.ejb.Local;


@Local
public interface PharmCompanyServiceLocal {
	
	Company findPharmCompanyByCompanyCode(String companyCode);
	
    void destroy();
}
