package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
import hk.org.ha.model.pms.vo.letter.LetterContent;

import javax.ejb.Local;

@Local
public interface LetterTemplateManagerLocal {
	LetterTemplate retrieveLetterTemplateByCode(String code);
	LetterContent retrieveLetterContentByCode(String code);
}
