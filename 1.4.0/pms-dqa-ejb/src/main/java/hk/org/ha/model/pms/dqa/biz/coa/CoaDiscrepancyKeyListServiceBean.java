package hk.org.ha.model.pms.dqa.biz.coa;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

import java.util.List;

@Stateful
@Scope(ScopeType.SESSION)
@Name("coaDiscrepancyKeyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@SuppressWarnings("unchecked")
public class CoaDiscrepancyKeyListServiceBean implements CoaDiscrepancyKeyListServiceLocal {
	
	@Logger
	private Log logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<CoaDiscrepancyKey> coaDiscrepancyKeyList;
	
	public void retrieveCoaDiscrepancyKeyList(){
		logger.debug("retrieveCoaDiscrepancyKeyList");
		coaDiscrepancyKeyList = retrieveCoaDiscrepancyKeyListForReport();		
	}
	
	public List<CoaDiscrepancyKey> retrieveCoaDiscrepancyKeyListForReport(){
		logger.debug("retrieveCoaDiscrepancyKeyListForReport");
		List<CoaDiscrepancyKey> resultList = em.createNamedQuery("CoaDiscrepancyKey.findAll")
		  										.setParameter("recordStatus", RecordStatus.Active)
		  										.getResultList();
		return resultList;
	}
	
	
	@Remove
	public void destroy(){
		if(coaDiscrepancyKeyList != null) {
			coaDiscrepancyKeyList = null;  
		}
	}
}
