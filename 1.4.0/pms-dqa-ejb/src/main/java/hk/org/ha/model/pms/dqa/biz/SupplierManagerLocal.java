package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Supplier;

import javax.ejb.Local;


@Local
public interface SupplierManagerLocal {
	
	Supplier retrieveSupplier(String supplierCode);
	
	Supplier retrieveSupplierForEmail( String supplierCode );
}
