package hk.org.ha.model.pms.dqa.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import hk.org.ha.model.pms.persistence.VersionEntity;


@Entity
@Table(name = "FUNC_SEQ_NUM")
public class FuncSeqNum extends VersionEntity{

	private static final long serialVersionUID = 5597424659609438574L;

	@Id
	@Column(name="FUNC_CODE", nullable = false)
	private String funcCode;
	
	@Column(name="FUNC_DESC", nullable = false)
	private String funcDesc;
	
	@Column(name="START_NUM", nullable = false)
	private Integer startNum;
	
	@Column(name="END_NUM", nullable = false)
	private Integer endNum;
	
	@Column(name="FUNC_PREFIX", nullable = false)
	private String funcPrefix;
	
	@Column(name="LAST_NUM", nullable = false)
	private Integer lastNum;
	
	@Column(name="YEAR")
	private Integer year;


	public void setFuncCode(String funcCode) {
		this.funcCode = funcCode;
	}

	public String getFuncCode() {
		return funcCode;
	}

	public String getFuncDesc() {
		return funcDesc;
	}

	public void setFuncDesc(String funcDesc) {
		this.funcDesc = funcDesc;
	}

	public Integer getStartNum() {
		return startNum;
	}

	public void setStartNum(Integer startNum) {
		this.startNum = startNum;
	}

	public Integer getEndNum() {
		return endNum;
	}

	public void setEndNum(Integer endNum) {
		this.endNum = endNum;
	}

	public String getFuncPrefix() {
		return funcPrefix;
	}

	public void setFuncPrefix(String funcPrefix) {
		this.funcPrefix = funcPrefix;
	}

	public void setLastNum(Integer lastNum) {
		this.lastNum = lastNum;
	}

	public Integer getLastNum() {
		return lastNum;
	}
	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}
