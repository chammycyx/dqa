package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.ArrayList;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.vo.sample.SampleItemFull;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("sampleItemFullRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class SampleItemFullRptServiceBean implements SampleItemFullRptServiceLocal {

	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String UNCHECKED = "unchecked";
	private static final String NA = "NA";
	private static final String MICROBIOLOGY_TEST = "MICRO";
	private static final String CHEMICAL_ANAYLYSIS = "CHEM";
	
	private List<SampleItemFull> sampleItemFullReportList;
	
	@SuppressWarnings(UNCHECKED)
	private List<SampleItem> retrieveSampleDataList(){
		
		List<SampleItem> dataList = em.createNamedQuery("SampleItem.findByRecordStatus")
			.setParameter("recordStatus", RecordStatus.Active)
			.getResultList();
		
		return dataList;
	}
	
	public void retrieveSampleItemFullList(){
    	logger.debug("retrieveSampleItemFullList");
    	
    	List<SampleItem> sampleItemList = retrieveSampleDataList();
    	sampleItemFullReportList = new ArrayList<SampleItemFull>();

    	if (sampleItemList.size() > 0){
	    	SampleItemFull sampleItemFull;
	    	
	    	for (SampleItem sampleItem : sampleItemList ){
		    	
	    		sampleItemFull = constructSampleItemFullData(sampleItem,
		    			 sampleItem.getExclusionTestList());
		    	
		    	if (sampleItemFull !=null){
		    		sampleItemFullReportList.add(sampleItemFull);
		    	}
	    	} 	
    	}	
    } 
   
	private SampleItemFull constructSampleItemFullData(SampleItem sampleItem,
    		List<ExclusionTest> exList){
    	SampleItemFull sampleItemFull = new SampleItemFull();
    	
    	sampleItemFull.setItemCode(sampleItem.getItemCode());
    	sampleItemFull.setItemDesc(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getFullDrugDesc());
    	sampleItemFull.setValidTestQtyReq(sampleItem.getValidTestQtyReqDesc());
    	
    	sampleItemFull.setCommodityGroup(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getCommodityGroup());
    	sampleItemFull.setBaseUnit(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getBaseUnit());
    	sampleItemFull.setOrderType(OrderType.findByDataValue(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getDmProcureInfo().getOrderType()));
    	sampleItemFull.setRegStatus(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getDqaDrugSupplier().getRegStatus());
    	sampleItemFull.setReleaseIndicator(sampleItem.getDmDrug()==null?"":sampleItem.getDmDrug().getDqaDrugSupplier().getReleaseIndicator());
    	sampleItemFull.setRiskLevelCode(sampleItem.getRiskLevel().getRiskLevelCode());
    	sampleItemFull.setRiskLevelDesc(sampleItem.getRiskLevel().getRiskLevelDesc());
    	sampleItemFull.setSpeicalCat(sampleItem.getSpeicalCat().getDataValue());
    	
    	sampleItemFull.setChemAnalysisQtyReq(sampleItem.getChemAnalysisQtyReq().getDataValue());
    	sampleItemFull.setChemAnalysisQtyReqDesc(sampleItem.getChemAnalysisQtyReqDesc());
    	sampleItemFull.setMicroBioTestQtyReq(sampleItem.getMicroBioTestQtyReq().getDataValue());
    	sampleItemFull.setMicroBioTestQtyReqDesc(sampleItem.getMicroBioTestQtyReqDesc());
    	sampleItemFull.setValidTestQtyReq(sampleItem.getValidTestQtyReq().getDataValue());
    	sampleItemFull.setValidTestQtyReqDesc(sampleItem.getValidTestQtyReqDesc());
    	sampleItemFull.setChemAnalysisRemark(sampleItem.getChemAnalysisRemark());
    	sampleItemFull.setMicroBioTestRemark(sampleItem.getMicroBioTestRemark());
    	sampleItemFull.setRemark(sampleItem.getRemark());
    	
    	sampleItemFull.setMicroBioTestExCode(NA);
    	sampleItemFull.setMicroBioTestExDesc(NA);
    	sampleItemFull.setChemAnalysisExCode(NA);
    	sampleItemFull.setChemAnalysisExDesc(NA);
		
    	for(ExclusionTest exTest : exList){
			if (exTest.getSampleTest().getTestCode().equals(CHEMICAL_ANAYLYSIS) ){
				sampleItemFull.setChemAnalysisExCode(exTest.getExclusionCode());
				sampleItemFull.setChemAnalysisExDesc(exTest.getExclusionDesc());
			}else if (exTest.getSampleTest().getTestCode().equals(MICROBIOLOGY_TEST)){
				sampleItemFull.setMicroBioTestExCode(exTest.getExclusionCode());
				sampleItemFull.setMicroBioTestExDesc(exTest.getExclusionDesc());
			}
		}
    	
    	return sampleItemFull;
    }
    
    public List<SampleItemFull> getSampleItemFullReportList(){
    	return sampleItemFullReportList;
    }
    
    @Remove
	public void destroy(){
    	sampleItemFullReportList = null;
	}

}
