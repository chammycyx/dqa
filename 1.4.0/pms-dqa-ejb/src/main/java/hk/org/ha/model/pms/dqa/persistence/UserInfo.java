package hk.org.ha.model.pms.dqa.persistence;

import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name = "UserInfo.findAll", query = "select o from UserInfo o order by o.userCode"),
	@NamedQuery(name = "UserInfo.findByUserCode", query = "select o from UserInfo o where o.userCode=:userCode")
})
@Table(name = "USER_INFO")
public class UserInfo extends VersionEntity {

	private static final long serialVersionUID = -7472705555645393472L;

	@Id
	@Column(name="USER_CODE", length = 6)
	private String userCode;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="CONTACT_ID")
	private Contact contact;
	
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}
}
