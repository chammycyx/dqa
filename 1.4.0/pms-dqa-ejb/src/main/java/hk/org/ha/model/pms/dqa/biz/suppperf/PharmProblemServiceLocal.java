package hk.org.ha.model.pms.dqa.biz.suppperf;
import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemNature;
import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemFileUploadData;

import javax.ejb.Local;

@Local
public interface PharmProblemServiceLocal {
	
	void addPharmProblem();
	
	void createPharmProblem(PharmProblem pharmProblemIn, 
								List<String> pharmBatchNumList, 
								List<String>countryList, 
								List<PharmProblemNature> pharmProblemNatureList,
								List<PharmProblemFileUploadData> pharmProblemFileUploadDataList,
								Boolean sendEmailBoolean);
	
	 void createUpdateDraftPharmProblem(PharmProblem pharmProblemIn, 
			List<String> pharmBatchNumList, 
			List<String> countryList, 
			List<PharmProblemNature> pharmProblemNatureList,
			List<PharmProblemFileUploadData> pharmProblemFileUploadDataList);
	 
	void retrievePharmProblemByPharmProblem(PharmProblem pharmProblemIn);
	
	void deletePharmProblem(PharmProblem pharmProblemIn, String deleteReason);
	
	void createPharmProblemRpt(PharmProblem pharmProblemIn);
	
	void generatePharmProblemRpt();
	
	boolean isSuccess();
	
	String getErrorCode();
	
	void destroy();

}
