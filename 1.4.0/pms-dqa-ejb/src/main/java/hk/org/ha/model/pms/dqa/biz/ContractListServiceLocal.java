package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.udt.OrderType;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;

import javax.ejb.Local;

@Local
public interface ContractListServiceLocal {

	void retrieveContractListForCoaLiveContractList(String contractNum, String itemCode);
	
	void retrieveContractListForSampleTest(String contractNum, String contractSuffix, OrderType orderType, String itemCode);
	
	void retrieveContractListLikeContractByItemCode(String contractNum, String contractSuffix, String itemCode);
	
	void retrieveContractListLikeContractByItemCodeOrderType(String contractNum, String contractSuffix, String itemCode, OrderTypeAll orderType);
	
    void destroy();
}
