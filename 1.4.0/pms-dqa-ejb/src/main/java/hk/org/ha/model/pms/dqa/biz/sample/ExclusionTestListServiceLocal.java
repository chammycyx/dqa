package hk.org.ha.model.pms.dqa.biz.sample;


import hk.org.ha.model.pms.dqa.persistence.sample.ExclusionTest;

import java.util.List;

import javax.ejb.Local;


@Local
public interface ExclusionTestListServiceLocal {
		
	void retrieveExclusionTestList();
	
	List<ExclusionTest> retrieveExclusionTestListByItemCode(String code);
	
    void destroy();
}
