package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleMovement;
import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;

import javax.ejb.Local;

@Local
public interface SampleMovementServiceLocal {
	
	void retrieveSampleMovement(SampleMovement sampleMovementIn);
	SampleMovement retrieveSampleMovementNewestByItemCode(String itemCode);
	void createSampleMovement(SampleMovement sampleMovementIn);
	void updateSampleMovement(SampleMovement sampleMovementIn);
	void createSampleMovementForReverseSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn);
	void createSampleMovementForNormalSchedule(SampleTestSchedule sampleTestScheduleIn, ScheduleStatus scheduleStatusIn);
	
	boolean isSuccess();
	
	void destroy(); 
}
