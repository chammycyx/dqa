package hk.org.ha.model.pms.dqa.biz.delaydelivery;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Contact;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormContact;
import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptFormPo;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
import hk.org.ha.model.pms.dqa.udt.Rank;
import hk.org.ha.model.pms.dqa.udt.delaydelivery.DelayDeliveryRptFormStatus;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormCriteria;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormData;
import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormPoData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.drools.util.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
@Stateful
@Scope(ScopeType.SESSION)
@Name("delayDeliveryRptFormListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class DelayDeliveryRptFormListServiceBean implements DelayDeliveryRptFormListServiceLocal  {
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	private static final String UNCHECKED = "unchecked";
	private static final String DATE_TIME_FORMAT = "dd-MMM-yyyy";
	private static final String CPO_ACTION_TARGET = "delayDeliveryRptFormCPOAction";
	private static final String CPO_ACTION_FULL = "full";
	private static final String VIEW_CLUSTER_TARGET = "delayDeliveryRptFormViewAllCluster";
	private static final String VIEW_CLUSTER_READ_ONLY = "read only";
	
	@Out(required = false)
	public boolean viewAllClusterFlag;
	
	
	
	@Out(required = false)
	public Integer delayDeliveryRptFromMaxMonthRange;
	
	
	@Out(required = false)
	public Integer delayDeliveryRptFromDefaultMonthRange;
	
	
	public List<DelayDeliveryRptForm> retrieveDraftDelayDeliveryRptFormList(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn ){
		logger.debug("retrieveDraftDelayDeliveryRptFormListByDelayDeliveryRptFormCriteria");
		
		StringBuffer delayDeliveryRptFormListSql = new StringBuffer();
		delayDeliveryRptFormListSql.append("select o from DelayDeliveryRptForm o "); 
		delayDeliveryRptFormListSql.append("where o.delayDeliveryRptFormVer.status = :delayDeliveryRptFormVerStatus ");
		
		if(checkInst(delayDeliveryRptFormCriteriaIn)){
			
			delayDeliveryRptFormListSql.append("and (o.institution.institutionCode in :institutionList) ");
		}
		
		if(checkItemCode(delayDeliveryRptFormCriteriaIn)){
			delayDeliveryRptFormListSql.append("and (o.contract.itemCode = :itemCode) ");
		}
		
		Query q = em.createQuery(delayDeliveryRptFormListSql.toString())
		.setParameter("delayDeliveryRptFormVerStatus", DelayDeliveryRptFormStatus.Drafted);
		
		if(checkItemCode(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("itemCode", delayDeliveryRptFormCriteriaIn.getItemCode());
		}
		
		if(checkInst(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("institutionList", delayDeliveryRptFormCriteriaIn.getInstitutionList());
		}
//		
		q=q.setHint(QueryHints.FETCH, "o.institution")
			.setHint(QueryHints.FETCH, "o.contract")
			.setHint(QueryHints.BATCH, "o.delayDeliveryRptFormVer");
		
		List<DelayDeliveryRptForm> resultList = q.getResultList();
		
		UamInfo uamInfo = (UamInfo) Contexts.getSessionContext().get("uamInfo");
		if(uamInfo.getPermissionMap().isEmpty()){
			viewAllClusterFlag = false;
		}else{
			if (VIEW_CLUSTER_READ_ONLY.equals(uamInfo.getPermissionMap().get(VIEW_CLUSTER_TARGET))){
				viewAllClusterFlag = true;
			}else{
				viewAllClusterFlag = false;
			}
		}
		
		
		if(!resultList.isEmpty())
		{
			for(DelayDeliveryRptForm d:resultList)
			{
				getLazyDelayDeliveryRptForm(d);
			}
			Collections.sort(resultList, new RefNumComparator());
			
		}
		else
		{
			resultList=null;
		}
		
		
		
		return resultList;
	
	}
	
	
	private void getLazyDelayDeliveryRptForm(DelayDeliveryRptForm d) {
		
		d.getInstitution();
		d.getContract();
		d.getContract().getSupplier();
		d.getContract().getManufacturer();
		d.getContract().getPharmCompany();
	}

	@SuppressWarnings("unchecked")
	public List<DelayDeliveryRptForm> retrieveDelayDeliveryRptFormList(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn ){
		logger.debug("retrieveDelayDeliveryRptFormListByDelayDeliveryRptFormCriteria");
		
		StringBuffer delayDeliveryRptFormListSql = new StringBuffer();
		delayDeliveryRptFormListSql.append("select o from DelayDeliveryRptForm o "); 
		delayDeliveryRptFormListSql.append("where (o.reportDate >= :fromReportDate ) ");
		delayDeliveryRptFormListSql.append("and (o.reportDate <= :toProblemDate ) ");
		
		if(checkInst(delayDeliveryRptFormCriteriaIn) && Boolean.FALSE.equals((delayDeliveryRptFormCriteriaIn.isAllInstitutionFlag()))){
			
			delayDeliveryRptFormListSql.append("and (o.institution.institutionCode in :institutionList ) ");
		}
		if(checkItemCode(delayDeliveryRptFormCriteriaIn)){
			
			delayDeliveryRptFormListSql.append("and (o.contract.itemCode = :itemCode ) ");
		}
		if(checkOrderType(delayDeliveryRptFormCriteriaIn)){
			
			delayDeliveryRptFormListSql.append("and (o.contract.contractType = :contractType ) ");
		}
		if(checkContractNum(delayDeliveryRptFormCriteriaIn)){
			delayDeliveryRptFormListSql.append("and (o.contract.contractNum= :contractNum ) ");
		}
		if(checkContractSuffix(delayDeliveryRptFormCriteriaIn)){

			delayDeliveryRptFormListSql.append("and (o.contract.contractSuffix= :contractSuffix ) ");
		}
		if(checkStatus(delayDeliveryRptFormCriteriaIn)){
			
			delayDeliveryRptFormListSql.append("and (o.delayDeliveryRptFormVer.status in :delayDeliveryRptFormStatusList ) ");
		}
	
		delayDeliveryRptFormListSql.append("order by o.createDate desc ");
		
		Calendar c = Calendar.getInstance();
		c.setTime(delayDeliveryRptFormCriteriaIn.getToReportDate());
		c.add(Calendar.DATE,1);
		
		Query q = em.createQuery(delayDeliveryRptFormListSql.toString())
		.setParameter("fromReportDate", delayDeliveryRptFormCriteriaIn.getFromReportDate(), TemporalType.DATE)
		.setParameter("toProblemDate", c.getTime(), TemporalType.DATE);

		if(checkInst(delayDeliveryRptFormCriteriaIn) && Boolean.FALSE.equals(delayDeliveryRptFormCriteriaIn.isAllInstitutionFlag())){
			
			q=q.setParameter("institutionList", delayDeliveryRptFormCriteriaIn.getInstitutionList());
			
		}
		if(checkItemCode(delayDeliveryRptFormCriteriaIn)){
			
			q=q.setParameter("itemCode", delayDeliveryRptFormCriteriaIn.getItemCode());
		}			
		if(checkOrderType(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("contractType", delayDeliveryRptFormCriteriaIn.getOrderType().getDataValue());
		}
		if(checkContractNum(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("contractNum", delayDeliveryRptFormCriteriaIn.getContractNum());
		}
		if(checkContractSuffix(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("contractSuffix", delayDeliveryRptFormCriteriaIn.getContractSuffix());
		}
		
		if(checkStatus(delayDeliveryRptFormCriteriaIn)){
			q=q.setParameter("delayDeliveryRptFormStatusList", delayDeliveryRptFormCriteriaIn.getDelayDeliveryRptFormStatusList());
		}
		
		
		q=q.setHint(QueryHints.FETCH, "o.institution");
		q=q.setHint(QueryHints.FETCH, "o.contract");
		q=q.setHint(QueryHints.BATCH, "o.delayDeliveryRptFormVer");
		
		
//		
		List<DelayDeliveryRptForm> resultList = q.getResultList();
		
		if(resultList!=null && resultList.size()>0)
		{
			
			for(DelayDeliveryRptForm pc:resultList)
			{
				getLazyDelayDeliveryRptForm(pc);
			}
			
			Collections.sort(resultList, new RefNumComparator());
			
		}
		else
		{
			resultList=null;
		}
		
		return resultList;
		
	}
	
	


	@Override
	public 	List<DelayDeliveryRptFormContactData>  retrieveDelayDeliveryRptFormContactList(
			DelayDeliveryRptForm delayDeliveryRptFormIn) {
		StringBuffer delayDeliveryRptFormContactListSql = new StringBuffer();
		
		delayDeliveryRptFormContactListSql.append("select o from DelayDeliveryRptFormContact o "); 
		delayDeliveryRptFormContactListSql.append("where o.delayDeliveryRptForm.refNum = :refNum ");
		delayDeliveryRptFormContactListSql.append("order by o.priority ");
		
		Query q = em.createQuery(delayDeliveryRptFormContactListSql.toString())
		.setParameter("refNum", delayDeliveryRptFormIn.getRefNum());
		

		
		List<DelayDeliveryRptFormContact> resultList = q.getResultList();
		
		if(!resultList.isEmpty())
		{
			return constructDelayDeliveryRptFormContactData(resultList);
		}
		else
		{
			return null;
		}
		
	}


	


	@Override
	public 	List<DelayDeliveryRptFormPoData>  retrieveDelayDeliveryRptFormPoList(
			DelayDeliveryRptForm delayDeliveryRptFormIn, String baseUnit) {
		StringBuffer delayDeliveryRptFormPoListSql = new StringBuffer();
		delayDeliveryRptFormPoListSql.append("select o from DelayDeliveryRptFormPo o "); 
		delayDeliveryRptFormPoListSql.append("where o.delayDeliveryRptForm.refNum = :refNum ");
		delayDeliveryRptFormPoListSql.append("order by o.poNum ");
		
		Query q = em.createQuery(delayDeliveryRptFormPoListSql.toString())
		.setParameter("refNum", delayDeliveryRptFormIn.getRefNum());
		
		
		
		List<DelayDeliveryRptFormPo> resultList = q.getResultList();
		
		
		
		
		
		if(!resultList.isEmpty())
		{
			return constructDelayDeliveryRptFormPoData(resultList, baseUnit);
		}
		else
		{
			return null;
		}
		
		
	}


	
	@Override
	public 	List<DelayDeliveryRptFormContactData>  retrieveDelayDeliveryRptFormContactQuickAddList(
			Institution institution) {
		

		StringBuffer delayDeliveryRptFormContactListSql = new StringBuffer();
		
		delayDeliveryRptFormContactListSql.append("select o from DelayDeliveryRptForm o "); 
		delayDeliveryRptFormContactListSql.append("where o.institution.institutionCode = :institutionCode ");
		delayDeliveryRptFormContactListSql.append("and o.delayDeliveryRptFormVer.status not in :statusList order by o.reportDate desc");
		
		
		List<DelayDeliveryRptFormStatus> notSubmittedStatusList = new ArrayList<DelayDeliveryRptFormStatus>();
		notSubmittedStatusList.add(DelayDeliveryRptFormStatus.Drafted);
		notSubmittedStatusList.add(DelayDeliveryRptFormStatus.Deleted);
		
		Query q = em.createQuery(delayDeliveryRptFormContactListSql.toString())
		.setParameter("institutionCode", institution.getInstitutionCode());
		
		q = q.setParameter("statusList", notSubmittedStatusList);
		

		List<DelayDeliveryRptForm> resultList = q.getResultList();
		
		q=q.setHint(QueryHints.FETCH, "o.institution");
		
		if(!resultList.isEmpty())
		{
			if(resultList.size() >5){
				resultList = resultList.subList(0, 5);
			}
			List<DelayDeliveryRptFormContactData> contactList = new ArrayList<DelayDeliveryRptFormContactData>();
			for(DelayDeliveryRptForm d : resultList){
				contactList.addAll(constructDelayDeliveryRptFormContactData(d.getDelayDeliveryRptFormContactList()));
			}
			return contactList;
		}
		else
		{
			return null;
		}
		
	}
	

	
	
	
	@Override
	public 	DelayDeliveryRptFormData  retrieveDelayDeliveryRptFormData(DelayDeliveryRptForm delayDeliveryRptFormIn) {

		if(delayDeliveryRptFormIn == null){
			
			return  new DelayDeliveryRptFormData();
		}
		
		StringBuffer delayDeliveryRptFormListSql = new StringBuffer();
		
		delayDeliveryRptFormListSql.append("select o from DelayDeliveryRptForm o "); 
		delayDeliveryRptFormListSql.append("where o.refNum = :refNum ");
		
		Query q = em.createQuery(delayDeliveryRptFormListSql.toString())
		.setParameter("refNum", delayDeliveryRptFormIn.getRefNum());
		
		q=q.setHint(QueryHints.FETCH, "o.institution")
			.setHint(QueryHints.FETCH, "o.contract")
			.setHint(QueryHints.BATCH, "o.delayDeliveryRptFormVer");
		
		List<DelayDeliveryRptForm> resultList = q.getResultList();
		
		
		
		if(!resultList.isEmpty())
		{
			DelayDeliveryRptForm d = ((DelayDeliveryRptForm) resultList.get(0));
			
			List<DelayDeliveryRptFormContactData> clist =  retrieveDelayDeliveryRptFormContactList(delayDeliveryRptFormIn);
			List<DelayDeliveryRptFormPoData> plist = retrieveDelayDeliveryRptFormPoList(delayDeliveryRptFormIn, d.getContract().getBaseUnit());
			return constructDelayDeliveryRptFormData(d,clist,plist);
		}
		else
		{
			
			return new DelayDeliveryRptFormData();
			
		}
		
		
		
	}
	
	private List<DelayDeliveryRptFormContactData> constructDelayDeliveryRptFormContactData(
			List<DelayDeliveryRptFormContact> delayDeliveryRptFormContactList) {
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList = new ArrayList<DelayDeliveryRptFormContactData>();
		for(DelayDeliveryRptFormContact d: delayDeliveryRptFormContactList){
			Contact c = d.getContact();
			DelayDeliveryRptFormContactData delayDeliveryRptFormContactData = new DelayDeliveryRptFormContactData();
			
			if(c!=null){
				delayDeliveryRptFormContactData.setContactId(c.getContactId());
				delayDeliveryRptFormContactData.setEmail(c.getEmail());
				delayDeliveryRptFormContactData.setFirstName(c.getFirstName());
				delayDeliveryRptFormContactData.setLastName(c.getLastName());
				delayDeliveryRptFormContactData.setOfficePhone(c.getOfficePhone());
				delayDeliveryRptFormContactData.setPosition(Rank.valueOf(c.getPosition()));
				delayDeliveryRptFormContactData.setPriority(d.getPriority());
				delayDeliveryRptFormContactData.setTitle(c.getTitle());
			}
			
			delayDeliveryRptFormContactData.setRefNum(d.getDelayDeliveryRptForm().getRefNum());
			
			delayDeliveryRptFormContactDataList.add(delayDeliveryRptFormContactData);
		}
		return delayDeliveryRptFormContactDataList;
	}
	
	private List<DelayDeliveryRptFormPoData> constructDelayDeliveryRptFormPoData(
			List<DelayDeliveryRptFormPo> delayDeliveryRptFormPoList, String baseUnit) {
		List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList = new ArrayList<DelayDeliveryRptFormPoData>();
		for(DelayDeliveryRptFormPo d: delayDeliveryRptFormPoList){
			DelayDeliveryRptFormPoData delayDeliveryRptFormPoData = new DelayDeliveryRptFormPoData();
			delayDeliveryRptFormPoData.setApproveDate(d.getApproveDate());
			delayDeliveryRptFormPoData.setPackSize(d.getPackSize() == null? "": d.getPackSize());
			delayDeliveryRptFormPoData.setOrderQty(d.getOrderQty());
			delayDeliveryRptFormPoData.setOutstandQty(d.getOutstandQty());
			delayDeliveryRptFormPoData.setPoNum(d.getPoNum());
			delayDeliveryRptFormPoData.setQuotationLeadTime(d.getQuotationLeadTime());
			delayDeliveryRptFormPoData.setDelayDeliveryRptFormPoId(d.getDelayDeliveryRptFormPoId());
			delayDeliveryRptFormPoData.setFileItem(d.getFileId());
			delayDeliveryRptFormPoData.setFileData(null);
			delayDeliveryRptFormPoData.setRefUploadFileName("");
			delayDeliveryRptFormPoData.setBaseUnit(baseUnit);
			delayDeliveryRptFormPoData.setSupplierReplyDate(d.getSupplierReplyDate());
			delayDeliveryRptFormPoDataList.add(delayDeliveryRptFormPoData);
		}
		
		
		return delayDeliveryRptFormPoDataList;
	}


	
	private DelayDeliveryRptFormData constructDelayDeliveryRptFormData(DelayDeliveryRptForm d, 
			List<DelayDeliveryRptFormContactData> delayDeliveryRptFormContactDataList, 
			List<DelayDeliveryRptFormPoData> delayDeliveryRptFormPoDataList) {
		
		DelayDeliveryRptFormData delayDeliveryRptFormData = new DelayDeliveryRptFormData();
		
		delayDeliveryRptFormData.setAvgMthConsumptQty(d.getAvgMthConsumptQty());
		delayDeliveryRptFormData.setConflictByName(d.getConflictByName());
		delayDeliveryRptFormData.setConflictByRank(d.getConflictByRank());
		delayDeliveryRptFormData.setConflictFlag(d.getConflictFlag());
		delayDeliveryRptFormData.setConflictMspName(d.getConflictMspName());
		delayDeliveryRptFormData.setConflictRelationship(d.getConflictRelationship());
		delayDeliveryRptFormData.setFileItem(d.getConflictFile());
		
		delayDeliveryRptFormData.setContractId(d.getContract().getContractId());
		delayDeliveryRptFormData.setContractNum(d.getContract().getContractNum());
		delayDeliveryRptFormData.setContractSuffix(d.getContract().getContractSuffix());
		delayDeliveryRptFormData.setDelayDeliveryRptFormStatus(d.getDelayDeliveryRptFormVer().getStatus());
		delayDeliveryRptFormData.setItemCode(d.getContract().getItemCode());
		delayDeliveryRptFormData.setFullDrugDesc(d.getContract().getDmDrug().getFullDrugDesc());
		delayDeliveryRptFormData.setBaseUnit(d.getContract().getDmDrug().getBaseUnit());
		delayDeliveryRptFormData.setVersion(d.getVersion());
		if(d.getContract().getManufacturer() != null){
			delayDeliveryRptFormData.setManufCode(d.getContract().getManufacturer().getCompanyCode());
		}
		if(d.getContract().getSupplier() != null){
			delayDeliveryRptFormData.setSupplierCode(d.getContract().getSupplier().getSupplierCode());
		}
		
		delayDeliveryRptFormData.setOnHandLastFor(d.getOnHandLastFor());
		delayDeliveryRptFormData.setOnHandQty(d.getOnHandQty());
		
		OrderTypeAll orderTypeAll = OrderTypeAll.findByDataValue(d.getContract().getContractType());
		if(orderTypeAll != null){
			delayDeliveryRptFormData.setOrderType(orderTypeAll);
		}
		
		delayDeliveryRptFormData.setRefNum(d.getRefNum());
		delayDeliveryRptFormData.setReportDate(d.getReportDate());
		delayDeliveryRptFormData.setInstitution(d.getInstitution());
		
		

		UamInfo uamInfo = (UamInfo) Contexts.getSessionContext().get("uamInfo");
		if(uamInfo.getPermissionMap().isEmpty()){
			delayDeliveryRptFormData.setCpoActionFlag(false);
		}else{
			if (CPO_ACTION_FULL.equals(uamInfo.getPermissionMap().get(CPO_ACTION_TARGET))){
				delayDeliveryRptFormData.setCpoActionFlag(true);
			}else{
				delayDeliveryRptFormData.setCpoActionFlag(false);
				
			}
		}
		
		delayDeliveryRptFormData.setDelayDeliveryRptFormContactDataList(delayDeliveryRptFormContactDataList);
		delayDeliveryRptFormData.setDelayDeliveryRptFormPoDataList(delayDeliveryRptFormPoDataList);
		
		return delayDeliveryRptFormData;
	}
	
	

	public static class RefNumComparator implements Comparator<DelayDeliveryRptForm>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		public int compare(DelayDeliveryRptForm c1, DelayDeliveryRptForm c2){
			return c1.getRefNum()
			.compareTo( 
					c2.getRefNum()
			);
		}						
	}
	
	
	
	private boolean checkInst(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return !delayDeliveryRptFormCriteriaIn.getInstitutionList().isEmpty(); 	
	}
	
	private boolean checkItemCode(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return !StringUtils.isEmpty(delayDeliveryRptFormCriteriaIn.getItemCode()); 	
	}
	private boolean checkOrderType(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return delayDeliveryRptFormCriteriaIn.getOrderType()!= null; 	
	}
	
	private boolean checkContractNum(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return !StringUtils.isEmpty(delayDeliveryRptFormCriteriaIn.getContractNum()); 	
	}
	private boolean checkContractSuffix(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return !StringUtils.isEmpty(delayDeliveryRptFormCriteriaIn.getContractSuffix()); 	
	}
	private boolean checkStatus(DelayDeliveryRptFormCriteria delayDeliveryRptFormCriteriaIn){
		
		return !delayDeliveryRptFormCriteriaIn.getDelayDeliveryRptFormStatusList().isEmpty(); 	
	}
	
	@Remove
	public void destroy(){
		
	}


	@Override
	public DelayDeliveryRptFormContactData retrieveDelayDeliveryRptFormContact(
			DelayDeliveryRptFormContactData delayDeliveryRptFormContactData) {
		

		StringBuffer delayDeliveryRptFormContactListSql = new StringBuffer();
		
		delayDeliveryRptFormContactListSql.append("select o from DelayDeliveryRptFormContact o "); 
		delayDeliveryRptFormContactListSql.append("where o.contact.email = :email ");
		
		Query q = em.createQuery(delayDeliveryRptFormContactListSql.toString())
		.setParameter("email", delayDeliveryRptFormContactData.getEmail());
		

		List<DelayDeliveryRptFormContact> resultList = q.getResultList();
		
		q=q.setHint(QueryHints.FETCH, "o.contact");
		
		if(!resultList.isEmpty())
		{
			return constructDelayDeliveryRptFormContactData(resultList).get(0);
		}
		else
		{
			return delayDeliveryRptFormContactData;
		}
		
	}
	
	
	
	


}
