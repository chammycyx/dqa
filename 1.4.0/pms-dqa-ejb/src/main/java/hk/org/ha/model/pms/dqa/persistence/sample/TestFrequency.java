package hk.org.ha.model.pms.dqa.persistence.sample;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.Customizer;
import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;
import hk.org.ha.model.pms.dqa.udt.sample.Frequency;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name = "TEST_FREQUENCY")
@NamedQueries({
	@NamedQuery(name = "TestFrequency.findAll", query = "select o from TestFrequency o order by o.sampleTest.testCode, o.orderType "),
	@NamedQuery(name = "TestFrequency.findByRecordStatusRiskLevelCode", query = "select o from TestFrequency o where o.recordStatus = :recordStatus and o.riskLevel.riskLevelCode = :riskLevelCode order by o.sampleTest.testCode, o.orderType, o.riskLevel.riskLevelCode "),
	@NamedQuery(name = "TestFrequency.findByRecordStatus", query = "select o from TestFrequency o LEFT OUTER JOIN o.riskLevel r  where o.recordStatus = :recordStatus order by o.sampleTest.testCode, o.orderType, r.riskLevelCode" ),
	
	@NamedQuery(name = "TestFrequency.findByRecordStatusTestCodeOrderTypeNullRiskLevel", query = "select o from TestFrequency o where o.recordStatus = :recordStatus and o.sampleTest.testCode = :testCode and o.orderType = :orderType and o.riskLevel is null"),
	@NamedQuery(name = "TestFrequency.findByRecordStatusTestCodeRiskLevelCodeNullOrderType", query = "select o from TestFrequency o where o.recordStatus = :recordStatus and o.sampleTest.testCode = :testCode and o.orderType is null and o.riskLevel.riskLevelCode = :riskLevelCode "),
	@NamedQuery(name = "TestFrequency.findByOrderTypeRiskLevelCodeTestCode", query = "select o from TestFrequency o where o.orderType = :orderType and o.riskLevel.riskLevelCode = :riskLevelCode and  o.sampleTest.testCode = :testCode"),
	@NamedQuery(name = "TestFrequency.findByRecordStatusOrderTypeRiskLevelCodeTestCode", query = "select o from TestFrequency o where o.recordStatus = :recordStatus and o.orderType = :orderType and o.riskLevel.riskLevelCode = :riskLevelCode and  o.sampleTest.testCode = :testCode"),
	
	@NamedQuery(name = "TestFrequency.findByRecordStatusTestCodeOrderType", query = "select o from TestFrequency o where o.recordStatus = :recordStatus and o.sampleTest.testCode = :testCode and (o.orderType = :orderType or o.orderType is null)"),
	@NamedQuery(name = "TestFrequency.findByRecordStatusTestCodeRiskLevel", query = "select o from TestFrequency o LEFT OUTER JOIN o.riskLevel r where o.recordStatus = :recordStatus and o.sampleTest.testCode = :testCode and (r is null or r.riskLevelCode = :riskLevelCode)"),

	@NamedQuery(name = "TestFrequency.findByTestCodeOrderTypeNullRiskLevel", query = "select o from TestFrequency o where o.sampleTest.testCode = :testCode and o.orderType = :orderType and o.riskLevel is null"),
	@NamedQuery(name = "TestFrequency.findByTestCodeRiskLevelNullOrderType", query = "select o from TestFrequency o where o.sampleTest.testCode = :testCode and o.orderType is null and o.riskLevel.riskLevelCode = :riskLevelCode"),
	@NamedQuery(name = "TestFrequency.findByFrequencyId", query = "select o from TestFrequency o where o.frequencyId = :frequencyId"),
	
	@NamedQuery(name = "TestFrequency.findByTestCodeOrderTypeFullPercentage", query = "select o from TestFrequency o where o.sampleTest.testCode = :testCode " +
																					  " and ((o.orderType = :orderType and o.riskLevel is not null and o.riskLevel.proceedSampleTestFlag=:proceedSampleTestFlag and o.riskLevel.recordStatus=:recordStatus ) " +
																					  " or (o.orderType is null and o.riskLevel is not null and o.riskLevel.proceedSampleTestFlag=:proceedSampleTestFlag and o.riskLevel.recordStatus=:recordStatus ) " +
																					  " or (o.orderType = :orderType and o.riskLevel is null)) " +
																					  " and o.percentage = :percentage and o.recordStatus=:recordStatus "),
																					  
	@NamedQuery(name = "TestFrequency.findByTestCodeOrderTypeNotFullPercentage", query = "select o from TestFrequency o where o.sampleTest.testCode = :testCode " +
																					  " and ((o.orderType = :orderType and o.riskLevel is not null and o.riskLevel.proceedSampleTestFlag=:proceedSampleTestFlag and o.riskLevel.recordStatus=:recordStatus ) " +
																					  " or (o.orderType is null and o.riskLevel is not null and o.riskLevel.proceedSampleTestFlag=:proceedSampleTestFlag and o.riskLevel.recordStatus=:recordStatus ) " +
																					  " or (o.orderType = :orderType and o.riskLevel is null)) " +
																					  " and o.percentage <> :percentage and o.recordStatus=:recordStatus ")
	
})

@Customizer(AuditCustomizer.class)
public class TestFrequency extends VersionEntity {

	private static final long serialVersionUID = 1433264623863270681L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "testFrequencySeq")
	@SequenceGenerator(name = "testFrequencySeq", sequenceName = "SEQ_TEST_FREQUENCY", initialValue=10000)
	@Id
	@Column(name="FREQUENCY_ID")
	private Long frequencyId;

	@Column(name="ORDER_TYPE", length=1, nullable=true) // accept Null('ALL' type)
	private String orderType;
	
	@ManyToOne
	@JoinColumn(name="RISK_LEVEL_CODE", nullable=true)
	private RiskLevel riskLevel;

	@ManyToOne
	@JoinColumn(name="TEST_CODE")
	private SampleTest sampleTest;

	@Column(name="PERCENTAGE")
	private Integer percentage;

	@Converter(name = "TestFrequency.frequencyPerYear", converterClass = Frequency.Converter.class )
	@Convert("TestFrequency.frequencyPerYear")
	@Column(name="FREQUENCY_PER_YEAR")
	private Frequency frequencyPerYear;
	
	@Converter(name = "TestFrequency.recordStatus", converterClass = RecordStatus.Converter.class )
	@Convert("TestFrequency.recordStatus")
	@Column(name="RECORD_STATUS")
    private RecordStatus recordStatus;

	public Long getFrequencyId() {
		return frequencyId;
	}

	public void setFrequencyId(Long frequencyId) {
		this.frequencyId = frequencyId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public RiskLevel getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}

	public SampleTest getSampleTest() {
		return sampleTest;
	}

	public void setSampleTest(SampleTest sampleTest) {
		this.sampleTest = sampleTest;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Frequency getFrequencyPerYear() {
		return frequencyPerYear;
	}

	public void setFrequencyPerYear(Frequency frequencyPerYear) {
		this.frequencyPerYear = frequencyPerYear;
	}

	public RecordStatus getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(RecordStatus recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	
	
}
