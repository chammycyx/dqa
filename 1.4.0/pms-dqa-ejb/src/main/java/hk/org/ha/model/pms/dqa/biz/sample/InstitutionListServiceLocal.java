package hk.org.ha.model.pms.dqa.biz.sample;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
import hk.org.ha.model.pms.dqa.udt.RecordStatus;

import javax.ejb.Local;

@Local
public interface InstitutionListServiceLocal {
	
	void retreiveInstitutionListByPcuFlag(YesNoFlag pcuFlag);
	
	void retrieveInstitutionListByPcuFlagRecordStatus(YesNoFlag pcuFlag, RecordStatus recordStatus);

	void destroy();
}
