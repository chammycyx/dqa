package hk.org.ha.model.pms.dqa.udt.sample;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum InventorySelectionType implements StringValuedEnum {
	
	Blank(" ","ALL"),
	I("I", "Show Institution Only"),
	L("L", "Show Lab Only");
	
	
    private final String dataValue;
    private final String displayValue;
        
    InventorySelectionType(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }        
    
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }    
        
	public static class Converter extends StringValuedEnumConverter<InventorySelectionType> {

		private static final long serialVersionUID = -1623620317837922548L;

		@Override
    	public Class<InventorySelectionType> getEnumClass() {
    		return InventorySelectionType.class;
    	}
    }
}
