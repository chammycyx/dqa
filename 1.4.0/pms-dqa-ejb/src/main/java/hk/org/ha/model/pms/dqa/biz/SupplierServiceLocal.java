package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Supplier;

import javax.ejb.Local;


@Local
public interface SupplierServiceLocal {
	
	void retrieveSupplierBySupplierCode(String code);
	
	Supplier retrieveSupplierForErp( String supplierCode );
	
	Supplier retrieveSupplierForPhs( String supplierCode );
	
	Supplier findSupplierBySupplierCode( String supplierCode);
	
    void destroy();
}
