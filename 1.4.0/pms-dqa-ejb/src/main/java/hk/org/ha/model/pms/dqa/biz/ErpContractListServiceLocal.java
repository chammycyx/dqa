package hk.org.ha.model.pms.dqa.biz;
import hk.org.ha.model.pms.dqa.persistence.Contract;
import hk.org.ha.model.pms.dqa.udt.ModuleType;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ErpContractListServiceLocal {

	List<Contract> retrieveContractListByContractNumModuleType( String contractNum, ModuleType moduleType);
		
	List<Contract> retrieveContractListByContractNumItemCodeModuleType( String contractNum, String itemCode, ModuleType moduleType);
	
	void updateContractListForSuspension( hk.org.ha.model.pms.dqa.vo.Contract ch, ModuleType moduleType );
	
	void updateContractListForSampleTest( hk.org.ha.model.pms.dqa.vo.Contract ch); 
	
}
