package hk.org.ha.model.pms.dqa.biz;

import java.util.List;
import java.util.Locale;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.dqa.persistence.Company;
import hk.org.ha.model.pms.dqa.udt.YesNoFlag;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("companyListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class CompanyListServiceBean implements CompanyListServiceLocal {
	
	@Logger
	private Log logger;

	@PersistenceContext
	private EntityManager em;
	
	@Out(required = false)
	private List<Company> companyList;
	
	@SuppressWarnings("unchecked")
	public void retrieveManufacturerList(){
		logger.debug("retrieveManufacturerList");
		companyList = em.createNamedQuery("Company.findByManufFlag")
		.setParameter("manufFlag", YesNoFlag.Yes)
		.setHint(QueryHints.LEFT_FETCH, "o.contact")
		.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public void retrieveManufacturerListLike(String manufacturerCode){
		logger.debug("retrieveManufacturerListLike");
		companyList = em.createNamedQuery("Company.findLikeManufacturerCode")
		.setParameter("manufacturerCode", manufacturerCode+"%")
		.setParameter("manufFlag", YesNoFlag.Yes)
		.setHint(QueryHints.LEFT_FETCH, "o.contact")
		.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public void retrieveCompanyListLikeCompanyCode(String companyCode) {
		logger.debug("retrieveCompanyListLikeCompanyCode");
		companyList = em.createNamedQuery("Company.findLikeCompanyCode")
		.setParameter("companyCode", companyCode.trim().toUpperCase(Locale.ENGLISH)+"%").getResultList();
	}
		
	@Remove
	public void destroy(){
		if ( companyList != null ){
			companyList = null;
		}
	}
}
