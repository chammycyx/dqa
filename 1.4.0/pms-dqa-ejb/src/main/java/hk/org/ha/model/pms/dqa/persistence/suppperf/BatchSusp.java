package hk.org.ha.model.pms.dqa.persistence.suppperf;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "BATCH_SUSP")
@NamedQueries({
	@NamedQuery(name = "BatchSusp.findByBatchSuspId", query = "select o from BatchSusp o where o.batchSuspId = :batchSuspId and o.qaBatchNum.qaProblem.recordStatus = :recordStatus "),
	@NamedQuery(name = "BatchSusp.findByBatchSuspCriteria", query = "select o from BatchSusp o where o.institution.institutionCode = :institutionCode " +
															"and o.qaBatchNum.qaProblem.caseNum = :caseNum " +
															"and o.qaBatchNum.qaProblem.recordStatus = :recordStatus "),
	@NamedQuery(name = "BatchSusp.findByInstitutionCodeCaseNumBatchNum", query = "select o from BatchSusp o where o.institution.institutionCode = :institutionCode " +
															"and o.qaBatchNum.qaProblem.caseNum = :caseNum " +
															"and o.qaBatchNum.batchNum = :batchNum " +
															"and o.qaBatchNum.qaProblem.recordStatus = :recordStatus ")
															
	
})
@Customizer(AuditCustomizer.class)
public class BatchSusp extends VersionEntity {

	private static final long serialVersionUID = -7160157512699823979L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batchSuspSeq")
	@SequenceGenerator(name = "batchSuspSeq", sequenceName = "SEQ_BATCH_SUSP", initialValue=10000)
	@Id
	@Column(name="BATCH_SUSP_ID", nullable=false)
	private Long batchSuspId;
	
	@Column(name="BATCH_SUSP_QTY_ON_HAND")
    private Integer batchSuspQtyOnHand;
	
	
	@ManyToOne
	@JoinColumn(name="INSTITUTION_CODE")
	private Institution institution;
	
	@ManyToOne
	@JoinColumn(name="QA_BATCH_NUM_ID")
	private QaBatchNum qaBatchNum;
	

	public Long getBatchSuspId() {
		return batchSuspId;
	}

	public void setBatchSuspId(Long batchSuspId) {
		this.batchSuspId = batchSuspId;
	}

	public Integer getBatchSuspQtyOnHand() {
		return batchSuspQtyOnHand;
	}

	public void setBatchSuspQtyOnHand(Integer batchSuspQtyOnHand) {
		this.batchSuspQtyOnHand = batchSuspQtyOnHand;
	}
	
	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public QaBatchNum getQaBatchNum() {
		return qaBatchNum;
	}

	public void setQaBatchNum(QaBatchNum qaBatchNum) {
		this.qaBatchNum = qaBatchNum;
	}

	
}	