package hk.org.ha.model.pms.dqa.vo.suppperf;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class FaxDetailLogRpt {
	private String caseStatus;
	private String caseNum;
	private String refNum;
	private String classification;
	private String itemCode;
	private String itemDesc;
	private String orderType;
	private String supplier;
	private String manuf;
	private String pharmCom;
	private String faxType;
	private String from;
	private String to;
	private String problemDetail;
	@Temporal(TemporalType.TIMESTAMP)
	private Date caseCreateDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date faxGenerationDate;
	
	public String getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	public String getCaseNum() {
		return caseNum;
	}
	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getManuf() {
		return manuf;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getPharmCom() {
		return pharmCom;
	}
	public void setPharmCom(String pharmCom) {
		this.pharmCom = pharmCom;
	}
	public String getFaxType() {
		return faxType;
	}
	public void setFaxType(String faxType) {
		this.faxType = faxType;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getProblemDetail() {
		return problemDetail;
	}
	public void setProblemDetail(String problemDetail) {
		this.problemDetail = problemDetail;
	}
	public Date getCaseCreateDate() {
		return (caseCreateDate!=null)?new Date(caseCreateDate.getTime()):null;
	}
	public void setCaseCreateDate(Date caseCreateDate) {
		if(caseCreateDate!=null)
		{
			this.caseCreateDate = new Date(caseCreateDate.getTime());
		}
	}
	public Date getFaxGenerationDate() {
		return (faxGenerationDate!=null)?new Date(faxGenerationDate.getTime()):null;
	}
	public void setFaxGenerationDate(Date faxGenerationDate) {
		if(faxGenerationDate!=null)
		{
			this.faxGenerationDate = new Date(faxGenerationDate.getTime());
		}
	}
	
}