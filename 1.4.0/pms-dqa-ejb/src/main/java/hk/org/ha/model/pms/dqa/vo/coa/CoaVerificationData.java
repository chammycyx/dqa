package hk.org.ha.model.pms.dqa.vo.coa;

import hk.org.ha.model.pms.dqa.udt.OrderType;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class CoaVerificationData implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long coaBatchId;
	private String itemCode;
	private String fullDrugDesc;	
	private String supplierCode;
	private String supplierName;
	private String contractNum;
	private String contractSuffix;
	private String batchNum;
	private Date createDate;
	private OrderType orderType;	
	
	public Long getCoaBatchId() {
		return coaBatchId;
	}
	public void setCoaBatchId(Long coaBatchId) {
		this.coaBatchId = coaBatchId;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setFullDrugDesc(String fullDrugDesc) {
		this.fullDrugDesc = fullDrugDesc;
	}
	public String getFullDrugDesc() {
		return fullDrugDesc;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public void setContractSuffix(String contractSuffix) {
		this.contractSuffix = contractSuffix;
	}
	public String getContractSuffix() {
		return contractSuffix;
	}
	public String getBatchNum() {
		return batchNum;
	}
	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}
