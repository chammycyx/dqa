package hk.org.ha.model.pms.dqa.biz.sample;

import java.util.List;

import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
import hk.org.ha.model.pms.dqa.vo.sample.InstAssignmentCriteria;

import javax.ejb.Local;

@Local
public interface SampleTestScheduleListInstAssignCriteriaServiceLocal {
	
	void retrieveSampleTestScheduleListForInstAssignByCriteria(InstAssignmentCriteria instAssignmentCriteriaIn);
	
	void updateSampleTestScheduleForInstAssignmentDetailPopup(SampleTestSchedule sampleTestScheduleIn, String actionType, List<SampleTestSchedule> sampleTestScheduleListIn);
	
	void destroy(); 
}