package hk.org.ha.model.pms.dqa.persistence.sample;

import hk.org.ha.fmk.pms.audit.customizer.AuditCustomizer;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Customizer;

@Entity
@Table(name = "EXCLUSION_TEST")
@NamedQueries({
	@NamedQuery(name = "ExclusionTest.findByRecordStatus", query = "select o from ExclusionTest o where o.sampleItem.recordStatus =:recordStatus order by o.sampleItem.itemCode, o.sampleTest.testCode, o.exclusionCode "),
	@NamedQuery(name = "ExclusionTest.findByItemCode", query = "select o from ExclusionTest o where o.sampleItem.itemCode = :itemCode and o.sampleItem.recordStatus =:recordStatus "),
	@NamedQuery(name = "ExclusionTest.findByItemCodeTestCodeRecordStatus", query = "select o from ExclusionTest o where o.sampleTest.testCode = :testCode and o.sampleItem.itemCode = :itemCode and o.sampleItem.recordStatus = :recordStatus ")
})
@Customizer(AuditCustomizer.class)
public class ExclusionTest extends VersionEntity {


	private static final long serialVersionUID = 7529917583589225680L;
	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exclusionTestSeq")
	@SequenceGenerator(name = "exclusionTestSeq", sequenceName = "SEQ_EXCLUSION_TEST", initialValue=10000)
	@Id
	@Column(name="EXCLUSION_TEST_ID", nullable = false)
    private Long exclusionTestId;

	@ManyToOne  
	@JoinColumn(name="TEST_CODE",  nullable = false)
    private SampleTest sampleTest;

	@Column(name="EXCLUSION_CODE", length = 4)
    private String exclusionCode;
	
	@Column(name="EXCLUSION_DESC", length = 150)
    private String exclusionDesc;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="SAMPLE_ITEM_ID")
	private SampleItem sampleItem;
	
	public ExclusionTest() {
    }
	
	public Long getExclusionTestId() {
		return exclusionTestId;
	}

	public void setExclusionTestId(Long exclusionTestId) {
		this.exclusionTestId = exclusionTestId;
	}

	public SampleTest getSampleTest() {
		return sampleTest;
	}

	public void setSampleTest(SampleTest sampleTest) {
		this.sampleTest = sampleTest;
	}

	

	public void setSampleItem(SampleItem sampleItem) {
		this.sampleItem = sampleItem;
	}

	public SampleItem getSampleItem() {
		return sampleItem;
	}

	public void setExclusionCode(String exclusionCode) {
		this.exclusionCode = exclusionCode;
	}

	public String getExclusionCode() {
		return exclusionCode;
	}

	public String getExclusionDesc() {
		return exclusionDesc;
	}

	public void setExclusionDesc(String exclusionDesc) {
		this.exclusionDesc = exclusionDesc;
	}
		
}
