package hk.org.ha.model.pms.dqa.biz.suppperf;

import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
import hk.org.ha.model.pms.dqa.persistence.suppperf.FaxDetailPharmProblem;
import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;

import java.util.List;

import javax.ejb.Local;


@Local
public interface FaxInitSuppServiceLocal {
	
	void createFaxInitSupp(QaProblem qaProblemIn, EventLog eventLogIn, List<FaxDetailPharmProblem> faxDetailPharmProblemListIn);
	
	void createFaxInitSuppRpt(QaProblem qaProblemIn, EventLog eventLogIn, String refNumIn, Boolean reGenFlag);
	
	void generateFaxInitSuppRpt();
	
	boolean isSuccess();
	
	String getErrorCode();
	
	String getRefNum();
	
    void destroy();
}
