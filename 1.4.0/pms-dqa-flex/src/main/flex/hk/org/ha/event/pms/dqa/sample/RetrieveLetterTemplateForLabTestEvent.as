package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact
	
	public class RetrieveLetterTemplateForLabTestEvent extends AbstractTideEvent
	{
		private var _event:Event;
		
		private var _scheduleList:ListCollectionView;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _userInfo:UserInfo;
		private var _supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact;
		
		
		public function RetrieveLetterTemplateForLabTestEvent(scheduleList:ListCollectionView, sampleTestSchedule:SampleTestSchedule, userInfo:UserInfo, supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact, event:Event=null):void {
			super();
			_event = event;
			_scheduleList = scheduleList;
			_sampleTestSchedule = sampleTestSchedule;
			_userInfo = userInfo;
			_supplierPharmCompanyManufacturerContact = supplierPharmCompanyManufacturerContact;
		}
		
		public function get event():Event {
			return _event;
		}

		public function get scheduleList():ListCollectionView {
			return _scheduleList;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
		
		public function get userInfo():UserInfo {
			return _userInfo;
		}
		
		public function get supplierPharmCompanyManufacturerContact():SupplierPharmCompanyManufacturerContact {
			return _supplierPharmCompanyManufacturerContact;
		}
		
	}
	
}