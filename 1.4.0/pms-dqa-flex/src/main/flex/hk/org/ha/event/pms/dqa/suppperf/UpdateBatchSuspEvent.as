package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.BatchSusp;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class UpdateBatchSuspEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _batchSusp:BatchSusp;
		
		
		public function UpdateBatchSuspEvent(batchSusp:BatchSusp, event:Event=null):void {
			super();
			_event = event;
			_batchSusp = batchSusp;			
		}
		
		public function get batchSusp():BatchSusp 
		{
			return _batchSusp;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}