package hk.org.ha.event.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleTestFileCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestFileListByCriteriaEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestFileCriteria:SampleTestFileCriteria;
		
		public function RetrieveSampleTestFileListByCriteriaEvent(sampleTestFileCriteria:SampleTestFileCriteria, event:Event=null):void {
			super();
			_event = event;
			_sampleTestFileCriteria = sampleTestFileCriteria;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestFileCriteria():SampleTestFileCriteria
		{
			return _sampleTestFileCriteria;
		}
	}
}