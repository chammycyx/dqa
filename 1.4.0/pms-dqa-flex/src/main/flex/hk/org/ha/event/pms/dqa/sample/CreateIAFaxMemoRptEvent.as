package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.sample.IAMemoContentVo;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateIAFaxMemoRptEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _iAMemoContentVo:IAMemoContentVo;
		
		
		public function CreateIAFaxMemoRptEvent(iAMemoContentVo:IAMemoContentVo, event:Event=null):void {
			super();
			_event = event;
			_iAMemoContentVo = iAMemoContentVo;			
		}
		
		public function get iAMemoContentVo():IAMemoContentVo 
		{
			return _iAMemoContentVo;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}