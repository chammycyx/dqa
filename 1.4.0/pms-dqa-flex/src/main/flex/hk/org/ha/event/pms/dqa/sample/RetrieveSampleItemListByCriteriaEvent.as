package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.vo.sample.SampleItemCriteria;
	
	public class RetrieveSampleItemListByCriteriaEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _sampleItemCriteria:SampleItemCriteria;
		
		public function RetrieveSampleItemListByCriteriaEvent(sampleItemCriteria:SampleItemCriteria, event:Event=null):void {
			super();
			_event = event;
			_sampleItemCriteria = sampleItemCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleItemCriteria():SampleItemCriteria
		{
			return _sampleItemCriteria;
		}
	}
}