package hk.org.ha.event.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.AuditRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAuditRptExcelEvent extends AbstractTideEvent 
	{
		private var _auditRptCriteria:AuditRptCriteria;
		private var _callBack:Function;
		
		public function RetrieveAuditRptExcelEvent(auditRptCriteria:AuditRptCriteria, callBack:Function):void 
		{
			super();
			_auditRptCriteria = auditRptCriteria;
			_callBack = callBack;
		}
		
		public function get auditRptCriteria():AuditRptCriteria {
			return _auditRptCriteria;
		}
		
		public function get callBack():Function
		{
			return _callBack;
		}
	}
}
