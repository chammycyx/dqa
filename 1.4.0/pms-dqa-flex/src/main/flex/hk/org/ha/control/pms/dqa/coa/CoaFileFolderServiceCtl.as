package hk.org.ha.control.pms.dqa.coa{
	
	import hk.org.ha.event.pms.dqa.coa.DeleteCoaFileFolderEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaFileFolderListForCoaItemMaintEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaFileFolderEvent;
	import hk.org.ha.event.pms.dqa.coa.UploadCoaFileFolderFilesEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowCoaItemMaintMessageEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowFileItemUploadMessageEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaFileFolderServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaFileFolder;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	import hk.org.ha.view.pms.dqa.coa.popup.FileItemUploadPopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("coaFileFolderServiceCtl", restrict="true")]
	public class CoaFileFolderServiceCtl {
		
		[In]
		public var coaFileFolderService:CoaFileFolderServiceBean;
		
		[In]
		public var coaItem:CoaItem;
		
		public var coaFileFolder:CoaFileFolder;
		
		[In]
		public var fileItemUploadPopup:FileItemUploadPopup; 
		
		
		[Observer]
		public function uploadCoaFileFolderFiles(evt:UploadCoaFileFolderFilesEvent):void {

			coaFileFolder = evt.coaFileFolder;
			In(Object(coaFileFolderService).success);
			In(Object(coaFileFolderService).duplicated );
			In(Object(coaFileFolderService).physicalFilePath);
			coaFileFolderService.doUpload(evt.data, evt.refUploadFileName, evt.coaFileFolder, uploadCoaFileFolderFilesResult);
		}
	
		public function uploadCoaFileFolderFilesResult(evt: TideResultEvent):void {
			if (Object(coaFileFolderService).duplicated ){
				evt.context.dispatchEvent(new ShowFileItemUploadMessageEvent("0007"));
			}else if (!Object(coaFileFolderService).success ){
				evt.context.dispatchEvent(new ShowFileItemUploadMessageEvent("0019"));
			}else if (Object(coaFileFolderService).success ){	
				afterUploadCoaFileFolderFilesResult();
			}	
		}
		
		private function afterUploadCoaFileFolderFilesResult():void {
			fileItemUploadPopup.closeWin();
			dispatchEvent(new RetrieveCoaFileFolderListForCoaItemMaintEvent(coaItem));
		}

		[Observer]
		public function updateCoaFileFolder(evt:UpdateCoaFileFolderEvent):void {
			coaFileFolder = evt.coaFileFolder;
			In(Object(coaFileFolderService).success);
			coaFileFolderService.updateCoaFileFolder(coaFileFolder,updateCoaFileFolderResult);
		}
		
		public function updateCoaFileFolderResult(evt:TideResultEvent):void {
			if (Object(coaFileFolderService).success ){	
				fileItemUploadPopup.closeWin();
				dispatchEvent(new RetrieveCoaFileFolderListForCoaItemMaintEvent(coaItem));	
			}
			
		}
		
		[Observer]
		public function deleteCoaFileFolder(evt:DeleteCoaFileFolderEvent):void {
			In(Object(coaFileFolderService).success);
			coaFileFolderService.deleteCoaFileFolder(evt.coaFileFolder, deleteCoaFileFolderResult);
		}
		
		public function deleteCoaFileFolderResult(evt: TideResultEvent):void {
			if (Object(coaFileFolderService).success ){	
				evt.context.dispatchEvent(new RetrieveCoaItemEvent(coaItem));		
			}else {
				evt.context.dispatchEvent(new ShowFileItemUploadMessageEvent("0018"));
			}
		}
		
		
	}
}