package hk.org.ha.event.pms.dqa.coa.show
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCoaEmailEnquiryViewEvent extends AbstractTideEvent 
	{	
		private var _clearMessages:Boolean;		
		
		private var _event:Event;
		
		public function ShowCoaEmailEnquiryViewEvent(clearMessages:Boolean=true, event:Event=null):void 
		{
			super();
			_clearMessages = clearMessages;
			_event = event
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
		
		public function get event():Event
		{
			return _event;
		}		
	}
}