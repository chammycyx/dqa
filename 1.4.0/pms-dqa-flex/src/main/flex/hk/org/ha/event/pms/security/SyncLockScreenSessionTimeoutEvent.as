package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SyncLockScreenSessionTimeoutEvent extends AbstractTideEvent 
	{		
		public function SyncLockScreenSessionTimeoutEvent():void 
		{
			super();
		}
	}
}