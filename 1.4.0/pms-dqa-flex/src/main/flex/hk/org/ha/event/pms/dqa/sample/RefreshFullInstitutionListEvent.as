package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshFullInstitutionListEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		public function RefreshFullInstitutionListEvent(screenName:String):void {
			super();
			_screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}