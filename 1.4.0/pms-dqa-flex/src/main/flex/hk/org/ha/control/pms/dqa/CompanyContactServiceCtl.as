package hk.org.ha.control.pms.dqa
{
	import hk.org.ha.event.pms.dqa.AddCompanyContactEvent;
	import hk.org.ha.event.pms.dqa.CreateCompanyContactEvent;
	import hk.org.ha.event.pms.dqa.UpdateCompanyContactEvent;
	import hk.org.ha.event.pms.dqa.DeleteCompanyContactEvent;
	import hk.org.ha.event.pms.dqa.RetrieveCompanyContactByCompanyCodeContactIdEvent;
	
	import hk.org.ha.event.pms.dqa.popup.ShowCompanyContactPopupEvent;
	import hk.org.ha.event.pms.dqa.RetrieveCompanyByCompanyCodeEvent;
	
	import hk.org.ha.view.pms.dqa.popup.CompanyContactPopup;
	
	import hk.org.ha.model.pms.dqa.persistence.Company;
	import hk.org.ha.model.pms.dqa.persistence.CompanyContact;
	
	import hk.org.ha.model.pms.dqa.biz.CompanyContactServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("companyContactServiceCtl", restrict="true")]
	public class CompanyContactServiceCtl
	{
		[In]
		public var companyContactService:CompanyContactServiceBean;		
				
		[In] 
		public var company:Company;
				
		[In]
		public var companyContactPopup:CompanyContactPopup;
				
		[In]
		public var companyContact:CompanyContact;
			
		[Observer]
		public function addCompanyContact(evt:AddCompanyContactEvent):void
		{
			companyContactService.addCompanyContact(addCompanyContactResult);
		}
				
		public function addCompanyContactResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new ShowCompanyContactPopupEvent("Add")); 
		}
			
		[Observer]
		public function createCompanyContact(evt:CreateCompanyContactEvent):void
		{
			In(Object(companyContactService).updateSucceed)
			companyContactService.createCompanyContact(createCompanyContactResult);
		}
		
		public function createCompanyContactResult(evt:TideResultEvent):void
		{
			if (Object(companyContactService).updateSucceed ) {
				companyContactPopup.closeWin();
				evt.context.dispatchEvent(new RetrieveCompanyByCompanyCodeEvent(company.companyCode));
			}	
		}	
				
		[Observer]
		public function retrieveCompanyContactByCompanyCodeContactId(evt:RetrieveCompanyContactByCompanyCodeContactIdEvent):void
		{
			companyContactService.retrieveCompanyContactByCompanyCodeContactId(evt.companyCode, 
			evt.contactId, retrieveCompanyContactByCompanyCodeContactIdResult);
		}
		
		public function retrieveCompanyContactByCompanyCodeContactIdResult(evt:TideResultEvent):void
		{
			if (companyContact!=null){
				evt.context.dispatchEvent(new ShowCompanyContactPopupEvent("Modify"));
			}	
		}
				
		[Observer]
		public function updateCompanyContact(evt:UpdateCompanyContactEvent):void
		{
			In(Object(companyContactService).updateSucceed)
			companyContactService.updateCompanyContact(updateCompanyContactResult);
		}	
			
		public function updateCompanyContactResult(evt:TideResultEvent):void
		{
			if (Object(companyContactService).updateSucceed ) {
				companyContactPopup.closeWin();
				evt.context.dispatchEvent(new RetrieveCompanyByCompanyCodeEvent(company.companyCode));
			}
		}	
		
		[Observer]
		public function deleteCompanyContact(evt:DeleteCompanyContactEvent):void
		{
			var companyContact:CompanyContact = evt.companyContact;	
			companyContactService.deleteCompanyContact(companyContact, deleteCompanyContactResult );	
		}
		
		public function deleteCompanyContactResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new RetrieveCompanyByCompanyCodeEvent(company.companyCode));
		}
	}
}