package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;

	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleItemExportListEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleItemKeyList:ListCollectionView;
		
		public function RetrieveSampleItemExportListEvent(sampleItemKeyList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_sampleItemKeyList = sampleItemKeyList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleItemKeyList():ListCollectionView{
			return _sampleItemKeyList;
		}
	}
}