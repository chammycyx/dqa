package hk.org.ha.control.pms.dqa.sample{

	import hk.org.ha.event.pms.dqa.sample.AddSampleTestScheduleFileForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleFileForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleFileFullDetailForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleFileByScheduleIdFileCatEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleFileForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleFileForTestResultEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleFileServiceBean;
	import hk.org.ha.view.pms.dqa.sample.popup.DocumentFileUploadPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	
	[Bindable]
	[Name("sampleTestScheduleFileServiceCtl", restrict="true")]
	public class SampleTestScheduleFileServiceCtl {

		[In]
		public var sampleTestScheduleFileService:SampleTestScheduleFileServiceBean;
		
		[In]
		public var documentFileUploadPopup:DocumentFileUploadPopup;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleTestScheduleFileForDocument(evt:RetrieveSampleTestScheduleFileForDocumentEvent):void{
			event = evt.event;
			sampleTestScheduleFileService.retrieveSampleTestScheduleFileForDocument(evt.scheduleId, retrieveSampleTestScheduleFileForDocumentResult);	
		}
		
		public function retrieveSampleTestScheduleFileForDocumentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveSampleTestScheduleFileFullDetailForDocument(evt:RetrieveSampleTestScheduleFileFullDetailForDocumentEvent):void{
			event = evt.event;
			sampleTestScheduleFileService.retrieveSampleTestScheduleFileFullDetailForDocument(evt.scheduleId, retrieveSampleTestScheduleFileFullDetailForDocumentResult);	
		}
		
		public function retrieveSampleTestScheduleFileFullDetailForDocumentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}

		[Observer]
		public function retrieveSampleTestScheduleFileByScheduleIdFileCat(evt:RetrieveSampleTestScheduleFileByScheduleIdFileCatEvent):void{
			event = evt.event;
			sampleTestScheduleFileService.retrieveSampleTestScheduleFileByScheduleIdFileCat(evt.scheduleId, evt.fileCat, retrieveSampleTestScheduleFileByScheduleIdFileCatResult); 
		}
		
		public function retrieveSampleTestScheduleFileByScheduleIdFileCatResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function addSampleTestScheduleFileForDocument(evt:AddSampleTestScheduleFileForDocumentEvent):void{
			event = evt.event;
			
			sampleTestScheduleFileService.addSampleTestScheduleFileForDocument(evt.sampleTestFile, addSampleTestScheduleFileForDocumentResult);
		}
		
		public function addSampleTestScheduleFileForDocumentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function updateSampleTestScheduleFileForDocument(evt:UpdateSampleTestScheduleFileForDocumentEvent):void{
		
			event = evt.event;
			sampleTestScheduleFileService.updateSampleTestScheduleFileForDocLink(evt.sampleTestFile, evt.orgSampleTestSchedFile, updateSampleTestScheduleFileForDocumentResult);
		}
		
		public function updateSampleTestScheduleFileForDocumentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}

		[Observer]
		public function updateSampleTestScheduleFileForTestResult(evt:UpdateSampleTestScheduleFileForTestResultEvent):void{
			
			event = evt.event;
			sampleTestScheduleFileService.updateSampleTestScheduleFileForTestResult(evt.sampleTestScheduleFile, evt.sampleTestFile, evt.actionType, updateSampleTestScheduleFileForTestResultResult);
		}
		
		public function updateSampleTestScheduleFileForTestResultResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
	}
}