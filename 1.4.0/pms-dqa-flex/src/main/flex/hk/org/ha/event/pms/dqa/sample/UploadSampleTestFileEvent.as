package hk.org.ha.event.pms.dqa.sample {
	
	import flash.utils.ByteArray;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	
	import org.granite.tide.events.AbstractTideEvent;

	
	public class  UploadSampleTestFileEvent extends AbstractTideEvent 
	{
		
		private var _data:ByteArray;
		private var _fileName:String;
		private var _sampleTestFile:SampleTestFile;
		private var _screenName:String;
		
		
		public function UploadSampleTestFileEvent(data:ByteArray, fileName:String, sampleTestFile:SampleTestFile, screenName:String ):void {
			super();
			_data = data;
			_fileName = fileName;
			_sampleTestFile = sampleTestFile;
			_screenName = screenName;
		}
		
		public function get data():ByteArray 
		{
			return _data;
		}
		
		public function get sampleTestFile():SampleTestFile
		{
			return _sampleTestFile;
		}
		
		public function get fileName():String
		{
			return _fileName;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}