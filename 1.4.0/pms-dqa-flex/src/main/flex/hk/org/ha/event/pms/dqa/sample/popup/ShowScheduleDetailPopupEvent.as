package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowScheduleDetailPopupEvent extends AbstractTideEvent 
	{	
		public function ShowScheduleDetailPopupEvent():void
		{
			super();
		}
		
	}
}