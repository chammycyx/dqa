package hk.org.ha.event.pms.dqa.coa {
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateCoaDiscrepancyEvent extends AbstractTideEvent 
	{		
		private var _coaDiscrepancyKeyList:ListCollectionView;
		private var _coaBatch:CoaBatch;
		
		public function CreateCoaDiscrepancyEvent(coaBatch:CoaBatch, coaDiscrepancyKeyList:ListCollectionView):void 
		{
			super();
			_coaBatch = coaBatch;
			_coaDiscrepancyKeyList = coaDiscrepancyKeyList;
		}
		
		public function get coaDiscrepancyKeyList():ListCollectionView {
			return _coaDiscrepancyKeyList;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
	}
}