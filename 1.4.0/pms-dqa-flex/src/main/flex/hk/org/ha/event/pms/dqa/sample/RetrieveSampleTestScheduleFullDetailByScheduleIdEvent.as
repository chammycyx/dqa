package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleFullDetailByScheduleIdEvent extends AbstractTideEvent 
	{
		private var _scheduleId:Number;
		private var _eventDocument:Event;
		
		public function RetrieveSampleTestScheduleFullDetailByScheduleIdEvent(scheduleId:Number, eventDocument:Event=null):void {
			super();
			_scheduleId = scheduleId;
			_eventDocument = eventDocument;
		}
		
		public function get scheduleId():Number 
		{
			return _scheduleId;
		}
		
		public function get eventDocument():Event {
			return _eventDocument;
		}
	}
}