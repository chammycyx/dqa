package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class ValidationConfirmDocumentDetailFollowUpEvent extends AbstractTideEvent 
	{
		
		private var _event:Event;
		private var _actionType:String;
		private var _requiredQtyNum:int;
		
		public function ValidationConfirmDocumentDetailFollowUpEvent(evt:Event, actionType:String, requiredQtyNum:int):void {
			super();
			this._event = evt;
			this._actionType = actionType;		
			this._requiredQtyNum = requiredQtyNum;
		}

		public function get event():Event
		{
			return _event;
		}
		
		public function get actionType():String
		{
			return _actionType;
		}
		
		public function get requiredQtyNum():int
		{
			return _requiredQtyNum;
		}
	}
}