package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	public class ShowFaxSummaryPopupFaxContactPopupEvent extends AbstractTideEvent 
	{	
		private var _corrType:String;
		private var _qaProblem:QaProblem;
		
		public function ShowFaxSummaryPopupFaxContactPopupEvent(qaProblem:QaProblem, corrType:String):void
		{
			super();
			_qaProblem = qaProblem;
			_corrType = corrType;
		}
		
		public function get qaProblem():QaProblem {
			return _qaProblem;
		}
		
		public function get corrType():String {
			return _corrType;
		}
		
	}
}