package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchVerListByCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchVerListByCoaBatchIdEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchVerListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaBatchVerListServiceCtl", restrict="false")]
	public class CoaBatchVerListServiceCtl
	{					
				
		[In]
		public var coaBatchVerListService:CoaBatchVerListServiceBean;
		
		[In]
		public var coaBatchVerList:ArrayCollection;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaBatchVerListByCoaBatch(evt:RetrieveCoaBatchVerListByCoaBatchEvent):void 
		{	
			event = evt.event;			
			coaBatchVerListService.retrieveCoaBatchVerListByCoaBatch( evt.coaBatch, retrieveCoaBatchVerListByCoaBatchResult );
		}	
		
		private function retrieveCoaBatchVerListByCoaBatchResult(evt:TideResultEvent):void 
		{										
			evt.context.dispatchEvent( event );
		}
		
		[Observer]
		public function retrieveCoaBatchVerListByCoaBatchId(evt:RetrieveCoaBatchVerListByCoaBatchIdEvent):void 
		{	
			event = evt.event;			
			coaBatchVerListService.retrieveCoaBatchVerListByCoaBatchId( evt.coaBatchId, retrieveCoaBatchVerListByCoaBatchIdResult );
		}	
		
		private function retrieveCoaBatchVerListByCoaBatchIdResult(evt:TideResultEvent):void 
		{										
			if( event != null ){
				evt.context.dispatchEvent( event );
			}
		}	

	}
}