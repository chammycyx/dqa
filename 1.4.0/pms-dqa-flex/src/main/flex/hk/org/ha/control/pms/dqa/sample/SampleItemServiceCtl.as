package hk.org.ha.control.pms.dqa.sample{

	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.AddSampleItemEvent;
	import hk.org.ha.event.pms.dqa.sample.ClearSampleItemListEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateSampleItemEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteSampleItemEvent;
	import hk.org.ha.event.pms.dqa.sample.RefreshSampleItemEvent;
	import hk.org.ha.event.pms.dqa.sample.RefreshSampleItemMaintenanceEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemByItemCodeRecordStatusEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveAllSampleItemByItemCodeRecordStatusEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemForSampleTestEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemForScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleItemEvent;
	import hk.org.ha.event.pms.dqa.sample.popup.ShowSampleItemPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleItemServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleItem;
	import hk.org.ha.view.pms.dqa.sample.popup.SampleItemMaintPopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleItemServiceCtl", restrict="true")]
	public class SampleItemServiceCtl {
		
		[In]
		public var sampleItemService:SampleItemServiceBean;
		
		[In]
		public var sampleItem:SampleItem;
		
		[In]
		public var sampleItemMaintPopup:SampleItemMaintPopup;
		
		private var screenName:String;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleItem(evt:RetrieveSampleItemByItemCodeRecordStatusEvent):void {
			sampleItemService.retrieveSampleItemByItemCodeRecordStatus(evt.itemCode, evt.recordStatus, retrieveSampleItemResult);
		}
		
		public function retrieveSampleItemResult(evt: TideResultEvent):void {
			if (sampleItem!=null){
				evt.context.dispatchEvent(new ShowSampleItemPopupEvent("Edit"));	
			}		
		}
		
		
		[Observer]
		public function createSampleItem(evt:CreateSampleItemEvent):void{
			In(Object(sampleItemService).duplicated);
			In(Object(sampleItemService).drugItemNotExist);
			
			sampleItemService.createSampleItem(evt.sampleItem, createSampleItemResult);
			
		}
		
		public function createSampleItemResult(evt: TideResultEvent):void {
			if (!Object(sampleItemService).duplicated && !Object(sampleItemService).drugItemNotExist){
				sampleItemMaintPopup.closeWin();
				evt.context.dispatchEvent(new ClearSampleItemListEvent());
//				evt.context.dispatchEvent(new RefreshSampleItemMaintenanceEvent());
			}		
		}
		
		[Observer]
		public function addSampleItem(evt:AddSampleItemEvent):void {
			sampleItemService.addSampleItem(addSampleItemResult);
		}
		
		public function addSampleItemResult(evt: TideResultEvent):void {
			evt.context.dispatchEvent(new ShowSampleItemPopupEvent("Add"));	
		}
		
		[Observer]
		public function updateSampleItem(evt:UpdateSampleItemEvent):void {
			sampleItemService.updateSampleItem(evt.sampleItem, updateSampleItemResult);
		}
		
		public function updateSampleItemResult(evt: TideResultEvent):void {
			sampleItemMaintPopup.closeWin();
			//evt.context.dispatchEvent(new ClearSampleItemListEvent());
			evt.context.dispatchEvent(new RefreshSampleItemMaintenanceEvent());
		}
		
		[Observer]
		public function deleteSampleItem(evt:DeleteSampleItemEvent):void {
			sampleItemService.deleteSampleItem(evt.sampleItem, deleteSampleItemResult);
		}
		
		public function deleteSampleItemResult(evt: TideResultEvent):void {
			evt.context.dispatchEvent(new ClearSampleItemListEvent()); 
		}
		
		[Observer]
		public function retrieveSampleItemForSampleTest(evt:RetrieveSampleItemForSampleTestEvent):void{			
			this.screenName = evt.screenName;
			sampleItemService.retrieveSampleItemForSampleTest(evt.itemCode,  retrieveSampleItemForSampleTestResult);
		}
		
		public function retrieveSampleItemForSampleTestResult(evt: TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshSampleItemEvent(screenName, sampleItem));
		}
		
		[Observer]
		public function retrieveSampleItemForSchedule(evt:RetrieveSampleItemForScheduleEvent):void{
			event = evt.event;
			sampleItemService.retrieveSampleItemForSchedule(evt.itemCode, retrieveSampleItemForScheduleResult);
		}
		
		public function retrieveSampleItemForScheduleResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveAllSampleItemByItemCodeRecordStatus(evt:RetrieveAllSampleItemByItemCodeRecordStatusEvent):void{			
			this.screenName = evt.screenName;
			sampleItemService.retrieveAllSampleItemByItemCodeRecordStatus(evt.itemCode, evt.recordStatus, retrieveAllSampleItemByItemCodeRecordStatusResult);
		}
		
		public function retrieveAllSampleItemByItemCodeRecordStatusResult(evt: TideResultEvent):void{
			evt.context.dispatchEvent(new RefreshSampleItemEvent(screenName, sampleItem));
		}
	}
}
