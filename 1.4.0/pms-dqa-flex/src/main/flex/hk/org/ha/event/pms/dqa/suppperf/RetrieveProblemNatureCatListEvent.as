package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
	
	public class RetrieveProblemNatureCatListEvent extends AbstractTideEvent 
	{

		private var _screenName:String;
		private var _problemNatureParam:ProblemNatureParam;
		private var _event:Event;
		
		public function RetrieveProblemNatureCatListEvent(screenName:String, problemNatureParam:ProblemNatureParam, event:Event=null):void {
			super();
			_screenName = screenName;
			_problemNatureParam = problemNatureParam;
			_event = event;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get problemNatureParam():ProblemNatureParam 
		{
			return _problemNatureParam;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}