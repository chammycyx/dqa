package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshCoaVerificationViewEvent extends AbstractTideEvent 
	{		
		public function RefreshCoaVerificationViewEvent():void 
		{
			super();
		}		
	}
}