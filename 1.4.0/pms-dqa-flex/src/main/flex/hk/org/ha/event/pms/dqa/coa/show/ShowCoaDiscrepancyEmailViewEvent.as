package hk.org.ha.event.pms.dqa.coa.show
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.coa.DiscrepancyStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowCoaDiscrepancyEmailViewEvent extends AbstractTideEvent 
	{	
		private var _discrepancyStatus:DiscrepancyStatus;
		
		public function ShowCoaDiscrepancyEmailViewEvent(discrepancyStatus:DiscrepancyStatus):void 
		{
			super();
			_discrepancyStatus = discrepancyStatus;
		}
		
		public function get discrepancyStatus():DiscrepancyStatus
		{
			return _discrepancyStatus;
		}		
	}
}