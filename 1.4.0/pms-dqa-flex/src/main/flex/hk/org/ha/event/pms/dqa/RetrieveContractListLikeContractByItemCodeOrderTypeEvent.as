package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	import hk.org.ha.model.pms.dqa.udt.OrderTypeAll;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveContractListLikeContractByItemCodeOrderTypeEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _contractSuffix:String;
		private var _itemCode:String;
		private var _screenName:String;
		private var _event:Event;
		private var _orderType:OrderTypeAll;
		
		public function RetrieveContractListLikeContractByItemCodeOrderTypeEvent(contractNum:String, contractSuffix:String, itemCode:String, screenName:String, orderType:OrderTypeAll, event:Event=null):void {
			super();
			_contractNum = contractNum;
			_contractSuffix = contractSuffix;
			_itemCode = itemCode;
			_screenName = screenName;
			_event = event;
			_orderType = orderType;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get contractSuffix():String 
		{
			return _contractSuffix;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get event():Event
		{
			return _event;
		}
		
		public function get orderType():OrderTypeAll
		{
			return _orderType;
		}
	}
}