package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ClearSampleTestFileListEvent extends AbstractTideEvent 
	{
		public function ClearSampleTestFileListEvent():void {
			super();
		}
	}
}