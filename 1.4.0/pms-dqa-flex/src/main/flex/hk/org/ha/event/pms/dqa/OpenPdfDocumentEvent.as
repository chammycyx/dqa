package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.FileItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class OpenPdfDocumentEvent extends AbstractTideEvent 
	{						
		private var _fileItem:FileItem;				
		private var _winName:String;
		private var _event:Event;
		
		public function OpenPdfDocumentEvent(fileItem:FileItem, winName:String, event:Event=null):void 
		{
			super();
			_winName = winName;
			_fileItem = fileItem;
			_event = event;
		}
		
		public function get winName():String
		{
			return _winName;
		}
		
		public function get fileItem():FileItem {
			return _fileItem;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}