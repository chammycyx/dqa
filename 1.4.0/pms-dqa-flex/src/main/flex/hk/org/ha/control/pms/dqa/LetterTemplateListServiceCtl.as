package hk.org.ha.control.pms.dqa{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.RetrieveLetterTemplateListByModuleTypeRecordStatusEvent;
	import hk.org.ha.model.pms.dqa.biz.LetterTemplateListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import mx.controls.Alert;
	
	[Bindable]
	[Name("letterTemplateListServiceCtl", restrict="true")]
	public class LetterTemplateListServiceCtl {
		
		private var event:Event;
		
		[In]
		public var letterTemplateList:ArrayCollection;	
		
		[In]
		public var letterTemplateListService:LetterTemplateListServiceBean;
		
		[Observer]
		public function retrieveLetterTemplateListByModuleTypeRecordStatus(evt:RetrieveLetterTemplateListByModuleTypeRecordStatusEvent):void 
		{
			event = evt.event;
			letterTemplateListService.retrieveLetterTemplateListByModuleTypeRecordStatus(evt.moduleType, evt.recordStatus, retrieveLetterTemplateListByModuleTypeRecordStatusResult );				
		}	
		
		private function retrieveLetterTemplateListByModuleTypeRecordStatusResult(evt:TideResultEvent):void {
			if(event != null) {		
				evt.context.dispatchEvent(event);				
			}
		}
	}
}