package hk.org.ha.event.pms.dqa.delaydelivery {
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormFileUploadData;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshDelayDeliveryRptFormDataPopupFileViewPopupEvent extends AbstractTideEvent 
	{
		private var _delayDeliveryRptFormFileUploadData:DelayDeliveryRptFormFileUploadData;
		
		
		public function RefreshDelayDeliveryRptFormDataPopupFileViewPopupEvent(delayDeliveryRptFormFileUploadData:DelayDeliveryRptFormFileUploadData):void {
			super();
			_delayDeliveryRptFormFileUploadData = delayDeliveryRptFormFileUploadData;			
		}
		
		public function get delayDeliveryRptFormFileUploadData():DelayDeliveryRptFormFileUploadData 
		{
			return _delayDeliveryRptFormFileUploadData;
		}
	}
}