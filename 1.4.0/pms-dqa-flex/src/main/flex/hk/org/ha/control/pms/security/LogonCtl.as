package hk.org.ha.control.pms.security {
	
	import flash.external.ExternalInterface;
	
	import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
	import hk.org.ha.event.pms.security.DebugLogonEvent;
	import hk.org.ha.event.pms.security.LogoffEvent;
	import hk.org.ha.event.pms.security.PostLogonEvent;
	import hk.org.ha.event.pms.security.RefreshSessionTimeoutEvent;
	import hk.org.ha.event.pms.security.SaveLogonReasonEvent;
	import hk.org.ha.event.pms.security.SyncLockScreenSessionTimeoutEvent;
	import hk.org.ha.event.pms.security.UpdateSessionTimeoutEvent;
	import hk.org.ha.event.pms.security.show.ShowLogonReasonPopupEvent;
	import hk.org.ha.fmk.pms.security.UamInfo;
	import hk.org.ha.model.pms.biz.security.AccessReasonServiceBean;
	import hk.org.ha.model.pms.biz.security.PostLogonServiceBean;
	import hk.org.ha.model.pms.biz.security.ScreenIdleServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.security.Identity;
	
	[Bindable]
	[Name("logonCtl")]
	public class LogonCtl 
	{
		[In]
		public var postLogonService:PostLogonServiceBean;
		
		[In]
		public var accessReasonService:AccessReasonServiceBean;
		
		[In]
		public var identity:Identity; 
		
		[In]
		public var uamInfo:UamInfo;
		
		[In]
		public var screenIdleService:ScreenIdleServiceBean;
				
		[Observer]
		public function debugLogon(evt:DebugLogonEvent):void 
		{
			identity.username = evt.username;
			identity.password = evt.password;
			identity.login(debugLogonResult);
		}
		
		private function debugLogonResult(evt:TideResultEvent):void 
		{
			evt.context.dispatchEvent(new PostLogonEvent());
		}
		
		[Observer]
		public function logoff(evt:LogoffEvent):void 
		{	
			ExternalInterface.call("logoff");
		}
		
		[Observer]
		public function postLogon(evt:PostLogonEvent):void {
			postLogonService.postLogon(postLogonResult);
		}
		
		private function postLogonResult(evt:TideResultEvent):void {
			if (uamInfo.userRole.split("\.")[1] == "ITD") {
				evt.context.dispatchEvent(new ShowLogonReasonPopupEvent());
			}
			else {
				evt.context.dispatchEvent(new ShowStartupViewEvent());
			}
		}
		
		[Observer]
		public function saveLogonReason(evt:SaveLogonReasonEvent):void {
			accessReasonService.saveLogonReason(evt.reason, saveLogonReasonResult);
		}
		
		private function saveLogonReasonResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ShowStartupViewEvent());
		}
		
		[Observer]
		public function syncLockScreenSessionTimeout(evt:SyncLockScreenSessionTimeoutEvent):void
		{
			screenIdleService.syncLockScreenSessionTimeout(syncLockScreenSessionTimeoutResult);
		}
		
		private function syncLockScreenSessionTimeoutResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new UpdateSessionTimeoutEvent(evt.result as int));
		}
		
		[Observer]
		public function refreshSessionTimeout(evt:RefreshSessionTimeoutEvent):void
		{
			screenIdleService.refreshSessionTimeOut(evt.userId);
		}
	}
}