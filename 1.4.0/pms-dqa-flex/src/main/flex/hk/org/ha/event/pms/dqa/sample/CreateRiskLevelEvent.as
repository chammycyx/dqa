package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateRiskLevelEvent extends AbstractTideEvent 
	{
		private var _event:Event;
	
		
		public function CreateRiskLevelEvent( event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}