package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class RefreshInEmailEnqPropEvent extends AbstractTideEvent 
	{		
		private var _event:Event;
		
		public function RefreshInEmailEnqPropEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}