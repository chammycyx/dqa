package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.CopySampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListByCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListByRecordStatusScheduleStatusEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListByTestResultCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListByEnquiryDetailCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListRptEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListByCriteriaRptEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleListForCopyEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleListForInstAssignmentEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowInstitutionAssignmentMessageEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleCopyListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListInstAssignCriteriaServiceBean;
	import hk.org.ha.view.pms.dqa.sample.popup.ScheduleCopyPopup;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleListServiceCtl", restrict="true")]
	public class SampleTestScheduleListServiceCtl {
		[In]
		public var sampleTestScheduleListService:SampleTestScheduleListServiceBean;
		
		[In]
		public var sampleTestScheduleListInstAssignCriteriaService:SampleTestScheduleListInstAssignCriteriaServiceBean;
		
		[In]
		public var sampleTestScheduleCopyListService:SampleTestScheduleCopyListServiceBean;
		
		[In]
		public var scheduleCopyPopup:ScheduleCopyPopup;
		
		private var event:Event;
		
		private var event2:Event;

		[Observer]
		public function retrieveSampleTestScheduleListByCriteria(evt:RetrieveSampleTestScheduleListByCriteriaEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteria(evt.sampleTestScheduleCriteria, retrieveSampleTestScheduleListByCriteriaResult);

		}
		
		public function retrieveSampleTestScheduleListByCriteriaResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		

		[Observer]
		public function retrieveSampleTestScheduleListForInstAssignByCriteria(evt:RetrieveSampleTestScheduleListForInstAssignByCriteriaEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleListInstAssignCriteriaService).success);
			In(Object(sampleTestScheduleListInstAssignCriteriaService).errorCode);
			sampleTestScheduleListInstAssignCriteriaService.retrieveSampleTestScheduleListForInstAssignByCriteria(evt.instAssignmentCriteria, retrieveSampleTestScheduleListForInstAssignByCriteriaResult);
			
		}
		
		public function retrieveSampleTestScheduleListForInstAssignByCriteriaResult(evt: TideResultEvent):void {
			if (!Object(sampleTestScheduleListInstAssignCriteriaService).success ){
				evt.context.dispatchEvent(new ShowInstitutionAssignmentMessageEvent(Object(sampleTestScheduleListInstAssignCriteriaService).errorCode));
			}else if ( event != null ){
				evt.context.dispatchEvent(event);		
			}
		}

		
		[Observer]
		public function retrieveSampleTestScheduleListByTestResultCriteria(evt:RetrieveSampleTestScheduleListByTestResultCriteriaEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListByTestResultCriteria(evt.sampleTestResultCriteria, retrieveSampleTestScheduleListByTestResultCriteriaResult);
		}
		
		public function retrieveSampleTestScheduleListByTestResultCriteriaResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveSampleTestScheduleListByEnquiryDetailCriteria(evt:RetrieveSampleTestScheduleListByEnquiryDetailCriteriaEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListByEnquiryDetailCriteria(evt.sampleEnquiryDetailCriteria, retrieveSampleTestScheduleListByEnquiryDetailCriteriaResult);
		}
		
		public function retrieveSampleTestScheduleListByEnquiryDetailCriteriaResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveSampleTestScheduleListByRecordStatusScheduleStatus(evt:RetrieveSampleTestScheduleListByRecordStatusScheduleStatusEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListByRecordStatusScheduleStatus(evt.recordStatus, evt.scheduleStatus, retrieveSampleTestScheduleListByRecordStatusScheduleStatusResult); 
		}
		
		public function retrieveSampleTestScheduleListByRecordStatusScheduleStatusResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveSampleTestScheduleListForCopy(evt:RetrieveSampleTestScheduleListForCopyEvent):void{
			event = evt.event;
			event2 = evt.event2;
			In(Object(sampleTestScheduleCopyListService).success);	
			sampleTestScheduleCopyListService.retrieveSampleTestScheduleListForCopy(evt.scheduleId, evt.itemCode, retrieveSampleTestScheduleListForCopyResult);	
		}
		
		public function retrieveSampleTestScheduleListForCopyResult(evt:TideResultEvent):void{
			trace("copy list :"+Object(sampleTestScheduleCopyListService).success)
			
			if (Object(sampleTestScheduleCopyListService).success){
				trace("copy list 1:"+Object(sampleTestScheduleCopyListService).success)
				if( event != null ) {
					evt.context.dispatchEvent(event);		
				}
			}else {
				trace("copy list 2:"+Object(sampleTestScheduleCopyListService).success)
				if( event2 != null ) {
					evt.context.dispatchEvent(event2);
				}
			}
		}
		
		[Observer]
		public function copySampleTestSchedule(evt:CopySampleTestScheduleEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleListService).success);	
			In(Object(sampleTestScheduleListService).errorCode);
			sampleTestScheduleListService.copySampleTestSchedule(evt.sampleTestScheduleList, copySampleTestScheduleResult);
		}
		
		public function copySampleTestScheduleResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleListService).success){
				scheduleCopyPopup.closeWin();
				
				if( event != null ) {
					evt.context.dispatchEvent(event);		
				}
			}else if (Object(sampleTestScheduleListService).errorCode !=null){
				scheduleCopyPopup.showMessage(Object(sampleTestScheduleListService).errorCode);
			}
		}
		
		
		[Observer]
		public function updateSampleTestScheduleListForInstAssignment(evt:UpdateSampleTestScheduleListForInstAssignmentEvent):void{
			event = evt.event;
			sampleTestScheduleListService.updateSampleTestScheduleListForInstAssignment(evt.sampleTestScheduleList, updateSampleTestScheduleListForInstAssignmentResult); 
		}
		
		public function updateSampleTestScheduleListForInstAssignmentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveSampleTestScheduleListReport(evt:RetrieveSampleTestScheduleListRptEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListReport(evt.sampleItemKeyList, retrieveSampleTestScheduleListReportResult);
		}
				
		public function retrieveSampleTestScheduleListReportResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
				
		[Observer]
		public function retrieveSampleTestScheduleListByCriteriaReport(evt:RetrieveSampleTestScheduleListByCriteriaRptEvent):void{
			event = evt.event;
			sampleTestScheduleListService.retrieveSampleTestScheduleListByCriteriaReport(evt.sampleEnquiryDetailCriteria, retrieveSampleTestScheduleListByCriteriaReportResult);
		}
				
		public function retrieveSampleTestScheduleListByCriteriaReportResult(evt: TideResultEvent):void {
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
	}
}
