package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _problemNatureSubCat:ProblemNatureSubCat;
		
		
		public function RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent(problemNatureSubCat:ProblemNatureSubCat, event:Event=null):void {
			super();
			_event = event;
			_problemNatureSubCat = problemNatureSubCat;			
		}
		
		public function get problemNatureSubCat():ProblemNatureSubCat 
		{
			return _problemNatureSubCat;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}