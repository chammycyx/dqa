package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class UpdateSampleTestScheduleForSchedConfirmEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _sampleTestSchedule:SampleTestSchedule;
		
		private var _sampleTestFileList:ListCollectionView;

		public function UpdateSampleTestScheduleForSchedConfirmEvent(sampleTestSchedule:SampleTestSchedule, sampleTestFileList:ListCollectionView, event:Event=null):void {
			super();
			_event = event;
			_sampleTestSchedule = sampleTestSchedule;
			_sampleTestFileList = sampleTestFileList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule{
		
			return _sampleTestSchedule;
		}
		
		public function get sampleTestFileList():ListCollectionView{
			return _sampleTestFileList;
		}
	}
}