package hk.org.ha.control.pms.dqa.sample
{		
	import hk.org.ha.event.pms.dqa.sample.PrintDdrListRptEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.DdrRptServiceBean;
	import hk.org.ha.event.pms.dqa.sample.show.ShowDdrListRptEvent;
	
	import flash.events.Event;
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("ddrListRptServiceCtl", restrict="false")]
	public class DdrListRptServiceCtl
	{
		
		[In]
		public var ddrRptService:DdrRptServiceBean;
		
		[Observer]
		public function printDdrListRpt(evt:PrintDdrListRptEvent):void
		{
			ddrRptService.retrieveDdrList(evt.ddrRptCriteria, printDdrListRptResult);
			
		}
		
		public function printDdrListRptResult(evt:TideResultEvent):void{
			dispatchEvent(new ShowDdrListRptEvent());
		}
		
	}	
}
