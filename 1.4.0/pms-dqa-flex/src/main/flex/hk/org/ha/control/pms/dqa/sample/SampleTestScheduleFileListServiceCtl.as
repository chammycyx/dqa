package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleFileListServiceBean;
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleFileListServiceCtl", restrict="true")]
	public class SampleTestScheduleFileListServiceCtl {
		[In]
		public var sampleTestScheduleFileListService:SampleTestScheduleFileListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat(evt:RetrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatEvent):void{
			event = evt.event;
			sampleTestScheduleFileListService.retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCat(evt.recordStatus, evt.scheduleStatus, evt.fileCat, retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatResult); 
		}
		
		public function retrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
	}
}
