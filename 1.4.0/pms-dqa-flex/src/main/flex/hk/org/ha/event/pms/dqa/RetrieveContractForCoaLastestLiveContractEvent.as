package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveContractForCoaLastestLiveContractEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _itemCode:String;
		
		public function RetrieveContractForCoaLastestLiveContractEvent(contractNum:String, itemCode:String):void {
			super();
			_contractNum = contractNum;
			_itemCode = itemCode;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		
	}
}