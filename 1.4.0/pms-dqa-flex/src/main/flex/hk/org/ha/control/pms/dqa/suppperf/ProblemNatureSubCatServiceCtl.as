package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.AddProblemNatureSubCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateProblemNatureSubCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.DeleteProblemNatureSubCatEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent;
	import hk.org.ha.event.pms.dqa.suppperf.UpdateProblemNatureSubCatEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureSubCatServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureSubCat;
	import hk.org.ha.view.pms.dqa.suppperf.popup.NatureOfProblemSubCategoryPopup;
	import hk.org.ha.view.pms.dqa.suppperf.NatureOfProblemMaintView;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureSubCatServiceCtl", restrict="true")]
	public class ProblemNatureSubCatServiceCtl {
		
		[In]       
		public var problemNatureSubCatService:ProblemNatureSubCatServiceBean;
		
		[In]
		public var problemNatureSubCat:ProblemNatureSubCat;
		
		[In]
		public var natureOfProblemSubCategoryPopup:NatureOfProblemSubCategoryPopup;
		
		[In]
		public var natureOfProblemMaintView:NatureOfProblemMaintView;
		
		private var event:Event;
		
		
		
		[Observer]
		public function addProblemNatureSubCat(evt:AddProblemNatureSubCatEvent):void{
			event = evt.event;
			problemNatureSubCatService.addProblemNatureSubCat(addProblemNatureSubCatResult);
		}
		
		public function addProblemNatureSubCatResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function createProblemNatureSubCat(evt:CreateProblemNatureSubCatEvent):void{
			event = evt.event;
			In(Object(problemNatureSubCatService).success);
			In(Object(problemNatureSubCatService).errorCode);
			problemNatureSubCatService.createProblemNatureSubCat(evt.problemNatureSubCat, createProblemNatureSubCatResult);
		}
		
		public function createProblemNatureSubCatResult(evt:TideResultEvent):void{
			if (Object(problemNatureSubCatService).success ){
				natureOfProblemSubCategoryPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureSubCatService).errorCode !=null){
					natureOfProblemSubCategoryPopup.showMessage(Object(problemNatureSubCatService).errorCode );
				}
			}
		}
		
		[Observer]
		public function updateProblemNatureSubCat(evt:UpdateProblemNatureSubCatEvent):void{
			event = evt.event;
			In(Object(problemNatureSubCatService).success);
			In(Object(problemNatureSubCatService).errorCode);
			problemNatureSubCatService.updateProblemNatureSubCat(evt.problemNatureSubCat, updateProblemNatureSubCatResult);
		}
		
		public function updateProblemNatureSubCatResult(evt:TideResultEvent):void{
			if (Object(problemNatureSubCatService).success ){
				natureOfProblemSubCategoryPopup.closeWin();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
			}else{
				if (Object(problemNatureSubCatService).errorCode !=null){
					natureOfProblemSubCategoryPopup.showMessage(Object(problemNatureSubCatService).errorCode );
				}
			}
		}
		
		[Observer]
		public function deleteProblemNatureSubCat(evt:DeleteProblemNatureSubCatEvent):void{
			event = evt.event;
			problemNatureSubCatService.deleteProblemNatureSubCat(evt.problemNatureSubCat, deleteProblemNatureSubCatResult);
		}
		
		public function deleteProblemNatureSubCatResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveProblemNatureSubCatByProblemNatureSubCatId(evt:RetrieveProblemNatureSubCatByProblemNatureSubCatIdEvent):void{
			event = evt.event;
			problemNatureSubCatService.retrieveProblemNatureSubCatByProblemNatureSubCatId(evt.problemNatureSubCat, retrieveProblemNatureSubCatByProblemNatureSubCatIdResult);
		}
		
		public function retrieveProblemNatureSubCatByProblemNatureSubCatIdResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}
