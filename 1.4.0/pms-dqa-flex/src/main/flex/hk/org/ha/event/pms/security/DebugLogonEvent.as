package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class DebugLogonEvent extends AbstractTideEvent 
	{
		private var _username:String;        
		private var _password:String;
		
		public function DebugLogonEvent(username:String, password:String):void 
		{
			super();
			_username = username;
			_password = password;
		}        
		
		public function get username():String 
		{
			return _username;
		}
		
		public function get password():String 
		{
			return _password;
		}
	}
}