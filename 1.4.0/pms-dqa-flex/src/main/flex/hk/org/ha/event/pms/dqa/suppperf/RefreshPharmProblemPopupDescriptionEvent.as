package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RefreshPharmProblemPopupDescriptionEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _description:String;
		
		
		public function RefreshPharmProblemPopupDescriptionEvent(description:String, event:Event=null):void {
			super();
			_event = event;
			_description = description;			
		}
		
		public function get description():String 
		{
			return _description;
		}
	}
}