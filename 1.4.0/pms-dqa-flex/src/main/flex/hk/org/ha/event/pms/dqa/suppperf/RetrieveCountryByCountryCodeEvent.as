package hk.org.ha.event.pms.dqa.suppperf
{
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCountryByCountryCodeEvent extends AbstractTideEvent 
	{	
		
		private var _countryCode:String;
		private var _screenName:String;
		private var _event:Event;
		
		public function RetrieveCountryByCountryCodeEvent(countryCode:String, screenName:String,  event:Event=null):void
		{
			super();
			_countryCode = countryCode;
			_screenName = screenName;
			_event = event;
		}
		
		public function get countryCode():String 
		{
			return _countryCode;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}
