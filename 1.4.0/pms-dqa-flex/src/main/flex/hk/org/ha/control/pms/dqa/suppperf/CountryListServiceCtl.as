package hk.org.ha.control.pms.dqa.suppperf{
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveCountryListLikeCountryCodeEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.CountryListServiceBean;

	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("countryListServiceCtl", restrict="true")]
	public class CountryListServiceCtl {
		
		[In]
		public var countryListService:CountryListServiceBean;
		
		[In]
		public var countryList:ArrayCollection = new ArrayCollection();
		
		private var event:Event;
		
		
		[Observer]
		public function retrieveCountryListLikeCountryCode(evt:RetrieveCountryListLikeCountryCodeEvent):void{
			event = evt.event;
			countryListService.retrieveCountryListLikeCountryCode(evt.countryCode, retrieveCountryListLikeCountryCodeResult);
		}
		
		public function retrieveCountryListLikeCountryCodeResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
	}
}