package hk.org.ha.control.pms.dqa.coa
{				
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyKeyListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaDiscrepancyKeyListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaDiscrepancyKey;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaDiscrepancyKeyListServiceCtl", restrict="false")]
	public class CoaDiscrepancyKeyListServiceCtl
	{
		private var coaDiscrepancyKey:CoaDiscrepancyKey;
		
		[In]
		public var coaDiscrepancyKeyListService:CoaDiscrepancyKeyListServiceBean;
		
		[Observer]
		public function retrieveCoaDiscrepancyKeyList(evt:RetrieveCoaDiscrepancyKeyListEvent):void 
		{
			coaDiscrepancyKeyListService.retrieveCoaDiscrepancyKeyList();
		}	
	}
}