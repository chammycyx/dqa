package hk.org.ha.event.pms.dqa
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SendEmailByLetterTemplateEvent extends AbstractTideEvent 
	{
		
		private var _letterTemplate:LetterTemplate;
		
		private var _event:Event;
		
		public function SendEmailByLetterTemplateEvent(letterTemplate:LetterTemplate, event:Event=null):void {
			super();
			_letterTemplate = letterTemplate;
			_event = event;
		}
		
		public function get letterTemplate():LetterTemplate
		{
			return _letterTemplate;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}