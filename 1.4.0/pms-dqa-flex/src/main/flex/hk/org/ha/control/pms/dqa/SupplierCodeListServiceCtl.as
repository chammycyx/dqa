package hk.org.ha.control.pms.dqa{
	
	import hk.org.ha.event.pms.dqa.RetrieveSupplierCodeListEvent;
	import hk.org.ha.model.pms.dqa.biz.SupplierCodeListServiceBean;
	
	import flash.events.Event;
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("supplierCodeListServiceCtl", restrict="true")]
	public class SupplierCodeListServiceCtl {

		[In]
		public var supplierCodeListService:SupplierCodeListServiceBean;
		
		[In]
		public var supplierCodeList:ArrayCollection = new ArrayCollection();
		
		private var event:Event;

		[Observer]
		public function retrieveSupplierCodeList(evt:RetrieveSupplierCodeListEvent):void
		{
			event = evt.event;		
			supplierCodeListService.retrieveSupplierCodeList(retrieveSupplierCodeListResult);
		}
		
		public function retrieveSupplierCodeListResult(evt:TideResultEvent):void
		{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}
		}
	}
}