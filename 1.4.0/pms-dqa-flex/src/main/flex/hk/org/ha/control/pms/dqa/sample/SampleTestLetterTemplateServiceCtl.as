package hk.org.ha.control.pms.dqa.sample
{					
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.CreateIAFaxMemoRptEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateSupplierFaxMemoRptEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveLetterTemplateForInstitutionAssignmentEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveLetterTemplateForLabTestEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveLetterTemplateForScheduleEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestLetterTemplateServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("sampleTestLetterTemplateServiceCtl", restrict="false")]
	public class SampleTestLetterTemplateServiceCtl
	{		 
		[In]
		public var sampleTestLetterTemplateService:SampleTestLetterTemplateServiceBean;
		
		[In]
		public var letterTemplate:LetterTemplate;
		
		private var event:Event;
		
	
		[Observer]
		public function retrieveLetterTemplateForLabTest(evt:RetrieveLetterTemplateForLabTestEvent):void
		{
			event = evt.event;
			
			sampleTestLetterTemplateService.retrieveLetterTemplateForLabTest(evt.scheduleList, evt.sampleTestSchedule, evt.userInfo, evt.supplierPharmCompanyManufacturerContact, retrieveLetterTemplateForLabTestResult);
		}
		
		private function retrieveLetterTemplateForLabTestResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
				event = null;
			}
		}
		
		[Observer]
		public function retrieveLetterTemplateForSchedule(evt:RetrieveLetterTemplateForScheduleEvent):void
		{
			event = evt.event;
			
			sampleTestLetterTemplateService.retrieveLetterTemplateForSchedule(evt.scheduleList, evt.sampleTestSchedule, evt.userInfo, evt.supplierPharmCompanyManufacturerContact, retrieveLetterTemplateForScheduleResult);
		}
		
		private function retrieveLetterTemplateForScheduleResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
				event = null;
			}
		}
		
		[Observer]
		public function retrieveLetterTemplateForInstitutionAssignment(evt:RetrieveLetterTemplateForInstitutionAssignmentEvent):void
		{
			event = evt.callBackEvent;
			
			sampleTestLetterTemplateService.retrieveLetterTemplateForInstitutionAssignment(evt.sampleTestSchedule, evt.supplierPharmCompanyManufacturerContact, evt.defaultCpoContactUserInfo, evt.cpoSignatureUserInfo, retrieveLetterTemplateForInstitutionAssignmentResult);
		}
		
		private function retrieveLetterTemplateForInstitutionAssignmentResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
				event = null;
			}
		}
		
		[Observer]
		public function createIAFaxMemoRpt(evt:CreateIAFaxMemoRptEvent):void
		{
			event = evt.event;
			
			sampleTestLetterTemplateService.createIAFaxMemoRpt(evt.iAMemoContentVo, createIAFaxMemoRptResult);
		}
		
		private function createIAFaxMemoRptResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
				event = null;
			}
		}
		
		[Observer]
		public function createSupplierFaxMemoRpt(evt:CreateSupplierFaxMemoRptEvent):void
		{
			event = evt.event;
			sampleTestLetterTemplateService.createSupplierFaxMemoRpt(evt.supplierMemoContentVo, createSupplierFaxMemoRptResult);
		}
		
		private function createSupplierFaxMemoRptResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( event );
			event = null;
		}	
	}
}