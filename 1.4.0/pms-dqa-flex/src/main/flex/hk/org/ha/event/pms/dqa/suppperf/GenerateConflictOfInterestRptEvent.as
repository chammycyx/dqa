package hk.org.ha.event.pms.dqa.suppperf {
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.ConflictOfInterestRpt;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenerateConflictOfInterestRptEvent extends AbstractTideEvent 
	{
		private var _conflictOfInterestRpt:ConflictOfInterestRpt;
		
		public function GenerateConflictOfInterestRptEvent(conflictOfInterestRptIn:ConflictOfInterestRpt):void 
		{
			super();
			_conflictOfInterestRpt = conflictOfInterestRptIn;
		}
		
		public function get conflictOfInterestRpt():ConflictOfInterestRpt {
			return _conflictOfInterestRpt;
		}
		
	}
}