package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.udt.OrderType;
	
	public class GenerateSampleTestScheduleEvent extends AbstractTideEvent 
	{
		private var _testCode:String;
		private var _orderType:OrderType;
		
		private var _event:Event;
		
		public function GenerateSampleTestScheduleEvent(testCode:String, orderType:OrderType, event:Event=null):void {
			super();
			_testCode = testCode;
			_orderType = orderType;
			_event = event;
		}
		
		public function get testCode():String 
		{
			return _testCode;
		}
		
		public function get orderType():OrderType 
		{
			return _orderType;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}