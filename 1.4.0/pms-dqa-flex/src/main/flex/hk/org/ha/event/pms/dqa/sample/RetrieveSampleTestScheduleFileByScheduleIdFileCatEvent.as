package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleFileByScheduleIdFileCatEvent extends AbstractTideEvent 
	{
		private var _scheduleId:Number;
		private var _fileCat:SampleTestFileCat;
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleFileByScheduleIdFileCatEvent(scheduleId:Number, fileCat:SampleTestFileCat, event:Event=null):void {
			super();
			_scheduleId = scheduleId;
			_fileCat = fileCat;
			_event = event;
		}
		
		public function get scheduleId():Number 
		{
			return _scheduleId;
		}
		
		public function get fileCat():SampleTestFileCat 
		{
			return _fileCat;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}