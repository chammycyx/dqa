package hk.org.ha.event.pms.dqa {
	
	import hk.org.ha.model.pms.dqa.udt.ModuleType;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveContractListForCoaLiveContractListEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _itemCode:String;
		private var _screenName:String;	
		
		public function RetrieveContractListForCoaLiveContractListEvent(contractNum:String, itemCode:String, screenName:String):void {
			super();
			_contractNum = contractNum;
			_itemCode = itemCode;
			_screenName = screenName;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
	}
}