package hk.org.ha.event.pms.dqa.coa.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowFailCoaPopupEvent extends AbstractTideEvent 
	{		
		public function ShowFailCoaPopupEvent():void 
		{
			super();
		}
	}
}