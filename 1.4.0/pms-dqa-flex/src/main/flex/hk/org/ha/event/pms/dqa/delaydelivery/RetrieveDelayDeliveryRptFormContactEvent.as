package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import hk.org.ha.model.pms.dqa.vo.delaydelivery.DelayDeliveryRptFormContactData
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelayDeliveryRptFormContactEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData;
		
		public function RetrieveDelayDeliveryRptFormContactEvent(delayDeliveryRptFormContactData:DelayDeliveryRptFormContactData, callBackFunction:Function):void {
			super();
			_callBackFunction= callBackFunction;
			_delayDeliveryRptFormContactData = delayDeliveryRptFormContactData;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get delayDeliveryRptFormContactData():DelayDeliveryRptFormContactData
		{
			return _delayDeliveryRptFormContactData;
		}
	}
}