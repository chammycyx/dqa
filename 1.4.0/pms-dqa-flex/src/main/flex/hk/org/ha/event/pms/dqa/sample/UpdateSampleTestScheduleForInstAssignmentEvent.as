package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateSampleTestScheduleForInstAssignmentEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _actionType:String;

		public function UpdateSampleTestScheduleForInstAssignmentEvent(sampleTestSchedule:SampleTestSchedule, actionType:String, event:Event=null):void {
			super();
			_event = event;
			_sampleTestSchedule = sampleTestSchedule;
			_actionType = actionType;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule{
			return _sampleTestSchedule;
		}
		
		public function get actionType():String{
			return _actionType;
		}
	}
}