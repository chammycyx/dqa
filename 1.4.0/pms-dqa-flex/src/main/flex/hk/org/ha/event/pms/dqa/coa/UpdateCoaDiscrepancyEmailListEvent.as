package hk.org.ha.event.pms.dqa.coa {
	
	import flash.utils.*;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class UpdateCoaDiscrepancyEmailListEvent extends AbstractTideEvent 
	{
		
		private var _tosText:String;
		
		private var _ccsText:String;
		
		public function UpdateCoaDiscrepancyEmailListEvent(tosText:String, ccsText:String):void {
			super();
			_tosText = tosText;
			_ccsText = ccsText;
		}

		public function get tosText():String
		{
			return _tosText;
		}

		public function get ccsText():String
		{
			return _ccsText;
		}
	}
}