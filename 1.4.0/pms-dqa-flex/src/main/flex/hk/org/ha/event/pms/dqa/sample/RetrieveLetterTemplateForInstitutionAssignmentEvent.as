package hk.org.ha.event.pms.dqa.sample {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.UserInfo;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	import hk.org.ha.model.pms.dqa.vo.sample.SupplierPharmCompanyManufacturerContact;
	
	import mx.collections.ListCollectionView;
	
	import org.granite.tide.events.AbstractTideEvent;
		
		public class RetrieveLetterTemplateForInstitutionAssignmentEvent extends AbstractTideEvent
		{
			private var _callBackEvent:Event;
			
			private var _sampleTestSchedule:SampleTestSchedule;
			private var _supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact;
			private var _defaultCpoContactUserInfo:UserInfo;
			private var _cpoSignatureUserInfo:UserInfo;
			
			
			public function RetrieveLetterTemplateForInstitutionAssignmentEvent(sampleTestSchedule:SampleTestSchedule, supplierPharmCompanyManufacturerContact:SupplierPharmCompanyManufacturerContact, defaultCpoContactUserInfo:UserInfo, cpoSignatureUserInfo:UserInfo, event:Event=null):void {
				super();
				_callBackEvent = event;
				_sampleTestSchedule = sampleTestSchedule;
				_supplierPharmCompanyManufacturerContact = supplierPharmCompanyManufacturerContact;
				_defaultCpoContactUserInfo = defaultCpoContactUserInfo;
				_cpoSignatureUserInfo = cpoSignatureUserInfo;
			}
			
			public function get callBackEvent():Event {
				return _callBackEvent;
			}
			
			public function get supplierPharmCompanyManufacturerContact():SupplierPharmCompanyManufacturerContact {
				return _supplierPharmCompanyManufacturerContact;
			}
			
			public function get sampleTestSchedule():SampleTestSchedule {
				return _sampleTestSchedule;
			}
			
			public function get defaultCpoContactUserInfo():UserInfo {
				return _defaultCpoContactUserInfo;
			}
			
			public function get cpoSignatureUserInfo():UserInfo {
				return _cpoSignatureUserInfo;
			}
			
		}
		
}