package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemExportListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemExportFullListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemListByCriteriaEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleItemListForSampleTestEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleItemFrequencyRptServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleItemFullRptServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleItemListServiceBean;

	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleItemListServiceCtl", restrict="true")]
	public class SampleItemListServiceCtl {
		
		[In]
		public var sampleItemListService:SampleItemListServiceBean;
		
		[In]
		public var sampleItemFrequencyRptService:SampleItemFrequencyRptServiceBean;
		
		[In]
		public var sampleItemFullRptService:SampleItemFullRptServiceBean;
		
		private var event:Event;

		[Observer]
		public function retrieveSampleItemMaintenanceList(evt:RetrieveSampleItemListByCriteriaEvent):void{
			
			event = evt.event;
			sampleItemListService.retrieveSampleItemListByCriteria(evt.sampleItemCriteria, retrieveSampleItemMaintenanceListResult);
		}
		
		
		public function retrieveSampleItemMaintenanceListResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
	
		
		[Observer]
		public function retrieveSampleItemExportList(evt:RetrieveSampleItemExportListEvent):void{
			event = evt.event;
			sampleItemFrequencyRptService.retrieveSampleItemFrequencyList(evt.sampleItemKeyList,retrieveSampleItemExportListResult);
		}
		
		public function retrieveSampleItemExportListResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveSampleItemExportFullList(evt:RetrieveSampleItemExportFullListEvent):void{
			event = evt.event;
			sampleItemFullRptService.retrieveSampleItemFullList(retrieveSampleItemExportFullListResult);
		}
			
		public function retrieveSampleItemExportFullListResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveAllSampleItemListLikeItemCodeRecordStatus(evt:RetrieveAllSampleItemListLikeItemCodeRecordStatusEvent):void{
			event = evt.event;
			sampleItemListService.retrieveAllSampleItemListLikeItemCodeRecordStatus(evt.recordStatus, evt.itemCode, retrieveAllSampleItemListLikeItemCodeRecordStatusResult);
		}
		
		public function retrieveAllSampleItemListLikeItemCodeRecordStatusResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveSampleItemListForSampleTest(evt:RetrieveSampleItemListForSampleTestEvent):void{
			event = evt.event;
			sampleItemListService.retrieveSampleItemListForSampleTest(evt.recordStatus, evt.itemCode, retrieveSampleItemListForSampleTestResult);
		}
		
		public function retrieveSampleItemListForSampleTestResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
	}
}
