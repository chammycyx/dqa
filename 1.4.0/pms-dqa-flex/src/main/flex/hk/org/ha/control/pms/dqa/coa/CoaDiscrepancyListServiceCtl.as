package hk.org.ha.control.pms.dqa.coa
{				
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.CreateCoaDiscrepancyListEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListByCoaStatusEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyListByCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyListByCoaBatchIdEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaDiscrepancyListForReportDiscrepancyEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaDiscrepancyListEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowReportDiscrepancyPopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaDiscrepancyListServiceBean;
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaDiscrepancyListServiceCtl", restrict="false")]
	public class CoaDiscrepancyListServiceCtl
	{	
		[In]
		public var coaDiscrepancyListService:CoaDiscrepancyListServiceBean;				
		
		private var event:Event;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveCoaDiscrepancyListForReportDiscrepancy(evt:RetrieveCoaDiscrepancyListForReportDiscrepancyEvent):void 
		{						
			coaDiscrepancyListService.retrieveCoaDiscrepancyListForReportDiscrepancy(evt.coaDiscrepancyCriteria, retrieveCoaDiscrepancyListForReportDiscrepancyResult);
		}
		
		private function retrieveCoaDiscrepancyListForReportDiscrepancyResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent( new ShowReportDiscrepancyPopupEvent() );
		}
		
		[Observer]
		public function retrieveCoaDiscrepancyListByCoaBatch(evt:RetrieveCoaDiscrepancyListByCoaBatchEvent):void
		{
			event = evt.event;
			coaDiscrepancyListService.retrieveCoaDiscrepancyListByCoaBatch( evt.coaBatch, retrieveCoaDiscrepancyListByCoaBatchResult );
		}
		
		private function retrieveCoaDiscrepancyListByCoaBatchResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retreiveCoaDiscrepancyListByCoaBatchId(evt:RetrieveCoaDiscrepancyListByCoaBatchIdEvent):void
		{
			event = evt.event;
			coaDiscrepancyListService.retrieveCoaDiscrepancyListByCoaBatchId( evt.coaBatchId, retreiveCoaDiscrepancyListByCoaBatchIdResult );
		}
		
		private function retreiveCoaDiscrepancyListByCoaBatchIdResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function createCoaDiscrepancyList(evt:CreateCoaDiscrepancyListEvent):void 
		{						
			event = evt.event;
			callBackFunc = evt.callBackFunc;
			coaDiscrepancyListService.createCoaDiscrepancyList(evt.coaBatch, evt.coaDiscrepancyKeyList, createCoaDiscrepancyListResult);			
		}
		
		private function createCoaDiscrepancyListResult(evt:TideResultEvent):void
		{
			if (event != null) {
				evt.context.dispatchEvent(event);
			}
			if (callBackFunc != null) {
				callBackFunc();
			}
		}
		
		[Observer]
		public function updateCoaDiscrepancyList(evt:UpdateCoaDiscrepancyListEvent):void
		{
			coaDiscrepancyListService.updateCoaDiscrepancyList( evt.coaDiscrepancyList, function(resultEvent:TideResultEvent):void {
				if (evt.event != null) {
					resultEvent.context.dispatchEvent( evt.event );
				}
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();				
				msgProp.messageCode = evt.messageCode;
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;	
				resultEvent.context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			});
		}
	}
}