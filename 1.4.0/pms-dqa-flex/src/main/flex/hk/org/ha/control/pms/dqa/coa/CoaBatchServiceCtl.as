package hk.org.ha.control.pms.dqa.coa
{		
	
	import hk.org.ha.event.pms.dqa.coa.CreateCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.CreateEmailLogByCoaBatchLetterTemplateEvent;
	import hk.org.ha.event.pms.dqa.coa.DeleteCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.RefreshCoaVerificationViewEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchByCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchByCoaBatchIdEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaBatchListByCoaStatusEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaItemEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaBatchEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaBatchForFailCoaEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaBatchForRevertEvent;
	import hk.org.ha.event.pms.dqa.coa.UpdateCoaBatchStatusEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowCoaItemMaintMessageEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowVerifyCoaPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaBatchServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	import hk.org.ha.model.pms.dqa.udt.EmailType;
	import hk.org.ha.model.pms.dqa.udt.coa.CoaStatus;
	import hk.org.ha.view.pms.dqa.coa.CoaItemMaintenanceView;
	import hk.org.ha.view.pms.dqa.coa.popup.AddCoaBatchPopup;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.Context;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("coaBatchServiceCtl", restrict="false")]
	public class CoaBatchServiceCtl
	{
		
		[In]
		public var coaBatchService:CoaBatchServiceBean;
		
		[In]
		public var addCoaBatchPopup:AddCoaBatchPopup;
		
		
		[In]
		public var coaBatch:CoaBatch;
		
		[In]
		public var coaItem:CoaItem;
		
		[In]
		public var ctx:Context;
		
		private var event:Event;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveCoaBatchByCoaBatch(evt:RetrieveCoaBatchByCoaBatchEvent):void 
		{		
			ctx.coaBatch = null;
			coaBatchService.retrieveCoaBatchByCoaBatch(
				evt.coaBatch, 
				function(tideResultEvent:TideResultEvent):void {
					tideResultEvent.context.dispatchEvent(evt.event);
				}
			);			
		}
		
		[Observer]
		public function retrieveCoaBatchByCoaBatchId(evt:RetrieveCoaBatchByCoaBatchIdEvent):void 
		{	
			ctx.coaBatch = null;
			event = evt.event;
			coaBatchService.retrieveCoaBatchByCoaBatchId(evt.coaBatchId, function(tideResultEvent:TideResultEvent):void {				
				if (evt.event != null) {
					tideResultEvent.context.dispatchEvent(evt.event);
				}
				if (evt.callBack != null) {
					evt.callBack();
				}
			});			
		}

		[Observer]
		public function updateCoaBatch(evt:UpdateCoaBatchEvent):void
		{
			event = evt.event;
			coaBatchService.updateCoaBatch(updateCoaBatchResult);
		}
		
		private function updateCoaBatchResult(evt:TideResultEvent):void 
		{
			if( event != null ) {
				evt.context.dispatchEvent(event);
			}
		}
		
		[Observer]
		public function updateCoaBatchStatus(evt:UpdateCoaBatchStatusEvent):void 
		{
			coaBatchService.updateCoaBatchStatus(evt.coaBatch, evt.coaStatus, evt.discrepancyStatus, 
				function(tideResultEvent:TideResultEvent):void {
					if( evt.event != null ) {
						tideResultEvent.context.dispatchEvent(evt.event);		
					}
					
					if(evt.callBackFunc != null) {
						evt.callBackFunc();
					}
				}
			);		
		}
		
		private function updateCoaBatchStatusResult(evt:TideResultEvent):void 
		{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
			
			if(callBackFunc != null) {
				callBackFunc();
			}
		}		

		[Observer]
		public function updateCoaBatchForRevert(evt:UpdateCoaBatchForRevertEvent):void
		{
			event = evt.event;
			coaBatchService.updateCoaBatchForRevert(evt.coaBatchVer, updateCoaBatchForRevertResult);
		}
		
		private function updateCoaBatchForRevertResult(evt:TideResultEvent):void 
		{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}				
		}

		[Observer]
		public function updateCoaBatchForFailCoa(evt:UpdateCoaBatchForFailCoaEvent):void
		{
			event = evt.event;
			coaBatchService.updateCoaBatchForFailCoa(evt.coaBatch, updateCoaBatchForFailCoaResult);
		}
		
		private function updateCoaBatchForFailCoaResult(evt:TideResultEvent):void
		{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function createEmailLogForCoa(evt:CreateEmailLogByCoaBatchLetterTemplateEvent):void
		{
			event = evt.event;
			coaBatchService.createEmailLogForCoa(evt.coaBatch, evt.letterTemplate, EmailType.Outgoing, createEmailLogForCoaResult);
		}
		
		private function createEmailLogForCoaResult(evt:TideResultEvent):void
		{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function createCoaBatch(evt:CreateCoaBatchEvent):void {
			In(Object(coaBatchService).duplicated);
			coaBatchService.createCoaBatch(evt.coaBatch, createCoaBatchResult);
		}
		
		public function createCoaBatchResult(evt: TideResultEvent):void {
			if (!Object(coaBatchService).duplicated ){
				addCoaBatchPopup.closeWin();
				evt.context.dispatchEvent(new RetrieveCoaItemEvent(coaItem));		
			}
		}
		
		[Observer]
		public function deleteCoaBatch(evt:DeleteCoaBatchEvent):void {
			In(Object(coaBatchService).errorCode);
			coaBatchService.deleteCoaBatch(evt.coaBatch, deleteCoaBatchResult);
		}
		
		public function deleteCoaBatchResult(evt: TideResultEvent):void {
			if (Object(coaBatchService).errorCode==null){
				evt.context.dispatchEvent(new RetrieveCoaItemEvent(coaItem));
			}else {
				evt.context.dispatchEvent(new ShowCoaItemMaintMessageEvent(Object(coaBatchService).errorCode));
			}
		}
	}
}