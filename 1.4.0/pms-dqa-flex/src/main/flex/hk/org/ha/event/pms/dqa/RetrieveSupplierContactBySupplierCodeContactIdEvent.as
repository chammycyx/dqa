package hk.org.ha.event.pms.dqa
{
	import hk.org.ha.model.pms.dqa.persistence.SupplierContact;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveSupplierContactBySupplierCodeContactIdEvent  extends AbstractTideEvent 
	{
		private var _supplierContact:SupplierContact;
		
		private var _supplierCode:String;
		private var _contactId:Number;
		
		public function RetrieveSupplierContactBySupplierCodeContactIdEvent(supplierCode:String, contactId:Number):void
		{
			super();	
			_supplierCode = supplierCode;
			_contactId = contactId;
		}
		
		public function get supplierCode():String
		{
			return _supplierCode;
		}	
		
		public function get contactId():Number
		{
			return _contactId;
		}
	}
}