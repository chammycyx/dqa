package hk.org.ha.control.pms.dqa.suppperf{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInitSuppEvent;
	import hk.org.ha.event.pms.dqa.suppperf.CreateFaxInitSuppRptEvent;
	import hk.org.ha.event.pms.dqa.suppperf.popup.ShowFaxInitSuppRptEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.FaxInitSuppServiceBean;
	import hk.org.ha.view.pms.dqa.suppperf.popup.FaxContactPopupInitSuppPopup;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("faxInitSuppServiceCtl", restrict="true")]
	public class FaxInitSuppServiceCtl {
		
		private var event:Event;
		private var qaProblemIn:QaProblem;
		private var eventLogIn:EventLog;
		
		[In]
		public var faxInitSuppService:FaxInitSuppServiceBean;
		
		public var faxContactPopupInitSuppPopupIn:FaxContactPopupInitSuppPopup;
		
		[Observer]
		public function createFaxInitSupp(evt:CreateFaxInitSuppEvent):void 
		{		
			event = evt.event;
			qaProblemIn = evt.qaProblem;
			eventLogIn = evt.eventLog;
			faxContactPopupInitSuppPopupIn = evt.faxContactPopupInitSuppPopup;
			In(Object(faxInitSuppService).success);
			In(Object(faxInitSuppService).errorCode);
			In(Object(faxInitSuppService).refNum);
			faxInitSuppService.createFaxInitSupp(evt.qaProblem, evt.eventLog, evt.faxDetailPharmProblemList, createFaxInitSuppResult);		
		}	
		
		private function createFaxInitSuppResult(evt:TideResultEvent):void {
			if (Object(faxInitSuppService).success ){
				faxContactPopupInitSuppPopupIn.btnCancel_click();
				if ( event != null ){
					evt.context.dispatchEvent( event );
				}
				else
				{
					evt.context.dispatchEvent(new CreateFaxInitSuppRptEvent(qaProblemIn,
																			eventLogIn,
																			Object(faxInitSuppService).refNum,
																			false,
																			new ShowFaxInitSuppRptEvent()));
					
				}
			}else{
				if (Object(faxInitSuppService).errorCode !=null){
					faxContactPopupInitSuppPopupIn.showMessage(Object(faxInitSuppService).errorCode );
				}
			}
		}
		
		[Observer]
		public function createFaxInitSuppRpt(evt:CreateFaxInitSuppRptEvent):void 
		{		
			event = evt.event;
			faxInitSuppService.createFaxInitSuppRpt(evt.qaProblem, evt.eventLog, evt.refNum, evt.reGenFlag, createFaxInitSuppRptResult);		
		}	
		
		private function createFaxInitSuppRptResult(evt:TideResultEvent):void {
			
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
	}
}