package hk.org.ha.event.pms.dqa.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowSupplierContactPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		
		public function ShowSupplierContactPopupEvent(actionType:String):void
		{
			super();
			_actionType = actionType;
		}
		
		public function get action():String {
			return _actionType;
		}
	}
}