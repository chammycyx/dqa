package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class RefreshInstitutionListEvent extends org.granite.tide.events.AbstractTideEvent
	{
		private var _screenName:String;
		
		public function RefreshInstitutionListEvent(screenName:String):void{
			super();
			_screenName = screenName;
		
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}	
}

