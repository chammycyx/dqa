package hk.org.ha.control.pms.dqa.sample
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.CreateReleaseRetentionSampleMemoEvent;
	import hk.org.ha.event.pms.dqa.sample.PrintDdrListRptEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveReleaseRetentionSampleMemoEvent;
	import hk.org.ha.event.pms.dqa.sample.show.ShowDdrListRptEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.DdrRptServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.ReleaseRetentionSampleRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("releaseRetentionSampleRptServiceCtl", restrict="false")]
	public class ReleaseRetentionSampleRptServiceCtl
	{
		
		[In]
		public var releaseRetentionSampleRptService:ReleaseRetentionSampleRptServiceBean;
		
		[Observer]
		public function retrieveReleaseRetentionSampleMemo(evt:RetrieveReleaseRetentionSampleMemoEvent):void
		{
			releaseRetentionSampleRptService.retrieveReleaseRetentionSampleMemo(
				evt.defaultCpoContact, 
				evt.cpoSignature, 
				evt.sampleTestSchedule,
				function(event:TideResultEvent):void {
					evt.setContentToReleaseRetentionSamplePopup(event.result);
				}
			);
		}
		
		[Observer]
		public function createReleaseRetentionSampleMemo(evt:CreateReleaseRetentionSampleMemoEvent):void {
			releaseRetentionSampleRptService.createReleaseRetentionSampleMemo(
				evt.releaseRetentionSampleMemo,
				function(event:TideResultEvent):void {
					event.context.dispatchEvent(evt.event);
				}
			);
		}
	}	
}
