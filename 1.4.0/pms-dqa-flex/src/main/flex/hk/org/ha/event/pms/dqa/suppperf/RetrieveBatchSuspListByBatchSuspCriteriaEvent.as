package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.BatchSuspCriteria;
	
	public class RetrieveBatchSuspListByBatchSuspCriteriaEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _batchSuspCriteria:BatchSuspCriteria;
		
		public function RetrieveBatchSuspListByBatchSuspCriteriaEvent(batchSuspCriteria:BatchSuspCriteria, event:Event=null):void {
			super();
			_event = event;
			_batchSuspCriteria = batchSuspCriteria;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get batchSuspCriteria():BatchSuspCriteria
		{
			return _batchSuspCriteria;
		}
	}
}