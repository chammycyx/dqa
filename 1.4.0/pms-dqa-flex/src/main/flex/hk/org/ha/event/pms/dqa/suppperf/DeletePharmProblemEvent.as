package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
		
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class DeletePharmProblemEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _pharmProblem:PharmProblem; 
		private var _deleteReason:String; 
		
		
		
		public function DeletePharmProblemEvent(pharmProblem:PharmProblem, 
												  deleteReason:String, 
												  event:Event=null):void {
			super();
			_event = event;
			_pharmProblem = pharmProblem; 
			_deleteReason = deleteReason; 
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get deleteReason():String {
			return _deleteReason;
		}
		
		
	}
}