package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class RetrieveSampleTestFileListBySampleTestScheduleEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function RetrieveSampleTestFileListBySampleTestScheduleEvent(sampleTestSchedule:SampleTestSchedule, event:Event=null):void {
			super();
			_sampleTestSchedule = sampleTestSchedule;
			_event = event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule 
		{
			return _sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}