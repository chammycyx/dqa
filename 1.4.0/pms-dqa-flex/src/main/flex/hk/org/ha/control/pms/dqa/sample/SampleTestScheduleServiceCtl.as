package hk.org.ha.control.pms.dqa.sample{
	
	import hk.org.ha.event.pms.dqa.sample.AddSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.CreateNextScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.DeleteSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.RefreshSampleTestDetailViewEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleByScheduleIdEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveNextSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestScheduleFullDetailByScheduleIdEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForDocumentEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForInstAssignmentEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForInstAssignmentDetailPopupEvent;
	import hk.org.ha.event.pms.dqa.sample.UpdateSampleTestScheduleForSchedConfirmEvent;
	import hk.org.ha.event.pms.dqa.sample.ReverseSampleTestScheduleStatusEvent;
	import hk.org.ha.event.pms.dqa.sample.GenerateSampleTestScheduleEvent;
	import hk.org.ha.event.pms.dqa.sample.popup.RetrieveSampleTestScheduleAndShowScheduleConfirmationPopupEvent;
	import hk.org.ha.event.pms.dqa.sample.popup.ShowScheduleConfirmationPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleFullDetailServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleNextServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleValidateServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestScheduleListInstAssignCriteriaServiceBean;
	import hk.org.ha.view.pms.dqa.sample.DocumentDetailView;
	import hk.org.ha.view.pms.dqa.sample.popup.ScheduleConfirmationPopup;
	import hk.org.ha.view.pms.dqa.sample.popup.SchedulePopup;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("sampleTestScheduleServiceCtl", restrict="true")]
	public class SampleTestScheduleServiceCtl {
		[In]
		public var sampleTestScheduleService:SampleTestScheduleServiceBean;
		
		[In]
		public var sampleTestScheduleNextService:SampleTestScheduleNextServiceBean;
		
		[In]
		public var sampleTestScheduleFullDetailService:SampleTestScheduleFullDetailServiceBean;
		
		[In]
		public var schedulePopup:SchedulePopup;
		
		[In]
		public var scheduleConfirmationPopup:ScheduleConfirmationPopup;
		
		[In]
		public var documentDetailView:DocumentDetailView;
		
		[In] 
		public var sampleTestSchedule:SampleTestSchedule;
		
		
		private var event:Event;
		private var confirmEvent:Event;
		private var showNextScheduleEvent:Event;
		private var eventDocument:Event;
		private var screenName:String;
		
		private var sampleTestScheduleFileIn:SampleTestScheduleFile;
		
		[In]
		public var sampleTestScheduleValidateService:SampleTestScheduleValidateServiceBean;
		
		[In]
		public var sampleTestScheduleListInstAssignCriteriaService:SampleTestScheduleListInstAssignCriteriaServiceBean;
		
		[Observer]
		public function retrieveSampleTestScheduleByScheduleId(evt:RetrieveSampleTestScheduleByScheduleIdEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.retrieveSampleTestScheduleByScheduleId(evt.scheduleId, retrieveSampleTestScheduleByScheduleIdResult);
		}
		
		public function retrieveSampleTestScheduleByScheduleIdResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}

		[Observer]
		public function retrieveSampleTestScheduleForDocument(evt:RetrieveSampleTestScheduleForDocumentEvent):void{
			event = evt.event;
			sampleTestScheduleService.retrieveSampleTestScheduleForDocument(evt.scheduleId, retrieveSampleTestScheduleForDocumentResult);
		}
		
		public function retrieveSampleTestScheduleForDocumentResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}

		
		[Observer]
		public function addSampleTestSchedule(evt:AddSampleTestScheduleEvent):void{
			event = evt.event;
				
			sampleTestScheduleService.addSampleTestSchedule(addSampleTestScheduleResult);
		}
		
		public function addSampleTestScheduleResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function createSampleTestSchedule(evt:CreateSampleTestScheduleEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleService).success);	
			In(Object(sampleTestScheduleService).errorCode);
			sampleTestScheduleService.createSampleTestSchedule(evt.isConfirm, createSampleTestScheduleResult);
		}
		
		public function createSampleTestScheduleResult(evt: TideResultEvent):void{
			if (Object(sampleTestScheduleService).success ){
				if( event != null ) {
					evt.context.dispatchEvent(event);		
				}
			}else if (Object(sampleTestScheduleService).errorCode != null){
				schedulePopup.showMessage(Object(sampleTestScheduleService).errorCode );
			}
		}
		
		[Observer]
		public function updateSampleTestSchedule(evt:UpdateSampleTestScheduleEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleService).success);
			In(Object(sampleTestScheduleService).errorCode);
			sampleTestScheduleService.updateSampleTestSchedule(evt.sampleTestSchedule, evt.isConfirm, updateSampleTestScheduleResult);			
		}
		
		public function updateSampleTestScheduleResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleService).success ){
				if( event != null) {
					evt.context.dispatchEvent(event);		
				}
			}else {
				if (Object(sampleTestScheduleService).errorCode !=null){
					schedulePopup.showMessage(Object(sampleTestScheduleService).errorCode );
				}
			}
		}
		
		[Observer]
		public function updateSampleTestScheduleforSchedConfirm(evt:UpdateSampleTestScheduleForSchedConfirmEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleService).success);
			In(Object(sampleTestScheduleService).errorCode);
			sampleTestScheduleService.updateSampleTestScheduleForSchedConfirm(evt.sampleTestSchedule, evt.sampleTestFileList, updateSampleTestScheduleForSchedConfirmResult);
		}
		
		public function updateSampleTestScheduleForSchedConfirmResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleService).success ){
				
				//scheduleConfirmationPopup.closeWin();
				if( event != null) {
					evt.context.dispatchEvent(event);		
				}
			}else {
				if (Object(sampleTestScheduleService).errorCode !=null){
					scheduleConfirmationPopup.showMessage(Object(sampleTestScheduleService).errorCode );
				}
			}	
		}

		[Observer]
		public function updateSampleTestScheduleForDocument(evt:UpdateSampleTestScheduleForDocumentEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleService).success);
			In(Object(sampleTestScheduleService).errorCode);
			sampleTestScheduleService.updateSampleTestScheduleForDocument(evt.sampleTestSchedule, evt.sampleTestScheduleFileList, evt.actionType, evt.isClick, updateSampleTestScheduleForDocumentResult);
		}
		
		public function updateSampleTestScheduleForDocumentResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleService).success){
				if( event != null) {
					evt.context.dispatchEvent(event);		
				}
			}
			else
			{
				if (Object(sampleTestScheduleService).errorCode !=null){
					documentDetailView.showMessage(Object(sampleTestScheduleService).errorCode );
				}
			}
		}
		
		[Observer]
		public function updateSampleTestScheduleForInstAssignment(evt:UpdateSampleTestScheduleForInstAssignmentEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleService).success);
			sampleTestScheduleService.updateSampleTestScheduleForInstAssignment(evt.sampleTestSchedule, evt.actionType, updateSampleTestScheduleForInstAssignmentResult);
		}
		
		public function updateSampleTestScheduleForInstAssignmentResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleService).success){
				if( event != null) {
					evt.context.dispatchEvent(event);		
				}
			}	
		}
		
		[Observer]
		public function updateSampleTestScheduleForInstAssignmentDetailPopup(evt:UpdateSampleTestScheduleForInstAssignmentDetailPopupEvent):void{
			event = evt.event;
			In(Object(sampleTestScheduleListInstAssignCriteriaService).success);
			sampleTestScheduleListInstAssignCriteriaService.updateSampleTestScheduleForInstAssignmentDetailPopup(evt.sampleTestSchedule, evt.actionType, evt.sampleTestScheduleList, updateSampleTestScheduleForInstAssignmentDetailPopupResult);
		}
		
		public function updateSampleTestScheduleForInstAssignmentDetailPopupResult(evt:TideResultEvent):void{
			if (Object(sampleTestScheduleListInstAssignCriteriaService).success){
				if( event != null) {
					evt.context.dispatchEvent(event);		
				}
			}	
		}
		
		[Observer]
		public function deleteSampleTestSchedule(evt:DeleteSampleTestScheduleEvent):void{
			event = evt.event;
			
			sampleTestScheduleService.deleteSampleTestSchedule(evt.sampleTestSchedule, evt.cancelSchedReason, deleteSampleTestScheduleResult);
		}
		
		public function deleteSampleTestScheduleResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function reverseSampleTestScheduleStatus(evt:ReverseSampleTestScheduleStatusEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.reverseSampleTestScheduleStatus(evt.sampleTestSchedule, reverseSampleTestScheduleStatusResult);
		}
		
		public function reverseSampleTestScheduleStatusResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveNextSampleTestSchedule(evt:RetrieveNextSampleTestScheduleEvent):void{
			
			confirmEvent = evt.confirmEvent;
			showNextScheduleEvent = evt.showNextScheduleEvent;
			sampleTestScheduleFileIn = evt.sampleTestScheduleFile;
			In(Object(sampleTestScheduleNextService).sampleTestScheduleNext);
			
			sampleTestScheduleNextService.retrieveNextSampleTestSchedule(evt.sampleTestScheduleFile, retrieveNextSampleTestScheduleResult);
		}
		
		public function retrieveNextSampleTestScheduleResult(evt: TideResultEvent):void {
			
			if (Object(sampleTestScheduleNextService).sampleTestScheduleNext !=null){
				evt.context.dispatchEvent(showNextScheduleEvent);	
			}
			else{
				evt.context.dispatchEvent(confirmEvent);
			}
		}
		
		
		[Observer]
		public function createNextSchedule(evt:CreateNextScheduleEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.createNextSchedule(evt.sampleTestSchedule, createNextScheduleResult);
		}
		
		public function createNextScheduleResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		
		[Observer]
		public function retrieveSampleTestScheduleFullDetailByScheduleId(evt:RetrieveSampleTestScheduleFullDetailByScheduleIdEvent):void{
			eventDocument = evt.eventDocument;
			sampleTestScheduleFullDetailService.retrieveSampleTestScheduleFullDetailByScheduleId(evt.scheduleId, retrieveSampleTestScheduleFullDetailByScheduleIdResult);
			
		}
		
		public function retrieveSampleTestScheduleFullDetailByScheduleIdResult(evt: TideResultEvent):void {
			
			if( eventDocument != null ) {
				evt.context.dispatchEvent(eventDocument);		
			}
		}
		
		[Observer]
		public function generateSampleTestSchedule(evt:GenerateSampleTestScheduleEvent):void{
			
			event = evt.event;
			sampleTestScheduleService.scheduleGeneration(evt.testCode, evt.orderType, scheduleGenerationResult);
		}
		
		public function scheduleGenerationResult(evt: TideResultEvent):void {
			
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
		
		[Observer]
		public function retrieveSampleTestScheduleAndShowScheduleConfirmationPopup(evt:RetrieveSampleTestScheduleAndShowScheduleConfirmationPopupEvent):void{
			screenName = evt.screenName;
			sampleTestScheduleService.retrieveSampleTestScheduleAndShowScheduleConfirmationPopup(evt.sampleTestSchedule, retrieveSampleTestScheduleAndShowScheduleConfirmationPopupResult);
			
		}
		
		public function retrieveSampleTestScheduleAndShowScheduleConfirmationPopupResult(evt: TideResultEvent):void {
			if(screenName=="scheduleCopyPopup" || 
				screenName=="schedulePopup" ||
				screenName=="scheduleDetailView")
			{
				evt.context.dispatchEvent(new ShowScheduleConfirmationPopupEvent(sampleTestSchedule));
			}
		}
		
		
	}
}
