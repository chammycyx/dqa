package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.TestFrequency;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestFileBySampleTestFileIdEvent extends AbstractTideEvent 
	{
		private var _sampleTestFileId:Number;
		
		private var _event:Event;
		
		public function RetrieveSampleTestFileBySampleTestFileIdEvent(sampleTestFileId:Number, event:Event=null):void {
			super();
			_sampleTestFileId = sampleTestFileId;
			_event = event;
		}
		
		public function get sampleTestFileId():Number{
			return _sampleTestFileId;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}