package hk.org.ha.event.pms.dqa.suppperf
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	
	public class RefreshPharmProblemPopupProblemNatureSubCatListEvent extends AbstractTideEvent 
	{	
		private var _problemNatureSubCatList:ArrayCollection;
		private var _problemNatureSubCatListSelect:ArrayCollection;
		
		public function RefreshPharmProblemPopupProblemNatureSubCatListEvent(problemNatureSubCatList:ArrayCollection, problemNatureSubCatListSelect:ArrayCollection):void
		{
			super();
			_problemNatureSubCatList = problemNatureSubCatList;
			_problemNatureSubCatListSelect = problemNatureSubCatListSelect;
		}
		
		public function get problemNatureSubCatList():ArrayCollection {
			return _problemNatureSubCatList;
		}
		
		public function get problemNatureSubCatListSelect():ArrayCollection {
			return _problemNatureSubCatListSelect;
		}
		
	}
}