package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import hk.org.ha.model.pms.dqa.vo.suppperf.ConflictOfInterestRpt;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenerateDelayDeliveryRptFormConflictOfInterestRptEvent extends AbstractTideEvent 
	{
		private var _conflictOfInterestRpt:ConflictOfInterestRpt;
		
		public function GenerateDelayDeliveryRptFormConflictOfInterestRptEvent(conflictOfInterestRptIn:ConflictOfInterestRpt):void 
		{
			super();
			_conflictOfInterestRpt = conflictOfInterestRptIn;
		}
		
		public function get conflictOfInterestRpt():ConflictOfInterestRpt {
			return _conflictOfInterestRpt;
		}
		
	}
}