package hk.org.ha.control.pms.dqa.coa
{				
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaChecklistKeyListEvent;
	import hk.org.ha.event.pms.dqa.coa.popup.ShowVerifyCoaPopupEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaChecklistKeyListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaChecklistKey;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaChecklistKeyListServiceCtl", restrict="false")]
	public class CoaChecklistKeyListServiceCtl
	{			
		[In]
		public var coaChecklistKeyListService:CoaChecklistKeyListServiceBean;		
	
		private var coaChecklistKey:CoaChecklistKey;				
		
		[In]
		public var coaChecklistKeyList:ArrayCollection;
		
		[Observer]
		public function retrieveCoaChecklistKeyList(evt:RetrieveCoaChecklistKeyListEvent):void 
		{
			coaChecklistKeyListService.retrieveCoaChecklistKeyList();
		}
		
	}
}