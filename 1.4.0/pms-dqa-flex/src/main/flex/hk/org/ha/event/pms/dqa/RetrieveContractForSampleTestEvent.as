package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.OrderType;
	
	import org.granite.tide.events.AbstractTideEvent;

	public class RetrieveContractForSampleTestEvent extends AbstractTideEvent 
	{
		private var _contractNum:String;
		private var _contractSuffix:String;
		private var _orderType:OrderType;
		private var _itemCode:String;
		private var _event:Event;
		
		public function RetrieveContractForSampleTestEvent(contractNum:String, contractSuffix:String, orderType:OrderType, itemCode:String, event:Event=null):void {
			super();
			_contractNum = contractNum;
			_contractSuffix = contractSuffix;
			_orderType = orderType;
			_itemCode = itemCode;
			_event = event;
		}
		
		public function get contractNum():String 
		{
			return _contractNum;
		}
		
		public function get contractSuffix():String 
		{
			return _contractSuffix;
		}
		
		public function get itemCode():String 
		{
			return _itemCode;
		}
		
		public function get orderType():OrderType
		{
			return _orderType;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}