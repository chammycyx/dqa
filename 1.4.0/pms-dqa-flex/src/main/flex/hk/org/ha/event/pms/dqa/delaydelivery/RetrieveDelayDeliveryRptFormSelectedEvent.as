package hk.org.ha.event.pms.dqa.delaydelivery {
	
	import hk.org.ha.model.pms.dqa.persistence.delaydelivery.DelayDeliveryRptForm;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveDelayDeliveryRptFormSelectedEvent extends AbstractTideEvent 
	{	
		private var _callBackFunction:Function;
		private var _delayDeliveryRptForm:DelayDeliveryRptForm;
		
		public function RetrieveDelayDeliveryRptFormSelectedEvent(delayDeliveryRptForm:DelayDeliveryRptForm, callBackFunction:Function=null):void {
			super();
			_callBackFunction= callBackFunction;
			_delayDeliveryRptForm = delayDeliveryRptForm;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get delayDeliveryRptForm():DelayDeliveryRptForm
		{
			return _delayDeliveryRptForm;
		}
	}
}