package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestFileListForSchedConfirmEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _itemCode:String;
		
		private var _supplierCode:String;
		
		private var _manufCode:String;
		
		private var _pharmCode:String;
		
		public function RetrieveSampleTestFileListForSchedConfirmEvent(itemCode:String, supplierCode:String, manufCode:String, pharmCode:String, event:Event=null):void {
			super();
			_event = event;
			_itemCode = itemCode;
			_supplierCode= supplierCode;
			_manufCode = manufCode;
			_pharmCode = pharmCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}
		
		public function get supplierCode():String
		{
			return _supplierCode;
		}
		
		public function get manufCode():String
		{
			return _manufCode;
		}
		
		public function get pharmCode():String
		{
			return _pharmCode;
		}
	}
}