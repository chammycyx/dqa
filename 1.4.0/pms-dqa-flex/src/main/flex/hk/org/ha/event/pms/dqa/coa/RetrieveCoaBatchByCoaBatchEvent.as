package hk.org.ha.event.pms.dqa.coa {	
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaBatchByCoaBatchEvent extends AbstractTideEvent 
	{
		
		private var _coaBatch:CoaBatch;
		
		private var _event:Event;
		
		private var _refreshEvent:Event;
		
		public function RetrieveCoaBatchByCoaBatchEvent(coaBatch:CoaBatch, event:Event=null, _refreshEvent:Event=null):void 
		{
			super();
			_coaBatch = coaBatch;
			_event = event;
			_refreshEvent = refreshEvent;
		}
		
		public function get coaBatch():CoaBatch {
			return _coaBatch;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get refreshEvent():Event {
			return _refreshEvent;
		}
	}
}