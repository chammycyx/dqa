package hk.org.ha.control.pms.dqa.sample{
	
	import org.granite.tide.events.TideResultEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveExclusionTestListEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.ExclusionTestListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import flash.events.Event;
	
	[Bindable]
	[Name("exclusionTestListServiceCtl", restrict="true")]
	public class ExclusionTestListServiceCtl {
		
	
		[In]
		public var exclusionTestListService:ExclusionTestListServiceBean;
		
		private var event:Event;
	
		[Observer]
		public function retrieveExclusionTestList(evt:RetrieveExclusionTestListEvent):void {
			event = evt.event;
			exclusionTestListService.retrieveExclusionTestList(retrieveExclusionTestListResult);
		}
		
		public function retrieveExclusionTestListResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
	
	}
}
