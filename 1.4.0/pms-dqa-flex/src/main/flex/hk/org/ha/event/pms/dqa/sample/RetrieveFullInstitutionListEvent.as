package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.Institution;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveFullInstitutionListEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function RetrieveFullInstitutionListEvent(event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}