package hk.org.ha.control.pms.dqa{
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;    
	
	[Bindable]
	[Name("fileItemServiceCtl", restrict="true")]
	public class FileItemServiceCtl {
		
		[In]
		public var fileItemService:Object;
		
		
	}
}