package hk.org.ha.event.pms.dqa.suppperf {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshQaProblemListForCaseNumEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		public function RefreshQaProblemListForCaseNumEvent(screenName:String):void {
			super();
			_screenName = screenName;	
		}
		
		public function get screenName():String 
		{
			return _screenName;
		}
	}
}