package hk.org.ha.event.pms.dqa.suppperf.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQaProblemViewEvent extends AbstractTideEvent 
	{
		private var _clearMessages:Boolean;
		
		public function ShowQaProblemViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
		
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}