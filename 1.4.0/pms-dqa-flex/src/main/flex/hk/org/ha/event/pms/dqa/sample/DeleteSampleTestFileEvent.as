package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestFile;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class DeleteSampleTestFileEvent extends AbstractTideEvent 
	{
		private var _sampleTestFile:SampleTestFile;
		private var _event:Event;
		
		public function DeleteSampleTestFileEvent(sampleTestFile:SampleTestFile, event:Event=null):void {
			super();
			_sampleTestFile = sampleTestFile;
			_event = event;
		}
		
		public function get sampleTestFile():SampleTestFile 
		{
			return _sampleTestFile;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}