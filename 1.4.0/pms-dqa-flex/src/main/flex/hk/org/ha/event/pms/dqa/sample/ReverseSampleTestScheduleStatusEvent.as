package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import flash.events.Event;
	
	public class ReverseSampleTestScheduleStatusEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestSchedule:SampleTestSchedule;
		
		public function ReverseSampleTestScheduleStatusEvent(sampleTestSchedule:SampleTestSchedule, event:Event=null):void{
			super();
			
			_event = event;
			_sampleTestSchedule = sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule {
			return _sampleTestSchedule;
		}
		
	}
}