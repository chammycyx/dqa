package hk.org.ha.event.pms.dqa.suppperf {
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenPharmProblemRptEvent extends AbstractTideEvent 
	{
		private var _pharmProblem:PharmProblem;
		
		public function GenPharmProblemRptEvent(pharmProblem:PharmProblem):void 
		{
			super();
			_pharmProblem = pharmProblem;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
	}
}