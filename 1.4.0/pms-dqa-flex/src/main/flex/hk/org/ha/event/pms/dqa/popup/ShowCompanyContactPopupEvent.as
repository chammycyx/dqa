package hk.org.ha.event.pms.dqa.popup
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowCompanyContactPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
			
		public function ShowCompanyContactPopupEvent(actionType:String):void
		{
			super();
			_actionType = actionType;
		}
				
		public function get action():String {
			return _actionType;
		}
	}
}