package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveProblemNatureParamListEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.ProblemNatureParamListServiceBean;
		
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("problemNatureParamListServiceCtl", restrict="true")]
	public class ProblemNatureParamListServiceCtl {
		
		[In]       
		public var problemNatureParamListService:ProblemNatureParamListServiceBean;
		
		private var event:Event;
		
		
		[Observer]
		public function retrieveProblemNatureParamList(evt:RetrieveProblemNatureParamListEvent):void{
			event = evt.event;
			problemNatureParamListService.retrieveProblemNatureParamList(retrieveProblemNatureParamListResult);
		}
		
		public function retrieveProblemNatureParamListResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
