package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.RiskLevel;
	
	import flash.events.Event;
	
	public class DeleteRiskLevelEvent extends AbstractTideEvent 
	{
		private var _riskLevel:RiskLevel;
		
		private var _event:Event;
		
		public function DeleteRiskLevelEvent(riskLevel:RiskLevel, event:Event=null):void {
			super();
			_riskLevel = riskLevel;	
			_event = event;
		}
		
		public function get riskLevel():RiskLevel 
		{
			return _riskLevel;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}