package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
	import hk.org.ha.model.pms.dqa.udt.sample.ScheduleStatus;
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatEvent extends AbstractTideEvent 
	{
		
		private var _recordStatus:RecordStatus;
		private var _scheduleStatus:ScheduleStatus;
		private var _fileCat:SampleTestFileCat;
		private var _event:Event;
		
		public function RetrieveSampleTestScheduleFileListByRecordStatusScheduleStatusFileCatEvent(recordStatus:RecordStatus, scheduleStatus:ScheduleStatus, fileCat:SampleTestFileCat, event:Event=null):void {
			super();
			_recordStatus = recordStatus;
			_scheduleStatus = scheduleStatus;
			_fileCat = fileCat;
			_event = event;
		}
		
		public function get recordStatus():RecordStatus{
			return _recordStatus;
		}
		
		public function get scheduleStatus():ScheduleStatus{
			return _scheduleStatus;
		}

		
		public function get fileCat():SampleTestFileCat 
		{
			return _fileCat;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}