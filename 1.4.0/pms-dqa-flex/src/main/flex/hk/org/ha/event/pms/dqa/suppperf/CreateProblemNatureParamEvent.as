package hk.org.ha.event.pms.dqa.suppperf {
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.ProblemNatureParam;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class CreateProblemNatureParamEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _problemNatureParam:ProblemNatureParam;
		
		
		public function CreateProblemNatureParamEvent(problemNatureParam:ProblemNatureParam, event:Event=null):void {
			super();
			_event = event;
			_problemNatureParam = problemNatureParam;			
		}
		
		public function get problemNatureParam():ProblemNatureParam 
		{
			return _problemNatureParam;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}