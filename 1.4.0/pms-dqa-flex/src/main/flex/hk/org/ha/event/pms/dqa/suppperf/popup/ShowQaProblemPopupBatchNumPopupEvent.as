package hk.org.ha.event.pms.dqa.suppperf.popup {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaBatchNum;

	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQaProblemPopupBatchNumPopupEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _qaBatchNum:QaBatchNum; 
		private var _qaBatchNumList:ListCollectionView;
		private var _action:String;
		private var _selectedIndexIn:int;
		
		public function ShowQaProblemPopupBatchNumPopupEvent(qaBatchNum:QaBatchNum, 
															   qaBatchNumList:ListCollectionView,
  															   action:String,
															   selectedIndexIn:int,
															   event:Event=null):void {
			super();
			_event = event;
			_qaBatchNum = qaBatchNum; 
			_qaBatchNumList = qaBatchNumList; 
			_action = action;
			_selectedIndexIn = selectedIndexIn;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaBatchNum():QaBatchNum {
			return _qaBatchNum;
		}
		
		public function get qaBatchNumList():ListCollectionView {
			return _qaBatchNumList;
		}
		
		public function get action():String {
			return _action;
		}
		
		public function get selectedIndexIn():int {
			return _selectedIndexIn;
		}
		
	}
}