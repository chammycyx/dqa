package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrievePharmProblemFormDataByPharmProblemEvent extends AbstractTideEvent 
	{	
		private var _callBackEvent:Event;
		private var _pharmProblem:PharmProblem;
		
		public function RetrievePharmProblemFormDataByPharmProblemEvent(pharmProblem:PharmProblem, callBackEvent:Event):void {
			super();
			_pharmProblem = pharmProblem;
			_callBackEvent = callBackEvent;
		}
		
		public function get pharmProblem():PharmProblem
		{
			return _pharmProblem;
		}
		
		public function get callBackEvent():Event
		{
			return _callBackEvent;
		}
	}
}