package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleItemListForSampleTestEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _recordStatus:RecordStatus;
		
		private var _itemCode:String;
		
		public function RetrieveSampleItemListForSampleTestEvent(recordStatus:RecordStatus, itemCode:String, event:Event=null):void {
			super();
			_event = event;
			_recordStatus = recordStatus;
			_itemCode = itemCode;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get recordStatus():RecordStatus
		{
			return _recordStatus;
		}
		
		public function get itemCode():String
		{
			return _itemCode;
		}	
	}
}