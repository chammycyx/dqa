package hk.org.ha.control.pms.dqa{
	
	import hk.org.ha.event.pms.dqa.RefreshContractListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractListByItemCodeOrderTypeWithCallBackEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractListForCoaLiveContractListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractListForSampleTestEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractListLikeContractByItemCodeEvent;
	import hk.org.ha.event.pms.dqa.RetrieveContractListLikeContractByItemCodeOrderTypeEvent;
	import hk.org.ha.model.pms.dqa.biz.ContractListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("contractListServiceCtl", restrict="true")]
	public class ContractListServiceCtl {
		
		[In]
		public var contractListService:ContractListServiceBean;
		
		[In]
		public var contractList:ArrayCollection = new ArrayCollection();

		public var screenName:String;
		
		private var event:Event;
	
		[Observer]  
		public function retrieveContractListForCoaLiveContractList(evt:RetrieveContractListForCoaLiveContractListEvent):void{
			screenName = evt.screenName;
			contractListService.retrieveContractListForCoaLiveContractList(evt.contractNum, evt.itemCode, retrieveContractListForCoaLiveContractListResult);
		}
		
		public function retrieveContractListForCoaLiveContractListResult(evt: TideResultEvent):void {
			dispatchEvent(new RefreshContractListEvent(screenName));
		} 
		
		
		[Observer]
		public function retrieveContractListForSampleTest(evt:RetrieveContractListForSampleTestEvent):void{
			event = evt.event;
			contractListService.retrieveContractListForSampleTest(evt.contractNum, evt.contractSuffix, evt.orderType, evt.itemCode, retrieveContractListForSampleTestResult);
		}
		
		public function retrieveContractListForSampleTestResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveContractListLikeContractByItemCode(evt:RetrieveContractListLikeContractByItemCodeEvent):void{
			event = evt.event;
			contractListService.retrieveContractListLikeContractByItemCode(evt.contractNum, evt.contractSuffix, evt.itemCode, retrieveContractListLikeContractByItemCodeResult);
		}
		
		public function retrieveContractListLikeContractByItemCodeResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveContractListLikeContractByItemCodeOrderType(evt:RetrieveContractListLikeContractByItemCodeOrderTypeEvent):void{
			event = evt.event;
			contractListService.retrieveContractListLikeContractByItemCodeOrderType(evt.contractNum, evt.contractSuffix, evt.itemCode, evt.orderType, retrieveContractListLikeContractByItemCodeOrderTypeResult);
		}
		
		public function retrieveContractListLikeContractByItemCodeOrderTypeResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveContractListByItemCodeOrderTypeWithCallBack(evt:RetrieveContractListByItemCodeOrderTypeWithCallBackEvent):void {
			contractListService.retrieveContractListLikeContractByItemCodeOrderType(
				evt.contractNum, evt.contractSuffix, evt.itemCode, evt.orderType, 
				function(tideResultEvent:TideResultEvent):void {
					if (evt.callBack != null) {
						evt.callBack();
					}
				}
			);
		}
	}
}