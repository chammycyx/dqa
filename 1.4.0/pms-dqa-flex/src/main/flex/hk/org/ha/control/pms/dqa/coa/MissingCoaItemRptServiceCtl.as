package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveMissingCoaItemRptListEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.MissingCoaItemRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("missingCoaItemRptServiceCtl", restrict="false")]
	public class MissingCoaItemRptServiceCtl
	{
		[In]
		public var missingCoaItemRptService:MissingCoaItemRptServiceBean;
		
			
		private var event:Event;
		
		[Observer]
		public function retrieveMissingCoaItemRptList(evt:RetrieveMissingCoaItemRptListEvent):void
		{
			event = evt.event;
			missingCoaItemRptService.retrieveMissingCoaItemRptList(retrieveMissingCoaItemRptListResult);
		}
		
		public function retrieveMissingCoaItemRptListResult(evt:TideResultEvent):void{
			if ( event == null){
				var today:Date = new Date();
				var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
				evt.context.dispatchEvent( new GenExcelDocumentEvent("/excelTemplate/missingCoaItemRpt.xhtml", "MissingCoaItemReport", "MissingCoaItemReport_"+fileDate) );
			}else{
				evt.context.dispatchEvent( event );
			}
		}
	}	
}