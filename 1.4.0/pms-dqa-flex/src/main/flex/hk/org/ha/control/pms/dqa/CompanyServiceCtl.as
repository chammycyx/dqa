package hk.org.ha.control.pms.dqa
{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.RetrieveCompanyByCompanyCodeEvent;
	import hk.org.ha.event.pms.dqa.ChangePharmCompanyManufViewStateEvent;
	
	import hk.org.ha.model.pms.dqa.biz.CompanyServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("companyServiceCtl", restrict="true")]
	public class CompanyServiceCtl
	{	
		private var companyCode:String;
			
		private var event:Event;
	
		[In]
		public var companyService:CompanyServiceBean;		
			
		[Observer]
		public function retrieveCompanyByCompanyCode(evt:RetrieveCompanyByCompanyCodeEvent):void 
		{
			this.companyCode = evt.companyCode;
			companyService.retrieveCompanyByCompanyCode(this.companyCode, retrieveCompanyByCompanyCodeResult);		
		}	
				
		private function retrieveCompanyByCompanyCodeResult(evt:TideResultEvent):void {
			evt.context.dispatchEvent(new ChangePharmCompanyManufViewStateEvent());				
		}
	}
}