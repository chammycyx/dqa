package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveEventLogListByQaProblemCriteriaEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveEventLogListForPharmProblemFormEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrieveFaxDetailLogRptByEventLogListEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.EventLogListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("eventLogListServiceCtl", restrict="true")]
	public class EventLogListServiceCtl {
		
		[In]       
		public var eventLogListService:EventLogListServiceBean;
		
		private var event:Event;
		
		private var callBackFunc:Function;
		
		[Observer]
		public function retrieveEventLogListByQaProblemCriteria(evt:RetrieveEventLogListByQaProblemCriteriaEvent):void{
			event = evt.event;
			eventLogListService.retrieveEventLogListByQaProblemCriteria(evt.qaProblemCriteria, 
				retrieveEventLogListByQaProblemCriteriaResult);
		}
		
		public function retrieveEventLogListByQaProblemCriteriaResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveFaxDetailLogRptByEventLogList(evt:RetrieveFaxDetailLogRptByEventLogListEvent):void{
			eventLogListService.retrieveFaxDetailLogByEventLogList(evt.eventLogList, retrieveFaxDetailLogRptByEventLogListResult);
		}
		
		public function retrieveFaxDetailLogRptByEventLogListResult(evt:TideResultEvent):void{
			if ( event == null){
				var today:Date = new Date();
				var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
				evt.context.dispatchEvent( new GenExcelDocumentEvent("/excelTemplate/faxDetailLogRpt.xhtml", "FaxDetailLogReport", "FaxDetailLogReport_"+fileDate) );
			}else{
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function retrieveEventLogListForPharmProblemForm(evt:RetrieveEventLogListForPharmProblemFormEvent):void {
			callBackFunc = evt.callBackFunc
			eventLogListService.retrieveEventLogListForPharmProblemForm(evt.qaProblemCriteria,retrieveEventLogListForPharmProblemFormResult);
		}
		
		private function retrieveEventLogListForPharmProblemFormResult(evt:TideResultEvent):void {
			if (callBackFunc != null) {
				callBackFunc(evt.result);
			}
		}
	}
}
