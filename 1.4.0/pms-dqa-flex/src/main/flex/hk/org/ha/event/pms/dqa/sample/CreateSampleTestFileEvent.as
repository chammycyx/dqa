package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;

	import org.granite.tide.events.AbstractTideEvent;
	
	public class CreateSampleTestFileEvent extends AbstractTideEvent 
	{
		private var _event:Event;

		public function CreateSampleTestFileEvent( event:Event=null):void {
			super();
			_event = event;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}