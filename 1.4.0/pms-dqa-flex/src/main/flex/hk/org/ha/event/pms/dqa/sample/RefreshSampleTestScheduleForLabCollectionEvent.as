package hk.org.ha.event.pms.dqa.sample {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSampleTestScheduleForLabCollectionEvent extends AbstractTideEvent
	{
		private var _event:Event;
		
		public function RefreshSampleTestScheduleForLabCollectionEvent(event:Event=null):void
		{
			super();
			_event = event;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
	
}