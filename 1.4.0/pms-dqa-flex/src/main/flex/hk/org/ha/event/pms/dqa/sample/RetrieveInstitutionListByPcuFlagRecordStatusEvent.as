package hk.org.ha.event.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.YesNoFlag;
	import hk.org.ha.model.pms.dqa.udt.RecordStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveInstitutionListByPcuFlagRecordStatusEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		private var _pcuFlag:YesNoFlag;
		private var _recordStatus:RecordStatus;
		
		
		public function RetrieveInstitutionListByPcuFlagRecordStatusEvent(pcuFlag:YesNoFlag, recordStatus:RecordStatus, event:Event=null):void {
			super();
			_event = event;
			_pcuFlag = pcuFlag;
			_recordStatus = recordStatus;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get pcuFlag():YesNoFlag {
			return _pcuFlag;
		}
		
		public function get recordStatus():RecordStatus {
			return _recordStatus;
		}
	}
}