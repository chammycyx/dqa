package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	
	public class AddUserInfoEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		public function AddUserInfoEvent(event:Event=null):void {
			super();
			_event = event;			
		}
		
		public function get event():Event {
			return _event;
		}
	}
}