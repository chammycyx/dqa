package hk.org.ha.event.pms.dqa.sample.popup
{
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestScheduleFile;
	import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowTestResultFileUploadPopupEvent extends AbstractTideEvent 
	{	
		private var _sampleTestScheduleFile:SampleTestScheduleFile;
		private var _sampleTestfileCat:SampleTestFileCat;
		
		public function ShowTestResultFileUploadPopupEvent(sampleTestfileCat:SampleTestFileCat):void
		{
			super();
			_sampleTestfileCat = sampleTestfileCat;
		}
		
		public function get sampleTestfileCat():SampleTestFileCat {
			return _sampleTestfileCat;
		}
	}
}