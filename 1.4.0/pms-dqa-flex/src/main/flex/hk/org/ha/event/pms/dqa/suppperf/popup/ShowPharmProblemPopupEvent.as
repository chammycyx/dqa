package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	
	public class ShowPharmProblemPopupEvent extends AbstractTideEvent 
	{	
		private var _actionType:String;
		private var _problemNatureSubCatList:ArrayCollection;
		private var _pharmProblem:PharmProblem;
		private var _screenName:String;
		
		public function ShowPharmProblemPopupEvent(actionType:String, problemNatureSubCatList:ArrayCollection, pharmProblem:PharmProblem, screenName:String):void
		{
			super();
			_actionType = actionType;
			_problemNatureSubCatList = problemNatureSubCatList;
			_pharmProblem = pharmProblem;
			_screenName = screenName;
		}
		
		public function get action():String {
			return _actionType;
		}
		
		public function get problemNatureSubCatList():ArrayCollection {
			return _problemNatureSubCatList;
		}
		
		public function get pharmProblem():PharmProblem {
			return _pharmProblem;
		}
		
		public function get screenName():String {
			return _screenName;
		}
		
	}
}