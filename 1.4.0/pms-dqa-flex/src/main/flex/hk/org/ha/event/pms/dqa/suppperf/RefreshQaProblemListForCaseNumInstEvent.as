package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	import mx.collections.ArrayCollection;
	
	public class RefreshQaProblemListForCaseNumInstEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblemListForCaseNum:ArrayCollection;
		private var _screenName:String;
		
		public function RefreshQaProblemListForCaseNumInstEvent(screenName:String, qaProblemListForCaseNum:ArrayCollection):void {
			super();
			_event = event;
			_qaProblemListForCaseNum = qaProblemListForCaseNum;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblemListForCaseNum():ArrayCollection
		{
			return _qaProblemListForCaseNum;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}