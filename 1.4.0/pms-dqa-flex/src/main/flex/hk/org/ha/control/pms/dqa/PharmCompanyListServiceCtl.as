package hk.org.ha.control.pms.dqa{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.RetrievePharmCompanyListEvent;
	import hk.org.ha.event.pms.dqa.RetrievePharmCompanyListLikeEvent;
	import hk.org.ha.model.pms.dqa.biz.PharmCompanyListServiceBean;
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("pharmCompanyListServiceCtl", restrict="true")]
	public class PharmCompanyListServiceCtl {
		
		[In]
		public var pharmCompanyListService:PharmCompanyListServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrievePharmCompanyList(evt:RetrievePharmCompanyListEvent):void{
			event = evt.event;
			pharmCompanyListService.retrievePharmCompanyList(retrievePharmCompanyListResult);			
		}
		
		public function retrievePharmCompanyListResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrievePharmCompanyListLike(evt:RetrievePharmCompanyListLikeEvent):void{
			event = evt.event;
			pharmCompanyListService.retrievePharmCompanyListLike(evt.pharmCompanyCode, retrievePharmCompanyListLikeResult);			
		}
		
		public function retrievePharmCompanyListLikeResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
	}
}