package hk.org.ha.event.pms.dqa.coa.popup {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	
	
	public class ShowCoaItemMaintMessageEvent extends AbstractTideEvent 
	{
		private var _msgCode:String;
		
		
		public function ShowCoaItemMaintMessageEvent(msgCode:String):void {
			super();
			_msgCode = msgCode;			
		}
		
		public function get msgCode():String 
		{
			return _msgCode;
		}
		
	}
}