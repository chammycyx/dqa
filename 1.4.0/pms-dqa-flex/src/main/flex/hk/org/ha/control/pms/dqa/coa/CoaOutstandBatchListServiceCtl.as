package hk.org.ha.control.pms.dqa.coa
{		
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.coa.RefreshOutstandBatchCoaPropEvent;
	import hk.org.ha.event.pms.dqa.coa.RetrieveCoaOutstandBatchListForOutstandBatchCoaEvent;
	import hk.org.ha.model.pms.dqa.biz.coa.CoaOutstandBatchListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("coaOutstandBatchListServiceCtl", restrict="false")]
	public class CoaOutstandBatchListServiceCtl
	{	
		[In]
		public var coaOutstandBatchListService:CoaOutstandBatchListServiceBean;
		
		[In]
		public var maxCreateDateMonthRangeForOutstandBatchCoa:Number;
		
		[In]
		public var defaultCreateDateMonthRangeForOutstandBatchCoa:Number;
		
		private var event:Event;
		
		[Observer]
		public function retrieveCoaOutstandBatchListForOutstandBatchCoa(evt:RetrieveCoaOutstandBatchListForOutstandBatchCoaEvent):void 
		{						
			event = evt.event;	
			coaOutstandBatchListService.retrieveCoaOutstandBatchListForOutstandBatchCoa( evt.criteria, retrieveCoaOutstandBatchListForOutstandBatchCoaResult );
		}
		
		private function retrieveCoaOutstandBatchListForOutstandBatchCoaResult(evt:TideResultEvent):void 
		{				
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
		
		[Observer]
		public function refreshPropValueForOutstandBatchCoa(evt:RefreshOutstandBatchCoaPropEvent):void
		{
			event = evt.event;
			coaOutstandBatchListService.getDefaultCreateDateMonthRangeForOutstandBatchCoa();
			coaOutstandBatchListService.getMaxCreateDateMonthRangeForOutstandBatchCoa( refreshPropValueForOutstandBatchCoaResult );
		}
		
		private function refreshPropValueForOutstandBatchCoaResult(evt:TideResultEvent):void
		{
			if(event != null) {
				evt.context.dispatchEvent( event );
			}
		}
	}
}