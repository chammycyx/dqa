package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemFileListServiceBean;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrievePharmProblemFileListByQaProblemEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblemFile;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pharmProblemFileListServiceCtl", restrict="true")]
	public class PharmProblemFileListServiceCtl {
		
		[In]       
		public var pharmProblemFileListService:PharmProblemFileListServiceBean;
		
		private var event:Event;
		
		
		[Observer]
		public function retrievePharmProblemFileListByQaProblem(evt:RetrievePharmProblemFileListByQaProblemEvent):void{
			event = evt.event;
			pharmProblemFileListService.retrievePharmProblemFileListByQaProblem(evt.qaProblem, 
				retrievePharmProblemFileListByQaProblemResult);
		}
		
		public function retrievePharmProblemFileListByQaProblemResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
