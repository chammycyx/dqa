package hk.org.ha.event.pms.dqa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshPharmCompanyListEvent extends AbstractTideEvent 
	{
		private var _screenName:String;
		
		public function RefreshPharmCompanyListEvent(screenName:String):void {
			super();
			_screenName = screenName;
		}
		
		public function get screenName():String {
			return _screenName;
		}
	}
}