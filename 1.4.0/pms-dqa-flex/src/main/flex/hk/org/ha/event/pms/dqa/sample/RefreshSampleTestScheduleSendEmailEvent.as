package hk.org.ha.event.pms.dqa.sample
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.LetterTemplate;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshSampleTestScheduleSendEmailEvent extends AbstractTideEvent 
	{
		
		private var _screenName:String;
		private var _event:Event;
		
		public function RefreshSampleTestScheduleSendEmailEvent(screenName:String, event:Event=null):void {
			super();
			_screenName = screenName;
			_event = event;
		}
		
		
		public function get screenName():String
		{
			return _screenName;
		}
		
		public function get event():Event
		{
			return _event;
		}
	}
}