package hk.org.ha.event.pms.dqa.coa {
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaBatch;
	import hk.org.ha.model.pms.dqa.persistence.coa.CoaItem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveCoaFileFolderListWithoutBatchOthersEvent extends AbstractTideEvent 
	{		
		private var _coaBatchId:Number;		
		
		private var _event:Event;
		
		public function RetrieveCoaFileFolderListWithoutBatchOthersEvent(coaBatchId:Number, event:Event=null):void 
		{
			super();
			_coaBatchId = coaBatchId;
			_event = event;
		}
		
		public function get coaBatchId():Number {
			return _coaBatchId;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}