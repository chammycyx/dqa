package hk.org.ha.control.pms.dqa{
	
	import hk.org.ha.event.pms.dqa.RetrieveSupplierListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveSupplierListLikeEvent;
	import hk.org.ha.event.pms.dqa.RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent;
	import hk.org.ha.event.pms.dqa.RetrieveSupplierPharmCompanyManufacturerContactListEvent;
	import hk.org.ha.event.pms.dqa.RetrieveMspContactListByContractEvent;
	import hk.org.ha.event.pms.dqa.coa.show.ShowCoaItemMaintenanceViewEvent;
	import hk.org.ha.model.pms.dqa.biz.SupplierListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.SupplierPharmCompanyManufacturerContactListServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("supplierListServiceCtl", restrict="true")]
	public class SupplierListServiceCtl {
		
		[In]
		public var supplierListService:SupplierListServiceBean;
		
		[In]
		public var supplierPharmCompanyManufacturerContactListService:SupplierPharmCompanyManufacturerContactListServiceBean;

		[In]
		public var supplierList:ArrayCollection = new ArrayCollection();
		
		private var event:Event;
		
		
		[Observer]
		public function retrieveSupplierList(evt:RetrieveSupplierListEvent):void{
			event = evt.event;
			supplierListService.retrieveSupplierList(retrieveSupplierListResult);
		}
		
		public function retrieveSupplierListResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveSupplierListLike(evt:RetrieveSupplierListLikeEvent):void{
			event = evt.event;
			supplierListService.retrieveSupplierListLike(evt.supplierCode, retrieveSupplierListLikeResult);
		}
		
		public function retrieveSupplierListLikeResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveSupplierPharmCompanyManufacturerContactList(evt:RetrieveSupplierPharmCompanyManufacturerContactListEvent):void{
			event = evt.event;
			supplierPharmCompanyManufacturerContactListService.retrieveSupplierPharmCompanyManufacturerContactList(evt.sampleTestSchedule, retrieveSupplierPharmCompanyManufacturerContactListResult);
		}
		
		public function retrieveSupplierPharmCompanyManufacturerContactListResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen(evt:RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent):void{
			event = evt.event;
			supplierPharmCompanyManufacturerContactListService.retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreen(evt.sampleTestSchedule, retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenResult);
		}
		
		public function retrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
		
		[Observer]
		public function retrieveMspContactListByContract(evt:RetrieveMspContactListByContractEvent):void{
			event = evt.event;
			supplierPharmCompanyManufacturerContactListService.retrieveMspContactListByContract(evt.contract, retrieveMspContactListByContractResult);
		}
		
		public function retrieveMspContactListByContractResult(evt:TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent( event );
			}	
		}
	}
}