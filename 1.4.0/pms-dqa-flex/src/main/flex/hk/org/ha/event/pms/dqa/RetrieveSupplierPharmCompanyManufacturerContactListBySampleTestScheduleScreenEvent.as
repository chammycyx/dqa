package hk.org.ha.event.pms.dqa {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	public class RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent extends AbstractTideEvent 
	{
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _event:Event;
		
		public function RetrieveSupplierPharmCompanyManufacturerContactListBySampleTestScheduleScreenEvent(sampleTestSchedule:SampleTestSchedule, event:Event=null):void {
			super();
			_sampleTestSchedule = sampleTestSchedule;
			_event = event;
		}
		
		public function get sampleTestSchedule():SampleTestSchedule 
		{
			return _sampleTestSchedule;
		}
		
		public function get event():Event {
			return _event;
		}
		
	}
}