package hk.org.ha.event.pms.dqa.coa {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ResetCoaItemMaintEvent extends AbstractTideEvent 
	{
		public function ResetCoaItemMaintEvent():void {
			super();
		}
	}
}