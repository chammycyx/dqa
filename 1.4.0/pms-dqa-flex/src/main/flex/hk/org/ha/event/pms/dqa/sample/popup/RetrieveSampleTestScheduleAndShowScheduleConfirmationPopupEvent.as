package hk.org.ha.event.pms.dqa.sample.popup
{
	import flash.events.Event;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestScheduleAndShowScheduleConfirmationPopupEvent extends AbstractTideEvent 
	{	
		
		private var _event:Event;
		private var _sampleTestSchedule:SampleTestSchedule;
		private var _screenName:String;
		
		public function RetrieveSampleTestScheduleAndShowScheduleConfirmationPopupEvent(sampleTestSchedule:SampleTestSchedule, screenName:String, event:Event=null):void
		{
			super();
			_sampleTestSchedule = sampleTestSchedule;
			_screenName = screenName;
			_event = event;	
		}
		
		public function get sampleTestSchedule():SampleTestSchedule{
			
			return _sampleTestSchedule;
		}
		
		public function get screenName():String{
			
			return _screenName;
		}
		
		public function get event():Event {
			return _event;
		}
	}
}