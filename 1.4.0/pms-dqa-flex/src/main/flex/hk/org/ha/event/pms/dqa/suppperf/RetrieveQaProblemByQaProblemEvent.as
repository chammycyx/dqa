package hk.org.ha.event.pms.dqa.suppperf {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	public class RetrieveQaProblemByQaProblemEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblem:QaProblem;
		private var _screenName:String;
		
		public function RetrieveQaProblemByQaProblemEvent(qaProblem:QaProblem, screenName:String):void {
			super();
			_event = event;
			_qaProblem = qaProblem;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblem():QaProblem
		{
			return _qaProblem;
		}
		
		public function get screenName():String
		{
			return _screenName;
		}
	}
}