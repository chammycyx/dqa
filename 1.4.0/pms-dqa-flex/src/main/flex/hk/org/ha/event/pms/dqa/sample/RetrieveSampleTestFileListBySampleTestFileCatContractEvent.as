package hk.org.ha.event.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.udt.sample.SampleTestFileCat;
	import hk.org.ha.model.pms.dqa.persistence.Contract;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestFileListBySampleTestFileCatContractEvent extends AbstractTideEvent 
	{
		private var _event:Event;
		
		private var _sampleTestFileCat:SampleTestFileCat;
		private var _contract:Contract;
		
		public function RetrieveSampleTestFileListBySampleTestFileCatContractEvent(sampleTestFileCat:SampleTestFileCat, contract:Contract, event:Event=null):void {
			super();
			_event = event;
			_sampleTestFileCat = sampleTestFileCat;
			_contract = contract;
		}	
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestFileCat():SampleTestFileCat
		{
			return _sampleTestFileCat;
		}
		
		public function get contract():Contract
		{
			return _contract;
		}
	}
}