package hk.org.ha.event.pms.dqa.sample {
	
	import org.granite.tide.events.AbstractTideEvent;
	import hk.org.ha.model.pms.dqa.persistence.sample.SampleTestSchedule;
	
	import mx.collections.ListCollectionView;
	
	import flash.events.Event;
	
	public class UpdateSampleTestScheduleForDrugSampleEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		private var _sampleTestScheduleList:ListCollectionView;
		
		public function UpdateSampleTestScheduleForDrugSampleEvent(sampleTestScheduleList:ListCollectionView, event:Event=null):void{
			super();
			
			_event = event;
			_sampleTestScheduleList = sampleTestScheduleList;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get sampleTestScheduleList():ListCollectionView {
			return _sampleTestScheduleList;
		}
		
		
	}
}