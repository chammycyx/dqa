package hk.org.ha.event.pms.dqa.sample {
	
import flash.events.Event;
	
import mx.collections.ListCollectionView;
	
import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveSampleTestStockOnHandRptEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		public function RetrieveSampleTestStockOnHandRptEvent(event:Event=null):void {
			super();
			_event = event;
		}
			
		public function get event():Event {
			return _event;
		}
	}
}
