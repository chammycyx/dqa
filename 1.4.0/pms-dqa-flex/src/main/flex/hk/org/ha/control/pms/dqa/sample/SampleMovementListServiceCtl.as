package hk.org.ha.control.pms.dqa.sample{
	
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleMovementListBySampleMovementCriteriaEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleMovementListServiceBean;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleMovementExportListEvent;
	import hk.org.ha.event.pms.dqa.sample.RetrieveSampleTestStockOnHandRptEvent;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestRptServiceBean;
	import hk.org.ha.model.pms.dqa.biz.sample.SampleTestStockOnhandRptServiceBean;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("sampleMovementListServiceCtl", restrict="true")]
	public class SampleMovementListServiceCtl {
		
		
		[In]
		public var sampleMovementListService:SampleMovementListServiceBean;
		
		[In]
		public var sampleTestRptService:SampleTestRptServiceBean;
	
		[In]
		public var sampleTestStockOnhandRptService:SampleTestStockOnhandRptServiceBean;
		
		private var event:Event;
		
		[Observer]
		public function retrieveSampleMovementListBySampleMovementCriteria(evt:RetrieveSampleMovementListBySampleMovementCriteriaEvent):void {
			event = evt.event;
			sampleMovementListService.retrieveSampleMovementListBySampleMovementCriteria(evt.sampleMovementCriteria);
		}
		
		public function retrieveSampleMovementListBySampleMovementCriteriaResult(evt: TideResultEvent):void {
			if ( event != null){
				evt.context.dispatchEvent( event );
			}	
		}
		
		
		[Observer]
		public function retrieveEnquiredSampleMovementExportList(evt:RetrieveSampleMovementExportListEvent):void{
			event = evt.event;
			sampleTestRptService.retrieveSampleMovementList(evt.sampleMovementKeyExportList, retrieveSampleMovementExportListResult);
		}
	
		public function retrieveSampleMovementExportListResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}
			
		[Observer]
		public function retrieveSampleMovementStockOnhandExportList(evt:RetrieveSampleTestStockOnHandRptEvent):void{
			event = evt.event;
			sampleTestStockOnhandRptService.retrieveSampleMovementStockOnhandList(retrieveSampleMovementStockOnhandExportListResult);
		}
		
		public function retrieveSampleMovementStockOnhandExportListResult(evt: TideResultEvent):void{
			if( event != null ) {
				evt.context.dispatchEvent(event);		
			}
		}

	}
}
