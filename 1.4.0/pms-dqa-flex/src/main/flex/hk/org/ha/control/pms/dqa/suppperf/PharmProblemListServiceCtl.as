package hk.org.ha.control.pms.dqa.suppperf{
	import flash.events.Event;
	
	import hk.org.ha.event.pms.dqa.suppperf.RetrievePharmProblemListByPharmProblemCriteriaEvent;
	import hk.org.ha.event.pms.dqa.suppperf.RetrievePharmProblemListWithNullProblemStatusEvent;
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemListServiceBean;
	import hk.org.ha.model.pms.dqa.biz.suppperf.PharmProblemNewListServiceBean;
	import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
	import hk.org.ha.model.pms.dqa.vo.suppperf.PharmProblemCriteria;
	
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.TideResultEvent;
	import org.granite.tide.seam.In;
	
	[Bindable]
	[Name("pharmProblemListServiceCtl", restrict="true")]
	public class PharmProblemListServiceCtl {
		
		[In]       
		public var pharmProblemListService:PharmProblemListServiceBean;
		
		[In]       
		public var pharmProblemNewListService:PharmProblemNewListServiceBean;
		
		[In]
		public var pharmProblem:PharmProblem;
		
		private var event:Event;
		
		private var retrievePharmProblemListByPharmProblemCriteriaEvent:RetrievePharmProblemListByPharmProblemCriteriaEvent;
		
		[Observer]
		public function retrievePharmProblemListByPharmProblemCriteria(evt:RetrievePharmProblemListByPharmProblemCriteriaEvent):void{
			event = evt.event;
			retrievePharmProblemListByPharmProblemCriteriaEvent= evt;
			
			pharmProblemListService.retrievePharmProblemListByPharmProblemCriteria(evt.pharmProblemCriteria, retrievePharmProblemListByPharmProblemCriteriaResult);
		}
		
		public function retrievePharmProblemListByPharmProblemCriteriaResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
				
			}
			if (retrievePharmProblemListByPharmProblemCriteriaEvent.callBack != null) {
				retrievePharmProblemListByPharmProblemCriteriaEvent.callBack();
			}
			
			
		}
		//QAPROBLEM
		[Observer]
		public function retrievePharmProblemListWithNullProblemStatus(evt:RetrievePharmProblemListWithNullProblemStatusEvent):void{
			event = evt.event;
			pharmProblemNewListService.retrievePharmProblemListWithNullProblemStatus(retrievePharmProblemListWithNullProblemStatusResult);
		}
		
		public function retrievePharmProblemListWithNullProblemStatusResult(evt:TideResultEvent):void{
			if ( event != null ){
				evt.context.dispatchEvent( event );
			}
		}
		
	}
}
