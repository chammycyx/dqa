package hk.org.ha.event.pms.dqa.suppperf.popup
{
	import flash.events.Event;
	
	import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowQaProblemDeletePopupEvent extends AbstractTideEvent 
	{	
		private var _event:Event;
		
		private var _qaProblem:QaProblem;
		private var _screenName:String;
		
		public function ShowQaProblemDeletePopupEvent(qaProblem:QaProblem, screenName:String,event:Event=null):void
		{
			super();
			_event = event;
			_qaProblem = qaProblem;
			_screenName = screenName;
		}
		
		public function get event():Event {
			return _event;
		}
		
		public function get qaProblem():QaProblem{
			return _qaProblem;
		}
		
		public function get screenName():String{
			return _screenName;
		}
	}
}