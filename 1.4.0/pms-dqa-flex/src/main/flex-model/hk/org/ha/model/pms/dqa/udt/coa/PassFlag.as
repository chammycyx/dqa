/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.dqa.udt.coa {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.udt.coa.PassFlag")]
    public class PassFlag extends Enum {

        public static const Passed:PassFlag = new PassFlag("Passed", _, "Y", "Passed");
        public static const NotYetPassed:PassFlag = new PassFlag("NotYetPassed", _, "N", "Not yet passed");

        function PassFlag(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Passed.name), restrictor, (dataValue || Passed.dataValue), (displayValue || Passed.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Passed, NotYetPassed];
        }

        public static function valueOf(name:String):PassFlag {
            return PassFlag(Passed.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PassFlag {
            return PassFlag(Passed.dataConstantOf(dataValue));
        }
    }
}