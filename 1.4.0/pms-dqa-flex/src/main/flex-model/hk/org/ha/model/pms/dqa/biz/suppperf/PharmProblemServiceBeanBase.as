/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (PharmProblemServiceBean.as).
 */

package hk.org.ha.model.pms.dqa.biz.suppperf {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dqa.persistence.suppperf.PharmProblem;
    import mx.collections.ListCollectionView;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class PharmProblemServiceBeanBase extends Component {

        public function setSuppPerfNotificationToEmail(arg0:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("setSuppPerfNotificationToEmail", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("setSuppPerfNotificationToEmail", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("setSuppPerfNotificationToEmail", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function addPharmProblem(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("addPharmProblem", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("addPharmProblem", resultHandler);
            else if (resultHandler == null)
                callProperty("addPharmProblem");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createPharmProblem(arg0:PharmProblem, arg1:ListCollectionView, arg2:ListCollectionView, arg3:ListCollectionView, arg4:ListCollectionView, arg5:Boolean, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createPharmProblem", arg0, arg1, arg2, arg3, arg4, arg5, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createPharmProblem", arg0, arg1, arg2, arg3, arg4, arg5, resultHandler);
            else if (resultHandler == null)
                callProperty("createPharmProblem", arg0, arg1, arg2, arg3, arg4, arg5);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createUpdateDraftPharmProblem(arg0:PharmProblem, arg1:ListCollectionView, arg2:ListCollectionView, arg3:ListCollectionView, arg4:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createUpdateDraftPharmProblem", arg0, arg1, arg2, arg3, arg4, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createUpdateDraftPharmProblem", arg0, arg1, arg2, arg3, arg4, resultHandler);
            else if (resultHandler == null)
                callProperty("createUpdateDraftPharmProblem", arg0, arg1, arg2, arg3, arg4);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function savePharmProblemCountry(arg0:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("savePharmProblemCountry", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("savePharmProblemCountry", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("savePharmProblemCountry", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function constructEmailContent(arg0:PharmProblem, arg1:ListCollectionView, arg2:ListCollectionView, arg3:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("constructEmailContent", arg0, arg1, arg2, arg3, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("constructEmailContent", arg0, arg1, arg2, arg3, resultHandler);
            else if (resultHandler == null)
                callProperty("constructEmailContent", arg0, arg1, arg2, arg3);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function constructContractSave(arg0:PharmProblem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("constructContractSave", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("constructContractSave", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("constructContractSave", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function constructPharmProblemCountrysSave(arg0:PharmProblem, arg1:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("constructPharmProblemCountrysSave", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("constructPharmProblemCountrysSave", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("constructPharmProblemCountrysSave", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function constructPharmBatchNum(arg0:PharmProblem, arg1:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("constructPharmBatchNum", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("constructPharmBatchNum", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("constructPharmBatchNum", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function retrievePharmProblemByPharmProblem(arg0:PharmProblem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("retrievePharmProblemByPharmProblem", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("retrievePharmProblemByPharmProblem", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("retrievePharmProblemByPharmProblem", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function deletePharmProblem(arg0:PharmProblem, arg1:String, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("deletePharmProblem", arg0, arg1, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("deletePharmProblem", arg0, arg1, resultHandler);
            else if (resultHandler == null)
                callProperty("deletePharmProblem", arg0, arg1);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getLazyPharmProblem(arg0:PharmProblem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getLazyPharmProblem", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getLazyPharmProblem", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("getLazyPharmProblem", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createPharmProblemRpt(arg0:PharmProblem, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createPharmProblemRpt", arg0, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createPharmProblemRpt", arg0, resultHandler);
            else if (resultHandler == null)
                callProperty("createPharmProblemRpt", arg0);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generatePharmProblemRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generatePharmProblemRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generatePharmProblemRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("generatePharmProblemRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isSuccess(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isSuccess", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isSuccess", resultHandler);
            else if (resultHandler == null)
                callProperty("isSuccess");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getErrorCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getErrorCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getErrorCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getErrorCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
