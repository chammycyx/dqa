/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (QaProblem.as).
 */

package hk.org.ha.model.pms.dqa.persistence.suppperf {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.dqa.persistence.Company;
    import hk.org.ha.model.pms.dqa.persistence.Supplier;
    import hk.org.ha.model.pms.dqa.udt.RecordStatus;
    import hk.org.ha.model.pms.dqa.udt.suppperf.CaseStatus;
    import hk.org.ha.model.pms.dqa.udt.suppperf.HasFlag;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.collections.ListCollectionView;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;
    import org.granite.util.Enum;

    use namespace meta;

    [Managed]
    public class QaProblemBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _caseNum:String;
        private var _caseStatus:CaseStatus;
        private var _caseSummary:String;
        private var _classification:Classification;
        private var _deleteReason:String;
        private var _eventLogList:ListCollectionView;
        private var _faxSummary:FaxSummary;
        private var _manufacturer:Company;
        private var _pharmCompany:Company;
        private var _pharmProblemList:ListCollectionView;
        private var _problemHeader:ProblemHeader;
        private var _qaBatchNumList:ListCollectionView;
        private var _qaProblemFileList:ListCollectionView;
        private var _qaProblemId:Number;
        private var _qaProblemInstitutionList:ListCollectionView;
        private var _qaProblemNature:QaProblemNature;
        private var _qaProblemNatureSubCatList:ListCollectionView;
        private var _recordStatus:RecordStatus;
        private var _similarProblem:HasFlag;
        private var _supplier:Supplier;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is QaProblem) || (property as QaProblem).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set caseNum(value:String):void {
            _caseNum = value;
        }
        public function get caseNum():String {
            return _caseNum;
        }

        public function set caseStatus(value:CaseStatus):void {
            _caseStatus = value;
        }
        public function get caseStatus():CaseStatus {
            return _caseStatus;
        }

        public function set caseSummary(value:String):void {
            _caseSummary = value;
        }
        public function get caseSummary():String {
            return _caseSummary;
        }

        public function set classification(value:Classification):void {
            _classification = value;
        }
        public function get classification():Classification {
            return _classification;
        }

        public function set deleteReason(value:String):void {
            _deleteReason = value;
        }
        public function get deleteReason():String {
            return _deleteReason;
        }

        public function set eventLogList(value:ListCollectionView):void {
            _eventLogList = value;
        }
        public function get eventLogList():ListCollectionView {
            return _eventLogList;
        }

        public function set faxSummary(value:FaxSummary):void {
            _faxSummary = value;
        }
        public function get faxSummary():FaxSummary {
            return _faxSummary;
        }

        public function set manufacturer(value:Company):void {
            _manufacturer = value;
        }
        public function get manufacturer():Company {
            return _manufacturer;
        }

        public function set pharmCompany(value:Company):void {
            _pharmCompany = value;
        }
        public function get pharmCompany():Company {
            return _pharmCompany;
        }

        public function set pharmProblemList(value:ListCollectionView):void {
            _pharmProblemList = value;
        }
        public function get pharmProblemList():ListCollectionView {
            return _pharmProblemList;
        }

        public function set problemHeader(value:ProblemHeader):void {
            _problemHeader = value;
        }
        public function get problemHeader():ProblemHeader {
            return _problemHeader;
        }

        public function set qaBatchNumList(value:ListCollectionView):void {
            _qaBatchNumList = value;
        }
        public function get qaBatchNumList():ListCollectionView {
            return _qaBatchNumList;
        }

        public function set qaProblemFileList(value:ListCollectionView):void {
            _qaProblemFileList = value;
        }
        public function get qaProblemFileList():ListCollectionView {
            return _qaProblemFileList;
        }

        public function set qaProblemId(value:Number):void {
            _qaProblemId = value;
        }
        [Id]
        public function get qaProblemId():Number {
            return _qaProblemId;
        }

        public function set qaProblemInstitutionList(value:ListCollectionView):void {
            _qaProblemInstitutionList = value;
        }
        public function get qaProblemInstitutionList():ListCollectionView {
            return _qaProblemInstitutionList;
        }

        public function set qaProblemNature(value:QaProblemNature):void {
            _qaProblemNature = value;
        }
        public function get qaProblemNature():QaProblemNature {
            return _qaProblemNature;
        }

        public function set qaProblemNatureSubCatList(value:ListCollectionView):void {
            _qaProblemNatureSubCatList = value;
        }
        public function get qaProblemNatureSubCatList():ListCollectionView {
            return _qaProblemNatureSubCatList;
        }

        public function set recordStatus(value:RecordStatus):void {
            _recordStatus = value;
        }
        public function get recordStatus():RecordStatus {
            return _recordStatus;
        }

        public function set similarProblem(value:HasFlag):void {
            _similarProblem = value;
        }
        public function get similarProblem():HasFlag {
            return _similarProblem;
        }

        public function set supplier(value:Supplier):void {
            _supplier = value;
        }
        public function get supplier():Supplier {
            return _supplier;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (isNaN(_qaProblemId))
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_qaProblemId) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:QaProblemBase = QaProblemBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._caseNum, _caseNum, null, this, 'caseNum', function setter(o:*):void{_caseNum = o as String}, false);
               em.meta_mergeExternal(src._caseStatus, _caseStatus, null, this, 'caseStatus', function setter(o:*):void{_caseStatus = o as CaseStatus}, false);
               em.meta_mergeExternal(src._caseSummary, _caseSummary, null, this, 'caseSummary', function setter(o:*):void{_caseSummary = o as String}, false);
               em.meta_mergeExternal(src._classification, _classification, null, this, 'classification', function setter(o:*):void{_classification = o as Classification}, false);
               em.meta_mergeExternal(src._deleteReason, _deleteReason, null, this, 'deleteReason', function setter(o:*):void{_deleteReason = o as String}, false);
               em.meta_mergeExternal(src._eventLogList, _eventLogList, null, this, 'eventLogList', function setter(o:*):void{_eventLogList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._faxSummary, _faxSummary, null, this, 'faxSummary', function setter(o:*):void{_faxSummary = o as FaxSummary}, false);
               em.meta_mergeExternal(src._manufacturer, _manufacturer, null, this, 'manufacturer', function setter(o:*):void{_manufacturer = o as Company}, false);
               em.meta_mergeExternal(src._pharmCompany, _pharmCompany, null, this, 'pharmCompany', function setter(o:*):void{_pharmCompany = o as Company}, false);
               em.meta_mergeExternal(src._pharmProblemList, _pharmProblemList, null, this, 'pharmProblemList', function setter(o:*):void{_pharmProblemList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._problemHeader, _problemHeader, null, this, 'problemHeader', function setter(o:*):void{_problemHeader = o as ProblemHeader}, false);
               em.meta_mergeExternal(src._qaBatchNumList, _qaBatchNumList, null, this, 'qaBatchNumList', function setter(o:*):void{_qaBatchNumList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._qaProblemFileList, _qaProblemFileList, null, this, 'qaProblemFileList', function setter(o:*):void{_qaProblemFileList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._qaProblemId, _qaProblemId, null, this, 'qaProblemId', function setter(o:*):void{_qaProblemId = o as Number}, false);
               em.meta_mergeExternal(src._qaProblemInstitutionList, _qaProblemInstitutionList, null, this, 'qaProblemInstitutionList', function setter(o:*):void{_qaProblemInstitutionList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._qaProblemNature, _qaProblemNature, null, this, 'qaProblemNature', function setter(o:*):void{_qaProblemNature = o as QaProblemNature}, false);
               em.meta_mergeExternal(src._qaProblemNatureSubCatList, _qaProblemNatureSubCatList, null, this, 'qaProblemNatureSubCatList', function setter(o:*):void{_qaProblemNatureSubCatList = o as ListCollectionView}, false);
               em.meta_mergeExternal(src._recordStatus, _recordStatus, null, this, 'recordStatus', function setter(o:*):void{_recordStatus = o as RecordStatus}, false);
               em.meta_mergeExternal(src._similarProblem, _similarProblem, null, this, 'similarProblem', function setter(o:*):void{_similarProblem = o as HasFlag}, false);
               em.meta_mergeExternal(src._supplier, _supplier, null, this, 'supplier', function setter(o:*):void{_supplier = o as Supplier}, false);
            }
            else {
               em.meta_mergeExternal(src._qaProblemId, _qaProblemId, null, this, 'qaProblemId', function setter(o:*):void{_qaProblemId = o as Number});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _caseNum = input.readObject() as String;
                _caseStatus = Enum.readEnum(input) as CaseStatus;
                _caseSummary = input.readObject() as String;
                _classification = input.readObject() as Classification;
                _deleteReason = input.readObject() as String;
                _eventLogList = input.readObject() as ListCollectionView;
                _faxSummary = input.readObject() as FaxSummary;
                _manufacturer = input.readObject() as Company;
                _pharmCompany = input.readObject() as Company;
                _pharmProblemList = input.readObject() as ListCollectionView;
                _problemHeader = input.readObject() as ProblemHeader;
                _qaBatchNumList = input.readObject() as ListCollectionView;
                _qaProblemFileList = input.readObject() as ListCollectionView;
                _qaProblemId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
                _qaProblemInstitutionList = input.readObject() as ListCollectionView;
                _qaProblemNature = input.readObject() as QaProblemNature;
                _qaProblemNatureSubCatList = input.readObject() as ListCollectionView;
                _recordStatus = Enum.readEnum(input) as RecordStatus;
                _similarProblem = Enum.readEnum(input) as HasFlag;
                _supplier = input.readObject() as Supplier;
            }
            else {
                _qaProblemId = function(o:*):Number { return (o is Number ? o as Number : Number.NaN) } (input.readObject());
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_caseNum is IPropertyHolder) ? IPropertyHolder(_caseNum).object : _caseNum);
                output.writeObject((_caseStatus is IPropertyHolder) ? IPropertyHolder(_caseStatus).object : _caseStatus);
                output.writeObject((_caseSummary is IPropertyHolder) ? IPropertyHolder(_caseSummary).object : _caseSummary);
                output.writeObject((_classification is IPropertyHolder) ? IPropertyHolder(_classification).object : _classification);
                output.writeObject((_deleteReason is IPropertyHolder) ? IPropertyHolder(_deleteReason).object : _deleteReason);
                output.writeObject((_eventLogList is IPropertyHolder) ? IPropertyHolder(_eventLogList).object : _eventLogList);
                output.writeObject((_faxSummary is IPropertyHolder) ? IPropertyHolder(_faxSummary).object : _faxSummary);
                output.writeObject((_manufacturer is IPropertyHolder) ? IPropertyHolder(_manufacturer).object : _manufacturer);
                output.writeObject((_pharmCompany is IPropertyHolder) ? IPropertyHolder(_pharmCompany).object : _pharmCompany);
                output.writeObject((_pharmProblemList is IPropertyHolder) ? IPropertyHolder(_pharmProblemList).object : _pharmProblemList);
                output.writeObject((_problemHeader is IPropertyHolder) ? IPropertyHolder(_problemHeader).object : _problemHeader);
                output.writeObject((_qaBatchNumList is IPropertyHolder) ? IPropertyHolder(_qaBatchNumList).object : _qaBatchNumList);
                output.writeObject((_qaProblemFileList is IPropertyHolder) ? IPropertyHolder(_qaProblemFileList).object : _qaProblemFileList);
                output.writeObject((_qaProblemId is IPropertyHolder) ? IPropertyHolder(_qaProblemId).object : _qaProblemId);
                output.writeObject((_qaProblemInstitutionList is IPropertyHolder) ? IPropertyHolder(_qaProblemInstitutionList).object : _qaProblemInstitutionList);
                output.writeObject((_qaProblemNature is IPropertyHolder) ? IPropertyHolder(_qaProblemNature).object : _qaProblemNature);
                output.writeObject((_qaProblemNatureSubCatList is IPropertyHolder) ? IPropertyHolder(_qaProblemNatureSubCatList).object : _qaProblemNatureSubCatList);
                output.writeObject((_recordStatus is IPropertyHolder) ? IPropertyHolder(_recordStatus).object : _recordStatus);
                output.writeObject((_similarProblem is IPropertyHolder) ? IPropertyHolder(_similarProblem).object : _similarProblem);
                output.writeObject((_supplier is IPropertyHolder) ? IPropertyHolder(_supplier).object : _supplier);
            }
            else {
                output.writeObject(_qaProblemId);
            }
        }
    }
}
