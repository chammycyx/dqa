/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (FaxInterimSuppServiceBean.as).
 */

package hk.org.ha.model.pms.dqa.biz.suppperf {

    import flash.utils.flash_proxy;
    import hk.org.ha.model.pms.dqa.persistence.suppperf.EventLog;
    import hk.org.ha.model.pms.dqa.persistence.suppperf.QaProblem;
    import mx.collections.ListCollectionView;
    import org.granite.tide.BaseContext;
    import org.granite.tide.Component;
    import org.granite.tide.ITideResponder;
    
    use namespace flash_proxy;

    public class FaxInterimSuppServiceBeanBase extends Component {

        public function createFaxInterimSupp(arg0:QaProblem, arg1:EventLog, arg2:ListCollectionView, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createFaxInterimSupp", arg0, arg1, arg2, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createFaxInterimSupp", arg0, arg1, arg2, resultHandler);
            else if (resultHandler == null)
                callProperty("createFaxInterimSupp", arg0, arg1, arg2);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function createFaxInterimSuppRpt(arg0:QaProblem, arg1:EventLog, arg2:String, arg3:Boolean, resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("createFaxInterimSuppRpt", arg0, arg1, arg2, arg3, resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("createFaxInterimSuppRpt", arg0, arg1, arg2, arg3, resultHandler);
            else if (resultHandler == null)
                callProperty("createFaxInterimSuppRpt", arg0, arg1, arg2, arg3);
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function generateFaxInterimSuppRpt(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("generateFaxInterimSuppRpt", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("generateFaxInterimSuppRpt", resultHandler);
            else if (resultHandler == null)
                callProperty("generateFaxInterimSuppRpt");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function isSuccess(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("isSuccess", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("isSuccess", resultHandler);
            else if (resultHandler == null)
                callProperty("isSuccess");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getErrorCode(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getErrorCode", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getErrorCode", resultHandler);
            else if (resultHandler == null)
                callProperty("getErrorCode");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function getRefNum(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("getRefNum", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("getRefNum", resultHandler);
            else if (resultHandler == null)
                callProperty("getRefNum");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }

        public function destroy(resultHandler:Object = null, faultHandler:Function = null):void {
            if (faultHandler != null)
                callProperty("destroy", resultHandler, faultHandler);
            else if (resultHandler is Function || resultHandler is ITideResponder)
                callProperty("destroy", resultHandler);
            else if (resultHandler == null)
                callProperty("destroy");
            else
                throw new Error("Illegal argument to remote call (last argument should be Function or ITideResponder): " + resultHandler);
        }
    }
}
