/**
 * Generated by Gas3 v2.1.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.dqa.persistence.coa {
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.utils.ObjectUtil;
	
    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.persistence.coa.CoaItem")]
    public class CoaItem extends CoaItemBase {
		public function get nonNullBatchList():ArrayCollection{
			var nonNullcoaBatchList:ArrayCollection = new ArrayCollection;
			
			for each(var tempCoaBatch:CoaBatch in coaBatchList ){
				if (tempCoaBatch.batchNum !=null)
					nonNullcoaBatchList.addItem(tempCoaBatch);
			}
			
			var batchNumSoft:Sort = new Sort();
			batchNumSoft.compareFunction = batchNumSortFunc;
			nonNullcoaBatchList.sort = batchNumSoft;
			nonNullcoaBatchList.refresh();
			
			return nonNullcoaBatchList;
		}
		public function get nonNullContractNum():String {
			return contract.nonNullContractNum;
		}
		
		private function batchNumSortFunc(o1:CoaBatch, o2:CoaBatch, array:Array= null):int
		{	
			return ObjectUtil..stringCompare(o1.batchNum.toLowerCase(), o2.batchNum.toLowerCase());
		}
    }
}