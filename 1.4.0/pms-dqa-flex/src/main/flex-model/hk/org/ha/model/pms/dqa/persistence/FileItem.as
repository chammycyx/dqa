/**
 * Generated by Gas3 v2.1.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.dqa.persistence {

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.dqa.persistence.FileItem")]
    public class FileItem extends FileItemBase {
		public function get screenFilePath():String{
			var screenFilePath:String = filePath==null?"":filePath.substr(filePath.lastIndexOf('/')+1, filePath.lastIndexOf('.'));
			
			return screenFilePath;
		}
		
    }
}