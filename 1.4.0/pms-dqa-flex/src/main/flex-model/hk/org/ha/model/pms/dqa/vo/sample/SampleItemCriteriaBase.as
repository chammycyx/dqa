/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (SampleItemCriteria.as).
 */

package hk.org.ha.model.pms.dqa.vo.sample {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import hk.org.ha.model.pms.dqa.udt.OrderType;
    import hk.org.ha.model.pms.dqa.udt.RecordStatus;
    import org.granite.util.Enum;

    [Bindable]
    public class SampleItemCriteriaBase implements IExternalizable {

        private var _exclusionCode:String;
        private var _itemCode:String;
        private var _orderType:OrderType;
        private var _recordStatus:RecordStatus;
        private var _riskLevelCode:String;
        private var _testCode:String;

        public function set exclusionCode(value:String):void {
            _exclusionCode = value;
        }
        public function get exclusionCode():String {
            return _exclusionCode;
        }

        public function set itemCode(value:String):void {
            _itemCode = value;
        }
        public function get itemCode():String {
            return _itemCode;
        }

        public function set orderType(value:OrderType):void {
            _orderType = value;
        }
        public function get orderType():OrderType {
            return _orderType;
        }

        public function set recordStatus(value:RecordStatus):void {
            _recordStatus = value;
        }
        public function get recordStatus():RecordStatus {
            return _recordStatus;
        }

        public function set riskLevelCode(value:String):void {
            _riskLevelCode = value;
        }
        public function get riskLevelCode():String {
            return _riskLevelCode;
        }

        public function set testCode(value:String):void {
            _testCode = value;
        }
        public function get testCode():String {
            return _testCode;
        }

        public function readExternal(input:IDataInput):void {
            _exclusionCode = input.readObject() as String;
            _itemCode = input.readObject() as String;
            _orderType = Enum.readEnum(input) as OrderType;
            _recordStatus = Enum.readEnum(input) as RecordStatus;
            _riskLevelCode = input.readObject() as String;
            _testCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_exclusionCode);
            output.writeObject(_itemCode);
            output.writeObject(_orderType);
            output.writeObject(_recordStatus);
            output.writeObject(_riskLevelCode);
            output.writeObject(_testCode);
        }
    }
}