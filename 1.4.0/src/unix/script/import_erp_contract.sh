#!/bin/ksh
####################################################################
#
# Script Name  :   import_erp_contract.sh
#
# Purpose      :   import ERP contract to DQA
#
# History      :   Date        Who         Description
#                  20101209    Terry Wong  Initial version
#
####################################################################

script_name=`basename $0`

echo
echo `date $LOG_DT_FMT`:" $script_name: start"

$JAVA_HOME/bin/java -jar -Dconfig="${JAVAPATH}/conf/pms-dqa-batch-connection.properties" $JAVAPATH/pms-dqa-ejb-client.jar -m importErpContract D_ERP_CONTRACT

status=$?

echo
if [ $status -eq 0 ]
then
        echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
        echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: end"

exit $status