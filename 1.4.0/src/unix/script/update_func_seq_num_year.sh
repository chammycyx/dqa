#!/bin/ksh
####################################################################
#
# Script Name  :   update_func_seq_num_year.sh
#
# Purpose      :   update the yaer column of func_seq_num yearly
#
# History      :   Date        Who         Description
#                  20120110    David Pang  Initial version
#
####################################################################

script_name=`basename $0`

echo
echo `date $LOG_DT_FMT`:" $script_name: start"

$JAVA_HOME/bin/java -jar -Dconfig="${JAVAPATH}/conf/pms-dqa-batch-connection.properties" $JAVAPATH/pms-dqa-ejb-client.jar -m updateFuncSeqNumYear Y_FUNC_SEQ_NUM_YEAR

status=$?

echo
if [ $status -eq 0 ]
then
        echo `date $LOG_DT_FMT`:" $script_name: COMPLETE"
else
        echo `date $LOG_DT_FMT`:" $script_name: FAILURE"
fi

echo
echo `date $LOG_DT_FMT`:" $script_name: end"

exit $status