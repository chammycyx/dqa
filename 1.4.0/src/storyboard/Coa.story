<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels>
    <screen href="maintenance/general/Supplier.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/general/Popup_SupplierContact.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/coa/CoaItem.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/coa/Popup_AddBatchCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="maintenance/coa/Popup_CoaFileDetails.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/CoaFileInventory.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/CoaVerification.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_OpenCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_VerifyCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_ReportDiscrepancy.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/GroupDiscrepancy.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/PharmacistFollowUp.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/GroupDiscrepancy.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/EmailToSupplier.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/ReminderToSupplier.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/SendEmail.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_SetFollowUpDate.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_FailCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_PassCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_Revert.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/Popup_BatchCoaDetails.screen#/"/>
  </panels>
  <panels>
    <screen href="coa/OutstandingBatchCoa.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/CoaDiscrepancyAnalysisReport.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/CoaDiscrepancyReport.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/CoaProcessDateReport.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/CoaProcessReport.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/MissingBatchCoaReport.screen#/"/>
  </panels>
  <panels>
    <screen href="report/coa/MissingCoaItemReport.screen#/"/>
  </panels>
</story:Storyboard>
