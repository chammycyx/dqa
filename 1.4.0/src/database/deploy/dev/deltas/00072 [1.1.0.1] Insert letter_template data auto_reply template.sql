insert into letter_template (CODE,CONTENT,CREATE_USER,SUBJECT,MODIFY_USER,NAME,MODIFY_DATE,CREATE_DATE,RECORD_STATUS,VERSION) values ('RCER','<p>We have encountered problems when downloading file(s) to our system, please check the attached files and resend.</p>{0}{1}','dqaadmin', null,'dqaadmin',null,sysdate,sysdate,'A',1);
insert into letter_template (CODE,CONTENT,CREATE_USER,SUBJECT,MODIFY_USER,NAME,MODIFY_DATE,CREATE_DATE,RECORD_STATUS,VERSION) values ('RTQA','<p>DQA has encountered problems when saving files, please add the COA Item in DQA if necessary and upload the attached batch COA file(s).</p>','dqaadmin', null,'dqaadmin',null,sysdate,sysdate,'A',1);

--//@UNDO
delete letter_template where CODE = 'RCER';
delete letter_template where CODE = 'RTQA';
--//
