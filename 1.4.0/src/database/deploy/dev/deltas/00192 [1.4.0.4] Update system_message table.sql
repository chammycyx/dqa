insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0178', 'EN_US', 'Order qty shall be equal to or greater than outstanding qty', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0179', 'EN_US', 'Invalid BPA No.', 'ITD', sysdate, 'ITD', sysdate, '1');
insert into SYSTEM_MESSAGE (APPLICATION_ID, FUNCTION_ID, SEVERITY_CODE, MESSAGE_CODE, LOCALE, MAIN_MSG, CREATE_USER, CREATE_DATE, MODIFY_USER, MODIFY_DATE, VERSION) values ('1006', '6000', 'W', '0180', 'EN_US', 'Please input email', 'ITD', sysdate, 'ITD', sysdate, '1');

update SYSTEM_MESSAGE set MAIN_MSG = 'Please remove all PO(s) before you change institution code, item code or order type' where MESSAGE_CODE = '0163';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input report date range within one year'  where MESSAGE_CODE = '0170';




--//@UNDO
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0178';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0179';
delete from SYSTEM_MESSAGE where MESSAGE_CODE = '0180';

update SYSTEM_MESSAGE set MAIN_MSG = 'Please remove all PO(s) before you change order type' where MESSAGE_CODE = '0163';
update SYSTEM_MESSAGE set MAIN_MSG = 'Please input valid repot date on range'  where MESSAGE_CODE = '0170';

--//