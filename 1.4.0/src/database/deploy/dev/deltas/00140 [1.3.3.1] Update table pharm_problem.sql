alter table PHARM_PROBLEM add CLASSIFICATION_CODE varchar2(3) null;
alter table PHARM_PROBLEM add RPT_INSTITUTION_CODE varchar2(3) null;

update PHARM_PROBLEM set RPT_INSTITUTION_CODE = INSTITUTION_CODE;
alter table PHARM_PROBLEM modify (RPT_INSTITUTION_CODE not null);

--//@UNDO
alter table PHARM_PROBLEM drop column CLASSIFICATION_CODE;
alter table PHARM_PROBLEM drop column RPT_INSTITUTION_CODE;
--//
