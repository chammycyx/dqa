alter table supplier add constraint fk_supplier_01 foreign key (contact_id) references contact (contact_id);

--//@UNDO
alter table supplier drop constraint fk_supplier_01;
--//