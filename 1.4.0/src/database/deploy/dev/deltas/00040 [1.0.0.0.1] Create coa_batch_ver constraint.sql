alter table coa_batch_ver add constraint fk_coa_batch_ver_01 foreign key (coa_batch_id) references coa_batch (coa_batch_id);

--//@UNDO
alter table coa_batch_ver drop constraint fk_coa_batch_ver_01;
--//