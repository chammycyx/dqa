insert into FUNC_SEQ_NUM (FUNC_CODE,FUNC_DESC,START_NUM,END_NUM,FUNC_PREFIX,LAST_NUM,YEAR,MODIFY_USER,MODIFY_DATE,CREATE_USER,CREATE_DATE,VERSION) values 
('RS','Release of Retention Sample',1,99999999,'RS',0,null,'dqaadmin',sysdate,'dqaadmin',sysdate,0);

--//@UNDO
delete FUNC_SEQ_NUM where FUNC_CODE = 'RS';
--//
