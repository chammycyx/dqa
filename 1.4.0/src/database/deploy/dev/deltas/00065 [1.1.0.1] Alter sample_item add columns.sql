ALTER TABLE SAMPLE_ITEM ADD (
	EXCLUSION VARCHAR2(30) NULL,
	EXPORT_DATE TIMESTAMP NULL,
	RELEASE_INDICATOR VARCHAR2(1) NULL
);

--//@UNDO
ALTER TABLE SAMPLE_ITEM DROP COLUMN EXCLUSION;
ALTER TABLE SAMPLE_ITEM DROP COLUMN EXPORT_DATE;
ALTER TABLE SAMPLE_ITEM DROP COLUMN RELEASE_INDICATOR;
--//