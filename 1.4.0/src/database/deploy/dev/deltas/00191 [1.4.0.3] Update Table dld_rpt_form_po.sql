alter table DLD_RPT_FORM modify CONFLICT_MSP_NAME varchar2(150) ;
alter table DLD_RPT_FORM modify CONFLICT_BY_RANK varchar2(50) ;
alter table DLD_RPT_FORM_PO add PACK_SIZE varchar2(100) null;
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid Item Code' where MESSAGE_CODE  = '0030';

--//@UNDO
alter table DLD_RPT_FORM modify CONFLICT_MSP_NAME varchar2(15) ;
alter table DLD_RPT_FORM modify CONFLICT_BY_RANK varchar2(19) ;
alter table DLD_RPT_FORM_PO drop column PACK_SIZE;
update SYSTEM_MESSAGE set MAIN_MSG = 'Invalid item' where MESSAGE_CODE  = '0030';
--//