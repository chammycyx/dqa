create table coa_discrepancy_key (
	discrepancy_id number(19) not null, 
	discrepancy_desc varchar2(200) not null, 
	order_seq number(10) null, 
	record_status varchar2(1) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 			
	version number(19) not null, 
	constraint pk_coa_discrepancy_key primary key (discrepancy_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_discrepancy_key;
--//