create table coa_checklist_key (
	coa_checklist_id number(19) not null, 
	name varchar2(200) not null, 
	order_seq number(10) null, 
	record_status varchar2(1) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 	
	modify_date timestamp not null, 		
	version number(19) not null, 
	constraint pk_coa_checklist_key primary key (coa_checklist_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table coa_checklist_key;
--//
