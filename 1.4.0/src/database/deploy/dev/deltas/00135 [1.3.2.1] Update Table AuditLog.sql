alter table AUDIT_LOG modify WORKSTATION_ID varchar2(100);

alter table AUDIT_LOG add EXCEPTION_PACKAGE_NAME varchar2(100) null;
alter table AUDIT_LOG add EXCEPTION_CLASS_NAME varchar2(100) null;
alter table AUDIT_LOG add EXCEPTION_MESSAGE varchar2(1000) null;
alter table AUDIT_LOG add CAUSE_PACKAGE_NAME varchar2(100) null;
alter table AUDIT_LOG add CAUSE_CLASS_NAME varchar2(100) null;
alter table AUDIT_LOG add CAUSE_MESSAGE varchar2(1000) null;

--//@UNDO
alter table AUDIT_LOG drop column EXCEPTION_PACKAGE_NAME;
alter table AUDIT_LOG drop column EXCEPTION_CLASS_NAME;
alter table AUDIT_LOG drop column EXCEPTION_MESSAGE;
alter table AUDIT_LOG drop column CAUSE_PACKAGE_NAME;
alter table AUDIT_LOG drop column CAUSE_CLASS_NAME;
alter table AUDIT_LOG drop column CAUSE_MESSAGE;
--//
