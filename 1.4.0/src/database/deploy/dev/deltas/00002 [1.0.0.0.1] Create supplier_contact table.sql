create table supplier_contact (
	supplier_contact_id number(19) not null, 
	supplier_code varchar2(5) null,
	contact_id number(19) null, 
	create_user varchar2(15) not null, 
	create_date timestamp not null, 
	modify_user varchar2(15) not null, 
	modify_date timestamp not null, 
	version number(19) not null, 
	constraint pk_supplier_contact primary key (supplier_contact_id) using index tablespace dqa_indx_01)
tablespace dqa_data_01;

--//@UNDO
drop table supplier_contact;
--//