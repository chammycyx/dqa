CREATE TABLE COMPANY_CONTACT (
	COMPANY_CONTACT_ID NUMBER(19) NOT NULL,
	COMPANY_CODE VARCHAR2(255) NULL,
	CONTACT_ID NUMBER(19) NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL, 
	CONSTRAINT PK_COMPANY_CONTACT PRIMARY KEY (COMPANY_CONTACT_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;


--//
ALTER TABLE LETTER_TEMPLATE ADD MODULE_TYPE VARCHAR2(1) NULL;


--//
CREATE TABLE CHEMICAL_ANALYSIS (
	CHEMICAL_ANALYSIS_ID NUMBER(19) NOT NULL, 
	MOA VARCHAR2(255) NULL, 
	FPS VARCHAR2(255) NULL, 
	REF_STD VARCHAR2(255) NULL,
	REF_STD_REQ_FLAG VARCHAR2(255) NULL,
	RETENTION_QTY NUMBER(10) NULL, 
	TEST VARCHAR2(255) NULL, 
	TEST_DESC VARCHAR2(80) NULL, 
	SPECIFICATION VARCHAR2(255) NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL, 
	CREATE_DATE TIMESTAMP NOT NULL,
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL,
	CONSTRAINT PK_CHEMICAL_ANALYSIS PRIMARY KEY (CHEMICAL_ANALYSIS_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE LETTER (
	LETTER_ID NUMBER(19) NOT NULL, 
	LETTER_FROM VARCHAR2(100) NULL, 
	LETTER_TO VARCHAR2(500) NULL, 
	LETTER_CC VARCHAR2(500) NULL, 
	SUBJECT VARCHAR2(300) NULL, 
	CONTENT CLOB NULL, 
	ATT_FILE_NAMES VARCHAR2(3000) NULL, 
	LETTER_TYPE VARCHAR2(1) NULL, 
	REF_NUM VARCHAR2(15) NULL,
	SEND_DATE TIMESTAMP NULL, 
	SEND_AT_SCHED_STATUS VARCHAR2(255) NULL,
	LETTER_CODE VARCHAR2(10) NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL, 
	CONSTRAINT PK_LETTER PRIMARY KEY (LETTER_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE MICRO_BIO_TEST (
	MICRO_BIO_TEST_ID NUMBER(19) NOT NULL, 
	RAW_MATERIAL VARCHAR2(255) NULL, 
	VALIDATION_RPT VARCHAR2(255) NULL,
	VALIDATION_RPT_VALID_FLAG VARCHAR2(255) NULL, 
	TEST_PARAM VARCHAR2(255) NULL, 
	NATIONAL_PHARM_STD VARCHAR2(255) NULL, 
	NATIONAL_PHARM_STD_DESC VARCHAR2(255) NULL,
	ACCEPT_CRITERIA VARCHAR2(255) NULL,
	ACCEPT_CRITERIA_DESC VARCHAR2(255) NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL,
	CONSTRAINT PK_MICRO_BIO_TEST PRIMARY KEY (MICRO_BIO_TEST_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE SAMPLE_ITEM (
	SAMPLE_ITEM_ID NUMBER(19) NOT NULL, 
	ITEM_CODE VARCHAR2(10) NULL, 
	RISK_LEVEL_CODE VARCHAR2(3) NULL,
	SPECIAL_CATEGORY VARCHAR2(2) NULL,
	CHEM_ANALYSIS_QTY_REQ VARCHAR2(4) NULL, 
	CHEM_ANALYSIS_QTY_REQ_DESC VARCHAR2(80) NULL,
	MICRO_BIO_TEST_QTY_REQ VARCHAR2(4) NULL,
	MICRO_BIO_TEST_QTY_REQ_DESC VARCHAR2(80) NULL, 
	VALID_TEST_QTY_REQ VARCHAR2(4) NULL,  
	VALID_TEST_QTY_REQ_DESC VARCHAR2(80) NULL,
	CHEM_ANALYSIS_REMARK VARCHAR2(80) NULL,
	MICRO_BIO_TEST_REMARK VARCHAR2(80) NULL,
	EXCLUSION VARCHAR2(166) NULL, 
	REMARK VARCHAR2(80) NULL, 
	EXPORT_DATE TIMESTAMP NULL, 
	RECORD_STATUS VARCHAR2(255) NULL,
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL, 
	CONSTRAINT PK_SAMPLE_ITEM PRIMARY KEY (SAMPLE_ITEM_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE SAMPLE_MOVEMENT (
	SAMPLE_LOG_ID NUMBER(19) NOT NULL, 
	LOCATION_CODE VARCHAR2(4) NULL,
	MANUF_CODE VARCHAR2(4) NULL, 
	SCHEDULE_ID NUMBER(19) NULL, 
	MOVEMENT_QTY NUMBER(10) NULL,
	ON_HAND_QTY NUMBER(10) NULL,
	ACTION_CODE VARCHAR2(1) NULL,
	ADJUST_FLAG VARCHAR2(1) NULL, 
	ADJUST_REASON VARCHAR2(50) NULL, 
	ACTION_DATE TIMESTAMP NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL,
	VERSION NUMBER(19) NOT NULL, 	
	CONSTRAINT PK_SAMPLE_MOVEMENT PRIMARY KEY (SAMPLE_LOG_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE STOCK_MONTHLY_EXP (
	INSTITUTION_CODE VARCHAR2(3) NOT NULL,
	ITEM_CODE VARCHAR2(255) NOT NULL,
	MONTH NUMBER(10) NOT NULL, 
	YEAR NUMBER(10) NOT NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL, 
	CREATE_DATE TIMESTAMP NOT NULL, 
	MODIFY_USER VARCHAR2(15) NOT NULL, 
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL, 
	CONSTRAINT PK_STOCK_MONTHLY_EXP PRIMARY KEY (MONTH, YEAR, ITEM_CODE, INSTITUTION_CODE) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//
CREATE TABLE SAMPLE_TEST_SCHEDULE (
	SCHEDULE_ID NUMBER(19) NOT NULL, 
	SCHEDULE_NUM VARCHAR2(15) NULL,
	SCHEDULE_MONTH TIMESTAMP NULL,
	SCHEDULE_STATUS VARCHAR2(255) NOT NULL,
	SCHEDULE_TYPE VARCHAR2(255) NOT NULL, 
	SCHEDULE_CONFIRM_DATE TIMESTAMP NULL,
	SCHEDULE_START_DATE TIMESTAMP NULL, 
	ORDER_TYPE VARCHAR2(255) NOT NULL, 
	BATCH_NUM VARCHAR2(20) NULL,
	EXPIRY_DATE VARCHAR2(6) NULL, 
	SAMPLE_ITEM_ID NUMBER(19) NULL,
	INSTITUTION_CODE VARCHAR2(3) NULL, 
	TEST_CODE VARCHAR2(5) NOT NULL, 
	LAB_CODE VARCHAR2(255) NULL, 
	REQ_QTY NUMBER(10) NULL, 
	REC_QTY NUMBER(10) NULL, 
	RECEIVE_DATE TIMESTAMP NULL, 
	LAB_COLLECT_NUM VARCHAR2(15) NULL, 
	LAB_COLLECT_DATE TIMESTAMP NULL, 
	TEST_RESULT VARCHAR2(3) NULL, 
	TEST_RESULT_DESC VARCHAR2(80) NULL, 
	TEST_REPORT_DATE TIMESTAMP NULL, 
	TEST_PRICE NUMBER(19,4) NULL, 
	REF_DRUG_COST NUMBER(19,4) NULL, 
	UNIT_PRICE NUMBER(19,4) NULL, 
	LAB_COLLECT_QTY NUMBER(10) NULL, 
	CANCEL_SCHED_REASON VARCHAR2(255) NULL, 
	NO_NEXT_SCHED_REASON VARCHAR2(40) NULL, 
	LAST_SCHEDULE TIMESTAMP NULL, 
	LAST_DOC_REQUEST_DATE TIMESTAMP NULL, 
	ACTION_FLAG VARCHAR2(1) NULL,
	CHEMICAL_ANALYSIS_ID NUMBER(19) NULL, 
	PAYMENT_ID NUMBER(19) NULL, 
	MICRO_BIO_TEST_ID NUMBER(19) NULL, 
	CONTRACT_ID NUMBER(19) NULL, 
	FREQUENCY_ID NUMBER(19) NULL, 
	RECORD_STATUS VARCHAR2(255) NOT NULL, 
	CREATE_USER VARCHAR2(15) NOT NULL,
	CREATE_DATE TIMESTAMP NOT NULL,
	MODIFY_USER VARCHAR2(15) NOT NULL,
	MODIFY_DATE TIMESTAMP NOT NULL, 
	VERSION NUMBER(19) NOT NULL,
	CONSTRAINT PK_SAMPLE_TEST_SCHEDULE PRIMARY KEY (SCHEDULE_ID) USING INDEX TABLESPACE DQA_INDX_01)
TABLESPACE DQA_DATA_01;

--//@UNDO
DROP TABLE COMPANY_CONTACT;
ALTER TABLE LETTER_TEMPLATE DROP COLUMN MODULE_TYPE;
DROP TABLE CHEMICAL_ANALYSIS;
DROP TABLE LETTER;
DROP TABLE MICRO_BIO_TEST;
DROP TABLE SAMPLE_ITEM;
DROP TABLE SAMPLE_MOVEMENT;
DROP TABLE STOCK_MONTHLY_EXP;
DROP TABLE SAMPLE_TEST_SCHEDULE;
--//